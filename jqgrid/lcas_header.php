<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">

  <!-- Always force latest IE rendering engine or request Chrome Frame -->
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">
  <script src="assets/js/jquery-1.10.1.min.js"></script>
  <!-- Use title if it's in the page YAML frontmatter -->
  <title> <?php echo $page_title;?></title>
  <link href="../stylesheets/jquery-ui.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="../stylesheets/application.css" media="screen" rel="stylesheet" type="text/css" />
 <link href="../stylesheets/tinybox2.css" media="screen" rel="stylesheet" type="text/css" />
  <script src="../javascripts/application.js" type="text/javascript"></script>
 <script src="../javascripts/tinybox2/tinybox.js" type="text/javascript"></script>
<script>

$(document).ready(function() {
    
});
</script>
<style>
#spinner {
position: fixed;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.5;
background: url('images/preload.gif') 50% 50% no-repeat #ede9df;
}
</style>
</head>

