<?php
session_start();

require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");

validate_login();

$page_name = "leadcard_jqgrid.php";
$page_title = $site_name." -  Lead Listing";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<link rel='stylesheet' type='text/css' href='assets/css/jquery-ui-1.10.4.custom.css' />
<link rel='stylesheet' type='text/css' href='assets/css/ui.jqgrid.css' />
<link rel="stylesheet" type="text/css" href="assets/css/ui.multiselect.css" />
<link rel="stylesheet" type="text/css" media="all" href="assets/css/daterangepicker-bs3.css" />

<!--<script src="assets/js/jquery-1.10.1.min.js"></script>
<script type='text/javascript' src='assets/js/jquery-ui-custom.min.js'></script>-->

<script src="assets/js/jquery-1.7.1.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/js/ui.multiselect.js"></script>     
<script type='text/javascript' src='assets/js/grid.locale-en.js'></script>
<script type='text/javascript' src='assets/js/jquery.jqGrid.js'></script>

<script type="text/javascript" src="assets/js/moment.js"></script>
<script type="text/javascript" src="assets/js/daterangepicker.js"></script>

<script type="text/javascript" src="../javascripts/bootbox.min.js"></script>

<script type="text/javascript">

var lastsel3=0;

var template1 = {"groupOp":"AND","rules":[{"field":"fname1","op":"bw","data":"cus"}]};
var template2 = {"groupOp":"AND","rules":[{"field":"fname1","op":"bw","data":"cus"}]};

$(document).ready(function () {

	var mySearchOptions = {
			// it's just some options which you use
			multipleSearch: true,
			multipleGroup: true,
			recreateFilter: true,
			closeOnEscape: true,
			closeAfterSearch: true,
			showQuery: true,
			overlay: 0,
			afterRedraw: function () {
			    $('input.add-rule',this).val('Add new rule').button();
			    $('input.add-group',this).val('Add new group or rules').button();
			    $('input.delete-rule',this).val('Delete rule').button();
			    $('input.delete-group',this).val('Delete group').button();
			},
			onClose:function()
		        {
		       
		          return true; 
		        }
			//"tmplNames" : ["Template One", "Template Two"],
			//"tmplFilters": [template1, template2],

		    };

 	
	var grid = $("#list_records");

	grid.jqGrid({
		url: "get_lead_details.php?showtmpl=1",
		datatype: "json",
		mtype: "GET",
		colNames: ["id", "leadid", "Actions", "Lead Name", "Address", "City", "State", "Zip", "Phone No ", "Lead In Date", "Lead Type", "Lead Subtype", "Referred By", "Lead Dealer","Start Contacting", "Set Date", "Set By", "Lead Status", "Lead Result", "Qual", "Modified On", "Comments"],
		colModel: [
			{ name: "id", index:'id', sortable: true, hidden:true},
			{ name: "leadid", index:'leadid', sortable: true, hidden:true},
			{ name: "actions", index:'leadid', sortable: false, align:"center", search:false, width:"100" },
			{ name: "leadname", index:'lname1', sortable: true, width:"150" },
			{ name: "leadaddress", index:'add_line1', sortable: true, width:"200"},
			{ name: "city", index:'city', sortable: true, hidden:true },
			{ name: "state", index:'state', sortable: true, hidden:true },
			{ name: "zip", index:'zip', sortable: true, hidden:true },
			{ name: "phoneno", index:'phone1', sortable: true},
			{ name: "leadin", index:'lead_in', sortable: true, editable: false, search:true, stype:'text',
				searchoptions: { 
					sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'], 
					sorttype: "datetime",
					dataInit: function(elem){ 

						 $(elem).daterangepicker(
						 { 
							ranges: {
								 'Today': [moment(), moment()],
								 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
								 'Last 7 Days': [moment().subtract('days', 6), moment()],
								 'Next 7 Days': [moment(), moment().add('days', 6)],
 								 'Last 14 Days': [moment().subtract('days', 13), moment()],
								 'Next 14 Days': [moment(), moment().add('days', 13)],
								 'Last 30 Days': [moment().subtract('days', 29), moment()],
								 'This Month': [moment().startOf('month'), moment().endOf('month')],
					 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
							      },
							startDate: moment().subtract('days', 29),
							endDate: moment()
						 },
						 function(start, end) {
							$(elem).val(start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY'));
							//$('#'+elem.id).val(start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY'));
							var test12=$(elem).val();
							//$.grid[0].triggerToolbar();
							 if (elem.id.substr(0, 3) === "gs_") {
								// call triggerToolbar only in case of searching toolbar
								setTimeout(function () {
								    grid[0].triggerToolbar();
								}, 100);
						    	}
						 }
					       );
						
					}
				} 
			},
			{ name: "leadtype", index:'lead_type', sortable: true, stype:'select', 
				searchoptions: {
					dataUrl: 'get_search_info.php?type=lt',
	                         	buildSelect:function(resp){
				        var sel= '<select><option value="">All</option>';
				        var obj = $.parseJSON(resp);
				        $.each(obj, function() {
					  sel += '<option value="'+this['value']+ '">'+this['label'] + "</option>"; 
				        });
				     sel+='</select>';

				     return sel;
					},
					dataEvents: [
					    { type: 'change', fn: function (e) { 

							var thisval = $(e.target).val();

							$.get('get_search_info.php?type=ltlst&lt='+thisval, function(data){ 
								data=data.trim();
								$("#gs_leadsubtype").empty();
								$("#gs_leadsubtype").html(data);
							
							}); 


							} 
						},
					    { type: 'keyup', fn: function (e) { $(e.target).trigger('change'); } }
					]
				}
			},
			{ name: "leadsubtype", index:'lead_subtype', sortable: true, stype:'select',
				searchoptions: {
					dataUrl: 'get_search_info.php?type=lst',
	                         	buildSelect:function(resp){
				        var sel= '<select><option value="">All</option>';
				        var obj = $.parseJSON(resp);
				        $.each(obj, function() {
					  sel += '<option value="'+this['value']+ '">'+this['label'] + "</option>"; 
				        });
				     sel+='</select>';

				     return sel;
					}
				}

			},
			{ name: "refby", index:'referred_by', sortable: true, stype:'select', 
				searchoptions: {
					dataUrl: 'get_search_info.php?type=refby',
	                         	buildSelect:function(resp){
				        var sel= '<select><option value="">All</option>';
				        var obj = $.parseJSON(resp);
				        $.each(obj, function() {
					  sel += '<option value="'+this['value']+ '">'+this['label'] + "</option>"; 
				        });
				     sel+='</select>';

				     return sel;
					}
				}
			},
			{ name: "lead_dealer", index:'lead_dealer', sortable: true, stype:'select', 
				searchoptions: {
					dataUrl: 'get_search_info.php?type=ldr',
	                         	buildSelect:function(resp){
				        var sel= '<select><option value="">All</option>';
				        var obj = $.parseJSON(resp);
				        $.each(obj, function() {
					  sel += '<option value="'+this['value']+ '">'+this['label'] + "</option>"; 
				        });
				     sel+='</select>';

				     return sel;
					}
				}
			},
			{ name: "startcontact", index:'oktocall', sortable: true, 
				searchoptions: { 
					sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'], 
					sorttype: "datetime",
					dataInit: function(elem){ 

						 $(elem).daterangepicker(
						 { 
							ranges: {
								 'Today': [moment(), moment()],
								 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
								 'Last 7 Days': [moment().subtract('days', 6), moment()],
								 'Next 7 Days': [moment(), moment().add('days', 6)],
 								 'Last 14 Days': [moment().subtract('days', 13), moment()],
								 'Next 14 Days': [moment(), moment().add('days', 13)],
								 'Last 30 Days': [moment().subtract('days', 29), moment()],
								 'This Month': [moment().startOf('month'), moment().endOf('month')],
					 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
							      },
							startDate: moment().subtract('days', 29),
							endDate: moment()
						 },
						 function(start, end) {
							$(elem).val(start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY'));
							
							var test12=$(elem).val();
							
							 if (elem.id.substr(0, 3) === "gs_") {
								// call triggerToolbar only in case of searching toolbar
								setTimeout(function () {
								    grid[0].triggerToolbar();
								}, 100);
						    	}
						 }
					       );
						
					}
				} 

			},
			{ name: "setdate", index:'setdate', sortable: true, 
				searchoptions: { 
					sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'], 
					sorttype: "datetime",
					dataInit: function(elem){ 

						 $(elem).daterangepicker(
						 { 
							ranges: {
								 'Today': [moment(), moment()],
								 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
								 'Last 7 Days': [moment().subtract('days', 6), moment()],
								 'Next 7 Days': [moment(), moment().add('days', 6)],
 								 'Last 14 Days': [moment().subtract('days', 13), moment()],
								 'Next 14 Days': [moment(), moment().add('days', 13)],
								 'Last 30 Days': [moment().subtract('days', 29), moment()],
								 'This Month': [moment().startOf('month'), moment().endOf('month')],
					 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
							      },
							startDate: moment().subtract('days', 29),
							endDate: moment()
						 },
						 function(start, end) {
							$(elem).val(start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY'));
							
							var test12=$(elem).val();
							
							 if (elem.id.substr(0, 3) === "gs_") {
								// call triggerToolbar only in case of searching toolbar
								setTimeout(function () {
								    grid[0].triggerToolbar();
								}, 100);
						    	}
						 }
					       );
						
					}
				} 
			},
			{ name: "setby", index:'setby', sortable: true, stype:'select', 
				searchoptions: {
					dataUrl: 'get_search_info.php?type=ldr',
	                         	buildSelect:function(resp){
				        var sel= '<select><option value="">All</option>';
				        var obj = $.parseJSON(resp);
				        $.each(obj, function() {
					  sel += '<option value="'+this['value']+ '">'+this['label'] + "</option>"; 
				        });
				     sel+='</select>';

				     return sel;
					}
				}
			},
			{ name: "leadstatus", index:'lead_status', sortable: true, formatter:lsFormatter, stype:'select', 
				searchoptions: {
					dataUrl: 'get_search_info.php?type=ls',
	                         	buildSelect:function(resp){
				        var sel= '<select><option value="">All</option>';
				        var obj = $.parseJSON(resp);
				        $.each(obj, function() {
					  sel += '<option value="'+this['value']+ '">'+this['label'] + "</option>"; 
				        });
				     sel+='</select>';

				     return sel;
					}
				}
			},
			{ name: "leadresult", index:'lead_result', sortable: true, stype:'select', 
				searchoptions: {
					dataUrl: 'get_search_info.php?type=lr',
	                         	buildSelect:function(resp){
				        var sel= '<select><option value="">All</option>';
				        var obj = $.parseJSON(resp);
				        $.each(obj, function() {
					  sel += '<option value="'+this['value']+ '">'+this['label'] + "</option>"; 
				        });
				     sel+='</select>';

				     return sel;
					}
				}
			},
			{ name: "leadquali", index:'lead_qualification', sortable: true, stype:'select', 
				searchoptions: {
					dataUrl: 'get_search_info.php?type=qual',
	                         	buildSelect:function(resp){
				        var sel= '<select><option value="">All</option>';
				        var obj = $.parseJSON(resp);
				        $.each(obj, function() {
					  sel += '<option value="'+this['value']+ '">'+this['label'] + "</option>"; 
				        });
				     sel+='</select>';

				     return sel;
					}
				}
			},
			{ name: "Modified On", index:'last_modifiedon', sortable: true, search:true, stype:'text',
				searchoptions: { 
					sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'], 
					sorttype: "datetime",
					dataInit: function(elem){ 

						 $(elem).daterangepicker(
						 { 
							ranges: {
								 'Today': [moment(), moment()],
								 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
								 'Last 7 Days': [moment().subtract('days', 6), moment()],
								 'Next 7 Days': [moment(), moment().add('days', 6)],
 								 'Last 14 Days': [moment().subtract('days', 13), moment()],
								 'Next 14 Days': [moment(), moment().add('days', 13)],
								 'Last 30 Days': [moment().subtract('days', 29), moment()],
								 'This Month': [moment().startOf('month'), moment().endOf('month')],
					 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
							      },
							startDate: moment().subtract('days', 29),
							endDate: moment()
						 },
						 function(start, end) {
							$(elem).val(start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY'));
							var test12=$(elem).val();
							 if (elem.id.substr(0, 3) === "gs_") {
								// call triggerToolbar only in case of searching toolbar
								setTimeout(function () {
								    grid[0].triggerToolbar();
								}, 100);
						    	}
						 }
					       );
						
					}
				} 
			},
			{ name: "Comments", index:'last_comments', sortable: true, width:"150" },
		],
		pager: "#pager",
		rowNum:10, 
		rowList:[10,20,30,50,100],
		sortname: "id",
		sortorder: "desc",
		autowidth: true,
		height: 400,
		viewrecords: true,
		shrinkToFit: false,
		forceFit:false,
		gridview: true,
		multiselect: true,
		jsonReader : { 
			root: "rows", 
			page: "page", 
			total: "total", 
			records: "records", 
			repeatitems: true, 
			cell: "cell", 
			id: "id",
			userdata: "userdata", 
			//subgrid: {root:"rows", repeatitems: true, cell:"cell" } 
		},
		loadComplete: function (data1) {

			var data = {
		    		type: 'get'
			}

			$.ajax({
	    		type: "POST",
			data:data,
	    		url:  "get_search_template.php",
	    		success: function(output) {
			var data =JSON.parse(output);

			//alert("data.tmplNames : "+data.tmplNames+" ; data.tmplFilters : "+data.tmplFilters);

				if (data.tmplNames && data.tmplFilters) {
				    mySearchOptions.tmplNames = data.tmplNames;
				    mySearchOptions.tmplFilters = data.tmplFilters;
				}
			}

			});


	        },
		caption: "Lead Listing",
		onSelectRow: function(id){ 
			if(id && id!==lastsel3){ 
				grid.jqGrid('restoreRow',lastsel3); 
				grid.jqGrid('editRow',id,true,pickdates); lastsel3=id; 
			} 
			
		},
		editurl: "edit_leads.php",
		toolbar: [true,"top"],
		grouping: true, 
		groupingView : { 
				groupField : [], 
				groupColumnShow : [true], 
				groupText : ['<b>{0} ({1})</b>'], 
				groupCollapse : false, 
				groupOrder: ['asc'], 
				groupSummary : [false], 
				groupDataSorted : true 
		},
		beforeRequest: function() {
	            responsive_jqgrid(grid);
        	},
		subGrid: true,
		subGridOptions: { 
			"plusicon" : "ui-icon-triangle-1-e", 
			"minusicon" : "ui-icon-triangle-1-s", 
			"openicon" : "ui-icon-arrowreturn-1-e", // load the subgrid data only once // and the just show/hide 
			"reloadOnExpand" : false, // select the row when the expand column is clicked 
			"selectOnExpand" : true 
		}, 
		subGridRowExpanded: function(subgrid_id, row_id) { 
			var subgrid_table_id, pager_id; 
			subgrid_table_id = subgrid_id+"_t"; 
			pager_id = "p_"+subgrid_table_id; 

			$("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
			
			jQuery("#"+subgrid_table_id).jqGrid({ 
				
				url:"leadsubgrid.php?id="+row_id, 
				datatype: "json",
				mtype: "GET",
				colNames: ['Id','Name & Address','Lead Details','Appointment Details','Survey Details', 'Google Map'], 
				colModel: [ 
						{name:"id",index:"id",key:true,hidden:true}, 
						{name:"contactinfo",index:"fname1",align:"top"}, 
						{name:"leaddetails",index:"leaddetails",align:"center"}, 
						{name:"apptinfo",index:"apptinfo",align:"center"},
						{name:"surveyinfo",index:"surveyinfo",align:"center"},
						{name:"gmapinfo",index:"gmapinfo",align:"center",sortable:false} 
					], 
				rowNum:20, 
				pager: pager_id, 
				sortname: 'id', 
				sortorder: "desc", 
				viewrecords: true,
				autoencode:false,
				autowidth:true,
				height: '100%',
				cmTemplate: { title: false },
				jsonReader : { 
					root: "rows", 
					page: "page", 
					total: "total", 
					records: "records", 
					repeatitems: true, 
					cell: "cell", 
					id: "id",
					userdata: "userdata", 
					subgrid: {root:"rows", repeatitems: true, cell:"cell" } 
				},
			}); 
			
			jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false,search:false});

		} //end of subGridRowExpanded	
		
	}); 	// end of main grid

	grid.jqGrid('navGrid','#pager', {edit:false,add:false,del:false,pdf:true }, {}, {}, {}, mySearchOptions );

	grid.jqGrid('navButtonAdd',"#pager",{caption:"",title:"Show/Hide Search Filters", buttonicon :'ui-icon-signal-diag', onClickButton:function(){ grid[0].toggleToolbar() } }); 

	grid.jqGrid('navButtonAdd',"#pager",{caption:"",title:"Clear Search Filters", buttonicon :'ui-icon-arrow-4-diag', onClickButton:function(){ grid[0].clearToolbar() } });

	grid.jqGrid('navButtonAdd', '#pager', {
		id: 'pager_excel',
		caption: 'Excel',
		title: 'Export To Excel',
		onClickButton: function (e) {
			var cols = [];
			var mycolModel = grid.getGridParam("colModel");
			var mycolNames = grid.getGridParam("colNames");
			$.each(mycolModel, function(i) {
				if (!this.hidden) {
				    //cols.push(this.name);
					cols.push(mycolNames[i]);
				}
			});

			var pdata = grid.jqGrid('getGridParam', 'postData');
			var colsJ = JSON.stringify(cols);
			var params = jQuery.param(pdata);
			params = params + "&columns=" + colsJ;
		
			var url = 'export_leads.php' + "?showtmpl=1&ept=excel&" + params;
		       // window.location = url;
			window.open(url);

		},
		buttonicon: 'ui-icon-extlink'
        });

	grid.jqGrid('navButtonAdd', '#pager', {
		id: 'pager_word',
		caption: 'Word',
		title: 'Export To Word',
		onClickButton: function (e) {
			var cols = [];
			var mycolModel = grid.getGridParam("colModel");
			var mycolNames = grid.getGridParam("colNames");
			$.each(mycolModel, function(i) {
				if (!this.hidden) {
				    //cols.push(this.name);
					cols.push(mycolNames[i]);
				}
			});

			var pdata = grid.jqGrid('getGridParam', 'postData');
			var colsJ = JSON.stringify(cols);
			var params = jQuery.param(pdata);
			params = params + "&columns=" + colsJ;
		
			var url = 'export_leads.php' + "?showtmpl=1&ept=word&" + params;
		       // window.location = url;
			window.open(url);

		},
		buttonicon: 'ui-icon-extlink'
        });

	grid.jqGrid('navButtonAdd', '#pager', {
		id: 'pager_pdf',
		caption: 'Pdf',
		title: 'Export To Pdf',
		onClickButton: function (e) {
			var cols = [];
			var mycolModel = grid.getGridParam("colModel");
			var mycolNames = grid.getGridParam("colNames");
			$.each(mycolModel, function(i) {
				if (!this.hidden) {
				    //cols.push(this.name);
					cols.push(mycolNames[i]);
				}
			});

			var pdata = grid.jqGrid('getGridParam', 'postData');
			var colsJ = JSON.stringify(cols);
			var params = jQuery.param(pdata);
			params = params + "&columns=" + colsJ;
		
			var url = 'export_leads.php' + "?showtmpl=1&ept=pdf&" + params;
		       // window.location = url;
			window.open(url);

		},
		buttonicon: 'ui-icon-extlink'
        });

	grid.jqGrid('navButtonAdd','#pager',{ caption: "", title: "Show/Hide Columns", 
		onClickButton : function (){ 
			grid.jqGrid("columnChooser", {
				done: function(perm) {
				  if (perm) {
				  	grid.jqGrid("remapColumns", perm, true);
				  }
				  grid.trigger("resize");
				}
			      });
	
		} //end of btn click
	});

	grid.jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false, defaultSearch: 'cn'});

	var btns="<input type='button' id='savetmpl' class='btntmpl' value='Save this Search'> &nbsp;&nbsp; <input type='button' id='viewtmpl' class='btntmpl' value='View Search Templates'> &nbsp;&nbsp; <input type='button' id='dfttmpl' class='btntmpl' value='My Default Search Template'> &nbsp;&nbsp; <input type='button' id='resetgrid' class='btntmpl' value=' Reset Grid '> &nbsp;&nbsp; <input type='button' id='removegrouping' class='btntmpl' value=' Remove Grouping '> ";

	$("#t_list_records").append(btns); 
	
	$("input#savetmpl","#t_list_records").click(function(){ 

		var postData = grid.jqGrid("getGridParam", "postData");
		var filters=postData.filters;

		showUrlInDialog('get_search_template.php?type=getHtmlNew', {error: function() { alert('Could not load form') }}); 

		function showUrlInDialog(url, options){
		    options = options || {};
		    var tag = $("<div id='window'></div>"); //This tag will the hold the dialog content.
		    $.ajax({
		      url: url,
		      type: (options.type || 'GET'),
		      beforeSend: options.beforeSend,
		      error: options.error,
		      complete: options.complete,
		      success: function(data, textStatus, jqXHR) {
			$('body').find("#window").remove();
			  tag.html(data).dialog({
				modal: true, 
				title: "Search Template", 
				buttons: {
					Save: function() {

						var tmplname=$('#tmplname').val();
						var pagename="leadcard";
						var isChecked = $('#isdefault').is(':checked');

						var isdefault=0;

						if(isChecked)
							isdefault=1;
						else
							isdefault=0;

					    if(tmplname=="")
					    {
						alert("Please Fill Template Name!");
						return false;

					    }else if(filters=="" || filters==null){
						alert("Search Filter Template Empty! Please do some search filter!");
						return false;
					    }else{
					
						var data = {
					    		type: 'save',
							tmplname:tmplname,
							filters:filters,
							isdefault:isdefault,
							pagename:pagename
						}

						$.ajax({
					    		type: "POST",
					    		url:  "get_search_template.php",
					    		data: data,
					    		success: function(output) {

								alert("Search Template Saved Successfully!");
								
							} 
						});
					}

						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				}
			    }).dialog('open');
			
			$.isFunction(options.success) && (options.success)(data, textStatus, jqXHR);
		      }
		    });

		}


	});

	$("input#viewtmpl","#t_list_records").click(function(){ 

		showUrlInDialog('get_search_template.php?type=getTmplHtml&pagename=leadcard', {error: function() { alert('Could not load form') }}); 

		function showUrlInDialog(url, options){
		    options = options || {};
		    var tag = $("<div></div>"); //This tag will the hold the dialog content.
		    $.ajax({
		      url: url,
		      type: (options.type || 'GET'),
		      beforeSend: options.beforeSend,
		      error: options.error,
		      complete: options.complete,
		      success: function(data, textStatus, jqXHR) {
			
			  tag.html(data).dialog({
				modal: true, 
				width:500,
				title: "View Search Template", 
				buttons: {
					Ok: function() {

						$( this ).dialog( "close" );
					}
				}
			    }).dialog('open');

				
			$(".deltmpl").click(function(){

				var tmplid=$(this).attr('value');

				$(this).parent().parent().remove();

				var data = {
				    		type: 'delete',
						tmplid:tmplid
					}

					$.ajax({
				    		type: "POST",
				    		url:  "get_search_template.php",
				    		data: data,
				    		success: function(output) {
				
							alert("Search Template Deleted Successfully!");
							
						} 
					});
	
			});	

			$(".loadtmpl").click(function(){

				var tmplid=$(this).attr('value');

				var data = {
				    		type: 'loadtmpl',
						tmplid:tmplid
					}

					$.ajax({
				    		type: "POST",
				    		url:  "get_search_template.php",
				    		data: data,
				    		success: function(output) {
					
			var output1=output.trim();
			grid.jqGrid('setGridParam',{ search:true, postData: {filters: output1}}).trigger("reloadGrid");
										
						} 
					});
	
			});	
			
			$.isFunction(options.success) && (options.success)(data, textStatus, jqXHR);
		      }
		    });

		}

	});

	$("input#dfttmpl","#t_list_records").click(function(){ 

		var data = {
		    		type: 'dfttmpl'
			}

			$.ajax({
		    		type: "POST",
		    		url:  "get_search_template.php",
		    		data: data,
		    		success: function(output) {
			
				var output1=output.trim();
		grid.jqGrid('setGridParam',{ search:true, postData: {showtmpl:0, filters: output1}}).trigger("reloadGrid");
								
				} 
			});
	
	});

	$("input#resetgrid","#t_list_records").click(function(){ 
	
		grid.jqGrid('setGridParam',{ search:false, postData: {showtmpl:0 }}).trigger("reloadGrid");

		grid[0].clearToolbar();

	});

	$("#chngroup").change(function(){ 

		var vl = $(this).val(); 
		if(vl) { 
				if(vl == "clear") { 
					grid.jqGrid('groupingRemove',true); 
				} else { 
					grid.jqGrid('groupingGroupBy',vl); 
				} 
		} 
	});

	$("input#removegrouping","#t_list_records").click(function(){ 
	
		$('.rgch').parent().remove();

		var grheader=$('#groups ol');

	        $('<li class="placeholder">Drop headers here</li>').appendTo(grheader);
		
		grid.jqGrid('groupingRemove',true); 

	});


// Grid binding based on window resize

	$(window).bind('resize', function() {

	    // Get width of parent container
	    var width = jQuery("#gridcontainer").attr('clientWidth');

	    if (width == null || width < 1){
		// For IE, revert to offsetWidth if necessary
		width = jQuery("#gridcontainer").attr('offsetWidth');
	    }
	   
	    if (width == null || width < 1){
		width = jQuery("#gridcontainer").width();
	    }
	    width = width - 2;
	    if (width > 0 && Math.abs(width - grid.width()) > 5)
	    {
		grid.setGridWidth(width);
		
	    }else{
		grid.setGridWidth(width);
	   }
		width1 = width + 2;
		$("#gbox_list_records").width(width1);

	}).trigger('resize');

// Drag and Drop grouping 

	var gridId="list_records";

	$('tr.ui-jqgrid-labels th div') .draggable({
	    appendTo: 'body',
	    helper: 'clone'
	});

	$('#groups ol').droppable({
	    activeClass: 'ui-state-default',
	    hoverClass: 'ui-state-hover',
	    accept: ':not(.ui-sortable-helper)',
	    drop: function(event, ui) {
		var $this = $(this);
		$this.find('.placeholder').remove();
		var groupingColumn = $('<li></li>').attr('data-column', ui.draggable.attr('id').replace('jqgh_' + gridId + '_', ''));
		$('<span class="ui-icon ui-icon-close rgch"></span>').click(function() {
		    $(this).parent().remove();
		    $('#' + gridId).jqGrid('groupingRemove');
		    $('#' + gridId).jqGrid('groupingGroupBy', $('#groups ol li:not(.placeholder)').map(function(){ return $(this).attr('data-column'); }).get());
		    if ($('#groups ol li:not(.placeholder)').length === 0) {
		        $('<li class="placeholder">Drop headers here</li>').appendTo($this);
		    }
		}).appendTo(groupingColumn);
		groupingColumn.append(ui.draggable.text());
		groupingColumn.appendTo($this);
		$('#' + gridId).jqGrid('groupingRemove');
		$('#' + gridId).jqGrid('groupingGroupBy', $('#groups ol li:not(.placeholder)').map(function(){ return $(this).attr('data-column'); }).get());
	    }
	}).sortable({
		items: 'li:not(.placeholder)',
		sort: function() {
		    $( this ).removeClass('ui-state-default');
		},
		stop: function() {
		    $('#' + gridId).jqGrid('groupingRemove');
		    $('#' + gridId).jqGrid('groupingGroupBy', $('#groups ol li:not(.placeholder)').map(function(){ return $(this).attr('data-column'); }).get());
		}
	});

// Page Button Actions

$('#emailit').click(function(){ 
	
	var lids = [];
	var leadids = [];

	var grid=$("#list_records");
	var ids = grid.jqGrid('getGridParam','selarrrow');
	if (ids.length>0) {
	    for (var i=0, il=ids.length; i < il; i++) {
		var lid = grid.jqGrid('getCell', ids[i], 'id');
		lids.push(lid);

		var leadid = grid.jqGrid('getCell', ids[i], 'leadid');
		leadids.push(leadid);
	    }
	}
	
	
	if(leadids=="")
	{
		alert("Please select any Lead to send E-Mail");
	}else{
		window.open('../messages/composemail.php?type=leadmail&tbl=3&leadid='+leadids,'_blank');
	}

});

$('#bulkDelete').click(function(){ 

	var lids = [];
	var leadids = [];

	var grid=$("#list_records");
	var ids = grid.jqGrid('getGridParam','selarrrow');
	if (ids.length>0) {

	    for (var i=0, il=ids.length; i < il; i++) {
		var lid = grid.jqGrid('getCell', ids[i], 'id');
		lids.push(lid);

		var leadid = grid.jqGrid('getCell', ids[i], 'leadid');
		leadids.push(leadid);
	    }
	}
	
	if(leadids=="")
	{
		alert("Please select any Lead");
	}else{

	var result=confirm("Deleting this lead will also delete any appointments and information associated with it. Continue deleting this lead?");

		if(result==true)
		{
			var data={
					type:"BulkDelete",
					leadids:leadids,
					lids:lids,
					page:"leadcard"
				}
			$.ajax({
				type:"POST",
				url:"../bulkdelete.php",
				data:data,
				success:function(res){

					alert("Selected Leads Deleted Successfully!");
					$('#list_records').trigger( 'reloadGrid' );

				}
			});
		}
	
		
	} //else

});

$("#mapit").click(function(){
	
	var lids = [];
	var leadids = [];

	var grid=$("#list_records");
	var ids = grid.jqGrid('getGridParam','selarrrow');
	if (ids.length>0) {
	   
	    for (var i=0, il=ids.length; i < il; i++) {
		var lid = grid.jqGrid('getCell', ids[i], 'id');
		lids.push(lid);

		var leadid = grid.jqGrid('getCell', ids[i], 'leadid');
		leadids.push(leadid);
	    }
	}
	//alert("lids : "+lids+" ; Leadids : "+leadids);
	
	if(leadids=="")
	{
		alert("Please select any Leads to view Map");
	}else{
		window.open('../gmap_listing.php?page=leadcard&leadid='+leadids,'_blank');
	}
 });

$('#bulk').click(function(){ 

	var lids = [];
	var leadids = [];

	var grid=$("#list_records");
	var ids = grid.jqGrid('getGridParam','selarrrow');
	if (ids.length>0) {

	    for (var i=0, il=ids.length; i < il; i++) {
		var lid = grid.jqGrid('getCell', ids[i], 'id');
		lids.push(lid);

		var leadid = grid.jqGrid('getCell', ids[i], 'leadid');
		leadids.push(leadid);
	    }
	}
	//alert("lids : "+lids+" ; Leadids : "+leadids);

	if(leadids==''){
		alert("Please Select any Lead ");
		return false;
	}

	var data = {
    			type: 'editlead',
			leadid:leadids,
		}
    	$.ajax({
    		type: "POST",
    		url:  "../multi_edit_leadstatus.php",
    		data: data,
    		success: function(output) {
			var obj =JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:600,
				height:450,
				openjs:function(){
				
					$('a#cancel-event-edit').click(function(){
						window.parent.TINY.box.hide();
					});	

					$("#bsuleadin").datepicker({ autoclose:true });	
					$("#startcont").datepicker({ autoclose:true });	

					$("#bsuleadtype").change(function() {

	var lt=$(this).val();
	if(lt!="")
	{
	    var res=confirm("Changing Lead type will affect survey details if already taken to selected Lead(s)! Are you sure do you want to change?");
	    if(res==true)
	    {
			$.get('../loadsubtype.php?leadtype=' + lt, function(data) {
				$("#bsuleadsubtype").html(data);
			});	
	    }else{
		$("#bsuleadtype").val('Select');
	    }
	}else{
		$("#bsuleadsubtype").html('<option>Select</option>');
	}
					});								

					$('a#save-event-edit').click(function(){

							var leadtype = $('#bsuleadtype').val();
							var leadsubtype = $('#bsuleadsubtype').val();
							var leaddealer = $('#bsuleaddealer').val();
							var status = $('#bsustatus').val();
							var result = $('#bsuresult').val();
							var quali = $('#bsuquali').val();
							var leadindate = $('#bsuleadin').val();
							var startcont = $('#startcont').val();

	if(leadtype=='' && leadsubtype=='' && leaddealer=='' && status=='' && result=='' && quali==''  && leadindate=='' &&startcont==''){
								$('.msg').css({'display':'block'});
								$('.msg').html('Pls Choose atleast one options');
								return false ;
							}

							var data = {
							    	type: 'editSaveEvent',
								leadid:leadids,
								leadtype: leadtype,
								leadsubtype:leadsubtype,
								leaddealer:leaddealer,
								status:status,
								result:result,
								quali:quali,
								leadindate:leadindate,
								startcont:startcont
							}

							$.ajax({
							    		type: "POST",
							    		url: "../multi_edit_leadstatus.php",
							    		data: data,
							    		success: function(resp) {
										window.parent.TINY.box.hide();
										$('#list_records').trigger( 'reloadGrid' );
									}

							});
					});	
								
				} // end of open js				    	

			});

	    } // end of success function
            

        });
});// end of bulk click


}); // end of jquery

function lsFormatter (cellvalue, options, rowObject){
	//alert(" id : "+rowObject[0]+" ; leadid : "+rowObject[1]);
    return '<a title=LeadStatus href=# class=customlink onclick=javascript:loadAjaxContent(' + rowObject[0] + ','+rowObject[1]+')>'+cellvalue+'<a>' + '&nbsp;';
}


function responsive_jqgrid(jqgrid) {
        jqgrid.find('.ui-jqgrid').addClass('clear-margin span12').css('width', '');
        jqgrid.find('.ui-jqgrid-view').addClass('clear-margin span12').css('width', '');
        jqgrid.find('.ui-jqgrid-view > div').eq(1).addClass('clear-margin span12').css('width', '').css('min-height', '0');
        jqgrid.find('.ui-jqgrid-view > div').eq(2).addClass('clear-margin span12').css('width', '').css('min-height', '0');
        jqgrid.find('.ui-jqgrid-sdiv').addClass('clear-margin span12').css('width', '');
        jqgrid.find('.ui-jqgrid-pager').addClass('clear-margin span12').css('width', '');
}

function pickdates(id){ 

//jQuery("#"+id+"_leadin","#list_records").datepicker({dateFormat:"mm/dd/y"});

//jQuery("#"+id+"_leadin","#list_records").data('daterangepicker').setOptions(optionSet1, cb);

//$('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);

}

  function loadAjaxContent(x,y)
    { 

var data = {
    			type: 'editlead',
			id:x,
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "edit_leadstatus.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:800,
							height:550,
							openjs:function(){
							

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});		

							$('a#save-event-edit').click(function(){
								
							var lead_status = $('#status').val();
							var activity = $('#activity').val();
							var contactreason = $('#contactreason').val();
							var comments = $('#comments').val();
					if(activity==''||contactreason==''||lead_status==''){
						$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose All The Options ');
							return false ;
						}
							var data = {
							    	type: 'editSaveEvent',
								id: x,
								leadid:y,
								status: lead_status,
								activity:activity,
								contactreason:contactreason,
								comments:comments,
						
							 }


								$.ajax({
								    		type: "POST",
								    		url: "edit_leadstatus.php",
								    		data: data,
								    		success: function(resp) {
										var output=resp.trim();
										alert(output);
									        window.parent.TINY.box.hide();
										$('#list_records').trigger( 'reloadGrid' );
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
    }

function confirmDelete(id,cus) 
{

if(cus=='1'){
$(document).ready(function(){
bootbox.dialog({
  message: "<input type='radio' value='cus' name='opt'> Customer</input> <input type='radio' value='both' name='opt' >Both Lead and Customer</input>",
  title: "Please Confirm to delete this customer from",
  buttons: {
    danger: {
      label: "Cancel",
      callback: function() {
        //do something
      }
    },
    main: {
      label: "Ok",
      className: "btn-primary",
      callback: function() {
       var sel=$('input[name="opt"]:checked').val();
if(sel=='cus'){
//window.location.href="../add_leads.php?action=deletecustomer&lid="+id+"&redir=3";
window.open('../add_leads.php?action=deletecustomer&lid='+id+'&redir=3','_blank');
      }
if(sel=='both'){
window.open("../add_leads.php?action=delete&lid="+id+"&redir=3",'_blank');
      }
else{
return false;
}
      }
    }
  }
});
});
}
else{
	var agree=confirm("Deleting this lead will also delete any appointments and information associated with it. Continue deleting this lead? ");
}
	if (agree)
		window.open("../add_leads.php?action=deletelead&lid="+id+"&redir=3","_blank");
	else
		return false ;
}
</script>

<style>
#searchmodfbox_list_records{
	width:700px !important;
	min-height:150px;
}

#fbox_list_records{
	width:700px !important;
	min-height:150px;
}

#t_list_records{
	height:35px;
	padding:4px;
}

.btntmpl{
	padding:3px;
}

.clear-margin {
    margin:0 !important;
}
.ui-jqgrid .ui-jqgrid-titlebar
{
	padding: 0.6em 0.3em 0.3em 0.6em;
	height:32px;
}

.ui-jqgrid-btable .ui-jqgrid tr.jqgrow td {vertical-align:top; }

.ui-jqgrid tr.jqgrow td{ 

/*white-space: pre-line; */

 white-space: normal !important;
 height:auto;

}

.col-md-5{
	padding-left: 0px;
	padding-right: 0px;
	text-align:left;
	display:inline-block;
}
.col-md-7{
	/*padding-left: 0px;
	padding-right: 0px;*/
	text-align:left;
	display:inline-block;
}

.col-md-4{
	padding-left: 0px;
	padding-right: 0px;
	text-align:left;
	display:inline-block;
	width:25%
}
.col-md-8{
	/*padding-left: 0px;
	padding-right: 0px;*/
	text-align:left;
	display:inline-block;
	width:74%
}

.clear{
	clear:both;
}

pre{
	background-color: transparent;
	border: none;
	border-radius: none;
	font-family: Verdana;
	font-size:11px;
	font-weight:normal;
	color:#000;
	word-break: keep-all;
}

table{
	border-collapse: none;
}
#groups li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; height: 18px; list-style-type: none; }
#groups li span { position: absolute; margin-left: -1.3em; }

h6{
	margin-bottom: 0px;
	margin-top: 0px;
	padding:4px;
}

.customlink{
	text-decoration:underline;
}

</style>


<div id="spinner"></div>
<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>Lead Listing&nbsp;&nbsp; <a class="btn btn-blue" href="../add_leads.php"><span>Add New Lead</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="../createtask.php"><span>Create Task</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="../create_event.php"><span>Create Event</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="#" id="emailit"><span>Email</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" id="mapit" href="#" ><span>Map It</span></a>	
&nbsp;&nbsp;<button type="button" class="btn btn-blue" onclick="printpage()">Print</button>
&nbsp;&nbsp;<button type="button" id='bulk' class="btn btn-blue" >Bulk Update</button> 
&nbsp;&nbsp;<button type="button" id='bulkDelete' class="btn btn-red" >Bulk Delete</button></a>
	</h3>
</div> 

      </div>
    </div>
  </div>

<div class="container" id="gridcontainer">
<div id="groups">
    <h6 class="ui-widget-header">Grouping</h6>
    <div class="ui-widget-content">
        <ol >
            <li class="placeholder">Drop headers here</li>
        </ol>
    </div>
</div><br>
<table id="list_records" width="100%"></table> 
<div id="pager"></div>

<br>

      </div>
	
    </div>
	
   </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";

?>
