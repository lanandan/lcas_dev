<?php
session_start();

require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");

if(isset($_REQUEST['type']))
{
	$cur_uid=get_session('LOGIN_ID');
	$cur_userid=get_session('LOGIN_USERID');
	$cur_email=get_session('LOGIN_EMAIL');
	$cur_username=get_session('DISPLAY_NAME');

	if($_REQUEST['type']=="get")
	{		
		$tmplNames=array();
		$tmplFilters=array();

		$sql="select * from search_templates where userid='$cur_uid'";

		$res=mysql_query($sql)or die("Error : ".mysql_error());
		$i=0;
		while($r=mysql_fetch_array($res))
		{
			$tmplNames[$i]=$r['tmplname'];
			$tmplFilters[$i]=$r['tmplfilter'];
			$i++;
		}

		$search_templates=array(
			'tmplNames' => $tmplNames,
			'tmplFilters' => $tmplFilters
		);

		echo json_encode($search_templates);
	}
	elseif($_REQUEST['type']=="save")
	{
		
		$tmplname=$_REQUEST['tmplname'];
		$filters=$_REQUEST['filters'];
		$isdefault=$_REQUEST['isdefault'];
		$pagename=$_REQUEST['pagename'];

		$created=date('Y-m-d H:i:s');

		if(1==$isdefault)
		{
			$sql1="select * from search_templates where userid='$cur_uid' and default_tmpl='1'";

			$res=mysql_query($sql1)or die("Error : ".mysql_error());

			$n=mysql_num_rows($res);
			if(0!=$n)
			{
				$sql2="update search_templates set default_tmpl='0' where userid='$cur_uid' and default_tmpl='1'";

				$res2=mysql_query($sql2)or die("Error : ".mysql_error());
			}
		}

		$sql="INSERT INTO `search_templates` (`id`, `userid`, `page`, `tmplname`, `tmplfilter`, `default_tmpl`, `createdby`, `created`, `modifiedby`, `modified`) VALUES (NULL, '$cur_uid', '$pagename', '$tmplname', '$filters', '$isdefault', '$cur_username', '$created', '$cur_username', '$created')";

		mysql_query($sql)or die("Error : ".mysql_error());


	}elseif($_REQUEST['type']=="delete")
	{
		
		$tmplid=$_REQUEST['tmplid'];

		$sql="DELETE FROM `search_templates` WHERE `id` = $tmplid";

		mysql_query($sql)or die("Error : ".mysql_error());


	}elseif($_REQUEST['type']=="getHtml")
	{
		$filters=$_REQUEST['filters'];

		$sql="select * from search_templates where userid='$cur_uid'";
		$res=mysql_query($sql)or die("Error : ".mysql_error());
		$i=1;
		$prevTable="<table class='table table-bordered'><tr><td><b>S.No</b></td><td><b>Template Name</b></td><td><b>Action</b></td></tr>";
		while($r=mysql_fetch_array($res))
		{
			$tmplName=$r['tmplname'];
			$prevTable.="<tr>
					<td>".$i."</td>
					<td><a href='#' class='loadtmpl' style='text-decoration:underline;' value='".$r['id']."'>".$tmplName."</a></td>
					<td><a href='#' class='deltmpl' value='".$r['id']."'>Delete</a></td>
	 			     </tr>";
			$i++;
		}
		$prevTable.="<table>";

		$popupHtml= '<div style="height: 400px; overflow-y: auto"><div class="box">
      			<div class="box-header"><span class="title">Save Search Template</span></div>	
			<div class="box-content padded ">
				<p>Do you want to save the search filter? 
				<input type="radio" name="savetemp" value="1">Yes <input type="radio" name="savetemp" value="0">No </p>
				<table id="showTable" cellpadding="8">
				<tr><td>Template Name</td><td><input type="text" name="tmplname" id="tmplname" /> </td></tr>
				<tr><td></td>
				    <td>
<input type="button" name="savetmpl" id="savetmpl" value="Save" class="fm-button ui-state-default ui-corner-all fm-button-icon-right ui-reset" />
<input type="button" name="cancel" id="cancel" value="Cancel" class="fm-button ui-state-default ui-corner-all fm-button-icon-right ui-reset" />
				    </td>
				</tr>
				</table>
				<br><div class="scrollable" style="height: 200px; overflow-y: auto">'.$prevTable.'</div><br>		
			</div>
			</div></div></div>';

		$event_edit = array('popupHtml' => $popupHtml);

		echo json_encode($event_edit);
	}


	if($_REQUEST['type']=="getHtmlNew")
	{
		$html="Template Name <input type='text' name='tmplname' id='tmplname' /><br><br>
			<input type='checkbox' name='isdefault' id='isdefault' /> Mark as Default Search Template<br>";

		echo $html;
	}

	if($_REQUEST['type']=="getTmplHtml")
	{

		$pagename=$_REQUEST['pagename'];

		$sql="select * from search_templates where userid='$cur_uid' and page='$pagename' order by id desc";
		$res=mysql_query($sql)or die("Error : ".mysql_error());
		$i=1;
		$prevTable="<div style='height: 200px; overflow-y: auto'>
		<table class='table table-bordered'><tr><td width='80%'><b>Search Template Name</b></td><td align='center'><b>Action</b></td></tr>";
		while($r=mysql_fetch_array($res))
		{
			$tmplName=$r['tmplname'];
			$isdefault=$r['default_tmpl'];
		 
			$prevTable.="<tr><td>
	<a href='#' class='loadtmpl' title='Load Template' style='text-decoration:underline;' value='".$r['id']."'>";
	if(1==$isdefault)
	{
		$prevTable.="<span class='ui-icon ui-icon-circle-check' style='float:right;' title='Default Search Template'></span>$tmplName</a>";
	}else{
		$prevTable.="$tmplName</a>";
	}

	$prevTable.="</td>
		     <td align='center'>
			<a href='#' class='deltmpl' title='Delete' value='".$r['id']."'><span class='ui-icon ui-icon-closethick'></span></a>
		     </td>
	 	  </tr>";

			$i++;
		}
		$prevTable.="<table></div>";

		echo $prevTable;
	}

	if($_REQUEST['type']=="loadtmpl")
	{		

		$tmplid=$_REQUEST['tmplid'];

		$sql="select * from search_templates where userid='$cur_uid' and id='$tmplid'";

		$res=mysql_query($sql)or die("Error : ".mysql_error());

		$r=mysql_fetch_array($res);

		echo $r['tmplfilter'];
	}

	if($_REQUEST['type']=="dfttmpl")
	{		

		$sql="select * from search_templates where userid='$cur_uid' and default_tmpl='1'";

		$res=mysql_query($sql)or die("Error : ".mysql_error());

		$r=mysql_fetch_array($res);

		echo $r['tmplfilter'];
	}
	

}

?>
