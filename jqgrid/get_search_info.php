<?php
session_start();

require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");


if($_REQUEST['type']=="lt")
{

	$sql="select * from tps_lead_type where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	$lt=array();

	$i=0;
	while($row = mysql_fetch_array($result)){

		$lt[$i]['label']=$row['type_displayname'];
		$lt[$i]['value']=$row['lead_type'];
		
		$i++;
	}

	echo json_encode($lt);
}


if($_REQUEST['type']=="lst")
{

	$sql="select * from tps_lead_subtype where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	$lt=array();

	$i=0;
	while($row = mysql_fetch_array($result)){

		$lt[$i]['label']=$row['subtype_displayname'];
		$lt[$i]['value']=$row['lead_subtype'];
		
		$i++;
	}

	echo json_encode($lt);
}

if($_REQUEST['type']=="ltlst")
{

	$lt=$_REQUEST['lt'];

	$sql="select * from tps_lead_subtype where parent='$lt' and status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='<option value="">All</option>';
	
	$lt=array();

	$i=0;
	while($row = mysql_fetch_array($result)){

		//$lt[$i]['label']=$row['subtype_displayname'];
		//$lt[$i]['value']=$row['lead_subtype'];
		
		$string.="<option value='".$row['lead_subtype']."'>".$row['subtype_displayname']."</option>";
		$i++;
	}

	//echo json_encode($lt);
	echo $string;

}


if($_REQUEST['type']=="qual")
{

	$sql="select * from tps_lead_qualified where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	$lt=array();

	$i=0;
	while($row = mysql_fetch_array($result)){

		$lt[$i]['label']=$row['display'];
		$lt[$i]['value']=$row['display'];
		
		$i++;
	}

	echo json_encode($lt);
}


if($_REQUEST['type']=="ls")
{

	$sql="select * from tps_lead_status where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	$lt=array();

	$i=0;
	while($row = mysql_fetch_array($result)){

		$lt[$i]['label']=$row['display'];
		$lt[$i]['value']=$row['name'];
		
		$i++;
	}

	echo json_encode($lt);
}

if($_REQUEST['type']=="lr")
{

	$sql="select * from tps_lead_result where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	$lt=array();

	$i=0;
	while($row = mysql_fetch_array($result)){

		$lt[$i]['label']=$row['display'];
		$lt[$i]['value']=$row['name'];
		
		$i++;
	}

	echo json_encode($lt);
}

if($_REQUEST['type']=="ldr")
{
	$re=mysql_query("select fname,lname,userid,nickname from tps_users where status='1'");

	$displayname="";

	$lt=array();

	$i=0;
	while($r=mysql_fetch_array($re))
	{       
		if($r['nickname']!="")
			$displayname=ucfirst($r['nickname'])." ".$r['lname'];
		else
			$displayname=ucfirst($r['fname'])." ".$r['lname'];

		$lt[$i]['label']=$displayname;
		$lt[$i]['value']=$r['userid'];
		
		$i++;

	}

	echo json_encode($lt);
}


if($_REQUEST['type']=="refby")
{
	$userid=get_session('LOGIN_USERID');
	$ID=get_session('LOGIN_ID');

	$today=date('Y-m-d H:i:s');

	$child=array($ID);

	$re1=mysql_query("select id from tps_users where parentid='$userid'");

	while($r1=mysql_fetch_array($re1)){
		array_push($child,$r1['id']);
	}

	$child=implode(',',$child);

$sql="select DISTINCT referred_by from tps_lead_card where FIND_IN_SET(uid,'$child')  and delete_flag='0' and referred_by!='' order by id desc";
	$re=mysql_query($sql);

	$lt=array();

	$i=0;
	while($row=mysql_fetch_array($re))
	{       
		$refby="";

		if($row['referred_by']!='')
		{
			$ref=$row['referred_by'];
			$im=explode('_',$ref);
			if($im[0]==1){
				$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]'")or die(mysql_error());
				$r=mysql_fetch_array($sq);
				$refby=ucfirst($r['lname1'])." ".$r['fname1']." ".$r['lname2']." ".$r['fname2'];
			}
			if($im[0]==2){
				$sq=mysql_query("select campaign from tps_campaign where id='$im[1]'")or die(mysql_error());
				$r=mysql_fetch_array($sq);
				$refby=$r['campaign'];
			}
			if($im[0]==3){
				$sq=mysql_query("select fname1,lname1 from tps_lead_card where id='$im[1]'")or die(mysql_error());
				$r=mysql_fetch_array($sq);
				$refby=$r['fname1'].' '.$r['lname1'];
			}
		}

		$lt[$i]['label']=trim($refby);
		$lt[$i]['value']=trim($row['referred_by']);
		
		$i++;

	}

	echo json_encode($lt);
}

?>
