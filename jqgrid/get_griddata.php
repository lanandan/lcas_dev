<?php
session_start();

require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");


$cur_uid=get_session('LOGIN_ID');
$cur_userid=get_session('LOGIN_USERID');
$cur_email=get_session('LOGIN_EMAIL');
$cur_username=get_session('DISPLAY_NAME');

$tablename="live_tps_events";

if($_REQUEST['type']=="getcolumns")
{
	$sql="SHOW COLUMNS FROM $tablename";
	$result=mysql_query($sql) or die(mysql_error());
	
	$cn=array();
	$cm=array();

	$i=0;
	while($row = mysql_fetch_array($result)){

		$cn[$i]=$row['Field'];

		$cm[$i]['index']=$row['Field'];
		$cm[$i]['name']=$row['Field'];
		$cm[$i]['width']="200";
		$cm[$i]['sortable']="false";
		
		if($row['Type']=="int(11)" || $row['Type']=="tinyint(4)")
		{
			//$cm[$i]['hidden']="true";
		}

		$searchoptions=array();

		if($row['Type']=="date" || $row['Type']=="timestamp" || $row['Type']=="datetime")
		{
			$cm[$i]['search']="true";
			$cm[$i]['stype']="text";
			//$cm[$i]['searchoptions']="";
				
			//$searchoptions[$i]['sopt']="";	
					/*sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'], 
					sorttype: 'datetime',
					dataInit: function(elem){ 

						 $(elem).daterangepicker(
						 { 
							ranges: {
								 'Today': [moment(), moment()],
								 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
								 'Last 7 Days': [moment().subtract('days', 6), moment()],
								 'Next 7 Days': [moment(), moment().add('days', 6)],
 								 'Last 14 Days': [moment().subtract('days', 13), moment()],
								 'Next 14 Days': [moment(), moment().add('days', 13)],
								 'Last 30 Days': [moment().subtract('days', 29), moment()],
								 'This Month': [moment().startOf('month'), moment().endOf('month')],
					 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
							      },
							startDate: moment().subtract('days', 29),
							endDate: moment()
						 },
						 function(start, end) {
							$(elem).val(start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY'));
							var test12=$(elem).val();
							 if (elem.id.substr(0, 3) === 'gs_') {
								// call triggerToolbar only in case of searching toolbar
								setTimeout(function () {
								    grid[0].triggerToolbar();
								}, 100);
						    	}
						 }
					       );	
					}";*/

		}

		$i++;
	}

	$result = array(
	    'colNames' => $cn,
	    'colModel' => $cm,
	);

	echo json_encode($result);

}else{

	$cur_uid=get_session('LOGIN_ID');

	$page = $_GET['page']; 
	$limit = $_GET['rows']; 
	$sidx = $_GET['sidx']; 
	$sord = $_GET['sord']; 

	if(!$sidx) $sidx =1; 

	// Search Options

	$wh = "";
	$searchOn = Strip($_REQUEST['_search']);
	if($searchOn=='true') {

		$searchstr = Strip($_REQUEST['filters']);

		$wh= constructWhere($searchstr);

		//echo $wh;
	}else{

		$showtmpl=$_REQUEST['showtmpl'];

		if($showtmpl!=0)
		{
		
			$sql="select * from search_templates where userid='$cur_uid' and default_tmpl='1'";

			$res=mysql_query($sql)or die("Error : ".mysql_error());

			$r=mysql_fetch_array($res);

			$searchstr = Strip($r['tmplfilter']);

			$wh= constructWhere($searchstr);
		
		}else{
			$wh=" where 1 ";
		}

	}

	$result = mysql_query("SELECT COUNT(*) AS count FROM $tablename ".$wh." "); 

	$row = mysql_fetch_array($result,MYSQL_ASSOC); 

	$count = $row['count']; 
	if( $count > 0 && $limit > 0) { 
	    $total_pages = ceil($count/$limit); 
	} else { 
	    $total_pages = 0; 
	} 

	if ($page > $total_pages) $page=$total_pages;

	$start = $limit*$page - $limit;

	if($start <0) $start = 0; 

	$SQL="SELECT * FROM `$tablename` ".$wh." ORDER BY $sidx $sord LIMIT $start , $limit";

	//echo $SQL;

	$result = mysql_query( $SQL ) or die("Couldn't execute query.".mysql_error()); 

	$responce = new StdClass;

	$responce->page = $page; 
	$responce->total = $total_pages; 
	$responce->records = $count;

	$i=0;
	
	$sql1="SHOW COLUMNS FROM $tablename";
	$result1=mysql_query($sql1) or die(mysql_error());

	$ci=0;
	$cols=array();

	while($r = mysql_fetch_array($result1)){
		$cols[$ci]=$r['Field'];
		$ci++;
	}
	
	//print_r($cols);

	$cols_count=count($cols);

	//echo $cols_count;

	$res_arr=array();

	while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {


		for($j=0;$j<$cols_count;$j++)
		{
			$fn=$cols[$j];
			$res_arr[$j]=$row[$fn];
		}

		$responce->rows[$i]['cell']=$res_arr;

		$i++;

	}

	//print_r($res_arr);

	echo json_encode($responce);

} // end of main if else part


function constructWhere($s){

    $qwery = " where 1 ";
	//['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
    $qopers = array(
				  'eq'=>" = ",
				  'ne'=>" <> ",
				  'lt'=>" < ",
				  'le'=>" <= ",
				  'gt'=>" > ",
				  'ge'=>" >= ",
				  'bw'=>" LIKE ",
				  'bn'=>" NOT LIKE ",
				  'in'=>" IN ",
				  'ni'=>" NOT IN ",
				  'ew'=>" LIKE ",
				  'en'=>" NOT LIKE ",
				  'cn'=>" LIKE " ,
				  'nc'=>" NOT LIKE " );
    if ($s) {
	$jsona = json_decode($s,true);
	if(is_array($jsona)){
			$gopr = $jsona['groupOp'];
			$rules = $jsona['rules'];
	    $i =0;
	    foreach($rules as $key=>$val) {
	        $field = $val['field'];
	        $op = $val['op'];
	        $v = $val['data'];

				if($v && $op) {

		       		 $i++;
			
			
				switch ($field) {
					case 'id':
						return intval($val);
						break;
					case 'lead_in':
					{
						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
					
						$stt="";
						$edt="";
						$dt=explode("-",$v);
										
						$st1=fmt_db_date_time(strtotime($dt[0]));
						$ed1=fmt_db_date_time(strtotime($dt[1]));

						$st=explode(" ",$st1);
						$ed=explode(" ",$ed1);

						$stt=$st[0];
						$edt=$ed[0];

						$qwery.="DATE($field) BETWEEN '$stt' AND '$edt'";

						break;
					}
					case 'oktocall':
					{
						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
					
						$stt="";
						$edt="";
						$dt=explode("-",$v);
										
						$st1=fmt_db_date_time(strtotime($dt[0]));
						$ed1=fmt_db_date_time(strtotime($dt[1]));

						$st=explode(" ",$st1);
						$ed=explode(" ",$ed1);

						$stt=$st[0];
						$edt=$ed[0];

						$qwery.="DATE($field) BETWEEN '$stt' AND '$edt'";

						break;
					}
					case 'setdate':
					{
						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
					
						$stt="";
						$edt="";
						$dt=explode("-",$v);
										
						$st1=fmt_db_date_time(strtotime($dt[0]));
						$ed1=fmt_db_date_time(strtotime($dt[1]));

						$st=explode(" ",$st1);
						$ed=explode(" ",$ed1);

						$stt=$st[0];
						$edt=$ed[0];

						$qwery.="DATE($field) BETWEEN '$stt' AND '$edt'";

						break;
					}
					case 'last_modifiedon':
					{
						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
					
						$stt="";
						$edt="";
						$dt=explode("-",$v);
										
						$st1=fmt_db_date_time(strtotime($dt[0]));
						$ed1=fmt_db_date_time(strtotime($dt[1]));

						$st=explode(" ",$st1);
						$ed=explode(" ",$ed1);

						$stt=$st[0];
						$edt=$ed[0];

						$qwery.="DATE($field) BETWEEN '$stt' AND '$edt'";

						break;
					}
					default :
					{
						// ToSql in this case is absolutley needed
						$v = ToSql($field,$op,$v);

						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
						switch ($op) {
							// in need other thing
						    case 'in' :
						    case 'ni' :
							$qwery .= $field.$qopers[$op]." (".$v.")";
							break;
							default:
							$qwery .= $field.$qopers[$op].$v;
						} // end switch

						break;
					}
				} //end switch
				

				} //end if
	    } //end foreach
	}// end json array
    } //end if(s)

    return $qwery;
}

function ToSql ($field, $oper, $val) {
	// we need here more advanced checking using the type of the field - i.e. integer, string, float
	switch ($field) {
		case 'id':
			return intval($val);
			break;
		case 'total':
			return floatval($val);
			break;
		default :
			//mysql_real_escape_string is better
			if($oper=='bw' || $oper=='bn') return "'" . addslashes($val) . "%'";
			else if ($oper=='ew' || $oper=='en') return "'%" . addcslashes($val) . "'";
			else if ($oper=='cn' || $oper=='nc') return "'%" . addslashes($val) . "%'";
			else return "'" . addslashes($val) . "'";
	}
}

function Strip($value)
{
	if(get_magic_quotes_gpc() != 0)
  	{
    	if(is_array($value))  
			if ( array_is_associative($value) )
			{
				foreach( $value as $k=>$v)
					$tmp_val[$k] = stripslashes($v);
				$value = $tmp_val; 
			}				
			else  
				for($j = 0; $j < sizeof($value); $j++)
        			$value[$j] = stripslashes($value[$j]);
		else
			$value = stripslashes($value);
	}
	return $value;
}

function array_is_associative ($array)
{
    if ( is_array($array) && ! empty($array) )
    {
        for ( $iterator = count($array) - 1; $iterator; $iterator-- )
        {
            if ( ! array_key_exists($iterator, $array) ) { return true; }
        }
        return ! array_key_exists(0, $array);
    }
    return false;
}

/*
echo "<pre>";

$myarr=array();

for($i=1;$i<=45;$i++)
{
	if($i%3==0)
		$myarr[$i]=$i;
}
echo implode(',',$myarr);

echo "<hr>";

for($i=3;$i<=45;$i+=3)
{
	echo $i;
	($i<45)? print ",":print "";
}

echo "</pre>";
*/

?>
