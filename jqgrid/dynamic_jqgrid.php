<?php
session_start();

require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");

validate_login();

$page_name = "leadcard_jqgrid.php";
$page_title = $site_name." -  Lead Listing";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<link rel='stylesheet' type='text/css' href='assets/css/jquery-ui-1.10.4.custom.css' />
<link rel='stylesheet' type='text/css' href='assets/css/ui.jqgrid.css' />
<link rel="stylesheet" type="text/css" href="assets/css/ui.multiselect.css" />
<link rel="stylesheet" type="text/css" media="all" href="assets/css/daterangepicker-bs3.css" />
<link rel='stylesheet' type='text/css' href='assets/css/custom.css' />
<!--<script src="assets/js/jquery-1.10.1.min.js"></script>
<script type='text/javascript' src='assets/js/jquery-ui-custom.min.js'></script>-->

<script src="assets/js/jquery-1.7.1.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/js/ui.multiselect.js"></script>     
<script type='text/javascript' src='assets/js/grid.locale-en.js'></script>
<script type='text/javascript' src='assets/js/jquery.jqGrid.js'></script>

<script type="text/javascript" src="assets/js/moment.js"></script>
<script type="text/javascript" src="assets/js/daterangepicker.js"></script>

<script type="text/javascript" src="../javascripts/bootbox.min.js"></script>

<script type="text/javascript">

$(document).ready(function () {

var data={
	type:"getcolumns"
}
$.ajax(
    {
       type: "POST",
       url: "get_griddata.php",
       data: data,
       dataType: "json",
       success: function(result)
       {

	    //console.log(result);

            //colD = result.colData;
            colN = result.colNames;
            colM = result.colModel;

	    
	var mySearchOptions = {

			multipleSearch: true,
			multipleGroup: true,
			recreateFilter: true,
			closeOnEscape: true,
			closeAfterSearch: true,
			showQuery: true,
			overlay: 0,
			afterRedraw: function () {
			    $('input.add-rule',this).val('Add new rule').button();
			    $('input.add-group',this).val('Add new group or rules').button();
			    $('input.delete-rule',this).val('Delete rule').button();
			    $('input.delete-group',this).val('Delete group').button();
			},
			onClose:function()
		        {
		       
		          return true; 
		        }
	       };

            var grid= $("#list_records").jqGrid({
                jsonReader : {
			root: "rows", 
			page: "page", 
			total: "total", 
			records: "records", 
			repeatitems: true, 
			cell: "cell", 
			id: "id",
			userdata: "userdata", 
                },
                url: 'get_griddata.php?type=getvalues&showtmpl=0',
                datatype: 'json',
                mtype: 'GET',
                //datastr : colD,
                colNames:colN,
                colModel:colM,
                pager: jQuery('#pager'),
                rowNum: 5,
                rowList: [5, 10, 20, 50, 100],
		sortname: "id",
		sortorder: "desc",
                viewrecords: true,
		autowidth:true,
		height:400,
		caption:"Dynamic Grid"
            });
	    grid.jqGrid('navGrid','#pager', {edit:false,add:false,del:false,pdf:true }, {}, {}, {}, mySearchOptions );

	    grid.jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false, defaultSearch: 'cn'});

	    grid.jqGrid('navButtonAdd',"#pager",{caption:"",title:"Show/Hide Search Filters", buttonicon :'ui-icon-signal-diag', onClickButton:function(){ grid[0].toggleToolbar() } }); 

	    grid.jqGrid('navButtonAdd',"#pager",{caption:"",title:"Clear Search Filters", buttonicon :'ui-icon-arrow-4-diag', onClickButton:function(){ grid[0].clearToolbar() } });

	    grid.jqGrid('navButtonAdd','#pager',{ caption: "", title: "Show/Hide Columns", 
			onClickButton : function (){ 
				grid.jqGrid("columnChooser", {
					done: function(perm) {
					  if (perm) {
					  	grid.jqGrid("remapColumns", perm, true);
					  }
					  grid.trigger("resize");
					}
				      });
	
			} //end of btn click
	    });

 // Grid binding based on window resize

	$(window).bind('resize', function() {

	    var grid=$("#list_records");

	    // Get width of parent container
	    var width = jQuery("#gridcontainer").attr('clientWidth');

	    if (width == null || width < 1){
		// For IE, revert to offsetWidth if necessary
		width = jQuery("#gridcontainer").attr('offsetWidth');
	    }
	   
	    if (width == null || width < 1){
		width = jQuery("#gridcontainer").width();
	    }
	    width = width - 2;
	    if (width > 0 && Math.abs(width - grid.width()) > 5)
	    {
		grid.setGridWidth(width);
		
	    }else{
		grid.setGridWidth(width);
	   }
		width1 = width + 2;
		$("#gbox_list_records").width(width1);

	}).trigger('resize');

// Drag and Drop grouping 

	var gridId="list_records";

	$('tr.ui-jqgrid-labels th div') .draggable({
	    appendTo: 'body',
	    helper: 'clone'
	});

	$('#groups ol').droppable({
	    activeClass: 'ui-state-default',
	    hoverClass: 'ui-state-hover',
	    accept: ':not(.ui-sortable-helper)',
	    drop: function(event, ui) {
		var $this = $(this);
		$this.find('.placeholder').remove();
		var groupingColumn = $('<li></li>').attr('data-column', ui.draggable.attr('id').replace('jqgh_' + gridId + '_', ''));
		$('<span class="ui-icon ui-icon-close rgch"></span>').click(function() {
		    $(this).parent().remove();
		    $('#' + gridId).jqGrid('groupingRemove');
		    $('#' + gridId).jqGrid('groupingGroupBy', $('#groups ol li:not(.placeholder)').map(function(){ return $(this).attr('data-column'); }).get());
		    if ($('#groups ol li:not(.placeholder)').length === 0) {
		        $('<li class="placeholder">Drop headers here</li>').appendTo($this);
		    }
		}).appendTo(groupingColumn);
		groupingColumn.append(ui.draggable.text());
		groupingColumn.appendTo($this);
		$('#' + gridId).jqGrid('groupingRemove');
		$('#' + gridId).jqGrid('groupingGroupBy', $('#groups ol li:not(.placeholder)').map(function(){ return $(this).attr('data-column'); }).get());
	    }
	}).sortable({
		items: 'li:not(.placeholder)',
		sort: function() {
		    $( this ).removeClass('ui-state-default');
		},
		stop: function() {
		    $('#' + gridId).jqGrid('groupingRemove');
		    $('#' + gridId).jqGrid('groupingGroupBy', $('#groups ol li:not(.placeholder)').map(function(){ return $(this).attr('data-column'); }).get());
		}
	});



       },
       error: function(x, e)
       {
            alert(x.readyState + " "+ x.status +" "+ e.msg);   
       }
    });

setTimeout(function() {$("#list_records").jqGrid('setGridParam',{datatype:'json'}); },50);


	  

}); // end of document ready

</script>

<div id="spinner"></div>
<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>Lead Listing&nbsp;&nbsp; <a class="btn btn-blue" href="../add_leads.php"><span>Add New Lead</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="../createtask.php"><span>Create Task</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="../create_event.php"><span>Create Event</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="#" id="emailit"><span>Email</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" id="mapit" href="#" ><span>Map It</span></a>	
&nbsp;&nbsp;<button type="button" class="btn btn-blue" onclick="printpage()">Print</button>
&nbsp;&nbsp;<button type="button" id='bulk' class="btn btn-blue" >Bulk Update</button> 
&nbsp;&nbsp;<button type="button" id='bulkDelete' class="btn btn-red" >Bulk Delete</button></a>
	</h3>
</div> 

      </div>
    </div>
  </div>

<div class="container" id="gridcontainer">
<div id="groups">
    <h6 class="ui-widget-header">Grouping</h6>
    <div class="ui-widget-content">
        <ol >
            <li class="placeholder">Drop headers here</li>
        </ol>
    </div>
</div><br>
<table id="list_records" width="100%"></table> 
<div id="pager"></div>

<br>

      </div>
	
    </div>
	
   </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";

?>
