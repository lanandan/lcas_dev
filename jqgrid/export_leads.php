<?php
session_start();

$export_type=$_REQUEST['ept'];

$dt=date('d-m-Y');

if($export_type=="excel")
{
	header("Content-type: application/octet-stream");
	header("Content-type: application/x-ms-download");
	header('Content-type: application/vnd.ms-excel');	
	header("Content-Disposition: attachment; filename=Rpt-Leads-".$dt.".xls");
	header("Pragma: ");
	header("Cache-Control: ");
}
elseif($export_type=="pdf")
{
	
}
elseif($export_type=="word")
{
	header("Content-type: application/octet-stream");
	header("Content-type: application/x-ms-download");
	header('Content-type: application/msword doc');
	header("Content-Disposition: attachment; filename=Rpt-Leads-".$dt.".doc");
	header("Pragma: ");
	header("Cache-Control: ");
}

require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
?>
<style>
.leadstable {
    border-collapse: collapse;
    font-family:arial;
    width: 100%;
}
.leadstable td, .leadstable th {
    border: 1px solid #ccc;
    font-size: 13px;
    padding: 3px 7px 2px;
}
.leadstable th {
    background-color: #ddd;
    color: #000;
    font-size: 13px;
    padding-bottom: 4px;
    padding-top: 5px;
    text-align: left;
}
.leadstable tr.alt td {
    background-color: #f3f4f8;
    color: #000;
}
</style>
<?php
$cur_uid=get_session('LOGIN_ID');


$userid=get_session('LOGIN_USERID');
$ID=get_session('LOGIN_ID');
$today=date('Y-m-d H:i:s');
$child=array($ID);
$re=mysql_query("select id from tps_users where parentid='$userid'");
while($r=mysql_fetch_array($re)){
	array_push($child,$r['id']);
}
$child=implode(',',$child);

$page = $_GET['page']; 
$limit = $_GET['rows']; 
$sidx = $_GET['sidx']; 
$sord = $_GET['sord']; 

if(!$sidx) $sidx =1; 

// Search Options

$wh = "";
$searchOn = Strip($_REQUEST['_search']);
if($searchOn=='true') {

	$searchstr = Strip($_REQUEST['filters']);

	$wh= constructWhere($searchstr);

	//echo $wh;
}else{

	$showtmpl=$_REQUEST['showtmpl'];

	if($showtmpl!=0)
	{
		
		$sql="select * from search_templates where userid='$cur_uid' and default_tmpl='1'";

		$res=mysql_query($sql)or die("Error : ".mysql_error());

		$r=mysql_fetch_array($res);

		$searchstr = Strip($r['tmplfilter']);

		$wh= constructWhere($searchstr);
		
	}else{
		$wh=" where FIND_IN_SET(uid,'$child') ";
	}

}

function constructWhere($s){

    $cur_uid=get_session('LOGIN_ID');


	$userid=get_session('LOGIN_USERID');
	$ID=get_session('LOGIN_ID');
	$today=date('Y-m-d H:i:s');
	$child=array($ID);
	$re=mysql_query("select id from tps_users where parentid='$userid'");
	while($r=mysql_fetch_array($re)){
		array_push($child,$r['id']);
	}
	$child=implode(',',$child);

    $qwery = " where FIND_IN_SET(uid,'$child') ";
	//['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
    $qopers = array(
				  'eq'=>" = ",
				  'ne'=>" <> ",
				  'lt'=>" < ",
				  'le'=>" <= ",
				  'gt'=>" > ",
				  'ge'=>" >= ",
				  'bw'=>" LIKE ",
				  'bn'=>" NOT LIKE ",
				  'in'=>" IN ",
				  'ni'=>" NOT IN ",
				  'ew'=>" LIKE ",
				  'en'=>" NOT LIKE ",
				  'cn'=>" LIKE " ,
				  'nc'=>" NOT LIKE " );
    if ($s) {
        $jsona = json_decode($s,true);
        if(is_array($jsona)){
			$gopr = $jsona['groupOp'];
			$rules = $jsona['rules'];
            $i =0;
            foreach($rules as $key=>$val) {
                $field = $val['field'];
                $op = $val['op'];
                $v = $val['data'];

				if($v && $op) {

	               		 $i++;
				
				
				switch ($field) {
					case 'id':
						return intval($val);
						break;
					case 'lead_in':
					{
						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
						
						$stt="";
						$edt="";
						$dt=explode("-",$v);
											
						$st1=fmt_db_date_time(strtotime($dt[0]));
						$ed1=fmt_db_date_time(strtotime($dt[1]));

						$st=explode(" ",$st1);
						$ed=explode(" ",$ed1);

						$stt=$st[0];
						$edt=$ed[0];

						$qwery.="DATE($field) BETWEEN '$stt' AND '$edt'";

						break;
					}
					case 'oktocall':
					{
						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
						
						$stt="";
						$edt="";
						$dt=explode("-",$v);
											
						$st1=fmt_db_date_time(strtotime($dt[0]));
						$ed1=fmt_db_date_time(strtotime($dt[1]));

						$st=explode(" ",$st1);
						$ed=explode(" ",$ed1);

						$stt=$st[0];
						$edt=$ed[0];

						$qwery.="DATE($field) BETWEEN '$stt' AND '$edt'";

						break;
					}
					case 'setdate':
					{
						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
						
						$stt="";
						$edt="";
						$dt=explode("-",$v);
											
						$st1=fmt_db_date_time(strtotime($dt[0]));
						$ed1=fmt_db_date_time(strtotime($dt[1]));

						$st=explode(" ",$st1);
						$ed=explode(" ",$ed1);

						$stt=$st[0];
						$edt=$ed[0];

						$qwery.="DATE($field) BETWEEN '$stt' AND '$edt'";

						break;
					}
					case 'last_modifiedon':
					{
						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
						
						$stt="";
						$edt="";
						$dt=explode("-",$v);
											
						$st1=fmt_db_date_time(strtotime($dt[0]));
						$ed1=fmt_db_date_time(strtotime($dt[1]));

						$st=explode(" ",$st1);
						$ed=explode(" ",$ed1);

						$stt=$st[0];
						$edt=$ed[0];

						$qwery.="DATE($field) BETWEEN '$stt' AND '$edt'";

						break;
					}
					default :
					{
						// ToSql in this case is absolutley needed
						$v = ToSql($field,$op,$v);

						if ($i == 1) $qwery .= " AND ";
						else $qwery .= " " .$gopr." ";
						switch ($op) {
							// in need other thing
						    case 'in' :
						    case 'ni' :
							$qwery .= $field.$qopers[$op]." (".$v.")";
							break;
							default:
							$qwery .= $field.$qopers[$op].$v;
						} // end switch

						break;
					}
				} //end switch
					

				} //end if
            } //end foreach
        }// end json array
    } //end if(s)

    return $qwery;
}
function ToSql ($field, $oper, $val) {
	// we need here more advanced checking using the type of the field - i.e. integer, string, float
	switch ($field) {
		case 'id':
			return intval($val);
			break;
		case 'total':
			return floatval($val);
			break;
		default :
			//mysql_real_escape_string is better
			if($oper=='bw' || $oper=='bn') return "'" . addslashes($val) . "%'";
			else if ($oper=='ew' || $oper=='en') return "'%" . addcslashes($val) . "'";
			else if ($oper=='cn' || $oper=='nc') return "'%" . addslashes($val) . "%'";
			else return "'" . addslashes($val) . "'";
	}
}


$result = mysql_query("SELECT COUNT(*) AS count FROM live_leads ".$wh." "); 

$row = mysql_fetch_array($result,MYSQL_ASSOC); 

$count = $row['count']; 
if( $count > 0 && $limit > 0) { 
    $total_pages = ceil($count/$limit); 
} else { 
    $total_pages = 0; 
} 

if ($page > $total_pages) $page=$total_pages;

$start = $limit*$page - $limit;

if($start <0) $start = 0; 

$SQL="SELECT * FROM `live_leads` ".$wh." ORDER BY $sidx $sord LIMIT $start , $limit";

//echo $SQL;

$result = mysql_query( $SQL ) or die("Couldn't execute query.".mysql_error()); 

/*$responce = new StdClass;

$responce->page = $page; 
$responce->total = $total_pages; 
$responce->records = $count;
*/

$i=1;
$lead_address="";

$columns=stripslashes(trim($_REQUEST['columns']));

$arr=array();
$arr=json_decode($columns);

/*
echo "<pre>";
print_r($arr);
echo "</pre>";
*/

$filecont="<table border='0' width='100%' class='leadstable'>";

$arrcnt=count($arr);

$filecont.="<tr>";
for($j=3;$j<$arrcnt;$j++)
{
	$filecont.="<th>".ucfirst($arr[$j])."</th>";
}
$filecont.="</tr>";

while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {

	$responce->rows[$i]['id']=$row['id'];

	$leadname=$row['fname1']." ".$row['lname1']."<br>".$row['fname2']." ".$row['lname2'];

	$lead_address=$row['add_line1'];
	if ( $row['add_line2'] != '' )
	{
		$lead_address.=", <br>".$row['add_line2'];
	}
	if($row['city']!="")
	{
		if($lead_address!="")
		{
			$lead_address.=", <br>".$row['city'];
		}
		else
		{
			$lead_address.=$row['city'];
		}
	}

	if($row['state']!="")
	{
		if($lead_address!="" )
		{
			$lead_address.=", ".$row['state'];
		}
		else
		{
			$lead_address.=$row['state'];
		}
	}

	if($row['zip']!="")
	{
		if($lead_address!="")
		{
			$lead_address.=", ".$row['zip'];
		}
		else
		{
			$lead_address.=$row['zip'];
		}
	}

	$phoneno=$row['phone1'];
			if( $row['phone2'] != ''  && $row['phone2'] !='')
				$phoneno.=", <br/>".$row['phone2'];
			if( $row['phone3'] != '' &&  $row['phone3'] !='' )
				$phoneno.=", <br/>".$row['phone3']; 

	if($row['lead_in']!='0000-00-00'){
		$date = date_create($row['lead_in']);
		$lindate=date_format($date, 'm/d/y');
	}
	else{
		$lindate =' ';
	}

	
	$leadtype=getleadtype_name($row['lead_type']);
	$leadsubtype=getleadsubtype_name($row['lead_subtype']);

	if($row['referred_by']!='')
	{
		$ref=$row['referred_by'];
		$im=explode('_',$ref);
		if($im[0]==1){
			$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]' ")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=ucfirst($r['lname1'])." ".$r['fname1']."<br>".$r['lname2']." ".$r['fname2'];
		}
		if($im[0]==2){
			$sq=mysql_query("select campaign from tps_campaign where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=$r['campaign'];
		}
		if($im[0]==3){
			$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=ucfirst($r['fname1'])." ".$r['lname1'];
		}
	}else{
		$refby="";
	}

	$sql_select="select fname,lname from tps_users where userid='$row[dealer]'";
	$result_select = mysql_query($sql_select) or die(mysql_error());
	$row1 = mysql_fetch_array($result_select);

	
	if($row['setdate']==NULL)
	{
		$setdate="&nbsp;";
	}else{
		$setdate=date('m/d/y h:i A',strtotime($row['setdate']));
	}

	if($row['setby']==NULL)
	{
		$setby="&nbsp;";
	}else{
		$sql2="select fname,lname from tps_users where userid='".$row['setby']."'";
		$result2=mysql_query($sql2) or die(mysql_error());
		$row5 = mysql_fetch_array($result2);
		$setby=ucfirst($row5['fname'])." ".$row5['lname'];
	}
	

	$displayname="";
	$sql_select="select fname,lname,nickname from tps_users where userid='$row[lead_dealer]'";
	
	$result_select = mysql_query($sql_select) or die(mysql_error());
	$row2 = mysql_fetch_array($result_select);
	if($row2['nickname']!="")
			$displayname=ucfirst($row2['nickname'])." ".$row2['lname'];
		else
			$displayname=ucfirst($row2['fname'])." ".$row2['lname'];

	$lead_dealer=$displayname;

	if($row['oktocall']!="0000-00-00") 
		$startcontact=date('m/d/y',strtotime($row['oktocall']));
	else
		$startcontact="&nbsp;";

	$lead_status=$row['lead_status'];

	if($lead_status=="")
		$lead_status="&nbsp;";

	$lead_result=$row['lead_result'];
	
	if($lead_result=="")
		$lead_result="&nbsp;";

	if($lead_result=="")
		$lead_result="&nbsp;";

	$lead_quali=$row['lead_qualification'];

	if($lead_quali=="")
		$lead_quali="&nbsp;";

	if($row['last_modifiedon']==NULL)
	{
		$last_modifiedon="";
	}else{
		$last_modifiedon=date('m/d/y h:i A',strtotime($row['last_modifiedon']));
	}

	if($row['last_comments']==NULL)
	{
		$last_comments="";
	}else{
		$last_comments=$row['last_comments'];
	}

	$city=$row['city'];
	$state=$row['state'];
	$zip=$row['zip'];

	//$responce->rows[$i]['cell']=array($row['id'],$row['leadid'],$leadname,$lead_address,$phoneno,$lindate,$leadtype,$leadsubtype,$refby,$lead_dealer,$startcontact,$setdate,$setby,$lead_status,$lead_result,$lead_quali);

	if($i%2==0)
		$filecont.="<tr class='alt'>";
	else
		$filecont.="<tr>";

	for($j=3;$j<$arrcnt;$j++)
	{
		$colname=trim($arr[$j]);
		switch($colname)
		{
			case 'Lead Name':
				$filecont.="<td>".$leadname."</td>";
				break;
			case 'Address':
				$filecont.="<td>".$lead_address."</td>";
				break;
			case 'City':
				$filecont.="<td>".$city."</td>";
				break;
			case 'State':
				$filecont.="<td>".$state."</td>";
				break;
			case 'Zip':
				$filecont.="<td>".$zip."</td>";
				break;
			case 'Phone No':
				$filecont.="<td>".$phoneno."</td>";
				break;
			case 'Lead In Date':
				$filecont.="<td>".$lindate."</td>";
				break;
			case 'Lead Type':
				$filecont.="<td>".$leadtype."</td>";
				break;
			case 'Lead Subtype':
				$filecont.="<td>".$leadsubtype."</td>";
				break;
			case 'Referred By':
				$filecont.="<td>".$refby."</td>";
				break;
			case 'Lead Dealer':
				$filecont.="<td>".$lead_dealer."</td>";
				break;
			case 'Start Contacting':
				$filecont.="<td>".$startcontact."</td>";
				break;
			case 'Set Date':
				$filecont.="<td>".$setdate."</td>";
				break;
			case 'Set By':
				$filecont.="<td>".$setby."</td>";
				break;
			case 'Lead Status':
				$filecont.="<td>".$lead_status."</td>";
				break;
			case 'Lead Result':
				$filecont.="<td>".$lead_result."</td>";
				break;
			case 'Qual':
				$filecont.="<td>".$lead_quali."</td>";
				break;
			case 'Modified On':
				$filecont.="<td>".$last_modifiedon."</td>";
				break;
			case 'Comments':
				$filecont.="<td>".$last_comments."</td>";
				break;
			default:
				$filecont.="<td>Columns - Values Not Matching<td>";
				break;
		}
	}
	$filecont.="</tr>";

	$i++;
}
$filecont.="</table>";

//echo json_encode($responce);

if($export_type=="pdf")
{
	ob_start();
	require('../mpdf/mpdf.php');

	$mpdf=new mPDF('utf-8', 'A4');
	$stylesheet = file_get_contents('assets/css/pdf.css'); // external css
	$mpdf->WriteHTML($stylesheet,1);
	$mpdf->WriteHTML($filecont);
	$pdf = $mpdf->Output('', 'S'); 
	$ob = ob_get_contents(); 
	ob_end_clean(); 

	header('Content-Description: File Transfer');
	header('Content-Transfer-Encoding: binary');
	header('Content-Type: application/force-download');
	header('Content-Type: application/octet-stream', false);
	header('Content-Type: application/download', false);
	header('Content-Type: application/pdf', false);
	header("Content-Disposition: attachment; filename=Rpt-Leads-".$dt.".pdf");	
	header("Pragma: no-cache");

	echo $pdf;

}else{
	echo $filecont;
}

function Strip($value)
{
	if(get_magic_quotes_gpc() != 0)
  	{
    	if(is_array($value))  
			if ( array_is_associative($value) )
			{
				foreach( $value as $k=>$v)
					$tmp_val[$k] = stripslashes($v);
				$value = $tmp_val; 
			}				
			else  
				for($j = 0; $j < sizeof($value); $j++)
        			$value[$j] = stripslashes($value[$j]);
		else
			$value = stripslashes($value);
	}
	return $value;
}

function array_is_associative ($array)
{
    if ( is_array($array) && ! empty($array) )
    {
        for ( $iterator = count($array) - 1; $iterator; $iterator-- )
        {
            if ( ! array_key_exists($iterator, $array) ) { return true; }
        }
        return ! array_key_exists(0, $array);
    }
    return false;
}
?>
