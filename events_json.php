<?php
session_start();
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
require_once("include/tps_constants.php");

if(isset($_REQUEST['type'])){

  if($_REQUEST['type'] == 'fetchEventsDashBoard')
  { //for DashBoard fetching All Events 
	
	$starttime=$_REQUEST['start'];
	$endtime=$_REQUEST['end']; 

	$createdby=get_session('LOGIN_NAME');
	
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

	$tm=request_get('tm');
	$demo=request_get('demo');
	$events=request_get('events');
	$task=request_get('task');

if($demo=="1")
{
	$sql_grp="";
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

	if($tm=="1")
	{
		$re=mysql_query("select id,userid from tps_users where parentid='$cur_userid'");

		$sql_grp="select id,title,start,end,allDay,appttype,createdby from tps_events where delete_flag='0' AND ( assignedto='$cur_userid' OR  createdby in (".$cur_userid;

		$usrids=array();
		$i=0;
		while($r=mysql_fetch_array($re))
		{
			$usrids[$i]=$r['userid'];
			$i++;
		}
		if($i>0)
		{
			$sql_grp.=", ";
		}
		$sql_grp.=implode(",", $usrids);
		$sql_grp.=") ) GROUP BY lead_id";
	}else{
		$sql_grp="select id,title,start,end,allDay,appttype,createdby from tps_events where delete_flag='0' AND ( assignedto='$cur_userid' OR createdby='$cur_userid' ) GROUP BY lead_id";
	}

	//echo $sql_grp;

	$result=mysql_query($sql_grp) or die(mysql_error());

	if(mysql_num_rows($result)>0)
	{
	   while ($record = mysql_fetch_array($result))
	   {
		$titlesplit = explode("-", $record['title']);
		//echo $titlesplit[0]."<br><hr>";
		$titlesplit[0]=trim($titlesplit[0]);
		$titlesplit[1]=trim(isset($titlesplit[1]));
		
		if($record['createdby']==$cur_userid)
		{
		   if($record['appttype']=="Demo")
		   {
			$event_array[] = array(
		    		'id' => $record['id'],
		    		'title' => $record['title'],
		    		'start' => $record['start'],
		    		'end' => $record['end'],
		    		'allDay' => (bool)$record['allDay'],
		    		'address' => $titlesplit['1'],
		    		'appttype' => $record['appttype'],
				'className'=> 'apptSelf'
			);
		   }else if($record['appttype']=="Training"){
			$event_array[] = array(
		    		'id' => $record['id'],
		    		'title' => $record['title'],
		    		'start' => $record['start'],
		    		'end' => $record['end'],
		    		'allDay' => (bool)$record['allDay'],
		    		'address' => $titlesplit['1'],
		    		'appttype' => $record['appttype'],
		       		'className'=> 'apptSelf'
			);

		   }else if($record['appttype']=="AdvancedTrainingClinics"){

			$event_array[] = array(
		    		'id' => $record['id'],
		    		'title' => $record['title'],
		    		'start' => $record['start'],
		    		'end' => $record['end'],
		    		'allDay' => (bool)$record['allDay'],
		    		'address' => $titlesplit['1'],
		    		'appttype' => $record['appttype'],
	            		'className'=> 'apptSelf'	
			);
	
		} 
	     }else{
		
		if($tm=="1")
		{

		    if($record['appttype']=="Demo")
		    {
			$event_array[] = array(
		    		'id' => $record['id'],
		    		'title' => $record['title'],
		    		'start' => $record['start'],
		    		'end' => $record['end'],
		    		'allDay' => (bool)$record['allDay'],
		    		'address' => $titlesplit['1'],
		    		'appttype' => $record['appttype'],
        	    		'className'=> 'apptOther'	
			);
		    }else if($record['appttype']=="Training"){
			$event_array[] = array(
		    		'id' => $record['id'],
		    		'title' => $record['title'],
		    		'start' => $record['start'],
		    		'end' => $record['end'],
		    		'allDay' => (bool)$record['allDay'],
		    		'address' => $titlesplit['1'],
		    		'appttype' => $record['appttype'],
		       		'className'=> 'apptOther'	
			);

		    }else if($record['appttype']=="AdvancedTrainingClinics"){
 
			$event_array[] = array(
		    		'id' => $record['id'],
		    		'title' => $record['title'],
		    		'start' => $record['start'],
		    		'end' => $record['end'],
		    		'allDay' => (bool)$record['allDay'],
		    		'address' => $titlesplit['1'],
		    		'appttype' => $record['appttype'],
	            		'className'=> 'apptOther'
			);
		   }
		}

	     } // end user check if
		
	  }	//end while
	
	}else{
			$event_array[] = array(
			    'id' => "",
			    'title' => "",
			    'start' => "",
			    'end' => "",
			    'allDay' => "",
			    'address' => "",
			    'appttype' => "",
			    'className'=> ""
			);
        }

} // if Demo is 1


if($events=="1")
{

//for Creators 
	$sql_ml_creator=mysql_query("select * from `tps_training_details` where creatoruserid ='".$cur_userid."' and delete_flag='0' GROUP BY trainingseqid") or die(mysql_error());

	if(mysql_num_rows($sql_ml_creator)>0)
        {
			   
		while($rml=mysql_fetch_array($sql_ml_creator))
		{
			
			$event_array[] = array(
		    		'id' => $rml['id'],
				'tseqid' => $rml['trainingseqid'],
		    		'title' => $rml['trainingname']." - ".$rml['trainingsubname'].'-'.$rml['trainingdesc'],
		    		'start' => $rml['starttime'],
		    		'end' => $rml['endtime'],
		    		'allDay' => (bool)'0',
		    		'appttype' => "TrainingFor",
		       		'className'=> 'eventSelf'
			);    

	 	} // end while

	}else{
			$event_array[] = array(
			    'id' => "",
			    'tseqid' => "",
			    'title' => "",
			    'start' => "",
			    'end' => "",
			    'allDay' => 0,
			    'appttype' => "",
			    'className'=> ""
			);
       }

//for Receivers

	$sql_ml_receiver=mysql_query("select * from `tps_training_details` where starttime >= '".date("Y-m-d H:i:s", $starttime)."' AND endtime <= '".date("Y-m-d H:i:s", $endtime)."' AND receiveruserid ='".$cur_userid."' AND creatoruserid !='".$cur_userid."'") or die(mysql_error());

	if(mysql_num_rows($sql_ml_receiver)>0)
        {
			   
		while($rml=mysql_fetch_array($sql_ml_receiver))
		{
			if($rml['eventid']==0)
			{
				$event_array[] = array(
		    			'id' => $rml['id'],
					'tseqid' => $rml['trainingseqid'],
		    			'title' => $rml['trainingname']." - ".$rml['trainingsubname'].'-'.$rml['trainingdesc'],
		    			'start' => $rml['starttime'],
		    			'end' => $rml['endtime'],
		    			'allDay' => (bool)'0',
		    			'address' => "",
		    			'appttype' => "TrainingFor",
			       		'className'=> 'eventSelf'		       				
				);    
			}else{

	$sql_grp="select id,title,start,end,allDay,appttype,createdby from tps_events where id='".$rml['eventid']."' and delete_flag='0'";
	
		$result=mysql_query($sql_grp) or die(mysql_error());
			   while ($record = mysql_fetch_array($result))
	   		   {
				$titlesplit = explode("-", $record['title']);
		
				$titlesplit[0]=trim($titlesplit[0]);
				$titlesplit[1]=trim($titlesplit[1]);

				if($record['appttype']=="Training"){
				   $event_array[] = array(
		    			'id' => $record['id'],
					'tseqid' => $rml['trainingseqid'],
		    			'title' => $record['title'],
		    			'start' => $record['start'],
		    			'end' => $record['end'],
		    			'allDay' => (bool)$record['allDay'],
		    			'address' => $titlesplit['1'],
		    			'appttype' => $record['appttype'],
			       		'className'=> 'eventSelf'
				);

			    }
			}
		
			}
	 	} // end while

	 }else{
			$event_array[] = array(
			    'id' => "",
			    'tseqid' => "",
			    'title' => "",
			    'start' => "",
			    'end' => "",
			    'allDay' => 0,
			    'address' => "",
			    'appttype' => "",
	     		    'className'=> 'eventSelf'
			);
            }

}// for events 1


if($task=="1")
{

// For Task for Creators

	$cur_uid=get_session('LOGIN_ID');

	$task_sql=mysql_query("select id,task_seqid,task_id,title,start,end,createdby from `tps_tasks` where start >= '".date("Y-m-d H:i:s", $starttime)."' AND end <= '".date("Y-m-d H:i:s", $endtime)."' AND taskuserid ='".$cur_uid."' AND createdby ='".$cur_userid."' group by task_seqid") or die(mysql_error());

	if(mysql_num_rows($task_sql)>0)
        {
			   
		while($rml=mysql_fetch_array($task_sql))
		{
			
			$event_array[] = array(
	    			'id' => $rml['task_id'],
				'tseqid' => $rml['task_seqid'],
	    			'title' => $rml['title'],
	    			'start' => $rml['start'],
	    			'end' => $rml['end'],
	    			'allDay' => (bool)'0',
	    			'address' => "",
	    			'appttype' => "Task",
	       			'className'=> 'taskSelf'	
			);    
		
	 	} // end while

	 }else{
			$event_array[] = array(
			    'id' => "",
			    'tseqid' => "",
			    'title' => "",
			    'start' => "",
			    'end' => "",
			    'allDay' => 0,
			    'address' => "",
			    'appttype' => "",
			    'backgroundColor'=>"",
			    'borderColor'=>"",
			    'textColor'=>""
			);
            }

// For Task for Receivers

	$cur_uid=get_session('LOGIN_ID');

	$task_sql=mysql_query("select id,task_seqid,task_id,title,start,end,createdby from `tps_tasks` where start >= '".date("Y-m-d H:i:s", $starttime)."' AND end <= '".date("Y-m-d H:i:s", $endtime)."' AND taskuserid ='".$cur_uid."' AND createdby !='".$cur_userid."' group by task_seqid") or die(mysql_error());

	if(mysql_num_rows($task_sql)>0)
        {
			   
		while($rml=mysql_fetch_array($task_sql))
		{
			
			$event_array[] = array(
	    			'id' => $rml['task_id'],
				'tseqid' => $rml['task_seqid'],
	    			'title' => $rml['title'],
	    			'start' => $rml['start'],
	    			'end' => $rml['end'],
	    			'allDay' => (bool)'0',
	    			'address' => "",
	    			'appttype' => "Task",
	       			'className'=> 'taskSelf'
			);    
		
	 	} // end while

	 }else{
			$event_array[] = array(
			    'id' => "",
			    'tseqid' => "",
			    'title' => "",
			    'start' => "",
			    'end' => "",
			    'allDay' => 0,
			    'address' => "",
			    'appttype' => "",
			    'className'=> 'taskSelf'
			);
            }

} // for task is 1

	echo json_encode($event_array);

  }
  elseif($_REQUEST['type'] == 'addTrainingEvent'){

		$r="0";
		$popupHtml = genTrainingPopup($r);
		$startSecs = getSec($_REQUEST['start']);
		$endSecs = getSec($_REQUEST['end']);
		$event_edit = array(
			      'popupHtml' => $popupHtml,
			      'startSecs' => $startSecs,
			      'endSecs' => $endSecs
			  	);
		echo json_encode($event_edit);
   }
   elseif($_REQUEST['type'] == 'deleteEventFiles'){
		
		$fileid=$_REQUEST['fileid'];
		$eventid=$_REQUEST['eventid'];

		$fpres=mysql_query("select filepath from attachments where `id`='".$fileid."'")or die(mysql_error());
		$fpr=mysql_fetch_array($fpres);

		$filedname=$fpr['filepath'];

		if(unlink($filedname))
		{
			mysql_query("delete from attachments where `id`='".$fileid."'")or die(mysql_error());

			$res=mysql_query("select trainingseqid,attachments from tps_training_details where id='".$eventid."'");	

			$r=mysql_fetch_array($res);
			$tseqid=$r['trainingseqid'];
		
			$oldupids=explode(',',$r['attachments']);

			$newupids=array();

			for($i=0;$i<count($oldupids);$i++)
			{
				if($oldupids[$i]!=$fileid)
				{
					$newupids[$i]=$oldupids[$i];
				}
			}

			$updnewids=implode(",",$newupids);

		mysql_query("update tps_training_details set attachments='".$updnewids."' where trainingseqid='".$tseqid."'") or die(mysql_error());
			
			echo "update tps_training_details set attachments='".$updnewids."' where trainingseqid='".$tseqid."'";
			echo "<br>Image Deleted";

			$user_dispname=get_session('DISPLAY_NAME');
			$user_logid=get_session('LOGIN_ID');
			$url= $_SERVER['HTTP_REFERER'];
			$log_desc=" Event Attachment Deleted by $user_dispname. <b><a href=$url target=_blank >$url</a></b>";
			tps_log_error(__INFO__, __FILE__, __LINE__, "Event Attachment Deleted ", $user_logid, $log_desc);

		}
		else
		{
			echo "Error in Deleting..!";
		}

   }elseif($_REQUEST['type'] == 'saveRemarks'){
		
		$appttype=$_REQUEST['appttype'];

		$isaccept=$_REQUEST['isaccept'];
		$remarks=$_REQUEST['remarks'];
		$id=$_REQUEST['id'];
		$seqid=$_REQUEST['seqid'];
		$tseqid=$_REQUEST['tseqid'];
		
		$cur_userid=get_session('LOGIN_USERID');

		if($appttype=="Training"){
		
	$sql="update tps_training_details set isaccepted='".$isaccept."', remarks='".$remarks."', acceptdate='".date("Y-m-d H:i:s")."' where receiveruserid='".$cur_userid."' and trainingname='0' and id='".$seqid."'";
		
		}elseif($appttype=="TrainingFor"){

	$sql="update tps_training_details set isaccepted='".$isaccept."', remarks='".$remarks."', acceptdate='".date("Y-m-d H:i:s")."' where receiveruserid='".$cur_userid."' and id='".$seqid."' and eventid='0' and trainingseqid='".$tseqid."'";
	
		}
	mysql_query($sql) or die(mysql_error());

	$user_dispname=get_session('DISPLAY_NAME');
	$user_logid=get_session('LOGIN_ID');
	$url= $_SERVER['HTTP_REFERER'];
	$log_desc=" Event Invite Accepted by $user_dispname with remarks of $remarks. <b><a href=$url target=_blank >$url</a></b>";
	tps_log_error(__INFO__, __FILE__, __LINE__, "Event Invite Accepted ", $user_logid, $log_desc);


   }elseif($_REQUEST['type'] == 'editDropTrainingEvent'){
		$eventId = $_REQUEST['id'];
		$tseqid = $_REQUEST['tseqid'];
		$appttype=$_REQUEST['appttype'];

		if($appttype=="TrainingFor")
		{
			$popupHtml = genTrainingPopupRemarks($eventId);
			$event_edit = array(
				      'popupHtml' => $popupHtml,
				      'id' => $eventId
				  	);
			echo json_encode($event_edit);
		}else{
			$popupHtml = genPopupRemarks($eventId);
			$event_edit = array(
				      'popupHtml' => $popupHtml,
				      'id' => $eventId
				  	);
			echo json_encode($event_edit);
		}
  }
  elseif($_REQUEST['type'] == 'saveDropTrainingEvent')
  {
		$rId = $_REQUEST['id'];
		
		$remarks=$_REQUEST['remarks'];
		$starttime=strtotime($_REQUEST['start']);
		$endtime=strtotime($_REQUEST['end']);
		$appttype=$_REQUEST['appttype'];
		
		$cur_userid=get_session('LOGIN_USERID');

		$newupdremarks="";

		if($appttype=="Training"){

			$sql="update tps_training_details set updremarks='".$remarks."', starttime='".date("Y-m-d H:i:s", $starttime)."', endtime='".date("Y-m-d H:i:s", $endtime)."' where creatoruserid='".$cur_userid."' and trainingname='0' and eventid='".$rId."'";
		
			$result=mysql_query($sql);

			if (!$result) {
				    
			     $user_dispname=get_session('DISPLAY_NAME');
			     $user_logid=get_session('LOGIN_ID');
			     $url= $_SERVER['HTTP_REFERER'];
			     $log_desc="<b>Query</b>: ".$sql."<br><b>Error</b>:".mysql_error()."<br><a href=$url target=_blank >$url</a></b>";

			     tps_log_error(__CRITICAL__, __FILE__, __LINE__, "Event Rescheduled CRITICAL ERROR", $user_logid, $log_desc);

			}else{

			     $user_dispname=get_session('DISPLAY_NAME');
			     $user_logid=get_session('LOGIN_ID');
			     $url= $_SERVER['HTTP_REFERER'];
			     $log_desc= ucfirst($user_dispname)." at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank >$url</a></b>";
			     tps_log_error(__INFO__, __FILE__, __LINE__, "Event Rescheduled ", $user_logid, $log_desc);

			}

		}
		elseif($appttype=="TrainingFor")
		{
			$tseqid=$_REQUEST['tseqid'];

			$prevdetails=mysql_query("select starttime,endtime,updremarks from tps_training_details where creatoruserid='".$cur_userid."' and trainingseqid='".$tseqid."'");

			$pd=mysql_fetch_array($prevdetails);

			if($pd['updremarks']!="")
			{
				$newupdremarks=$pd['updremarks']."/".$pd['starttime']." ".$pd['endtime']."@".$remarks;
			}else{
				$newupdremarks=$pd['starttime']." ".$pd['endtime']."@".$remarks;
			}
		
			$sql="update tps_training_details set updremarks='".$newupdremarks."', starttime='".date("Y-m-d H:i:s", $starttime)."', endtime='".date("Y-m-d H:i:s", $endtime)."', lastupddate='".date("Y-m-d H:i:s")."' where creatoruserid='".$cur_userid."' and trainingseqid='".$tseqid."'";
			$result=mysql_query($sql);
		
			if (!$result) {
				    
			     $user_dispname=get_session('DISPLAY_NAME');
			     $user_logid=get_session('LOGIN_ID');
			     $url= $_SERVER['HTTP_REFERER'];
			     $log_desc="<b>Query</b>: ".$sql."<br><b>Error</b>:".mysql_error()."<br><a href=$url target=_blank >$url</a></b>";

			     tps_log_error(__CRITICAL__, __FILE__, __LINE__, "Event Rescheduled CRITICAL ERROR", $user_logid, $log_desc);

			}else{

			     $user_dispname=get_session('DISPLAY_NAME');
			     $user_logid=get_session('LOGIN_ID');
			     $url= $_SERVER['HTTP_REFERER'];
			     $log_desc= ucfirst($user_dispname)." at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank >$url</a></b>";
			     tps_log_error(__INFO__, __FILE__, __LINE__, "Event Rescheduled ", $user_logid, $log_desc);

			}

		}
		else if($appttype=="Demo")
		{
			
			$sql="UPDATE tps_events set remarks='$remarks', 
				start='".date("Y-m-d H:i:s", $starttime)."', 
				end='".date("Y-m-d H:i:s", $endtime)."'
				WHERE id =".$rId;
			$result=mysql_query($sql);

			if (!$result) {
				    
			     $user_dispname=get_session('DISPLAY_NAME');
			     $user_logid=get_session('LOGIN_ID');
			     $url= $_SERVER['HTTP_REFERER'];
			     $log_desc="<b>Query</b>: ".$sql."<br><b>Error</b>:".mysql_error()."<br><a href=$url target=_blank >$url</a></b>";

			     tps_log_error(__CRITICAL__, __FILE__, __LINE__, "Appointment Rescheduled CRITICAL ERROR", $user_logid, $log_desc);

			}else{

			     $user_dispname=get_session('DISPLAY_NAME');
			     $user_logid=get_session('LOGIN_ID');
			     $url= $_SERVER['HTTP_REFERER'];
			     $log_desc= ucfirst($user_dispname)." at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank >$url</a></b>";
			     tps_log_error(__INFO__, __FILE__, __LINE__, "Appointment Rescheduled ", $user_logid, $log_desc);

			}

		}else if($appttype=="AdvancedTrainingClinics"){
			
			$sql="UPDATE tps_events set remarks='$remarks', 
				start='".date("Y-m-d H:i:s", $starttime)."', 
				end='".date("Y-m-d H:i:s", $endtime)."'
				WHERE id =".$rId;

			$result=mysql_query($sql);

			if (!$result) {
				    
			     $user_dispname=get_session('DISPLAY_NAME');
			     $user_logid=get_session('LOGIN_ID');
			     $url= $_SERVER['HTTP_REFERER'];
			     $log_desc="<b>Query</b>: ".$sql."<br><b>Error</b>:".mysql_error()."<br><a href=$url target=_blank >$url</a></b>";

			     tps_log_error(__CRITICAL__, __FILE__, __LINE__, "Appointment Rescheduled", $user_logid, $log_desc);

			}else{
		
				$user_dispname=get_session('DISPLAY_NAME');
				$user_logid=get_session('LOGIN_ID');
				$url= $_SERVER['HTTP_REFERER'];
				$log_desc= ucfirst($user_dispname)." at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank >$url</a></b>";
				tps_log_error(__INFO__, __FILE__, __LINE__, "Appointment Rescheduled ", $user_logid, $log_desc);

			}

		}

		//$result=mysql_query($sql) or die(mysql_error());

		echo "Successfully Updated";
  }
  elseif($_REQUEST['type'] == 'saveAttendedDetails'){
		
		$checkedids=implode(",",$_REQUEST['checkedids']);
		$uncheckedids=implode(",",$_REQUEST['uncheckedids']);
		$appttype=$_REQUEST['appttype'];

		$tseqid = $_REQUEST['tseqid'];

		$cur_userid=get_session('LOGIN_USERID');

		$mlist=explode(",",$checkedids);

		if(count($mlist)>0)
		{	
		   for($i=0;$i<count($mlist);$i++)
		   {
			$sql="update tps_training_details set isattended='1' where receiveruserid='".$mlist[$i]."' and trainingseqid='".$tseqid."'";
			$result=mysql_query($sql);
			if (!$result) {
				    
			     $user_dispname=get_session('DISPLAY_NAME');
			     $user_logid=get_session('LOGIN_ID');
			     $url= $_SERVER['HTTP_REFERER'];
			  
		$log_desc="<b>Query</b>: ".$sql."<br><b>Error</b>:".mysql_error()."<br><a href=$url target=_blank class=react>$url</a></b>";

			     tps_log_error(__CRITICAL__, __FILE__, __LINE__, "Event Updated - Attended List", $user_logid, $log_desc);
			}
		   }
		}
		$unmlist=explode(",",$uncheckedids);
	
		if(count($unmlist)>0)
		{
		   for($i=0;$i<count($unmlist);$i++)
		   {
			$sql="update tps_training_details set isattended='0' where receiveruserid='".$unmlist[$i]."' and trainingseqid='".$tseqid."'";
			$result=mysql_query($sql);
			if (!$result) {
				    
			     $user_dispname=get_session('DISPLAY_NAME');
			     $user_logid=get_session('LOGIN_ID');
			     $url= $_SERVER['HTTP_REFERER'];

		   $log_desc="<b>Query</b>: ".$sql."<br><b>Error</b>:".mysql_error()."<br><a href=$url target=_blank class=react>$url</a></b>";

			     tps_log_error(__CRITICAL__, __FILE__, __LINE__, "Event Updated - Attended List", $user_logid, $log_desc);
			}
		   }
		}

		$user_dispname=get_session('DISPLAY_NAME');
		$user_logid=get_session('LOGIN_ID');
		$url= $_SERVER['HTTP_REFERER'];
		$log_desc= ucfirst($user_dispname)." at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank class=react >$url</a></b>";
		tps_log_error(__INFO__, __FILE__, __LINE__, "Event Updated - Attended List ", $user_logid, $log_desc);

		echo "Successfully Updated";
  }
  else if($_REQUEST['type'] == 'saveTraining'){
		
		$trnids=implode(",",$_REQUEST['trainingname']);
		$trndesc=$_REQUEST['trndesc'];
		$eventdate=$_REQUEST['eventdate'];
		$start=$_REQUEST['start'];
		$end=$_REQUEST['end'];

		$memberslist= implode(",", $_REQUEST['memberslist1']);
		$file=$_REQUEST['file'];

		$start=$eventdate." ".$start;
		$starttime=date("Y-m-d H:i:s",strtotime($start));

		$end=$eventdate." ".$end;
		$endtime=date("Y-m-d H:i:s", strtotime($end));

		$tid=explode(",",$trnids);

		$trnnames=array();	
		$parentid=0;

		for($i=0;$i<count($tid);$i++)
		{
			$id=$tid[$i];
			$tres=mysql_fetch_array(mysql_query("select id,event_subtype,parent from tps_event_subtype where id='$id'"));
			$trnnames[$i]=$tres['event_subtype'];
			$parentid=$tres['parent'];
		}
		$eres=mysql_fetch_array(mysql_query("select id,event_type from tps_event_type where id='$parentid'"));
		
		$eventname=$eres['event_type'];
		$topics=implode(",",$trnnames);		
		
		$recuserids=explode(',',$memberslist);
		
		$cur_userid=get_session('LOGIN_USERID');
		$cur_username=get_session('LOGIN_NAME');
		
		mysql_query("INSERT INTO `tps_training_seq` (`id`) VALUES (NULL)") or die(mysql_error());
		$tseqid=mysql_insert_id();

		// message seq id 

		mysql_query("INSERT INTO `msg_seqid` (`id`) VALUES (NULL);");
		$msgseqid=mysql_insert_id();

		$invdate=date("M d,Y ",strtotime($eventdate)).", ".date("h:i A",strtotime($start))." - ".date("h:i A",strtotime($end));

		$createddate=date('Y-m-d H:i:s');
		$cur_userdispname=get_session('DISPLAY_NAME');

		$msgbody="";
		$msgbody.="<div style=color:#000; line-height:25px;> Hi %firstname%, <br><br> ".ucfirst($cur_userdispname)." would like to invite you to the following event: <br><br>";
		$msgbody.= "<b><a href=# onclick=javascript:eventpopup($tseqid); >".$eventname.' - '.$topics.' - '.$invdate.'</a></b>';
		$msgbody.="<br><br> Please take a moment and respond if you will be attending.<br><br> Thanks!<br><br> Event created by ".ucfirst($cur_userdispname)." on ".date('m/d/Y h:i A',strtotime($createddate))." <br>";

		$msgbody.="<br></div><br>";

		$toUserIdList=array();
		for($i=0;$i<count($recuserids);$i++)
		{
			$rcid=explode('-',$recuserids[$i]);
			$toUserIdList[$i]=$rcid[0];

			if($rcid[0]!=$cur_userid)
			{
				$sql1="INSERT INTO `tps_training_details` (`id`, `trainingseqid`, `trainingname`,`topics_parent_id`,`trainingsubname`, `topicids`, `attachments`, `trainingdesc`, `creatoruserid`, `creatorusername`, `receiveruserid`,  `starttime`, `endtime`, `createddate`, `createdby`) VALUES (NULL, '".$tseqid."', '".$eventname."','".$parentid."','".$topics."', '".$trnids."', '".$file."',  '".$trndesc."', '".$cur_userid."', '".$cur_username."', '".$rcid[0]."', '".$starttime."', '".$endtime."', '".date("Y-m-d H:i:s")."', '".$cur_username."')";

				$result=mysql_query($sql1);

				if (!$result) {
				    
				     $user_dispname=get_session('DISPLAY_NAME');
				     $user_logid=get_session('LOGIN_ID');
				     $url= $_SERVER['HTTP_REFERER'];
		
			 $log_desc="<b>Query</b>: ".$sql1."<br><b>Error</b>:".mysql_error()."<br><a href=$url target=_blank class=react>$url</a></b>";

				     tps_log_error(__CRITICAL__, __FILE__, __LINE__, "Event Created", $user_logid, $log_desc);
				}

			}else{
				$sql1="INSERT INTO `tps_training_details` (`id`, `trainingseqid`, `trainingname`,`topics_parent_id`,`trainingsubname`, `topicids`, `attachments`, `trainingdesc`, `creatoruserid`, `creatorusername`, `receiveruserid`,  `starttime`, `endtime`, `isaccepted`, `isattended`, `createddate`, `createdby`) VALUES (NULL, '".$tseqid."', '".$eventname."','".$parentid."','".$topics."', '".$trnids."', '".$file."',  '".$trndesc."', '".$cur_userid."', '".$cur_username."', '".$rcid[0]."', '".$starttime."', '".$endtime."', 'YES', '1', '".date("Y-m-d H:i:s")."', '".$cur_username."')";

				$result=mysql_query($sql1);

				if (!$result) {
				    
				     $user_dispname=get_session('DISPLAY_NAME');
				     $user_logid=get_session('LOGIN_ID');
				     $url= $_SERVER['HTTP_REFERER'];
			   $log_desc="<b>Query</b>: ".$sql1." <br> Error : ".mysql_error()." <br> <a href=$url target=_blank class=react>$url</a></b>";

				     tps_log_error(__CRITICAL__, __FILE__, __LINE__, "Event Created", $user_logid, $log_desc);
				}
			}
		}

		$user_dispname=get_session('DISPLAY_NAME');
		$user_logid=get_session('LOGIN_ID');
		$url= $_SERVER['HTTP_REFERER'];
		$log_desc="<b>$eventname - $topics</b> Event created by $user_dispname <b><a href=$url target=_blank class=react >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Event Created", $user_logid, $log_desc);


		$sendto1=implode(",",$toUserIdList);
		
		$sendto=get_user_ids_messages($sendto1);
	
		$subject=htmlentities("A New Event from LCAS", ENT_QUOTES, "UTF-8");
		$message=htmlentities($msgbody, ENT_QUOTES, "UTF-8");

		send_internal_message($msgseqid,$sendto,$subject,$message);

		$msgurl="/messages/sent.php";

		$log_desc=" <b>$subject</b> subjected message sent by $user_dispname <b><a href=$msgurl class=react target=_blank >http://".$_SERVER['SERVER_NAME']."$msgurl</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Event Message Sent", $user_logid, $log_desc);

		//echo "Successfully Updated";

	}else if($_REQUEST['type'] == 'saveTraining1'){
		
		$trnids = implode(",",$_REQUEST['trainingname']);

		$memberslist= implode(",", $_REQUEST['memberslist1']);

		$tid=explode(",",$trnids);

		$trnnames=array();	
		$parentid=0;

		for($i=0;$i<count($tid);$i++)
		{
			$id=$tid[$i];
			$tres=mysql_fetch_array(mysql_query("select id,event_subtype,parent from tps_event_subtype where id='$id'"));
			$trnnames[$i]=$tres['event_subtype'];
			$parentid=$tres['parent'];
		}
		$eres=mysql_fetch_array(mysql_query("select id,event_type from tps_event_type where id='$parentid'"));
		
		$eventname=$eres['event_type'];
		$topics=implode(",",$trnnames);	

		$trndesc=$_REQUEST['trndesc'];
		
		$curtrainingseqid=$_REQUEST['curtrainingseqid'];
		
		$cur_userid=get_session('LOGIN_USERID');
		$cur_username=get_session('LOGIN_NAME');
		
		$recuserids=explode(',',$memberslist);
	
		$exist_sql=mysql_query("select receiveruserid,starttime,endtime,attachments from tps_training_details where creatoruserid='".$cur_userid."' and trainingseqid='".$curtrainingseqid."'")or die(mysql_error());		

		$oldMembers=array();
		$newMembers=array();
		
		$starttime="";
		$endtime="";
		$file="";

		$i=0;
		while($ex_res=mysql_fetch_array($exist_sql))
		{
			$oldMembers[$i]=$ex_res['receiveruserid'];
			$starttime=$ex_res['starttime'];
			$endtime=$ex_res['endtime'];
			$file=$ex_res['attachments'];
			$i++;
		}

		for($i=0;$i<count($recuserids);$i++)
		{
			$rcid=explode('-',$recuserids[$i]);
			$newMembers[$i]=$rcid[0];
		}
	
		// Checking for removed users
		$removed = array_diff($oldMembers, $newMembers);
		$removed = array_values($removed);

		// Checking for Newly added users
		$added = array_diff($newMembers,$oldMembers);
		$added = array_values($added);

		// Merging two arrays
		$merged = array_unique(array_merge($oldMembers, $newMembers));

		// ReIndexing array
		$curMembers = array_values($merged);
		
		$isLoggedDelete=0;
		$isLoggedInsert=0;
		$isLoggedUpdate=0;

		foreach ($curMembers as $search_item)
		{
	    		if (in_array($search_item, $removed))
	    		{
				$rem_sql="delete from tps_training_details where creatoruserid='".$cur_userid."' and trainingseqid='".$curtrainingseqid."' and receiveruserid='$search_item'";
				$result=mysql_query($rem_sql) or die(mysql_error());

			   if($isLoggedDelete==0)
			   {
				$user_dispname=get_session('DISPLAY_NAME');
				$user_logid=get_session('LOGIN_ID');
				$url= $_SERVER['HTTP_REFERER'];
				$log_desc=" <b>$eventname - $topics</b> Event updated by $user_dispname <b><a href=$url target=_blank >$url</a></b>";
			tps_log_error(__INFO__, __FILE__, __LINE__, "Event Updated - Members Deleted from Invited List ", $user_logid, $log_desc);

				$isLoggedDelete=1;

			   }

	    		}
			else if (in_array($search_item, $added)){

				$add_sql="INSERT INTO `tps_training_details` (`id`, `trainingseqid`, `trainingname`,`topics_parent_id`,`trainingsubname`, `topicids`, `attachments`, `trainingdesc`, `creatoruserid`, `creatorusername`, `receiveruserid`,  `starttime`, `endtime`, `createddate`, `createdby`) VALUES (NULL, '".$curtrainingseqid."', '".$eventname."','".$parentid."','".$topics."', '".$trnids."', '".$file."',  '".$trndesc."', '".$cur_userid."', '".$cur_username."', '".$search_item."', '".$starttime."', '".$endtime."', '".date("Y-m-d H:i:s")."', '".$cur_username."')";

				$result=mysql_query($add_sql) or die(mysql_error());
			
			   if($isLoggedInsert==0)
			   {
				$user_dispname=get_session('DISPLAY_NAME');
				$user_logid=get_session('LOGIN_ID');
				$url= $_SERVER['HTTP_REFERER'];
				$log_desc=" <b>$eventname - $topics</b> Event updated by $user_dispname <b><a href=$url target=_blank >$url</a></b>";
				tps_log_error(__INFO__, __FILE__, __LINE__, "Event Updated - New Members Invited ", $user_logid, $log_desc);

				$isLoggedInsert=1;
			   }

			}
			else{
			
				$upd_sql="update tps_training_details set `trainingname`='".$eventname."', `topics_parent_id`='$parentid', `trainingsubname`='".$topics."', `topicids`='$trnids', `trainingdesc`='".$trndesc."' where creatoruserid='".$cur_userid."' and trainingseqid='".$curtrainingseqid."' and receiveruserid='$search_item'";

				$result=mysql_query($upd_sql) or die(mysql_error());	
			     
			     if($isLoggedUpdate==0)
			     {
				$user_dispname=get_session('DISPLAY_NAME');
				$user_logid=get_session('LOGIN_ID');
				$url= $_SERVER['HTTP_REFERER'];
				$log_desc=" <b>$eventname - $topics</b> Event updated by $user_dispname <b><a href=$url target=_blank >$url</a></b>";
				tps_log_error(__INFO__, __FILE__, __LINE__, "Event Updated - Details Changed ", $user_logid, $log_desc);
				$isLoggedUpdate=1;
			     }

			}
			$k++;
		}

				
	}else if($_REQUEST['type'] == 'Deletetraining'){

		$curtrainingseqid=$_REQUEST['curtrainingseqid'];

		mysql_query("delete from tps_training_details where trainingseqid='$curtrainingseqid'");

		$user_dispname=get_session('DISPLAY_NAME');
		$user_logid=get_session('LOGIN_ID');
		$url= $_SERVER['HTTP_REFERER'];
		$log_desc=" <b>$eventname - $topics</b> Event deleted by $user_dispname <b><a href=$url target=_blank >$url</a></b>";
		tps_log_error(__INFO__, __FILE__, __LINE__, "Event Deleted", $user_logid, $log_desc);
		
	}else if($_REQUEST['type'] == 'editTrainingEventbyCreator'){
		
		$req_id=$_REQUEST['id'];
		$eventType=$_REQUEST['appttype'];		
		$cur_userid=get_session('LOGIN_USERID');

		if($eventType=="TrainingFor")
		{
			$tseq_id=$_REQUEST['tseqid'];
			$sql="select * from tps_training_details where id='".$req_id."' and trainingseqid='".$tseq_id."'";

			$result=mysql_query($sql) or die(mysql_error());
		
			while($r=mysql_fetch_array($result))
			{

			    if($r['creatoruserid']==$cur_userid)
			    {
				$popupHtml = genTrainingPopup($r);
				$startSecs = getSec($_REQUEST['start']);
				$endSecs = getSec($_REQUEST['end']);
				$event_edit = array(
				      'popupHtml' => $popupHtml,
				      'startSecs' => $startSecs,
				      'endSecs' => $endSecs
			  	);
		    	    }else if($r['receiveruserid']==$cur_userid){

				$popupHtml = genTrainingAcceptPopup($r);
				$startSecs = getSec($_REQUEST['start']);
				$endSecs = getSec($_REQUEST['end']);
				$event_edit = array(
				      'popupHtml' => $popupHtml,
				      'startSecs' => $startSecs,
				      'endSecs' => $endSecs
			  	);
			
		    	    } 
		       }
		    echo json_encode($event_edit);

		}
		else if($eventType=="Training")
		{

			$sql="select * from tps_training_details where eventid='".$req_id."'";

			$result=mysql_query($sql) or die(mysql_error());
		
			while($r=mysql_fetch_array($result))
			{
			    if($r['creatoruserid']==$cur_userid)
			    {
				$eventId = $_REQUEST['id'];
				$sql="SELECT  * 
					FROM tps_events 
					WHERE id =".$eventId;
				$result=mysql_query($sql) or die(mysql_error());
				$row = mysql_fetch_assoc($result);
				$popupHtml = genPopup($row);

				$startSecs = getSec($_REQUEST['start']);
				$endSecs = getSec($_REQUEST['end']);
				$event_edit = array(
				      'popupHtml' => $popupHtml,
				      'startSecs' => $startSecs,
				      'endSecs' => $endSecs
			  	);
		    	    }else if($r['receiveruserid']==$cur_userid){

				$popupHtml = genTrainingAcceptPopup($r);
				$startSecs = getSec($_REQUEST['start']);
				$endSecs = getSec($_REQUEST['end']);
				$event_edit = array(
				      'popupHtml' => $popupHtml,
				      'startSecs' => $startSecs,
				      'endSecs' => $endSecs
			  	);
			
		    	    } 
		       }
		    echo json_encode($event_edit);

		}
else if($eventType=="Task"){

			$sql="select * from tps_task_details where id='".$req_id."'";

			$result=mysql_query($sql) or die(mysql_error());
			$row = mysql_fetch_assoc($result);

			$popupHtml = genTaskPopup($row);
			$startSecs = getSec($_REQUEST['start']);
			$endSecs = getSec($_REQUEST['end']);
			$event_edit = array(
			      'popupHtml' => $popupHtml,
			      'startSecs' => $startSecs,
			      'endSecs' => $endSecs
			  	);
			echo json_encode($event_edit);

		}
else{
			$eventId = $_REQUEST['id'];
			$sql="SELECT  * 
				FROM tps_events 
				WHERE id =".$eventId;
			$result=mysql_query($sql) or die(mysql_error());
			$row = mysql_fetch_assoc($result);

			$popupHtml = genPopup($row);
			$startSecs = getSec($_REQUEST['start']);
			$endSecs = getSec($_REQUEST['end']);
			$event_edit = array(
			      'popupHtml' => $popupHtml,
			      'startSecs' => $startSecs,
			      'endSecs' => $endSecs
			  	);
			echo json_encode($event_edit);
		}
		
      }
 
}

function getSec($Date){
	$startHours = (int)date('H',strtotime($Date));
	$startMins = (int)date('i',strtotime($Date));
	$startSecs = ($startHours*60*60)+($startMins*60);
	return $startSecs;
}
function genTaskPopup($row){

	$whos=explode('-',$row['who']);
	$who=$whos[1];

	$html = '<div class="box"><div class="box-header"><span class="title">Task Details</span></div>
		<div class="box-content" align="center"><table class="table table-normal"><tbody>
		  <tr><td>Contact Activity </td><td>'.$row['contact_activity'].'</td></tr>
		  <tr><td>Who </td><td>'.$who.'</td></tr>
		  <tr><td>Contact Reason </td><td>'.$row['contact_reason'].'</td></tr>
		  <tr><td>Assigned To </td><td>'.$row['assigned_tousers'].'</td></tr>
 		  <tr><td>Start Date</td><td>'.date('m-d-Y h:i A',strtotime($row['start_date'])).'</td></tr>
		  <tr><td>Due Date </td><td>'.date('m-d-Y h:i A',strtotime($row['due_date'])).'</td></tr>
		  <tr><td>Task Status </td><td>'.$row['task_status'].'</td></tr>
		  <tr><td>Comments </td><td>'.$row['comments'].'</td></tr>
		  <tr><td>Created By</td><td>'.getEventCreator($row['createdby']).'</td></tr>
		  <tr><td>Created Date</td><td>'.date('m-d-Y h:i A',strtotime($row['createddate'])).'</td></tr>
		  <tr><input type="hidden" value='.$row['id'].' id="event-id-hd"></tr>
			<tr>
				<td colspan="2">
					<div class="buttons" align="right">
						<a id="ok-event-edit" class="btn btn-green" > Ok </a>
						<a id="cancel-event-edit" class="btn btn-default" > Cancel </a>
					</div>				
				</td>
			</tr>
		<tr>';
		$html .= '</tr></tbody></table></div></div><br>';

	return $html;

}
function genPopup($row){
	$startDate = date('M d, Y h:i:s A',strtotime($row['start']));
	
	$titlesplit = explode("-", $row['title']);
	$titlesplit[0]=trim($titlesplit[0]);
	$titlesplit[1]=trim($titlesplit[1]);

	$appttype=$row['appttype'];

	$s=$row['start'];
	$s1=$row['end'];

	$p=explode("-",$s);
	$a=explode(" ",$p[2]);
	
	$dt=$p[1]."/".$a[0]."/".$p[0];

	$se=explode(" ",$s1);

	$dt1=date('M d, Y', strtotime($dt));

	$st=date('h:i a',strtotime($a[1]));
	$ed=date('h:i a',strtotime($se[1]));

	$invdate=$dt1." ".$st." - ".$ed;

	$html = '<div class="box"><div class="box-header"><span class="title">Appointment Details</span></div>
		<div class="box-content" align="center"><table class="table table-normal"><tbody>
		  <tr><td>Name </td><td>'.ucfirst($titlesplit[0]).'</td></tr>
		  <tr><td>Address </td><td>'.ucfirst($titlesplit[1]).'</td></tr>
		  <tr><td>Type </td><td>'.ucfirst($appttype).'</td></tr>
		  <tr><td>Date </td><td>'.$invdate.'</td></tr>
		  <tr><td>Created By</td><td>'.getEventCreator($row['createdby']).'</td></tr>
		  <tr><input type="hidden" value='.$row['id'].' id="event-id-hd"></tr>
			<tr>
				<td colspan="2">
					<div class="buttons" align="right">
						<a id="ok-event-edit" class="btn btn-green" > Ok </a>
						<a id="cancel-event-edit" class="btn btn-default" > Cancel </a>
					</div>				
				</td>
			</tr>
		<tr>';
	if($row['remarks'] != ''){
		$html .= $row['remarks'];
	}
	$html .= '</tr></tbody></table></div></div><br>';

	return $html;
}

function genTrainingPopup($row){
	$esubtype='';
	$cur_userid=get_session('LOGIN_USERID');

	if($row=="0")
	{
		$cur_userid=get_session('LOGIN_USERID');

		$mlist=$cur_userid;
		$trnid="0";

		/*$startdate = date('m/d/Y',strtotime($_REQUEST['sdate']));
		$starttime = date('h:i A',strtotime($_REQUEST['start']));
		$endtime = date('h:i A',strtotime($_REQUEST['end']));*/

		$startdate = $_REQUEST['sdate'];
		$starttime = $_REQUEST['starttime'];
		$endtime = $_REQUEST['endtime'];
		
		$descp="";
		$tseq_id="";	
	}else{
		
		$rid=$row['id'];
		$topicidslist=$row['topicids'];
		$trnid=$row['trainingname']; //echo $trnid;
		$esubtype=$row['trainingsubname']; //echo $esubtype; //exit;
		$mlist="";
		$tseq_id=$row['trainingseqid'];		

$re=mysql_query("select receiveruserid from tps_training_details where trainingname='".$trnid."' and trainingseqid='".$tseq_id."'") or die(mysql_error());
		
		$i=0;
		while($r=mysql_fetch_array($re))
		{
			$mr[$i]=$r['receiveruserid'];
			$i++;
		}
		$mlist=implode(",",$mr);
		$startDate = $row['starttime'];
		$descp=$row['trainingdesc'];

		$attachmentslist="";

		if($row['attachments']!="")
		{
			$attachmentslist='<ul class="attachment-list pull-left list-unstyled">';
				
			$imgid=array_values(array_filter(explode(',',$row['attachments'])));

			for($j=0;$j<count($imgid);$j++)
			{
				if($imgid[$j]!="")
				{
					$upimgres=mysql_query("select filepath from attachments where id='".$imgid[$j]."'") or die(mysql_error());

					$upimgr=mysql_fetch_array($upimgres);
					if($upimgr['id']!=""){
					$attachmentslist.='<li>
			<a href="download.php?type=download&filename='.$upimgr['filepath'].'" class="attached-file inline">
				<i class="icon-file-alt bigger-110 middle"></i>
				<span class="attached-name middle">'.basename($upimgr['filepath']).'</span>
			</a>
			<span class="action-buttons inline">
				<a href="download.php?type=download&filename='.$upimgr['filepath'].'"><i class="icon-download-alt bigger-125 blue"></i>
				</a>
			</span>
			</li>';
					}
				}
		
			}
			$attachmentslist.='</ul>';
		}

	}
	$html="";
	if($row==0)
	{
		$s=0;
		$html .= '<div class="box"><div class="box-header"><span class="title">Create New Event</span></div>
<div class="msg"  style="margin-left:150px;margin-top:10px;display:none;"></div><br><br>
		<div class="box-content" align="center" >
		<table style="width:100%; border-spacing: 8px; border-collapse: separate;" id="edit-drop-event-dialog-table">
		<tbody>
			<tr>
			    <td>Event Type/Topic </td>
			    <td>
	<select id="edit-training-dialog-trainingname" tabindex="1" multiple="multiple" class="populate placeholder" placeholder="Select Event Type/Topic">'.getEventTypeTopic($s).'</select>
			    </td>
		  	</tr>
			<tr>
			    <td valign="top">Description </td>
			    <td>
		<textarea tabindex="2" id="edit-training-dialog-desc" cols="46" rows="2" style="width: 368px; height: 48px;"></textarea>
			    </td>
		  	</tr>
<tr>
			    <td valign="top">Attachment</td>
			    <td>

<div id="queue" style="
	border: 1px solid #E5E5E5;
	height: 60px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 100%;">Drag & Drop Files Here</div>
		<input id="file" name="file" type="file" multiple="false">
		<div id="output" style="display:none;"></div>
		<input type="hidden" name="upimgids" id="upimgids">

</div>
 </td>
		  	</tr>
		  	<tr>
			    <td>Invite Members </td>
	  		        <td>
 <select id="memberslist1" style="width:450px;" tabindex="3" name="memberslist1" multiple="multiple">'.getTeamMembersWithGroup2($mlist).'</select>
				</td>
		  	</tr>
			<tr>
		  	<td style="width: 100px;">Date: </td>
		    <td>
		    	<span class="group">
			<input type="text" id="edit-training-dialog-start-date" tabindex="4" placeholder="Date" value="'.$startdate.'">
		      	<input type="text" id="training-time-from" tabindex="5" placeholder="Start Time" value="'.$starttime.'">
			<input type="text" id="training-time-to" tabindex="6" placeholder="End Time" value="'.$endtime.'">
		      </span>
		    </td>
		  </tr>
			<tr>
				<td colspan="2">
					<div class="buttons">
						<a id="save-training-edit" tabindex="7" class="btn btn-blue"> Save  </a>
						<a id="cancel-event-edit" tabindex="8" class="btn btn-default" >Cancel</a>
					</div>				
				</td>
			</tr>
		</tbody>
	</table></div></div>';
	}else{
		$html .= '<div style="height:440px; padding:8px; overflow-y:auto;"><div class="box"><div class="box-header"><span class="title">Edit Event Details</span></div>
		<div class="box-content" align="center">
		<table style="width: 600px; border-spacing: 8px; border-collapse: separate;" id="edit-drop-event-dialog-table">
		<tbody>
			<tr>
			    <td>Event Type/Topic </td>
			    <td>
<select id="edit-training-dialog-trainingname" multiple="multiple" tabindex="1" class="populate placeholder" placeholder="Select Event Topic">
			'.getEventTypeTopicListByIds($topicidslist).'			
				</select>
			    </td>
		  	</tr>
			<tr>
			    <td valign="top">Description </td>
			    <td>
	<textarea id="edit-training-dialog-desc1" tabindex="2" cols="46" rows="2" style="width: 368px; height: 48px;">'.$descp.'</textarea>
			    </td>
		  	</tr>
<tr>
			    <td valign="top">Attachment</td>
			    <td>

<div id="queue1" style="border: 1px solid #E5E5E5;
	height: 40px;
	overflow: auto;
	margin-bottom: 10px;
	width: 100%;">Drag & Drop Files Here</div>
		<input id="file1" name="file1" type="file" multiple="false">
		<div id="output1" style="display:none;"></div>
		<input type="hidden" name="upimgids1" id="upimgids1">

</div>
<br>';

if($row['attachments']!="") 
{ 
	$imgid=array_values(array_filter(explode(',',$row['attachments'])));

	for($j=0;$j<count($imgid);$j++)
	{
		if($imgid[$j]!="")
		{
			$upimgres=mysql_query("select * from attachments where id='".$imgid[$j]."'") or die(mysql_error());

			$upimgr=mysql_fetch_array($upimgres);

			if($upimgr['id']!=""){

			$html .= '<div id="g'.$upimgr['id'].'"><i class="icon-file-alt bigger-110 middle"></i> '.basename($upimgr['filepath']);

$html .= ' &nbsp; <a href="#" style="color:brown;" onclick="javascript:return deleteupimg('.$upimgr['id'].', '.$row['id'].');"><i class="icon-trash"></i> Delete</a>';
		$html .= "</div>";
			}

		}
	}


 }

 $html.='</td>
		  	</tr>
			<tr>
			    <td>Invite Members </td>
	  		        <td>
 <select id="memberslist1" style="width:450px;" tabindex="3" name="memberslist1" multiple="multiple">'.getTeamMembersWithGroup2($mlist).'</select>
				</td>
		  	</tr>
			<tr><td></td>
				<td colspan="2">
					<input type="hidden" id="curtrainingseqid1" value="'.$tseq_id.'" />
					<div class="buttons">
						<a id="save-training-edit1" tabindex="4" class="btn btn-blue"> Update Details </a>
						<a id="cancel-event-edit" tabindex="5" class="btn btn-default" >Cancel</a>
						<a id="delete-event-edit" tabindex="6" class="btn btn-red">Delete this Event</a>
					</div>				
				</td>
			</tr>
		</tbody>
	</table></div></div>';
	}
	if($row!="0")
	{
		$s=$row['starttime'];
		$s1=$row['endtime'];

		$p=explode("-",$s);
		$a=explode(" ",$p[2]);
	
		$dt=$p[1]."/".$a[0]."/".$p[0];

		$se=explode(" ",$s1);

		$dt1=date('M d, Y', strtotime($dt));

		$st=date('h:i a',strtotime($a[1]));
		$ed=date('h:i a',strtotime($se[1]));

		$invdate=$dt1." ".$st." - ".$ed;

		$tcnt=0;
		$yescnt=0;
		$maybecnt=0;
		$nocnt=0;

		$tid=$row['trainingname'];
		$tsid=$row['trainingseqid'];

$cntres=mysql_query("SELECT isaccepted,count(*) as cnt FROM `tps_training_details` where trainingseqid='".$tsid."' and delete_flag='0' group by isaccepted");

$tcnt=0;
$yescnt=0;
$nocnt=0;
$maybecnt=0;

while($cntr=mysql_fetch_array($cntres))
{

	if($cntr['isaccepted']=="YES")
	{
		$yescnt=$cntr['cnt'];
	}
	if($cntr['isaccepted']=="NO")
	{
		$nocnt=$cntr['cnt'];
	}

	if($cntr['isaccepted']=="MAYBE")
	{
		$maybecnt=$cntr['cnt'];
	}

	$tcnt=$tcnt+$cntr['cnt'];
	
}

$ares=mysql_query("select count(*) AS atdcnt from tps_training_details where isattended='1' and trainingseqid='".$tsid."' and delete_flag='0'");
	$ar=mysql_fetch_array($ares);
	$acnt=$ar['atdcnt'];

	if($acnt>0)
	{
		$acnt=$acnt-1;
	}else{
		$acnt=0;
	}

$re=mysql_query("select creatoruserid,receiveruserid,isaccepted,isattended from tps_training_details where trainingseqid='".$tsid."' and delete_flag='0'") or die(mysql_error());
		
	$cur_userid=get_session('LOGIN_USERID');

	$i=0;
	$rmkstatus="<div style='height:170px; overflow-y: auto;'><table style='border-spacing: 3px; border-collapse: separate;'>";
	$attended="<div style='height:170px; overflow-y: auto;'><table style='border-spacing: 3px; border-collapse: separate;'>";

	
	$rmkstatus.="<tr>
			<td><img src='images/yes.png' width='20px' height='20px'/></td>
			<td><b>".getTeamMembersByUserId($cur_userid)."</b></td>
		     </tr>";
	
	while($r=mysql_fetch_array($re))
	{
		if($cur_userid!=$r['receiveruserid'])
		{
			$rmkstatus.="<tr>";
			if($r['isaccepted']=="YES")
			{
				$rmkstatus.="<td><img src='images/yes.png' width='20px' height='20px' /></td>";
			}else if($r['isaccepted']=="MAYBE"){
				$rmkstatus.="<td><img src='images/maybe.png' width='20px' height='20px' /></td>";
			}else if($r['isaccepted']=="NO"){
				$rmkstatus.="<td><img src='images/no.png' width='20px' height='20px' /></td>";
			}else{
				$rmkstatus.="<td><img src='images/waiting.jpeg' width='20px' height='20px' /></td>";
			}
			$rmkstatus.="<td>".getTeamMembersByUserId($r['receiveruserid'])."</td></tr>";
		
			/*if($r['isaccepted']=="YES" || $r['isaccepted']=="MAYBE" || $r['isaccepted']=="NO")
			{*/
				if($r['isattended']=="1")
				{
					$attended.="<td><input type='checkbox' name='isatd' value='".$r['receiveruserid']."' checked ></td>";	
				}
				else if($r['isattended']=="0")
				{ 
					$attended.="<td><input type='checkbox' name='isatd' value='".$r['receiveruserid']."' ></td>";	
				}else{
					$attended.="<td><input type='checkbox' name='isatd' value='".$r['receiveruserid']."' ></td>";	
				}
			
				$attended.="<td>".getTeamMembersByUserId($r['receiveruserid'])."</td></tr>";
			/*}else{
				$attended.="<td> </td></tr>";
			}*/
		}
		$i++;
	}
	$rmkstatus.="</table></div>";
	$attended.="</table><br><div class='buttons' align='center'>
					<a id='save-TrainingAttend-edit' class='btn btn-blue'> Save </a>
					 <a id='cancel-event-edit' class='btn btn-default' >Cancel</a>
					</div></div></div>";

		$html .='<div class="box"><div class="box-header"><span class="title">Event Details</span></div>
		<div class="box-content" align="center"><table style="width: 100%; border-spacing: 8px; border-collapse: separate;" border="0">';
		$html .='<tr>
		<td colspan="2">'.$row['trainingname'].' - '.$row['trainingsubname'].' - '.$row['trainingdesc'].' on <b> '.$invdate.'</b>';

		if($attachmentslist!=""){
			$html .='<br> <b>Attachements</b> <br>'.$attachmentslist;
		}
		
		$html .='</td></tr>';
		$html .='<tr><td valign="top" width="50%"><b>Invited Members ('.$tcnt.')</b> <br> Yes : '.$yescnt.'  Maybe : '.$maybecnt.'  No : '.$nocnt.' <br> <br>'.$rmkstatus.'</td>';
		$html .='<td width="50%" valign="top"><b>Attended Members ('.$acnt.') </b><br><br><br>'.$attended.'</td></tr>';
		$html .='</table>';


$urres=mysql_query("select updremarks from tps_training_details where trainingseqid='".$tsid."' group by trainingseqid order by id desc") or die(mysql_error());
$updrow=mysql_fetch_array($urres);

		$updr=$updrow['updremarks'];
	
		if($updr!="")
		{
			$ur=explode("/",$updr);

			$html .='<b>Last Modifications : </b><div style="height:100px; overflow-y: auto;">';
			$html .='<table style="width: 100%; border-spacing: 5px; border-collapse: separate;" border="0">';
			$html .='<tr><td width="50%"><b>Previous Date/Time</b></td><td width="50%"><b>Remarks</b></td></tr>';
			
			if(count($ur)>1)
			{		
				for($k=0;$k<count($ur);$k++)
				{
					$iur=explode("@",$ur[$k]);
					$html .='<tr>';
					$html .='<td>'.$iur[0].'</td>';
					$html .='<td>'.$iur[1].'</td>';
					$html .='</tr>';
				}
			}else{
				$iur=explode("@",$ur[0]);
				$html .='<tr>';
				$html .='<td>'.$iur[0].'</td>';
				$html .='<td>'.$iur[1].'</td>';
				$html .='</tr>';
			}
			$html .='</table></div></br></div></br></br></div>';
		}	
 	}

	return $html;
}

function genTrainingAcceptPopup($r){

$tr=mysql_query("select trainingname from tps_training where id='".$r['trainingname']."'") or die(mysql_error());
$t=mysql_fetch_array($tr);

$s=$r['starttime'];
$s1=$r['endtime'];

$p=explode("-",$s);
$a=explode(" ",$p[2]);

$dt=$p[1]."/".$a[0]."/".$p[0];

$se=explode(" ",$s1);

$dt1=date('M d, Y', strtotime($dt));

$st=date('h:i a',strtotime($a[1]));
$ed=date('h:i a',strtotime($se[1]));

$invdate=$dt1." ".$st." - ".$ed;

$appttype=$_REQUEST['appttype'];

$seqid=$r['id'];

$tseqid=$r['trainingseqid'];

$attachmentslist="";

if($r['attachments']!="")
{
	$attachmentslist='<ul class="attachment-list pull-left list-unstyled">';
	$imgid=array_values(array_filter(explode(',',$r['attachments'])));
	
	for($j=0;$j<count($imgid);$j++)
	{
		if($imgid[$j]!="")
		{
			$upimgres=mysql_query("select filepath from attachments where id='".$imgid[$j]."'") or die(mysql_error());

			$upimgr=mysql_fetch_array($upimgres);

			if($upimgr['id']!=""){
			$attachmentslist.='<li>
	<a href="download.php?type=download&filename='.$upimgr['filepath'].'" class="attached-file inline">
		<i class="icon-file-alt bigger-110 middle"></i>
		<span class="attached-name middle">'.basename($upimgr['filepath']).'</span>
	</a>
	<span class="action-buttons inline">
		<a href="download.php?type=download&filename='.$upimgr['filepath'].'"><i class="icon-download-alt bigger-125 blue"></i>
		</a>
	</span>
	</li>';
			}
		}

	}
	$attachmentslist.='</ul>';
}

	$html = '<div class="box"><div class="box-header"><span class="title">Event Details</span></div>
		<div class="box-content" align="center">
	<table style="width: 100%; border-spacing: 8px; border-collapse: separate;" id="edit-drop-event-dialog-table">
		<tbody>
			<tr>
		<td valign="top" colspan="2">'.$r['trainingname'].' - '.$r['trainingsubname'].' - '.$r['trainingdesc'].' - <b>'.$invdate.'</b>';
		if($attachmentslist!=""){
			$html .='<br> <b>Attachements</b> <br>'.$attachmentslist;
		}
			$html .='</td>
			</tr>
			<tr>
			    <td>
			<input type="hidden" name="appttype" id="edit-drop-event-dialog-appttype-remarks" value="'.$appttype.'" />
			<input type="hidden" name="seqid" id="edit-drop-event-dialog-seqid" value="'.$seqid.'" />
			<input type="hidden" name="tseqid" id="edit-drop-event-dialog-tseqid" value="'.$tseqid.'" />
			    </td>
			    <td>';

	if($r['isaccepted']=="YES"){
		$html.='<button id="yes" class="btn btn-green" value="YES">YES</button>
		<!--<input type="radio" name="isaccept" id="isaccept1" value="YES" checked /> YES-->';
	}else{
		$html.='<button id="yes" class="btn btn-default" value="YES">YES</button>
		<!--<input type="radio" name="isaccept" id="isaccept1" value="YES" /> YES-->';
 	} 

	if($r['isaccepted']=="MAYBE"){
		$html.='<button id="maybe" class="btn btn-gray" value="MAYBE">MAYBE</button>
			<!--<input type="radio" name="isaccept" id="isaccept2" value="MAYBE" checked /> MAY BE-->';
	}else{
		$html.='<button id="maybe" class="btn btn-default" value="MAYBE">MAYBE</button>
			<!--<input type="radio" name="isaccept" id="isaccept2" value="MAYBE"/> MAY BE-->';
	}

	if($r['isaccepted']=="NO"){
		$html.='<button id="no" class="btn btn-red" value="NO">NO</button>
			<!--<input type="radio" name="isaccept" id="isaccept3" value="NO" checked /> NO-->';
	}else{
		$html.='<button id="no" class="btn btn-default" value="NO">NO</button>
			<!--<input type="radio" name="isaccept" id="isaccept3" value="NO"/> NO-->';
	}
			$html.='</td>
		  	</tr>
			<tr>
			    <td valign="top">Remarks : </td>
			    <td><textarea id="edit-drop-event-dialog-remarks" rows="2" cols="40"></textarea></td>
		  	</tr>
		 	<tr>
				<td colspan="2">
					<div class="buttons">
						<a id="save-TrainingAccept-edit" tabindex="7" class="btn btn-blue" > Save Details </a>
						<a id="cancel-event-edit" tabindex="8" class="btn btn-default" >Cancel</a>
					</div>				
				</td>
			</tr>
		</tbody>
	</table></div></div><br>';
	return $html;
}

function genPopupRemarks($eventId){

$res=mysql_query("select createdby from tps_events where id='".$eventId."' and delete_flag='0'") or die(mysql_error());
$r=mysql_fetch_array($res);

$cur_userid=get_session('LOGIN_USERID');

if($r['createdby']==$cur_userid)
{
	$html = '<div class="box"><div class="box-header"><span class="title">Remarks for the Action</span></div>
		<div class="box-content" align="center">
	<table style="width: 650px; border-spacing: 8px; border-collapse: separate;" id="edit-drop-event-dialog-table">
		<tbody>
			<tr>
			    <td>Remarks : </td>
			    <td><textarea id="edit-drop-event-dialog-remarks" tabindex="2" rows="2" cols="40"></textarea></td>
		  	</tr>
		  
			<tr>
				<td colspan="2">
					<div class="buttons">
						<a id="save-event-edit" tabindex="2" class="btn btn-blue"> Save </a>
						<a id="cancel-event-edit" tabindex="3" class="btn btn-default" >Cancel</a>
					</div>				
				</td>
			</tr>
		</tbody>
	</table></div></div><br>';
	return $html;
}
else
{
	$html = '<div class="box"><div class="box-header"><span class="title">Information</span></div>
		<div class="box-content padded" align="center">
		<div style="height:200px; text-align:justify;" align="center">
		<br> This event created by your Team Member. You are not authorized to edit/delete this event.<br> 
		<br><br>
		<a id="cancel-event-edit" tabindex="1" class="btn btn-default" >Cancel</a></div></div></div><br>';

	return $html;
}

}

function genTrainingPopupRemarks($eventId){

$tseqid=$_REQUEST['tseqid'];

$res=mysql_query("select creatoruserid,createdby from tps_training_details where trainingseqid='".$tseqid."' and id='$eventId' and delete_flag='0'") or die(mysql_error());
$r=mysql_fetch_array($res);

$cur_userid=get_session('LOGIN_USERID');

if($r['creatoruserid']==$cur_userid)
{
	$html = '<div class="box"><div class="box-header"><span class="title">Remarks for the Action</span></div>
		<div class="box-content" align="center">
	<table style="width: 650px; border-spacing: 8px; border-collapse: separate;" id="edit-drop-event-dialog-table">
		<tbody>
			<tr>
			    <td>Remarks : </td>
			    <td><textarea id="edit-drop-event-dialog-remarks" tabindex="1" rows="2" cols="40"></textarea></td>
		  	</tr>
		  
			<tr>
				<td colspan="2">
					<div class="buttons">
						<a id="save-event-edit" tabindex="2" class="btn btn-blue"> Save </a>
						<a id="cancel-event-edit" tabindex="3" class="btn btn-default" >Cancel</a>
					</div>				
				</td>
			</tr>
		</tbody>
	</table></div></div><br>';
	return $html;
}
else
{
	$html = '<div class="box"><div class="box-header"><span class="title">Information</span></div>
		<div class="box-content padded" align="center">
		<div style="height:200px; text-align:justify;" align="center">
		<br><br><br> This event created by your Team Member. You are not authorized to edit/delete this event. <br> 
		<br><br>
		<a id="cancel-event-edit" class="btn btn-default" tabindex="1" >Cancel</a></div></div></div><br>';

	return $html;
}

}
?>
