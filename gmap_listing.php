<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "gmap_listing.php";
$page_title = $site_name." -  Google Map Address Listing";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?> 

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAlwR1DxEprXXiw0_sCUwOHMGOBQUVyJDo&sensor=false" type="text/javascript"></script>

<?php

$Id=get_session('LOGIN_ID');
$page=$_REQUEST['page'];

$tab=1;

if($page=="leadcard")
{
	$lids=$_REQUEST['leadid'];

	//echo "<br> Lead Ids: ".$lids;

	if($lids!='0')
	{
		$query = "SELECT title1,fname1,lname1,title2,fname2,lname2,add_line1,add_line2,city,state FROM tps_lead_card where uid = '$Id' and delete_flag='0' and FIND_IN_SET( `leadid`, '$lids' ) order by id desc";

	}else{

		$query = "SELECT title1,fname1,lname1,title2,fname2,lname2,add_line1,add_line2,city,state FROM tps_lead_card where uid = '$Id' and delete_flag='0' order by id desc";

	}
	$tab=1;

}else if($page=="customer"){

	$lids=$_REQUEST['leadid'];

	//echo "<br> Lead Ids: ".$lids;

	if($lids!='0')
	{
		$query = "SELECT title1,fname1,lname1,title2,fname2,lname2,add_line1,add_line2,city,state FROM tps_lead_card where uid = '$Id' and customer_flag='1' and delete_flag='0' and FIND_IN_SET( `leadid`, '$lids' ) order by id desc";

	}else{

		$query = "SELECT title1,fname1,lname1,title2,fname2,lname2,add_line1,add_line2,city,state FROM tps_lead_card where uid = '$Id' and customer_flag='1' and delete_flag='0' order by id desc";

	}
	$tab=1;

}else if($page=="campaign"){

	$lids=$_REQUEST['leadid'];

	if($lids!='0')
	{
	
	$query = "SELECT title,fname,lname,add_line,city,state FROM tps_campaign where uid = '$Id' and FIND_IN_SET( `id`, '$lids' ) order by id desc";

	}else{

	$query = "SELECT title,fname,lname,add_line,city,state FROM tps_campaign where uid = '$Id' and delete_flag='0' order by id desc";

	}

	$tab=2;
}

//echo "<pre>".$query."</pre>";

$result = mysql_query($query) or die(mysql_error());

$maparray=array();
$locs="";

$responseall="";

$i=0;
$okcnt=0;
while($row = mysql_fetch_array($result))
{
	if($tab==2)
	{	

		$lead_name=ltrim($row['title'],"Select") . ucfirst($row['fname'])." ".$row['lname'];
		$lead_address=$row['add_line'];
		if($row['city']!="")
		{
			if($lead_address!="")
			{
				$lead_address.=", ".$row['city'];
			}
			else
			{
				$lead_address.=$row['city'];
			}
		}

		if($row['state']!="")
		{
			if($lead_address!="" )
			{
				$lead_address.=", ".$row['state'];
			}
			else
			{
				$lead_address.=$row['state'];
			}
		}

	}else{

		$lead_name=ltrim($row['title1'],"Select") . ucfirst($row['fname1'])." ".$row['lname1'];

		if($row['fname2']!="")
			$lead_name.=" , ".ltrim($row['title2'],"Select") . ucfirst($row['fname2'])." ".$row['lname2']; 

		$lead_address=$row['add_line1'];
		if ( $row['add_line2'] != '' )
		{
			$lead_address.=" ".$row['add_line2'];
		}
		if($row['city']!="")
		{
			if($lead_address!="")
			{
				$lead_address.=", ".$row['city'];
			}
			else
			{
				$lead_address.=$row['city'];
			}
		}

		if($row['state']!="")
		{
			if($lead_address!="" )
			{
				$lead_address.=", ".$row['state'];
			}
			else
			{
				$lead_address.=$row['state'];
			}
		}
	}

	$url = "https://maps.google.com/maps/api/geocode/json?key=AIzaSyAlwR1DxEprXXiw0_sCUwOHMGOBQUVyJDo&address=".urlencode($lead_address)."&sensor=false";

//echo "<pre>".$url."</pre>";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$response = curl_exec($ch);
	curl_close($ch);

	$response_json = json_decode($response);
	
 	$responseall.="<pre> Lead Address : ".$lead_address."<br>".$response."</pre>";
	//exit;
	$lat = $response_json->results[0]->geometry->location->lat;
	$long = $response_json->results[0]->geometry->location->lng;

	$status=$response_json->status;

	if($status=="OK")
	{
		$title="<br><b>".$lead_name."</b> <br><br> ".$lead_address." <br><br>";

		$addr=$lead_address;

		$locs.="['$title', $lat, $long, '$addr', $i]".", ";

		$okcnt++;
	}
	$i++;
}

//echo "<b>All Responses</b><br><pre>".$responseall."</pre>";

$user_dispname=get_session('DISPLAY_NAME');
$user_logid=get_session('LOGIN_ID');
$url= $_SERVER['HTTP_REFERER'];
$log_desc= ucfirst($user_dispname)." requested $i address listing from $page page, and successfully listed $okcnt addresses. <b><a href=$url target=_blank >$url</a></b>";

tps_log_error(__INFO__, __FILE__, __LINE__, "Google Map Listing", $user_logid, $log_desc);

?>
<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>Google Map Address Listing&nbsp;&nbsp;
	<a class="btn btn-blue" href="#" onClick="javascript:window.close();" ><span>Close Window</span></a>
	</h3>
</div> 

      </div>
    </div>
  </div>

<div class="container">

<div class="box">

<div class="box-header"><span class="title">Google Map Address Listing</span>

</div>
<div class="box-content padded">
  <div id="map" style="width:100%; min-height: 480px;"></div>

  <script type="text/javascript">

/*  var locations = [
      ['Bondi Beach', -33.890542, 151.274856, 4],
      ['Coogee Beach', -33.923036, 151.259052, 5],
      ['Cronulla Beach', -34.028249, 151.157507, 3],
      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
      ['Maroubra Beach', -33.950198, 151.259302, 1]
    ];
*/

    var locations =[<?php echo $locs; ?>];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: new google.maps.LatLng(locations[0][1], locations[0][2]),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    var markerBounds = new google.maps.LatLngBounds();

    for (i = 0; i < locations.length; i++) {  

      mappingPoint = new google.maps.LatLng(locations[i][1], locations[i][2]);

      marker = new google.maps.Marker({
        position: mappingPoint,
        map: map
      });

      var content = '<div class="map-content" style="color:black;padding:8px;">' + locations[i][0] + ' <a href="http://maps.google.com/?daddr=' + locations[i][3] + '" target="_blank"><img src="images/directions.png" width="20px" height="20px"><b style="color:#27aae1;">Get Directions</b></a></div>';

      google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
   	 return function() {
	   map.setCenter(marker.getPosition());
     	   infowindow.setContent(content);
     	   infowindow.open(map,marker);
         };
      })(marker,content,infowindow));  

	markerBounds.extend(marker.getPosition());

    }

    map.fitBounds(markerBounds);

  </script>

  </div>
	
    </div>
	
   </div>

 </div> 
 </div>

<?php


include "lcas_footer.php";

?>
