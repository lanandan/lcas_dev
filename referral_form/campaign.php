<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");


validate_login();
$page_name = "Campaign.php";
$page_title = $site_name." Campaign";
$cur_page="Campaig";
$delete_flag='';
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
include "js/campaign.js";
?>

<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Campaign Listing&nbsp;&nbsp;<a class="btn btn-blue" href="add_campaign.php"><span>Add New Campaigns</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="createtask.php"><span>Create Task</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="create_event.php"><span>Create Event</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="#"><span>Email</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="gmap_listing.php?page=campaign"><span>Map It</span></a>	
&nbsp;&nbsp;<button  type="button" id="ddd"class="btn btn-blue" onclick="printpage()">Print</button></h3>
</div> 
<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
      </div>
    </div>
  </div>

<div class="container">

<div class="box">

<div class="box-header"><span class="title">Campaign Listing</span>

</div>
<div class="box-content">
<div id="dataTables"  style="overflow-x:scroll;">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive" id="click" >
<thead>
<tr>
	<th><div>Action</th></div>
	<th style="display:none;"><div>Name</th></div>
	<th style="display:none;"><div>Address</th></div>
	<th style="display:none;"><div>City,State,zip</th></div>
	<th style="display:none;"><div>Phone Num</th></div>
	<th style="display:none;"><div>Comments</th></div>
	<th style="display:none;"><div>Email</th></div>
	<th><div>Lead Type</th></div>
	<th><div>Lead SubType</th></div>
	<th><div>Campaign Name ("Referred By")</th></div>
	<th><div>City</th></div>
	<th><div>Dealer</th></div>
	<th><div>Start Date</th></div>
	<th><div>Expiration Date</th></div>
	<th><div>Active</th></div>
	<th><div>Forms</th></div>
</tr>
</thead>

<?php 
$loginid=get_session('LOGIN_ID');

$sql = "";

if(isset($_REQUEST['type']))
{
	if($_REQUEST['type']=="newtask")
	{
		$viewid=$_REQUEST['id'];
		$sql="select * from tps_campaign where uid = '$loginid' and id='$viewid' and delete_flag='0' order by id desc";
		$result=mysql_query($sql) or die(mysql_error());

	}else{
		$sql="select * from tps_campaign where uid = '$loginid' and delete_flag='0' order by id desc";
		$result=mysql_query($sql) or die(mysql_error());
	}
}
else{

$sql="select * from tps_campaign where uid = '$loginid' and delete_flag='0' order by id desc";
$result=mysql_query($sql) or die(mysql_error());

}

while($row = mysql_fetch_array($result))
{
$sql_select="select fname,lname from tps_users where userid='$row[lead_dealer]'";
$result_select = mysql_query($sql_select) or die(mysql_error());
$row1 = mysql_fetch_array($result_select);
?>

			<tr>
			  <td>
	<div class="btn-group">
	      <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button>
	      <ul class="dropdown-menu">
		 <li><a href="campaign_lead.php?id=<?php print $row['id'];?>"><img src="images/view.png" width="18px"/>&nbsp;&nbsp;View Campaign</a></li>
		 <li><a href="add_campaign.php?action=edit&lid=<?php print $row['id'];?>"><img src="images/edit.png" height="18px" width="18px"/>&nbsp;&nbsp;Edit Campaign</a></li>
		 <li class="divider"></li>
		 <li><a href="add_campaign.php?action=delete&lid=<?php print $row['id'];?>" onClick="return confirmDelete();"><img src="images/block.png" width="18px"/>&nbsp;&nbsp;Delete</a></li>
	      </ul>
	</div>

<!--<a href="campaign_lead.php?id=<?php print $row['id'];?>"><img src=images/view.png  title="View Campaign Leads" height="10px" width="20px"></a>&nbsp;|&nbsp; <a href="add_campaign.php?action=edit&lid=<?php print $row['id'];?>" title="Edit"><img src="images/edit.png"  title="Edit"/></a>&nbsp;|&nbsp;
 	<a href="add_campaign.php?action=delete&lid=<?php print $row['id'];?>" title="Delete" onClick="return confirmDelete();" > <img src="images/block.png" width="15px" class="key_image" title="Delete"/></a> -->

 </td>
 <td style="display:none;"><?php echo $row['title']." ".$row['fname']." ".$row['lname']; ?></td>
 <td style="display:none;"><?php echo $row['add_line']; ?></td>
 <td style="display:none;"><?php echo $row['city']." ".$row['state']." ".$row['zip']; ?></td>
 <td style="display:none;"><?php echo $row['phone1'].",".$row['phone2']; ?></td>
 <td style="display:none;"><?php echo $row['comments']; ?></td>
<td style="display:none;"><?php echo $row['email1']; ?></td>
			  <td><?php echo $row['lead_type']; ?></td>
				<td><?php echo $row['lead_subtype']; ?></td>
			  <td><?php echo $row['campaign']; ?></td>
			  <td><?php echo $row['city']; ?></td>
			  <td><?php echo ucfirst($row1['fname'])." ".ucfirst($row1['lname']);?></td>
			  <td><?php echo date('m/d/Y',strtotime($row['start_date'])); ?></td>
			  <td><?php echo date('m/d/Y',strtotime($row['expiration_date'])); ?></td>
			  <td><?php if($row['active']=='1'){echo "Yes"; } else { echo "No"; } ?></td>
<?php 
$ref="2_".$row['id'];

$sql1="select * from tps_lead_card where referred_by='$ref' and form_type='Event_Form' and delete_flag='0' ";
$result1=mysql_query($sql1) or die(mysql_error());
$r=mysql_num_rows($result1);

$sql2="select * from tps_lead_card where referred_by='$ref' and form_type='Door_Canvas_Form' and delete_flag='0'";
$result2=mysql_query($sql2) or die(mysql_error());
$r1=mysql_num_rows($result2);
?>
			  <td><a href="source/eventform.php?action=fill&lid=<?php print $row['id'];?>"><img src=images/ref.png  title='Event form'></a>(<?php echo $r;?>)&nbsp;&nbsp;&nbsp;&nbsp;<a href="source/doorcanvas.php?action=fill&lid=<?php print $row['id'];?>"><img src=images/ref.png  title='Marketing Analysis'></a>(<?php echo $r1;?>)</td>
			</tr>
<?php } ?>
		</table>

	 </div>  
     </div>
    </div>

	    
		<div class="row">
  <div class="col-md-4" style="width:50%">
    <div class="box">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Name & Address</span>
      </div>
      <div class="box-content padded">
       				<p id="name"></p>
				<p id="addr"></p>
				<p id="addr2"></p>
				<p id="phone"></p>
				<p id="email"></p>
        
      </div>
    </div>
  </div>
 
  <div class="col-md-4" style="width:50%">
    <div class="box">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Dates & Specifics</span>
      </div>
      <div class="box-content padded">
				<p id="ltype"></p>
				<p id="ldeal"></p>
				<p id="sdate"></p>
				<p id="edate"></p>
				<p id="notes"></p>

        </form>
      </div>
    </div>
  </div>
</div>


  </div>

<?php
include "lcas_footer.php";
?>

