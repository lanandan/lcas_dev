<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

	$page_name = "lead_listing.php";
	$page_title = $site_name." Lead Listing";
$cur_page="lead_list";
	
$qn_count_query = "select count(*) as total_rows from tps_lead_type";

	$qn_count_result = mysql_query($qn_count_query) or die(mysql_error('Fetch from Profile Count failed'));
	$total_records = mysql_result($qn_count_result, 0);
	mysql_free_result($qn_count_result);
	
	if ( $total_records <= 0 ) {
echo "record fetched 0 rows";
	//	header("Location: add_lead_type.php");
	exit;
	}

if ( $total_records > 0 ) {
		if ( isset($_GET["page"]) ) {
			$current_page = $_GET['page'];
		}
		else {
			$current_page = 1;
		}
		
		$records_per_page = $tps_records_per_page;  // get from global config files
		$upper_record = $current_page * $records_per_page;
		$lower_record = ($upper_record + 1) - $records_per_page;
	
		
		$pagin_str = get_pagination($total_records,$current_page,$records_per_page,$lower_record,$upper_record); 
		
		$lower_record--;
		
		if($upper_record > $total_records){$upper_record=$total_records;$records_per_page= $total_records-$lower_record;}
	
		
		$qn_list_query="SELECT a.id, a.lead_type, a.type_displayname,a.lead_subtype, a.subtype_displayname, a.status, a.script_id, a.createdby,UNIX_TIMESTAMP( a.createddate) as createddate, a.modifiedby, UNIX_TIMESTAMP( a.`modifieddate` ) as modifieddate, b.name FROM `tps_lead_type` a LEFT JOIN tps_scripts b on a.script_id = b.id order by a.modifieddate desc LIMIT $lower_record, $records_per_page";

	
		$result_list=mysql_query($qn_list_query) or die(mysql_error());
		
	}
	else {
		$pagin_str='';
	}



include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

                                   

?>
<script type="text/javascript">
function confirmendis(var1) 
{
	
	if(var1==0)
		var agree=confirm("Are you sure you want to Disable the lead type?");
	else
		var agree=confirm("Are you sure you want to Enable the lead type?");
	if (agree)
		return true ;
	else
		return false ;
}
</script>
<div class="main-content">
  <div class="container">
    <div class="row">

      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
            <i class="icon-magic"></i>
            Script List
          </h3>
          <h5>
            <span>
          
            </span>
          </h5>
        </div>

        <ul class="list-inline pull-right sparkline-box">

          <li class="sparkline-row">
            <h4 class="blue"><span>Orders</span> 847</h4>
            <div class="sparkline big" data-color="blue"><!--28,20,6,24,15,6,4,5,29,10,11,3--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="green"><span>Reviews</span> 223</h4>
            <div class="sparkline big" data-color="green"><!--28,7,26,14,26,3,8,22,9,14,9,20--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="red"><span>New visits</span> 7930</h4>
            <div class="sparkline big"><!--3,29,15,7,4,22,3,7,23,25,27,9--></div>
          </li>

        </ul>
      </div>
    </div>
  </div>

  <div class="container padded">
    <div class="row">

      <!-- Breadcrumb line -->

      <div id="breadcrumbs">
        <div class="breadcrumb-button blue">
          <span class="breadcrumb-label"><i class="icon-home"></i> Home</span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>

         <div class="breadcrumb-button">
          <span class="breadcrumb-label">
            <i class=" icon-globe"></i> Admin
          </span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>
           
           

        <div class="breadcrumb-button">
          <span class="breadcrumb-label">
            <i class="icon-magic"></i> Lead Type List
          </span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>
      </div>
    </div>
  </div>
 <div class="container">
<div class="box">
   <div class="box-header">
     <span class="title">Lead Type List</span>
   </div>
   <div class="box-content">
<br /><br />
 <form action="scripts_listing.php" method="post" name="scripts_listing">
    <table width="100%" border="0" cellspacing="0"  cellpadding="5" class="display_table" style="overflow:auto;">
        <tr  >
        <td  align="left" colspan="3" style="padding-left:5%;"><a class="btn btn-blue" href="add_lead_type.php"><span>Add New Lead Type</span></a></td>
        <td  colspan="6" align="right" style="padding-right:5%;"> <?php echo $pagin_str; ?></td>
        </tr>

        <tr>
        <td width="10%" align="center"><b>Action</b></td>
        <td width="20%"><b>Lead Type</b></td>
        <td width="20%"><b>Lead SubType</b></td>
        <td width="5%" align="center"><b>Status</b></td>
       	<td width="15%" style="text-align:center;"><b>Script Name</b></td>
        <td width="15%" align="center"><b>Created</b></td>
        <td width="15%" align="center"><b>Modified</b></td>
  
        </tr>	

		<?php
        while($result=mysql_fetch_array($result_list)) {	
        ?>

        <tr style="padding:0.5%";>
        <td align="center">
        <a href="add_lead_type.php?action=edit&l_id=<?php print $result['id'];?>" title="Edit"><img src="../images/edit.png"  title="Edit"/></a>
        &nbsp;|&nbsp;
 <a href="add_lead_type.php?action=enable&l_id=<?php print $result['id'];?>" title="Delete" onClick="return confirmendis(<?php print $result['status'] ?>);" > <img src="../images/block.png" width="15px" class="key_image" title="Delete"/></a>        </td>
       
 
        
        <td  align="left"><?php print $result['type_displayname']."  ( ".$result['lead_type']." )"; ?></a></td>
        <td  align="left"><?php print $result['subtype_displayname']."  ( ".$result['lead_subtype']." )"; ?></td>

  
        <td align="center">
        <?php 
			if ( $result['status'] == 0 ) {
			echo '<img src="../images/checked.gif"  title="Active" />';
			}
			else {
			echo '<img src="../images/cancel.png"  title="Active" />';
			}
        ?>		</td>
	<?php 
		if($result['name']==''){
 			echo ' <td  align="center">Not created </td>';
		} else { 
                 echo '<td  align="center"><a href="test_questionnaire.php?qn_id='. $result['script_id'].'" style="color:#409392;">'. $result['name']. ' </a></td>';
		}
	?>
                 <td  align="center"><?php print display_time_diff_format($result['createddate'],1); ?></a></td>
         <td  align="center"><?php print display_time_diff_format($result['modifieddate'],1); ?></a></td>
        </tr>

		<?php
        } /* while closing */
        ?>


    <tr>
        <td  align="left" colspan="3" style="padding-left:5%;"><a class="btn btn-blue" href="add_lead_type.php"><span>Add New Lead Type</span></a></td>
        <td  colspan="6" align="right" style="padding-right:5%;"> <?php echo $pagin_str; ?></td>
    </tr>

	</table>
</form>

   </div>
 </div>
 </div>

<?php

include "lcas_footer.php";
?>
