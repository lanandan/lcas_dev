<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");

validate_login();
$page_name = "add_training.php";
$page_title = $site_name." Add /Edit Training";
$cur_page="configuration";

$tname='';
$manager1='';
$manager2='';
$status='';
$id='';

if(request_get('action')){
	if ( request_get('action') == "do"  )
	{
		$flag = true;
		$tname=isset($_POST['tname'])?safe_sql_nq(trim($_POST['tname'])):'';
		$status=isset($_POST['status'])?trim($_POST['status']):'';

		$timestamp =  time();

		$id = request_get('id');
		if ($status == '') {$status='0';}
		if ($id > 0)
		{
			//update databse
			$sql= "update tps_training set ".
				" trainingname = '". $tname ."', ".
				" status = '". $status ."', ".
				" createdby = '". get_session('DISPLAY_NAME') ."', ".
				" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
				" modifiedtime = '". $timestamp ."' ".
				" where id = '".$id."' " ;
		}
		else {
			$sql= "insert into tps_training set ".
				" trainingname = '". $tname ."', ".
				" status = '". $status ."', ".
				" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
				" modifiedtime = '". $timestamp ."', ".
				" createdby = '". get_session('DISPLAY_NAME') ."', ".
				" createdtime = '". $timestamp ."' ";			
		}	
		mysql_query($sql) or die(mysql_error());

		$url = "Location: training_listing.php";
		header($url);
		exit();	
	}
	if ( request_get('action') == "edit"  )
	{
		$id = request_get('id');
		$sql_qry = "select * from tps_training where id = $id ";

		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$tname=$row['trainingname'];
			$status=$row['status'];			
		
		}
	}
	if ( request_get('action') == "delete"  )
	{
		$id = request_get('id');
		$sql_qry = "delete from tps_training where id = '".$id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$url = "Location: training_listing.php";
		header($url);
		exit();	
	}
}
	
include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );		

?>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Training &nbsp;&nbsp;<a class="btn btn-blue" href="training_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit Training</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" action="add_training.php" method="POST" id="add_training" name="add_training">
          <div class="padded">
            <div class="form-group">
              <label class="control-label col-lg-2">Training Name*</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="tname" size="128" id="name" title="Training Name" value="<?php echo $tname;?>"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="status" checked="checked" id="iradio1" value="$status"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>


	    <input type="hidden" name="action" value="do" />
	    <input type="hidden" name="id" value="<?php echo $id;?>" />

          <div class="form-actions">
            <button type="submit" class="btn btn-blue">Save changes</button>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='training_listing.php'">Cancel</button>
          </div>
        </form>
<br/><br/>
   </div>
 </div>
 </div>

<?php
include "lcas_footer.php";
?>
