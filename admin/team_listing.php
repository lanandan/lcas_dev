<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "team_listing.php";
$page_title = $site_name." Team Listing";
$cur_page="configuration";
	
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<script type="text/javascript">
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete the Team?");
	if (agree)
		return true ;
	else
		return false ;
}
</script>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Team Listing&nbsp;&nbsp;<a class="btn btn-blue" href="add_team.php"><span>Add Team</span></a></h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Team Listing</span></div>
<div class="box-content">
<div id="dataTables">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
<thead>
<tr>
  <th><div>Action</div></th>
  <th><div>Team Name</div></th>
  <th><div>Manager1</div></th>
  <th><div>Manager1</div></th>
  <th><div>Status</div></th>
  <th><div>Modified By</div></th>
  <th><div>Modified Time</div></th>
</tr>
</thead>
<tbody>
<?php
$sql_qry = "select * from tps_team order by modifiedtime desc";

$result_list = mysql_query($sql_qry) or die(mysql_error());
while($result=mysql_fetch_array($result_list)) {	
$sql_select2="select * from tps_users where username='$result[manager1]'";
$result_select2 = mysql_query($sql_select2) or die(mysql_error());
$row1 = mysql_fetch_array($result_select2);


$sql_select="select * from tps_users where username='$result[manager2]'";
$result_select = mysql_query($sql_select) or die(mysql_error());
$row2 = mysql_fetch_array($result_select);

?>
<tr>
<td>
        <a href="add_team.php?action=edit&lid=<?php print $result['id'];?>" title="Edit"><img src="../images/edit.png"  title="Edit"/></a>&nbsp;|&nbsp;
 	<a href="add_team.php?action=delete&lid=<?php print $result['id'];?>" title="Delete" onClick="return confirmDelete();" > <img src="../images/block.png" width="15px" class="key_image" title="Delete"/></a>        
</td>
<td ><?php print $result['teamname']; ?></td>
<td ><?php print $row1['fname'].' '.$row1['lname']; ?></td>
<td ><?php print $row2['fname'].' '.$row2['lname']; ?></td>
<td align="center">
<?php 
	if ( $result['status'] == 0 ) {
		echo '<img src="../images/checked.gif"  title="Active" />';
	}
	else {
		echo '<img src="../images/cancel.png"  title="In Active" />';
	}
?>		
</td>
<td  align="center"><?php print $result['modifiedby']; ?></td>
<td  align="center"><?php echo display_time_diff_format($result['modifiedtime'],1);?></td>
</tr>
<?php
} // while closing
?>
</table>
<br/>
<br/>
   </div>
 </div>
 </div>
</div>

<?php
include "lcas_footer.php";
?>
