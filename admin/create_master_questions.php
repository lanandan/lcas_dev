<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();
$page_name = "create_master_questions.php";
$page_title = $site_name." Create / Edit Master Questions";

$qn_id_to_go = get_session('qn_id');

$action_go = request_get('action');

$cur_username=get_session('DISPLAY_NAME');

$ques_id='';
$ques_name='';
$ques_type=__QUESTION_YES_NO__;
$ques_status = '';
$ques_description='';
$ques_example='';
$title='';
$answer_option1 = '';
$answer_option2 = '';
$answer_option3 = '';
$answer_option4 = '';
$answer_option5 = '';
$answer_option6 = '';

if(request_get('action')){

	if ( request_get('action') == "do"  )
	{
		$flag = true;		
		$ques_type=request_get('ques_type');
		$ques_name=request_get('rt_ques_name');
		$ques_status = request_get('ques_status');
		$title= request_get('title');
		$answer_option1 = request_get('rt_answer_option1');
		$answer_option2 = request_get('rt_answer_option2');			
		$answer_option3 = request_get('rt_answer_option3');						
		$answer_option4 = request_get('rt_answer_option4');						
		$answer_option5 = request_get('rt_answer_option5');						
		$answer_option6 = request_get('rt_answer_option6');
		$timestamp = time();

		
		if (request_get('whattodo') == "QuestionTypeChange" )
		{

		}
		else 
		{
			$q_id= request_get('q_id');

			if($q_id>0)
			{

			$rootpage=request_get('root');
			$scriptid=request_get('scriptid');

			$sql_qry="UPDATE `tps_questions` SET `name` = '$ques_name',`title` = '$title', `type` = '$ques_type', `status` = '$ques_status', `answer_option1` = '$answer_option1', `answer_option2` = '$answer_option2', `answer_option3` = '$answer_option3', `answer_option4` = '$answer_option4', `answer_option5` = '$answer_option5', `answer_option6` = '$answer_option6', `modified` = '$timestamp', `modifiedby` = '$cur_username' WHERE  `id`='$q_id'";
			
			mysql_query($sql_qry) or die(mysql_error());	
			
			if($rootpage=="ql")
				$url = "Location: question_listing.php?qn_id=$scriptid";
			else		
				$url = "Location: master_questions_listing.php";
	
			header($url);
			exit();
		

			}else{

			$sql_qry="INSERT INTO `tps_questions` (`id`, `name`,`title`, `type`, `status`, `answer_option1`, `answer_option2`, `answer_option3`, `answer_option4`, `answer_option5`, `answer_option6`, `created`, `createdby`) VALUES (NULL, '$ques_name','$title', '$ques_type', '$ques_status', '$answer_option1', '$answer_option2', '$answer_option3', '$answer_option4', '$answer_option5', '$answer_option6', '$timestamp', '$cur_username')";
			
			
			mysql_query($sql_qry) or die(mysql_error());	

			$url = "Location: master_questions_listing.php";
			header($url);
			exit();

			}
			
		}
		
	}

		
	if ( request_get('action') == "edit"  )
	{
		$q_id_to_go = request_get('q_id');
		$sql_qry = "select * from tps_questions where id = '".$q_id_to_go."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$flag = true;	
			$ques_name=$row['name'];

			$ques_type=$row['type'];;
			$ques_status = $row['status'];
			$ques_description=$row['name'];
			$title=$row['title'];
			$answer_option1 = addslashes($row['answer_option1']);
			$answer_option2 = addslashes($row['answer_option2']);			
			$answer_option3 = addslashes($row['answer_option3']);						
			$answer_option4 = addslashes($row['answer_option4']);						
			$answer_option5 = addslashes($row['answer_option5']);						
			$answer_option6 = addslashes($row['answer_option6']);	
		}
	}
	
	if ( request_get('action') == "delete"  )
	{
		$q_id_to_go = request_get('q_id');
		$sql_qry = "delete from tps_questions where id = '".$q_id_to_go."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		//updateQuesionsTotals(get_session('qn_id'));
		$url = "Location: master_question_listing.php";
		header($url);
		exit();						
		
	}
		
}

function get_ques_type_list($ques_type)
{   
	global $ques_description;
	global $ques_example;	
	$str = '';
	$sql = "select ques_value, ques_type, description, sample from tps_question_types where status = '0' ";
	$res = mysql_query($sql) or die(mysql_error());
	if ( mysql_num_rows($res) > 0 ) {
		$str = '<select name="ques_type" id="ques_type" onchange="onQuestionChange();" style="width:267px;" >';
			while($row = mysql_fetch_array($res)){
				//echo "<br>",$qn_id," | ",$row['id'];
				if ( $ques_type == $row['ques_value'] ) {
					
					$str .= '<option selected="selected" value="'.$row['ques_value'].'">'.$row['ques_type'].'</option>';
					$ques_description = $row['description'];
					$ques_example =  $row['sample'];
				}
				else {
					$str .= '<option value="'.$row['ques_value'].'">'.$row['ques_type'].'</option>';
				}
			}
		$str .= '</select>';
	}
	mysql_free_result($res);
	return $str;
}

function display_points_options($answer_points)
{
	if ($answer_points == '' ) 
		$answer_points='5';
	for ($i = 1 ; $i <= 10; $i++)
	{
		if ($i == $answer_points)
		{
			echo '<option selected="selected" value="'.$i.'" ><'.$i.'</option>';
		}
		else 
		{
			echo '<option value="'.$i.'" >< '.$i.'</option>';
		}
	}
}

$ques_type_list = get_ques_type_list($ques_type);


include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>

<script language="JavaScript">

function submitform()
{
	if ( onSubmitCheck(document.forms['create_question'], true, false) )
	{
		//testvalue =<?php echo $ques_type;?>;
		//alert (testvalue);
		document.create_question.submit();
	}
	else {
		return false;
	}
}

function onQuestionChange()
{
	if ( document.create_question.ques_type.value > 0 )
	{
		document.create_question.whattodo.value = "QuestionTypeChange";
		document.create_question.submit();
	}
}
</script>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Master Questions&nbsp;&nbsp;<a class="btn btn-blue" href="master_questions_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit Master Questions</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" method="post" id="create_question" name="create_question">

          <div class="padded">

            <div class="form-group">
		 <div class="col-lg-10">
              <label class="control-label col-lg-7"><?php echo $ques_description;?></label>
		 </div>
            </div>


		  <div class="form-group">
              <label class="control-label col-lg-2">Question Title *</label>
              <div class="col-lg-4">
              <input type="text" value="<?php echo $title;?>" size="40" name="title" id="title" title="Question Title" required />
              </div>
            </div>


           

            <div class="form-group">
              <label class="control-label col-lg-2">Question *</label>
              <div class="col-lg-4">
		<textarea name="rt_ques_name" id="rt_ques_name" title="Question Name" class="expand75-300" cols="75" required ><?php echo $ques_name;?></textarea>
              </div>
            </div>

 <div class="form-group">
              <label class="control-label col-lg-2">Question Type *</label>
              <div class="col-lg-4">
                <?php print $ques_type_list;?>
              </div>
            </div>




            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="ques_status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="ques_status" <?php if($ques_status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

<?php 
	if ( $ques_type == __QUESTION_YES_NO__ || $ques_type == __QUESTION_MULTI_ANSWER__ || $ques_type == __QUESTION_MULTI_CHOICE__ || $ques_type == __QUESTION_DROP_DOWN__)
	{
?>

            <div class="form-group">
              <label class="control-label col-lg-2">Answer Option 1</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option1);?>" size="40" name="rt_answer_option1" id="rt_answer_option1" title="Answer Option 1"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Answer Option 2</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option2);?>" size="40" name="rt_answer_option2" id="rt_answer_option2" title="Answer Option 2"/>
              </div>
            </div>

<?php
	}
	if ( $ques_type == __QUESTION_MULTI_ANSWER__ || $ques_type == __QUESTION_MULTI_CHOICE__ || $ques_type == __QUESTION_DROP_DOWN__ )
	{
?>

            <div class="form-group">
              <label class="control-label col-lg-2">Answer Option 3</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option3);?>" size="40" name="rt_answer_option3" id="rt_answer_option3" title="Answer Option 3"/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Answer Option 4</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option4);?>" size="40" name="rt_answer_option4" id="rt_answer_option4" title="Answer Option 4"/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Answer Option 5</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option5);?>" size="40" name="rt_answer_option5" id="rt_answer_option5" title="Answer Option 5"/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Answer Option 6</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option6);?>" size="40" name="rt_answer_option6" id="rt_answer_option6" title="Answer Option 6"/>
              </div>
            </div>
<?php
	}
	if ( $ques_type == __QUESTION_RATING__ )
	{
?>

            <div class="form-group">
              <label class="control-label col-lg-2">Rating legend 1</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option1);?>" size="40" name="rt_answer_option1" id="rt_answer_option1" title="Rating Legend 1"/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Rating legend 2</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option2);?>" size="40" name="rt_answer_option2" id="rt_answer_option2" title="Rating Legend 2"/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Rating legend 3</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option3);?>" size="40" name="rt_answer_option3" id="rt_answer_option3" title="Rating Legend 3"/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Rating legend 4</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option4);?>" size="40" name="rt_answer_option4" id="rt_answer_option4" title="Rating Legend 4"/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Rating legend 5</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option5);?>" size="40" name="rt_answer_option5" id="rt_answer_option5" title="Rating Legend 5"/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Rating legend 6</label>
              <div class="col-lg-4">
		<input type="text" value="<?php echo addslashes($answer_option6);?>" size="40" name="rt_answer_option6" id="rt_answer_option6" title="Rating Legend 6"/>
              </div>
            </div>
<?php
	}
?>


    <input type="hidden" name="action" value="do" />
	<input type="hidden" name="whattodo" value="" /> 
     <input type="hidden" name="q_id" value="<?php echo $q_id_to_go;?>" />      
    <input type="hidden" name="ques_id" value="<?php echo $ques_id;?>" />
 
<?php
    $url_cancel = "javascript:window.location='question_listing.php?qn_id=".$qn_id_to_go."'";
?>
     
            <button type="submit" class="btn btn-blue" name="Save"  onclick="javascript:return submitform()" >Save changes</button>
           <a href="master_questions_listing.php"> <button type="button" class="btn btn-default"  >Cancel</button></a>

<br/><br/><br/>
	<?php echo $ques_example;?>

<br/><br/><br/>
</form> 

   </div>
 </div>
 </div>
</div>
<?php

include "lcas_footer.php";
?>
