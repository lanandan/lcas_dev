<?php
session_start();

require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "preview_scripts.php";
$page_title = $site_name." Scripts Preview";

$script_id = '';

$script_id = request_get('qn_id');
	
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Script Preview&nbsp;&nbsp;<a class="btn btn-blue" href="question_listing.php?qn_id=<?php echo $script_id; ?>"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

<?php
$sql ="SELECT name, no_of_questions from tps_scripts where id = $script_id ";
$result_list=mysql_query($sql) or die(mysql_error());
$result = mysql_fetch_array($result_list);
?>

<div class="container">
<div class="box">
   <div class="box-header">
     <span class="title">Script Preview - <?php echo $result['name']." #Questions - ".$result['no_of_questions']; ?></span>
   </div>
   <div class="box-content" style="padding:1%">
<?php 

$qn_list_query="SELECT q.id, q.name, q.type, q.answer_option1, q.answer_option2, q.answer_option3, q.answer_option4, q.answer_option5, q.answer_option6, q.modified, q.modifiedby, sq.id as sqid, sq.script_id, sq.question_id, sq.displayorder FROM tps_questions q JOIN tps_scripts_questions sq ON sq.question_id=q.id WHERE sq.script_id='$script_id' order by sq.displayorder ASC";

$result_list=mysql_query($qn_list_query) or die(mysql_error());

if ( mysql_num_rows($result_list) > 0 ) {
	echo '<table width="100%"   border="0" cellpadding="5" cellspacing="5" align="center"  >';
	$i=1;
	while($row = mysql_fetch_array($result_list)){
	echo '<tr>';
	echo '<td>&nbsp;</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td>'.$i.'.    '.$row['name'].'</td>';
	echo '</tr>';

	if($row['type']==__QUESTION_YES_NO__ or $row['type']==__QUESTION_MULTI_CHOICE__){
		echo '<tr><td>';
		if($row['answer_option1']!=''){
		echo '<input value="'.$row['answer_option1'].'" id="ques'.$i.'1" name="ques'.$i.'"  type="radio">'.$row['answer_option1'].'<br />';}
		if($row['answer_option2']!=''){
		echo '<input value="'.$row['answer_option2'].'" id="ques'.$i.'2" name="ques'.$i.'"  type="radio">'.$row['answer_option2'];}
		echo '</td></tr>';
	}
	if($row['type']==__QUESTION_MULTI_CHOICE__ ){
		echo '<tr><td>';
		if($row['answer_option3']!=''){
		echo '<input value="'.$row['answer_option3'].'" id="ques'.$i.'3" name="ques'.$i.'"  type="radio">'.$row['answer_option3'].'<br />';}
		if($row['answer_option4']!=''){
		echo '<input value="'.$row['answer_option4'].'" id="ques'.$i.'4" name="ques'.$i.'"  type="radio">'.$row['answer_option4'].'<br />';}
		if($row['answer_option5']!=''){
		echo '<input value="'.$row['answer_option5'].'" id="ques'.$i.'5" name="ques'.$i.'"  type="radio">'.$row['answer_option5'].'<br />';}
		if($row['answer_option6']!=''){
		echo '<input value="'.$row['answer_option6'].'" id="ques'.$i.'6" name="ques'.$i.'"  type="radio">'.$row['answer_option6'].'<br />';}
		echo '</td></tr>';
	}
	if($row['type']==__QUESTION_MULTI_ANSWER__ ){
		echo '<tr><td>';
		if($row['answer_option1']!=''){
		echo '<input value="'.$row['answer_option1'].'" id="ques'.$i.'1" name="ques'.$i.'"  type="checkbox">'.$row['answer_option1'].'<br />';}
		if($row['answer_option2']!=''){
		echo'<input value="'.$row['answer_option2'].'" id="ques'.$i.'2" name="ques'.$i.'"  type="checkbox">'.$row['answer_option2'].'<br />';}
		if($row['answer_option3']!=''){
		echo '<input value="'.$row['answer_option3'].'" id="ques'.$i.'3" name="ques'.$i.'"  type="checkbox">'.$row['answer_option3'].'<br />';}
		if($row['answer_option4']!=''){
		echo'<input value="'.$row['answer_option4'].'" id="ques'.$i.'4" name="ques'.$i.'"  type="checkbox">'.$row['answer_option4'].'<br />';}
		if($row['answer_option5']!=''){
		echo '<input value="'.$row['answer_option5'].'" id="ques'.$i.'5" name="ques'.$i.'"  type="checkbox">'.$row['answer_option5'].'<br />';}
		if($row['answer_option6']!=''){
		echo'<input value="'.$row['answer_option6'].'" id="ques'.$i.'6" name="ques'.$i.'"  type="checkbox">'.$row['answer_option6'];}

		echo '</td></tr>';
	}
	if($row['type']==__QUESTION_RATING__ ){
		echo '';
		$ratval=1;$ratname='';
		if($row['answer_option1']!=''){
$ratname .= "~".$row['answer_option1'];
		echo '<tr><td>'.$row['answer_option1'].' : &nbsp;<input value="1" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat1" name="ques'.$i.'rat1" type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">10 &nbsp;</td></tr>';

		}
		if($row['answer_option2']!=''){
		echo '<tr><td>'.$row['answer_option2'].' : &nbsp;<input value="1" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat2" name="ques'.$i.'rat2" type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat2" name="ques'.$i.'rat2" type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">6 &nbsp; <input value="7" name="ques'.$i.'rat2" id="ques'.$i.'rat2"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option2'];
		$ratval+=1;
		}
		if($row['answer_option3']!=''){
		echo '<tr><td>'.$row['answer_option3'].' : &nbsp;<input value="1" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option3'];
		$ratval+=1;
		}
		if($row['answer_option4']!=''){
		echo '<tr><td>'.$row['answer_option4'].' : &nbsp;<input value="1" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option4'];
		$ratval+=1;
		}
		if($row['answer_option5']!=''){
		echo '<tr><td>'.$row['answer_option5'].' : &nbsp;<input value="1" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option5'];
		$ratval+=1;
		}
		if($row['answer_option6']!=''){
		echo '<tr><td>'.$row['answer_option6'].' : &nbsp;<input value="1" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option6'];
		$ratval+=1;
		}
		$ratfinal=$ratval.$ratname;
		
		echo "<input type='hidden'  name='hdques".$i."' id='ques".$i."' value='".serialize($ratfinal)."'>";
		echo '';
	}
	if($row['type']==__QUESTION_SHORT_ANS__ ){
		echo '<tr>';
		echo '<td><input value="" type="text" name="ques'.$i.'" ></td>';
		echo '</tr>';
	}
	if($row['type']==__QUESTION_LONG_ANS__ ){
		echo '<tr>';
		echo '<td><input value="" type="textarea" name="ques'.$i.'"></td>';
		echo '</tr>';
	}

	if($row['type']==__QUESTION_DROP_DOWN__ ){
		echo '<tr>';
		echo '<td><select name="ques'.$i.'" id="ques'.$i.'" >';
		echo '<option selected="selected" value="select">-- select --</option>';
		if($row['answer_option1']!=''){
		echo '<option  value="'.$row['answer_option1'].'">'.$row['answer_option1'].'</option>'; }
		if($row['answer_option2']!=''){
		echo '<option  value="'.$row['answer_option2'].'">'.$row['answer_option2'].'</option>'; }
		if($row['answer_option3']!=''){
		echo '<option  value="'.$row['answer_option3'].'">'.$row['answer_option3'].'</option>'; }
		if($row['answer_option4']!=''){
		echo '<option  value="'.$row['answer_option4'].'">'.$row['answer_option4'].'</option>'; }
		if($row['answer_option5']!=''){
		echo '<option  value="'.$row['answer_option5'].'">'.$row['answer_option5'].'</option>'; }
		if($row['answer_option6']!=''){
		echo '<option  value="'.$row['answer_option6'].'">'.$row['answer_option6'].'</option>'; }
		echo '</td>';
		echo '</tr>';
	}

	$i++;
    } //While close
    
    echo '</table>';
}
else {
	
echo '<h5>There are no questions added to this Script.  Please add Questions and do Preview. <br/>&nbsp;&nbsp;<a class="btn btn-blue" href="question_listing.php?qn_id='.$script_id.'"><span>Back to Question Listing</span></a></h5>';
}

?>

   </div>
 </div> 
 </div>

<?php

include "lcas_footer.php";
?>
