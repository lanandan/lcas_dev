<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "master_pdf_listing.php";
$page_title = $site_name." Master PDF Listing";

$script_id ='';
$Q_Type_Def ='';

$script_id = request_get('qn_id');
if ($script_id != '') {
	set_session('qn_id', $script_id);
}
	
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<script type="text/javascript">
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete the PDF?");
	if (agree)
		return true ;
	else
		return false ;
}
</script>


<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Master PDF Listing&nbsp;&nbsp;<a class="btn btn-blue" href="create_master_pdf.php"><span>Create New PDF Template</span></a>&nbsp;&nbsp;&nbsp;

<!--<a class="btn btn-blue" href="scripts_listing.php"><span>Back</span></a>--></h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Master PDF Listing</span></div>
<div class="box-content">
<div id="dataTables">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
<thead>
<tr>
  <th><div>Action</div></th>
  <th><div>Name</div></th>
  <th><div>Content</div></th>
  <th><div>Status</div></th>
  <th><div>Modified By</div></th>
  <th><div>Modified Time</div></th>
</tr>
</thead>
<tbody>
<?php
$sql_qry = "select * from tps_master_pdfs order by modifieddate desc";
$result_list = mysql_query($sql_qry) or die(mysql_error());
while($result=mysql_fetch_array($result_list)) {	
?>
<tr>
<td>
	<a href="create_master_pdf.php?action=edit&q_id=<?php echo $result['id'];?>" title="Edit"><img src="../images/edit.png" class="key_image"  title="Edit"/></a>
        &nbsp;|&nbsp;
        <a href="create_master_pdf.php?action=delete&q_id=<?php echo $result['id'];?>" title="Delete" onClick="return confirmDelete();" > <img src="../images/small-bin.png" class="key_image" title="Delete"/></a>
</td>
<td><?php print $result['name']; ?></td>
<td><?php //print $Q_Type_Def; ?></td>
<td align="center">
<?php 
	if ( $result['status'] == 0 ) {
		echo '<img src="../images/checked.gif"  title="Active" />';
	}
	else {
		echo '<img src="../images/cancel.png"  title="In Active" />';
	}
?>		
</td>
<td ><?php print $result['modifiedby']; ?></td>
<td ><?php echo display_time_diff_format(strtotime($result['modifieddate']),1);?></td>
</tr>
<?php
}
?>
</table>
<br/>
<br/>
   </div>
 </div>
 </div>
</div>

<?php
include "lcas_footer.php";
?>
