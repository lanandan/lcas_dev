	function load_canv_1(){
		var canvas = document.getElementById("_canv_1");

		if (canvas && canvas.getContext){
			var ctx = canvas.getContext("2d");
			ctx.fillStyle = "rgba(255,255,255,1)";
			ctx.beginPath();
			ctx.moveTo(0,22.6493);
			ctx.lineTo(106.71,22.6493);
			ctx.lineTo(106.71,0);
			ctx.lineTo(0,0);
			ctx.closePath();
			ctx.fill();
		}
	}
	
	function load_canv_2(){
		var canvas = document.getElementById("_canv_2");

		if (canvas && canvas.getContext){
			var ctx = canvas.getContext("2d");
			ctx.lineWidth = 4;
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 13.3333;
			ctx.strokeStyle = "rgba(0,0,0,1)";
			ctx.beginPath();
			ctx.moveTo(4,4.0001);
			ctx.lineTo(750.68,4.0001);
			ctx.lineTo(750.68,403.76);
			ctx.lineTo(4,403.76);
			ctx.lineTo(4,4.0001);
			ctx.closePath();
			ctx.stroke();
		}
	}
	
	function load_canv_3(){
		var canvas = document.getElementById("_canv_3");

		if (canvas && canvas.getContext){
			var ctx = canvas.getContext("2d");
			ctx.lineWidth = 2.6667;
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 13.3333;
			ctx.strokeStyle = "rgba(0,0,0,1)";
			ctx.beginPath();
			ctx.moveTo(2.6667,2.6626);
			ctx.lineTo(759.14,2.6626);
			ctx.lineTo(759.14,529.44);
			ctx.lineTo(2.6667,529.44);
			ctx.lineTo(2.6667,2.6626);
			ctx.closePath();
			ctx.stroke();
		}
	}
	
function DrawPage1(){
	load_canv_1();
	load_canv_2();
	load_canv_3();
}
