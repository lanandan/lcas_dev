<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");

validate_login();
$page_name = "add_leadtype_scripts.php";
$page_title = $site_name." Add / Edit LeadType Scripts";
$cur_page="add_leadtype_scripts";

$lead_type='';
$lead_type_disp='';
$status='';
$l_id='';

if(request_get('action')){
	if ( request_get('action') == "do"  )
	{
		$flag = true;
		//set the values from post profile
		$lead_type=isset($_POST['lead_type'])?safe_sql_nq(trim($_POST['lead_type'])):'';
		$lead_type_disp=isset($_POST['lead_type_disp'])?safe_sql_nq(trim($_POST['lead_type_disp'])):'';
		$status=isset($_POST['lead_status'])?trim($_POST['lead_status']):'';
		$timestamp =  time();

		$l_id = request_get('l_id');
		if ($l_id > 0)
		{
			//update databse
			$sql= "update tps_lead_type set ".
					" lead_type = '". $lead_type ."', ".
					" type_displayname = '". $lead_type_disp ."', ".
					" status = '". $status ."', ".		
					" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
					" modified = '". $timestamp ."' ".
					" where id = '".$l_id."' " ;
		}
		else {
			$sql= "insert into tps_lead_type set ".
					" lead_type = '". $lead_type ."', ".
					" type_displayname = '". $lead_type_disp ."', ".
					" status = '". $status ."', ".
					" createdby = '". get_session('DISPLAY_NAME') ."', ".
					" created = '". $timestamp ."', ".			
					" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
					" modified = '". $timestamp ."'  " ;
		}		
		mysql_query($sql) or die(mysql_error());

		$url = "Location: lead_type_listing.php";
		header($url);
		exit();	
	}
	if ( request_get('action') == "edit"  )
	{
		$l_id = request_get('l_id');
		$sql_qry = "select * from tps_lead_type where id = $l_id ";

		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$lead_type=$row['lead_type'];
			$lead_type_disp=$row['type_displayname'];
			$status=$row['status'];			
		}
	}
	if ( request_get('action') == "delete"  )
	{
		$l_id = request_get('l_id');
		$sql_qry = "delete from tps_lead_type where id = '".$l_id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$url = "Location: lead_type_listing.php";
		header($url);
		exit();	
	}
}
	
include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );		


$lsubtype="";

?>
<script  type="text/javascript" >

$(document).ready(function() {

$('#lead_type').change(function() {
        $.ajax({
            type: "POST",
	    dataType: "json",
	    url: "subtyperet.php",
            data: {
		ltype: $("#lead_type").val()
            },
            success: function(result) {
		$('#lead_subtype').empty();
		$(result).each(function () {
		    // Create option
		    var $option = $('<option>');
		    // Add value and text to option

		    $option.attr("value", this.sublvalue).text(this.subltext);
		    // Add option to drop down list
		    $('#lead_subtype').append($option);
		});  
		
            }
        });
    });

});
</script>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Lead Type&nbsp;&nbsp;<a class="btn btn-blue" href="lead_type_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit LeadType Scripts</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" action="add_lead_type.php" method="POST" id="add_lead_type" name="add_lead_type">
          <div class="padded">
            <div class="form-group">
              <label class="control-label col-lg-2">Lead Type *</label>
              <div class="col-lg-4">
		<select name="lead_type" id="lead_type" class="uniform validate[required]" title="Lead Type" data-prompt-position="topLeft">
		<option value="">Select</option>
		<?php

	$ltres=mysql_query("select DISTINCT lead_type,type_displayname from tps_lead_type") or die(mysql_error());
	while($ltr=mysql_fetch_array($ltres)){
		
		if($lead_type==$ltr['lead_type'])
		{
			echo "<option value='".$ltr['lead_type']."' selected >".$ltr['type_displayname']."</option>";
		}else{
			echo "<option value='".$ltr['lead_type']."' >".$ltr['type_displayname']."</option>";
		}

	}	

		?>
		</select>
              <!--  <input type="text" class="validate[required]" data-prompt-position="topLeft" name="lead_type" size="128" id="lead_type" title="Lead Type" value="<?php echo $lead_type;?>"/>-->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Lead Sub Type *</label>
              <div class="col-lg-4">
		<select name="lead_subtype" id="lead_subtype" class="uniform validate[required]" title="Lead Sub Type" data-prompt-position="topLeft">
		<?php 
			if($lsubtype !='' )
				echo getLeadSubType($lsubtype,$ltype); 
			else
				echo getLeadSubType(__DEFAULT_LTYPE__,__DEFAULT_LSUBTYPE__); 
		?>
		</select>
                <!--<input type="text" class="validate[required]" data-prompt-position="topLeft" name="lead_type_disp" size="128" id="lead_type_disp" title="Lead Type" value="<?php echo $lead_type_disp;?>"/>-->
              </div>
            </div>

	     <div class="form-group">
              <label class="control-label col-lg-2">Select Script *</label>
              <div class="col-lg-4">
<select name="scripts" id="scripts" class="uniform validate[required]" title="scripts" data-prompt-position="topLeft">
	<option value="">Select</option>
	<?php
		$sres=mysql_query("SELECT id,name,no_of_questions FROM `tps_scripts`") or die(mysql_error());
	while($sr=mysql_fetch_array($sres)){
		
		if($script_id==$sr['id'])
		{
			echo "<option value='".$sr['id']."' selected >".$sr['name']." (".$sr['no_of_questions'].")"."</option>";
		}else{
			echo "<option value='".$sr['id']."' >".$sr['name']." (".$sr['no_of_questions'].")"."</option>";
		}

	}	
		?></select>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="lead_status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="lead_status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>


	    <input type="hidden" name="action" value="do" />
	    <input type="hidden" name="l_id" value="<?php echo $l_id;?>" />

          <div class="form-actions">
            <button type="submit" class="btn btn-blue">Save changes</button>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='lead_type_listing.php'">Cancel</button>
          </div>
        </form>
<br/><br/>
   </div>
 </div>
 </div>

<?php
include "lcas_footer.php";
?>
