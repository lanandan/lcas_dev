<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();
$page_name = "add_team_status.php";
$page_title = $site_name." Add /Edit Team Status ";
$cur_page="configuration";

$name='';
$display='';
$status='';
$displayorder='';
$notes='';
$lid='';

if(request_get('action')){
	if ( request_get('action') == "do"  )
	{
		$flag = true;
		//set the values from post profile
		$name=isset($_POST['name'])?safe_sql_nq(trim($_POST['name'])):'';
		$display=isset($_POST['display'])?safe_sql_nq(trim($_POST['display'])):'';
		$displayorder=isset($_POST['displayorder'])?safe_sql_nq(trim($_POST['displayorder'])):'';
		$notes=isset($_POST['notes'])?safe_sql_nq(trim($_POST['notes'])):'';
		$status=isset($_POST['status'])?trim($_POST['status']):'';

		$timestamp =  time();

		$lid = request_get('lid');
		if ($status == '') {$status='0';}
		if ($lid > 0)
		{
			//update databse
			$sql= "update tps_team_status set ".
				" name = '". $name ."', ".
				" display = '". $display ."', ".
				" status = '". $status ."', ".
				" displayorder = '". $displayorder ."', ".
				" notes = '". $notes ."', ".
				" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
				" modified = '". $timestamp ."' ".
				" where id = '".$lid."' " ;
		}
		else {
			$sql= "insert into tps_team_status set ".
				" name = '". $name ."', ".
				" display = '". $display ."', ".
				" status = '". $status ."', ".
				" displayorder = '". $displayorder ."', ".
				" notes = '". $notes ."', ".
				" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
				" modified = '". $timestamp ."', ".
				" createdby = '". get_session('DISPLAY_NAME') ."', ".
				" created = '". $timestamp ."' ";			
		}	
		mysql_query($sql) or die(mysql_error());

		$url = "Location: team_status_listing.php";
		header($url);
		exit();	
	}
	if ( request_get('action') == "edit"  )
	{
		$lid = request_get('lid');
		$sql_qry = "select * from tps_team_status where id = $lid ";

		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$name=$row['name'];
			$display=$row['display'];
			$notes=$row['notes'];
			$status=$row['status'];			
			$displayorder=$row['displayorder'];			
		}
	}
	if ( request_get('action') == "delete"  )
	{
		$lid = request_get('lid');
		$sql_qry = "delete from tps_team_status where id = '".$lid."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$url = "Location: team_status_listing.php";
		header($url);
		exit();	
	}
}
	
include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );		

?>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Team Status &nbsp;&nbsp;<a class="btn btn-blue" href="team_status_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit Team Status</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" action="add_team_status.php" method="POST" id="add_team_status" name="add_team_status">
          <div class="padded">
            <div class="form-group">
              <label class="control-label col-lg-2">Team Status Abbr*</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="name" size="128" id="name" title="Team Status Abbr" value="<?php echo $name;?>"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Display Name *</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="display" size="128" id="display" title="Team Status Display Name" value="<?php echo $display;?>"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Display Order</label>
              <div class="col-lg-1">
                  <select class="uniform" name="displayorder" id="displayorder">
		   <option value="" >Select</option>
		   <?php echo getDisplayOrderNum($displayorder); ?>
		  </select>
              </div>
		<label class="control-label col-lg-3">Lower Value = Top Position</label>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Notes</label>
              <div class="col-lg-4">
                <textarea name="notes" id="notes" title="Notes"  cols="75"><?php echo $notes;?></textarea><span class="counter"></span>
              </div>
            </div>

	    <input type="hidden" name="action" value="do" />
	    <input type="hidden" name="lid" value="<?php echo $lid;?>" />

          <div class="form-actions">
            <button type="submit" class="btn btn-blue">Save changes</button>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='team_status_listing.php'">Cancel</button>
          </div>
        </form>
<br/><br/>
   </div>
 </div>
 </div>

<?php
include "lcas_footer.php";
?>
