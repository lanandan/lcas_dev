<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "gift_choice_listing.php";
$page_title = $site_name." Gift Choice Listing";
$cur_page="configuration";
	
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

$lead_type='';

?>
<style>
#sortable1, #sortable2 
{ 
	list-style-type: none; 
	margin: 0; 
	padding: 0 0 2.5em; 
	float: left; 
	margin-right: 10px;
	width: 100%;
	z-index:999;
}
#sortable1 li, #sortable2 li 
{ 
	margin: 0 5px 5px 5px; 
	padding: 8px; 
	font-size: 13px; 
	width: 100%; 
	cursor:move;
	z-index:999;
}
.highlight {
    border: 1px dashed grey;
    font-weight: bold;
    margin: 0 5px 5px 5px; 
    padding: 8px; 
    font-size: 13px;
}

.padded{
	overflow: hidden;
}

.lt{
 border:1px solid #990000;
}
</style>

<script type="text/javascript">

function delLeadType(id){

	var res=confirm("Are you sure do you want to delete this Gift from Lead Type?");
	
	var ltid=document.getElementById('curleadtypeid').value;

	if(res==true)
	{
		var data={
				type:"delgift",
				delid:id
		}
		$.ajax({
			type:"POST",
			url:"gift_actions.php",
			data:data,
			success:function(output){

				$("#sortable1").load('show_new_lt_content.php?show=1&ltid='+ltid);

			}
		});
	}

}

function delLeadSubtype(id,ltid,lstid,curCont){

	//alert(" Container : "+curCont.id+" ; LT :"+ltid+" ; LST : "+lstid+" ; ID : "+id);

	var res=confirm("Are you sure do you want to delete this Gift from Lead Subtype?");
	
	if(res==true)
	{
		var data={
				type:"delgift",
				delid:id
		}
		$.ajax({
			type:"POST",
			url:"gift_actions.php",
			data:data,
			success:function(output){

				$("#"+curCont.id).load('show_new_lt_content.php?show=2&ltid='+ltid+'&lstid='+lstid+'&container='+curCont.id);

			}
		});
	}

}

$(function() {

	$("#ltype").change(function(){

		var ltype=$("#ltype").val();

		var a = ltype.split("_");

		var ltid=a[0];
		var ltname=a[1];
		
		$("#ltTitle").html(" Selected Lead Type : "+ltname);
		$("#spinner").show();
		var data={
			type:'getLeadSubType',
			leadtypeid:ltid,
			leadtypename:ltname
		}
		$.ajax({
			type:'POST',
			url:'gift_actions.php',
			data:data,
			success:function(output){
				$("#spinner").hide();
				$('#dynamic_content').html(output);

				$( "#sortable1" ).sortable({
					connectWith: ".connectedSortable",
					receive: function( event, ui ) {
			
						var gid = ui.item.attr('id').match(/gid-([0-9]+)/).pop();
				     	 	var index = ui.item.index();

						//alert("Lead Type ID : "+ltid+" ; Selected Gift ID : "+gid+" ; Index : "+index);

						var data={
								type:"add_to_leadtype",
								leadtypeid:ltid,
								selgiftid:gid,
								displayorder:index
						}
						$.ajax({
							type:"POST",
							url:"gift_actions.php",
							data:data,
							success:function(output){

								$("#sortable1").load('show_new_lt_content.php?show=1&ltid='+ltid);
					
							}
	
						});
					},
			   		update : function (event, ui) {
			
						var newOrder = $('#sortable1').sortable('toArray').toString();
			
						//alert(newOrder);

						var data={
							type:"sortorderLeadType",
							leadtypeid:ltid,
							displayorder:newOrder
						}
						$.ajax({
							type:"POST",
							url:"gift_actions.php",
							data:data,
							success:function(output){
								$("#sortable1").load('show_new_lt_content.php?show=1&ltid='+ltid);
							}
	
						});
			    		}
				});

				var curTotal=document.getElementById('total').value;
				var newTotal=parseInt(curTotal)+parseInt(3);

			for(var i=3;i<newTotal;i++)
			{

				var curContainerName="#sortable"+i;
				var curSubType="#subtype"+i;

				$(curContainerName).sortable({
					connectWith: ".connectedSortable",
					receive: function( event, ui ) {
			
						var gid = ui.item.attr('id').match(/gid-([0-9]+)/).pop();
				     	 	var index = ui.item.index();
					
						var curUL=$(this).attr('id');
						var lstid=$(this).attr('value');

						var data={
								type:"add_to_leadsubtype",
								leadtypeid:ltid,
								leadsubtypeid:lstid,
								selgiftid:gid,
								displayorder:index
						}
						$.ajax({
							type:"POST",
							url:"gift_actions.php",
							data:data,
							success:function(output){

						$('#'+curUL).load('show_new_lt_content.php?show=2&ltid='+ltid+'&lstid='+lstid+'&container='+curUL);
					
							}
	
						});
					},
			   		update : function (event, ui) {
			
						var curUL=$(this).attr('id');						
						var lstid=$(this).attr('value');

						var newOrder = $(this).sortable('toArray').toString();

			//			alert(newOrder);

						var data={
							type:"sortorderLeadSubType",
							leadtypeid:ltid,
							leadsubtypeid:lstid,
							displayorder:newOrder
						}
						$.ajax({
							type:"POST",
							url:"gift_actions.php",
							data:data,
							success:function(output){
					
						$('#'+curUL).load('show_new_lt_content.php?show=2&ltid='+ltid+'&lstid='+lstid+'&container='+curUL);

							}
	
						});
			    		}
				});

 			  } // end of for loop

			},
			error:function(){
				alert("Error in Fetching Details from Gift Actions!");
			}
			
		});

	});

	
	
	$("#sortable2").sortable({
		connectWith: "ul",
		placeholder: "highlight",
		helper: 'clone',
		start: function (event, ui) {
                        ui.item.toggleClass("highlight");
                },
                stop: function (event, ui) {
                        ui.item.toggleClass("highlight");
                },
		remove: function(event, ui) {
                	//ui.item.clone().appendTo('#sortable1');
                	$(this).sortable('cancel');
            	}
	});

	$( "#sortable1, #sortable2" ).disableSelection();



}); // document ready

</script>



<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Gift Choice Assigning&nbsp;&nbsp;<a class="btn btn-blue" href="assign_gift_choice.php"><span>Assign Gift Choice</span></a></h3> 
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Assigned Gift Choices </span> 

<select class="nline" name="ltype" id="ltype"><option value="0">Select Lead Type</option>
<?php 
	$sql="select * from tps_lead_type where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	
	while($row = mysql_fetch_array($result))
	{
		if($sel==$row['id'])
			echo '<option value="'.$row['id'].'_'.$row['type_displayname'].'" selected>'.$row['type_displayname'].'</option>';
		else
			echo '<option value="'.$row['id'].'_'.$row['type_displayname'].'" >'.$row['type_displayname'].'</option>';
	
	}

?>
</select> 

</div>
<div class="box-content padded">

<div class="row">
		<div class="col-md-6">
			<div class="box">
				<div class="box-header"><span class="title" id="ltTitle">Lead Type</span></div>

				<div class="box-content padded" id="dynamic_content"></div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box">
				<div class="box-header"><span class="title">Master Gifts</span></div>
					<div class="box-content padded" style="min-height:350px;">
					
					<ul id="sortable2" class="connectedSortable">
			<?php
				$mgres=mysql_query("SELECT * FROM tps_gift_choice WHERE status=0 ORDER BY displayorder DESC")or die(mysql_error());
				$i=1;
				while($mgr=mysql_fetch_array($mgres))
				{
					echo '<li class="ui-state-highlight" id="gid-'.$mgr['id'].'">'.$mgr['name'].'</li>';
					$i++;
				}
			?>
					</ul>
				</div>
			</div>
		</div>
	</div>

 </div>
 </div>
</div>

<style>
#spinner {
position: fixed;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.5;
background: url('./images/preload.gif') 50% 50% no-repeat #ede9df;
}
</style>
<div id="spinner"></div>

<?php
include "lcas_footer.php";
?>
