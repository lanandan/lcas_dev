<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();
$page_name = "add_team.php";
$page_title = $site_name." Add /Edit Team";
$cur_page="configuration";

echo "aaaaaa";

if(request_get('action')){
	if ( request_get('action') == "do"  )
	{
		$flag = true;
		//set the values from post profile
		$tname=isset($_POST['tname'])?safe_sql_nq(trim($_POST['tname'])):'';
		$manager1=isset($_POST['manager1'])?safe_sql_nq(trim($_POST['manager1'])):'';
		$manager2=isset($_POST['manager2'])?safe_sql_nq(trim($_POST['manager2'])):'';
		$status=isset($_POST['status'])?trim($_POST['status']):'';

		$timestamp =  time();

		$id = request_get('id');
		if ($status == '') {$status='0';}
		if ($id > 0)
		{
			//update databse
			$sql= "update tps_team set ".
				" teamname = '". $tname ."', ".
				" manager1 = '". $manager1 ."', ".
				" manager2 = '". $manager2 ."', ".
				" status = '". $status ."', ".
				" createdby = '". get_session('DISPLAY_NAME') ."', ".
				" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
				" modifiedtime = '". $timestamp ."' ".
				" where id = '".$id."' " ;
		}
		else {
			$sql= "insert into tps_team set ".
				" teamname = '". $tname ."', ".
				" manager1 = '". $manager1 ."', ".
				" manager2 = '". $manager2 ."', ".
				" status = '". $status ."', ".
				" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
				" modifiedtime = '". $timestamp ."', ".
				" createdby = '". get_session('DISPLAY_NAME') ."', ".
				" createdtime = '". $timestamp ."' ";			
		}	
		mysql_query($sql) or die(mysql_error());

		$url = "Location: team_listing.php";
		header($url);
		exit();	
	}
	if (request_get('action')=="edit")
	{       

		$id = request_get('id');
		$sql_qry = "select * from tps_team where id = $id ";

		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
			$row = mysql_fetch_array($res_qry);
			$tname=$row['teamname'];
			$display=$row['display'];
			$notes=$row['notes'];
			$status=$row['status'];
		
	}
	if ( request_get('action') == "delete"  )
	{
		$id = request_get('id');
		$sql_qry = "delete from tps_team where id = '".$id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$url = "Location: team_listing.php";
		header($url);
		exit();	
	}
}
	
include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );		

?>


<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Team &nbsp;&nbsp;<a class="btn btn-blue" href="team_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit Team</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" action="add_team.php" method="POST" id="add_team" name="add_team">
          <div class="padded">
            <div class="form-group">
              <label class="control-label col-lg-2">Team Name*</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="tname" size="128" id="name" title="Team Name" value="<?php echo $tname;?>"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Manager 1 *</label>
              <div class="col-lg-4">
               	<select name="manager1" id= "manager1" class="nline"> 
			<?php echo getManager($manager1);?>
		</select>
	
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Manager 2 *</label>
              <div class="col-lg-4">
                <select name="manager2" id= "manager2" class="nline"> 
			<?php echo getManager($manager2);?>
		</select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>


	    <input type="hidden" name="action" value="do" />
	    <input type="hidden" name="id" value="<?php echo $id;?>" />

          <div class="form-actions">
            <button type="submit" class="btn btn-blue">Save changes</button>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='team_listing.php'">Cancel</button>
          </div>
        </form>
<br/><br/>
   </div>
 </div>
 </div>

<?php
include "lcas_footer.php";
?>
