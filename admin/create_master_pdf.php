<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();
$page_name = "create_master_pdf.php";
$page_title = $site_name." Create / Edit Master PDF";

$pdf_id_to_go = get_session('pdf_id');

$action_go = request_get('action');

$cur_username=get_session('DISPLAY_NAME');

$pdf_id='';
$pdf_name='';
$pdf_status = '';
$pdf_content='';
$edit=0;

if(request_get('action')){

			
	if ( request_get('action') == "edit"  )
	{
		$edit=1;

		$q_id_to_go = request_get('q_id');
		$sql_qry = "select * from tps_master_pdfs where id = '".$q_id_to_go."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$flag = true;	
			
			$pdf_id=$row['id'];
			$pdf_name=$row['name'];
			$pdf_status = $row['status'];
			$pdf_content=$row['content'];
		}
	}
	
	if ( request_get('action') == "delete"  )
	{
		$q_id_to_go = request_get('q_id');
		$sql_qry = "delete from tps_master_pdfs where id = '".$q_id_to_go."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		$url = "Location: master_pdf_listing.php";
		header($url);
		exit();						
		
	}
		
}

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?> 
<!--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />

<link rel="stylesheet" href="editor/summernote.css">
<script type="text/javascript" src="editor/summernote.js"></script>
-->
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script language="JavaScript">

function onQuestionChange()
{
	if ( document.create_question.ques_type.value > 0 )
	{
		document.create_question.whattodo.value = "QuestionTypeChange";
		document.create_question.submit();
	}
}

$(document).ready(function(){

  /*   $('.summernote').summernote({
        height: 200
      });
 var content = $('.summernote').code();
*/

     	$("#save").click(function() {

		var pdfname=$("#pdf_name").val();
		var content =  CKEDITOR.instances['pdf_content'].getData();
		var status=$('input[name=pdf_status]:checked', '#create_pdf').val();
		   
		var data={
				type:"saveTemplate",
				pdfname:pdfname,
				content:content,
				status:status
		}		
		$.ajax({
			type:"POST",
			url:"ref_rewards_actions.php",
			data:data,
			success:function(output){
				alert("PDF Template Saved Successfully");
				window.location.href="master_pdf_listing.php";
			}
		});

	});

	$("#update").click(function() {

		var tempid=$("#pdf_id").val();
		//alert(tempid);
		var pdfname=$("#pdf_name").val();
		var content =  CKEDITOR.instances['pdf_content'].getData();
		var status=$('input[name=pdf_status]:checked', '#create_pdf').val();

		var data={
				type:"updTemplate",
				tempid:tempid,
				pdfname:pdfname,
				content:content,
				status:status
		}
		$.ajax({
			type:"POST",
			url:"ref_rewards_actions.php",
			data:data,
			success:function(output){
				alert("PDF Template Updated Successfully");
				window.location.href="master_pdf_listing.php";
			}
		});

	});

});

</script>


<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Master PDF&nbsp;&nbsp;<a class="btn btn-blue" href="master_pdf_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit Master PDF</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" method="post" id="create_pdf" name="create_pdf">

          <div class="padded">

            <div class="form-group">
              <label class="control-label col-lg-2">PDF Name *</label>
              <div class="col-lg-4">
              <input type="text" value="<?php echo $pdf_name;?>" size="40" name="pdf_name" id="pdf_name" title="PDF Name" required />
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">PDF Content *</label>
              <div class="col-lg-10">
<!--  <textarea class="summernote" name="pdf_content" id="pdf_content" title="PDF Content" ><?php echo $pdf_content;?></textarea>
    <iframe src="editor/pdf_temp_editor.php" width="100%" />-->

<textarea name="pdf_content" id="pdf_content" title="PDF Content" class="ckeditor" cols="75" required><?php echo $pdf_content;?></textarea>
  

              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="0" name="pdf_status" checked="checked" id="iradio1" />
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
             <input type="radio" class="icheck" value="1" name="pdf_status" <?php if($pdf_status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

    <input type="hidden" name="action" value="do" />
	<input type="hidden" name="whattodo" value="" /> 
     <input type="hidden" name="q_id" value="<?php echo $pdf_id_to_go;?>" />      
    <input type="hidden" name="pdf_id" id="pdf_id" value="<?php echo $pdf_id; ?>" />

<?php if($edit==1){ ?>
	<button type="button" class="btn btn-blue" name="update" id="update">Update</button>
<?php }else{ ?>
	<button type="button" class="btn btn-blue" name="save" id="save">Save</button>
<?php } ?>
        <a href="master_pdf_listing.php"> <button type="button" class="btn btn-default"  >Cancel</button></a>

<br/><br/><br/>
</form> 

   </div>
 </div>
 </div>
</div>
<?php

include "lcas_footer.php";
?>
