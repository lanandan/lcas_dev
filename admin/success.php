<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();



include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
                  

?>

<div class="main-content">
  <div class="container">
    <div class="row">

      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
            <i class="icon-magic"></i>
            Success
          </h3>
          <h5>
            <span>
          
            </span>
          </h5>
        </div>

        <ul class="list-inline pull-right sparkline-box">

          <li class="sparkline-row">
            <h4 class="blue"><span>Orders</span> 847</h4>
            <div class="sparkline big" data-color="blue"><!--28,20,6,24,15,6,4,5,29,10,11,3--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="green"><span>Reviews</span> 223</h4>
            <div class="sparkline big" data-color="green"><!--28,7,26,14,26,3,8,22,9,14,9,20--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="red"><span>New visits</span> 7930</h4>
            <div class="sparkline big"><!--3,29,15,7,4,22,3,7,23,25,27,9--></div>
          </li>

        </ul>
      </div>
    </div>
  </div>

  <div class="container padded">
    <div class="row">

      <!-- Breadcrumb line -->

      <div id="breadcrumbs">
        <div class="breadcrumb-button blue">
          <span class="breadcrumb-label"><i class="icon-home"></i> Home</span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>

         <div class="breadcrumb-button">
          <span class="breadcrumb-label">
            <i class=" icon-globe"></i> Admin
          </span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>
           
           

        <div class="breadcrumb-button">
          <span class="breadcrumb-label">
            <i class="icon-magic"></i> Success
          </span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>
      </div>
    </div>
  </div>
 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Form Submitted Successfully</span>
   </div>
   <div class="box-content" style="padding:1%;">
<br /><br />
 	<h4 style="color:#7697AA;">Your answers are registered!!! Thanks for your co-operation!!!</h4>
<br /><br />
   </div>
 </div>
 </div>

<?php

include "lcas_footer.php";
?>
