<?php
ob_start();
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "script_lead_listing.php";
$page_title = $site_name." Add / Edit Referral Rewards Template";
$cur_page="scripts_list";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

$edit=0;
if(isset($_REQUEST['action'])=="edit")
{
	$edit=1;
	$tempid=request_get('mid');

	$sql="select * from tps_ref_rewards_template where id='$tempid'";

	$res=mysql_query($sql)or die("Error".mysql_error());
	
	$r=mysql_fetch_array($res);
	
}

?>
 <style>
        body {
            text-align: center;
        }

        section {
            width: 80%;
            margin: auto;
            text-align: left;
        }
    </style>
<script src="ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="ckeditor/sample.css">

<script type="text/javascript">
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete the Script Lead Mapping?");
	if (agree)
		return true ;
	else
		return false ;
}

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});

$(document).ready(function(){

	$("#save").click(function() {

		var rrname=$("#rrname").val();

		var content = CKEDITOR.instances['editor1'].getData();
		   
		var data={
				type:"saveTemplate",
				rrname:rrname,
				content:content
		}
		$.ajax({
			type:"POST",
			url:"ref_rewards_actions.php",
			data:data,
			success:function(output){
				alert("Template Saved Successfully");
				window.location.href="referral_rewards_listing.php";
			}
		});

	});
	
	$("#update").click(function() {

		var tempid=$("#tempid").val();
		var rrname=$("#rrname").val();

		var content = CKEDITOR.instances['editor1'].getData();
		   
		var data={
				type:"updTemplate",
				tempid:tempid,
				rrname:rrname,
				content:content
		}
		$.ajax({
			type:"POST",
			url:"ref_rewards_actions.php",
			data:data,
			success:function(output){
				alert("Template Updated Successfully");
				window.location.href="referral_rewards_listing.php";
			}
		});

	});
	

});

</script>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Referral Rewards Template&nbsp;&nbsp;</h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Referral Rewards Template</span>
<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
</div>


<div class="box-content padded">

<form method="post">

	<table width="100%" border="0" class="table" >
		<tr>
			<td>Name</td>
			<td>
			<?php if($edit==1){ ?>
				<input type="text" name="rrname" id="rrname" value="<?php echo $r['name']; ?>" size="60" />
			<?php }else{ ?>
				<input type="text" name="rrname" id="rrname" size="60" />
			<?php } ?>
			</td>
		</tr>
		<tr>
			<td valign="top">Content</td>
			<td>
			<?php if($edit==1){ ?>
				<textarea class="ckeditor" cols="80" id="editor1" name="editor1" rows="10"><?php echo $r['content']; ?></textarea>
			<?php }else{ ?>
				<textarea class="ckeditor" cols="80" id="editor1" name="editor1" rows="10"></textarea>
			<?php } ?>
			</td>
		</tr>
		<tr>
			<td> </td>
			<td align="right">
			<?php if($edit==1){ ?>
				<input type="hidden" name="tempid" id="tempid" value="<?php echo $r['id']; ?>" />
				<button type="button" class="btn btn-blue" name="update" id="update">Update</button>
			<?php }else{ ?>
				<button type="button" class="btn btn-blue" name="save" id="save">Save</button>
			<?php } ?>
		<button type="button" class="btn btn-default" onclick="javascript:window.location='referral_rewards_listing.php'">Cancel</button>
			</td>
		</tr>
	</table>
</form>

<br/>
 </div>
 </div>
</div>


<?php
include "lcas_footer.php";
ob_end_flush();
?>
