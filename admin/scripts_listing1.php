<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "scripts_listing.php";
$page_title = $site_name." Script Listing";
$cur_page="scripts_list";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<script type="text/javascript">
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete the entire Script?");
	if (agree)
		return true ;
	else
		return false ;
}
</script>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Script Listing&nbsp;&nbsp;<a class="btn btn-blue" href="create_scripts.php"><span>Create New Scripts</span></a></h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Script Listing</span></div>
<div class="box-content">
<div id="dataTables">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
<thead>
<tr>
  <th><div>Action</div></th>
  <th><div>Name</div></th>
  <th><div>Description</div></th>
  <th><div>#Questions</div></th>
  <th><div>Lead Type</div></th>
  <th><div>Lead SubType</div></th>
  <th><div>Status</div></th>
  <th><div>Modified By</div></th>
  <th><div>Modified Time</div></th>
</tr>
</thead>
<tbody>
<?php
$sql_qry = "select * from tps_scripts order by modified desc";
$result_list = mysql_query($sql_qry) or die(mysql_error());
while($result=mysql_fetch_array($result_list)) {	
?>
<tr>
<td>
        <a href="create_scripts.php?action=edit&qn_id=<?php print $result['id'];?>" title="Edit"><img src="../images/edit.png"  title="Edit"/></a>
        &nbsp;|&nbsp;
        <a href="create_scripts.php?action=delete&qn_id=<?php print $result['id'];?>" title="Delete" onClick="return confirmDelete();" > <img src="../images/small-bin.png" class="key_image" title="Delete"/></a>       
</td>
<td ><a href="question_listing.php?qn_id=<?php print $result['id'];?>" style="color:blue; " ><?php print $result['name']; ?></a></td>
<td ><?php print substr($result['description'],0,30); ?></td>
<td  align="center"><?php print $result['no_of_questions']; ?></td>
<td ><?php print $result['type']; ?></td> 
<td ><?php print $result['subtype']; ?></td> 
<td align="center">
        <?php 
			if ( $result['status'] == 0 ) {
				echo '<img src="../images/checked.gif"  title="Active" />';
			}
			else {
				echo '<img src="../images/cancel.png"  title="In Active" />';
			}
        ?>		
</td>
<td  align="center"><?php print $result['modifiedby']; ?></td>
<td  align="center"><?php echo display_time_diff_format($result['modified'],1);?></td>
</tr>
<?php
}
?>
</table>
<br/>
<br/>
   </div>
 </div>
 </div>
</div>


<?php
include "lcas_footer.php";
?>
