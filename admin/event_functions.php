<?php
session_start();
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
if(isset($_REQUEST['type'])){
	if($_REQUEST['type'] == 'fetchEvents'){
//		$starttime = $_REQUEST['start'];
//		$endtime = $_REQUEST['end'];
		$start = date('Y-m-01'); 
		//$end  = date('Y-m-t');

		$nxt7date = strtotime("+6 day");
	
		$end=date('Y-m-d', $nxt7date);

	$cur_editeventid=$_REQUEST['editeventid'];

	$starttime=strtotime($start);
	$endtime=strtotime($end); 
	$createdby=get_session('LOGIN_USERID');

	$sql="SELECT id,lead_id,title,start,end,allDay,appttype,createdby
			FROM tps_events 
			WHERE createdby='".$createdby."' and delete_flag='0' AND start >= '".date("Y-m-d H:i:s", $starttime)."' 
			AND end <= '".date("Y-m-d H:i:s", $endtime)."' ";
//echo $sql;
		$result=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($result)>0)
		{
			while ($record = mysql_fetch_array($result)) {

				$titlesplit = explode("-", $record['title']);
				
				$titlesplit[0]=trim($titlesplit[0]);
				$titlesplit[1]=trim($titlesplit[1]);

			   if($record['lead_id']!=$cur_editeventid)
			   {	
				if($record['appttype']=="Demo")
				{
					$event_array[] = array(
				    		'id' => $record['id'],
				    		'title' => $record['title'],
				    		'start' => $record['start'],
				    		'end' => $record['end'],
				    		'allDay' => (bool)$record['allDay'],
				    		'address' => $titlesplit['1'],
				    		'appttype' => $record['appttype'],
			            		'backgroundColor'=>"#3A87AD",
				    		'borderColor'=>"#3A87AD",
				    		'textColor'=>"#fff"
						
					);
				}else if($record['appttype']=="Training"){

					$event_array[] = array(
				    		'id' => $record['id'],
				    		'title' => $record['title'],
				    		'start' => $record['start'],
				    		'end' => $record['end'],
				    		'allDay' => (bool)$record['allDay'],
				    		'address' => $titlesplit['1'],
				    		'appttype' => $record['appttype'],
			            		'backgroundColor'=>"#0B610B",
				    		'borderColor'=>"#0B610B",
				    		'textColor'=>"#fff",
					);

				}else if($record['appttype']=="AdvancedTrainingClinics"){

					$event_array[] = array(
				    		'id' => $record['id'],
				    		'title' => $record['title'],
				    		'start' => $record['start'],
				    		'end' => $record['end'],
				    		'allDay' => (bool)$record['allDay'],
				    		'address' => $titlesplit['1'],
				    		'appttype' => $record['appttype'],
			            		'backgroundColor'=>"#F5A9A9",
				    		'borderColor'=>"#F5A9A9",
				    		'textColor'=>"#000"
					);
	
				}

			   }else{
				
				if($record['appttype']=="Demo")
				{
					$event_array[] = array(
				    		'id' => $record['id'],
				    		'title' => $record['title'],
				    		'start' => $record['start'],
				    		'end' => $record['end'],
				    		'allDay' => (bool)$record['allDay'],
				    		'address' => $titlesplit['1'],
				    		'appttype' => $record['appttype'],
			            		'backgroundColor'=>"#3A87AD",
				    		'borderColor'=>"#FFF000",
				    		'textColor'=>"#fff"
					);
				}else if($record['appttype']=="Training"){

					$event_array[] = array(
				    		'id' => $record['id'],
				    		'title' => $record['title'],
				    		'start' => $record['start'],
				    		'end' => $record['end'],
				    		'allDay' => (bool)$record['allDay'],
				    		'address' => $titlesplit['1'],
				    		'appttype' => $record['appttype'],
			            		'backgroundColor'=>"#0B610B",
				    		'borderColor'=>"#0B610B",
				    		'textColor'=>"#fff"
					);

				}else if($record['appttype']=="AdvancedTrainingClinics"){

					$event_array[] = array(
				    		'id' => $record['id'],
				    		'title' => $record['title'],
				    		'start' => $record['start'],
				    		'end' => $record['end'],
				    		'allDay' => (bool)$record['allDay'],
				    		'address' => $titlesplit['1'],
				    		'appttype' => $record['appttype'],
			            		'backgroundColor'=>"#F5A9A9",
				    		'borderColor'=>"#F5A9A9",
				    		'textColor'=>"#000"
					);
	
				}

			   }


			}
		}else{
			$event_array[] = array(
			    'id' => "",
			    'title' => "",
			    'start' => "",
			    'end' => "",
			    'allDay' => 0,
			    'address' => "",
			    'appttype' => "",
			    'backgroundColor'=>"",
			    'borderColor'=>"",
			    'textColor'=>""
			);
		}

		echo json_encode($event_array);
	}
	else if($_REQUEST['type'] == 'fetchDealerEvents'){
		
		$start = date('Y-m-d'); 

		$nxt7date = strtotime("+15 day");
	
		$end=date('Y-m-d', $nxt7date);

	$dealeruserid=$_REQUEST['dealeruserid'];

	/*$seldate=date('Y-m-d',strtotime($_REQUEST['selecteddate']));

	$st=$seldate." 08:00:00";
	$ed=$seldate." 23:00:00";
	*/

	/*$starttime=strtotime($start);
	$endtime=strtotime($end); 

	$createdby=get_session('LOGIN_USERID');*/

	$starttime=strtotime($start);
	$endtime=strtotime($end); 

	$sql="SELECT id,lead_id,title,start,end,allDay,appttype,createdby
			FROM tps_events 
			WHERE createdby='".$dealeruserid."' and delete_flag='0' AND start >= '".date("Y-m-d H:i:s", $starttime)."' 
			AND end <= '".date("Y-m-d H:i:s", $endtime)."' ";
//echo $sql;
		$result=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($result)>0)
		{
			while ($record = mysql_fetch_array($result)) {

				$titlesplit = explode("-", $record['title']);
				
				$titlesplit[0]=trim($titlesplit[0]);
				$titlesplit[1]=trim($titlesplit[1]);

			   
				if($record['appttype']=="Demo")
				{
					$event_array[] = array(
				    		'id' => $record['id'],
				    		'title' => $record['title'],
				    		'start' => $record['start'],
				    		'end' => $record['end'],
				    		'allDay' => (bool)$record['allDay'],
				    		'address' => $titlesplit['1'],
				    		'appttype' => $record['appttype'],
			            		'backgroundColor'=>"#3A87AD",
				    		'borderColor'=>"#3A87AD",
				    		'textColor'=>"#fff"
					);
				}else if($record['appttype']=="Training"){

					$event_array[] = array(
				    		'id' => $record['id'],
				    		'title' => $record['title'],
				    		'start' => $record['start'],
				    		'end' => $record['end'],
				    		'allDay' => (bool)$record['allDay'],
				    		'address' => $titlesplit['1'],
				    		'appttype' => $record['appttype'],
			            		'backgroundColor'=>"#0B610B",
				    		'borderColor'=>"#0B610B",
				    		'textColor'=>"#fff"
					);

				}else if($record['appttype']=="AdvancedTrainingClinics"){

					$event_array[] = array(
				    		'id' => $record['id'],
				    		'title' => $record['title'],
				    		'start' => $record['start'],
				    		'end' => $record['end'],
				    		'allDay' => (bool)$record['allDay'],
				    		'address' => $titlesplit['1'],
				    		'appttype' => $record['appttype'],
			            		'backgroundColor'=>"#F5A9A9",
				    		'borderColor'=>"#F5A9A9",
				    		'textColor'=>"#000"
					);
	
				}

			   

			}
		}else{
			$event_array[] = array(
			    'id' => "",
			    'title' => "",
			    'start' => "",
			    'end' => "",
			    'allDay' => 0,
			    'address' => "",
			    'appttype' => "",
			    'backgroundColor'=>"",
			    'borderColor'=>"",
			    'textColor'=>""
			);
		}

		echo json_encode($event_array);
	}
	elseif($_REQUEST['type'] == 'addEvent'){
		$start = date("Y-m-d H:i:s",$_REQUEST['start']);
		$end = date("Y-m-d H:i:s",$_REQUEST['end']);
		$title = $_REQUEST['title'];
		$elid=$_REQUEST['leadId'];

		if($elid==0)
		{
			echo "Please Add as a Lead First";
			exit;
		}else{
			$leadId =$elid;
		}

		$allDay = $_REQUEST['allDay'];
		$createdby=get_session('LOGIN_USERID');
		
		$sres=mysql_query("select start,appttype from tps_events where lead_id = '".$leadId."' and delete_flag='0'") or die(mysql_error());
		$sr=mysql_num_rows($sres);

		 if($sr==0)
		{
			$sql="INSERT INTO tps_events(start,end,title,lead_id,allDay,createdby) 
				VALUES('".$start."','".$end."','".$title."','".$leadId."','".$allDay."','".$createdby."')";
			$result=mysql_query($sql) or die(mysql_error());
			//echo mysql_insert_id();
			echo "0";
		}else {
			$s=mysql_fetch_array($sres);
			echo "The Requested Appointment Details already Exists on <b>".date('m/d/Y H:i:s A',strtotime($s['start']))."</b> with the  Appointment Type of <b>".$s['appttype']."</b>";
		}

	}
	elseif($_REQUEST['type'] == 'editEvent'){
		$eventId = $_REQUEST['id'];
		$sql="SELECT  * 
			FROM tps_events 
			WHERE id =".$eventId;
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_assoc($result);
		$popupHtml = genPopup($row);
		$startSecs = getSec($row['start']);
		$endSecs = getSec($row['end']);
		$event_edit = array(
			      'popupHtml' => $popupHtml,
			      'startSecs' => $startSecs,
			      'endSecs' => $endSecs
			  	);
		echo json_encode($event_edit);
	}
	elseif($_REQUEST['type'] == 'editDropEvent'){
		$eventId = $_REQUEST['id'];
		$popupHtml = genPopupRemarks();
		$event_edit = array(
			      'popupHtml' => $popupHtml,
			      'id' => $eventId
			  	);
		echo json_encode($event_edit);
	}
	elseif($_REQUEST['type'] == 'DeleteEvent'){
		$eventId = $_REQUEST['eventid'];
                $lead_id=$_REQUEST['eventid1'];
		mysql_query("update tps_events set delete_flag='1' where id='".$eventId."'") or die(mysql_error());
		echo json_encode("1");
echo $lead_id;
mysql_query("update tps_appt_status_update set delete_flag='1' where lead_id='".$lead_id."'") or die(mysql_error());
		//mysql_query("update tps_appt_status_update set delete_flag='1' where id='".$eventId."'") or die(mysql_error());
	}
	elseif($_REQUEST['type'] == 'saveEvent'){
		$eventId = $_REQUEST['id'];
		$remarks=$_REQUEST['remarks'];
		$starttime=strtotime($_REQUEST['start']);
		$endtime=strtotime($_REQUEST['end']);
		$allDay=$_REQUEST['allDay'];
		$sql="UPDATE tps_events set remarks='$remarks', 
			start='".date("Y-m-d H:i:s", $starttime)."', 
			end='".date("Y-m-d H:i:s", $endtime)."', allDay='$allDay' 
			WHERE id =".$eventId;
		$result=mysql_query($sql) or die(mysql_error());
		
		echo "Successfully Updated";
	}
	elseif($_REQUEST['type'] == 'editsaveEvent'){
		$eventId = $_REQUEST['id'];
		$title=$_REQUEST['title'];
		$type1=$_REQUEST['type1'];
		
		$starttime=strtotime($_REQUEST['start']);
		$endtime=strtotime($_REQUEST['end']);

		$sql="UPDATE tps_events set title='$title', appttype='$type1', 
			start='".date("Y-m-d H:i:s", $starttime)."', 
			end='".date("Y-m-d H:i:s", $endtime)."' 
			WHERE id =".$eventId;
		
		if($type1=="Training"){
	
		$starttime=$_REQUEST['start'];
		$endtime=$_REQUEST['end'];

		$re=mysql_query("select * from tps_training_details where trainingid='0' and eventid='".$eventId."'") or die(mysql_error());
		$n=mysql_num_rows($re);
		
		$cur_userid=get_session('LOGIN_USERID');
		$cur_username=get_session('LOGIN_NAME');

		$memberslist=implode(",", $_REQUEST['memberslist']);

		$recuserids=explode(',',$memberslist);
		
		if($n>0)
		{
		    $j=0;
		    while($r=mysql_fetch_array($re))
		    {
			for($i=0;$i<count($recuserids);$i++)
			{
				$rcid=explode('-',$recuserids[$i]);

				if($rcid[0]==$r['receiveruserid'])
				{
					$sql1="UPDATE `tps_training_details` SET `trainingid`='".$trainingname."', `trainingdesc`='".$trndesc."', `starttime`='".$starttime."',  `endtime`='".$endtime."', `lastupddate`='".date("Y-m-d H:i:s")."',  `lastupdby`='".$cur_username."' WHERE `id` = '".$r['id']."' and `receiveruserid` = '".$rcid[0]."' and `eventid`='".$eventId."'";
					$result=mysql_query($sql1) or die(mysql_error());
				}
				
			}
			$j++;
		    }
		}else{
		
			for($i=0;$i<count($recuserids);$i++)
			{
				$rcid=explode('-',$recuserids[$i]);

			$sql1="INSERT INTO `tps_training_details` (`id`, `trainingid`, `eventid`, `creatoruserid`, `creatorusername`, `receiveruserid`,  `starttime`, `endtime`, `createddate`, `createdby`) VALUES (NULL, '0', '".$eventId."', '".$cur_userid."', '".$cur_username."', '".$rcid[0]."', '".$starttime."', '".$endtime."', '".date("Y-m-d H:i:s")."', '".$cur_username."')";
		
			$result=mysql_query($sql1) or die(mysql_error());

			}
		}

		

	     }

		$result=mysql_query($sql) or die(mysql_error());

		echo "Successfully Updated";
	}
	

}
function genPopup($row){
	$startDate = date('d/m/Y',strtotime($row['start']));
	
	$titlesplit = explode("-", $row['title']);
	$titlesplit[0]=trim($titlesplit[0]);
	$titlesplit[1]=trim($titlesplit[1]);
	$titlesplit[2]=trim($titlesplit[2]);

	$appttype=$row['appttype'];

	$eventid=$row['id'];	
	
	$html = '<div class="box"><div class="box-header"><span class="title">Edit Appoinment</span></div>
		<div class="box-content" align="center">
		<table style="width: 100%; border-spacing: 8px; border-collapse: separate;" id="edit-event-dialog-table">
		<tbody>
			<tr>
		    <td>Name </td>
		    <td><input type="text" id="edit-event-dialog-title1" value="'.$titlesplit[0].'"></td>
		  </tr>
		  <tr>
		    <td>Address </td>
		    <td><textarea id="edit-event-dialog-title2" rows="3">'.$titlesplit[1].' </textarea></td></tr>
			<tr>
		    <td>Phone# </td>
		    <td><input type="text" id="edit-event-dialog-title3" value="'.$titlesplit[2].'" disabled></td>
		  </tr>		<tr>
				<td>Type </td>
				<td>
					<select id="edit-event-dialog-type" style="width: 150px;">'.getEventType($appttype).'</select>
		    </td>
		   <tr>
		  	<td style="width: 100px;">Date </td>
		    <td>
		    	<span class="group">
		      	<input type="text" class="date-input" id="edit-event-dialog-start-date" data-date-format="dd/mm/yyyy" value="'.$startDate.'">
		      	<select id="event-time-from">
							<option value="0">12:00 AM</option><option value="1800">12:30 AM</option><option value="3600">01:00 AM</option>
							<option value="5400">01:30 AM</option><option value="7200">02:00 AM</option><option value="9000">02:30 AM</option>
							<option value="10800">03:00 AM</option><option value="12600">03:30 AM</option><option value="14400">04:00 AM</option>
							<option value="16200">04:30 AM</option><option value="18000">05:00 AM</option><option value="19800">05:30 AM</option>
							<option value="21600">06:00 AM</option><option value="23400">06:30 AM</option><option value="25200">07:00 AM</option>
							<option value="27000">07:30 AM</option><option value="28800">08:00 AM</option><option value="30600">08:30 AM</option>
							<option value="32400">09:00 AM</option><option value="34200">09:30 AM</option><option value="36000">10:00 AM</option>
							<option value="37800">10:30 AM</option><option value="39600">11:00 AM</option><option value="41400">11:30 AM</option>
							<option value="43200">12:00 PM</option><option value="45000">12:30 PM</option><option value="46800">01:00 PM</option>
							<option value="48600">01:30 PM</option><option value="50400" selected="selected">02:00 PM</option>
							<option value="52200">02:30 PM</option><option value="54000">03:00 PM</option><option value="55800">03:30 PM</option>
							<option value="57600">04:00 PM</option><option value="59400">04:30 PM</option><option value="61200">05:00 PM</option>
							<option value="63000">05:30 PM</option><option value="64800">06:00 PM</option><option value="66600">06:30 PM</option>
							<option value="68400">07:00 PM</option><option value="70200">07:30 PM</option><option value="72000">08:00 PM</option>
							<option value="73800">08:30 PM</option><option value="75600">09:00 PM</option><option value="77400">09:30 PM</option>
							<option value="79200">10:00 PM</option><option value="81000">10:30 PM</option><option value="82800">11:00 PM</option>
							<option value="84600">11:30 PM</option>
						</select>
						to
						<select id="event-time-to">
							<option value="0">12:00 AM</option><option value="1800">12:30 AM</option><option value="3600">01:00 AM</option>
							<option value="5400">01:30 AM</option><option value="7200">02:00 AM</option><option value="9000">02:30 AM</option>
							<option value="10800">03:00 AM</option><option value="12600">03:30 AM</option><option value="14400">04:00 AM</option>
							<option value="16200">04:30 AM</option><option value="18000">05:00 AM</option><option value="19800">05:30 AM</option>
							<option value="21600">06:00 AM</option><option value="23400">06:30 AM</option><option value="25200">07:00 AM</option>
							<option value="27000">07:30 AM</option><option value="28800">08:00 AM</option><option value="30600">08:30 AM</option>
							<option value="32400">09:00 AM</option><option value="34200">09:30 AM</option><option value="36000">10:00 AM</option>
							<option value="37800">10:30 AM</option><option value="39600">11:00 AM</option><option value="41400">11:30 AM</option>
							<option value="43200">12:00 PM</option><option value="45000">12:30 PM</option><option value="46800">01:00 PM</option>
							<option value="48600">01:30 PM</option><option value="50400" selected="selected">02:00 PM</option>
							<option value="52200">02:30 PM</option><option value="54000">03:00 PM</option><option value="55800">03:30 PM</option>
							<option value="57600">04:00 PM</option><option value="59400">04:30 PM</option><option value="61200">05:00 PM</option>
							<option value="63000">05:30 PM</option><option value="64800">06:00 PM</option><option value="66600">06:30 PM</option>
							<option value="68400">07:00 PM</option><option value="70200">07:30 PM</option><option value="72000">08:00 PM</option>
							<option value="73800">08:30 PM</option><option value="75600">09:00 PM</option><option value="77400">09:30 PM</option>
							<option value="79200">10:00 PM</option><option value="81000">10:30 PM</option><option value="82800">11:00 PM</option>
							<option value="84600">11:30 PM</option>
						</select>
		      </span>
		    </td>
		  </tr>';

	if($row['remarks'] != ''){
		$html .= '<tr><td>Remarks </td><td>'.$row['remarks'].'</td></td></tr>';
	}	
		$html .= '<tr><td>Created By</td><td>'.getEventCreator($row['createdby']).'</td></tr>
			  <tr><td colspan="2"><input type="hidden" value='.$row['id'].' id="event-id-hd"><input type="hidden" value='.$row['lead_id'].' id="event-id-hd1">
					<div class="buttons">
						<a id="save-event-edit" class="btn btn-blue" >Save</a>
						<a id="cancel-event-edit" class="btn btn-default" >Cancel</a>
						<a id="delete-event-edit" class="btn btn-red">Delete this Event</a>
					</div>				
				</td>
			</tr>';
	
	$html .= '</tbody></table></div></div><br>';

	return $html;
}
function getSec($Date){
	$startHours = (int)date('H',strtotime($Date));
	$startMins = (int)date('i',strtotime($Date));
	$startSecs = ($startHours*60*60)+($startMins*60);
	return $startSecs;
}


function genPopupRemarks(){
	$html = 
	'<table style="width: 650px;" id="edit-drop-event-dialog-table-header">
		<tr><td>Edit Appoinment<td></tr>
	</table>
	<table style="width: 650px;" id="edit-drop-event-dialog-table">
		<tbody>
			<tr>
			    <td>Remarks : </td>
			    <td><textarea id="edit-drop-event-dialog-remarks" rows="3" cols="30"></textarea></td>
		  	</tr>
		  
			<tr>
				<td >
						<a id="save-event-edit" class="btn btn-blue" >Save</a>
						<a id="cancel-event-edit" class="btn btn-default" >Cancel</a>			
				</td>
			</tr>
		</tbody>
	</table>';
	return $html;
}
?>
