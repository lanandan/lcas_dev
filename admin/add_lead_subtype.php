<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();
$page_name = "add_lead_subtype.php";
$page_title = $site_name." Add / Edit Lead Sub type";
$cur_page="lead_subtype";

$lead_type='';
$lead_sub_type='';
$lead_sub_type_disp='';
$status='';
$script_id='';
$l_id='';

if(request_get('action')){
	if ( request_get('action') == "do"  )
	{
		$flag = true;
		//set the values from post profile
		$lead_sub_type=isset($_POST['lead_sub_type'])?safe_sql_nq(trim($_POST['lead_sub_type'])):'';
		$lead_sub_type_disp=isset($_POST['lead_sub_type_disp'])?safe_sql_nq(trim($_POST['lead_sub_type_disp'])):'';
		$status=isset($_POST['lead_status'])?trim($_POST['lead_status']):'';
		$parent=isset($_POST['parent'])?safe_sql_nq(trim($_POST['parent'])):'';
		$timestamp =  time();

		$l_id = request_get('l_id');
		if ($l_id > 0)
		{
			//update databse
			$sql= "update tps_lead_subtype set ".
					" subtype_displayname = '". $lead_sub_type_disp ."', ".
					" parent = '". $parent ."', ".
					" status = '". $status ."', ".		
					" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
					" modified = '". $timestamp ."' ".
					" where id = '".$l_id."' " ;
		}
		else{
			//update databse

		$sql= "insert into tps_lead_subtype set ".
					" lead_subtype = '". $lead_sub_type ."', ".
					" subtype_displayname = '". $lead_sub_type_disp ."', ".
					" parent = '". $parent ."', ".
					" status = '". $status ."', ".
								
					" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
					" modified = '". $timestamp ."' " ;

		}
			
		mysql_query($sql) or die(mysql_error());

		$url = "Location: lead_subtype_listing.php";
		header($url);
		exit();	
	}
	if ( request_get('action') == "edit"  )
	{
		$l_id = request_get('l_id');
		$sql_qry = "select * from tps_lead_subtype where id = $l_id ";

		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$lead_sub_type=$row['lead_subtype'];
			$lead_sub_type_disp=$row['subtype_displayname'];
			$ltype=$row['parent'];
			$status=$row['status'];			
		}
	}
	if ( request_get('action') == "delete"  )
	{
		$l_id = request_get('l_id');
		$sql_qry = "delete from tps_lead_subtype where id = '".$l_id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$url = "Location: lead_subtype_listing.php";
		header($url);
		exit();	
	}
}
	
include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );		

?>
<script>
$(document).ready(function(){
$("#lead_sub_type").change(function(){
var lid=$('#lid').val();
if(lid==''){
		var data = {
    			type: 'check_subtype',
			subtype:$('#lead_sub_type').val(),
			}
			$.ajax({
		    		type: "POST",
		    		url: "lead_validation.php",
		    		data: data,
		    		success: function(resp) {
				if(resp==1){
				alert('Lead Subtype already Exist');
				$('#lead_sub_type').val('');
					}
				}
				});
}
});
});
</script>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Lead Type&nbsp;&nbsp;<a class="btn btn-blue" href="lead_subtype_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit Lead Type</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" action="add_lead_subtype.php" method="POST" id="add_lead_type" name="add_lead_type">
          <div class="padded">
            <div class="form-group">
              <label class="control-label col-lg-2">Lead Type *</label>
              <div class="col-lg-4">
                	<select class="nline" name="parent" id="ltype" required><option value="">Select</option>
			<?php	
			
				 echo getLeadType($ltype); 
			 
			?>
			</select> 
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Sub Lead Type Name *</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="lead_sub_type" size="128" id="lead_sub_type" title="Sub Lead Type Abbr" value="<?php echo $lead_sub_type;?>" <?php if(request_get('action')=='edit'){echo 'disabled';}?>/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Sub Lead Type DisplayName *</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="lead_sub_type_disp" size="128" id="lead_sub_type_disp" title="Sub Lead Type Display Name" value="<?php echo $lead_sub_type_disp;?>"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="lead_status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="lead_status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

            
	    <input type="hidden" name="action" value="do" />
	    <input type="hidden" name="l_id" id="lid" value="<?php echo $l_id;?>" />

          <div class="form-actions">
            <button type="submit" class="btn btn-blue">Save changes</button>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='lead_subtype_listing.php'">Cancel</button>
          </div>
        </form>
<br/><br/>
   </div>
 </div>
 </div>

<?php
include "lcas_footer.php";
?>
