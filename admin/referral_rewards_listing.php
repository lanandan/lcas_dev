<?php
ob_start();
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "script_lead_listing.php";
$page_title = $site_name." Referral Rewards Template Listing";
$cur_page="scripts_list";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

if(isset($_REQUEST['action']))
{
	if($_REQUEST['action']=="delete")
	{
		$mid=$_REQUEST['mid'];

		mysql_query("delete from tps_ref_rewards_template where id='$mid'")or die(mysql_error());

		header("location:referral_rewards_listing.php");
		exit;
	}

}

?>
<script type="text/javascript">
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete the Template?");
	if (agree)
		return true ;
	else
		return false ;
}

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});
</script>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Referral Rewards Template Listing&nbsp;&nbsp;
		<a class="btn btn-blue" href="add_referral_rewards.php"><span>Create New Template</span></a></h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Referral Rewards Template Listing</span>
<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
</div>
<div class="box-content">
<div id="dataTables">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
<thead>
<tr>
  <th><div>Action</div></th>
  <th><div>Template Name</div></th>
  <th><div>Content</div></th>
  <th><div>Modified By</div></th>
  <th><div>Modified Time</div></th>
</tr>
</thead>
<tbody>
<?php
$sql_qry = "select * from tps_ref_rewards_template order by id desc";
$result_list = mysql_query($sql_qry) or die(mysql_error());
while($result=mysql_fetch_array($result_list)) {	
?>
<tr>
<td>
      <a href="add_referral_rewards.php?action=edit&mid=<?php print $result['id'];?>" title="Edit"><img src="../images/edit.png"  title="Edit"/></a>
        &nbsp;|&nbsp;
      <a href="referral_rewards_listing.php?action=delete&mid=<?php print $result['id'];?>" title="Delete" onClick="return confirmDelete();" > <img src="../images/small-bin.png" class="key_image" title="Delete"/></a>    
</td>
<td><?php echo $result['name']; ?></td>
<td><?php //echo $result['content']; ?></td>

<td  align="center"><?php print $result['modifiedby']; ?></td>
<td  align="center"><?php echo display_time_diff_format(strtotime($result['modifieddate']),1);?></td>
</tr>
<?php
}
?>
</table>
<br/>
<br/>
   </div>
 </div>
 </div>
</div>


<?php
include "lcas_footer.php";
ob_end_flush();
?>
