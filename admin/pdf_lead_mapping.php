<?php
ob_start();
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "pdf_lead_mapping.php";
$page_title = $site_name."PDF Mapping";
	

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

$cur_uid=get_session('LOGIN_ID');
$cur_userid=get_session('LOGIN_USERID');
$cur_email=get_session('LOGIN_EMAIL');
$cur_username=get_session('DISPLAY_NAME');

$scriptid="";
$leadtype="";
$leadsubtype="";
$dealer="";
$status="";

$action=0;

if(isset($_REQUEST['save']))
{
	$scriptid=request_get('scriptid');
	$leadtype=request_get('lead_type');
	$leadsubtype=request_get('lead_subtype');
	$dealer=request_get('dealer');
	$status=request_get('status');

	$createdate=date("Y-m-d H:i:s");

	$sltres=mysql_query("select leadtype from `tps_masterpdf_lead_mapping` where leadtype='$leadtype' and leadsubtype='$leadsubtype' and dealerid='$dealer'")or die(mysql_error());

	$n=mysql_num_rows($sltres);

	if($n>0)
	{
		echo "<script>alert('Requested Lead Type already available in the mapping!')</script>";
	}else{
	
		$sql="INSERT INTO `tps_masterpdf_lead_mapping` (`id`, `pdf_temp_id`, `leadtype`, `leadsubtype`, `dealerid`, `status`, `createddate`, `createdby`) VALUES (NULL, '$scriptid', '$leadtype', '$leadsubtype', '$dealer', '$status', '$createdate', '$cur_username')";

		mysql_query($sql)or die(mysql_error());

		$message="PDF Lead Type Mapped Successfully";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		
		header("location:pdf_lead_mapping.php?suc=1");
		exit;
	}

}

if(isset($_REQUEST['update']))
{
	$mid=request_get('mid');
	$scriptid=request_get('scriptid');
	$leadtype=request_get('lead_type');
	$leadsubtype=request_get('lead_subtype');
	$dealer=request_get('dealer');
	$status=request_get('status');

	$createdate=date("Y-m-d H:i:s");

	$sltres=mysql_query("select leadtype from `tps_masterpdf_lead_mapping` where leadtype='$leadtype' and leadsubtype='$leadsubtype' and dealerid='$dealer' and status='$status'")or die(mysql_error());

	$n=mysql_num_rows($sltres);

	if($n>0)
	{
		echo "<script>alert('Requested Lead Type already available in the mapping!')</script>";
	}else{
	
		$sql="UPDATE `tps_masterpdf_lead_mapping` SET `pdf_temp_id` = '$scriptid', `leadtype` = '$leadtype', `leadsubtype` = '$leadsubtype', `dealerid` = '$dealer', `status` = '$status', `modified` = '$createdate', `modifiedby` = '$cur_username' WHERE `id` = '$mid'";

		mysql_query($sql)or die(mysql_error());

		$message="PDF Lead Type Mapping Updated Successfully";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		
		header("location:pdf_lead_listing.php?suc=1");
		exit;
	}

}


if(isset($_REQUEST['action']))
{
	if($_REQUEST['action']=="edit")
	{
		$mid=$_REQUEST['mid'];
		$res=mysql_query("select * from tps_masterpdf_lead_mapping where id='$mid'")or die(mysql_error());
		$m=mysql_fetch_array($res);

		$scriptid=$m['pdf_temp_id'];
		$leadtype=$m['leadtype'];
		$leadsubtype=$m['leadsubtype'];
		$dealer=$m['dealerid'];
		$status=$m['status'];
		$action=1;
	}
}
?>
<script type="text/javascript">

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});

function validate(){

	var scriptid=document.getElementById('scriptid').value;
	var leadtype=document.getElementById('lead_type').value;
	
	if(scriptid==0)
	{
		alert("Please Select PDF Name!");
		return false;
	}
	else if(leadtype==0)
	{
		alert("Please Select Lead Type!");
		return false;
	}else{
		return true;
	}

}
</script>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit PDF Mapping &nbsp;&nbsp;<a class="btn btn-blue" href="pdf_lead_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit PDF Mapping</span>

<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" method="POST" onsubmit="javascript: return validate();">

          <div class="padded">

            <div class="form-group">
              <label class="control-label col-lg-2">PDF Template Name *</label>
              <div class="col-lg-4">
                <select id="scriptid" name="scriptid" class="uniform" >
			<option value="0">Select One</option>
			<?php 
				$sres=mysql_query("select id,name from tps_pdf_templates where status='0'")or die(mysql_error());
				while($sr=mysql_fetch_array($sres))
				{
					if($scriptid==$sr['id'])
						echo "<option value=".$sr['id']." selected >".$sr['name']."</option>";
					else 
						echo "<option value=".$sr['id'].">".$sr['name']."</option>";
				}				
			?>
		</select>
              </div>
            </div>

           <div class="form-group">
              <label class="control-label col-lg-2">Select Lead Type *</label>
              <div class="col-lg-4">
		<select name="lead_type" id="lead_type" class="uniform" onchange="showsubtype(this);" >
		<option value="0" class="Select">Select One</option>
		<?php echo getLeadType($leadtype); ?>
		</select>
              </div>
            </div>

	 <div class="form-group">
              <label class="control-label col-lg-2">Select Lead Subtype </label>

              <div class="col-lg-4">
		<select name="lead_subtype" id="lsubtype" class="uniform" >
			  <option value="" class="Select">Select One</option>
	<?php	
		echo getLeadSubType2($leadsubtype,$leadtype); 

	?>
		</select>
              </div>
            </div>

	 <div class="form-group">
              <label class="control-label col-lg-2">Select Lead Dealer </label>

              <div class="col-lg-4">
		<select name="dealer" id="dealer" class="uniform">
		  <option value="">Select One</option>
	<?php	
		echo getTeamMembers($dealer); 
	?>
		</select>
              </div>
            </div>

 <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

         <div class="form-actions">
		<?php if($action==1){ ?>
		<input type="hidden" name="mid" id="mid" value="<?php echo $mid; ?>" />
            <button type="submit" class="btn btn-blue" name="update" >Update Details</button>
		<?php }else{ ?>
            <button type="submit" class="btn btn-blue" name="save" >Save Details</button>
	    <?php } ?>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='pdf_lead_listing.php'">Cancel</button>
          </div>
        </form>

<br/><br/>
   </div>
 </div>
 </div>
<script>

function showsubtype(t){var val=$('#lead_type option:selected').attr("class")+" showsubtype";$('#lsubtype .showsubtype').css({display:'none'});$('#lsubtype .showsubtype').removeAttr("selected");$('#lsubtype option[class="'+val+'"]').css({display:'block'});}
</script>

<?php

include "lcas_footer.php";
ob_end_flush();

?>
