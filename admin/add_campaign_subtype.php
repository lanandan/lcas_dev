<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();
$page_name = "add_campaign_subtype.php";
$page_title = $site_name." Add /Edit Campaign Subtype";
$cur_page="configuration";

$name='';
$display='';
$status='';
$displayorder='';
$notes='';
$id='';$parent=0;

if(request_get('action')){
	if ( request_get('action') == "do"  )
	{
		$flag = true;
		//set the values from post profile
		$name=isset($_POST['name'])?safe_sql_nq(trim($_POST['name'])):'';
		$display=isset($_POST['display'])?safe_sql_nq(trim($_POST['display'])):'';
		$displayorder=isset($_POST['displayorder'])?safe_sql_nq(trim($_POST['displayorder'])):'';
		$notes=isset($_POST['notes'])?safe_sql_nq(trim($_POST['notes'])):'';
		$parent=isset($_POST['parent'])?safe_sql_nq(trim($_POST['parent'])):'';
		$status=isset($_POST['status'])?trim($_POST['status']):'';

		$timestamp =  time();

		$id = request_get('id');
		if ($status == '') {$status='0';}
		if ($id > 0)
		{
			//update databse
			$sql= "update tps_campaign_subtype set ".
				" name = '". $name ."', ".
				" display = '". $display ."', ".
				" parent = '". $parent ."', ".
				" status = '". $status ."', ".
				" displayorder = '". $displayorder ."', ".
				" notes = '". $notes ."', ".
				" created = '". $timestamp ."', ".
				" modified = '". $timestamp ."', ".
				" createdby = '". get_session('DISPLAY_NAME') ."', ".
				" modifiedby = '". get_session('DISPLAY_NAME') ."' ".
				" where id = '".$id."' " ;
		}
		else {
			$sql= "insert into tps_campaign_subtype set ".
				" name = '". $name ."', ".
				" display = '". $display ."', ".
				" parent = '". $parent ."', ".
				" status = '". $status ."', ".
				" displayorder = '". $displayorder ."', ".
				" notes = '". $notes ."', ".
				" created = '". $timestamp ."', ".
				" modified = '". $timestamp ."', ".
				" createdby = '". get_session('DISPLAY_NAME') ."', ".
				" modifiedby = '". get_session('DISPLAY_NAME') ."' ";		
		}	
		mysql_query($sql) or die(mysql_error());

		$url = "Location: campaign_subtype_listing.php";
		header($url);
		exit();	
	}
	if ( request_get('action') == "edit"  )
	{
		$id = request_get('id');
		$sql_qry = "select * from tps_campaign_subtype where id = $id ";

		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$name=$row['name'];
			$display=$row['display'];
			$notes=$row['notes'];
			$status=$row['status'];	$parent=$row['parent'];		
			$displayorder=$row['displayorder'];			
		}
	}
	if ( request_get('action') == "delete"  )
	{
		$id = request_get('id');
		$sql_qry = "delete from tps_campaign_subtype where id = '".$id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$url = "Location: campaign_subtype_listing.php";
		header($url);
		exit();	
	}
}
	
include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );		

?>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Campaign Type &nbsp;&nbsp;<a class="btn btn-blue" href="campaign_subtype_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit Campaign Subtype</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" action="add_campaign_subtype.php" method="POST" id="add_campaign_type" name="add_campaign_type">
          <div class="padded">
		  <div class="form-group">
              <label class="control-label col-lg-2">Campaign Type *</label>
              <div class="col-lg-4">
                	<select class="nline" name="parent" id="campaign_type" >
					<option class="Select"value="Select">Select</option>
		<?php

	$sql=mysql_query("select * from tps_campaign_type") or die(mysql_error());
	while($row=mysql_fetch_array($sql)){
		
		if($parent==$row['id'])
		{
			echo "<option class='".$row['id']."'value='".$row['id']."' selected >".$row['display']."</option>";
		}else{
			echo "<option class='".$row['id']."'value='".$row['id']."' >".$row['display']."</option>";
		}

	}	

		?>
			</select> 
              </div>
            </div>
			
            <div class="form-group">
              <label class="control-label col-lg-2">Campaign Subtype Abbr*</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="name" size="128" id="name" title="Lead Status Abbr" value="<?php echo $name;?>"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Display Name *</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="display" size="128" id="display" title="Lead Status Display Name" value="<?php echo $display;?>"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Display Order</label>
              <div class="col-lg-1">
                  <select class="uniform" name="displayorder" id="displayorder">
		   <option value="" >Select</option>
		   <?php echo getDisplayOrderNum($displayorder); ?>
		  </select>
              </div>
		<label class="control-label col-lg-3">Lower Value = Top Position</label>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Notes</label>
              <div class="col-lg-4">
                <textarea name="notes" id="notes" title="Notes"  cols="75"><?php echo $notes;?></textarea><span class="counter"></span>
              </div>
            </div>

	    <input type="hidden" name="action" value="do" />
	    <input type="hidden" name="id" value="<?php echo request_get('id');?>" />

          <div class="form-actions">
            <button type="submit" class="btn btn-blue">Save changes</button>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='campaign_subtype_listing.php'">Cancel</button>
          </div>
        </form>
<br/><br/>
   </div>
 </div>
 </div>

<?php
include "lcas_footer.php";
?>
