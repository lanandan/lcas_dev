<?php
ob_start();
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");

validate_login();

$page_name = "pdf_template_mapping.php";
$page_title = $site_name."PDF Template";
	

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

$cur_uid=get_session('LOGIN_ID');
$cur_userid=get_session('LOGIN_USERID');
$cur_email=get_session('LOGIN_EMAIL');
$cur_username=get_session('DISPLAY_NAME');

$tempid="";
$tempname="";
$stage1="";
$stage2="";
$stage3="";
$emailtemplate="";

$status="";

$action=0;

	if ( isset($_REQUEST['save']) ||  isset($_REQUEST['update']) )
	{

		$flag = true;
				
		$timestamp = time();

		$tempid = request_get('mid');

		$tempname=request_get('tempname');
		$stage1=request_get('stage1');
		$stage2=request_get('stage2');
		$stage3=request_get('stage3');
		$emailtemplate=request_get('emailtemplate');

		if ($tempid > 0)
		{
			//update databse
			$sql= "update tps_pdf_templates set ".
					" `name` = '". $tempname ."', ".
					" `stage1` = '". $stage1 ."', ".
					" `stage2` = '". $stage2 ."', ".
					" `stage3`= '".$stage3 ."',".
					" `emailtemplate`= '".$emailtemplate ."',".
					" `status` = '". $status ."', ".
					" `modifieddate` = '". $timestamp ."',".
					" `modifiedby` = '". get_session('DISPLAY_NAME') ."' where id = '".$tempid."' " ;   

			mysql_query($sql) or die(mysql_error());	
			
			$error_flag = '0';
			$error_message = 'Successfully Updated PDF Template details !!';
			set_session('e_flag', '1');
			set_session('e_message', $error_message);
		}
		else {
			//insert new row
			$sql= "insert into tps_pdf_templates set ".
					" `name` = '". $tempname ."', ".
					" `stage1` = '". $stage1 ."', ".
					" `stage2` = '". $stage2 ."', ".
					" `stage3`= '".$stage3 ."',".
					" `emailtemplate`= '".$emailtemplate ."',".
					" `status` = '". $status ."', ".
					" `createddate` = '". $timestamp ."', ".			
					" `createdby` = '". get_session('DISPLAY_NAME') ."', ".
					" `modifieddate` = '". $timestamp ."',  ". 
					" `modifiedby` = '". get_session('DISPLAY_NAME') ."' ";

			mysql_query($sql) or die(mysql_error());		
			
			$error_flag = '0';
			$error_message = 'Successfully Added PDF Template details !!';
			set_session('e_flag', '1');
			set_session('e_message', $error_message);

			
		}

		$url = "Location: pdf_template_listing.php";
		header($url);
		exit();						
	}

	if ( request_get('action') == "edit"  )
	{
		$qn_id = request_get('mid');
		$sql_qry = "select * from tps_pdf_templates where id = '".$qn_id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$pdf_name=$row['name'];
			$status=$row['status'];	
			$stage1=$row['stage1'];
			$stage2=$row['stage2'];	
			$stage3=$row['stage3'];
			$emailtemplate=$row['emailtemplate'];
			$isdefault=$row['isdefault'];	
		}
	}
	if ( request_get('action') == "delete"  )
	{
		$qn_id = request_get('mid');
		$sql_qry = "delete from tps_scripts where id = '".$qn_id."' ";
		//$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$sql_qry_child = "delete from tps_scripts_questions where script_id = '".$qn_id."' ";
		//$res_qry = mysql_query($sql_qry_child) or die(mysql_error());
		
		$sql_qry_map = "delete from tps_script_lead_mapping where script_id = '".$qn_id."' ";
		//$res_qry = mysql_query($sql_qry_map) or die(mysql_error());
		
		$url = "Location: scripts_listing.php";
		//header($url);
		exit();						
		
	}



?>
<script type="text/javascript">

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});

function validate(){

	var scriptid=document.getElementById('scriptid').value;
	var leadtype=document.getElementById('lead_type').value;
	
	if(scriptid==0)
	{
		alert("Please Select PDF Name!");
		return false;
	}
	else if(leadtype==0)
	{
		alert("Please Select Lead Type!");
		return false;
	}else{
		return true;
	}

}
</script>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit PDF Template &nbsp;&nbsp;<a class="btn btn-blue" href="pdf_template_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit PDF Template</span>

<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" method="POST">

          <div class="padded">

            <div class="form-group">
              <label class="control-label col-lg-2">PDF Template Name *</label>
              <div class="col-lg-4">
                	<input type="text" name="tempname" id="tempname" required value="<?php echo $pdf_name; ?>"/>
              </div>
            </div>

           <div class="form-group">
              <label class="control-label col-lg-2">Stage 1 PDF </label>
              <div class="col-lg-4">
		<select id="stage1" name="stage1" class="uniform" >
			<option value="0">Select One</option>
			<?php 
				$sres=mysql_query("select id,name from tps_master_pdfs where status='0'")or die(mysql_error());
				while($sr=mysql_fetch_array($sres))
				{
					if($stage1==$sr['id'])
						echo "<option value=".$sr['id']." selected >".$sr['name']."</option>";
					else 
						echo "<option value=".$sr['id'].">".$sr['name']."</option>";
				}				
			?>
		</select>
              </div>
            </div>

	 <div class="form-group">
              <label class="control-label col-lg-2">Stage 2 PDF </label>

              <div class="col-lg-4">
		<select id="stage2" name="stage2" class="uniform" >
			<option value="0">Select One</option>
			<?php 
				$sres=mysql_query("select id,name from tps_master_pdfs where status='0'")or die(mysql_error());
				while($sr=mysql_fetch_array($sres))
				{
					if($stage2==$sr['id'])
						echo "<option value=".$sr['id']." selected >".$sr['name']."</option>";
					else 
						echo "<option value=".$sr['id'].">".$sr['name']."</option>";
				}				
			?>
		</select>
              </div>
            </div>

	 <div class="form-group">
              <label class="control-label col-lg-2">Stage 3 PDF</label>

              <div class="col-lg-4">
		<select id="stage3" name="stage3" class="uniform" >
			<option value="0">Select One</option>
			<?php 
				$sres=mysql_query("select id,name from tps_master_pdfs where status='0'")or die(mysql_error());
				while($sr=mysql_fetch_array($sres))
				{
					if($stage3==$sr['id'])
						echo "<option value=".$sr['id']." selected >".$sr['name']."</option>";
					else 
						echo "<option value=".$sr['id'].">".$sr['name']."</option>";
				}				
			?>
		</select>
              </div>
            </div>

	 <div class="form-group">
              <label class="control-label col-lg-2">Email Template PDF</label>

              <div class="col-lg-4">
		<select id="emailtemplate" name="emailtemplate" class="uniform" >
			<option value="0">Select One</option>
			<?php 
				$sres=mysql_query("select id,name from tps_master_pdfs where status='0'")or die(mysql_error());
				while($sr=mysql_fetch_array($sres))
				{
					if($stage3==$sr['id'])
						echo "<option value=".$sr['id']." selected >".$sr['name']."</option>";
					else 
						echo "<option value=".$sr['id'].">".$sr['name']."</option>";
				}				
			?>
		</select>
              </div>
            </div>


 <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

         <div class="form-actions">
		<?php if($action==1){ ?>
		<input type="hidden" name="mid" id="mid" value="<?php echo $mid; ?>" />
            <button type="submit" class="btn btn-blue" name="update" >Update Details</button>
		<?php }else{ ?>
            <button type="submit" class="btn btn-blue" name="save" >Save Details</button>
	    <?php } ?>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='pdf_lead_listing.php'">Cancel</button>
          </div>
        </form>

<br/><br/>
   </div>
 </div>
 </div>
<script>

function showsubtype(t){var val=$('#lead_type option:selected').attr("class")+" showsubtype";$('#lsubtype .showsubtype').css({display:'none'});$('#lsubtype .showsubtype').removeAttr("selected");$('#lsubtype option[class="'+val+'"]').css({display:'block'});}
</script>

<?php

include "lcas_footer.php";
ob_end_flush();

?>
