<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();
$page_name = "add_questions_toscripts.php";
$page_title = $site_name." Add / Remove Questions to Scripts";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
 <style>
#sortable1, #sortable2 
{ 
	list-style-type: none; 
	margin: 0; 
	padding: 0 0 2.5em; 
	float: left; 
	margin-right: 10px;
	width: 100%;
	z-index:999;
}
#sortable1 li, #sortable2 li 
{ 
	margin: 0 5px 5px 5px; 
	padding: 8px; 
	font-size: 13px; 
	width: 100%; 
	cursor:move;
	z-index:999;
}
.highlight {
    border: 1px dashed grey;
    font-weight: bold;
    margin: 0 5px 5px 5px; 
    padding: 8px; 
    font-size: 13px;
}

.padded{
	overflow: hidden;
}

#sticky {
    font-size: 13px;
    width:630px;
}
#sticky.stick {
    position: fixed;
    top: 0;
    z-index: 1000;
    width:630px;
}

</style>

<script language="JavaScript">

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function delscriptquestion(id){

	var res=confirm("Are you sure do you want to delete this Question from the Script?");

	var script_id=getParameterByName('qn_id');
	
	if(res==true)
	{
		var data={
				type:"delscriptquestion",
				delid:id,
				scriptid:script_id
		}
		$.ajax({
			type:"POST",
			url:"script_question_actions.php",
			data:data,
			success:function(output){
				
				 $("#sortable1").load('show_after_content.php?qn_id='+script_id);
			}
		});
	}

}


function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
    } else {
        $('#sticky').removeClass('stick');
    }
}
  

 $(function() {

	 $(window).scroll(sticky_relocate);
	 sticky_relocate();

	$( "#sortable1" ).sortable({
		connectWith: ".connectedSortable",
		receive: function( event, ui ) {
			
			var qid = ui.item.attr('id').match(/mqid-([0-9]+)/).pop();
             	 	var index = ui.item.index();
			var script_id=getParameterByName('qn_id');

			//alert("Question ID : "+qid+" ; Index : "+index+" ; Script ID : "+script_id);

			var data={
					type:"addnew",
					scriptid:script_id,
					selqid:qid,
					displayorder:index
			}
			$.ajax({
				type:"POST",
				url:"script_question_actions.php",
				data:data,
				success:function(output){

					if(output==1)
					{
						alert("Question already available in the Script");
					}
					$("#sortable1").load('show_after_content.php?qn_id='+script_id);
					
				}
	
			});
		},
   		update : function (event, ui) {
        	
			var script_id=getParameterByName('qn_id');
			var newOrder = $('#sortable1').sortable('toArray').toString();
			
//			alert(newOrder);

			var data={
				type:"sortorder",
				scriptid:script_id,
				displayorder:newOrder
			}
			$.ajax({
				type:"POST",
				url:"script_question_actions.php",
				data:data,
				success:function(output){
					$("#sortable1").load('show_after_content.php?qn_id='+script_id);
				}
	
			});
    		}
	});
	
	$("#sortable2").sortable({
		connectWith: "ul",
		placeholder: "highlight",
		helper: 'clone',
		start: function (event, ui) {
                        ui.item.toggleClass("highlight");
                },
                stop: function (event, ui) {
                        ui.item.toggleClass("highlight");
                },
		remove: function(event, ui) {
                	ui.item.clone().appendTo('#sortable1');
                	$(this).sortable('cancel');
            	}
	});

	$( "#sortable1, #sortable2" ).disableSelection();

});

</script>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Remove Master Questions to Scripts &nbsp;&nbsp;<a class="btn btn-blue" href="question_listing.php?qn_id=<?php echo get_session('qn_id'); ?>"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>


 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Remove Master Questions to Scripts</span>
   </div>
   <div class="box-content">
      <div class="padded">  
<?php
$script_id=get_session('qn_id');
$sql2 ="SELECT name, no_of_questions from tps_scripts where id = $script_id ";
$result_list2=mysql_query($sql2) or die(mysql_error());
$result2 = mysql_fetch_array($result_list2);
?>

	<div class="row">
		<div class="col-md-6">
			<div id="sticky-anchor"></div>
			<div class="box" id="sticky" class="scrollable" >
				<div class="box-header"><span class="title"><?php echo $result2['name']; ?> - Questions</span></div>
				<div class="box-content padded scrollable" style="height:400px; overflow-y: auto" id="cur_scripts_div">

	<ul id="sortable1">
	<?php				
	$scriptid=request_get('qn_id');

	$sqres=mysql_query("SELECT q.id,q.name,q.type, sq.id as sqid, sq.script_id,sq.question_id,sq.displayorder FROM tps_questions q JOIN tps_scripts_questions sq ON sq.question_id=q.id WHERE sq.script_id='$scriptid' order by sq.displayorder ASC")or die(mysql_error());
	$i=1;
			
	while($sqr=mysql_fetch_array($sqres))
	{
		echo '<li class="ui-state-default" id="'.$sqr['sqid'].'" >'.$sqr['name'].'<button class="close" onclick="javascript:delscriptquestion('.$sqr['sqid'].');" type="button">×</button></li>';
		$i++;
	}
	?>
	</ul>
					
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box">
				<div class="box-header"><span class="title">Master Questions</span></div>
					<div class="box-content padded" style="min-height:350px;">
					
					<ul id="sortable2" class="connectedSortable">
			<?php
				$mqres=mysql_query("select * from tps_questions where status='0'")or die(mysql_error());
				$i=1;
				while($mqr=mysql_fetch_array($mqres))
				{
					echo '<li class="ui-state-highlight" id="mqid-'.$mqr['id'].'">'.$mqr['name'].'</li>';
					$i++;
				}
			?>
					</ul>
				</div>
			</div>
		</div>
	</div>

      </div>
   </div>
 </div>
 </div>

<?php

include "lcas_footer.php";
?>
