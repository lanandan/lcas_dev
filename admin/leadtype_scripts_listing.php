<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "leadtype_scripts_listing.php";
$page_title = $site_name." LeadType Scripts Listing";
$cur_page="configuration";
	
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<script type="text/javascript">
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete the LeadType Scripts?");
	if (agree)
		return true ;
	else
		return false ;
}
</script>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>LeadType Scripts Listing&nbsp;&nbsp;<a class="btn btn-blue" href="add_leadtype_scripts.php"><span>Add LeadType Scripts</span></a></h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">LeadType Scripts Listing</span></div>
<div class="box-content">
<div id="dataTables">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
<thead>
<tr>
  <th><div>Action</div></th>
  <th><div>Lead Type</div></th>
  <th><div>Lead SubType</div></th>
  <th><div>Script</div></th>
  <th><div>Status</div></th>
  <th><div>Modified By</div></th>
  <th><div>Modified Time</div></th>
</tr>
</thead>
<tbody>
<?php
$sql_qry = "select * from tps_leadtype_scripts order by modified desc";

$result_list = mysql_query($sql_qry) or die(mysql_error());
while($result=mysql_fetch_array($result_list)) {

	$res=mysql_query("select name,no_of_questions from tps_scripts where id='".$result['script_id']."'") or die(mysql_error());	
	$s=mysql_fetch_array($res);

	$ns=mysql_num_rows($res);
?>
<tr>
<td>
 <a href="add_leadtype_scripts.php?action=edit&id=<?php print $result['id'];?>" title="Edit"><img src="../images/edit.png"  title="Edit"/></a>&nbsp;|&nbsp;
 <a href="add_leadtype_scripts.php?action=delete&id=<?php print $result['id'];?>" title="Delete" onClick="return confirmDelete();" > <img src="../images/block.png" width="15px" class="key_image" title="Delete"/></a>        
</td>
<td ><?php print $result['type_displayname']; ?></td>
<td ><?php print $result['subtype_displayname']; ?></td>
<td ><?php if($ns>0){ print $s['name']."(".$s['no_of_questions'].")"; } ?></td>
<td align="center">
<?php 
	if ( $result['status'] == 0 ) {
		echo '<img src="../images/checked.gif"  title="Active" />';
	}
	else {
		echo '<img src="../images/cancel.png"  title="In Active" />';
	}
?>		
</td>
<td  align="center"><?php print $result['modified']; ?></td>
<td  align="center"><?php echo display_time_diff_format($result['modified'],1);?></td>
</tr>
<?php
} // while closing
?>
</table>
   </div>
 </div>
 </div>
</div>

<?php
include "lcas_footer.php";
?>
