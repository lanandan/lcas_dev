<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

//validate_login();

$page_name = "appointments.php";
$page_title = $site_name." -  Appointments";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<!--
<script src="javascripts/jquery.dataTables.js" type="text/javascript"></script>-->
<script src="javascripts/jquery.dataTables.columnFilter.js" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function() {

 $('#appt').dataTable()
		  .columnFilter({   sPlaceHolder: "head:before",
			aoColumns: [ 
					null,
					{ type: "select" },
				     	null,
				     	null,
					null,
					null,
					null,
					{ type: "select" },
					null,
				     	{ type: "select" },
             			     	{ type: "select" }
				   ]

		});
	
			
	$('#dealer').change(function() { 
	
		var dealer=$('#dealer').val();

		var data = {
    			type: 'filterbydealer',
			dealer:dealer
		}
    		$.ajax({
	    		url: "apptfilters.php",
	    		data: data,
	    		success: function(output) {
				$('#dispContent').html(output);
			}
		});

	});
	

});

function saveChanges(object){   
    $.ajax({
        url: 'editlead.php',
        data: 'content=' + object.value,
        cache: false,
        
        success: function(response){
            
            var val=object.value;
            var value=val.replace(/\s/g, '');
            $('.name').css({display:'none'});
           

	document.getElementById(value).style.display='block';

}
    });   
}


  function loadAjaxContent(x,y)
    { 

var data = {
    			type: 'editlead',
			id:x,
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url: "editlead.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:550,
							openjs:function(){
							$('#datepicker').datepicker({
								autoclose:true								
								});
                                                           
             					$('#datepicker1').datepicker({
								autoclose:true								
								});

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	

							$('a#save-event-edit').click(function(){
							var appt_status = $('#status').val();
							var appt_status1=appt_status.replace(/\s/g, '');
							var appt_result = $('#'+appt_status1+' #result').val();
                                                        var remarks = $('#'+appt_status1+' #comments').val();
							var datepicker = $('#'+appt_status1+' .datestartnline').val();
							var besttime =$('#'+appt_status1+' #besttime').val();
							
							var data = {
							    	type: 'editSaveEvent',
								id: x,
								leadid:y,
								status: appt_status,
								result: appt_result,
								remarks: remarks,
								re_date: datepicker,
    								besttime: besttime,
							
							 }


								$.ajax({
								    		type: "POST",
								    		url: "editlead.php",
								    		data: data,
								    		success: function(resp) {
                                                                                window.location.href = 'appointments.php';
									        window.parent.TINY.box.hide();
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
    }


</script>
<div class="main-content" >
<div class="container">
<br /><br />

  <div class="col-md-16">
    <div class="box" >
      <div class="box-header"><span class="title">Appointments</span></div>
      <div class="box-content" style="height:600px;" align="center">
	<?php
	$userid=get_session('LOGIN_USERID');

	$today=date('Y-m-d H:i:s');

	$res=mysql_query("select * from tps_events where appttype!='Task' and start>='".$today."' and delete_flag='0' and createdby='".$userid."' order by start asc")or die(mysql_error());
	
	$table="<table width='98%' class='dTable responsive dataTable' id='appt'>";
$table.="<thead><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></thead><thead>";
	$table.="<tr><td>S.No</td>
		 <td>Lead Type</td>
		 <td>First Name</td>
		 <td>Last Name</td>
    		 <td>City</td>
		 <td>State</td>
		 <td>Phone</td>
		 <td>Dealer</td>
		 <td>Appt Date & Time</td>
		 <td>Appt Status</td>
		 <td>Appt Result</td>
		</tr></thead><tbody>";
	$i=1;
	while($r=mysql_fetch_array($res))
	{
		$table.="<tr><td align='center'>".$i."</td>";

		$lcres=mysql_query("select id,leadid,lead_type,fname1,lname1,city,state,zip,phone1,lead_dealer,apptresult from tps_lead_card where leadid='".$r['lead_id']."'") or die(mysql_error());
		
		$lcr=mysql_fetch_array($lcres);

		$table.="<td>".$lcr['lead_type']."</td>";
		$table.="<td>".$lcr['fname1']."</td>";
		$table.="<td>".$lcr['lname1']."</td>";
		$table.="<td>".$lcr['city']."</td>";
		$table.="<td>".$lcr['state']."</td>";
		$table.="<td>".$lcr['phone1']."</td>";
		$table.="<td>".$lcr['lead_dealer']."</td>";
		
		$table.="<td>".date('M d, Y h:i:s A',strtotime($r['start']))."</td>";
		$leadid=$lcr['leadid'];

		$sql_tas="select * from tps_appt_status_update where lead_seq_id='$leadid'and delete_flag='0' order by id desc ";

		$restas=mysql_query($sql_tas) or die(mysql_error());
	        $tas = mysql_fetch_array($restas);

		$table.='<td><a href="#" style="text-decoration:underline;" onclick="loadAjaxContent('.$lcr['id'].','.$lcr['leadid'].');">'.$tas['appt_status'].'</a></td>';
				
		$table.="<td>".$lcr['apptresult']."</td></tr>";

		$i++;

	}
	$table.="</tbody>";
	$table.="<tfoot><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tfoot></table>";
		
	echo $table;
		
	?>
	
 

      </div>
	<br />
    </div>
	<br /><br />
   </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";

?>
