<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();
$page_name = "add_gift_choice.php";
$page_title = $site_name." Add /Edit Gift Choice ";
$cur_page="configuration";

$lead_type='';
$lead_subtype='';
$status='';
$gift='';
$ltype='';
$lsubtype='';
$id='';

if(request_get('action')){
	if ( request_get('action') == "do"  )
	{
		$flag = true;
		//set the values from post profile
		$lead_type=isset($_POST['ltype'])?safe_sql_nq(trim($_POST['ltype'])):'';
		$lead_subtype=isset($_POST['lsubtype'])?safe_sql_nq(trim($_POST['lsubtype'])):'';
		$gift=isset($_POST['gift'])?safe_sql_nq(trim($_POST['gift'])):'';
		$status=isset($_POST['status'])?trim($_POST['status']):'';

		$timestamp =  time();

		$id = request_get('id');
		if ($status == '') {$status='0';}
		if ($id > 0)
		{
			//update databse
			$sql= "update tps_lead_gift set ".
				" lead_type = '". $lead_type ."', ".
				" lead_subtype = '". $lead_subtype ."', ".
				" gift = '". $gift ."', ".
				" status = '". $status ."' ".
				" where id = '".$id."' " ;
		}
		else {
			$sql= "insert into tps_lead_gift set ".
				" lead_type = '". $lead_type ."', ".
				" lead_subtype = '". $lead_subtype ."', ".
				" gift = '". $gift ."', ".
				" status = '". $status ."' ";		
		}	
		mysql_query($sql) or die(mysql_error());
        
		$url = "Location: gift_choice_assigning.php";
		header($url);
		exit();	
	}           
	
	if ( request_get('action') == "edit"  )
	{
		$id = request_get('id');
		$sql_qry = "select * from tps_lead_gift where id = $id ";

		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$lead_type=$row['lead_type'];
			$lead_subtype=$row['lead_subtype'];
			$gift=$row['gift'];
			$status=$row['status'];			
				
		}
	}
	if ( request_get('action') == "delete"  )
	{
		$id = request_get('id');
		$sql_qry = "delete from tps_lead_gift where id = '".$id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$url = "Location: gift_choice_assigning.php";
		header($url);
		exit();	
	}
}
	
include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );		

?>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Assign Gift Choice &nbsp;&nbsp;<a class="btn btn-blue" href="gift_choice_assigning.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Assign Gift Choice</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" action="assign_gift_choice.php" method="POST" id="add_gift_choice" name="add_gift_choice">
          <div class="padded">
            <div class="form-group">
              <label class="control-label col-lg-2">Lead Type *</label>
              <div class="col-lg-4">
                	<select class="nline" name="ltype" id="ltype" onchange="showsubtype(this);" ><option value="Select">Select</option>
			<?php	
			
				 echo getLeadType($lead_type); 
			 
			?>
			</select> 
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Lead Subtype *</label>
              <div class="col-lg-4">
                	<select class="nline" name="lsubtype" id="lsubtype" ><option value="Select">Select</option>
			<?php	
			
				 echo getLeadSubType2($lead_subtype,$lead_type); 
			 
			?>
			</select> 
              </div>
            </div>
           <div class="form-group">
              <label class="control-label col-lg-2">Gift Choice*</label>
              <div class="col-lg-4">
                	<select class="nline" name="gift" id="gift" ><option value="Select">Select</option>
			<?php	
			$sql="select * from tps_gift_choice where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	while($row = mysql_fetch_array($result)){ 
		if($gift==$row['display']){
			$string .='<option value="'.$row['display'].'"  selected>'.$row['display'].'</option>';
			}else{
			$string .='<option value="'.$row['display'].'" >'.$row['display'].'</option>';}
			
			}
	
	
	echo $string;	
			 
			?>
			</select> 
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

            


           

	    <input type="hidden" name="action" value="do" />
	    <input type="hidden" name="id" value="<?php echo request_get('id');?>" />

          <div class="form-actions">
            <button type="submit" class="btn btn-blue">Save changes</button>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='gift_choice_assigning.php'">Cancel</button>
          </div>
        </form>
<br/><br/>
   </div>
 </div>
 </div><?php echo getLeadSubType3($lsubtype,$ltype); ?>
<script>

function showsubtype(t){var val=$('#ltype option:selected').attr("class")+" showsubtype";$('#lsubtype option.showsubtype').remove();$('#lsubtype').append(asdf);$('#lsubtype option[class!="'+val+'"]').remove();$('#lsubtype .showsubtype,#gift .hide-gift').removeAttr("selected");$('<option selected>Select</option>').prependTo('#lsubtype');}

</script>
<?php
include "lcas_footer.php";
?>
