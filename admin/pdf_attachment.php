<?php
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
//require_once("pageaccess.php");
session_start();
$session_id='1'; //$session id
validate_login();
$page_name = "pdf_attachment.php";
$page_title = $site_name." Add Pdf Attachment ";
$cur_page="configuration";

function getExtension($str) 
{

         $i = strrpos($str,".");
         if (!$i) { return ""; } 

         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }


include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );		

?>
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery.wallform.js"></script>

<script type="text/javascript" >
function confirmDelete() 
{
	var agree=confirm("Are you sure  to delete this file?");
	if (agree)
		return true ;
	else
		return false ;
}
 $(document).ready(function() { 
		$('#triggerFile').on('click', function(e){
        e.preventDefault()
        $("#photoimg").trigger('click')
    });
            $('#photoimg').die('click').live('change', function()			{ 
			           //$("#preview").html('');
			    
				$("#imageform").ajaxForm({target: '#preview', 
				     beforeSubmit:function(){ 
					
					console.log('v');
					$("#imageloadstatus").show();
					 $("#imageloadbutton").hide();
					 }, 
					success:function(){ 
					console.log('z');
					 $("#imageloadstatus").hide();
					 $("#imageloadbutton").show();
					}, 
					error:function(){ 
							console.log('d');
					 $("#imageloadstatus").hide();
					$("#imageloadbutton").show();
					} }).submit();
					
		
			});


        }); 
</script>

<style>

body
{
font-family:arial;
}
.preview
{
width:100px;
height:100px;
margin-left:20px;
margin-bottom:20px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
width:100%;
color:#cc0000;
font-size:12px
}
.delete{
width:150px;
height:70px;
}
.download{
width:150px;
height:70px;
}
</style>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add Pdf Attachments &nbsp;&nbsp;<a class="btn btn-blue" href="#" onclick="history.back(1)"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add Pdf Attachments</span>
   </div>
   <div class="box-content"style="min-height:400px">
          <div class="padded"><div>
  <div class="form-group"style='clear:both;padding-bottom:30px;' >
<form id="imageform" method="post" enctype="multipart/form-data" action='file_attachment.php'>
              <label class="control-label col-lg-2" style="padding-top:10px;">Upload Your PDF Document</label>
              <div class="col-lg-10">
              <div id='imageloadstatus' style='display:none'><img src="loader.gif" alt="Uploading...."/></div>
<div id='imageloadbutton'>
<input type="file" class="hide" name="photoimg" id="photoimg" />
<a href="#" id="triggerFile" class="btn btn-primary">Browse File</a>
</div>    
</div>
</div>
</div>

<div id='preview'>


<?php $sql=mysql_query("select pdf_file_name,file_original_name,id from upload_pdf_attachment where delete_flag='0' order by id desc")or die(mysql_error());
while($row=mysql_fetch_array($sql)){
$actual_image_name=$row['pdf_file_name'];
$title=$row['file_original_name'];
$id=$row['id'];
echo "<div><img src='uploads/pdf.png'  class='preview' ><span style='margin-left:20px;width:100px;word-wrap:break-word;'>".$title."</span><a href='download.php?filename=".$actual_image_name."'><img src='uploads/download.jpeg'class='download'></a><a href='download.php?action=delete&filename=".$actual_image_name."'><img src='uploads/delete.jpeg'class='delete' onClick='return confirmDelete();'></a></div>";		
							
}?>


</div>	
</div>
</form>
 </div>
 </div>
</div>
<?php
include "lcas_footer.php";
?>
