<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();
$page_name = "add_event_subtype.php";
$page_title = $site_name." Add / Edit event Sub type";
$cur_page="event_subtype";

$event_type='';
$event_sub_type='';
$event_sub_type_disp='';
$status='';
$script_id='';
$l_id='';
$displayorder='';
if(request_get('action')){
	if ( request_get('action') == "do"  )
	{
		$flag = true;
		//set the values from post profile
		$event_sub_type=isset($_POST['event_sub_type'])?safe_sql_nq(trim($_POST['event_sub_type'])):'';
		$event_sub_type_disp=isset($_POST['event_sub_type_disp'])?safe_sql_nq(trim($_POST['event_sub_type_disp'])):'';
		$status=isset($_POST['event_status'])?trim($_POST['event_status']):'';
		$parent=isset($_POST['parent'])?safe_sql_nq(trim($_POST['parent'])):'';
		$timestamp =  time();
		$display_order=$_POST['displayorder'];
		$l_id = request_get('l_id');
		if ($l_id > 0)
		{
			//update databse
			$sql= "update tps_event_subtype set ".
					" event_subtype = '". $event_sub_type ."', ".
					" subtype_displayname = '". $event_sub_type_disp ."', ".
					" parent = '". $parent ."', ".
					" status = '". $status ."', ".
					" display_order = '". $display_order ."', ".		
					" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
					" modified = '". $timestamp ."' ".
					" where id = '".$l_id."' " ;
		}
		else{
			//update databse
			$sql= "insert into tps_event_subtype set ".
					" event_subtype = '". $event_sub_type ."', ".
					" subtype_displayname = '". $event_sub_type_disp ."', ".
					" parent = '". $parent ."', ".
					" status = '". $status ."', ".
					" display_order = '". $display_order ."', ".				
					" modifiedby = '". get_session('DISPLAY_NAME') ."', ".
					" modified = '". $timestamp ."' " ;

		}
			
		mysql_query($sql) or die(mysql_error());

		$url = "Location: event_subtype_listing.php";
		header($url);
		exit();	
	}
	if ( request_get('action') == "edit"  )
	{
		$l_id = request_get('l_id');
		$sql_qry = "select * from tps_event_subtype where id = $l_id ";

		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$event_sub_type=$row['event_subtype'];
			$event_sub_type_disp=$row['subtype_displayname'];
			$ltype=$row['parent'];
			$status=$row['status'];	
			$displayorder=$row['display_order'];				
		}
	}
	if ( request_get('action') == "delete"  )
	{
		$l_id = request_get('l_id');
		$sql_qry = "delete from tps_event_subtype where id = '".$l_id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$url = "Location: event_subtype_listing.php";
		header($url);
		exit();	
	}
}
	
include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );		

?>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Event SubType&nbsp;&nbsp;<a class="btn btn-blue" href="event_subtype_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit Event SubType</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" action="add_event_subtype.php" method="POST" id="add_event_type" name="add_event_type">
          <div class="padded">
            <div class="form-group">
              <label class="control-label col-lg-2">Event Type *</label>
              <div class="col-lg-4">
                	<select class="nline" name="parent" id="ltype" required><option value="">Select</option>
			<?php	
			
				 echo geteventType($ltype); 
			 
			?>
			</select> 
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Sub Event Type Abbr *</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="event_sub_type" size="128" id="event_sub_type" title="Sub event Type Abbr" value="<?php echo $event_sub_type;?>"/>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-lg-2">Sub Event Type Name *</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="event_sub_type_disp" size="128" id="event_sub_type_disp" title="Sub event Type Display Name" value="<?php echo $event_sub_type_disp;?>"/>
              </div>
            </div>
  <div class="form-group">
              <label class="control-label col-lg-2">Display Order</label>
              <div class="col-lg-1">
                  <select class="uniform" name="displayorder" id="displayorder">
		   <option value="" >Select</option>
		   <?php echo getDisplayOrderNum($displayorder); ?>
		  </select>
              </div>
		<label class="control-label col-lg-3">Lower Value = Top Position</label>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="event_status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="event_status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

            
	    <input type="hidden" name="action" value="do" />
	    <input type="hidden" name="l_id" value="<?php echo $l_id;?>" />

          <div class="form-actions">
            <button type="submit" class="btn btn-blue">Save changes</button>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='event_subtype_listing.php'">Cancel</button>
          </div>
        </form>
<br/><br/>
   </div>
 </div>
 </div>

<?php
include "lcas_footer.php";
?>
