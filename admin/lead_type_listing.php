<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "lead_type_listing.php";
$page_title = $site_name." Lead Type Listing";
$cur_page="lead_type_list";
	
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<script type="text/javascript">
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete the Lead Type?");
	if (agree)
		return true ;
	else
		return false ;
}
</script>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Lead Type Listing&nbsp;&nbsp;<a class="btn btn-blue" href="add_lead_type.php"><span>Add New Lead Type</span></a></h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Lead Type Listing</span></div>
<div class="box-content">
<div id="dataTables">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
<thead>
<tr>
  <th><div>Action</div></th>
  <th><div>Lead Type</div></th><th></th>
  <th><div>Status</div></th>
  <th><div>Modified By</div></th>
  <th><div>Modified Time</div></th>
</tr>
</thead>
<tbody>
<?php
$old_type = "";
$sql_qry = "select * from tps_lead_type order by modified desc";

$result_list = mysql_query($sql_qry) or die(mysql_error());
while($result=mysql_fetch_array($result_list)) {	
if ($old_type == $result['lead_type']) {
	continue;
}
else {
	$old_type = $result['lead_type'];
}
?>
<tr>
<td>
        <a href="add_lead_type.php?action=edit&l_id=<?php print $result['id'];?>" title="Edit"><img src="../images/edit.png"  title="Edit"/></a>&nbsp;|&nbsp;
 	<a href="add_lead_type.php?action=delete&l_id=<?php print $result['id'];?>&lead=<?php print $result['type_displayname'];?>" title="Delete" onClick="return confirmDelete();" > <img src="../images/block.png" width="15px" class="key_image" title="Delete"/></a>        
</td>

<td ><?php print $result['type_displayname']."  ( ".$result['lead_type']." )"; ?></td><td>
<?php $k=1;$sub__type='';$sql="select * from tps_lead_subtype where status=0 and parent='".$result['lead_type']."'";
	$result2=mysql_query($sql) or die(mysql_error());while($row = mysql_fetch_array($result2)){ 
	?><?php if($row['subtype_displayname']!=''){$sub__type.=$k.") ";}$sub__type.=$row['subtype_displayname'];if($row['subtype_displayname']!=''){$sub__type.="<br>";}$k++;?><?php }if($sub__type!=''){ ?><span style="position:relative;margin-right:0px;"
	
	><span class="btn btn-blue view-sub"style="padding:0px;">View Subtype</span><span style="display:none;position:absolute;z-index:900;border-radius:2px;width:300px;padding:10px;background-color:#000000;color:#ffffff;" class="show-sub"><?php echo $sub__type;?><img src="../images/delete.png"style="position:absolute;right:2px;top:2px;opacity:0.6;cursor:pointer;"></span></span><?php } ?>
</td>

<td align="center">
<?php 
	if ( $result['status'] == 0 ) {
		echo '<img src="../images/checked.gif"  title="Active" />';
	}
	else {
		echo '<img src="../images/cancel.png"  title="In Active" />';
	}
?>		
</td>
<td  align="center"><?php print $result['modifiedby']; ?></td>
<td  align="center"><?php echo display_time_diff_format($result['modified'],1);?></td>
</tr>
<?php
}
?>
</table>
<br/>
<br/>
   </div>
 </div>
 </div>
</div>
<script>
$('.view-sub').click(function(){$('.show-sub').css({display:'none'});$(this).parent().find('.show-sub').css({display:'inline'});});
$('.show-sub img').hover(function(){$(this).css({opacity:'0.9'});},function(){$(this).css({opacity:'0.6'});});$('.show-sub img').click(function(){$(this).parent().css({display:'none'});});
</script>
<?php
include "lcas_footer.php";
?>
