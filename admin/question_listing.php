<?php
ob_start();
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "question_listing.php";
$page_title = $site_name." Question Listing";

$script_id ='';
$Q_Type_Def ='';

$script_id = request_get('qn_id');
if ($script_id != '') {
	set_session('qn_id', $script_id);
}
	
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";


if(isset($_REQUEST['action']))
{
	if($_REQUEST['action']=="delete")
	{
		$delsqid=$_REQUEST['delsqid'];
		$scriptid=$_REQUEST['scriptid'];

		$del_sql="delete from tps_scripts_questions where id='$delsqid'";
		$suc=mysql_query($del_sql)or die(mysql_error());

		if($suc)
		{
			$total=0;
			$res=mysql_query("select id,no_of_questions from tps_scripts where id='$scriptid'")or die(mysql_error());

			$r=mysql_fetch_array($res);
			$total=$r['no_of_questions']-1;
	
			mysql_query("update tps_scripts set no_of_questions='$total' where id='$scriptid'")or die(mysql_error());

		}

		header("Location:question_listing.php?qn_id=$scriptid");
		exit();
	}

}

$scriptid=$_REQUEST['qn_id'];

$scr_res=mysql_query("select id,name,no_of_questions from tps_scripts where id='$scriptid'")or die(mysql_error());
$scr_r=mysql_fetch_array($scr_res);

$curPageTitle=$scr_r['name']." Question Listing (".$scr_r['no_of_questions'].") "; 

?>
<script type="text/javascript">
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete the Question?");
	if (agree)
		return true ;
	else
		return false ;
}
</script>


<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
<i class="icon-magic"></i><?php echo $curPageTitle; ?>&nbsp;&nbsp;<a class="btn btn-blue" href="add_questions_toscripts.php?qn_id=<?php echo $script_id;?>"><span>Add Questions to Script</span></a>&nbsp;&nbsp;<a class="btn btn-blue" href="preview_scripts.php?qn_id=<?php echo $script_id;?>"><span>Preview</span></a>&nbsp;&nbsp;<a class="btn btn-blue" href="scripts_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title"><?php echo $curPageTitle; ?></span></div>
<div class="box-content">
<div id="dataTables">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive">
<thead>
<tr>
  <th><div>Action</div></th>
  <th><div>Name</div></th>
  <th><div>Type</div></th>
  <th><div>Status</div></th>
  <th><div>Modified By</div></th>
  <th><div>Modified Time</div></th>
</tr>
</thead>
<tbody>
<?php

$sql_qry="SELECT q.id, q.name, q.type, q.modified, q.modifiedby,q.status, sq.id as sqid, sq.script_id, sq.question_id, sq.displayorder FROM tps_questions q JOIN tps_scripts_questions sq ON sq.question_id=q.id WHERE sq.script_id='$scriptid' order by sq.displayorder ASC";

$result_list = mysql_query($sql_qry) or die(mysql_error());
while($result=mysql_fetch_array($result_list)) {

?>
<tr>
<td>
	<a href="add_questions_toscripts.php?qn_id=<?php echo $scriptid;?>" title="Edit"><img src="../images/edit.png" class="key_image"  title="Edit"/></a>
        &nbsp;|&nbsp;
        <a href="question_listing.php?action=delete&scriptid=<?php echo $scriptid; ?>&delsqid=<?php echo $result['sqid']; ?>" title="Delete" onClick="return confirmDelete();" > <img src="../images/small-bin.png" class="key_image" title="Delete"/></a>
</td>
<td ><?php print $result['name']; ?></td>
<?php
	$Q_Type= $result['type'];
	switch ($Q_Type)
		{
		case 1:
		  $Q_Type_Def = "True/False";
		  break;
		case 2:
		  $Q_Type_Def = "Multiple Choice";
		  break;
		case 3:
		  $Q_Type_Def = "Multiple Answer";
		  break;
		case 4:
		  $Q_Type_Def = "Rating";
		  break;
		case 5:
		  $Q_Type_Def = "Short Answer";
		  break;
		case 6:
		  $Q_Type_Def = "Long Answer";
		  break;
		case 7:
		  $Q_Type_Def = "Drop Down";
		  break;
		default:
		  $Q_Type_Def = "Custom Defined";
		}
?>

<td ><?php print $Q_Type_Def; ?></td>
<td align="center">
        <?php 
			if ( $result['status'] == 0 ) {
				echo '<img src="../images/checked.gif"  title="Active" />';
			}
			else {
				echo '<img src="../images/cancel.png"  title="In Active" />';
			}
        ?>		
</td>
<td  align="center"><?php print $result['modifiedby']; ?></td>
<td  align="center"><?php echo display_time_diff_format($result['modified'],1);?></td>
</tr>
<?php
}
?>
</table>
<br/>
<br/>
   </div>
 </div>
 </div>
</div>

<?php
include "lcas_footer.php";
ob_end_flush();
?>


