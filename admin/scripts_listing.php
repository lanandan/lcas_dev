<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "scripts_listing.php";
$page_title = $site_name." Script Listing";
$cur_page="scripts_list";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>

<script type="text/javascript" src="javascripts/jquery.dataTables.js"></script>
<link href="stylesheets/jquery.dataTables.css" media="screen" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function confirmDelete() 
{
	var agree=confirm("Deleting this script will also delete the questions and mapping associated with it. Continue deleting this script?");
	if (agree)
		return true ;
	else
		return false ;
}
$(document).ready(function(){

    $('#scriptlisting').dataTable({
	"bRetrieve": true,
	"stateSave": true
    });

});

</script>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Script Listing&nbsp;&nbsp;<a class="btn btn-blue" href="create_scripts.php"><span>Create New Script</span></a></h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Script Listing</span></div>
<div class="box-content">
<div id="dataTables">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive" id="scriptlisting">
<thead>
<tr>
  <th width="6%"><div>Action</div></th>
  <th width="20%"><div>Name</div></th>
  <th width="24%"><div>Description</div></th>
  <th width="10%"><div>#Questions  </div></th>
  <th width="10%"><div>Show Yes/No</div></th>
  <th width="10%"><div>Status</div></th>
  <th width="10%"><div>Modified By</div></th>
  <th width="10%"><div>Modified Time</div></th>
</tr>
</thead>
<tbody>
<?php
$sql_qry = "select * from tps_scripts order by modified desc";
$result_list = mysql_query($sql_qry) or die(mysql_error());
while($result=mysql_fetch_array($result_list)) {	
?>
<tr>
<td>
        <a href="create_scripts.php?action=edit&qn_id=<?php print $result['id'];?>" title="Edit"><img src="../images/edit.png"  title="Edit"/></a>
      <?php if($result['isdefault']!="1") { ?>  &nbsp;|&nbsp;
        <a href="create_scripts.php?action=delete&qn_id=<?php print $result['id'];?>" title="Delete" onClick="return confirmDelete();" > <img src="../images/small-bin.png" class="key_image" title="Delete"/></a>  
      <?php } ?>     
</td>
<td ><a href="question_listing.php?qn_id=<?php print $result['id'];?>" style="color:blue; " ><?php print $result['name']; ?></a></td>
<td ><?php print substr($result['description'],0,30); ?></td>
<td  align="center">
<?php 
print $result['no_of_questions']; 
?>
</td>
<td align="center">
        <?php 
			if ( $result['showyesno'] == 1 ) {
				echo '<img src="../images/checked.gif"  title="Active" />';
			}
			else {
				//echo '<img src="../images/cancel.png"  title="In Active" />';
			}
        ?>		
</td>
<td align="center">
        <?php 
			if ( $result['status'] == 0 ) {
				echo '<img src="../images/checked.gif"  title="Active" />';
			}
			else {
				echo '<img src="../images/cancel.png"  title="In Active" />';
			}
        ?>		
</td>
<td  align="center"><?php print $result['modifiedby']; ?></td>
<td  align="center"><?php echo display_time_diff_format($result['modified'],1);?></td>
</tr>
<?php
}
?>
</table>
<br/>
<br/>
   </div>
 </div>
 </div>
</div>


<?php
include "lcas_footer.php";
?>
