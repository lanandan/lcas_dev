<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "create_scripts.php";
$page_title = $site_name." New / Edit Scripts";
$qn_previous_id ='';

$qn_name='';
$qn_description='';
$status='';

$lead_type='';

$qn_id = '';
$newScriptId='';
$flag = false;
                
if(request_get('action')){
	if ( request_get('action') == "do"  )
	{
		$flag = true;
		//set the values from post profile
		$qn_name = isset($_POST['rt_qn_name'])?safe_sql_nq(trim($_POST['rt_qn_name'])):'';
		$qn_description = isset($_POST['qn_description'])?safe_sql_nq(trim($_POST['qn_description'])):'';
		$status = isset($_POST['status'])?trim($_POST['status']):'';

		$timestamp = time();

		$qn_id = request_get('qn_id');
		$lead_subtype=$_REQUEST['lead_subtype'];
		$lead_type=$_REQUEST['lead_type'];

		if ($qn_id > 0)
		{
			//update databse
			$sql= "update tps_scripts set ".
					" `user_id` = '". get_session('LOGIN_ID') ."', ".
					" `name` = '". $qn_name ."', ".
					" `description` = '". $qn_description ."', ".
					" `type` = '". $lead_type ."', ".
					" `subtype`= '".$lead_subtype ."',".
					" `status` = '". $status ."', ".
					" `modified` = '". $timestamp ."',".
					" `modifiedby` = '". get_session('DISPLAY_NAME') ."' where id = '".$qn_id."' " ;   

			mysql_query($sql) or die(mysql_error());	
			updateQuesionsTotals($qn_id);
			$error_flag = '0';
			$error_message = 'Successfully Updated Scripts details !!';
			set_session('e_flag', '1');
			set_session('e_message', $error_message);
		}
		else {
			//insert new row
			$sql= "insert into tps_scripts set ".
					" `user_id` = '". get_session('LOGIN_ID') ."', ".
					" `name` = '". $qn_name ."', ".
					" `description` = '". $qn_description ."', ".
					" `type` = '". $lead_type ."', ".
					" `subtype`= '".$lead_subtype ."',".
					" `status` = '". $status ."', ".
					" `created` = '". $timestamp ."', ".			
					" `createdby` = '". get_session('DISPLAY_NAME') ."', ".
					" `modified` = '". $timestamp ."',  ". 
					" `modifiedby` = '". get_session('DISPLAY_NAME') ."' ";

			mysql_query($sql) or die(mysql_error());		
			$newScriptId = mysql_insert_id();
			$error_flag = '0';
			$error_message = 'Successfully Added Scripts details !!';
			set_session('e_flag', '1');
			set_session('e_message', $error_message);

			if ( request_get('script_template') > 0 ) {
				$oldScriptId = request_get('script_template');
				copyFromScript ($oldScriptId, $newScriptId);
			}
		}

		$url = "Location: scripts_listing.php";
		header($url);
		exit();						
	}
	if ( request_get('action') == "edit"  )
	{
		$qn_id = request_get('qn_id');
		$sql_qry = "select * from tps_scripts where id = '".$qn_id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			$qn_description=$row['description'];
			$qn_name=$row['name'];
			$lsubtype=$row['subtype'];
			$ltype=$row['type'];
			$status=$row['status'];			
		}
	}
	if ( request_get('action') == "delete"  )
	{
		$qn_id = request_get('qn_id');
		$sql_qry = "delete from tps_scripts where id = '".$qn_id."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		
		$sql_qry_child = "delete from tps_questions where script_id = '".$qn_id."' ";
		$res_qry = mysql_query($sql_qry_child) or die(mysql_error());
		
		$url = "Location: scripts_listing.php";
		header($url);
		exit();						
		
	}
}
	

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
                  
?>
<script type="text/javascript">
$(document).ready(function() {
/*
 $('#lead_type').change(function() {
$.ajax({
            type: "POST",
            url: "getsubtype.php",
            data: {
                ltype: $("#lead_type").val()
            },
            success: function(result) {
$('#lead_subtype').find('option').remove();
var json =jQuery.parseJSON (result);
function createSelect(options){
    $.each(options,function(index,item){
           $("#lead_subtype").append($("<option></option>").attr("value", item.subltext).text(item.subltext));

    });
}

$(document).ready(function(){
createSelect(json);
});

}
        });
    });*/
});
</script>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Add / Edit Scripts&nbsp;&nbsp;<a class="btn btn-blue" href="scripts_listing.php"><span>Back</span></a></h3>
        </div>
      </div>
    </div>
  </div>

 <div class="container">
 <div class="box">
   <div class="box-header">
     <span class="title">Add / Edit Scripts</span>
   </div>
   <div class="box-content">
        <form class="form-horizontal fill-up validatable" action="create_scripts.php" method="POST" id="create_scripts" name="create_scripts">

          <div class="padded">

            <div class="form-group">
              <label class="control-label col-lg-2">Script Name *</label>
              <div class="col-lg-4">
                <input type="text" class="validate[required]" data-prompt-position="topLeft" name="rt_qn_name" size="64" id="rt_qn_name" title="Script Name" value="<?php echo $qn_name;?>"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Description</label>
              <div class="col-lg-4">
                <textarea name="qn_description" id="qn_description" title="Script Description"  cols="75"><?php echo $qn_description;?></textarea><span class="counter"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Status</label>
              <div class="col-lg-1">
                        <input type="radio" class="icheck"  value="0"  name="status" checked="checked" id="iradio1"/>
                        <label for="iradio1">Show</label>
	      </div>
              <div class="col-lg-1">
                        <input type="radio" class="icheck" value="1"  name="status" <?php if($status==1){print "checked='checked'";} ?> id="iradio1" />
                        <label for="iradio2">Hide</label>
               </div>
	      </div>

		 <div class="form-group">
              <label class="control-label col-lg-2">Select Lead Type *</label>
              <div class="col-lg-4"><select name="lead_type" id="ltype" class="uniform validate[required]"onchange="showsubtype(this);" title="Lead Type" data-prompt-position="topLeft">
		<option value=""class="Select">Select</option>
		<?php echo getLeadType($ltype); ?>
		</select>
              </div>
            </div>
		 <div class="form-group">
              <label class="control-label col-lg-2">Select Lead SubType *</label>

              <div class="col-lg-4"><select name="lead_subtype" id="lsubtype"  title="Lead Type" data-prompt-position="topLeft" >
			  <option value=""class="Select">Select</option>
	<?php	
		echo getLeadSubType2($lsubtype,$ltype); 

	?>
		
		</select>
              </div>
            </div>


<?php
if ($qn_id == '') {
?>
            <div class="form-group">
              <label class="control-label col-lg-2">Copy from Scripts<br/>(or) Templates</label>
              <div class="col-lg-4">
                  <select class="uniform" name="script_template" id="script_template">
		   <option value="Select" >Copy from previous Scripts</option>
		   <?php echo getScriptNameList(0); ?>
		  </select>
              </div>
            </div>
<?php
}
?>


	    <input type="hidden" name="action" value="do" />
	    <input type="hidden" name="qn_id" value="<?php echo $qn_id;?>" />

          <div class="form-actions">
            <button type="submit" class="btn btn-blue">Save changes</button>
            <button type="button" class="btn btn-default" onclick="javascript:window.location='scripts_listing.php'">Cancel</button>
          </div>
        </form>

<br/><br/>
   </div>
 </div>
 </div><?php echo getLeadSubType3($lsubtype,$ltype); ?>
<script>

function showsubtype(t){var val=$('#ltype option:selected').attr("class")+" showsubtype";$('#lsubtype option.showsubtype,#gift option.hide-gift').remove();$('#lsubtype').append(asdf);$('#lsubtype option[class!="'+val+'"]').remove();$('#lsubtype .showsubtype,#gift .hide-gift').removeAttr("selected");$('<option selected>Select</option>').prependTo('#lsubtype,#gift');}
</script>
<?php

include "lcas_footer.php";
?>
