<?php
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "gift_choice_listing.php";
$page_title = $site_name." Gift Choice Listing";
$cur_page="configuration";
$address='';
$phone='';
$email='';	
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
?>
<style>
#spinner {
position: fixed;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.5;
background: url('img/preload.gif') 50% 50% no-repeat #ffffff;
}
</style>
<script type="text/javascript" src="../javascripts/bootbox.min.js"></script>
<script type="text/javascript">
function assign(id){
$(document).ready(function() {
var i=$('#' + id).is(":checked");
if(i==true)
check=1;
else
check=0;
$("#spinner").show();
var data={
	  type:'dealer',
          check:check,
	  id:id,
}
$.ajax({

	    		type: "POST",
	    		url: "assign.php",
	    		data: data,
	    		success: function(output) {
			$("#spinner").hide();
			bootbox.alert(output);
					}
});
});
}
function s_assign(id){
$(document).ready(function() {
var td_id='setby'+id;
var i=$('#'+td_id).is(":checked");
if(i==true){
check=1;
}
else
check=0;
$("#spinner").show();
var data={
	  type:'setby',
          check:check,
	  id:id,
}
$.ajax({

	    		type: "POST",
	    		url: "assign.php",
	    		data: data,
	    		success: function(output) {
			$("#spinner").hide();
			bootbox.alert(output);
					}
});
});
}


function l_assign(id){
$(document).ready(function() {
var td_id='lasst'+id;
var i=$('#'+td_id).is(":checked");
if(i==true){
check=1;
}
else
check=0;
$("#spinner").show();
var data={
	  type:'lasst',
          check:check,
	  id:id,
}
$.ajax({

	    		type: "POST",
	    		url: "assign.php",
	    		data: data,
	    		success: function(output) {
			$("#spinner").hide();
			bootbox.alert(output);
					}
});
});
}

function sa_assign(id){
$(document).ready(function() {
var td_id='sasst'+id;
var i=$('#'+td_id).is(":checked");
if(i==true){
check=1;
}
else
check=0;
$("#spinner").show();
var data={
	  type:'saasst',
          check:check,
	  id:id,
}
$.ajax({

	    		type: "POST",
	    		url: "assign.php",
	    		data: data,
	    		success: function(output) {
			$("#spinner").hide();
			bootbox.alert(output);
					}
});
});
}

function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete the Gift Choice?");
	if (agree)
		return true ;
	else
		return false ;
}
function edit(val1){
window.top.location="../adduser.php?action=adminedit&lid=" + val1;
}

</script>
<div id="spinner"></div>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Assigning of Dealers , Lead  Assist , Assign Set Assist and SetBy&nbsp;&nbsp;</h3>
        </div>
      </div>
    </div>
  </div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Assigning of Dealers , Lead  Assist , Assign Set Assist and SetBy</span></div>
<div class="box-content">
<div id="dataTables">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive" id="example">
<thead>
<tr>
   <th><div style='height:100%;display:inline-block;'>Edit Profile</div></th>
  <th><div style='height:100%;display:inline-block;'>Lead Dealer</div></th>
  <th><div style='height:100%;display:inline-block;'>Assign Dealer</div></th>
  <th><div style='height:100%;display:inline-block;'>Assign Set BY</div></th>
   <th><div style='height:100%;display:inline-block;'>Assign Lead Assist</div></th>
   <th><div style='height:100%;display:inline-block;'>Assign Set Assist</div></th>
  <th><div style='height:100%;display:inline-block;'>User Name</div></th>
  <th><div style='height:100%;display:inline-block;'>Name</div></th>
  <th><div style='height:100%;display:inline-block;'>Phone</div></th>
  <th><div style='height:100%;display:inline-block;'>Email</div></th>
  <th><div style='height:100%;display:inline-block;'>Address</div></th>
  
</tr>
</thead>
<tbody>
<?php
$sql_qry = "select * from tps_users where status=1";

$result_list = mysql_query($sql_qry) or die(mysql_error());
while($result=mysql_fetch_array($result_list)) {	
?>
<tr>
<td align="center"><img src='../images/edit.png' id='edit_option' onclick=edit(<?php echo $result['id']; ?>) style="cursor:pointer;" title="Edit Profile"></td>
<td align="center">
<input type="checkbox"  checked disabled/>
</td>
<td align="center">
<?php if($result["dealer_flag"]==1){ ?> <input id="<?php echo $result['id']; ?>"  type="checkbox" onchange="javascript:assign(<?php echo $result['id']; ?>);"  checked/><?php } else{?> <input   id="<?php echo $result['id']; ?>" type="checkbox" onchange="javascript:assign(<?php echo $result['id']; ?>);" > <?php } ?>
</td>
<td align="center">
<?php if($result["setby_flag"]==1){ ?> <input id="<?php echo 'setby'.$result['id']; ?>"  type="checkbox" onchange="javascript:s_assign(<?php echo $result['id']; ?>);"  checked/><?php } else{?> <input   id="<?php echo 'setby'.$result['id']; ?>" type="checkbox" onchange="javascript:s_assign(<?php echo $result['id']; ?>);" > <?php } ?>
</td>
<td align="center">
<?php if($result["lead_assist_flag"]==1){ ?> <input id="<?php echo 'lasst'.$result['id']; ?>"  type="checkbox" onchange="javascript:l_assign(<?php echo $result['id']; ?>);"  checked/><?php } else{?> <input   id="<?php echo 'lasst'.$result['id']; ?>" type="checkbox" onchange="javascript:l_assign(<?php echo $result['id']; ?>);" > <?php } ?>
</td>
<td align="center">
<?php if($result["set_assist_flag"]==1){ ?> <input id="<?php echo 'lasst'.$result['id']; ?>"  type="checkbox" onchange="javascript:sa_assign(<?php echo $result['id']; ?>);"  checked/><?php } else{?> <input   id="<?php echo 'sasst'.$result['id']; ?>" type="checkbox" onchange="javascript:sa_assign(<?php echo $result['id']; ?>);" > <?php } ?>
</td>
<td ><?php print $result['username']; ?></td>
<td ><?php print $result['fname']." ".$result['lname']; ?>
<td ><?php $phone.= $result['phone1'];
			if( $result['phone2'] != ''  && $result['phone2'] !=''){
				$phone.="<br/>".$result['phone2'];}
			if( $result['phone3'] != '' &&  $result['phone3'] !='' ){
				$phone.="<br/>".$result['phone3']; }
echo $phone;
?></td>
<td ><?php 
	$email.=$result['email1'];
	if ( $result['email2'] != ''){
	$email.="<br/>".$result['email2'];}
echo $email;
	     ?></td>
<td><?php if ( $result['address1'] != '' )
	{ $address.=$result['address1'];

			}
	if ( $result['address2'] != '' )
	{
		$address.="<br>".$result['address2'];
	}
	if($result['city']!="")
	{
		if($address!="")
		{
			$address.="<br>".$result['city'];
		}
		else
		{
			$address.=$result['city'];
		}
	}

	if($result['state']!="")
	{
		if($address!="" )
		{
			$address.=",".$result['state'];
		}
		else
		{
			$address.=$result['state'];
		}
	}

	if($result['zipcode']!="")
	{
		if($address!="")
		{
			$address.=",".$result['zipcode'];
		}
		else
		{
			$address.=$result['zipcode'];
		}
	}
	echo $address; ?></td>

</tr>
<?php
$address='';
$phone='';
$email='';
} // while closing
?>
</table>
<br/>
<br/>
   </div>
 </div>
 </div>
</div>

<?php
include "lcas_footer.php";
?>
