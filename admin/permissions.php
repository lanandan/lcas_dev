<?php
ob_start();
session_start();
require_once("../include/tps_constants.php");
require_once("../include/tps_db_conn.php");
require_once("../include/tps_gen_functions.php");
require_once("pageaccess.php");
validate_login();

$page_name = "permissions.php";
$page_title = $site_name." Permissions";
$cur_page="permissions";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";


?>
<script type="text/javascript">

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});
</script>
<script type="text/javascript">
  $(document).ready(function($) {
    $('#accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');

      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');
	
    });

// For User Change

	$("#userid").change(function(){

		var userid=$("#userid").val();

		if(userid!="0")
		{
			$("#spinner").show();

			var data={
				  type:'getPermission',
				  userid:userid
			}
			$.ajax({

		    		type: "POST",
		    		url: "permission_actions.php",
		    		data: data,
				datatype: 'json',
		    		success: function(data) {

					obj = JSON.parse(data);

					if(obj.edit_one==0)
						$("#lead_editone").prop('checked', true);
					else if (obj.edit_one==1)
						$("#lead_editone").prop('checked', false);
					else
						$("#lead_editone").prop('checked', false);

					if(obj.delete_one==0)
						$("#lead_deleteone").prop('checked', true);
					else if (obj.delete_one==1)
						$("#lead_deleteone").prop('checked', false);
					else
						$("#lead_deleteone").prop('checked', false);

					if(obj.bulk_update==0)
						$("#lead_bulk_update").prop('checked', true);
					else if (obj.bulk_update==1)
						$("#lead_bulk_update").prop('checked', false);
					else
						$("#lead_bulk_update").prop('checked', false);

					if(obj.bulk_delete==0)
						$("#lead_bulk_delete").prop('checked', true);
					else if (obj.bulk_delete==1)
						$("#lead_bulk_delete").prop('checked', false);
					else
						$("#lead_bulk_delete").prop('checked', false);


					$("#spinner").hide();
				}

			}); // end of ajax call

		}else{
			alert("Please Select any User!");
		}

	});

// For Check Box Selection 

	$("input[type='checkbox']").click(function(){

		var userid=$("#userid").val();
		
		if(userid=="0")
		{
			alert('Please select any User!');
			return false;
		}else{

			var chkname=$(this).attr("id");

			//alert(chkname);

			var chked=$('#' + chkname).is(":checked");

			//alert("chked : "+chked);

			var check;

			if(chked==true)
				check=0; // for Yes 
			else
				check=1; // for No

			$("#spinner").show();

			var data={
				  type:'addPermission',
				  userid:userid,
				  permission:check,
				  permissionfor:chkname
			}
			$.ajax({

		    		type: "POST",
		    		url: "permission_actions.php",
		    		data: data,
		    		success: function(output) {
					$("#spinner").hide();
				}

			}); // end of ajax call

		} //end of ifelse part

	}); // end of check box click


  }); // end of document ready

</script>


<!-- CSS -->
<style>
  .accordion-toggle {cursor: pointer; padding:8px; border:1px solid #ccc; background-color:#F3F4F8;}
  .accordion-content {
	display: none; 
	border:1px solid #ccc; 
	border-top:none; 
	padding:8px; 
   }
  .accordion-content.default {display: block;}

</style>

<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Permissions&nbsp;&nbsp;</h3>
        </div>
      </div>
    </div>
  </div>

<div id="spinner"></div>

<div class="container">
<div class="box">
<div class="box-header"><span class="title">Permissions</span>
<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
</div>
<div class="box-content padded">

Select User : 
<select class="nline" name="userid" id="userid">
<option value="0">Select</option>
<?php
	$ldeal="";

	$re=mysql_query("select nickname,fname,lname,userid from tps_users where status='1'")or die(mysql_error());

	while($r=mysql_fetch_array($re))
	{       
		if($ldeal==$r['userid'])
			echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
		else
			echo "<option value='$r[userid]'>".ucfirst($r['fname'])." ".$r['lname']."</option>";
	}

?>
</select>

<br/>
<br/>
<?php

?>
<!-- HTML -->
<div id="accordion">
  <div class="accordion-toggle"><label>Lead Listing</label></div>
  <div class="accordion-content default">
	
	<table class="table table-bordered padded">
	<tr>
		<td><input type="checkbox" name="lead_editone" id="lead_editone" value="" /> <label>Edit</label> </td>
		<td><input type="checkbox" name="lead_deleteone" id="lead_deleteone" value="" /> <label>Delete</label> </td>
		<td><input type="checkbox" name="lead_bulk_delete" id="lead_bulk_delete" value="" /> <label>Bulk Delete</label> </td>
		<td><input type="checkbox" name="lead_bulk_update" id="lead_bulk_update" value="" /> <label>Bulk Update</label> </td>
	</tr>
	</table>

  </div>
  <!--<div class="accordion-toggle">Accordion 2</div>
  <div class="accordion-content default">
    <p>Lorem ipsum dolor sit amet mauris eu turpis.</p>
  </div>
  <div class="accordion-toggle">Accordion 3</div>
  <div class="accordion-content default">
    <p>Vivamus facilisisnibh scelerisque laoreet.</p>
  </div>-->
</div>


<br/>
   </div>
 </div>
 </div>
</div>


<?php
include "lcas_footer.php";
ob_end_flush();
?>
