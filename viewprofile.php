<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
validate_login();

$page_name = "profile.php";
$page_title = $site_name." -View Profile";
$nameprefix='';
$fname='';
$lname='';
$phone='';
$address1='';
$address2='';
$city='';
$state='';
$zipcode='';
$country='';
$email1='';
$email2='';
$image1='';
$frame='';
$team='';
$address='';
$recruit='';
$personal='';
$email='';
if( request_get('action')=='delete')
{
$id=request_get('uid');
$link=request_get('link');
$sql= "update tps_users set image1='default.jpeg' WHERE id = '$id'";
$result=mysql_query($sql) or die(mysql_error());
if($link=='adminedit'){
$message="your Team Member Profile picture has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);	
       header("Location: teamview.php" );
		exit;
}
else{
$message="your Profile picture has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);	
       header("Location: viewprofile.php" );
		exit;
}
}
	$sql="select * from tps_users where id='".get_session('LOGIN_ID')."'";
	
	
	$result=mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_array($result);
			$nameprefix=$row['nameprefix'];				
			$image1=$row['image1'];			
                        $phonetype1=$row['phonetype1'];
                        $phonetype2=$row['phonetype2'];
                        $phonetype3=$row['phonetype3'];
			$manager1=$row['manager1'];
			$manager2=$row['manager2'];
			$recruited_by=$row['recruited_by'];
			$marketing_manager=$row['marketing_manager'];
	$re=mysql_query("select fname,lname from tps_users where userid='$manager1'");
	$r=mysql_fetch_array($re);
	$manager1=$r['fname']." ".$r['lname'];
	$re=mysql_query("select fname,lname from tps_users where userid='$manager2'");
	$r=mysql_fetch_array($re);
	$manager2=$r['fname']." ".$r['lname'];
	$re=mysql_query("select fname,lname from tps_users where userid='$marketing_manager'");
	$r=mysql_fetch_array($re);
	$marketing_manager=$r['fname']." ".$r['lname'];
	$re=mysql_query("select fname,lname from tps_users where userid='$recruited_by'");
	$r=mysql_fetch_array($re);
	$recruited_by=$r['fname']." ".$r['lname'];
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
include "js/profile.js";
?>


<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>Profile Details&nbsp;&nbsp;<a class="btn btn-blue" href="adduser.php?action=edit"><span>Edit Profile</span></a>
	</h3>
</div> 
<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
      </div>
    </div>
  </div>

<div class="container">

<div class="box">

<div class="box-header"><span class="title">Profile Details</span>

</div>

<div class="box-content" style='height:100%;'>
	<form id="frm" name="frm" class="fillup" action="" method="POST">

        <div id="img">
	 		<img id="pic" src="images/upload/<?php echo $image1; ?>">
 </div>
<div id="profile">
<table width="80%" >
<tbody>
<tr><td width="30%"><span>Name</span></td>
		<td><?php echo $nameprefix;?> <?php echo ucfirst($row['fname']);?> <?php echo $row['lname']; ?> </td></tr>
<?php if($row['nickname']!=''){?> <tr><td width="30%"><span>Nick Name</span></td><td><?php echo ucfirst($row['nickname']); ?> </td></tr><?php } ?>

		
	
	<tr><td><span>Address Info</span></td>
		<td><?php 
	if ( $row['address1'] != '' )
	{ $address.=$row['address1'];

			}
	if ( $row['address2'] != '' )
	{
		$address.="<br>".$row['address2'];
	}
	if($row['city']!="")
	{
		if($address!="")
		{
			$address.="<br>".$row['city'];
		}
		else
		{
			$address.=$row['city'];
		}
	}

	if($row['state']!="")
	{
		if($address!="" )
		{
			$address.=",".$row['state'];
		}
		else
		{
			$address.=$row['state'];
		}
	}

	if($row['zipcode']!="")
	{
		if($address!="")
		{
			$address.=",".$row['zipcode'];
		}
		else
		{
			$address.=$row['zipcode'];
		}
	}
	echo $address; ?> </td></tr>
	
<tr> 
<td width="30%"><span>Contact Info</span> </td>
<td> <?php 
			$phone.= $row['phone1'];
			if( $row['phone2'] != ''  && $row['phone2'] !=''){
				$phone.="<br/>".$row['phone2'];}
			if( $row['phone3'] != '' &&  $row['phone3'] !='' ){
				$phone.="<br/>".$row['phone3']; }
echo $phone;
	     ?>
</td></tr>
<tr><td width="30%"><span>Email Info</span> </td>
<td><?php 
	$email.=$row['email1'];
	if ( $row['email2'] != ''){
	$email.="<br/>".$row['email2'];}
echo $email;
	     ?></td></tr>

<tr><td width="30%"><span>Team Details </span></td><td><?php if($row['team_type']!=''){
					$team.="<label>Team Type :</label>". $row['team_type']; 
						}
				if($row['team_position']!=''){
					$team.="<br><label>Team Position :</label>".$row['team_position']; 
						}
				if($row['team_status']!=''){
					$team.="<br><label>Team Status :</label>".$row['team_status']; 
						}
				if($row['manager1']!=''){
					$team.="<br><label>Manager :</label>".$manager1; 
						}

				if($row['manager2']!=''){
					$team.="<br><label>Marketing Mgr :</label>".$manager2; 
						}

				if($row['marketing_manager']!=''){
					$team.="<br><label>Marketing Asst :</label>".$marketing_manager; 
						}
echo $team;


?></td></tr>


<tr><td width="30%"><span>Recruit Details </span></td><td><?php 
				if($row['demo_date']!='0000-00-00'){
					$recruit.="<label>Demo Date :</label>".date('m/d/Y',strtotime($row['demo_date'])); 
					}
				if($row['recruited_type']!=''){
					$recruit.="<br><label>Recruited Type :</label>".$row['recruited_type']; 
						}
				if($row['recruited_by']!=''){
					$recruit.="<br><label>Recruited By :</label>".$recruited_by; 
						}
				if($row['deposit']!=''){
					$recruit.="<br><label>Good Faith Deposit :</label>".$row['deposit']; 
						}

				if($row['intrested_date']!='0000-00-00'){
					$recruit.="<br><label>Interested Recruit Date :</label>".date('M d Y',strtotime($row['intrested_date'])); 
						}

				if($row['opportunity_date']!='0000-00-00'){
					$recruit.="<br><label>In For Opportunity Date :</label>".date('M d Y',strtotime($row['opportunity_date'])); 
						}
			
				if($row['training_date']!='0000-00-00'){
					$recruit.="<br><label>In Training Date :</label>".date('M d Y',strtotime($row['training_date'])); 
						}

				if($row['graduation_date']!='0000-00-00'){
					$recruit.="<br><label>Graduation & Start Date :</label>".date('M d Y',strtotime($row['graduation_date'])); 
						}
echo $recruit;


?></td></tr>


<tr><td width="30%"><span>Personal Details </span></td><td><?php 
				if($row['birth_date']!='0000-00-00'){
					$personal.="<label>Birth Date :</label>".date('M d Y',strtotime($row['birth_date'])); 
					}
				if($row['ssn']!=''){
					$personal.="<br><label>SSN :</label>".$row['ssn']; 
						}
				if($row['driving_license_num']!=''){
					$personal.="<br><label>Driver's License No :</label>".$row['driving_license_num']; 
						}
				if($row['partner_fname']!=''){
					$personal.="<br><label>Partner's First Name :</label>".$row['partner_fname']; 
						}

				if($row['partner_lname']!=''){
					$personal.="<br><label>Partner's Last Name :</label>".$row['partner_lname']; 
						}

				if($row['partner_phone_num']!=''){
					$personal.="<br><label>Partner's Phone No :</label>".$row['partner_phone_num']; 
						}
			
				if($row['partner_emailid']!=''){
					$personal.="<br><label>Partner's Email Address :</label>".$row['partner_emailid']; 
						}

				if($row['about_children']!=''){
					$personal.="<br> <label>Children's Name & Ages :</label><label style='width:50%;'>".stripslashes($row['about_children'])."</label>"; 
						}
				if($row['comment']!=''){
					$personal.="<br><label> Comments :</label><label style='width:50%;'>".stripslashes($row['comment'])."</label>"; 
						}
echo $personal;


?></td></tr>
<tbody></table>

</div>
	</div>

	</form>

      </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";
?>



