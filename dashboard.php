<?php
session_start();
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();
$page_title="LCAS - Dashboard";
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
$userid=get_session('LOGIN_USERID');
?>
<link href="javascripts/fullcalendar/fullcalendar.css" media="screen" rel="stylesheet" type="text/css" />
<link href="javascripts/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css"  media='print' />
<link href="javascripts/fullcalendar/events_style.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/fullcalendar/fullcalendar.js" type="text/javascript"></script>

<script src="js/jquery.uploadifive.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/uploadifive.css">

<link href="stylesheets/multiple-select.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/jquery.multiple.select.js"></script> 

<!----<script type="text/javascript" src="javascripts/base.js"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/base1.css" />--->

<script src="javascripts/datepair.js"></script> 

<script src="javascripts/formatter.js"></script> 

<script type="text/javascript" src="javascripts/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/jquery.timepicker.css" />

<script type="text/javascript" src="javascripts/limiters.js"></script>

<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>

<script src="js/dashboard_calender.js" type="text/javascript"></script>

<script>
function showsubevent(t) {

    var val = $('#edit-training-dialog-trainingname').val() + " showevent";

    $('#topic .showevent').css({
        display: 'none'
    });
    $('#topic .showevent').removeAttr("selected");
    $('#topic option[class="'+val+'"]').css({
        display: 'block'
    });
}

function deleteupimg(id, rid)
{
	bootbox.confirm("<b>Are you sure you want to delete this file?</b>", function(result) {
		if(result) {
			var data = {
				type: 'deleteEventFiles',
				fileid:id,
				eventid:rid
			}
			$.ajax({
				type: "POST",
				url: "events_json.php",
				data: data,
				success: function(resp) {
					$('#g'+id).hide();
					//window.location.href = 'add_company_features.php?job=edit&id='+id;
				}
			});
		}
	});

}


function delupfile(id){
	var data = {
		type: 'deleteEventFiles',
		fileid:id
	}
	$.ajax({
		type: "POST",
		url: "uploadifive.php?job=delete&fileid="+id,
		data: data,
		success: function(resp) {
			//$('#g'+id).hide();
			//window.location.href = 'add_company_features.php?job=edit&id='+id;
		}
	});
}

</script>

<div class="main-content">
  <div class="container">
    <div class="row">

      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
            <i class="icon-dashboard"></i>&nbsp;&nbsp;
            Lcas Dashboard
          </h3>
          
        </div>

        <ul class="list-inline pull-right sparkline-box">

          <li class="sparkline-row">
            <h4 class="blue"><span>Last Month</span> 847</h4>
            <div class="sparkline big" data-color="blue"><!--25,11,5,28,25,19,27,6,4,23,20,6--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="green"><span>This Month</span> 1140</h4>
            <div class="sparkline big" data-color="green"><!--28,26,13,18,8,6,24,19,3,6,19,6--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="red"><span>This Week</span> 230</h4>
            <div class="sparkline big"><!--16,23,28,8,12,9,25,11,16,16,17,13--></div>
          </li>

        </ul>
      </div>
    </div>
  </div>

  <div class="container padded">
    <div class="row">

      <!-- Breadcrumb line -->

      <div id="breadcrumbs">
        <div class="breadcrumb-button blue">
          <span class="breadcrumb-label"><i class="icon-home"></i> Home</span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>

        

        <div class="breadcrumb-button">
          <span class="breadcrumb-label">
            <i class="icon-dashboard"></i> Dashboard
          </span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">

  <div class="col-md-3">
    <!-- find me in partials/action_nav_normal -->
<?php

	$start = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
$end   = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
	$starttime=strtotime($start);
	$endtime=strtotime($end); 
	$uid=get_session('LOGIN_ID');
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

//Count for appointments
$userid=get_session('LOGIN_USERID');

	$today=date('Y-m-d H:i:s');
$child=array($userid);
$re=mysql_query("select userid from tps_users where parentid='$userid'");
while($r=mysql_fetch_array($re)){
array_push($child,$r['userid']);
}
$child=implode(',',$child);

	$sql_grp="select *  from tps_events where appttype!='Task' and delete_flag='0' and FIND_IN_SET(createdby,'$child') or assignedto='".$cur_userid."' GROUP BY lead_id  ";
	$result=mysql_query($sql_grp) or die(mysql_error());
	$ecnt=mysql_num_rows($result);

	
// Count for lead
$ID=get_session('LOGIN_ID');
$today=date('Y-m-d H:i:s');
$child=array($ID);
$re=mysql_query("select id from tps_users where parentid='$userid'");
while($r=mysql_fetch_array($re)){
array_push($child,$r['id']);
}
$child=implode(',',$child);
$sql_lcnt=mysql_query("select count(*) as lcnt from `tps_lead_card` where delete_flag='0' AND FIND_IN_SET(uid,'$child')  ") or die(mysql_error());
$lccnt=mysql_fetch_array($sql_lcnt);



// Count for customer

$sql_ccnt=mysql_query("select count(*) as ccnt from `tps_lead_card` where  FIND_IN_SET(uid,'$child') and delete_flag='0' and customer_flag=1 ") or die(mysql_error());

$ccnt=mysql_fetch_array($sql_ccnt);

// Count for Task

$sql_rl_receiver=mysql_query("select count(*) as tscnt from `tps_task_details` where userid = $uid  and createdby!='$cur_userid' ") or die(mysql_error());

$tscnt=mysql_fetch_array($sql_rl_receiver);

?>
<!--big normal buttons-->
<div class="action-nav-normal">

  <div class="row action-nav-row">
    <div class="col-sm-6 action-nav-button">
      <a href="appointments.php" title="Appointments">
        <i class="icon-file-alt"></i>
        <span>Appointments</span>
      </a>
      <span class="triangle-button red"><span class="inner"><?php echo $ecnt; ?></span></span>
    </div>

    <div class="col-sm-6 action-nav-button">
      <a href="messages/inbox.php" title="Messages">
        <i class="icon-comments-alt"></i>
        <span>Messages</span>
      </a>
	<?php
		 	$msgcnt=unreadmsgcountheader($id); 
			if($msgcnt>0){ echo '<span class="label label-black">'.$msgcnt.'</span>';  } 
	?>
   
    </div>
  </div>

  <div class="row action-nav-row">
    <div class="col-sm-6 action-nav-button">
      <a href="task_listing.php" title="Tasks">
        <i class="icon-folder-open-alt"></i>
        <span>Tasks</span>
      </a>
  <span class="triangle-button green"><span class="inner"><?php echo $tscnt['tscnt'];?></span></span>
    </div>

    <div class="col-sm-6 action-nav-button">
      <a href="customer.php" title="Customers">
        <i class="icon-user"></i>
        <span>Customers</span>
      </a>
      <span class="triangle-button green"><span class="inner"><?php echo $ccnt['ccnt'];?></span></span>
    </div>
  </div>

  <div class="row action-nav-row">
    <div class="col-sm-6 action-nav-button">
      <a href="leadcard.php" title="Leads">
        <i class="icon-facebook-sign"></i>
        <span>Leads</span>
      </a>
      <span class="triangle-button blue"><span class="inner"><?php echo $lccnt['lcnt']; ?></span></span>
    </div>

    <div class="col-sm-6 action-nav-button">
      <a href="#" title="Sales">
        <i class="icon-twitter"></i>
        <span>Sales</span>
      </a>
      <span class="triangle-button blue"><span class="inner">+12</span></span>
    </div>
  </div>

</div>

  </div>

  <div class="col-md-9">
    <!-- find me in partials/big_chart -->
    <div class="box">
      <div class="box-header">
        <div class="title">Full Calendar</div>
	<div class="title"style="margin-right:20px;float:right;">
	<b>
		<span class="team"><input type="checkbox" name="teammember" id="teammember" checked />Team Member Appointments</span> &nbsp;&nbsp; 
		<span class="appt"><input type="checkbox" name="demo" id="demo" checked />Appointments</span> &nbsp;&nbsp; 
		<span class="event"><input type="checkbox" name="events" id="events" checked />Events</span> &nbsp;&nbsp; 
		<span class='task'><input type="checkbox" name="task" id="task" checked />Task</span>
	</b>
      </div>
      </div>

      <div class="box-content">
        <div id="calendar"></div>
      </div>
   </div>

	
<div id="spinner"></div>

</div>

</div>

</form>
<?php
include "lcas_footer.php";
?>
