<?php
session_start();

ob_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "baby_survey_qa_form.php";
$page_title = $site_name." -  Survey Q&A Form";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

$lid=$_REQUEST['lid'];
$lseqid=$_REQUEST['leadseqid'];
$refby='';
$header='';
$footer='';

$ques1='';
$ques2='';
$ques3='';
$ques4='';
$ques5='';
$ques6='';
$ques7='';
$ques8='';
$ques9='';
$ques10='';

$cur_uid=get_session('LOGIN_ID');
$cur_userid=get_session('LOGIN_USERID');
$cur_email=get_session('LOGIN_EMAIL');
$cur_username=get_session('DISPLAY_NAME');

	$res=mysql_query("select * from tps_lead_card where id='".$lid."'") or die(mysql_error());
	
	$r=mysql_fetch_array($res);

	$res_users=mysql_query("select office_name,office_city from tps_users where userid='".get_session('LOGIN_USERID')."'") or die(mysql_error());
	$ru=mysql_fetch_array($res_users);

//echo "Lead Type : ".$r['lead_type']." Lead Sub Type : ".$r['lead_subtype'];

$subsql="";

$lead_type=$r['lead_type'];
$lead_subtype=$r['lead_subtype'];
$cur_dealerid=$r['lead_dealer'];

$subsql=getMappedScript($lead_type,$lead_subtype,$cur_dealerid);

$sql ="SELECT id, name, no_of_questions,header,footer,showyesno from tps_scripts where id=($subsql) and status='0'";

	$rs_list=mysql_query($sql) or die(mysql_error());
	$rs = mysql_fetch_array($rs_list);
?>

<script type="text/javascript" src="javascripts/bootbox.min.js"></script>

<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>

<script type="text/javascript">

function validate(){

	var callgift=document.getElementById("callforgift").checked;
	var callgift1=document.getElementById("callforgift1").checked;
	
	if(callgift==false && callgift1==false)
	{
		bootbox.alert('Please Check the Required Feilds!');
		return false;
	}else if(callgift==true || callgift1==true)
	{	
		bootbox.confirm("Please Confirm whether all the questions are answered?", function(res) {
		
		if(res==true)
			return true;
		else 
			return false;
		}); 
		
	}
}
</script>
<style type="text/css">
 #nostyle table, caption, tbody, tfoot, thead, tr, th, td {
          margin: 0;
          padding: 0;
          border: 0;
          outline: 0;
          font-size: 100%;
          vertical-align: baseline;
          background: transparent;
        }
</style>

<div class="main-content" >
<div class="container">
<br /><br />

  <div class="col-md-16">
    <div class="box">
      <div class="box-header">
<span class="title">Script Name - <?php echo $rs['name']." (".$rs['no_of_questions'].")"; ?> </span>
<a class="btn btn-xs btn-blue" href="leadcard.php" style="margin-top:7px;"><span>Back</span></a>
<a class="btn btn-xs btn-blue" href="add_leads.php?action=edit&lid=<?php echo $lid;?>&redir=3" style="margin-top:7px;"><span>Edit Lead</span></a>
<a href="add_leads.php?action=edit&lid=<?php echo $_REQUEST['lid'];?>&leadseqid=<?php echo $_REQUEST['leadseqid'];?>&backto=sf" class="btn btn-xs btn-blue" style="margin-top:7px;" ><span>Calender</span></a>
</div>
      <div class="box-content" align="center" style="min-height: 500px;">
	<input type="hidden" name="lid" value="<?php echo $_REQUEST['lid']; ?>" />
	<input type="hidden" name="leadseqid" value="<?php echo $_REQUEST['leadseqid']; ?>" />
	<?php 

	$lid=$_REQUEST['lid'];

	$res=mysql_query("select * from tps_lead_card where id='".$lid."'") or die(mysql_error());
	
	$r=mysql_fetch_array($res);

	$res_users=mysql_query("select office_name,office_city from tps_users where userid='".get_session('LOGIN_USERID')."'") or die(mysql_error());
	$ru=mysql_fetch_array($res_users);

	$survey_leadname=ucfirst($r['fname1'])." ".$r['lname1'];

	$showyesno=$rs['showyesno'];
	if($showyesno=="1")
	{
	?>
<form name="form" id="form" method="post" onsubmit="javascript: return validate();">
	<?php 

	}else{ ?>
<form name="form" id="form" method="post">
	<?php 
	 }
	?>
<table border="0" width="98%" class="table table-normal" style="line-height:24px; text-align:justify;">
  <tbody>
<tr><td>
<?php

//echo "Surveyed Date : ";

?>
</td></tr>
     <tr>
	<td>

<?php  
if($r['referred_by']!='')
{
	$ref=$r['referred_by'];
	$im=explode('_',$ref);
	if($im[0]==1){
		$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]' ")or die(mysql_error());
		$refname=mysql_fetch_array($sq);
		$refby=ucfirst($refname['lname1'])." ".$refname['fname1']." ".$refname['lname2']." ".$refname['fname2'];
	}
	if($im[0]==2){
		$sq=mysql_query("select campaign from tps_campaign where id='$im[1]'")or die(mysql_error());
		$refname=mysql_fetch_array($sq);
		$refby=$refname['campaign'];
	}
	if($im[0]==3){
		$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]'")or die(mysql_error());
		$refname=mysql_fetch_array($sq);
		$refby=ucfirst($refname['fname1'])." ".$refname['lname1'];
	}
}

$address=$r['add_line1'].' ,'.$r['add_line2'].' '.$r['city'].' '.$r['state'].' '.$r['zip'];

$sq="select * from tps_events where lead_id='$r[id]' and appttype='Demo' and delete_flag='0'";
$res=mysql_query($sq) or die(mysql_error());
$appt1 = mysql_fetch_array($res); 

$sql_select="select * from tps_users where userid='$r[lead_dealer]'";
$result_select = mysql_query($sql_select) or die(mysql_error());
$row1 = mysql_fetch_array($result_select);
 
if($appt1['start']!='' && $appt1['start']!='' && $appt1['end']!='')
{
	$startDate = date('M d Y',strtotime($appt1['start']));
	$startTime = date('h:i A',strtotime($appt1['start']));
	$endTime = date('h:i A',strtotime($appt1['end']));
	$apt_date_time=$startDate.'    '.$startTime.'-'.$endTime;
}else{
	$apt_date_time='';
}

$dealer=ucfirst($row1['fname'])." ".ucfirst($row1['lname']);
$login_name=get_session('DISPLAY_NAME'); 

if($r['fname1']!=''){
	$header=str_replace("%first name1%","<b>".$r['fname1']."</b>",$rs['header']); 
}else{
	$header=str_replace("%first name1%","<b><a>%first name1%</a></b>",$rs['header']); 
}

if($login_name!=''){
	$header1=str_replace("%logged in user%","<b>".$login_name."</b>",$header);
}else{
	$header1=str_replace("%logged in user%","<a>%logged in user%</a>",$header);
}

if($refby!=''){
	$header2=str_replace("%referred by%","<b>".$refby."</b>",$header1);
}else{
	$header2=str_replace("%referred by%","<b><a>%referred by%</a></b>",$header1);
}

if($r['lname1']!==''){
	$header3=str_replace("%last name1%","<b>".$r['lname1']."</b>",$header2);
}else{
	$header3=str_replace("%last name1%","<b><a>%last name%</a></b>",$header2);
}

$header4=str_replace("%office name%","<b>LCAS</b>",$header3);

if($dealer!=''){
	$header5=str_replace("%dealer%" ,"<b>".$dealer."</b>",$header4);
}else{
	$header5=str_replace("%dealer%" ,"<b><a>%dealer%</a></b>",$header4);
}

if($r['fname2']!=''){
	$header6=str_replace("%first name2%","<b>".$r['fname2']."</b>",$header5);
}else{
	$header6=str_replace("%first name2%","<b><a>%first name2%</a></b>",$header5);
}

if($r['lname2']!=''){
	$header7=str_replace("%last name2%","<b>".$r['lname2']."</b>",$header6);
}else{
	$header7=str_replace("%last name2%","<a>%last name2%</a>",$header6);
}

if($r['add_line1']!==''){
	$header8=str_replace("%addr1%","<b>".$r['add_line1']."</b>",$header7);
}else{
	$header8=str_replace("%addr1%","<b><a>%addr1%</a></b>",$header7);
}

if($r['add_line2']!=''){
	$header9=str_replace("%addr2%","<b>".$r['add_line2']."</b>",$header8);
}else{
	$header9=str_replace("%addr2%","<b><a>%addr2%</a></b>",$header8);
}

if($address!=''){
	$header10=str_replace("%address%","<b>".$address."</b>",$header9);
}else{
	$header10=str_replace("%address%","<b><a>%address%</a></b>",$header9);
}

if($apt_date_time!=''){
	$header11=str_replace("%appt%","<b>".$apt_date_time."</b>",$header10);
}else{
	$header11=str_replace("%appt%","<b><a>%appt%</a></b>",$header10);
}

if($r['time_contact']!=''){
	$header12=str_replace("%best time to contact%","<b>".$r['time_contact']."</b>",$header11);
}else{
	$header12=str_replace("%best time to contact%","<b><a>%best time to contact%</a></b>",$header11);
}

if($r['lead_type']!=''){
	$header13=str_replace("%lead type%","<b>".$r['lead_type']."</b>",$header12);
}else{
	$header13=str_replace("%lead type%","<b><a>%lead type%</a></b>",$header12);
}

if($r['lead_subtype']!=''){
	$header14=str_replace("%lead subtype%","<b>".$r['lead_subtype']."</b>",$header13);
}else{
	$header14=str_replace("%lead subtype%","<b><a>%lead subtype%</a></b>",$header13);
}

if($r['referred_by']!='')
{
	$ref=explode("_",$r['referred_by']);
	if($ref[0]==2)
	{

$sq=mysql_query("select drawing_prize,campaign_prize,gift1,gift2,gift3,gift4,gift5,gift6 from tps_campaign where id='$ref[1]'")or die(mysql_error());
		$re=mysql_fetch_array($sq);
		$gift=array($re['gift1'],$re['gift2'],$re['gift3'],$re['gift4'],$re['gift5'],$re['gift6']);
		$gift=array_filter($gift);
		$gift=implode(",",$gift);

		if($gift!='')
		{
			$header15=str_replace("%gift%","<b>".$gift."</b>",$header14);
		}else{
			$header15=str_replace("%gift%","<b><a>%gift%</a></b>",$header14);
		}
	
		if($re['drawing_prize']!="")
		{
			$header15=str_replace("%drawing prize%","<b>".$re['drawing_prize']."</b>",$header15);
		}else{
			$header15=str_replace("%drawing prize%","<b><a>%drawing prize%</a></b>",$header15);
		}
		
		if($re['campaign_prize']!="")
		{
			$header15=str_replace("%campaign prize%","<b>".$re['campaign_prize']."</b>",$header15);
		}else{
			$header15=str_replace("%campaign prize%","<b><a>%campaign prize%</a></b>",$header15);
		}
		
	}else{
	
		if($r['gift']!=''){
			$header15=str_replace("%gift%","<b>".$r['gift']."</b>",$header14);
		}else{
			$header15=str_replace("%gift%","<b><a>%gift%</a></b>",$header14);
		}
	}

}else{

	if($r['gift']!=''){
		$header15=str_replace("%gift%","<b>".$r['gift']."</b>",$header14);
	}else{
		$header15=str_replace("%gift%","</b><a>%gift%</a></b>",$header14);
	}

}

echo trim(stripslashes($header15));

?>

	</td>
    </tr>
    <tr>
	<td>
<?php

if($r['referred_by']!='')
{

	$ref=$r['referred_by'];

	//echo "<pre>$ref</pre>";

	$im=explode('_',$ref);
	if($im[0]==1){
		$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]' ")or die(mysql_error());
		$refname=mysql_fetch_array($sq);
		$refby=ucfirst($refname['lname1'])." ".$refname['fname1']." ".$refname['lname2']." ".$refname['fname2'];
	}
	if($im[0]==2){
		$sq=mysql_query("select campaign from tps_campaign where id='$im[1]'")or die(mysql_error());
		$refname=mysql_fetch_array($sq);
		$refby=$refname['campaign'];
	}
	if($im[0]==3){
		$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]'")or die(mysql_error());
		$refname=mysql_fetch_array($sq);
		$refby=ucfirst($refname['fname1'])." ".$refname['lname1'];
	}
	//echo "<pre>$refby</pre>";

	//echo "<pre>".$r['form_type']." <br>".$r['referral_data']."</pre>";

	echo "<input type='hidden' name='formtype' id='formtype' value='".$r['form_type']."'/>";
	
	if($r['form_type']=="Event_Form")
	{
		$data=json_decode($r['referral_data']);
		$ws=$data[0];
		$h=$data[1];
		$petss=$data[2];
		$isss=$data[3];
		$g=$data[4];
		$pp=explode(",",$petss);
		$i=explode(",",$isss);
?>
<table class="table table-bordered padded">
	<tr>
		<td colspan="5"><b>Campaign Name : <?php echo $refby; ?> - Event Form</b></td>
	</tr>
	<tr>
		<td width="18%"><b>Home</b></td>
		<td width="18%"><b>Pets</b></td>
		<td width="18%"><b>Employment</b></td>
		<td width="18%"><b>Breathing Issues</b></td>
		<td width="28%"><b>My Guess</b></td>
	</tr>
	<tr>
		<td>
			<input type="radio" id="low" name="home" value="Owner" <?php if($h=='Owner'){echo "checked";}?>>
			<label for="low">Owner</label><br>
			<input type="radio" id="med" name="home" value="Renter"<?php if($h=='Renter'){echo "checked";}?> >
			<label for="med">Renter</label><br>	
			<input type="radio" id="med" name="home" value="Other" <?php if($h=='Other'){echo "checked";}?>>
			<label for="med">Other</label><br>
		</td>
		<td>
			<input type="checkbox" id="low" name="p[]" value="None" <?php if (in_array("None", $pp)) {echo "checked";}?>>
			<label for="low">None</label><br>

			<input type="checkbox" id="med" name="p[]" value="Birds" <?php if (in_array("Birds", $pp)) {echo "checked";}?>>
			<label for="med">Birds</label><br>
	
			<input type="checkbox" id="med" name="p[]" value="Cat" <?php if (in_array("Cat", $pp)) {echo "checked";}?>>
			<label for="med">Cat</label><br>
	
			<input type="checkbox" id="med" name="p[]" value="Dog" <?php if (in_array("Dog", $pp)) {echo "checked";}?>>
			<label for="med">Dog</label><br>
	
			<input type="checkbox" id="med" name="p[]" value="Other" <?php if (in_array("Other", $pp)) {echo "checked";}?>>
			<label for="med">Other</label><br>
		</td>
		<td>
			<input type="radio" id="low" name="workstatus" value="Working" <?php if($ws=='Working'){echo "checked";}?>>
			<label for="low">Working</label><br>
		
			<input type="radio" id="med" name="workstatus" value="Retired" <?php if($ws=='Retired'){echo "checked";}?>>
			<label for="med">Retired</label><br>	
		
			<input type="radio" id="med" name="workstatus" value="Laid Off"<?php if($ws=='Laid Off'){echo "checked";}?> >
			<label for="med">Laid Off</label><br>	

			<input type="radio" id="med" name="workstatus" value="Other"<?php if($ws=='Other'){echo "checked";}?> >
			<label for="med">Other</label><br>	
		</td>
		<td>
			<input type="checkbox" id="low" name="issues[]" value="None" <?php if(in_array("None", $i)){echo "checked";}?>>
			<label for="low">None</label><br>

	<input type="checkbox" id="med" name="issues[]" value="Allergies Indoor"<?php if (in_array("Allergies Indoor", $i)) {echo "checked";}?>>
			<label for="med">Allergies Indoor</label><br>

	<input type="checkbox" id="med" name="issues[]" value="Allergies Outdoor" <?php if (in_array("Allergies Outdoor", $i)) {echo "checked";}?>>
			<label for="med">Allergies Outdoor</label><br>

			<input type="checkbox" id="med" name="issues[]" value="Asthma" <?php if (in_array("Asthma", $i)) {echo "checked";}?>>
			<label for="med">Asthma</label><br>

	<input type="checkbox" id="med" name="issues[]" value="Sinus Problem"<?php if (in_array("Sinus Problem", $i)) {echo "checked";}?> >
			<label for="med">Sinus Problem</label><br>

			<input type="checkbox" id="med" name="issues[]" value="Other"<?php if (in_array("Other", $i)) {echo "checked";}?>>
			<label for="med">Other</label>
		</td>
		<td>
			<textarea name="guess" style="width:100%;"><?php echo $g;?></textarea>
		</td>
	</tr>
</table>

<?php
	}elseif($r['form_type']=="Door_Canvas_Form"){
	
		$data=json_decode($r['referral_data']);
		$poll=$data[0];
		$dus=$data[1];
		$ho=$data[2];
		$pe=$data[3];
		$isss=$data[4];
		$us=$data[5];
		$year_in_house=$data[6];
		$pp=explode(",",$pe);
		$i=explode(",",$isss);
		$u=explode(",",$us);
?>
<table class="table table-bordered padded">
	<tr>
		<td colspan="6"><b>Campaign Name : <?php echo $refby; ?> - Door Canvas Form</b></td>
	</tr>
	<tr>
		<td width="16%"><b>Do you think we have a Problem with</b></td>
		<td width="16%"><b>Do you think you have a Problem with</b></td>
		<td width="16%"><b>How many years have you owned your home</b></td>
		<td width="16%"><b>Home</b></td>
		<td width="16%"><b>Do you use any of the following?</b></td>
		<td width="16%"><b>Does you or anyone in your family suffer from...</b></td>
	</tr>
	<tr>
		<td>
			<input type="radio" id="low" name="pollution" value="Air Pollution" <?php if($poll=='Air Pollution'){echo "checked";}?>>
			<label>Air Pollution</label>
		</td>
		<td>
			<input type="radio" id="low" name="dust" value="Dust in the home" <?php if($dus=='Dust in the home'){echo "checked";}?>>
			<label>Dust in the home</label>
		</td>
		<td>
		<input type="text"  name="yearsinhouse" class="txtinput6" value="<?php echo $year_in_house; ?>">
		</td>
		<td>
			<input type="radio" id="low" name="home" value="Owner" <?php if($ho=='Owner'){echo "checked";}?>>
			<label>Owner</label><br>
		
			<input type="radio" id="med" name="home" value="Renter" <?php if($ho=='Renter'){echo "checked";}?>>
			<label>Renter</label><br>
		
			<input type="radio" id="med" name="home" value="Other" <?php if($ho=='Other'){echo "checked";}?>>
			<label>Other</label><br>
		</td>
		<td>
			<input type="checkbox" id="low" name="use[]" value="None" <?php if (in_array("None", $u)) {echo "checked";}?>> 
			<label>None</label><br>

			<input type="checkbox" id="low" name="use[]" value="Candles" <?php if (in_array("Candles", $u)) {echo "checked";}?> >
			<label>Candles</label><br>
		
			<input type="checkbox" id="low" name="use[]" value="Sprays"<?php if (in_array("Sprays", $u)) {echo "checked";}?>>
			<label>Sprays</label><br>
		
			<input type="checkbox" id="low" name="use[]" value="Plug ins" <?php if (in_array("Plug ins", $u)) {echo "checked";}?>>
			<label>Plug ins</label><br>
		</td>
		<td>
	<input type="checkbox" id="low" name="issues[]" value="None" <?php if (in_array("None", $i)) {echo "checked";}?>>
	<label>None</label><br>

	<input type="checkbox" id="med" name="issues[]" value="Allergies Indoor" <?php if (in_array("Allergies Indoor", $i)) {echo "checked";}?>>
	<label>Allergies Indoor</label><br>

	<input type="checkbox" id="med" name="issues[]" value="Allergies Outdoor" <?php if (in_array("Allergies Outdoor", $i)) {echo "checked";}?>>
	<label>Allergies Outdoor</label><br>

	<input type="checkbox" id="med" name="issues[]" value="Asthma"<?php if (in_array("Asthma", $i)) {echo "checked";}?>>
	<label>Asthma</label><br>

	<input type="checkbox" id="med" name="issues[]" value="Sinus Problem" <?php if (in_array("Sinus Problem", $i)) {echo "checked";}?>>
	<label>Sinus Problem</label><br>

	<input type="checkbox" id="med" name="issues[]" value="Other" <?php if (in_array("Other", $i)) {echo "checked";}?>>
	<label>Other</label><br>

		</td>
	</tr>
</table>
<?php
	}else{

		$data=json_decode($r['referral_data']);
		$wstatus1=$data[0];
		$wstatus2=$data[1];
		$htype=$data[2];
		$pets=$data[3];
		$childrens=$data[4];
		$relation=$data[5];
		$issues=$data[6];
		$worktype1=$data[7];
		$worktype2=$data[8];
		$timeto=$data[9];
		$t=explode("<br>",$timeto); 
		$c=explode(",",$childrens);
		$p=explode(",",$pets);
		$iss=explode(",",$issues);

?>


<table class="table table-bordered padded">
	<tr>
		<td colspan="6"><b>Referred By : <?php echo $refby; ?> - Referral Form</b></td>
	</tr>
	<tr>
		<td><b>Employment 1</b></td>
		<td><b>Employment 2</b></td>
		<td><b>Home</b></td>
		<td><b>Best Time To Contact</b></td>
		<td><b>Work Type 1 & 2</b></td>
		<td><b>Pets</b></td>
		<td><b>Children</b></td>
		<td><b>You Are Their</b></td>
		<td><b>Breathing Issues</b></td>
	</tr>
	<tr>
		<td>
			<input type="radio" id="low" name="Employment1" value="Working" <?php if($wstatus1=='Working'){echo "checked";}?> >
			<label>Working</label><br>

			<input type="radio" id="med" name="Employment1" value="Retired"<?php if($wstatus1=='Retired'){echo "checked";}?> >
			<label >Retired</label><br>

			<input type="radio" id="med" name="Employment1" value="Laid Off"<?php if($wstatus1=='Laid Off'){echo "checked";}?> >
			<label>Laid Off</label><br>

			<input type="radio" id="med" name="Employment1" value="Other" <?php if($wstatus1=='Other'){echo "checked";}?>>
			<label >Other</label><br>
		</td>
		<td>
			<input type="radio" id="low" name="Employment2" value="Working"<?php if($wstatus2=='Working'){echo "checked";}?> >
			<label >Working</label><br>

			<input type="radio" id="med" name="Employment2" value="Retired"<?php if($wstatus2=='Retired'){echo "checked";}?> >
			<label >Retired</label><br>

			<input type="radio" id="med" name="Employment2" value="Laid Off"<?php if($wstatus2=='Laid Off'){echo "checked";}?> >
			<label>Laid Off</label><br>

			<input type="radio" id="med" name="Employment2" value="Other" <?php if($wstatus2=='Other'){echo "checked";}?>>
			<label >Other</label><br>
		</td>
		<td>
			<input type="radio" id="low" name="home" value="Owner" <?php if($htype=='Owner'){echo "checked";}?>>
			<label for="low">Owner</label><br>

			<input type="radio" id="med" name="home" value="Renter"<?php if($htype=='Renter'){echo "checked";}?> >
			<label >Renter</label><br>

			<input type="radio" id="med" name="home" value="Other"<?php if($htype=='Other'){echo "checked";}?> >
			<label >Other</label><br>
		</td>
		<td>
			<input type="checkbox" id="low" name="Time[]" value="10 - 02"  <?php if(in_array("10 - 02", $t)){echo "checked";}?> >
			<label>10 - 2</label><br>

			<input type="checkbox" id="med" name="Time[]" value="02 - 05"<?php if(in_array("02 - 05", $t)){echo "checked";}?> >
			<label>2 - 5</label><br>

			<input type="checkbox" id="med" name="Time[]" value="05 - 09"<?php if(in_array("05 - 09", $t)){echo "checked";}?> >
			<label>5 - 9</label><br>
		</td>
		<td>
	<input type="text" name="worktype1" id="addresss" placeholder="Name1 WorkType" value="<?php echo $worktype1;?>"><br><br>
	<input type="text" name="worktype2" id="addresss" placeholder="Name2 WorkType" value="<?php echo $worktype2;?>">
		</td>
		<td>
			<input type="checkbox" id="low" name="pets[]" value="None" <?php if (in_array("None", $p)) {echo "checked";}?>>
			<label >None</label><br>

			<input type="checkbox" id="med" name="pets[]" value="Birds" <?php if (in_array("Birds", $p)) {echo "checked";}?>>
			<label >Birds</label><br>

			<input type="checkbox" id="med" name="pets[]" value="Cat"  <?php if (in_array("Cat", $p)) {echo "checked";}?>>
			<label >Cat</label><br>

			<input type="checkbox" id="med" name="pets[]" value="Dog" <?php if (in_array("Dog", $p)) {echo "checked";}?>>
			<label >Dog</label><br>

			<input type="checkbox" id="med" name="pets[]" value="Other"<?php if (in_array("Other", $p)) {echo "checked";}?> >
			<label >Other</label><br>
		</td>
		<td>
			<input type="checkbox" id="med" name="child[]" value="None" <?php if (in_array("None", $c)) {echo "checked";}?>>
			<label >None</label><br>

			<input type="checkbox" id="med" name="child[]" value="Under5" <?php if (in_array("Under5", $c)) {echo "checked";}?>>
			<label >Under 5</label><br>

			<input type="checkbox" id="med" name="child[]" value="5-18" <?php if (in_array("5-18", $c)) {echo "checked";}?>>
			<label >5 - 18</label><br>

			<input type="checkbox" id="med" name="child[]" value="18+" <?php if (in_array("18+", $c)) {echo "checked";}?>>
			<label >18+</label><br>
		</td>
		<td>
			<select id="recipient" name="relation"  class="selmenu1">
				<option value=''>Select</option>
				<option <?php if($relation=='Aunt'){echo "selected";}?>>Aunt</option>
				<option <?php if($relation=='Brother'){echo "selected";}?>>Brother</option>
				<option <?php if($relation=='Co-Worker'){echo "selected";}?>>Co-Worker</option>
				<option <?php if($relation=='Daughter'){echo "selected";}?>>Daughter</option>
				<option <?php if($relation=='Friend'){echo "selected";}?>>Friend</option>
				<option <?php if($relation=='Grandchild'){echo "selected";}?>>Grandchild</option>
				<option <?php if($relation=='Grandparent'){echo "selected";}?>>Grandparent</option>
				<option <?php if($relation=='Neighbor'){echo "selected";}?>>Neighbor</option>
				<option <?php if($relation=='Nephew'){echo "selected";}?>>Nephew</option>
				<option <?php if($relation=='Niece'){echo "selected";}?>>Niece</option>
				<option <?php if($relation=='Parent'){echo "selected";}?>>Parent </option>
				<option <?php if($relation=='Sister'){echo "selected";}?>>Sister </option>
				<option <?php if($relation=='Son'){echo "selected";}?>> Son </option>
				<option <?php if($relation=='Uncle'){echo "selected";}?>>Uncle </option>
		                <option <?php if($relation=='Cousin'){echo "selected";}?>>Cousin </option>
			
			</select>
		</td>
		<td>

	<input type="checkbox" id="low" name="Issues[]" value="None"<?php if (in_array("None", $iss)) {echo "checked";}?>>
	<label >None</label><br>

	<input type="checkbox" id="med" name="Issues[]" value="Allergies Indoor"<?php if (in_array("Allergies Indoor", $iss)) {echo "checked";}?> >
	<label >Allergies Indoor</label><br>

	<input type="checkbox" id="med" name="Issues[]" value="Allergies Outdoor" <?php if (in_array("Allergies Outdoor", $iss)) {echo "checked";}?>>
	<label >Allergies Outdoor</label><br>

	<input type="checkbox" id="med" name="Issues[]" value="Asthma"<?php if (in_array("Asthma", $iss)) {echo "checked";}?> >
	<label >Asthma</label><br>

	<input type="checkbox" id="med" name="Issues[]" value="Sinus Problem"<?php if (in_array("Sinus Problem", $iss)) {echo "checked";}?> >
	<label >Sinus Problem</label><br>

	<input type="checkbox" id="med" name="Issues[]" value="Other"<?php if (in_array("Other", $iss)) {echo "checked";}?> >
	<label >Other</label><br>

		</td>
	</tr>
</table>

<?php
	}
}

?>	
	</td>
    </tr>
    <tr>
	<td>
<?php 
$lid=$_REQUEST['lid'];

$res=mysql_query("select * from tps_lead_card where id='".$lid."'") or die(mysql_error());
	
$r=mysql_fetch_array($res);

$address=$r['add_line1'].' ,'.$r['add_line2'].' '.$r['city'].' '.$r['state'].' '.$r['zip'];
$sq="select * from tps_events where lead_id='$r[id]' and appttype='Demo' and delete_flag='0'";
$res=mysql_query($sq) or die(mysql_error());
$appt1 = mysql_fetch_array($res); 

if($appt1['start']!='' && $appt1['start']!='' && $appt1['end']!=''){
	$startDate = date('M d Y',strtotime($appt1['start']));
	$startTime = date('h:i A',strtotime($appt1['start']));
	$endTime = date('h:i A',strtotime($appt1['end']));
	$apt_date_time=$startDate.'    '.$startTime.'-'.$endTime;
}
else{
	$apt_date_time='';
}

$sql_select="select * from tps_users where userid='$r[lead_dealer]'";
$result_select = mysql_query($sql_select) or die(mysql_error());
$row1 = mysql_fetch_array($result_select); 
$dealer="<b>".ucfirst($row1['fname'])." ".ucfirst($row1['lname'])."</b>";
$login_name="<b>".get_session('DISPLAY_NAME')."</b>"; 

$res=mysql_query("select * from tps_lead_card where id='".$lid."'") or die(mysql_error());
	
$r=mysql_fetch_array($res);

$res_users=mysql_query("select office_name,office_city from tps_users where userid='".get_session('LOGIN_USERID')."'") or die(mysql_error());
$ru=mysql_fetch_array($res_users);

if($r['fname1']!=''){
	$replacefname="<b>".$r['fname1']."</b>";
	$footer=str_replace("%first name1%",$replacefname,$rs['footer']); 
}else{
	$footer=str_replace("%first name1%","<b><a>%first name1%</a></b>",$rs['footer']); 
}

if($login_name!=''){
	$footer1=str_replace("%logged in user%",$login_name,$footer);
}else{
	$footer1=str_replace("%logged in user%","<b><a>%logged in user%</a></b>",$footer);
}

if($refby!=''){
	$refby1="<b>".$refby."</b>";
	$footer2=str_replace("%referred by%",$refby1,$footer1);
}else{
	$footer2=str_replace("%referred by%","<b><a>%referred by%</a></b>",$footer1);
}

if($r['lname1']!==''){
	$replacelname="<b>".$r['lname1']."</b>";
	$footer3=str_replace("%last name1%",$replacelname,$footer2);
}else{
	$footer3=str_replace("%last name1%","<b><a>%last name%</a></b>",$footer2);
}

$footer4=str_replace("%office name%","LCAS",$footer3);

if($dealer!=''){
	$footer5=str_replace("%dealer%" ,$dealer,$footer4);
}else{
	$footer5=str_replace("%dealer%" ,"<b><a>%dealer%</a></b>",$footer4);
}

if($r['fname2']!=''){
	$replacefname="<b>".$r['fname2']."</b>";
	$footer6=str_replace("%first name2%",$replacefname,$footer5);
}else{
	$footer6=str_replace("%first name2%","<b><a>%first name2%</a></b>",$footer5);
}

if($r['lname2']!=''){
	$replacelname="<b>".$r['lname2']."</b>";
	$footer7=str_replace("%last name2%",$replacelname,$footer6);
}else{
	$footer7=str_replace("%last name2%","<b><a>%last name2%</a></b>",$footer6);
}

if($r['add_line1']!==''){
	$rep_add1="<b>".$r['add_line1']."</b>";
	$footer8=str_replace("%addr1%",$r['add_line1'],$footer7);
}else{
	$footer8=str_replace("%addr1%","<b><a>%addr1%</a></b>",$footer7);
}

if($r['add_line2']!=''){
	$rep_add2="<b>".$r['add_line2']."</b>";
	$footer9=str_replace("%addr2%",$rep_add2,$footer8);
}else{
	$footer9=str_replace("%addr2%","<b><a>%addr2%</a></b>",$footer8);
}

if($address!=''){
	$address1="<b>".$address."</b>";
	$footer10=str_replace("%address%",$address1,$footer9);
}else{
	$footer10=str_replace("%address%","<b><a>%address%</a></b>",$footer9);
}

if($apt_date_time!=''){
	$apt_date_time1="<b>".$apt_date_time."</b>";
	$footer11=str_replace("%appt%",$apt_date_time1,$footer10);
}else{
	$footer11=str_replace("%appt%","<b><a>%appt%</a></b>",$footer10);
}

if($r['time_contact']!=''){
	$rep_time_contact="<b>".$r['time_contact']."</b>";
	$footer12=str_replace("%best time to contact%",$rep_time_contact,$footer11);
}else{
	$footer12=str_replace("%best time to contact%","<b><a>%best time to contact%</a></b>",$footer11);
}

if($r['lead_type']!=''){
	$rep_leadtype="<b>".$r['lead_type']."</b>";
	$footer13=str_replace("%lead type%",$rep_leadtype,$footer12);
}else{
	$footer13=str_replace("%lead type%","<b><a>%lead type%</a></b>",$footer12);
}

if($r['lead_subtype']!=''){
	$rep_leadsubtype="<b>".$r['lead_subtype']."</b>";
	$footer14=str_replace("%lead subtype%",$rep_leadsubtype,$footer13);
}else{
	$footer14=str_replace("%lead subtype%","<b><a>%lead subtype%</a></b>",$footer13);
}

if($r['referred_by']!=''){

	$ref=explode("_",$r['referred_by']);
	if($ref[0]==2){
$sq=mysql_query("select drawing_prize,campaign_prize,gift1,gift2,gift3,gift4,gift5,gift6 from tps_campaign where id='$ref[1]'")or die(mysql_error());
		$re=mysql_fetch_array($sq);
		$gift=array($re['gift1'],$re['gift2'],$re['gift3'],$re['gift4'],$re['gift5'],$re['gift6']);
		$gift=array_filter($gift);
		$gift=implode(",",$gift);
		$gift="<b>".$gift."</b>";

		if($gift!=''){
			$footer15=str_replace("%gift%",$gift,$footer14);
		}else{
			$footer15=str_replace("%gift%","<b><a>%gift%</a></b>",$footer14);
		}
	
		if($re['drawing_prize']!="")
		{
			$footer15=str_replace("%drawing prize%","<b>".$re['drawing_prize']."</b>",$footer15);
		}else{
			$footer15=str_replace("%drawing prize%","<b><a>%drawing prize%</a></b>",$footer15);
		}
		
		if($re['campaign_prize']!="")
		{
			$footer15=str_replace("%campaign prize%","<b>".$re['campaign_prize']."</b>",$footer15);
		}else{
			$footer15=str_replace("%campaign prize%","<b><a>%campaign prize%</a></b>",$footer15);
		}

	}else{

		if($r['gift']!=''){
			$rep_gift="<b>".$r['gift']."</b>";
			$footer15=str_replace("%gift%",$rep_gift,$footer14);
		}else{
			$footer15=str_replace("%gift%","<b><a>%gift%</a></b>",$footer14);
		}
	}

}else{

	if($r['gift']!=''){
		$rep_gift="<b>".$r['gift']."</b>";
		$footer15=str_replace("%gift%",$rep_gift,$footer14);
	}else{
		$footer15=str_replace("%gift%","<b><a>%gift%</a></b>",$footer14);
	}
}

echo trim(stripslashes($footer15));

$showyesno=$rs['showyesno'];

?>
	</td>
    </tr>
<!--
<?php 

if($_REQUEST['job']=="edit"){ 
		
	$tsdres=mysql_query("SELECT * FROM `tps_survey_details` where leadid='".$_REQUEST['lid']."' and lead_seqid='".$_REQUEST['leadseqid']."' ") or die(mysql_error());

		$tsdr=mysql_fetch_array($tsdres);
		$selans=$tsdr['call_if_selected'];

		if($showyesno=='1'){
?>
<tr>
<td>
	Can we call you if you are selected?  &nbsp;&nbsp;
	<input type="radio" name="callforgift" id="callforgift" value="Yes" class="icheck" <?php if($selans=="Yes") echo "checked"; ?> /> Yes &nbsp;
	<input type="radio" name="callforgift" id="callforgift1" value="No" class="icheck" <?php if($selans=="No") echo "checked"; ?>/> No
</td>
</tr>
		<?php   }
			
		  }else{  
			
			if($showyesno=='1'){
		 ?>
<tr>
<td>
	Can we call you if you are selected?  &nbsp;&nbsp;
	<input type="radio" name="callforgift" id="callforgift" value="Yes" class="icheck" /> Yes &nbsp;
	<input type="radio" name="callforgift" id="callforgift1" value="No" class="icheck" /> No	
</td>
</tr>
		<?php 
				}
			} 
		?>
		
-->
    <tr>
	<td align="right">
	<input type="hidden" name="showyesno" id="showyesno" value="<?php echo $showyesno; ?>" />
	<?php if($_REQUEST['job']=="edit"){ ?>
		<input type="submit" name="update" value="Update" id="update" class="btn btn-blue" />
	<?php }else{ ?>
		<input type="submit" name="save" value="Save" id="save" class="btn btn-blue" />
	<?php } ?>
		<input type="reset" name="cancel" value="Cancel" onclick="javascript:window.location='leadcard.php'" class="btn btn-default" />
	</td>
    </tr>
</tbody>
</table>
</form>

      </div>
    </div>
	<br /><br /><br />

<?php

if(isset($_REQUEST['update']))
{

	$id=request_get('lid');
	$leadseqid=request_get('leadseqid');

	$formtype=request_get('formtype');

	$timestamp=fmt_db_date_time();

	$data="";

	if($formtype=="Event_Form")
	{
		$home=request_get('home');
		$workstatus=request_get('workstatus');
		$pet=$_POST['p'];
		if(empty($pet))
		{$pets='';}
		else{
		foreach($pet as $check1) {
		$pets.=$check1.","; 
		} }
		$Issues=$_POST['issues'];
		if(empty($Issues))
		{$iss='';}
		else{
		foreach($Issues as $check) {
		$iss.=$check.","; 
		} }
		$guess=request_get('guess');
		$data = array($workstatus, $home,$pets,$iss,$guess);
		$data=json_encode( $data );
	}
	else if($formtype=="Door_Canvas_Form")
	{
		$uses="";
		$home=request_get('home');
		$pollution=request_get('pollution');
		$dust=$_POST['dust'];

		$pet=$_POST['p'];
		if(empty($pet))
		{$pets='';}
		else{
		foreach($pet as $check1) {
		$pets.=$check1.","; 
		} }
		$Issues=$_POST['issues'];
		if(empty($Issues))
		{$iss='';}
		else{
		foreach($Issues as $check) {
		$iss.=$check.","; 
		} }
		$use=$_POST['use'];
		if(empty($use))
		{$uses='';}
		else{
		foreach($use as $check3) {
		$uses.=$check3.","; 
		} } 

		$yh=request_get('yearsinhouse');
		$data = array($pollution,$dust,$home,$pets,$iss,$uses,$yh);
		$data=json_encode( $data );
		
	}else{

		$issues="";
		$childrens="";
		$pets="";
		$time="";

		$employement1=request_get('Employment1');
		$employement2=request_get('Employment2');
		$home=request_get('home');
		$timetocontact=$_POST['Time'];
		if(empty($timetocontact))
		{
		$time.='';
		}
		else{
		foreach($timetocontact as $check9) {
		$time.=$check9."<br>"; 
		} }
		$pet=$_POST['pets'];
		if(empty($pet))
		{$pets='';}
		else{
		foreach($pet as $check1) {
		$pets.=$check1.","; 
		} }
		$children=$_POST['child'];
		if(empty($children))
		{$childrens='';}
		else{
		foreach($children as $check2) {
		$childrens.=$check2.","; 
		} }
		$relation=request_get('relation');
		$Issues=$_POST['Issues'];
		if(empty($Issues))
		{$issues='';}
		else{
		foreach($Issues as $check) {
		$issues.=$check.","; 
		} }
		$worktype1=request_get('worktype1');
		$worktype2=request_get('worktype2');
		
		$data = array($employement1, $employement2,$home,$pets,$childrens,$relation,$issues,$worktype1,$worktype2,$time );
		$data=json_encode( $data );

	}

	$sql="update tps_lead_card set ".
		" `referral_data` = '".$data."', ".
		" `modified` = '". $timestamp ."', ".
		" `created_by` = '". get_session('DISPLAY_NAME') ."', ".
		" `modified_by` = '". get_session('DISPLAY_NAME') ."' ".
		" where id = '".$id."' and leadid='".$leadseqid."'";	
	
	$result=mysql_query($sql) or die(mysql_error());

	header("location:leadcard.php");
	exit;

}

/*
$lead_status="";
$apptnewstart="";

if(isset($_REQUEST['save']))
{

	$cur_leadid=$_REQUEST['lid'];
	$cur_leadseqid=$_REQUEST['leadseqid'];
	$cur_script_id=$rs['id'];
	$cur_no_of_questions=$rs['no_of_questions'];

	$surveyedScriptId=$cur_script_id;
	$surveyedQuestionsArr=array();

	for($i=1;$i<=$cur_no_of_questions;$i++)
	{
		$a="ans".$i;
		$q="ques".$i;		
		$qt="questionstype".$i;
		$qid="questionid".$i;
 
		$$a=$_REQUEST["$q"];

		$qt1=$_REQUEST["$qt"];

		$qids=$_REQUEST["$qid"];
		
		if($qt1=="3")
		{		
			$$a=implode(",",$$a);
		}
		
		$surveyedQuestionsArr[$i]=$qids;

		$sql_q="INSERT INTO `tps_survey_answers` (`id`, `leadid`, `leadseqid`, `script_id`, `script_question_id`, `script_question_type`, `no_of_questions`, `answeroption1`, `answeroption2`, `answeroption3`, `answeroption4`, `answeroption5`, `answeroption6`, `createdby`, `createdtime`) VALUES (NULL, '".$cur_leadid."', '".$cur_leadseqid."', '".$cur_script_id."', '".$qids."', '".$qt1."', '".$cur_no_of_questions."', '".$$a."', '', '', '', '', '', '".get_session('LOGIN_USERID')."', '".date('Y-m-d H:i:s')."')";
		
		mysql_query($sql_q)or die(mysql_error());
		
	}
	
	$surveyedQuestions=implode(',',$surveyedQuestionsArr);

	$timestamp =  time();

	$showyesno=$_REQUEST['showyesno'];

	if($showyesno=='1'){
		$callforgift=$_REQUEST['callforgift'];
	}else{
		$callforgift="Yes";
	}

	$job=$_REQUEST['job'];

	$sql_survey="INSERT INTO `tps_survey_details` (`id`, `leadid`, `lead_seqid`, `call_if_selected`, `survey_datetime`, `survey_by`, `gift_card_selected`, `followup_datetime`, `followup_by`, `createdby`, `createdtime`, `modifiedby`, `modifiedtime`) VALUES (NULL, '".$cur_leadid."', '".$cur_leadseqid."', '".$callforgift."', '".date('Y-m-d H:i:s')."', '".get_session('DISPLAY_NAME')."', '', '0000-00-00 00:00:00', '', '".get_session('DISPLAY_NAME')."', '".date('Y-m-d H:i:s')."', '', '');";

	mysql_query($sql_survey) or die(mysql_error());

	$user_logdispname=get_session('DISPLAY_NAME');
	$user_logid=get_session('LOGIN_ID');
	$url= $_SERVER['HTTP_REFERER'];

	$log_desc= ucfirst($user_logdispname)." surveyed $survey_leadname at ".date('M d Y h:i:s A').". <br><b><a href=$url target=_blank >$url</a></b>";
	tps_log_error(__INFO__, __FILE__, __LINE__, "Surveyed $survey_leadname ", $user_logid, $log_desc);

	if($callforgift=="Yes")
	{
		$lead_status="Surveyed CB"; 
	}else if ($callforgift=="No")
	{
		$lead_status="Surveyed NI"; 
	}

	$sql="update tps_lead_card set lead_status='".$lead_status."', is_surveyed='1', surveyed_script='$surveyedScriptId', surveyed_questions='$surveyedQuestions', modified='".$timestamp."', modified_by='".get_session('DISPLAY_NAME')."' where id='".$cur_leadid."' and leadid='".$cur_leadseqid."'";

	mysql_query($sql) or die(mysql_error());

	$user_logdispname=get_session('DISPLAY_NAME');
	$user_logid=get_session('LOGIN_ID');
	$url= $_SERVER['HTTP_REFERER'];
	$url=str_replace("job=add","job=edit",$url);

	$log_desc= ucfirst($user_logdispname)." surveyed $survey_leadname at ".date('M d Y h:i:s A').", Lead status updated as $lead_status. <br><b><a href=$url target=_blank >$url</a></b>";
		tps_log_error(__INFO__, __FILE__, __LINE__, "Lead Status Updated in Survey", $user_logid, $log_desc);

 	// tps_events where lead_id
		$hour = date('H');
		$minute = (date('n')>30)?'30':'00';

		$nxt2week = strtotime("+2 weeks");
		$start = date('Y-m-d', $nxt2week)." ".$hour.":".$minute.":00"; 

		$hour1 = date('H')+3;
		$minute1 = (date('n')>30)?'30':'00';

		$end=date('Y-m-d', $nxt2week)." ".$hour1.":".$minute1.":00"; 
	
		if($job=="add")
		{

		$start=date('Y-m-d', $nxt2week)." ".$hour.":".$minute.":00 ".date('A'); 

echo '<script> var lid="'.$cur_leadid.'";
	var ls= "'.$lead_status.'";
	var lseqid="'.$cur_leadseqid.'";
	var nxtapt="'.$start.'";
	var callgift="'.$callforgift.'";

	if(lid!=="" && ls!="" && nxtapt!="")
	{
	if(lid!=null || ls!=null || nxtapt!=null )
	{
		
		if(nxtapt!="no")
		{
		   var txt=" <br>Your Survey Saved with Lead Status "+ls+" Successfully! <br><br> The Call Date will be on <b>"+nxtapt+"</b>";
		   //bootbox.alert(txt);
			$.ajax({
	    		type: "POST",
	    		url: "nxtaptstatus.php?lid="+lid+"&leadseqid="+lseqid+"&ls="+ls+"&nxtapt="+nxtapt+"&callgift="+callgift,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:350,
							openjs:function(){
								
							 $("#startcontact").datetimepicker({
								format:"m-d-y",
								lang:"en",
								timepicker:false
							});

							$("#cancel").click(function(){
								window.parent.TINY.box.hide();
								window.location.href = "leadcard.php";
							});

							    $("#update").click(function(){

								var activity=$("#activity").val();
								var contactreason=$("#contactreason").val();
								var leadstatus=$("#leadstatus").val();
								var startcontact=$("#startcontact").val();
								var leadseqid=$("#leadseqid").val();
								var leadid=$("#leadid").val();

								var data = {
    									type: "updEvent",
									activity: activity,
									contactreason:contactreason,
									leadstatus:leadstatus,
									startcontact:startcontact,
									leadseqid:leadseqid,
									leadid:leadid
								}
								$.ajax({
	    								type: "POST",
	    								url: "nxtaptstatus.php",
	    								data: data,
	    								success: function(output) {
										window.location.href = "leadcard.php";
										window.parent.TINY.box.hide();
									}
								});

							    });
						  }
					    });
				}
			});
		}else{
		   var txt=" <br>Your Survey Saved with Lead Status "+ls+" Successfully! <br>";
		   bootbox.alert(txt);
		}
	}
	}</script>';
			//$start=date('Y-m-d', $nxt2week)." ".$hour.":".$minute.":00 ".date('A'); 
			//header("location:leadcard.php?lid=$cur_leadid&leadseqid=$cur_leadseqid&ls=$lead_status&nxtapt=$start");	
			//exit;
		}else if($job=="edit")
		{
			header("location:survey_followup_qa_form.php?lid=$cur_leadid&leadseqid=$cur_leadseqid");	
			exit;
		}else{
			header("location:leadcard.php");	
			exit;
		}	
}
 

if(isset($_REQUEST['update']))
{

	$job=$_REQUEST['job'];

	$cur_leadid=$_REQUEST['lid'];
	$cur_leadseqid=$_REQUEST['leadseqid'];
	$cur_script_id=$rs['id'];
	$cur_no_of_questions=$rs['no_of_questions'];

	for($i=1;$i<=$cur_no_of_questions;$i++)
	{
		$a="ans".$i;
		$q="ques".$i;		
		$qt="questionstype".$i;
		$qid="questionid".$i;
 
		$$a=$_REQUEST["$q"];

		$qt1=$_REQUEST["$qt"];

		$qids=$_REQUEST["$qid"];
		
		if($qt1=="3")
		{		
			$$a=implode(",",$$a);
		}
		
$sql_q="UPDATE `tps_survey_answers` SET  `answeroption1`='".$$a."' WHERE `leadid`='".$cur_leadid."' and `leadseqid`='".$cur_leadseqid."' and `script_id`='".$cur_script_id."' and `script_question_id`='".$qids."' and `script_question_type`='".$qt1."' and `no_of_questions`='".$cur_no_of_questions."'";
		
		mysql_query($sql_q)or die(mysql_error());
	}
	
	$timestamp =  time();

	$showyesno=$_REQUEST['showyesno'];

	if($showyesno=='1'){
		$callforgift=$_REQUEST['callforgift'];
	}else{
		$callforgift="Yes";
	}

	if($callforgift=="Yes")
	{
	// in tps_lead_card upd- lead_status
		$lead_status="Surveyed CB"; 
		
		$sql="update tps_lead_card set lead_status='".$lead_status."', modified='".$timestamp."', modified_by='".get_session('DISPLAY_NAME')."' where id='".$cur_leadid."' and leadid='".$cur_leadseqid."'";

		mysql_query($sql) or die(mysql_error());

		$user_logdispname=get_session('DISPLAY_NAME');
		$user_logid=get_session('LOGIN_ID');
		$url= $_SERVER['HTTP_REFERER'];

	$log_desc= ucfirst($user_logdispname)." updated $survey_leadname surveyed details at ".date('M d Y h:i:s A').", Lead status updated as $lead_status.<br><b><a href=$url target=_blank >$url</a></b>";
		tps_log_error(__INFO__, __FILE__, __LINE__, "$survey_leadname - Lead Status updated in Survey", $user_logid, $log_desc);


 	// tps_events where lead_id
		$hour = date('H');
		$ap = date('A');
		$minute = (date('n')>30)?'30':'00';

		$nxt2week = strtotime("+2 weeks");
		$start = date('Y-m-d', $nxt2week)." ".$hour.":".$minute.":00"; 

		$hour1 = date('H')+3;
		$minute1 = (date('n')>30)?'30':'00';

		$end=date('Y-m-d', $nxt2week)." ".$hour1.":".$minute1.":00"; 
	
		$sql1="UPDATE tps_events set start='".$start."', end='".$end."', modifiedby='".get_session('DISPLAY_NAME')."', modifiedtime='".date("Y-m-d H:i:s")."' WHERE lead_id='".$cur_leadseqid."'";

		mysql_query($sql1) or die(mysql_error());

		
		if($job=="add")
		{
			$start=date('Y-m-d', $nxt2week)." ".$hour.":".$minute.":00 ".date('A'); 
			header("location:leadcard.php?lid=$cur_leadid&ls=$lead_status&nxtapt=$start");	
			exit;
		}else if($job=="edit")
		{
			header("location:survey_followup_qa_form.php?lid=$cur_leadid&leadseqid=$cur_leadseqid");	
			exit;
		}else{
			header("location:leadcard.php");	
			exit;
		}
       
	
	}else if ($callforgift=="No")
	{
	// in tps_lead_card upd- lead_status		
		$lead_status="Surveyed NI"; 
		
		$sql="update tps_lead_card set lead_status='".$lead_status."', modified='".$timestamp."', modified_by='".get_session('DISPLAY_NAME')."' where id='".$cur_leadid."' and leadid='".$cur_leadseqid."'";

		mysql_query($sql) or die(mysql_error());

		$user_logdispname=get_session('DISPLAY_NAME');
		$user_logid=get_session('LOGIN_ID');
		$url= $_SERVER['HTTP_REFERER'];

	$log_desc= ucfirst($user_logdispname)." updated $survey_leadname survey details at ".date('M d Y h:i:s A').", Lead status updated as $lead_status. <br><b><a href=$url target=_blank >$url</a></b>";
		tps_log_error(__INFO__, __FILE__, __LINE__, "$survey_leadname Lead Status updated in Survey", $user_logid, $log_desc);

		if($job=="add")
		{
			header("location:leadcard.php?lid=$cur_leadid&ls=$lead_status&nxtapt=no");	
			exit;
		}else if($job=="edit")
		{
			header("location:survey_followup_qa_form.php?lid=$cur_leadid&leadseqid=$cur_leadseqid");	
			exit;
		}else{
			header("location:leadcard.php");	
			exit;
		}

	}
	
	
	
}
*/
?>	
<br> <br>	
   </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";

?>
<style>
.table a{text-decoration:underline}
</style>
