<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();
$page_name = "add_team.php";
$page_title = $site_name." Add /Edit Team";
$cur_page="configuration";
$cname='';
$ctoc='';
$title='' ;
$fname =  '' ;
$lname='' ;
$sdate='';
$sdate='';
$attachments='';
$gift1='';
$gift2='';
$gift3='';
$gift4='';
$gift5='';
$gift6='';
$add_line='' ;
$add_line2='' ;
$city='' ;
$state='' ;
$zip='' ;
$dprize='';
$cprize='';
$phonetype1='' ;
$phone1='' ;
$phonetype2='' ;
$phone2='' ;
$phonetype3='' ;
$phone3='' ;
$edate='';
$email1='' ;
$email2='' ;
$active='1';
$email_opt_in='' ;
$time_contact='' ;
$comments='' ;
$apptdate='' ;
$appttimefrom='' ;
$appttimeto='' ;

$ltype='' ;
$lsubtype='';
$refby='' ;
$ldeal='' ;
$lasst='' ;
$lindate='' ;
$lkdate='' ;
$lstatus='' ;
$lres='' ;
$gift='' ;
$lid='';
$apptresult='' ;
$user_dispname=get_session('DISPLAY_NAME');
$user_logid=get_session('LOGIN_ID');
$url= $_SERVER['HTTP_REFERER'];
function getExtension($str) 
{

         $i = strrpos($str,".");
         if (!$i) { return ""; } 

         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }
if( request_get('action')=='delete') {
$lid=request_get('lid');

$sql="update tps_campaign set delete_flag='1' where id='$lid'";
$result=mysql_query($sql)or die(mysql_error());
$message="Campaign has been Deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		header("Location: campaign.php" );
		exit;
}

if( request_get('action')=='edit') {
$lid=request_get('lid');

	if ($lid > 0 ) {
		$sql="select * from tps_campaign where id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);
		$cname=$row['campaign'];
		$title=$row['title'];
		$fname =  $row['fname'];
		$lname=$row['lname'];
		$add_line=$row['add_line'];
		$city=$row['city'];
		$state=$row['state'];
		$zip=$row['zip'];
		$lsubtype=$row['lead_subtype'];
		$phonetype1=$row['phonetype1'];
		$phone1=$row['phone1'];
		$phonetype2=$row['phonetype2'];
		$phone2=$row['phone2'];
		
		$email1=$row['email1'];

		$email_opt_in=$row['email_opt_in'];
		$lead_type=$row['lead_type'];
		$campaign_type=$row['campaign_type'];
		$campaign_subtype=$row['campaign_subtype'];
		$ldeal=$row['lead_dealer'];
		$lasst=$row['lead_asst'];
		$date = date_create($row['start_date']);
		$sdate=date_format($date, 'M d Y');
		$date = date_create($row['expiration_date']);
		$edate=date_format($date, 'M d Y');
		$dprize=$row['drawing_prize'];
		$cprize=$row['campaign_prize'];
		$gift1=$row['gift1'];
		$gift2=$row['gift2'];
		$gift3=$row['gift3'];
		$gift4=$row['gift4'];
		$gift5=$row['gift5'];
		$gift6=$row['gift6'];
		$pic=$row['attachments'];
		$comments=$row['comments'];
		$active=$row['active'];
		$attachments=$row['attachments'];
		
	}
}
if( isset($_POST['savecampaign']) ){
$pic=request_get('pic');
$lid=request_get('lid');
	if (isset($_FILES["file"]["type"])) {
$allowed =  array('gif','png' ,'jpg', 'pdf' ,'jpeg','zip','txt','doc','docx','xls','xlsx','pdf','rar');
$filename = $_FILES['file']['name'];
$ext = pathinfo($filename, PATHINFO_EXTENSION);
$ext =strtolower($ext);
			if(!in_array($ext,$allowed) ) {
   				 echo 'error';
				}
			else{
			$ext = getExtension($filename);
			$actual_file_name = time().substr(str_replace(" ", "_", $ext), 5).".".$ext;
			$file_name= $actual_file_name;
			$target = "images/attachments/"; 
			$target = $target . $actual_file_name; 
			$pic=$file_name; 
			move_uploaded_file($_FILES['file']['tmp_name'],$target);
			}

}
	

$timestamp=fmt_db_date_time();
if( $lid > 0){


		$sql="update tps_campaign set ".
		" `campaign` = '".ucfirst(request_get('cname'))  ."', ".
		" `title` = '".ltrim(request_get('prefname1'),"Select")  ."', ".
		" `fname` = '".  ucfirst(request_get('fname1')) ."', ".
		" `lname` = '". request_get('lname1') ."', ".
		" `add_line` = '". request_get('addr1') ."', ".
		" `city` = '". request_get('city') ."', ".
		" `state` = '". request_get('state1') ."', ".
		" `zip` = '". request_get('zip') ."', ".
		" `phonetype1` = '". request_get('phone1_type') ."', ".
		" `phone1` = '". request_get('phone1')."', ".
		" `phonetype2` = '". request_get('phone2_type') ."', ".
		" `phone2` = '". request_get('phone2') ."', ".
		" `email1` = '". request_get('email1') ."', ".
		" `email_opt_in` = '". request_get('eoptin') ."', ".
		" `lead_type` = '". request_get('lead_type') ."', ".
		" `lead_subtype` = '". request_get('lsubtype') ."', ".
		" `campaign_type` = '". request_get('campaign_type') ."', ".
		" `campaign_subtype` = '". request_get('campaign_subtype') ."', ".
		" `lead_asst` = '". request_get('lasst') ."', ".
		" `lead_dealer` = '". ltrim(request_get('ldeal'),"Select") ."', ".
		" `start_date` = '".date('Y-m-d',strtotime(request_get('sdate'))) ."', ".
		" `expiration_date` = '".date('Y-m-d',strtotime(request_get('edate'))) ."', ".
		" `drawing_prize` = '". request_get('dprize') ."', ".
		" `campaign_prize` = '". request_get('cprize') ."', ".
		" `gift1` = '". request_get('gift1') ."', ".
		" `gift2` = '". request_get('gift2') ."', ".
		" `gift3` = '". request_get('gift3') ."', ".
		" `gift4` = '". request_get('gift4') ."', ".
		" `gift5` = '". request_get('gift5') ."', ".
		" `gift6` = '". request_get('gift6') ."', ".
		" `attachments` = '". $pic ."', ".
		" `comments` = '". request_get('comment1') ."', ".
		" `active` = '". request_get('active') ."', ".
		" `modified` = '". $timestamp ."', ".
		" `modified_by` = '". get_session('DISPLAY_NAME') ."' where id='".$lid."'";	
		$result=mysql_query($sql) or die(mysql_error());
$log_desc= ucfirst($user_dispname)." Update ".request_get('cname')." Campaign Details.<br> <b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Campaign Updated", $user_logid, $log_desc);
$campaign_name=request_get('campaignname') ;
$uid=get_session('LOGIN_ID');
$cname=ucfirst(request_get('cname')); 
$sql="update tps_lead_card set referred_by='$cname' where referred_by='$campaign_name' and uid='$uid'";
$result=mysql_query($sql)  or die(mysql_error());
              $message="Campaign has been updated";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		header("Location: campaign.php" );
		exit;
		
	}
	else{

	$leadId =generate_id('leadcard','leadcard_id');
	set_session('leadid',$leadId);

	$sql="insert into tps_campaign set ".
		" `uid` = '". get_session('LOGIN_ID') ."', ".
		" `campaign` = '".request_get('cname')  ."', ".
		" `title` = '".ltrim(request_get('prefname1'),"Select")  ."', ".
		" `fname` = '".  ucfirst(request_get('fname1')) ."', ".
		" `lname` = '". request_get('lname1') ."', ".
		" `add_line` = '". request_get('addr1') ."', ".
		" `city` = '". request_get('city') ."', ".
		" `state` = '". request_get('state1') ."', ".
		" `zip` = '". request_get('zip') ."', ".
		" `phonetype1` = '". request_get('phone1_type') ."', ".
		" `phone1` = '". request_get('phone1')."', ".
		" `phonetype2` = '". request_get('phone2_type') ."', ".
		" `phone2` = '". request_get('phone2')."', ".
		" `email1` = '". request_get('email1') ."', ".
		" `email_opt_in` = '". request_get('eoptin') ."', ".
		" `lead_type` = '". request_get('lead_type') ."', ".
		" `lead_subtype` = '". request_get('lsubtype') ."', ".
		" `campaign_type` = '". request_get('campaign_type') ."', ".
		" `campaign_subtype` = '". request_get('campaign_subtype') ."', ".
		" `lead_asst` = '". request_get('lasst') ."', ".
		" `lead_dealer` = '". ltrim(request_get('ldeal'),"Select") ."', ".
		" `start_date` = '".date('Y-m-d',strtotime(request_get('sdate'))) ."', ".
		" `expiration_date` = '".date('Y-m-d',strtotime(request_get('edate'))) ."', ".
		" `drawing_prize` = '". request_get('dprize') ."', ".
		" `campaign_prize` = '". request_get('cprize') ."', ".
		" `gift1` = '". request_get('gift1') ."', ".
		" `gift2` = '". request_get('gift2') ."', ".
		" `gift3` = '". request_get('gift3') ."', ".
		" `gift4` = '". request_get('gift4') ."', ".
		" `gift5` = '". request_get('gift5') ."', ".
		" `gift6` = '". request_get('gift6') ."', ".
		" `attachments` = '". $pic ."', ".
		" `comments` = '". request_get('comment1') ."', ".
		" `active` = '". request_get('active') ."', ".
		" `modified` = '". $timestamp ."', ".
		" `modified_by` = '". get_session('DISPLAY_NAME') ."',".
		" `created` = '". $timestamp ."', ".
		" `created_by` = '". get_session('DISPLAY_NAME') ."' ";
		$result=mysql_query($sql) or die(mysql_error());
$log_desc= ucfirst($user_dispname)." Added  ".request_get('cname')." as a  New Campaign .<br> <b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "New Campaign Added", $user_logid, $log_desc);

		$message="New Campaign Added";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		
		header("Location: campaign.php" );
		exit;
	}
}


include_once( "lcas_header.php" );
include_once( "lcas_top_nav.php" );
include_once( "lcas_left_nav.php" );
echo getLeadSubType333($lsubtype,$ltype);
$asdf2=getGift_new($ltype,$lsubtype,$gift);
echo "<script>var asdf2='".$asdf2."';</script>";		
include_once( "js/add_campaign.js" );	
?>
<div class="area-top clearfix">
<div class="pull-left header">
<h3 class="title">
<i class="icon-magic"></i>
<?php if(request_get('action')){echo "Edit";}else{echo"Add";} ?>   Campaign 
</h3>
</div><?php if( get_session('e_flag') == 1 ) 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:250px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
</div>

<form id="frmaddleads" name="frmaddleads" action="add_campaign.php" method="POST" onsubmit="myFunction();" enctype="multipart/form-data" >
 <div class="row" style="margin-top:10px;" >
  <div class="col-md-4">
    <div class="box" style="margin-left:10px;min-height:565px;">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Name and Address</span>
      </div>
      <div class="box-content padded">
   <label id="label" class="nline">   Campaign Name * </label><input type="text"  tabindex="1"name="cname" id="cname"  value="<?php echo $cname; ?>" placeholder="Campaign Name"  style="width:150px;margin-left:5px;"required/>
		<input class="nline firstli" type="radio" name="active" id="active"  value="1"  style="margin-left:10px;" <?php if($active=='1'){echo "checked";}?>>
		Active<input class="nline firstli" type="radio" name="active" id="active"  value="0"  style="margin-left:5px;" <?php if($active=='0'){echo "checked";}?>>
		In Active<br/><br/>

	<label id="label">	<select class="nline firstli" name="prefname1" id="prefname1"  >
<option value="">Select</option>
		<?php echo getNamePrefix($title); ?>
		</select></label>
		<input type="text" class="validate[required] firstli" data-prompt-position="topLeft" name="fname1" value="<?php echo $fname; ?>" id="fname2" placeholder="First Name " style="width:150px;margin-left:5px;"/>
		<input class="nline firstli"type="text" name="lname1" id="lname1"  value="<?php echo $lname; ?>" style="width:150px;" placeholder="Last Name" />
		<br/><br/>
<label id="label">Campaign Address</label>
		<input  class="nline firstli" type="text" name="addr1" id="addr1"  value="<?php echo $add_line; ?>" placeholder="Address Line1" style="width:180px;" />
		<input class="nline firstli" type="text" name="zip" id="zip" placeholder="Zipcode "  value="<?php echo $zip; ?>" style="width:120px;"  /><br/><br/>
<label id="label"></label>
		<input class="nline firstli" type="text" name="city" id="city"  value="<?php echo $city; ?>" placeholder="City"/>

	<select class="nline " name="state1" id="state1" >
		<?php 
			if($state=='') { $state = __DEFAULT_STATE__; }

			echo getStates($state);
		?>
		</select>
		
		<br/><br/><label id="label">
		<select class="nline " name="phone1_type" id="phone1_type" >
		<?php echo getPhoneType($phonetype1); ?>
		</select></label>
		<?php
		if($phone1 != '')
			echo '<input class="nline " type="text" name="phone1"  id="phone1" value="'.$phone1.'" placeholder="Phone Number *"style="width:110px;" >';
		else
			echo '<input class="nline " type="text" name="phone1"  id="phone1" value="(717) . "  placeholder="Phone Number" style="width:110px;" >';
		?>
	<select class="nline" name="phone2_type" >
		<?php echo getPhoneType($phonetype2); ?>
		</select>
		<?php
		if($phone1 != '')
			echo '<input class="nline" type="text" name="phone2"  id="phone2" value="'.$phone2.'"  placeholder="Phone Number" style="width:110px;"/>';
		else
			echo '<input class="nline" type="text" name="phone2"  id="phone2" value="(717) . "  placeholder="Phone Number"style="width:110px;"/>';
		?>	
		<br /><br/>
		

		
<label id="label" class="nline">	Email Address	</label><input class="nline firstli" type="text"  name="email1" id="email1"  value="<?php echo $email1; ?>" onblur="validateEmail('email1', 'erremail1')"  placeholder="Email-Id"style="width:210px;margin-left:5px;" />
		<?php 
		if($email_opt_in==1 )
			echo '<input type="checkbox"  checked="checked"  id="chkemail1" value="email1" >';
		else
			echo '<input type="checkbox"    id="chkemail1" value="email1" >';
		?>
		Opt In
		<br/><br/>
<textarea placeholder="Notes" rows="5"  class="nline"  name="comment1" id="comment1"style="width:100%;" ><?php echo $comments;?></textarea ><br />

		<div class="alert alert-error" id="erremail1" style="display:none;position:absolute;bottom:4px;left:10%;">
		Email Format should be
		<strong>test@test.com</strong>
		</div>
      </div>
    </div>
  </div>



  <div class="col-md-3">
    <div class="box" style="min-height:565px;">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Campaign Details</span>
      </div>
      <div class="box-content padded">
	<label  class="nline" id="label">Lead Type *</label>
	<select name="lead_type" id="ltype" style="width:150px;" class="nline firstli" title="Lead Type" data-prompt-position="topLeft"onchange="showsubtype(this);" required>
		<option value=""class="Select">Select</option>
		

	<?php echo getLeadType($lead_type); ?>


		</select><br /><br/>

<label  class="nline" id="label">Lead SubType  * </label>
	<select class="nline" name="lsubtype" id="lsubtype" onchange="show_gift(this);" style="width:150px;" required>
	<option value=""class="Select">Select</option>
	<?php 

		echo getLeadSubType222($lsubtype,$lead_type);
	
		
	?>
	</select> <br /><br/>
<label  class="nline" id="label"> Campaign Type * </label><select name="campaign_type" id="campaign_type"onchange="showsubcampaign(this);" class="nline firstli" title="Lead Type" data-prompt-position="topLeft"style="width:150px;" required>
		<option class="Select"value="">Select</option>
		<?php

	$sql=mysql_query("select * from tps_campaign_type") or die(mysql_error());
	while($row=mysql_fetch_array($sql)){
		
		if($campaign_type==$row['name'])
		{
			echo "<option class='".$row['id']."'value='".$row['name']."' selected >".$row['display']."</option>";
		}else{
			echo "<option class='".$row['id']."'value='".$row['name']."' >".$row['display']."</option>";
		}

	}	

		?>
		</select><br /><br/>
		<label  class="nline" id="label"> Campaign Subtype </label> <select name="campaign_subtype" id="campaign_subtype" class="nline firstli" title="Lead Type" data-prompt-position="topLeft"style="width:150px;" >
		<option class="Select"value="">Select</option>
		<?php

	$sql=mysql_query("select * from tps_campaign_subtype") or die(mysql_error());
	while($row=mysql_fetch_array($sql)){
		
		if($campaign_subtype==$row['name'])
		{
			echo "<option class='".$row['parent']." campaignsubtype'value='".$row['name']."' selected >".$row['display']."</option>";
		}else{
			echo "<option class='".$row['parent']."'value='".$row['name']."' style='display:none;'>".$row['display']."</option>";
		}

	}	

		?>
		</select><br /><br/>
<label  class="nline" id="label">Lead Dealer  *</label> 
		<select class="nline" name="ldeal" id="ldeal"style="width:150px;"  required>
		<option value="">Select</option>
		<?php
		
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

	$re=mysql_query("select * from tps_users where status=1");

	while($r=mysql_fetch_array($re))
	{        if($ldeal==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".$r['fname']." ".$r['lname']."</option>";
	}
		 //echo getTeam($ldeal);
		 ?>
		</select><br /><br/>
	<label  class="nline" id="label">Lead Asst </label> 
	<select class="nline" name="lasst" id="lasst" style="width:150px;"><option value="">Select</option>
	
		<?php

	$re=mysql_query("select * from tps_users where lead_assist_flag=1  and status='1'");

	while($r=mysql_fetch_array($re))
	{        if($lasst==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".$r['fname']." ".$r['lname']."</option>";
	}
		 
		 ?>

		</select><br /><br/>
From *
<input type="text" id="from" name="sdate" value="<?php echo $sdate; ?>" style="width:110px;" required>
To *
<input type="text" id="to" name="edate" value="<?php echo $edate; ?>"  style="width:110px;" required>

<?php 
if(isset($_REQUEST['action'])=="edit"){

$form_sid=request_get('lid');
?>
<br/><br/><br/><br/>
Select Form to Enter New Leads <br>
<select onchange="location = this.options[this.selectedIndex].value;">
    <option>Please select Form</option>
    <option value="referral_form/eventform.php?action=fill&lid=<?php echo $form_sid; ?>">Event Form</option>
    <option value="referral_form/doorcanvas.php?action=fill&lid=<?php echo $form_sid; ?>">Door Canvas Form</option>
    <option value="referral_form/boxform.php?action=fill&lid=<?php echo $form_sid; ?>">Box Form</option>
</select>
<br/>
<?php
}
?>
</div>
    </div>
  </div>


  <div class="col-md-4" id='last'>
    <div class="box" style="min-height:565px;">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Campaign Incentives</span>
      </div>
      <div class="box-content padded">
<label  class="nline" id="label">Drawing Prize *</label>
<input type="text" class="validate[required] firstli"  name="dprize" value="<?php echo $dprize; ?>" id="fname2" placeholder="Drawing Prize" style="width:210px;" required/><br><br>
<label  class="nline" id="label">Campaign Prize *</label>
<input type="text" class="validate[required] firstli"  name="cprize" value="<?php echo $cprize; ?>" id="fname2" placeholder="Campaign Prize"  style="width:210px;" required/><br><br>
<label  class="nline" id="label">Gift Choice 1</label>
		<select class="nline" name="gift1" id="gift" >
<option value=""class="Select">Select</option>
		<?php 
			echo getGift_new222($lead_type,$lsubtype,$gift1);
			//echo getGift(__DEFAULT_GTYPE__);
		?>
		</select><br /><br/>
<label  class="nline" id="label">Gift Choice 2</label>
		<select class="nline" name="gift2" id="gift2" >
<option value="" class="Select">Select</option>
		<?php 
			echo getGift_new222($lead_type,$lsubtype,$gift2);
			//echo getGift(__DEFAULT_GTYPE__);
		?>
		</select><br /><br/>
<label  class="nline" id="label">Gift Choice 3</label>
		<select class="nline" name="gift3" id="gift3" >
<option value="" class="Select">Select</option>
		<?php 
			echo getGift_new222($lead_type,$lsubtype,$gift3);
			//echo getGift(__DEFAULT_GTYPE__);
		?>
		</select><br /><br/>
<label  class="nline" id="label">Gift Choice 4</label>
		<select class="nline" name="gift4" id="gift4" >
<option value="" class="Select">Select</option>
		<?php 
			echo getGift_new222($lead_type,$lsubtype,$gift4);
			//echo getGift(__DEFAULT_GTYPE__);
		?>
		</select><br /><br/>
<label  class="nline" id="label">Gift Choice 5</label>
		<select class="nline" name="gift5" id="gift5" >
<option value="" class="Select">Select</option>
		<?php 
			echo getGift_new222($lead_type,$lsubtype,$gift5);
			//echo getGift(__DEFAULT_GTYPE__);
		?>
		</select><br /><br/>
<label  class="nline" id="label">Gift Choice 6</label>
		<select class="nline" name="gift6" id="gift6" >
<option value="" class="Select">Select</option>
		<?php 
			echo getGift_new222($lead_type,$lsubtype,$gift6);
			//echo getGift(__DEFAULT_GTYPE__);
		?>
		</select><br /><br />

<label  class="nline" id="label">Attachment</label>


<input id="file"  type="file"  name="file" >
<br /><br />
<div id="g2">
<a id="download" href='file_action.php?type=download&attachments=<?php echo $attachments; ?>'><img  src="images/download.png">
<?php echo $attachments; ?>  </a>
<a id="delete_option">
<img src="images/small-bin.png">
Delete
</a>
</div>
<br />

      
<div id="button"><?php 
if(isset($_REQUEST['action'])!="edit"){
?>
<input type="submit" class="btn btn-blue nline" value="Save" name="savecampaign" id="savelead" onclick="return ctoccheck()"style="margin-left:0px;margin-top:0px;width:100px;" >
<?php
}else
{?>
<input type="submit" class="btn btn-blue" value="Update" name="savecampaign" id="savelead" onclick="return ctoccheck();"style="margin-top:0px;width:100px;">
<?php
}
?>
<button type="button" class="btn btn-default" onclick="javascript:window.location='campaign.php';" style="margin-left:20px;margin-top:0px;width:100px;height:35px;">Cancel</button></div>
  </div>

</div>


  </div>
		<input type="hidden" name="campaignname" value="<?php echo $cname; ?>">
		<input type="hidden" name="eoptin" id="eoptin" placeholder="eoptin" >
		<input type="hidden" id="attachments" value="<?php echo $attachments; ?>">
	<input type="hidden" name="pic" value="<?php echo $pic; ?>">


<input type="hidden" id="lid" value="<?php echo $lid; ?>" name="lid">

</form>
</div>

    </div>

<?php
include "lcas_footer.php";
?>
  <script>$('a').attr("tabindex","-1");</script>
