<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
validate_login();
if($_REQUEST['type'] == 'editlead'){
		$lead_id = $_REQUEST['leadid'];
		$popupHtml = genPopup($lead_id);
		$event_edit = array(
			      'popupHtml' => $popupHtml,
			      
			  	);
		echo json_encode($event_edit);


}
if($_REQUEST['type'] =='editSaveEvent'){

	$name=get_session('DISPLAY_NAME');	
	$timestamp=fmt_db_date_time();

	$lead_type=$_POST['leadtype'];
	$lead_subtype=$_POST['leadsubtype'];
	$lead_dealer=$_POST['leaddealer'];
	$lead_status=$_POST['status'];
	$lead_result=$_POST['result'];
	$lead_quali=$_POST['quali'];
	$lead_indate=$_POST['leadindate'];
	$startcont=$_POST['startcont'];

	$lead=$_POST['leadid'];
	$lead_id=implode(",",$lead);

	$query="";

	$delSurvey=0;

	$values_for_upd=array($lead_type,$lead_subtype,$lead_dealer,$lead_status,$lead_result,$lead_quali,$lead_indate,$startcont);
	$n=1;
	foreach($values_for_upd as $value)
	{
		switch($n)
		{
			case 1:
			  $column='lead_type';
			  break;
			case 2:
			  $column='lead_subtype';
			  break;
			case 3:
			  $column='lead_dealer';
			  break;
			case 4:
			  $column='lead_status';
			  break;
			case 5:
			  $column='lead_result';
			  break;
			case 6:
			  $column='lead_qualification';
			  break;
			case 7:
			  $column='lead_in';
			  break;
			case 8:
			  $column='oktocall';
			  break;
		} 

		if($n==7 && $value!=''){
			$value=date('Y-m-d',strtotime($value));
		}

		if($n==8 && $value!=''){
			$value=date('Y-m-d',strtotime($value));
		}
		
		if($n==1 && $value!=''){
			$delSurvey=1;
		}

		if($value!=''){
			$query .= $column."='".$value."' , ";
		}
		$n++;
	}

	$string = rtrim($query, ' , ');

	foreach($lead as $l_id){    
		$sql2="update tps_lead_card set $string where leadid='$l_id' ";
		//echo $sql2."<br>";
		$result2=mysql_query($sql2)or die(mysql_error());  

		if($delSurvey==1)
		{
			$sql2="update tps_lead_card set is_surveyed='0' where leadid='$l_id' ";
			$result2=mysql_query($sql2)or die(mysql_error());  

			$sql4="delete from tps_survey_details where lead_seqid='$l_id'";
			$result4=mysql_query($sql4)or die(mysql_error());  

			$sql5="delete from tps_survey_answers where leadseqid='$l_id'";
			$result5=mysql_query($sql5)or die(mysql_error());  

		}

		if($lead_status!='')
		{
		$sql3="insert into tps_lead_status_update set leadid='$l_id', lead_status='$lead_status', modifiedby='$name', modifiedon='$timestamp'";
		//echo $sql3."<br>";	
	  	$result3=mysql_query($sql3)or die(mysql_error()); 
		}    
	}

	/*$sql="update tps_lead_card set lead_status='$lead_status',lead_result='$lead_result' where FIND_IN_SET(leadid,'$lead_id')";
	$result=mysql_query($sql)or die(mysql_error());  
	foreach($lead as $l_id){    
	   $sql2="insert into tps_lead_status_update set leadid='$l_id' , lead_status='$lead_status', modifiedby='$name', modifiedon='$timestamp' ";
	   $result2=mysql_query($sql2)or die(mysql_error());      
	}*/

	echo "Lead has been Updated";

}

function genPopup($lead_id){
	$sel='';
	$sr='';
	$lstatus='';
	$modifiedon=fmt_db_date_time();
	$name=get_session('DISPLAY_NAME');	
	$contactmethod='';
		
	$ltype='';
	$lsubtype='';
	$lead_subtype='';
	$lead_type='';
	$lead_quali="";

	$status= getLeadStatus(__DEFAULT_LSTATUS__);
	$result= getLeadResult(__DEFAULT_LRESULT__);

	$lead_type=getLeadType($ltype);

	$re=mysql_query("select fname,lname,userid from tps_users where status='1'");
	$lead_dealer="";
	while($r=mysql_fetch_array($re))
	{       
		$lead_dealer.="<option value='$r[userid]'>".ucfirst($r['fname'])." ".$r['lname']."</option>";
	}
	

	$lqres=mysql_query("select name,display from tps_lead_qualified where status='0'");
	$lead_quali="";
	while($lqr=mysql_fetch_array($lqres))
	{       
		$lead_quali.="<option value='$lqr[display]'>".$lqr['display']."</option>";
	}
	

$html= '<div class="box" style="height:auto;margin-top:0px;">
	<div class="box-header" ><span class="title">Activity Details </span></div>
	<div align="center" class="box-content padded">
		<div class="msg" id="msg" style="margin-left:auto;float:none;display:none; color: red; "></div>
		<table style="width:100%; border-spacing: 1px; border-collapse: separate;" id="edit-event-dialog-table" >
		<tbody>
		<tr>
		    <td>Lead Type</td>
		    <td><select id="bsuleadtype" name="bsuleadtype">
                        <option value="">Select</option>
			'.$lead_type.'
			</select>
		    </td>
		</tr>
		<tr>
		    <td>Lead Subtype</td>
		    <td><select id="bsuleadsubtype" name="bsuleadsubtype">
                        <option value="">Select</option>
			'.$lead_subtype.'
			</select>
		    </td>
		</tr>
		<tr>
		    <td>Lead Dealer</td>
		    <td><select id="bsuleaddealer" name="bsuleaddealer">
                        <option value="">Select</option>
			'.$lead_dealer.'
			</select>
		    </td>
		</tr>
		<tr>
			<td>Lead Status </td> 
			<td><select id="bsustatus">
                        <option value="">Select</option>
			'.$status.'
			</select></td>
		</tr>
		<tr>
			<td>Lead Result </td> 
			<td><select id="bsuresult">
                        <option value="">Select</option>
			'.$result.'
			</select>
			</td>
		</tr>
		<tr>
			<td>Lead Qualification </td> 
			<td><select id="bsuquali">
                        <option value="">Select</option>
			'.$lead_quali.'
			</select>
			</td>
		</tr>
		<tr>
			<td>Lead In Date </td> 
			<td><input type="text" name="bsuleadin" id="bsuleadin" /></td>
		</tr>
		<tr>
			<td>Start Contacting </td> 
			<td><input type="text" name="startcont" id="startcont" /></td>
		</tr>
		<tr>
			<td align="right" colspan="2">
				<a id="save-event-edit" class="btn btn-blue">Save</a>  
				<a id="cancel-event-edit" class="btn btn-default" style="margin-left:5px;">Cancel</a>
			</td>
		</tr>
	</tbody>
	</table>		
</div> </div><br>';
	
return $html;	
}
?>
