<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "create_event.php";
$page_title = $site_name." -  Create New Event ";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

$cur_userid=get_session('LOGIN_ID');
$cur_email=get_session('LOGIN_EMAIL');
$cur_username=get_session('DISPLAY_NAME');

$cur_loguserid=get_session('LOGIN_USERID');
include "js/create_event.js";

$mlist=$cur_loguserid;

$startdate = date('m/d/Y');
$starttime = date('h:i A');
$endtime = date('h:i A',strtotime ("+1 hour"));

?>


<div class="main-content" >
<div class="container">
<br /><br />
<?php
$action=0;
?>
  <div class="col-md-16">
    <div class="box" >
      <div class="box-header">
	<span class="title">
	<?php echo "Create New Event"; ?>
	</span>
  </div>
      <div class="box-content padded" style="min-height:500px;" align="center">
	
		<table width="90%" id="edit-drop-event-dialog-table" class="responsive">
		<tbody>
			<tr>
			    <td width="20%">Event Type/Topic * </td>
			    <td width="70%">
<select id="edit-training-dialog-trainingname" multiple="multiple" tabindex="1" class="populate placeholder" placeholder="Select Event Type/Topic">
			<?php echo getEventTypeTopic($s); ?>
				</select>
			    </td>
		  	</tr>
			<tr>
			    <td valign="top">Description </td>
			    <td>
				<textarea id="edit-training-dialog-desc" tabindex="2" cols="46" rows="2" style="width: 60%; height: 70px;"></textarea>
			    </td>
		  	</tr>
<tr>
			    <td valign="top">Attachment</td>
			    <td>

		<div id="queue">Drag & Drop Files Here</div>
		<input id="file" name="file" type="file" multiple="true">
		<div id="output" style="display:none;"></div>
		<input type="hidden" name="upimgids" id="upimgids">

</div>
 </td>
		  	</tr>
		  	<tr>
			    <td>Invite Members *</td>
	  		        <td>
<select id="memberslist1" style="width:475px;" name="memberslist1" tabindex="3" multiple="multiple"><?php echo getTeamMembersWithGroup2($mlist); ?></select>
				</td>
		  	</tr>
			<tr>
		  	<td style="width: 100px;">Date * </td>
		    <td>
		    	<span class="group">
			<input type="text" id="edit-training-dialog-start-date" tabindex="4" placeholder="Date" value="<?php echo $startdate; ?>">
		      	<input type="text" id="training-time-from" tabindex="5" placeholder="Start Time" value="<?php echo $starttime; ?>" >
			<input type="text" id="training-time-to" tabindex="6" placeholder="End Time" value="<?php echo $endtime; ?>"  >
		      </span>
		    </td>
		  </tr>
			<tr>
				<td colspan="2" align="right">
					<div class="buttons">
<a id="save-training-edit" tabindex="7" class="btn btn-blue"> Save </a>
<a id="cancel-event-edit" class="btn btn-default" tabindex="8" onclick="javascript:window.location.href='user_training_details.php';">Cancel</a>
					</div>				
				</td>
			</tr>
		</tbody>
	</table>


	<br />
    </div>

   </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";

?>
