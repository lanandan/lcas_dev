<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
validate_login();
if($_REQUEST['type'] == 'editlead'){
		$lead_id = $_REQUEST['leadid'];
		$popupHtml = genPopup($lead_id);
		$event_edit = array(
			      'popupHtml' => $popupHtml,
			      
			  	);
		echo json_encode($event_edit);


}

if($_REQUEST['type'] =='editSaveEvent'){

	$name=get_session('DISPLAY_NAME');	
	$timestamp=fmt_db_date_time();

	$lead_type=$_POST['leadtype'];
	$lead_subtype=$_POST['leadsubtype'];
	$lead_dealer=$_POST['leaddealer'];
	
	$apt_status=$_POST['status'];
	$apt_result=$_POST['result'];
	
	$lead=$_POST['leadid'];
	$lead_id=implode(",",$lead);

	$aptid=$_POST['aptid'];
	$query="";
	$query1="";

	$values_for_upd_leadcard=array($lead_type,$lead_subtype,$lead_dealer);
	$n=1;
	foreach($values_for_upd_leadcard as $value)
	{
		switch($n)
		{
			case 1:
			  $column='lead_type';
			  break;
			case 2:
			  $column='lead_subtype';
			  break;
			case 3:
			  $column='lead_dealer';
			  break;
		} 

		if($value!=''){
			$query .= $column."='".$value."' , ";
		}
		$n++;
	}

	$string = rtrim($query, ' , ');
	
	foreach($lead as $l_id){    

		if($lead_type!="" || $lead_subtype!="" || $lead_dealer!="")
		{
			$sql2="update tps_lead_card set $string where leadid='$l_id' ";
			//echo $sql2."<br>";
			$result2=mysql_query($sql2)or die(mysql_error()); 
		}

		if($apt_status!="" || $apt_result!="")
		{
			$sql_upd_apt="INSERT INTO `tps_appt_status_update` (`id`, `lead_id`, `appt_status`, `appt_result`, `modifiedby`, `modifiedon`) VALUES (NULL, '$l_id', '$apt_status', '$apt_result', '$name', '$timestamp');";

			//echo $sql_upd_apt."<br>";
			$result3=mysql_query($sql_upd_apt)or die(mysql_error()); 

			if($apt_status=="Demo" && ($apt_result=="Sale" || $apt_result=="Used RB Sale"))
			{
				$sql=mysql_query("update tps_lead_card set customer_flag='1' where leadid='$l_id'")or die(mysql_error());
			}else{
				$sql=mysql_query("update tps_lead_card set customer_flag='0' where leadid='$l_id'")or die(mysql_error());
			}
		}

	}

	echo "Appointment has been Updated";

}

function genPopup($lead_id){
	$sel='';
	$sr='';
	$lstatus='';
	$modifiedon=fmt_db_date_time();
	$name=get_session('DISPLAY_NAME');	
	$contactmethod='';
		
	$ltype="";
	$lsubtype="";
	$lead_subtype="";
	$lead_type="";
	$apptstatus="";
	$apptresult="";

	$lead_type=getLeadType($ltype);

	$re=mysql_query("select fname,lname,userid from tps_users where status='1'");
	$lead_dealer="";
	while($r=mysql_fetch_array($re))
	{       
		$lead_dealer.="<option value='$r[userid]'>".ucfirst($r['fname'])." ".$r['lname']."</option>";
	}

$html= '<div class="box" style="height:auto;margin-top:0px;">
	<div class="box-header" ><span class="title">Activity Details </span></div>
	<div align="center" class="box-content padded">
		<div class="msg" id="msg" style="margin-left:auto;float:none;display:none; color: red; "></div>
		<table style="width:100%; border-spacing: 1px; border-collapse: separate;" id="edit-event-dialog-table" >
		<tbody>
		<tr>
		    <td>Lead Type</td>
		    <td><select id="bsuleadtype" name="bsuleadtype">
                        <option value="">Select</option>
			'.$lead_type.'
			</select>
		    </td>
		</tr>
		<tr>
		    <td>Lead Subtype</td>
		    <td><select id="bsuleadsubtype" name="bsuleadsubtype">
                        <option value="">Select</option>
			'.$lead_subtype.'
			</select>
		    </td>
		</tr>
		<tr>
		    <td>Lead Dealer</td>
		    <td><select id="bsuleaddealer" name="bsuleaddealer">
                        <option value="">Select</option>
			'.$lead_dealer.'
			</select>
		    </td>
		</tr>
		<tr>
			<td>Appoinment Status </td> 
			<td><select id="bsustatus">
                        <option value="">Select</option>
			'.getApptStatus($apptstatus).'
			</select></td>
		</tr>
		<tr>
			<td>Appoinment Result </td> 
			<td><select id="bsuresult">
                        <option value="">Select</option>
			'.getapptresult($apptresult).'
			</select>
			</td>
		</tr>
		<tr>
			<td align="right" colspan="2">
				<a id="save-event-edit" class="btn btn-blue">Save</a>  
				<a id="cancel-event-edit" class="btn btn-default" style="margin-left:5px;">Cancel</a>
			</td>
		</tr>
	</tbody>
	</table>		
</div> </div><br>';
	
return $html;	
}
?>
