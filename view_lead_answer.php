<style type="text/css">
 #nostyle table, caption, tbody, tfoot, thead, tr, th, td {
          margin: 0;
          padding: 0;
          border: 0;
          outline: 0;
          font-size: 100%;
          vertical-align: baseline;
          background: transparent;
        }
</style>
<?php
session_start();

ob_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

	
	$lid=$_REQUEST['lid'];
	$leadseqid=$_REQUEST['leadseqid'];

	//echo "<pre>$lid :: $leadseqid</pre>";

	$res=mysql_query("select * from tps_lead_card where id='".$lid."'") or die(mysql_error());
	
	$r=mysql_fetch_array($res);

	$subsql="";

	$lead_type=$r['lead_type'];
	$lead_subtype=$r['lead_subtype'];
	$cur_dealerid=$r['lead_dealer'];

	$subsql=getMappedScript($lead_type,$lead_subtype,$cur_dealerid);

	$sql ="SELECT id, name, no_of_questions,header,footer from tps_scripts where id=($subsql) and status='0'";

	$rs_list=mysql_query($sql) or die(mysql_error());
	$rs = mysql_fetch_array($rs_list);

	$cur_leadid=$_REQUEST['lid'];
	$cur_leadseqid=$_REQUEST['leadseqid'];
	$cur_script_id=$rs['id'];
	$cur_no_of_questions=$rs['no_of_questions'];

	$cur_scriptName=$rs['name']." (".$cur_no_of_questions.")";

	$sql1 ="SELECT q.id, q.name, q.title, q.type, q.answer_option1, q.answer_option2, q.answer_option3, q.answer_option4, q.answer_option5, q.answer_option6, q.modified, q.modifiedby, sq.id as sqid, sq.script_id, sq.question_id, sq.displayorder FROM tps_questions q JOIN tps_scripts_questions sq ON sq.question_id=q.id WHERE sq.script_id='".$rs['id']."' order by sq.displayorder ASC"; 

	$rq_list1=mysql_query($sql1) or die(mysql_error());

	$nor=mysql_num_rows($rq_list1);

	if($r['is_referred']=="1")
	{

		$ref=$r['referred_by'];

		$im=explode('_',$ref);
		if($im[0]==1){
			$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]' ")or die(mysql_error());
			$refname=mysql_fetch_array($sq);
			$refby=ucfirst($refname['lname1'])." ".$refname['fname1']." ".$refname['lname2']." ".$refname['fname2'];
		}
		if($im[0]==2){
			$sq=mysql_query("select campaign from tps_campaign where id='$im[1]'")or die(mysql_error());
			$refname=mysql_fetch_array($sq);
			$refby=$refname['campaign'];
		}
		if($im[0]==3){
			$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]'")or die(mysql_error());
			$refname=mysql_fetch_array($sq);
			$refby=ucfirst($refname['fname1'])." ".$refname['lname1'];
		}

		if($r['form_type']=="Event_Form")
		{
			$data=json_decode($r['referral_data']);
			$ws=$data[0];
			$h=$data[1];
			$petss=$data[2];
			$isss=$data[3];
			$g=$data[4];
			$pp=explode(",",$petss);
			$i=explode(",",$isss);

			echo '<table class="table table-bordered padded">
			      <tr>
				<td colspan="2"><b>Campaign Name : '.$refby.' - Event Form</b></td>
			      </tr>
			      <tr>
				  <td>Home</td>
				  <td>'.$h.'</td>
         		      </tr>
			      <tr>
				   <td>Pets</td>
				   <td>'.implode(", ",array_filter($pp)).'</td>
			      </tr>
			      <tr>
				   <td>Employment</td>
				   <td>'.$ws.'</td>
			      </tr>
			      <tr>
				   <td>Breathing Issues</td>
				   <td>'.implode(", ",array_filter($i)).'</td>
			      </tr>
			      <tr>
				   <td>My Guess</td>
				   <td>'.$g.'</td>
			     </tr></table>';

		}elseif($r['form_type']=="Box_Form"){

			$data=json_decode($r['referral_data']);
			$ws=$data[0];
			$h=$data[1];
			$petss=$data[2];
			$isss=$data[3];
			$g=$data[4];
			$pp=explode(",",$petss);
			$i=explode(",",$isss);

			echo '<table class="table table-bordered padded">
			      <tr>
				<td colspan="2"><b>Campaign Name : '.$refby.' - Box Form</b></td>
			      </tr>
			      <tr>
				  <td>Home</td>
				  <td>'.$h.'</td>
         		      </tr>
			      <tr>
				   <td>Pets</td>
				   <td>'.implode(", ",array_filter($pp)).'</td>
			      </tr>
			      <tr>
				   <td>Employment</td>
				   <td>'.$ws.'</td>
			      </tr>
			      <tr>
				   <td>Breathing Issues</td>
				   <td>'.implode(", ",array_filter($i)).'</td>
			      </tr>
			      <tr>
				   <td>My Guess</td>
				   <td>'.$g.'</td>
			     </tr></table>';
			
		}elseif($r['form_type']=="Door_Canvas_Form"){
	
			$data=json_decode($r['referral_data']);
			$poll=$data[0];
			$dus=$data[1];
			$ho=$data[2];
			$pe=$data[3];
			$isss=$data[4];
			$us=$data[5];
			$year_in_house=$data[6];
			$pp=explode(",",$pe);
			$i=explode(",",$isss);
			$u=explode(",",$us);

			echo '<table class="table table-bordered padded">
			      <tr>
				<td colspan="2"><b>Campaign Name : '.$refby.' - Door Canvas Form</b></td>
			      </tr>
			      <tr>
				  <td>Problem with</td>
				  <td>'.$poll.'</td>
         		      </tr>
			      <tr>
				   <td>Problem with</td>
				   <td>'.$dus.'</td>
			      </tr>
			      <tr>
				   <td>Years in the Home</td>
				   <td>'.$year_in_house.'</td>
			      </tr>
			      <tr>
				   <td>Home</td>
				   <td>'.$ho.'</td>
			      </tr>
			      <tr>
				   <td>Pet Type</td>
				   <td>'.implode(', ',array_filter($pp)).'</td>
			      </tr>
			      <tr>
				   <td>Currently Using</td>
				   <td>'.implode(', ',array_filter($u)).'</td>
			     </tr>
			      <tr>
				   <td>Breathing</td>
				   <td>'.implode(', ',array_filter($i)).'</td>
			     </tr>
			</table>';

		}else if($r['form_type']=="Referral_Form"){

			$data=json_decode($r['referral_data']);
			$wstatus1=$data[0];
			$wstatus2=$data[1];
			$htype=$data[2];
			$pets=$data[3];
			$childrens=$data[4];
			$relation=$data[5];
			$issues=$data[6];
			$worktype1=$data[7];
			$worktype2=$data[8];
			$timeto=$data[9];
			$t=explode("<br>",$timeto); 
			$c=explode(",",$childrens);
			$p=explode(",",$pets);
			$iss=explode(",",$issues);

			if($worktype2!='')
				$worktypes=$worktype1.' & '.$worktype2;
			else
				$worktypes=$worktype1;

			echo '<table class="table table-bordered padded">
			      <tr>
				<td colspan="2"><b>Referred By : '.$refby.' - Referral Form</b></td>
			      </tr>
			      <tr>
				  <td>Employment 1</td>
				  <td>'.$wstatus1.'</td>
         		      </tr>
			      <tr>
				   <td>Employment 2</td>
				   <td>'.$wstatus2.'</td>
			      </tr>
			      <tr>
				   <td>Home</td>
				   <td>'.$htype.'</td>
			      </tr>
			      <tr>
				   <td>Best Time To Contact</td>
				   <td>'.implode(', ',array_filter($t)).'</td>
			      </tr>
			      <tr>
				   <td>Work Type 1 & 2 </td>
				   <td>'.$worktypes.'</td>
			     </tr>
			      <tr>
				   <td>Pets</td>
				   <td>'.implode(', ',array_filter($p)).'</td>
			     </tr>
			     <tr>
				   <td>Children</td>
				   <td>'.implode(', ',array_filter($c)).'</td>
			     </tr>
			      <tr>
				   <td>Relationship</td>
				   <td>'.$relation.'</td>
			     </tr>
 			     <tr>
				   <td>Breathing Issues</td>
				   <td>'.implode(', ',array_filter($iss)).'</td>
			     </tr>
			</table>';

		}

	}

	if( $nor > 0)
	{
		echo '<table border="0" width="100%" class="table table-bordered" style="line-height:24px; text-align:justify;">';
		echo "<thead><tr><th colspan='2' align='center'><b>Survey Questions & Answers <br> [ $cur_scriptName ] </b></th></tr></thead><tbody>";

		$sqlsa="SELECT script_question_id, answeroption1 FROM `tps_survey_answers` where leadid='".$cur_leadid."' and leadseqid='".$cur_leadseqid."' and script_id='".$cur_script_id."' and no_of_questions='".$cur_no_of_questions."'";		
		$rsa=mysql_query($sqlsa) or die(mysql_error());	

		$qids=array();
		$ans=array();
		
		$c=0;
		while($ra = mysql_fetch_array($rsa))
		{
			$qids[$c]=$ra['script_question_id'];
			$ans[$c]=$ra['answeroption1'];
			$c++;
		}

		$i=1;
		while($row = mysql_fetch_array($rq_list1))
		{
			echo "<tr><td width='40%'>";
			//echo $i.')   '.$row['name'];
			echo $i.')   '.$row['title'];
			echo "</td><td width='60%' style='text-align:left;'>";
			
			for($j=0;$j<count($qids);$j++)
			{	
				if($row['id']==$qids[$j])
				{
					$cur_ans=$ans[$j];
					$answers=preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $cur_ans);
					echo "<span>".trim($answers)."</span>";
				}
			}
			
			echo "</td></tr>";
			$i++;
   		} //While close

		if(count($ans)>0)
		{
    			echo '<tr><td colspan="2">
		<div align="right"><a href="baby_survey_qa_form.php?job=edit&lid='.$cur_leadid.'&leadseqid='.$cur_leadseqid.'" title="survey" class="btn btn-blue"><span>Edit</span></a></div>
		</td></tr>';
		}

    		echo '<tbody></table>';
	
	}else {
	
echo '<h5>There are no Questions and Answers were found.  Please add Questions in Admin Panel. <br/>&nbsp;&nbsp;</h5>';
}	

?>

