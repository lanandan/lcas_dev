<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

	$page_name = "script_view.php";

	$page_title = $site_name." Questions Preview";

	$error_flag = '';
	$error_message = '';
	$script_id='';
	$row = '';

if (request_get('script_id'))
	{
		set_session('script_id' , request_get('script_id'));
	}
if (isset($_GET['ans_id']))
	{
		set_session('ans_id' , $_GET['ans_id']);
	}
if (request_get('lead_id'))
	{
		set_session('lead_id' , request_get('lead_id'));
	}

		$questionnaire= get_session('script_id');


		$sql_qry = "select name from tps_scripts where id = '".$questionnaire."' ";
		$res_qry = mysql_query($sql_qry) or die(mysql_error());
		if ( mysql_num_rows($res_qry) > 0 ) 
		{
			$row = mysql_fetch_array($res_qry);
			
			$Questionnairename=$row['name'];
				
		}
	$qn_count_query = "select count(*) as total_rows from tps_questions where script_id = '".get_session('script_id')."' ";

	
	$qn_count_result = mysql_query($qn_count_query) or die(mysql_error('Fetch from Profile Count failed'));
	$total_records = mysql_result($qn_count_result, 0);

	
	mysql_free_result($qn_count_result);
	
	if ( $total_records <= 0 ) {
		$error_flag=1;
	}
if( isset( $_POST['save'] ) ){



$var_new = unserialize(stripslashes($_POST['hdques']));


$insert_string = "<hr>". get_session('DISPLAY_NAME') ." update the answer on ".fmt_db_date_time();
if($_POST['notes']==''){
			$insert_string .= "<br />Notes :  No notes has been noted</p>"; 
		}
		else{
			$insert_string .= "<br />Notes : ".$_POST['notes']."<br/><hr>"; 
		}

$insert_string .= "<table width='100%'   border='0' cellpadding='5' cellspacing='0' align='center'  >";


for ( $i= 1 ; $i <= count($var_new); $i++) {
	$insert_string .="<tr><td>";
	switch($var_new[$i][2])
	{

	case __QUESTION_YES_NO__ :
		if($_POST['ques'.$i]==''){
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - Not answered</p>"; 
		}
		else{
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - ".$_POST['ques'.$i]."</p>"; 
		}

		break;

	case __QUESTION_MULTI_ANSWER__ :
		if($_POST['ques'.$i]==''){
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - Not answered</p>"; 
		}
		else{
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - ".$_POST['ques'.$i]."</p>"; 
		}

		

		break;
	case __QUESTION_MULTI_CHOICE__:
		if($_POST['ques'.$i]==''){
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - Not answered</p>"; 
		}
		else{
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - ".$_POST['ques'.$i]."</p>"; 
		}
	
		break;
	case __QUESTION_RATING__:
		$insert_string .="<p>".$i."  .  ".$var_new[$i][1]."</p>";  

		
	

		$ratval= explode("~", unserialize(stripslashes($_POST['hdques'.$i])));


		for($j=1;$j<= $ratval[0];$j++){

			if(isset($_POST['ques'.$i.'rat'.$j])){
				$insert_string .="<p>".$ratval[$j]." - ".$_POST['ques'.$i.'rat'.$j]."</p>"; 
				
			}
			else{
				$insert_string .="<p>".$ratval[$j]." - Not answered</p>"; 				
			}
		}

		
		break;
	case __QUESTION_SHORT_ANS__:
		if($_POST['ques'.$i]==''){
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - Not answered</p>"; 
		}
		else{
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - ".$_POST['ques'.$i]."</p>"; 
		}
	
		break;
	case __QUESTION_LONG_ANS__:
		if($_POST['ques'.$i]==''){
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - Not answered</p>"; 
		}
		else{
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - ".$_POST['ques'.$i]."</p>"; 
		}
	
		break;

	case __QUESTION_DROP_DOWN__:
		if($_POST['ques'.$i]=='select'){
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - Not answered</p>"; 
		}
		else{
			$insert_string .="<p>".$i."  .  ".$var_new[$i][1]." - ".$_POST['ques'.$i]."</p>"; 
		}
	
		break;
	default :
		echo "default case executed <br>";
		break;
	}
		$insert_string .="</td></tr>"; 

}
$insert_string .= "</table>";

$timestamp=fmt_db_date_time();

if(get_session('ans_id')==0) {

$sql_qry = " insert into tps_answers set ".
					" `user_id` = '". get_session('lead_id') ."', ".
					" `answers` = '". addslashes($insert_string) ."', ".	
					" `created` = '". $timestamp ."', ".										
					" `modified` = '". $timestamp ."'" ;

		mysql_query($sql_qry) or die(mysql_error());
		$last_id = mysql_insert_id(); 
$sql_qry = " update  tps_lead_card set ".
					" `answer_id` = '". $last_id ."', ".
					" `modifed` = '". $timestamp ."', ".	
										
					" `modifed_by` = '". get_session('LOGIN_ID') ."' Where id='". get_session('lead_id')  ."'" ;

		
			mysql_query($sql_qry) or die(mysql_error());
}
else{

	$sql_qry = "select answers from tps_answers where id=".get_session('ans_id') ;

			$result=mysql_query($sql_qry) or die(mysql_error());
			$row=mysql_fetch_array($result);
			$cont_string = $insert_string . $row['answers'];

$sql_qry = " update tps_answers set ".
					" `answers` = '". addslashes($cont_string) ."', ".	
					" `modified` = '". $timestamp ."' where id=". get_session('ans_id') ;


		mysql_query($sql_qry) or die(mysql_error());
}
	if(isset($_SESSION['ans_id']))
		{ 	unset($_SESSION['ans_id']);	}
if(isset($_SESSION['lead_id']))
		{ 	unset($_SESSION['lead_id']);	}
if(isset($_SESSION['script_id']))
		{ 	unset($_SESSION['script_id']);	}
set_session('e_flag' , 1);
		$url = "Location: leadcard.php";
		header($url);
		exit();
}	
		
	
		

	
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

                                   

?>

<div class="main-content">
  <div class="container">
    <div class="row">

      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
            <i class="icon-magic"></i>
            Script Questions 
          </h3>
          <h5>
            <span>
          
            </span>
          </h5>
        </div>

        <ul class="list-inline pull-right sparkline-box">

          <li class="sparkline-row">
            <h4 class="blue"><span>Orders</span> 847</h4>
            <div class="sparkline big" data-color="blue"><!--28,20,6,24,15,6,4,5,29,10,11,3--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="green"><span>Reviews</span> 223</h4>
            <div class="sparkline big" data-color="green"><!--28,7,26,14,26,3,8,22,9,14,9,20--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="red"><span>New visits</span> 7930</h4>
            <div class="sparkline big"><!--3,29,15,7,4,22,3,7,23,25,27,9--></div>
          </li>

        </ul>
      </div>
    </div>
  </div>

  <div class="container padded">
    <div class="row">

      <!-- Breadcrumb line -->

      <div id="breadcrumbs">
        <div class="breadcrumb-button blue">
          <span class="breadcrumb-label"><i class="icon-home"></i> Home</span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>

         <div class="breadcrumb-button">
          <span class="breadcrumb-label">
            <i class=" icon-globe"></i> User
          </span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>
           
           

        <div class="breadcrumb-button">
          <span class="breadcrumb-label">
            <i class="icon-magic"></i> Scripts
          </span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>
      </div>
    </div>
  </div>
 <div class="container">
<div class="box">
   <div class="box-header">
     <span class="title">Script Questions</span>
   </div>
   <div class="box-content" style="padding:1%">
<br /><br />
<?php //include('form_display_messages.php'); 
	 ?>
<form action="script_view.php" method="post" id="test_questionnaire" name="test_questionnaire"  >
<?php 
if ( $error_flag == '1' ) 
{
?>
<br/><br/> 
<b>There are no Questionnaire's avaialble for testing!!  <a href="leadcard.php" style="color:#65dcf1;">Click here</a> to go back to Lead Card page.
<br/><br/><br/><br/><br/><br/><br/><br/><br/>
<?php 
}

else 
{   
$qn_list_query="SELECT `id`, `script_id`, `name`, `type`, `status`, `answer_option1`, `answer_option2`, `answer_option3`, `answer_option4`, `answer_option5`, `answer_option6` FROM `tps_questions` where script_id = '".get_session('script_id')."' order by id ";
	
		$result_list=mysql_query($qn_list_query) or die(mysql_error());
$ques = array();
echo '<span>Notes : </span><input type="textarea" name="notes" value="" style="width:  50%;   overflow:  scroll;"><br /><br />';
echo '<table width="100%"   border="0" cellpadding="5" cellspacing="0" align="center"  >';

$i=1;
while($row = mysql_fetch_array($result_list)){
$ques[$i]=array();
$ques[$i][0]=$row['id'];
$ques[$i][1]=$row['name'];
$ques[$i][2]=$row['type'];

echo '<tr>';
echo '<td>'.$i.'.    '.$row['name'].'</td>';
echo '</tr>';
	if($row['type']==__QUESTION_YES_NO__ or $row['type']==__QUESTION_MULTI_CHOICE__){
		echo '<tr><td>';
		if($row['answer_option1']!=''){
		echo '<input value="'.$row['answer_option1'].'" id="ques'.$i.'1" name="ques'.$i.'"   type="radio">'.$row['answer_option1'].'<br />';}
		if($row['answer_option2']!=''){
		echo '<input value="'.$row['answer_option2'].'" id="ques'.$i.'2" name="ques'.$i.'"  type="radio">'.$row['answer_option2'];}
		echo '</td></tr>';
		echo '<tr><td>&nbsp;</td></tr>';
	}
	if($row['type']==__QUESTION_MULTI_CHOICE__ ){
		echo '<tr><td>';
		if($row['answer_option3']!=''){
		echo '<input value="'.$row['answer_option3'].'" id="ques'.$i.'3" name="ques'.$i.'"  type="radio">'.$row['answer_option3'].'<br />';}
		if($row['answer_option4']!=''){
		echo '<input value="'.$row['answer_option4'].'" id="ques'.$i.'4" name="ques'.$i.'"  type="radio">'.$row['answer_option4'].'<br />';}
		if($row['answer_option3']!=''){
		echo '<input value="'.$row['answer_option5'].'" id="ques'.$i.'5" name="ques'.$i.'"  type="radio">'.$row['answer_option5'].'<br />';}
		if($row['answer_option3']!=''){
		echo '<input value="'.$row['answer_option6'].'" id="ques'.$i.'6" name="ques'.$i.'"  type="radio">'.$row['answer_option6'].'<br />';}
		echo '</td></tr>';
		echo '<tr><td>&nbsp;</td></tr>';
	}
	if($row['type']==__QUESTION_MULTI_ANSWER__ ){
		echo '<tr><td>';
		if($row['answer_option1']!=''){
		echo '<input value="'.$row['answer_option1'].'" id="ques'.$i.'1" name="ques'.$i.'"  type="checkbox">'.$row['answer_option1'].'<br />';}
		if($row['answer_option2']!=''){
		echo'<input value="'.$row['answer_option2'].'" id="ques'.$i.'2" name="ques'.$i.'"  type="checkbox">'.$row['answer_option2'].'<br />';}
		if($row['answer_option3']!=''){
		echo '<input value="'.$row['answer_option3'].'" id="ques'.$i.'3" name="ques'.$i.'"  type="checkbox">'.$row['answer_option3'].'<br />';}
		if($row['answer_option4']!=''){
		echo'<input value="'.$row['answer_option4'].'" id="ques'.$i.'4" name="ques'.$i.'"  type="checkbox">'.$row['answer_option4'].'<br />';}
		if($row['answer_option5']!=''){
		echo '<input value="'.$row['answer_option5'].'" id="ques'.$i.'5" name="ques'.$i.'"  type="checkbox">'.$row['answer_option5'].'<br />';}
		if($row['answer_option6']!=''){
		echo'<input value="'.$row['answer_option6'].'" id="ques'.$i.'6" name="ques'.$i.'"  type="checkbox">'.$row['answer_option6'];}

		echo '</td></tr>';
		echo '<tr><td>&nbsp;</td></tr>';
	}
	if($row['type']==__QUESTION_RATING__ ){
		echo '';
		$ratval=1;$ratname='';
		if($row['answer_option1']!=''){
$ratname .= "~".$row['answer_option1'];
		echo '<tr><td>'.$row['answer_option1'].' : &nbsp;<input value="1" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat1" name="ques'.$i.'rat1" type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat1" name="ques'.$i.'rat1"  type="radio">10 &nbsp;</td></tr>';


		}
		if($row['answer_option2']!=''){
		echo '<tr><td>'.$row['answer_option2'].' : &nbsp;<input value="1" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat2" name="ques'.$i.'rat2" type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat2" name="ques'.$i.'rat2" type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">6 &nbsp; <input value="7" name="ques'.$i.'rat2" id="ques'.$i.'rat2"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat2" name="ques'.$i.'rat2"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option2'];
		$ratval+=1;
		}
		if($row['answer_option3']!=''){
		echo '<tr><td>'.$row['answer_option3'].' : &nbsp;<input value="1" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat3" name="ques'.$i.'rat3"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option3'];
		$ratval+=1;
		}
		if($row['answer_option4']!=''){
		echo '<tr><td>'.$row['answer_option4'].' : &nbsp;<input value="1" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat4" name="ques'.$i.'rat4"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option4'];
		$ratval+=1;
		}
		if($row['answer_option5']!=''){
		echo '<tr><td>'.$row['answer_option5'].' : &nbsp;<input value="1" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat5" name="ques'.$i.'rat5"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option5'];
		$ratval+=1;
		}
		if($row['answer_option6']!=''){
		echo '<tr><td>'.$row['answer_option6'].' : &nbsp;<input value="1" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">1 &nbsp; <input value="2" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">2 &nbsp; <input value="3" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">3 &nbsp; <input value="4" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">4 &nbsp; <input value="5" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">5 &nbsp; <input value="6" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">6 &nbsp; <input value="7" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">7 &nbsp; <input value="8" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">8 &nbsp; <input value="9" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">9 &nbsp; <input value="10" id="ques'.$i.'rat6" name="ques'.$i.'rat6"  type="radio">10 &nbsp;</td></tr>';
$ratname .= "~".$row['answer_option6'];
		$ratval+=1;
		}
		$ratfinal=$ratval.$ratname;
		
		echo "<input type='hidden'  name='hdques".$i."' id='ques".$i."' value='".serialize($ratfinal)."'>";
		echo '';
		echo '<tr><td>&nbsp;</td></tr>';
	}
	if($row['type']==__QUESTION_SHORT_ANS__ ){
		echo '<tr>';
		echo '<td><input value="" type="text" name="ques'.$i.'" ></td>';
		echo '</tr>';
		echo '<tr><td>&nbsp;</td></tr>';
	}
	if($row['type']==__QUESTION_LONG_ANS__ ){
		echo '<tr>';
		echo '<td><input value="" type="textarea" name="ques'.$i.'"></td>';
		echo '</tr>';
		echo '<tr><td>&nbsp;</td></tr>';
	}
	if($row['type']==__QUESTION_DROP_DOWN__ ){
		echo '<tr>';
		echo '<td><select name="ques'.$i.'" id="ques'.$i.'" >';
		echo '<option selected="selected" value="select">-- select --</option>';
		if($row['answer_option1']!=''){
		echo '<option  value="'.$row['answer_option1'].'">'.$row['answer_option1'].'</option>'; }
		if($row['answer_option2']!=''){
		echo '<option  value="'.$row['answer_option2'].'">'.$row['answer_option2'].'</option>'; }
		if($row['answer_option3']!=''){
		echo '<option  value="'.$row['answer_option3'].'">'.$row['answer_option3'].'</option>'; }
		if($row['answer_option4']!=''){
		echo '<option  value="'.$row['answer_option4'].'">'.$row['answer_option4'].'</option>'; }
		if($row['answer_option5']!=''){
		echo '<option  value="'.$row['answer_option5'].'">'.$row['answer_option5'].'</option>'; }
		if($row['answer_option6']!=''){
		echo '<option  value="'.$row['answer_option6'].'">'.$row['answer_option6'].'</option>'; }
		echo '</td>';
		echo '</tr>';
		echo '<tr><td>&nbsp;</td></tr>';
	}

$i++;
 } //While close


echo " <input type='hidden' name='hdques' id='hdques' value='".serialize($ques)."' >";
echo '</table>';
echo '<input type="submit" class=" btn btn-blue" name="save"   value="Save" >&nbsp;&nbsp;';
echo '<input type="button" class="btn btn-blue" onclick="javascript:window.location=lead_card.php" value="Cancel">';
} //Else close
?>

</form> 

<br /><br />
   </div>
 </div> 
 </div>

<?php

include "lcas_footer.php";
?>
