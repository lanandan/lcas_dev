<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

if(isset($_REQUEST['type'])=="BulkDelete")
{
	if($_REQUEST['page']=="leadcard")
	{
		$leadidArr=$_REQUEST['leadids'];
		$lidsArr=$_REQUEST['lids'];
		$leadid=implode(",",$leadidArr);
		$lid=implode(",",$lidsArr);

		$sql2="update tps_events set delete_flag='1' where find_in_set(lead_id,'$lid')";			
		$result2=mysql_query($sql2) or die(mysql_error());

		$sql3="update tps_lead_card set delete_flag='1' where find_in_set(id,'$lid')";
		$result3=mysql_query($sql3) or die(mysql_error());

		$cnt=count($lidsArr);

		$user_dispname=get_session('DISPLAY_NAME');
		$user_logid=get_session('LOGIN_ID');
		$url= $_SERVER['HTTP_REFERER'];
		$log_desc= ucfirst($user_dispname)." deleted $cnt Leads from Lead Listing. <b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Leads Bulk Deleted", $user_logid, $log_desc);

		$message="$cnt Leads has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);
	
	}

	if($_REQUEST['page']=="appointments")
	{

		$aptidArr=$_REQUEST['aptid'];
		$aptid=implode(",",$aptidArr);

		$sql2="update tps_events set delete_flag='1' where find_in_set(id,'$aptid')";			
		$result2=mysql_query($sql2) or die(mysql_error());

		$cnt=count($aptidArr);

		$user_dispname=get_session('DISPLAY_NAME');
		$user_logid=get_session('LOGIN_ID');
		$url= $_SERVER['HTTP_REFERER'];
	$log_desc=ucfirst($user_dispname)." deleted $cnt Appointments from Appointment Listing. <b><a href=$url target=_blank>$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Appointments Bulk Deleted", $user_logid, $log_desc);

		$message="$cnt Appointments has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);

	}

	if($_REQUEST['page']=="customer")
	{

		$cidsArr=$_REQUEST['cids'];
		$cid=implode(",",$cidsArr);

		$sql3="update tps_lead_card set delete_flag='1' where find_in_set(id,'$cid')";
		$result3=mysql_query($sql3) or die(mysql_error());

		$cnt=count($cidsArr);

		$user_dispname=get_session('DISPLAY_NAME');
		$user_logid=get_session('LOGIN_ID');
		$url= $_SERVER['HTTP_REFERER'];
		$log_desc= ucfirst($user_dispname)." deleted $cnt Customer from Customer Listing. <b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Customers Bulk Deleted", $user_logid, $log_desc);

		$message="$cnt Customers has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);

	}
}


?>
