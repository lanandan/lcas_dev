<?php
session_start();

ob_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "survey_followup_qa_form.php";
$page_title = $site_name." - Survey Follow Up Q&A Form";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<script type="text/javascript" src="javascripts/bootbox.min.js"></script>
<script type="text/javascript">

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 6000);
});

function validate(){
	var callgift=document.getElementById("callforgift").checked;
	var callgift1=document.getElementById("callforgift1").checked;
	
	if(callgift==false && callgift1==false)
	{
		bootbox.alert('Please Check the Required Feilds!');
		return false;
	}else if(callgift==true || callgift1==true)
	{	
		bootbox.confirm("Please Confirm whether all the questions are answered?", function(res) {
		
		if(res==true)
			return true;
		else 
			return false;
		}); 
		
	}
}

 function loadAjaxContent(x,y)
    { 

var data = {
    			type: 'editlead',
			id:x,
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "edit_leadstatus.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:800,
							height:550,
							openjs:function(){
							

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	



								

							$('a#save-event-edit').click(function(){
								
							var lead_status = $('#status').val();
							var activity = $('#activity').val();
							var contactreason = $('#contactreason').val();
							var comments = $('#comments').val();
					if(activity==''||contactreason==''||lead_status==''){
						$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose All The Options ');
							return false ;
						}
							var data = {
							    	type: 'editSaveEvent',
								id: x,
								leadid:y,
								status: lead_status,
								activity:activity,
								contactreason:contactreason,
								comments:comments,
						
							 }


								$.ajax({
								    		type: "POST",
								    		url: "edit_leadstatus.php",
								    		data: data,
								    		success: function(resp) {
										bootbox.alert(resp);
									        window.parent.TINY.box.hide();
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
}


</script>
<style type="text/css">
 #nostyle table, caption, tbody, tfoot, thead, tr, th, td {
          margin: 0;
          padding: 0;
          border: 0;
          outline: 0;
          font-size: 100%;
          vertical-align: baseline;
          background: transparent;
        }
</style>
<div class="main-content" >
<div class="container">
<br /><br />

  <div class="col-md-16">
    <div class="box">
      <div class="box-header">
<span class="title">Follow Up Question & Answer Form </span>
<a class="btn btn-xs btn-blue" href="leadcard.php" style="margin-top:7px;"><span>Back</span></a></div>
      <div class="box-content" align="center" style="min-height: 500px;">
	<?php 
	
	$lid=$_REQUEST['lid'];
	$leadseqid=$_REQUEST['leadseqid'];

	$lseqid=$_REQUEST['leadseqid'];

	$res=mysql_query("select * from tps_lead_card where id='".$lid."'") or die(mysql_error());
	
	$r=mysql_fetch_array($res);

	$res_users=mysql_query("select office_name,office_city from tps_users where userid='".get_session('LOGIN_USERID')."'") or die(mysql_error());
	$ru=mysql_fetch_array($res_users);

	?>
<form name="form" id="form" method="post" onsubmit="javascript: return validate();">
<table border="0" width="98%" class="table table-normal" style="line-height:24px; text-align:justify;">
  <tbody>
<tr><td>
<?php
	$subsql="";

	$lead_type=$r['lead_type'];
	$lead_subtype=$r['lead_subtype'];
	$cur_dealerid=$r['lead_dealer'];

	$subsql=getMappedScript($lead_type,$lead_subtype,$cur_dealerid);

	$sql ="SELECT id, name, no_of_questions,header,footer,followupheader,followupfooter from tps_scripts where id=($subsql) and status='0'";

	$rs_list=mysql_query($sql) or die(mysql_error());
	$rs = mysql_fetch_array($rs_list);


	$res_survey=mysql_query("select * from tps_survey_details where leadid ='".$lid."' and lead_seqid='$leadseqid'") or die(mysql_error());
	$rsy=mysql_fetch_array($res_survey);
?>

Survey Date : <?php echo date('M d Y h:i A',strtotime($rsy['survey_datetime'])); ?> &nbsp; &nbsp; &nbsp;
Surveyed By : <?php echo $rsy['survey_by']; ?> &nbsp; &nbsp; &nbsp;

 <a href="add_leads.php?action=edit&lid=<?php echo $_REQUEST['lid'];?>&leadseqid=<?php echo $_REQUEST['leadseqid'];?>&backto=sfu" target="_blank" class="btn btn-xs btn-blue"><span>Calender</span></a>
&nbsp; &nbsp; &nbsp;<a href="add_leads.php?action=edit&lid=<?php echo $_REQUEST['lid'];?>&leadseqid=<?php echo $_REQUEST['leadseqid'];?>&backto=sfu" target="_blank" class="btn btn-xs btn-blue"><span>Edit Lead</span></a>
&nbsp; &nbsp; &nbsp;
<a href="#" class="btn btn-xs btn-blue" onclick="loadAjaxContent('<?php echo $_REQUEST['lid'];?>','<?php echo $_REQUEST['leadseqid'];?>');"><span>Activity</span></a>


</td></tr>
     <tr>
	<td>
		<!--Hi, is <b><?php echo ucfirst($r['fname1']); ?></b> there? this is  <b><?php echo get_session('DISPLAY_NAME'); ?></b> from <b><?php echo $ru['office_name']; ?></b>. We contacted you back on <survey date> and entered you into a drawing for <b><?php echo $r['gift']; ?></b>. Do you remember that? That drawing will be held on <drawing date>. Now when I called, I also told you we were offering daily gifts for helping us to advertise.-->

<?php  
$refby="";
if($r['referred_by']!='')
{
	$ref=$r['referred_by'];
	$im=explode('_',$ref);
	if($im[0]==1){
		$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]' ")or die(mysql_error());
		$refname=mysql_fetch_array($sq);
		$refby=ucfirst($refname['lname1'])." ".$refname['fname1']." ".$refname['lname2']." ".$refname['fname2'];
	}
	if($im[0]==2){
		$sq=mysql_query("select campaign from tps_campaign where id='$im[1]'")or die(mysql_error());
		$refname=mysql_fetch_array($sq);
		$refby=$refname['campaign'];
	}
	if($im[0]==3){
		$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]'")or die(mysql_error());
		$refname=mysql_fetch_array($sq);
		$refby=ucfirst($refname['fname1'])." ".$refname['lname1'];
	}
}

$address=$r['add_line1'].' ,'.$r['add_line2'].' '.$r['city'].' '.$r['state'].' '.$r['zip'];

$sq="select * from tps_events where lead_id='$r[id]' and appttype='Demo' and delete_flag='0'";
$res=mysql_query($sq) or die(mysql_error());
$appt1 = mysql_fetch_array($res); 

$sql_select="select * from tps_users where userid='$r[lead_dealer]'";
$result_select = mysql_query($sql_select) or die(mysql_error());
$row1 = mysql_fetch_array($result_select);
 
if($appt1['start']!='' && $appt1['start']!='' && $appt1['end']!='')
{
	$startDate = date('M d Y',strtotime($appt1['start']));
	$startTime = date('h:i A',strtotime($appt1['start']));
	$endTime = date('h:i A',strtotime($appt1['end']));
	$apt_date_time=$startDate.'    '.$startTime.'-'.$endTime;
}else{
	$apt_date_time='';
}

$dealer=ucfirst($row1['fname'])." ".ucfirst($row1['lname']);
$login_name=get_session('DISPLAY_NAME'); 

if($r['fname1']!=''){
	$header=str_replace("%first name1%","<b>".$r['fname1']."</b>",$rs['followupheader']); 
}else{
	$header=str_replace("%first name1%","<b><a>%first name1%</a></b>",$rs['followupheader']); 
}

if($login_name!=''){
	$header1=str_replace("%logged in user%","<b>".$login_name."</b>",$header);
}else{
	$header1=str_replace("%logged in user%","<a>%logged in user%</a>",$header);
}

if($refby!=''){
	$header2=str_replace("%referred by%","<b>".$refby."</b>",$header1);
}else{
	$header2=str_replace("%referred by%","<b><a>%referred by%</a></b>",$header1);
}

if($r['lname1']!==''){
	$header3=str_replace("%last name1%","<b>".$r['lname1']."</b>",$header2);
}else{
	$header3=str_replace("%last name1%","<b><a>%last name%</a></b>",$header2);
}

$header4=str_replace("%office name%","<b>LCAS</b>",$header3);

if($dealer!=''){
	$header5=str_replace("%dealer%" ,"<b>".$dealer."</b>",$header4);
}else{
	$header5=str_replace("%dealer%" ,"<b><a>%dealer%</a></b>",$header4);
}

if($r['fname2']!=''){
	$header6=str_replace("%first name2%","<b>".$r['fname2']."</b>",$header5);
}else{
	$header6=str_replace("%first name2%","<b><a>%first name2%</a></b>",$header5);
}

if($r['lname2']!=''){
	$header7=str_replace("%last name2%","<b>".$r['lname2']."</b>",$header6);
}else{
	$header7=str_replace("%last name2%","<a>%last name2%</a>",$header6);
}

if($r['add_line1']!==''){
	$header8=str_replace("%addr1%","<b>".$r['add_line1']."</b>",$header7);
}else{
	$header8=str_replace("%addr1%","<b><a>%addr1%</a></b>",$header7);
}

if($r['add_line2']!=''){
	$header9=str_replace("%addr2%","<b>".$r['add_line2']."</b>",$header8);
}else{
	$header9=str_replace("%addr2%","<b><a>%addr2%</a></b>",$header8);
}

if($address!=''){
	$header10=str_replace("%address%","<b>".$address."</b>",$header9);
}else{
	$header10=str_replace("%address%","<b><a>%address%</a></b>",$header9);
}

if($apt_date_time!=''){
	$header11=str_replace("%appt%","<b>".$apt_date_time."</b>",$header10);
}else{
	$header11=str_replace("%appt%","<b><a>%appt%</a></b>",$header10);
}

if($r['time_contact']!=''){
	$header12=str_replace("%best time to contact%","<b>".$r['time_contact']."</b>",$header11);
}else{
	$header12=str_replace("%best time to contact%","<b><a>%best time to contact%</a></b>",$header11);
}

if($r['lead_type']!=''){
	$header13=str_replace("%lead type%","<b>".$r['lead_type']."</b>",$header12);
}else{
	$header13=str_replace("%lead type%","<b><a>%lead type%</a></b>",$header12);
}

if($r['lead_subtype']!=''){
	$header14=str_replace("%lead subtype%","<b>".$r['lead_subtype']."</b>",$header13);
}else{
	$header14=str_replace("%lead subtype%","<b><a>%lead subtype%</a></b>",$header13);
}

if($r['referred_by']!='')
{
	$ref=explode("_",$r['referred_by']);
	if($ref[0]==2)
	{
$sq=mysql_query("select drawing_prize,campaign_prize,gift1,gift2,gift3,gift4,gift5,gift6 from tps_campaign where id='$ref[1]'")or die(mysql_error());
		$re=mysql_fetch_array($sq);
		$gift=array($re['gift1'],$re['gift2'],$re['gift3'],$re['gift4'],$re['gift5'],$re['gift6']);
		$gift=array_filter($gift);
		$gift=implode(",",$gift);

		if($gift!='')
		{
			$header15=str_replace("%gift%","<b>".$gift."</b>",$header14);
		}else{
			$header15=str_replace("%gift%","<b><a>%gift%</a></b>",$header14);
		}
		
		if($re['drawing_prize']!="")
		{
			$header15=str_replace("%drawing prize%","<b>".$re['drawing_prize']."</b>",$header15);
		}else{
			$header15=str_replace("%drawing prize%","<b><a>%drawing prize%</a></b>",$header15);
		}
		
		if($re['campaign_prize']!="")
		{
			$header15=str_replace("%campaign prize%","<b>".$re['campaign_prize']."</b>",$header15);
		}else{
			$header15=str_replace("%campaign prize%","<b><a>%campaign prize%</a></b>",$header15);
		}

	}else{
	
		if($r['gift']!=''){
			$header15=str_replace("%gift%","<b>".$r['gift']."</b>",$header14);
		}else{
			$header15=str_replace("%gift%","<b><a>%gift%</a></b>",$header14);
		}
	}

}else{

	if($r['gift']!=''){
		$header15=str_replace("%gift%","<b>".$r['gift']."</b>",$header14);
	}else{
		$header15=str_replace("%gift%","</b><a>%gift%</a></b>",$header14);
	}

}

echo trim(stripslashes($header15));

?>
	</td>
    </tr>
   <!--  <tr>
	<td>
<?php
$tsdres=mysql_query("SELECT * FROM `tps_survey_details` where leadid='".$_REQUEST['lid']."' and lead_seqid='".$_REQUEST['leadseqid']."' ") or die(mysql_error());

		$tsdr=mysql_fetch_array($tsdres);
		$selgift=$tsdr['gift_card_selected'];
?>
Well your name was selected to receive a 
<?php 
$sql_qry ="select gift_name from tps_lead_gift where status='0' and lead_type_name='$r[lead_type]' or lead_subtype_name='$r[lead_subtype]' GROUP BY id";
$result_list = mysql_query($sql_qry) or die(mysql_error());
while($resu=mysql_fetch_array($result_list)) {
?>
<input type="radio" name="giftcard" id="giftcard1" value="<?php echo $resu['gift_name'];?>" <?php if(trim($selgift)==trim($resu['gift_name'])) echo "checked"; ?>><?php echo $resu['gift_name']; }?>  Which one of those would you and your family like best?

	</td>
   </tr>
	<tr>
		<td>
<b><?php echo $r['fname1']; ?></b> what we'd like to do, is stop by at a convenient time for you and your spouse, deliver your <span id="gc">s<span> and introduce you to our store and get your opinions of our products. You get the gift just for giving us your opinions and there is no obligation to buy anything. When is the best time to get both you and your spouse home together <?php echo $r['time_contact']; ?><best time to contact>. We have an opening for

		</td>
	</tr>-->
    <tr>
	<td>
	
<?php

	$cur_leadid=$_REQUEST['lid'];
	$cur_leadseqid=$_REQUEST['leadseqid'];
	$cur_script_id=$rs['id'];
	$cur_no_of_questions=$rs['no_of_questions'];

	$sndisp=$rs['name']." (".$cur_no_of_questions.") ";

	$sql1 ="SELECT q.id, q.name, q.type, q.answer_option1, q.answer_option2, q.answer_option3, q.answer_option4, q.answer_option5, q.answer_option6, q.modified, q.modifiedby, sq.id as sqid, sq.script_id, sq.question_id, sq.displayorder FROM tps_questions q JOIN tps_scripts_questions sq ON sq.question_id=q.id WHERE sq.script_id='".$rs['id']."' order by sq.displayorder ASC"; 

	$rq_list1=mysql_query($sql1) or die(mysql_error());

	$nor=mysql_num_rows($rq_list1);

	if( $nor > 0)
	{
		echo '<table width="80%" border="0" cellpadding="5" cellspacing="5" align="center" border="1">';
		echo "<thead><tr><td colspan='2'><b>Survey Q & A [ $sndisp ] </b></td></tr></thead><tbody>";

		$sqlsa="SELECT script_question_id, answeroption1 FROM `tps_survey_answers` where leadid='".$cur_leadid."' and leadseqid='".$cur_leadseqid."' and script_id='".$cur_script_id."' and no_of_questions='".$cur_no_of_questions."'";		
		$rsa=mysql_query($sqlsa) or die(mysql_error());
	
		$qids=array();
		$ans=array();
		
		$c=0;
		while($ra = mysql_fetch_array($rsa))
		{
			$qids[$c]=$ra['script_question_id'];
			$ans[$c]=$ra['answeroption1'];
			$c++;
		}

		$i=1;
		while($row = mysql_fetch_array($rq_list1))
		{
			echo "<tr><td width='50%'>";
			echo $i.')   '.$row['name'];
			echo "</td><td width='50%'>";
			
			for($j=0;$j<count($qids);$j++)
			{	
				if($row['id']==$qids[$j])
				{
					$cur_ans=$ans[$j];
					$answers=preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $cur_ans);
					echo "<span>".trim($answers)."</span>";
				}
			}
			
			echo "</td></tr>";
			$i++;
   		} //While close
    		echo '<tr><td colspan="2">
		<div align="right"><a href="baby_survey_qa_form.php?job=edit&lid='.$cur_leadid.'&leadseqid='.$cur_leadseqid.'" title="survey" class="btn btn-blue"><span>Edit</span></a></div>
		</td></tr>';
    		echo '<tbody></table>';
	
	}else {
	
echo '<h5>There are no Questions and Answers were found.  Please add Questions in Admin Panel. <br/>&nbsp;&nbsp;</h5>';
}	

?>

	</td>
    </tr>
    <tr>
	<td>
		<!--Well <b><?php echo $r['fname1']; ?></b> that takes care of our survey. Now we are going to enter your name in our monthly drawing. We are also giving away daily gifts up to the day of the drawing? ..so don't be surprised if you are selected to receive one of those! 

-->
<?php 
$lid=$_REQUEST['lid'];

$res=mysql_query("select * from tps_lead_card where id='".$lid."'") or die(mysql_error());
	
$r=mysql_fetch_array($res);

$address=$r['add_line1'].' ,'.$r['add_line2'].' '.$r['city'].' '.$r['state'].' '.$r['zip'];
$sq="select * from tps_events where lead_id='$r[id]' and appttype='Demo' and delete_flag='0'";
$res=mysql_query($sq) or die(mysql_error());
$appt1 = mysql_fetch_array($res); 

if($appt1['start']!='' && $appt1['start']!='' && $appt1['end']!=''){
	$startDate = date('M d Y',strtotime($appt1['start']));
	$startTime = date('h:i A',strtotime($appt1['start']));
	$endTime = date('h:i A',strtotime($appt1['end']));
	$apt_date_time=$startDate.'    '.$startTime.'-'.$endTime;
}
else{
	$apt_date_time='';
}

$sql_select="select * from tps_users where userid='$r[lead_dealer]'";
$result_select = mysql_query($sql_select) or die(mysql_error());
$row1 = mysql_fetch_array($result_select); 
$dealer="<b>".ucfirst($row1['fname'])." ".ucfirst($row1['lname'])."</b>";
$login_name="<b>".get_session('DISPLAY_NAME')."</b>"; 

$res=mysql_query("select * from tps_lead_card where id='".$lid."'") or die(mysql_error());
	
$r=mysql_fetch_array($res);

$res_users=mysql_query("select office_name,office_city from tps_users where userid='".get_session('LOGIN_USERID')."'") or die(mysql_error());
$ru=mysql_fetch_array($res_users);

if($r['fname1']!=''){
	$replacefname="<b>".$r['fname1']."</b>";
	$footer=str_replace("%first name1%",$replacefname,$rs['followupfooter']); 
}else{
	$footer=str_replace("%first name1%","<b><a>%first name1%</a></b>",$rs['followupfooter']); 
}

if($login_name!=''){
	$footer1=str_replace("%logged in user%",$login_name,$footer);
}else{
	$footer1=str_replace("%logged in user%","<b><a>%logged in user%</a></b>",$footer);
}

if($refby!=''){
	$refby1="<b>".$refby."</b>";
	$footer2=str_replace("%referred by%",$refby1,$footer1);
}else{
	$footer2=str_replace("%referred by%","<b><a>%referred by%</a></b>",$footer1);
}

if($r['lname1']!==''){
	$replacelname="<b>".$r['lname1']."</b>";
	$footer3=str_replace("%last name1%",$replacelname,$footer2);
}else{
	$footer3=str_replace("%last name1%","<b><a>%last name%</a></b>",$footer2);
}

$footer4=str_replace("%office name%","LCAS",$footer3);

if($dealer!=''){
	$footer5=str_replace("%dealer%" ,$dealer,$footer4);
}else{
	$footer5=str_replace("%dealer%" ,"<b><a>%dealer%</a></b>",$footer4);
}

if($r['fname2']!=''){
	$replacefname="<b>".$r['fname2']."</b>";
	$footer6=str_replace("%first name2%",$replacefname,$footer5);
}else{
	$footer6=str_replace("%first name2%","<b><a>%first name2%</a></b>",$footer5);
}

if($r['lname2']!=''){
	$replacelname="<b>".$r['lname2']."</b>";
	$footer7=str_replace("%last name2%",$replacelname,$footer6);
}else{
	$footer7=str_replace("%last name2%","<b><a>%last name2%</a></b>",$footer6);
}

if($r['add_line1']!==''){
	$rep_add1="<b>".$r['add_line1']."</b>";
	$footer8=str_replace("%addr1%",$r['add_line1'],$footer7);
}else{
	$footer8=str_replace("%addr1%","<b><a>%addr1%</a></b>",$footer7);
}

if($r['add_line2']!=''){
	$rep_add2="<b>".$r['add_line2']."</b>";
	$footer9=str_replace("%addr2%",$rep_add2,$footer8);
}else{
	$footer9=str_replace("%addr2%","<b><a>%addr2%</a></b>",$footer8);
}

if($address!=''){
	$address1="<b>".$address."</b>";
	$footer10=str_replace("%address%",$address1,$footer9);
}else{
	$footer10=str_replace("%address%","<b><a>%address%</a></b>",$footer9);
}

if($apt_date_time!=''){
	$apt_date_time1="<b>".$apt_date_time."</b>";
	$footer11=str_replace("%appt%",$apt_date_time1,$footer10);
}else{
	$footer11=str_replace("%appt%","<b><a>%appt%</a></b>",$footer10);
}

if($r['time_contact']!=''){
	$rep_time_contact="<b>".$r['time_contact']."</b>";
	$footer12=str_replace("%best time to contact%",$rep_time_contact,$footer11);
}else{
	$footer12=str_replace("%best time to contact%","<b><a>%best time to contact%</a></b>",$footer11);
}

if($r['lead_type']!=''){
	$rep_leadtype="<b>".$r['lead_type']."</b>";
	$footer13=str_replace("%lead type%",$rep_leadtype,$footer12);
}else{
	$footer13=str_replace("%lead type%","<b><a>%lead type%</a></b>",$footer12);
}

if($r['lead_subtype']!=''){
	$rep_leadsubtype="<b>".$r['lead_subtype']."</b>";
	$footer14=str_replace("%lead subtype%",$rep_leadsubtype,$footer13);
}else{
	$footer14=str_replace("%lead subtype%","<b><a>%lead subtype%</a></b>",$footer13);
}

if($r['referred_by']!=''){

	$ref=explode("_",$r['referred_by']);
	if($ref[0]==2){
$sq=mysql_query("select drawing_prize,campaign_prize,gift1,gift2,gift3,gift4,gift5,gift6 from tps_campaign where id='$ref[1]'")or die(mysql_error());
		$re=mysql_fetch_array($sq);
		$gift=array($re['gift1'],$re['gift2'],$re['gift3'],$re['gift4'],$re['gift5'],$re['gift6']);
		$gift=array_filter($gift);
		$gift=implode(",",$gift);
		$gift="<b>".$gift."</b>";

		if($gift!=''){
			$footer15=str_replace("%gift%",$gift,$footer14);
		}else{
			$footer15=str_replace("%gift%","<b><a>%gift%</a></b>",$footer14);
		}

		if($re['drawing_prize']!="")
		{
			$footer15=str_replace("%drawing prize%","<b>".$re['drawing_prize']."</b>",$footer15);
		}else{
			$footer15=str_replace("%drawing prize%","<b><a>%drawing prize%</a></b>",$footer15);
		}
		
		if($re['campaign_prize']!="")
		{
			$footer15=str_replace("%campaign prize%","<b>".$re['campaign_prize']."</b>",$footer15);
		}else{
			$footer15=str_replace("%campaign prize%","<b><a>%campaign prize%</a></b>",$footer15);
		}

	}else{

		if($r['gift']!=''){
			$rep_gift="<b>".$r['gift']."</b>";
			$footer15=str_replace("%gift%",$rep_gift,$footer14);
		}else{
			$footer15=str_replace("%gift%","<b><a>%gift%</a></b>",$footer14);
		}
	}

}else{

	if($r['gift']!=''){
		$rep_gift="<b>".$r['gift']."</b>";
		$footer15=str_replace("%gift%",$rep_gift,$footer14);
	}else{
		$footer15=str_replace("%gift%","<b><a>%gift%</a></b>",$footer14);
	}
}

echo trim(stripslashes($footer15));

?>

	</td>
    </tr>
    <tr>
	<td align="right">
		
<a href="add_leads.php?action=edit&lid=<?php echo $_REQUEST['lid'];?>&leadseqid=<?php echo $_REQUEST['leadseqid'];?>&backto=sfu" target="_blank" class="btn btn-xs btn-blue"><span>Calender</span></a>
&nbsp; &nbsp; &nbsp;<a href="add_leads.php?action=edit&lid=<?php echo $_REQUEST['lid'];?>&leadseqid=<?php echo $_REQUEST['leadseqid'];?>&backto=sfu" target="_blank" class="btn btn-xs btn-blue"><span>Edit Lead</span></a>
&nbsp; &nbsp; &nbsp;
<a href="#" class="btn btn-xs btn-blue" onclick="loadAjaxContent('<?php echo $_REQUEST['lid'];?>','<?php echo $_REQUEST['leadseqid'];?>');"><span>Activity</span></a>
&nbsp; &nbsp; &nbsp;
		<input type="submit" name="save" value="Save" id="save" class="btn btn-green" />
		<input type="reset" name="cancel" value="Cancel" onclick="javascript:window.location='leadcard.php'" class="btn btn-default" />
	</td>
    </tr>
</tbody>
</table>
</form>

      </div>
    </div>

<br /><br /><br />

<?php

$lead_status="";
$apptnewstart="";

if(isset($_REQUEST['save']))
{
	$cur_leadid=$_REQUEST['lid'];
	$cur_leadseqid=$_REQUEST['leadseqid'];
	$giftcard=$_REQUEST['giftcard'];

	$sql_up="UPDATE `tps_survey_details` SET  `gift_card_selected`='".$giftcard."',  `followup_datetime`='".date('Y-m-d H:i:s')."',  `followup_by`='".get_session('DISPLAY_NAME')."',  `modifiedby`='".get_session('DISPLAY_NAME')."',  `modifiedtime`='".date('Y-m-d H:i:s')."' WHERE leadid='".$cur_leadid."' and lead_seqid='".$cur_leadseqid."'";

	mysql_query($sql_up) or die(mysql_error());

	$user_logdispname=get_session('DISPLAY_NAME');
	$user_logid=get_session('LOGIN_ID');
	$url= $_SERVER['HTTP_REFERER'];

$log_desc= ucfirst($user_logdispname)." follow-up surveyed $survey_leadname at ".date('M d Y h:i:s A').". <b><a href=$url target=_blank >$url</a></b>";

	tps_log_error(__INFO__, __FILE__, __LINE__, "Follow-up Surveyed $survey_leadname ", $user_logid, $log_desc);

	header("location:leadcard.php");
	exit();
	
}

?>	
<br> <br>	
   </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";

?>
