
<script>
$(document).ready(function() {
(function($) {
var oTable = $('table').dataTable();
  var nNodes = oTable.fnGetNodes( );
    $.fn.multiFilter = function(filters) {
        var $table = $(this);
        return $table.find('tbody > tr').each(function() {
            var tr = $(this);
            
            // Make it an array to avoid special cases later.
            if(!$.isArray(filters))
                filters = [ filters ];
            howMany = 0;
            for(i = 0, f = filters[0]; i < filters.length; f = filters[++i]) {
                var index = 0;
                $table.find('thead > tr > th').each(function(i) {
                    if($(this).text() == f.column) {
                        index = i;
                        return false;
                    }
                });
                var text = tr.find('td:eq(' + index + ')').text();
                if(text.toLowerCase().indexOf(f.word.toLowerCase()) != -1)
                    ++howMany;
            }
            if(howMany == filters.length)
                tr.show();
            else
                tr.hide();
        });
    };
})(jQuery);


$('.user').change(function() {
    var t = $('table');
    $('table').multiFilter([
        { column: 'USER ID',  word: this.value }
    ]);
});
$('.subtype').change(function() {
var j=this.selectedIndex;
$('table tr').each(function(){
$(this).find('td').show();
});

$('table  th').show();
if(j!='0'){
$('table tr').each(function(){
$(this).find('th:not(th:eq(0),th:eq('+j+'))').hide();
});
j=j-1;
$('table tr').each(function(){
$(this).find('td:not(td:eq('+j+'),td:eq(0))').hide();
//$(this).find('td:eq('+i+')').css({'display':'block'}); 
});
}
});
$('.panel-heading').click(function(){
$('table tr').each(function(){
$(this).find('td').show();
});
$('table  th').show();
 $('table').multiFilter([
        { column: 'USER ID',  word: '' }
    ]);
$(".subtype").find('option').removeAttr("selected");
$(".user").find('option').removeAttr("selected");
});
$('.list').click(function(){
$('table tr').each(function(){
$(this).find('td').show();
});
$('table  th').show();
$(".subtype").find('option').removeAttr("selected");
$(".user").find('option').removeAttr("selected");
  $('table').multiFilter([
        { column: 'USER ID',  word: '' }
    ]);
});



});
</script>
