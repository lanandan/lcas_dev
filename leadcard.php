<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
$ref='Select';
$status='';
$sta='';
$lstatus='';
$lres='';
$sel_zip='';
$query='';
$team='';
$sel='';
$lqual='';
$refby='';
$sel_status='';
$q='';
$filtercity='';
$status1='';
validate_login();
$page_name = "leadcard.php";
$page_title = $site_name." View Leads";
$Id=get_session('LOGIN_ID');
if( request_get('action')=='leadstatus_delete') {
$id=request_get('lid');
$sql="update tps_lead_status_update set delete_flag='1' where id='$id'";
$result=mysql_query($sql);
$message="Lead Status has been Deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);
header("location:leadcard.php");
exit;
}
if( request_get('action')=='msg') {
$id= request_get('lid');
$message=$id." New Leads has been added through xls file ";
		set_session('e_flag' , 1);
		set_session('message' , $message);
header("location:leadcard.php");
exit;
}

include "lcas_header.php";
include "lcas_top_nav.php";
include "js/leadcard.js";

$userid=get_session('LOGIN_USERID');
$ID=get_session('LOGIN_ID');
$today=date('Y-m-d H:i:s');
$child=array($ID);
$re=mysql_query("select id from tps_users where parentid='$userid'");
while($r=mysql_fetch_array($re)){
array_push($child,$r['id']);
}
$child=implode(',',$child);
?>
<form id="frm" name="frm" class="fillup" action="leadcard.php" method="POST">	
<div class="main-content" style="margin:0px;">
  <div class="container">
<div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>Lead Listing&nbsp;&nbsp;<a class="btn btn-blue" href="add_leads.php"><span>Add New Lead</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="createtask.php"><span>Create Task</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="create_event.php"><span>Create Event</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="#" id="emailit"><span>Email</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" id="mapit" href="#" ><span>Map It</span></a>	
&nbsp;&nbsp;<button  type="button" class="btn btn-blue" onclick="printpage()">Print</button>
<?php
$bup=getPermissions(__BULKUPDATE__);
if($bup==0)
{
?>
&nbsp;&nbsp;<button  type="button" id='bulk' class="btn btn-blue" >Bulk Update</button>
<?php
}
?>
<?php 
$bdp=getPermissions(__BULKDELETE__);
if($bdp==0)
{
?>
&nbsp;&nbsp;<button type="button" id='bulkDelete' class="btn btn-red" >Bulk Delete</button>
<?php
}
?>
	</h3>
</div> 
<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
      </div>
    </div>
  </div>

<div class="container">

<div class="box">

<div class="box-header">
<div id="search_container">
<?php if(isset($_POST['filter']))
{
$array = array();
foreach($_POST['select'] as $val)
{

 array_push($array, $val);
	$q .= $val;
}
$se= implode(", ", $array);
$sel_array=explode(",",$se);
$sel_zip=$sel_array[0];
$sel_city=$sel_array[1];
$sel_dealer=$sel_array[2];
$lstatus=$sel_array[3];
$lres=$sel_array[4];
$ltype=$sel_array[5];
$sel_ref=$sel_array[6];
$lqual=$sel_array[7];
}?>
<label><span>Zip</span><select id="Zip"  name="select[]" >
<option value="">All</option>
<?php
	$re=mysql_query("select DISTINCT zip from tps_lead_card where FIND_IN_SET(uid,'$child') and customer_flag='0' and delete_flag='0' and zip!='' order by id desc");

	while($s=mysql_fetch_array($re))
	{       if($sel_zip==$s['zip'])
                echo "<option selected=selected>".$s['zip']."</option>";
  		else
		echo "<option>".$s['zip']."</option>";
	} ?>
     			
			</select>
</label>
<label>
<span >City</span><select id="city"  name="select[]" >
<option value="">All</option>
<?php
	$re=mysql_query("select DISTINCT city from tps_lead_card where FIND_IN_SET(uid,'$child') and customer_flag='0' and delete_flag='0' and city!='' order by id desc");

	while($s=mysql_fetch_array($re))
	{       if(trim($sel_city)==trim($s['city']))
                echo "<option selected=selected>".$s['city']."</option>";
  		else
		echo "<option>".$s['city']."</option>";
	} ?>
     			
			</select>
</label>
<label>
<span >Lead Dealer</span><select id="dealer" name="select[]" id="team">
<option value="">All</option>
<?php 	
	$re=mysql_query("select userid,fname,lname from tps_users where  status='1'");

	while($r=mysql_fetch_array($re))
	{    
		  if($sel_dealer==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]' >".$r['fname']." ".ucfirst($r['lname'])."</option>";
	} ?>
     			
			</select>
</label>
<label>
<span >Lead Status</span><select  name="select[]"  >
<option value="">All</option>
                  	
	<?php 	
     $sq=mysql_query("select name,display from tps_lead_status where status='0'")or die(mysql_error());


	while($row = mysql_fetch_array($sq)){

		 if(trim($lstatus)==trim($row['name']))
                echo '<option value="'.$row['name'].'" selected>'.$row['display'].'</option>';
  		else
			echo '<option value="'.$row['name'].'" >'.$row['display'].'</option>';
	
	}
		?>

</select> 
</label>
<label>
<span>Lead Result</span><select  name="select[]"  >
<option value="">All</option>
                  	
	<?php 	
     $sq=mysql_query("select name,display from tps_lead_result where status='0'")or die(mysql_error());


	while($row = mysql_fetch_array($sq)){

		 if(trim($lres)==trim($row['name']))
                echo '<option value="'.$row['name'].'" selected>'.$row['display'].'</option>';
  		else
			echo '<option value="'.$row['name'].'" >'.$row['display'].'</option>';
	
	}
		?>

			</select>
</label>

<label>
  <span>Lead Type</span><select id="leadtype" name="select[]" >
<option value="">All</option>
                  	<?php echo getLeadType(trim($ltype)); ?>
			</select>   
</label>
<label>
 <span>Referred By</span><select id="referredby" name="select[]"  >

<option value="">All</option>
<?php 
$sql_new="select DISTINCT referred_by from tps_lead_card where FIND_IN_SET(uid,'$child')  and delete_flag='0' and referred_by!='' order by id desc";
$result_new=mysql_query($sql_new);

while($row=mysql_fetch_array($result_new))
{	
	if($row['referred_by']!='')
	{
		$ref=$row['referred_by'];
		$im=explode('_',$ref);
		if($im[0]==1){
			$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=ucfirst($r['lname1'])." ".$r['fname1']." ".$r['lname2']." ".$r['fname2'];
		}
		if($im[0]==2){
			$sq=mysql_query("select campaign from tps_campaign where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=$r['campaign'];
		}
		if($im[0]==3){
			$sq=mysql_query("select fname1,lname1 from tps_lead_card where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=$r['fname1'].' '.$r['lname1'];
		}
	}

	if(trim($sel_ref)==trim($row['referred_by']))
		echo "<option value=".$row['referred_by']." selected=selected>".$refby."</option>";
	else
		echo "<option value=".$row['referred_by'].">".$refby."</option>";

	$refby='';

}
	?>
			</select>       
</label>
<label>
<span>Lead Qualification</span><select  name="select[]"  >
<option value="">All</option>
                  	
	<?php 	
     $sq=mysql_query("select display from tps_lead_qualified where status='0'")or die(mysql_error());


	while($row = mysql_fetch_array($sq)){

		 if(trim($lqual)==trim($row['display']))
                echo '<option value="'.$row['display'].'" selected>'.$row['display'].'</option>';
  		else
			echo '<option value="'.$row['display'].'" >'.$row['display'].'</option>';
	
	}
		?>

			</select>
</label>
	<button type="submit" id="list"  name="filter" class="btn btn-blue">List</button>
	<button type="submit" name="reset" class="btn btn-blue">Reset</button>
                      </a>

</div>
</div>
<div class="box-content">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive" id="example" >
<thead>
<tr>
  <th style="height:100%;">Action <span style='margin-left:10px;'><input type="checkbox" id="check"  class="icheck1" ></span></th>
 <th style="display:none;"><div>City</div></th>
 <th style="display:none;"><div>Lead_Satatus</div></th>
  <th ><div style='height:100%;display:inline-block;width:150px;'>Name1 & Name2</div></th>
  <th ><div style='height:100%;display:inline-block;'>Address<br>City,State,Zip</div></th>
  <th ><div style='height:100%;width:90px;display:inline-block;'>Phone-1 Phone-2  Phone-3</div></th>
  <th style="display:none;"><div style='height:100%;display:inline-block;'>Email</div></th>
  <th style="display:none;"><div style='height:100%;display:inline-block;'>Contact Time</div></th>
 <th ><div style='height:100%;display:inline-block;'>Lead In</div></th>
  <th ><div style='height:100%;display:inline-block;'>Lead Type </div></th>
 <th ><div style='height:100%;display:inline-block;'>Lead SubType </div></th>
  <th><div style='height:100%;display:inline-block;width:80px;'>Referred By</div></th>
<th><div style='height:100%;display:inline-block;'>Lead Dealer</div></th> 
<th><div style='height:100%;display:inline-block;'>Start Contacting</div></th> 
  <th><div style='height:100%;display:inline-block;'>Set Date</div></th>
  <th><div style='height:100%;display:inline-block;'>Set By</div></th>
   <th ><div style='height:100%;display:inline-block;'>Lead Status </div></th>
  <th><div style='height:100%;display:inline-block;'>Lead Result</div></th>
<th style="display:none;"><div>appt_date</div></th>
  <th style="display:none;"><div>Type</div></th>
  <th style="display:none;"><div>Name1</div></th>
 <th style="display:none;"><div>City</div></th>
<th style="display:none;"><div>leadsta</div></th>
 <th style="display:none;"><div>leadid</div></th>
<th style="display:none;"><div>leadseqid</div></th> 
<th style="display:none;"><div>Dealer</div></th>
<th><div style='height:100%;display:inline-block;'>Lead Qual.</div></th>
<th style="display:none;"><div>Phone1</div></th>
<th style="display:none;"><div>Appt Status</div></th>
<th style="display:none;"><div>Appt Result</div></th>
<th style="display:none;"><div>Phone2</div></th>
<th style="display:none;"><div>Phone3</div></th>
<th style="display:none;"><div>Email 2</div></th>
</tr>
</thead>
<tbody>
<?php

$sql_qry = "";

if(isset($_REQUEST['type']))
{
	if($_REQUEST['type']=="newtask")
	{
		$viewid=$_REQUEST['id'];
		$sql_qry = "select * from tps_lead_card  where id='$viewid' and delete_flag='0' order by id desc";
		//$result = mysql_query($sql_qry) or die(mysql_error());
		set_session('leadqry',$sql_qry);

	}
	else{
		$sql_qry = "select * from tps_lead_card where uid = '$child' and delete_flag='0'  order by id desc";
		set_session('leadqry',$sql_qry);
		//$result = mysql_query($sql_qry) or die(mysql_error());
	}
}else if(isset($_POST['filter'])){

	$new_array = array();
	$n=1;
	foreach($_POST['select'] as $value)
	{
		switch ($n)
		{
			case 1:
			  $column='zip';
			  break;
			  
			case 2:
			  $column='city';
			  break;
			  
			case 3:
			  $column='lead_dealer';
			  break;
			  
			case 4:
			  $column='lead_status';
			  break;
			  
			case 5:
			  $column='lead_result';
			  break;

			case 6:
			  $column='lead_type';
			  break;
			  
			case 7:
			  $column='referred_by';
			  break;

			case 8:
			  $column='lead_qualification';
			  break;
		} 
		if($value!=''){
		 array_push($new_array, $value);
			$query .= $column."='".$value."' and ";
			}
		$n++;	
	}
	$string = rtrim($query, ' and ');

	if($string=='')
	{
		$sql_qry="select * from tps_lead_card where FIND_IN_SET(uid,'$child') and delete_flag='0'  order by id desc";
	}
	else
	{
		$sql_qry="select * from tps_lead_card where $string and FIND_IN_SET(uid,'$child') and delete_flag='0'  order by id desc";
	}
	$selected= implode(", ", $new_array);
	$sel=explode(",",$selected);

	set_session('leadqry',$sql_qry);

}else if(isset($_POST['reset'])){

	$sql_qry = "select * from tps_lead_card where FIND_IN_SET(uid,'$child') and delete_flag='0' order by id desc";
	set_session('leadqry',$sql_qry);

}else{
	$sql_qry = "select * from tps_lead_card where FIND_IN_SET(uid,'$child') and delete_flag='0' order by id desc";
	
}

//echo "<pre>$sql_qry</pre>";

$leadQuery=get_session('leadqry');

if($leadQuery!="")
{
	$result = mysql_query($leadQuery) or die(mysql_error());
	set_session('leadqry',$leadQuery);
	$leadQuery=get_session('leadqry');
}else{
	$result = mysql_query($sql_qry) or die(mysql_error());
	set_session('leadqry',$sql_qry);
}


while($row = mysql_fetch_array($result))
{
	if($row['referred_by']!='')
	{
		$ref=$row['referred_by'];
		$im=explode('_',$ref);
		if($im[0]==1){
			$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]' ")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=ucfirst($r['lname1'])." ".$r['fname1']."<br>".$r['lname2']." ".$r['fname2'];
		}
		if($im[0]==2){
			$sq=mysql_query("select campaign from tps_campaign where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=$r['campaign'];
		}
		if($im[0]==3){
			$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=ucfirst($r['fname1'])." ".$r['lname1'];
		}
	}

$sql_select="select fname,lname from tps_users where userid='$row[dealer]'";
$result_select = mysql_query($sql_select) or die(mysql_error());
$row1 = mysql_fetch_array($result_select);

$sql_select1="select DATE_FORMAT(start , '%b %d, %Y') as apt_date,DATE_FORMAT(start , '%h:%i %p') as apt_start,DATE_FORMAT(end , '%h:%i %p') as apt_end, start,end,appttype,DATE_FORMAT(created_at , '%m/%d/%y %h:%i %p') as set_date,createdby from tps_events where lead_id='$row[id]' and delete_flag='0' and appttype='Demo'";
//echo "<pre>$sql_select1</pre>";
$result_select1 = mysql_query($sql_select1) or die(mysql_error());
$row2 = mysql_fetch_array($result_select1);
$num=mysql_num_rows($result_select1);
if($num>0)
{
$sql2="select fname,lname from tps_users where userid='".$row2['createdby']."'";
		$result2=mysql_query($sql2) or die(mysql_error());
		$row5 = mysql_fetch_array($result2);
		$setby=ucfirst($row5['fname'])." ".$row5['lname'];
$setdate=$row2['set_date'];
$startDate = date('M d , Y',strtotime($row2['start']));
$startTime = date('h:i A',strtotime($row2['start']));
$endTime = date('h:i A',strtotime($row2['end']));

//echo "<pre>".$row2['start']." ; ".$row2['end']."</pre>";

$aptdatetime=$row2['apt_date']." <br> ".$row2['apt_start']." - ".$row2['apt_end'];

$type=$row2['appttype'];
}
else
{
$setby='';
$setdate='';
$startDate ='';
$startTime ='' ;
$endTime = '';
$aptdatetime="";
$type='';
}
if($row['lead_in']!='0000-00-00'){
		$date = date_create($row['lead_in']);
		$lindate=date_format($date, 'm/d/y');
}
else{
$lindate ='' ;
}
$leadtype=getleadtype_name($row['lead_type']);
$leadsubtype=getleadsubtype_name($row['lead_subtype']);
?>
	<tr>
	<td>
<?php
	$subsql="";

	$lead_type=$row['lead_type'];
	$lead_subtype=$row['lead_subtype'];
	$cur_dealerid=$row['lead_dealer'];

	$subsql=getMappedScript($lead_type,$lead_subtype,$cur_dealerid);

	$scriptsql ="SELECT id, name, no_of_questions from tps_scripts where id=($subsql) and status='0'";

	$scriptsql_list=mysql_query($scriptsql) or die(mysql_error());
	$scriptres = mysql_fetch_array($scriptsql_list);

	$scriptname=ucfirst($scriptres['name'])." (".$scriptres['no_of_questions'].") ";

	$lead_address="";
	$brflag=0;
	
	$lead_address.=$row['add_line1'];
	if ( $row['add_line2'] != '' )
	{
		$lead_address.=", <br>".$row['add_line2'];
	}
	if($row['city']!="")
	{
		if($lead_address!="")
		{
			$lead_address.=", <br>".$row['city'];
		}
		else
		{
			$lead_address.=$row['city'];
		}
	}

	if($row['state']!="")
	{
		if($lead_address!="" )
		{
			$lead_address.=", ".$row['state'];
		}
		else
		{
			$lead_address.=$row['state'];
		}
	}

	if($row['zip']!="")
	{
		if($lead_address!="")
		{
			$lead_address.=", ".$row['zip'];
		}
		else
		{
			$lead_address.=$row['zip'];
		}
	}
?>

 <div class="btn-group" style="float:left;">
      <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button>
      <ul class="dropdown-menu">

<?php 
$eop=getPermissions(__EDITONE__);
if($eop==0)
{
?>
<?php if($row['customer_flag']=='1'){?> <li><a href="add_leads.php?action=updatecustomer&lid=<?php echo $row['id']; ?>&redir=3"><img src="images/edit.png"/>&nbsp;&nbsp;Edit</a></li><?php } else{?>
          <li><a href="add_leads.php?action=edit&lid=<?php echo $row['id'];?>&redir=3"><img src="images/edit.png"/>&nbsp;&nbsp;Edit</a></li><?php } ?>
<?php 
}
?>
	  <li>
<?php	
if($row['is_surveyed']=="1") 
	{ ?>
		<a href="survey_followup_qa_form.php?lid=<?php print $row['id'];?>&leadseqid=<?php print $row['leadid'];?>" title="<?php echo $scriptname; ?>"><img src="images/survey.png"  title="<?php echo $scriptname; ?>" width="18px" height="18px"/>&nbsp;&nbsp;Call Script [ <?php echo $scriptname; ?> ]</a>
	<?php 
	}else{ 
	?>
		<a href="baby_survey_qa_form.php?job=add&lid=<?php print $row['id'];?>&leadseqid=<?php print $row['leadid'];?>" title="<?php echo $scriptname; ?>"> <img src="images/survey.png" title="<?php echo $scriptname; ?>" width="18px" height="18px"/>&nbsp;&nbsp;Call Script [ <?php echo $scriptname; ?> ] </a>
	<?php
	}
?>
	</li>
	<li><a href="#" onclick="viewaddr_gmaps('<?php echo $lead_address; ?>','<?php echo $row['id']; ?>');"><img src="images/google_maps.png" width="18px" height="18px" title="View Address in Google Map">&nbsp;&nbsp;Google Map</a></li>

<?php 
$dop=getPermissions(__DELETEONE__);
if($dop==0)
{
?>	
	<li class="divider"></li>
	<li><a href="#" onClick="return confirmDelete(<?php print $row['id']; ?>,<?php echo $row['customer_flag'];?>);"><img src="images/block.png" width="18px"/>&nbsp;&nbsp;Delete</a></li>
<?php 
}
?>
<!--&nbsp;&nbsp;<a class="btn btn-blue" href="addlead_xls.php">Import Leads</a>&nbsp;&nbsp;-->
	      </ul>
  </div>
  <div style="margin-left:50px;margin-top:5px;">
	<input type="checkbox" id="icheck1" value="<?php echo $row['leadid'];?>" lid="<?php echo $row['id'];?>" class="icheck1" ></div>
	</td>
	<td style="display:none;"><?php echo $row['city']; ?></td>
	<td style="display:none;"><?php echo $row['lead_status']; ?></td>
	<td><?php echo ltrim($row['title1'],"Select") ." ". ucfirst($row['fname1'])." ".ucfirst($row['lname1'])." <br> ".ltrim($row['title2'],"Select") ." ". ucfirst($row['fname2'])." ".ucfirst($row['lname2']); ?></td>
	<td><?php echo $lead_address; ?></td>
	<td><?php 
			echo $row['phone1'];
			if( $row['phone2'] != ''  && $row['phone2'] !='')
				echo ", <br/>".$row['phone2'];
			if( $row['phone3'] != '' &&  $row['phone3'] !='' )
				echo ", <br/>".$row['phone3']; 
	     ?>
	</td>
	<td style="display:none;"><?php 
			echo $row['email1'];
	     ?>
	</td>
	<td style="display:none;"><?php echo $row['time_contact']; ?></td>
	<td><?php echo $lindate; ?></td>
	<td><?php echo $leadtype; ?></td>
	<td><?php echo $leadsubtype; ?></td>
	<td><?php echo $refby; ?></td>
	<td><?php 
		$sql_select="select fname,lname from tps_users where userid='$row[lead_dealer]'";
		$result_select = mysql_query($sql_select) or die(mysql_error());
		$row2 = mysql_fetch_array($result_select);
		echo ucfirst($row2['fname'])." ".ucfirst($row2['lname']);
	    ?>
	</td>
<td><?php if($row['oktocall']!="0000-00-00") echo date('m/d/y',strtotime($row['oktocall'])); ?></td>
<td><?php echo $setdate?></td>
<td><?php echo $setby;?></td>
<td><?php echo '<a href="#" class="link" style="text-decoration:underline;" onclick="loadAjaxContent('.$row['id'].','.$row['leadid'].');">'.$row['lead_status'].'</a>';?></td>
<td><?php echo $row['lead_result']; ?></td>
<td style="display:none;"> <?php echo $aptdatetime; ?> </td>		
<td style="display:none;"> <?php echo $type;?></td>
<td style="display:none;"><?php echo ltrim($row['title1'],"Select") . ucfirst($row['fname1'])." ".$row['lname1']; ?></td>
<td style="display:none;"> <?php echo $row['city'];?></td>
<td style="display:none;"> <?php echo $row['lead_status'];?></td>
<td style="display:none;"> <?php echo $row['id'];?></td>
<td style="display:none;"> <?php echo $row['leadid'];?></td>
<td style="display:none;"><?php echo ucfirst($row1['fname'])." ".ucfirst($row1['lname']);?></td>
<td><?php echo $row['lead_qualification'];?></td>
<td style="display:none;">
<?php
	if($row['phonetype1']!="")
		echo $row['phonetype1']." : ";

	echo $row['phone1'];

	if($row['phone2'] != ''  && $row['phone2'] !='')
	{
		if($row['phonetype2']!="")
			echo ",<br>".$row['phonetype2']." : ".$row['phone2'];
		else
			echo ",<br>".$row['phone2'];
	}
	if($row['phone3'] != ''  && $row['phone3'] !='')
	{
		if($row['phonetype3']!="")
			echo ",<br>".$row['phonetype3']." : ".$row['phone3'];
		else
			echo ",<br>".$row['phone3'];
	}
?>
</td>
<?php

$leadid=$row['leadid'];

$sql_tas="select appt_status,appt_result from tps_appt_status_update where lead_id='$leadid' and delete_flag='0' order by id desc ";

$restas=mysql_query($sql_tas) or die(mysql_error());
$tas = mysql_fetch_array($restas);

$appt_result=$tas['appt_result'];
$appt_status=$tas['appt_status'];
if($appt_status=='')
{
	$appt_status="No Appt Status";
}
if($appt_result=='')
{
	$appt_result="No Result";
}

?>
<td style="display:none;"><?php echo $appt_status; ?></td>
<td style="display:none;"><?php echo $appt_result; ?></td>
<td style="display:none;">
<?php
	if($row['phone2'] != ''  && $row['phone2'] !='')
	{
		if($row['phonetype2']!="")
			echo "<br>".$row['phonetype2']." : ".$row['phone2'];
		else
			echo "<br>".$row['phone2'];
	}
?>
</td>
<td style="display:none;">
<?php
	if($row['phone3'] != ''  && $row['phone3'] !='')
	{
		if($row['phonetype3']!="")
			echo "<br>".$row['phonetype3']." : ".$row['phone3'];
		else
			echo "<br>".$row['phone3'];
	}
?>
</td>
<td style="display:none;">
<?php 
	if ( $row['email2'] != '')
		echo $row['email2'];
?>
</td>
</tr>
<?php
$refby='';
} // while close		
?>
	
</tbody>
</table>
 </div>
    </div>
</div>

<div class="row">
  <div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Name and Address</span>
      </div>
      <div class="box-content padded">
        
	<div id="lead_info"></div>
	
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Lead Details</span>
      </div>
      <div class="box-content padded">

 	<div id="lead_details"></div>
	
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Appointment Details</span>
      </div>
      <div class="box-content padded">

	<div id="apt_info"></div>

        </form>
      </div>
    </div>

  </div>
<div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Lead Survey Details</span>
      </div>
<div id="spinner"></div>
      <div class="box-content padded">
	<div id="survey">

</div>
	
      </div>
    </div>
  </div>

  </div>
</form>
</div>
  <div id=ajaxcontainer> </div> 
<?php

include "lcas_footer.php";
?>
