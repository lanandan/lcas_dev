<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">

  <!-- Always force latest IE rendering engine or request Chrome Frame -->
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">

  <!-- Use title if it's in the page YAML frontmatter -->
  <title> <?php echo $page_title;?></title>
<script src="js/jquery-1.4.1.js"></script>

   <link href="stylesheets/jquery-ui.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="stylesheets/application.css" media="screen" rel="stylesheet" type="text/css" />
  <!--<link href="stylesheets/fullcalendar.css" media="screen" rel="stylesheet" type="text/css" /> -->
  <link href="stylesheets/custom.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="stylesheets/tinybox2.css" media="screen" rel="stylesheet" type="text/css" />
  <script src="javascripts/application.js" type="text/javascript"></script>
  <!--<script src="javascripts/fullcalendar.js" type="text/javascript"></script>
  <script src="javascripts/calendar_functions.js" type="text/javascript"></script>-->
  <script src="javascripts/tinybox2/tinybox.js" type="text/javascript"></script>
 <script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="javascripts/bootbox.min.js"></script>



<script>
(function(a){jQuery.sessionTimeout=function(b){function f(a){switch(a){case"start":redirTimer=setTimeout(function(){window.location=d.redirUrl},d.redirAfter-d.warnAfter);break;case"stop":clearTimeout(redirTimer);break}}function e(b){switch(b){case"start":dialogTimer=setTimeout(function(){a("#sessionTimeout-dialog").dialog("open");f("start")},d.warnAfter);break;case"stop":clearTimeout(dialogTimer);break}}var c={message:"Your session is about to expire.",keepAliveUrl:"keepAlive.asp",redirUrl:"logout.php",logoutUrl:"logout.php",warnAfter:9e5,redirAfter:12e5};var d=c;if(b){var d=a.extend(c,b)}a("body").append('<div title="Session Timeout" id="sessionTimeout-dialog">'+d.message+"</div>");a("#sessionTimeout-dialog").dialog({autoOpen:false,width:400,modal:true,closeOnEscape:false,open:function(b,c){a(".ui-dialog-titlebar-close").hide()},buttons:{"Log Out Now":function(){window.location=d.logoutUrl},"Stay Connected":function(){a(this).dialog("close");a.ajax({type:"POST",url:d.keepAliveUrl});f("stop");e("start")}}});e("start")}})(jQuery)

$(document).ready(function() {
    $.sessionTimeout({
        warnAfter : 20000000,
        redirAfter: 30000000
    });
});
</script>

<?php
//date_default_timezone_set ("America/New_York");
?>
<style>
#spinner {
position: fixed;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.5;
background: url('images/preload.gif') 50% 50% no-repeat #ede9df;
}

.react{
	color:blue;
}
.react a{
	color:blue;
}
.react a: hover{
	color:blue;
}
</style>
</head>

