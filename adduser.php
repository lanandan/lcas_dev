<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "add_leads.php";
$page_title = $site_name." -  Add / Edit Team Members";
$login_level=get_session('LOGIN_LEVEL');
$lid='';

$lid=request_get('lid');
if($lid==''){
$lid=get_session('LOGIN_ID');     
}
$title='' ;
$fname =  '' ;
$lname='' ;
$add_line1='' ;
$add_line2='' ;
$city='' ;
$state='' ;
$zip='' ;
$pwd='';
$dealer_flag='';
$nname='';
$radio='';
$phonetype1='' ;
$phone1='' ;
$phonetype2='' ;
$phone2='' ;
$phonetype3='' ;
$phone3='' ;
$comment='';
$email1='' ;
$email2='' ;
$comments='' ;
$image1='';
$tdate='' ;
$odate='' ;
$rdate='' ;
$bdate='';
$gdate='';
$idate='';
$ssn='';
$id='';
$dln='';
$pfname='';
$plname='';
$pnum='';
$pemail='';
$aboutchildren='';
$demodate='';
$ltype='';
$teamtype='';
$teamposition='';
$teamstatus='';
$manager1='';
$manager2='';
$mmgr='';
$rtype='';
$rby='';
$leadid='';
$username='';
$cus='';
$user_dispname=get_session('DISPLAY_NAME');
$user_logid=get_session('LOGIN_ID');
$url= $_SERVER['HTTP_REFERER'];
$image1='default.jpeg';
$action=request_get('action');
if(request_get('id')){
$id=request_get('id');
$sql="update tps_users set status='0' where id='$id'";
$result=mysql_query($sql) or die(mysql_error());
$message="The Employee Removed From Your Team";
		set_session('e_flag' , 1);
		set_session('message' , $message);
$log_desc= ucfirst($user_dispname)." Remove 1  User From his team. <b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, " User Deleted from team", $user_logid, $log_desc);
header("location:teamview.php");
exit;
}

if( request_get('action')=='add') {
if(isset($_REQUEST['cus'])){
$cus=1;
}
$leadid=request_get('leadid');
$sql="select * from tps_lead_card where leadid='$leadid'";
$result=mysql_query($sql)or die(mysql_error());
$row=mysql_fetch_array($result);
$take=request_get('take');
$lll=$row['id'];
$s=mysql_query("select start from tps_events where lead_id='$lll'")or die(mysql_error());
$r=mysql_fetch_array($s);
		$demodate=date('M d Y',strtotime($r['start'])) ;
		$idate=date('M d Y',strtotime($r['start'])) ;

if($take=='1')
{
$fname =$row['fname1'];
$lname=$row['lname1'];
$pfname=$row['fname2'];
$plname=$row['lname2'];
}
else
{
$fname =$row['fname2'];
$lname=$row['lname2'];
$pfname=$row['fname1'];
$plname=$row['lname1'];
}
		$add_line1=$row['add_line1'];
		$add_line2=$row['add_line2'];
		$city=$row['city'];
		$state=$row['state'];
		$zip=$row['zip'];
		$title=$row['title1'];
		$phonetype1=$row['phonetype1'];
		$phone1=$row['phone1'];
		$phonetype2=$row['phonetype2'];
		$phone2=$row['phone2'];
		$phonetype3=$row['phonetype3'];
		$phone3=$row['phone3'];
		$email1=$row['email1'];
		$email2=$row['email2'];
		$teamtype='Dealer';
		$teamposition='Recruit In Training';
		$teamstatus='PT - Part-Time (Active)';
		$rtype='Demo';
		$rby=$row['lead_dealer'];
}
if( request_get('action')=='edit' || request_get('action')=='adminedit' ) {
if( request_get('action')=='edit'){	
$userid =get_session('LOGIN_ID');
}
if( request_get('action')=='adminedit'){	
$userid =request_get('lid');
}
		$sql="select * from tps_users where id='".$userid."'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);
		$uid=$row['id'];
		$title=$row['nameprefix'];
		$fname =  $row['fname'];
		$lname=$row['lname'];
		$nname=$row['nickname'];
		$add_line1=$row['address1'];
		$add_line2=$row['address2'];
		$city=$row['city'];
		$state=$row['state'];
		$zip=$row['zipcode'];
		$username=$row['username'];
		$phonetype1=$row['phonetype1'];
		$phone1=$row['phone1'];
		$phonetype2=$row['phonetype2'];
		$phone2=$row['phone2'];
		$phonetype3=$row['phonetype3'];
		$phone3=$row['phone3'];
		$radio=$row['deposit'];
		$email1=$row['email1'];
		$email2=$row['email2'];
		$teamtype=$row['team_type'];
		$teamposition=$row['team_position'];
		$teamstatus=$row['team_status'];
		$manager1=$row['manager1'];
		$manager2=$row['manager2'];
		$mmgr=$row['marketing_manager'];
		$rtype=$row['recruited_type'];
		$rby=$row['recruited_by'];
		$dealer_flag=$row['dealer_flag'];
if($row['training_date']!='0000-00-00'){
		$tdate=date('M d Y',strtotime($row['training_date'])) ;
}
if($row['opportunity_date']!='0000-00-00'){
		$odate=date('M d Y',strtotime($row['opportunity_date'])) ;
}
if($row['birth_date']!='0000-00-00'){
		$bdate=date('M d Y',strtotime($row['birth_date']));
}
if($row['graduation_date']!='0000-00-00'){
		$gdate=date('M d Y',strtotime($row['graduation_date']));
}
if($row['intrested_date']!='0000-00-00'){
		$idate=date('M d Y',strtotime($row['intrested_date']));
}
if($row['demo_date']!='0000-00-00'){
		$demodate=date('M d Y',strtotime($row['demo_date']));
}
		$ssn=$row['ssn'];
		$dln=$row['driving_license_num'];
		$pfname=$row['partner_fname'];
		$plname=$row['partner_lname'];
		$pnum=$row['partner_phone_num'];
		$pemail=$row['partner_emailid'];
		$aboutchildren=stripslashes($row['about_children']);
		$comment=stripslashes($row['comment']);
		$image1=$row['image1'];
	}


if( isset($_POST['savemember']) ){
	
$pic=request_get('pic');
	if (isset($_FILES["photo"]["type"])) {
		$allowed =  array('gif','png' ,'jpg','jpeg');
$filename = $_FILES['photo']['name'];
$ext = pathinfo($filename, PATHINFO_EXTENSION);
$ext =strtolower($ext);
			if(!in_array($ext,$allowed) ) {
   				 echo 'error';
				}
			else{
			
		$expl=explode(".",$_FILES['photo']['name']);	

                   	$ext= $expl[sizeof($expl) - 1];
			$lid=$_POST['lid'];
			$sql=mysql_query("select username from tps_users where id='$lid'") or die(mysql_error());
			$r=mysql_fetch_array($sql);
                        $username=$r['username'];
                        $file_name= $username.".".$ext;
			$target = "images/upload/"."$file_name"; 
			$target = $target . $_FILES['photo']['name']; 
			$pic=$file_name; 
                        makeThumbnails("images/upload/thumb/", $_FILES['photo']['tmp_name'], $username,50,50);
			move_uploaded_file($_FILES['photo']['tmp_name'],$target);
			rename($target,"images/upload/".$file_name);

		}
		
	}
	$timestamp=fmt_db_date_time();


$userid=$_POST['userid'];
 $lid=$_POST['lid'];
	$title= $_POST['title'];
	$fname= $_POST['fname'];
	$lname= $_POST['lname'];
	$nname= $_POST['nname'];
	$email1= $_POST['email1'];
	$email2= $_POST['email2'];
	$mobile1 = $_POST['phone1'];
	$mobile2 = $_POST['phone2'];
	$mobile3 = $_POST['phone3'];
	$phonetype1=request_get('phone1_type');
	$phonetype2=request_get('phone2_type');
	$phonetype3=request_get('phone3_type');
	$addr_line1=request_get('addr1');
	$addr_line2=request_get('addr2');
	$city=request_get('city');
	$zip=request_get('zip');
	$state=request_get('state');
	$teamtype=request_get('teamtype');
	$teamposition=request_get('teamposition');
	$teamstatus=request_get('teamstatus');
	$manager1=request_get('manager1');
	$manager2=request_get('manager2');
	$mmgr=request_get('mmgr');
	$demodate=request_get('demodate');
	$rtype=request_get('rtype');
	$rby=request_get('rby');
	$radio=request_get('radio');
	$idate=request_get('idate');
	$odate=request_get('odate');
	$gdate=request_get('gdate');
	$tdate=request_get('tdate');
	$bdate=request_get('bdate');
	$ssn=request_get('ssn');
	$dln=request_get('dln');
	$pfname=request_get('pfname');
	$plname=request_get('plname');
	$pnum=request_get('pnum');
	$pemail=request_get('pemail');
	$aboutchildren=request_get('aboutchildren');
	$comment=request_get('comment');
	$uname=request_get('uname');
	$pwd=request_get('pwd');
	$dealer=request_get('dealer');
if($demodate!=''){
$demodate=date('Y-m-d',strtotime($demodate));
}
if($odate!=''){
$odate=date('Y-m-d',strtotime($odate));
}
if($idate!=''){
$idate=date('Y-m-d',strtotime($idate));
}
if($tdate!=''){
$tdate=date('Y-m-d',strtotime($tdate));
}
if($gdate!=''){ 
$gdate=date('Y-m-d',strtotime($gdate)) ;
}
if($bdate!=''){
$bdate=date('Y-m-d',strtotime($bdate));
}

if(request_get('admin')==1){
$sql="update tps_users set ".
	
		" `nameprefix` ='".$title."',".
		" `fname` ='".$fname."',".
		" `lname` ='".$lname."',".
		" `nickname` ='".$nname."',".
		" `email1` = '". $email1 . "', ".
		" `email2` = '". $email2 . "', ".
                " `phone1` ='".$mobile1."',".
		" `phone2` ='".$mobile2."',".
		" `phone3` ='".$mobile3."',".
		" `phonetype1` ='".$phonetype1."',".
		" `phonetype2` ='".$phonetype2."',".
		" `phonetype3` ='".$phonetype3."',".
		" `address1` ='".$addr_line1."',".
		" `address2` ='".$addr_line2."',".
		" `city` ='".$city."',".
		" `state` ='".$state."',".
		" `zipcode` ='".$zip."',".
		" `team_type` = '". $teamtype ."', ".
		" `team_position` = '". $teamposition ."', ".
		" `team_status` = '". $teamstatus ."', ".
		" `manager1` ='".$manager1."',".
		" `manager2` ='".$manager2."',".
		" `marketing_manager` = '".$mmgr."', ".
		" `demo_date` = '".$demodate."', ".
		" `recruited_type` = '". $rtype . "', ".
                " `recruited_by` ='".$rby."',".
		" `deposit` = '".$radio."', ".
		" `opportunity_date` = '". $odate . "', ".
                " `intrested_date` ='".$idate."',".
		" `training_date` = '".$tdate."', ".
		" `graduation_date` = '".$gdate."', ".
		" `birth_date` = '".$bdate ."', ".
		" `ssn` ='".$ssn."',".
		" `dealer_flag` = '". $dealer . "', ".
		" `driving_license_num` ='".$dln."',".
		" `partner_fname` = '".$pfname."', ".
		" `partner_lname` = '".$plname."', ".
		" `partner_phone_num` = '". $pnum . "', ".
                " `partner_emailid` ='".$pemail."',".
		" `about_children` = '". $aboutchildren . "', ".
                " `comment` ='".$comment."',".
                " `usertype`='1',".
                " `image1`='$pic',".
		" `modifiedtime` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."' where id='".$userid."'";	
		$result=mysql_query($sql) or die(mysql_error());
		$message="Your Team Member Profile has been updated";
		set_session('e_flag' , 1);
		set_session('message' , $message);
$log_desc= ucfirst($user_dispname)." Update ".$fname." ".$lname." profile.<br> <b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, " Team member profile Updated", $user_logid, $log_desc);
		header("Location: teamview.php" );
		exit;
		}

		$sql="update tps_users set ".
	
		" `nameprefix` ='".$title."',".
		" `fname` ='".$fname."',".
		" `lname` ='".$lname."',".
		" `nickname` ='".$nname."',".
		" `email1` = '". $email1 . "', ".
		" `email2` = '". $email2 . "', ".
                " `phone1` ='".$mobile1."',".
		" `phone2` ='".$mobile2."',".
		" `phone3` ='".$mobile3."',".
		" `phonetype1` ='".$phonetype1."',".
		" `phonetype2` ='".$phonetype2."',".
		" `phonetype3` ='".$phonetype3."',".
		" `address1` ='".$addr_line1."',".
		" `address2` ='".$addr_line2."',".
		" `city` ='".$city."',".
		" `state` ='".$state."',".
		" `birth_date` = '".$bdate ."', ".
		" `ssn` ='".$ssn."',".
		" `driving_license_num` ='".$dln."',".
		" `partner_fname` = '".$pfname."', ".
		" `partner_lname` = '".$plname."', ".
		" `partner_phone_num` = '". $pnum . "', ".
                " `partner_emailid` ='".$pemail."',".
		" `about_children` = '". $aboutchildren . "', ".
                " `comment` ='".$comment."',".
                " `usertype`='1',".
                " `image1`='$pic',".
		" `modifiedtime` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."' where id='".$userid."'";	
		$result=mysql_query($sql) or die(mysql_error());
$log_desc= ucfirst($user_dispname)." Update his  profile.<br> <b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, " Profile Updated", $user_logid, $log_desc);
              $message="Profile has been updated";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		header("Location: viewprofile.php" );
		exit;
	}
	

if( isset($_POST['addmember']) ){

$pic=request_get('pic');
	if (isset($_FILES["photo"]["type"])) {
		$allowed =  array('gif','png' ,'jpg','jpeg');
$filename = $_FILES['photo']['name'];
$ext = pathinfo($filename, PATHINFO_EXTENSION);
$ext =strtolower($ext);
			if(!in_array($ext,$allowed) ) {
   				 echo 'error';
				}
			else{
			
			$expl=explode(".",$_FILES['photo']['name']);	

                   	  $ext= $expl[sizeof($expl) - 1];
                          $username=request_get('uname');
                          $file_name= $username.".".$ext;

			$target = "images/upload/"."$file_name"; 
			$target = $target . $_FILES['photo']['name']; 
			$pic=$file_name; 
                        makeThumbnails("images/upload/thumb/", $_FILES['photo']['tmp_name'], $username,50,50);
			move_uploaded_file($_FILES['photo']['tmp_name'],$target);
			 rename($target,"images/upload/".$file_name);

		}
		
	}
$timestamp=fmt_db_date_time();


 	$lid=$_POST['lid'];
	$title= $_POST['title'];
	$fname= $_POST['fname'];
	$lname= $_POST['lname'];
	$nname= $_POST['nname'];
	$email1= $_POST['email1'];
	$email2= $_POST['email2'];
	$mobile1 = $_POST['phone1'];
	$mobile2 = $_POST['phone2'];
	$mobile3 = $_POST['phone3'];
	$phone1 = $_POST['phone1'];
	$phone2 = $_POST['phone2'];
	$phone3 = $_POST['phone3'];
	$phonetype1=request_get('phone1_type');
	$phonetype2=request_get('phone2_type');
	$phonetype3=request_get('phone3_type');
	$addr_line1=request_get('addr1');
	$add_line1=request_get('addr1');
	$add_line2=request_get('addr2');
	$addr_line2=request_get('addr2');
	$city=request_get('city');
	$zip=request_get('zip');
	$state=request_get('state');
	$teamtype=request_get('teamtype');
	$teamposition=request_get('teamposition');
	$teamstatus=request_get('teamstatus');
	$manager1=request_get('manager1');
	$manager2=request_get('manager2');
	$mmgr=request_get('mmgr');
	$demodate=request_get('demodate');
	$rtype=request_get('rtype');
	$rby=request_get('rby');
	$radio=request_get('radio');
	$idate=request_get('idate');
	$odate=request_get('odate');
	$gdate=request_get('gdate');
	$tdate=request_get('tdate');
	$bdate=request_get('bdate');
	$ssn=request_get('ssn');
	$dln=request_get('dln');
	$pfname=request_get('pfname');
	$plname=request_get('plname');
	$pnum=request_get('pnum');
	$pemail=request_get('pemail');
	$aboutchildren=request_get('aboutchildren');
	$comment=request_get('comment');
	$uname=request_get('uname');
	$pwd=request_get('pwd');
	$office_name='Lancaster County Air Systems, Inc';
	$office_city='Lancaster';
	$dealer=request_get('dealer');


$uname = $_POST['uname'];
$sql="select * from tps_users where username='$uname' and status='1'";
$result=mysql_query($sql) or die(mysql_error());
$row=mysql_num_rows($result);
if($row>0)
{
$message="Username Already Exist";
		set_session('e_flag' , 1);
		set_session('message' , $message);

	}
else{


  

	$sql="select * from tps_users where id='$lid'";
	$result=mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_array($result);
	$parentid=$row['userid'];
	$newuserid=generate_id('userid','userid');
	$level=$row['level'] + 1;
	$level1=$row['level1'];
	$level2=$row['level2'];
	$level3=$row['level3'];
	$level4=$row['level4'];


	switch ($level) {

		case 2:
			$level2=generate_id('level2','level2_id');
			$dummyid="DIR".$level2;
			 break;			
		case 3:
			$level3=generate_id('level3','level3_id');
			$dummyid="EMP".$level3;
			 break;
		case 4:
			$level4=generate_id('level4','level4_id');
			$dummyid="MEM".$level4;
			 break;

		default:
			$dummyid=$level1;
			 break;
	}


	if($demodate!=''){
$demodate=date('Y-m-d',strtotime($demodate));
}
	if($odate!=''){
$odate=date('Y-m-d',strtotime($odate));
}
if($idate!=''){
$idate=date('Y-m-d',strtotime($idate));
}
if($tdate!=''){
$tdate=date('Y-m-d',strtotime($tdate));
}
if($gdate!=''){ 
$gdate=date('Y-m-d',strtotime($gdate)) ;
}
if($bdate!=''){
$bdate=date('Y-m-d',strtotime($bdate));
}

	$sql="insert into tps_users set ".	
		" `userid` = '". $newuserid ."', ".
		" `parentid` = '". $parentid ."', ".
		" `level` = '". $level ."', ".
		" `level1` = '". $level1 ."', ".
		" `level2` = '". $level2 ."', ".
		" `level3` = '". $level3 ."', ".
		" `level4` = '". $level4 ."', ".
		" `dummyid` = '". $dummyid ."', ".
		" `nameprefix` ='".$title."',".
		" `fname` ='".$fname."',".
		" `lname` ='".$lname."',".
		" `nickname` ='".$nname."',".
		" `username` = '".$uname."', ".
		" `password` = '".md5($pwd)."', ".
		" `email1` = '". $email1 . "', ".
		" `email2` = '". $email2 . "', ".
                " `phone1` ='".$mobile1."',".
		" `phone2` ='".$mobile2."',".
		" `phone3` ='".$mobile3."',".
		" `phonetype1` ='".$phonetype1."',".
		" `phonetype2` ='".$phonetype2."',".
		" `phonetype3` ='".$phonetype3."',".
		" `address1` ='".$addr_line1."',".
		" `address2` ='".$addr_line2."',".
		" `city` ='".$city."',".
		" `state` ='".$state."',".
		" `zipcode` ='".$zip."',".
		" `team_type` = '". $teamtype ."', ".
		" `team_position` = '". $teamposition ."', ".
		" `team_status` = '". $teamstatus ."', ".
		" `manager1` ='".$manager1."',".
		" `manager2` ='".$manager2."',".
		" `marketing_manager` = '".$mmgr."', ".
		" `demo_date` = '".$demodate."', ".
		" `recruited_type` = '". $rtype . "', ".
                " `recruited_by` ='".$rby."',".
		" `deposit` = '".$radio."', ".
		" `office_name` = '".$office_name."', ".
		" `office_city` = '".$office_city."', ".
		" `opportunity_date` = '". $odate . "', ".
                " `intrested_date` ='".$idate."',".
		" `training_date` = '".$tdate."', ".
		" `graduation_date` = '".$gdate."', ".
		" `birth_date` = '".$bdate ."', ".
		" `ssn` ='".$ssn."',".
		" `driving_license_num` ='".$dln."',".
		" `partner_fname` = '".$pfname."', ".
		" `partner_lname` = '".$plname."', ".
		" `partner_phone_num` = '". $pnum . "', ".
                " `partner_emailid` ='".$pemail."',".
		" `about_children` = '". $aboutchildren . "', ".
		" `dealer_flag` = '". $dealer . "', ".
                " `comment` ='".$comment."',".
                " `usertype`='1',".
                "image1 = '".$pic. "', ".
                " `status` ='1',".
                " `createdtime`=  '".$timestamp."'";
		$result=mysql_query($sql) or die(mysql_error());
$log_desc= ucfirst($user_dispname)." Add ".$fname." ".$lname." to his team .<br> <b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, " New User added", $user_logid, $log_desc);
$leadid=request_get('leadid');
$cus=request_get('cus');
if($cus==1){
$sl=mysql_query("update tps_customer set recruit_flag=recruit_flag+1 where leadid='$leadid'")or die(mysql_error());
}
else
{
$sl=mysql_query("update tps_lead_card set recruit_flag=recruit_flag+1 where leadid='$leadid'")or die(mysql_error());
}
		$message="New Team Member has been Added";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		
		header("Location: teamview.php" );
		exit;
	}
}

include "lcas_header.php";
include "lcas_top_nav.php";

include "js/add_user.js";
?>

<form id="frmaddleads" name="frmaddleads" action="adduser.php" method="POST" enctype="multipart/form-data">
<div class="main-content" style="margin:0px;">
<div class="container">
<div class="row">
<div class="col-md-5" id='external-events' style="width:90%;position : absolute;   
    left:5%;
    top:5%;
    margin-top:20px;
">
<div class="accordion">
  <div class="accordion-group" >
    <div class="accordion-heading"><div style="float:right;padding:10px;padding-right:60px;">* Required</div> 
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
        <span class="title" style='font-size:18px;padding-left:40px;'><?php if(request_get('action')==""){ echo "Add New Team Member";} else if(request_get('action')=="edit"){ echo "Edit Profile";} else if(request_get('action')=="add"){ echo "Add New Recruit";}else { echo "Edit Team Member";} ?></span>
      </a>
   </div>
    <div id="collapseOne" class="accordion-body collapse in">
      <div class="accordion-inner padded">



<label for="male"></label><span><div class='div'><?php 

?>
<div style="position:absolute; left:5.2%;top:70px; border:1px solid black; height:100px; width:100px;border-radius:15px;"class="new2">
<a href="viewprofile.php?action=delete&uid=<?php echo $uid;?>&link=<?php echo $action;?>" title="Delete"  ><img src="images/delete.png" style="width:10px; position:absolute;right:5px;top:5px;<?php if($image1=='default.jpeg'){echo 'display:none';} ?>" id="pimage" class="key_image" title="Delete" onclick="return profconfirm()"/></a>
<img id="img" src="images/upload/<?php echo $image1; ?>" id="img" style="border-radius:15px;height:100%;width:100%;"><div id="link"><a  href="#">Change Picture</a></div></div>  <br>Name Prefix</div> <select class='nline' name="title" id="prefname1"  tabindex="1" >
		<option value="">Select</option>
		<?php echo getNamePrefix($title); ?>
	</select></span>

	<span><div class='div'>First Name *</div><input type="text" class="nline" data-prompt-position="topLeft" name="fname" id="fname1" tabindex="2" value="<?php echo $fname; ?>" placeholder="First Name" required/></span>
	<span><div class='div'>Last Name *</div>	<input class="nline" type="text" name="lname" id="lname1" tabindex="3" value="<?php echo $lname; ?>" placeholder="Last Name" required/></span><br><br>
<label for="male"></label><span><div class='div'>Nick Name</div><input type="text" class="nline" data-prompt-position="topLeft" name="nname" id="fname1" tabindex="4" value="<?php echo $nname; ?>" placeholder="Nick Name" /></span><br><br>

		<hr>
		




<label for="male">Address Info:</label><span><div class='div'>Address Line1</div> <input   class="nline" type="text" name="addr1" id="addr1" tabindex="7" value="<?php echo $add_line1; ?>" placeholder="Address Line1" /></span>
<span><div class='div'>Address Line2</div><input class="nline" type="text"  name="addr2" id="addr2" tabindex="8" value="<?php echo $add_line2; ?>" placeholder="Address Line2"/></span>
		
<span><div class='div'>Zip Code</div><input class="nline" type="text" name="zip" id="zip" placeholder="Zipcode" tabindex="9" value="<?php echo $zip;?>"/></span><br><br><label></label>
<span><div class='div'>City</div><input class="nline" type="text" name="city" id="city" tabindex="10" value="<?php echo $city; ?>" placeholder="City"/></span>

<span><div class='div'>State</div>
		<select  name="state" id="state1" tabindex="10">
		<?php 
			if($state=='') { $state = __DEFAULT_STATE__; }

			echo getStates($state);
		?>
		</select></span>
		<hr>



<label for="male">Contact Info:</label>
<span><div class='div'>Phone Num-1 *</div><select class='nline' name="phone1_type" id="phone_type"  tabindex="11">
<option value="">Select</option>
		<?php echo getPhoneType($phonetype1); ?>
		</select><?php
		if($phone1 != '')
			echo '<input class="phn"  type="text" name="phone1" tabindex="12" id="phone1" value="'.$phone1.'" placeholder="Phone Number" required >';
		else
			echo '<input class="phn"  type="text" name="phone1" tabindex="12" id="phone1"  placeholder="Phone Number" required>';
		?>
		</span>
		
<span><div class='div'>Phone Num-2 </div><select  class='nline' id="phone_type" name="phone2_type"  tabindex="13">
<option value="">Select</option>
		<?php echo getPhoneType($phonetype2); ?>
		</select><?php
		if($phone1 != '')
			echo '<input class="phn" type="text"  name="phone2" tabindex="14" id="phone2" value="'.$phone2.'"  placeholder="Phone Number"/>';
		else
			echo '<input class="phn" type="text"  name="phone2" tabindex="14" id="phone2"   placeholder="Phone Number"/>';
		?></span>
		

<span><div class='div'>Phone Num-3</div><select class='nline' id="phone_type" name="phone3_type"  tabindex="15">
<option value="">Select</option>
		<?php echo getPhoneType($phonetype3); ?>
		</select>	<?php
		if($phone1 != '')
		   echo '<input class="phn" type="text"  name="phone3" tabindex="16" id="phone3" value="'.$phone3.'"  placeholder="Phone Number"/>';
		else
			echo '<input class="phn"  type="text" name="phone3" tabindex="16" id="phone3"  placeholder="Phone Number"/>';
		?></span>
		
	<hr>








<label >Email Info:</label>
<span><div class='div'>Email Address-1*</div>		
		<input class="nline" type="text" tabindex="17" name="email1" id="email"  value="<?php echo $email1; ?>" onblur="validateEmail('email1', 'erremail1')"  placeholder="Email Address-1" required /></span>
	
		
		
		<div class="alert alert-error" id="erremail1" style="display:none;">
		Email Format should be
		<strong>test@test.com</strong>
		</div>
	<span><div class='div'>Email Address-2	</div>
		<input class="nline" type="text" name="email2" value="<?php echo $email2; ?>"  tabindex="18"id="email2" onblur="validateEmail('email2','erremail2')"  placeholder="Email Address-2"/></span>
		

		<div class="alert alert-error" id="erremail2" style="display:none;">
		Email Format should be
		<strong>test@test.com</strong>
		</div>

	
		
<br />
<hr>






<?php 
if(request_get('action')!="edit"){
?>
	
<label >Team Details:</label>		
<span><div class='div'>Team Type</div>
	<select class="nline" name="teamtype" id="lasst" tabindex="19"><option value="">Select</option>
		
		<?php
		
	

	$re=mysql_query("select name from tps_team_type where status=0");

	while($r=mysql_fetch_array($re))
	{        if($teamtype==$r['name'])
                echo "<option  selected>".ucfirst($r['name'])." </option>";
  		else
		echo "<option >".$r['name']."</option>";
	}
		 //echo getTeam($ldeal);
		 ?>
		</select></span>
<span><div class='div'>Team Position	</div>
	<select class="nline" name="teamposition"  id="position" tabindex="20"><option value="">Select</option>
		
		<?php
		
	$re=mysql_query("select display from tps_team_position where status=0");

	while($r=mysql_fetch_array($re))
	{        if($teamposition==$r['display'])
                echo "<option  selected>".ucfirst($r['display'])." </option>";
  		else
		echo "<option >".$r['display']."</option>";
	}
		 //echo getTeam($ldeal);
		 ?>
		</select></span>
<span><div class='div'>Status</div>
<select class="nline" name="teamstatus" id="lasst"   tabindex="21"><option value="">Select</option>
		
		<?php
	$re=mysql_query("select name from tps_team_status where status=0");

	while($r=mysql_fetch_array($re))
	{        if($teamstatus==$r['name'])
                echo "<option  selected>".ucfirst($r['name'])." </option>";
  		else
		echo "<option >".$r['name']."</option>";
	}
		 //echo getTeam($ldeal);
		 ?>
		</select></span><br /><br />
	 
<label></label>	
<span><div class='div'>Manager</div>
		<select class="nline" name="manager1"   id="lasst" tabindex="22"><option value="">Select</option>
		
		<?php
		
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

	$re=mysql_query("select fname,lname,userid from tps_users where status=1");

	while($r=mysql_fetch_array($re))
	{        if($manager1==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".$r['fname']." ".$r['lname']."</option>";
	}
		 //echo getTeam($ldeal);
		 ?>
		</select></span>
		
		
<span><div class='div'>Marketing Mgr</div>
<select class="nline" name="manager2" id="lasst" tabindex="23"><option value="">Select</option>
		
		<?php
		
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

	$re=mysql_query("select fname,lname,userid from tps_users where status=1");

	while($r=mysql_fetch_array($re))
	{        if($manager2==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".$r['fname']." ".$r['lname']."</option>";
	}
		 //echo getTeam($ldeal);
		 ?>
		</select></span>
<span><div class='div'>Marketing Asst</div>
<select class="nline" name="mmgr" id="lasst"  tabindex="24"><option value="">Select</option>
		
		<?php
		
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

	$re=mysql_query("select * from tps_users where status=1");

	while($r=mysql_fetch_array($re))
	{        if($mmgr==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".$r['fname']." ".$r['lname']."</option>";
	}
		 //echo getTeam($ldeal);
		 ?>
		</select></span><br /><br />
<hr>










<label >Recruit Details:</label>	
<span><div class='div'>Demo Date</div>
<input  type="text" class="nline" name="demodate" id="datepicker" value="<?php echo $demodate; ?>" tabindex="25" placeholder="Demo Date"   >  </span>	
<span><div class='div'>Recruited Type</div>
		<select class="nline" name="rtype" id="lasst"  tabindex="26"><option value="">Select</option>
		
		<?php
	$re=mysql_query("select * from tps_recruited_type where status=0");

	while($r=mysql_fetch_array($re))
	{        if($rtype==$r['name'])
                echo "<option  selected>".ucfirst($r['name'])." </option>";
  		else
		echo "<option >".$r['name']."</option>";
	}
		 //echo getTeam($ldeal);
		 ?>
		</select></span>
<span><div class='div'>Recruited By</div>
	<select class="nline" name="rby"  id="lasst" tabindex="27"><option value="">Select</option>
		
		<?php
		
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

	$re=mysql_query("select * from tps_users where status=1");

	while($r=mysql_fetch_array($re))
	{        if($rby==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".$r['fname']." ".$r['lname']."</option>";
	}
		 //echo getTeam($ldeal);
		 ?>
		</select></span> <br /><br />
<label ></label>
<span><div class='div'>Good Faith Deposit</div>
<input type="radio"<?php if($radio=='yes'){echo 'checked';}?> name="radio" tabindex="28" value="yes">&nbsp;YES&nbsp;&nbsp;<input class="nline newnew" type="radio" <?php if($radio=='no'){echo 'checked';}?> tabindex="29" name="radio" value="no">&nbsp;&nbsp;NO</span>
<span><div class='div'>Interested Recruit Date</div>
	
<input  type="text" class="nline" name="idate" id="datepicker1" class='date_picker'  value="<?php echo $idate; ?>"  tabindex="30" placeholder="Interested Recruit Date"></span>   
<span><div class='div'>In For Opportunity Date</div>
<input  type="text" class="nline" name="odate" id="datepicker2" class='date_picker' value="<?php echo $odate; ?>"  tabindex="31" placeholder="In For Opportunity Date"></span> 
<br><br>
<label ></label>
<span><div class='div'>In Training Date</div>

<input  type="text" class="nline"  name="tdate" id="datepicker3" class='date_picker' value="<?php echo $tdate; ?>"  tabindex="32" placeholder= "In Training Date"  >   </span>

<span><div class='div'>Graduation & Start  Date</div>

<input  type="text" name="gdate" class="nline" id="datepicker4" class='date_picker' value="<?php echo $gdate; ?>"  tabindex="33" placeholder="Graduation & Start  Date"  >   </span>

<br />

<hr>
<?php
}
?>









	<label>Personal Details:</label> 
	<span><div class='div'>Birth Date</div>
<input  type="text" name="bdate" class="nline" id="datepicker5" value="<?php echo $bdate; ?>"  tabindex="34" placeholder="Birth Date"  >   </span>
 
<span><div class='div'>SSN</div>
<input  class="nline firstli" type="text" name="ssn" id="ssn" tabindex="37"  value="<?php echo $ssn; ?>" placeholder="SSN" /></span>
<span><div class='div'>Driver's License No</div>
 <input  class="nline firstli" type="text" name="dln" id="addr1" tabindex="38"  value="<?php echo $dln; ?>" placeholder="Driver's License No" /></span>
	
<br /><br />
<label> </label> 
<span><div class='div'>Partner's First Name</div>
<input  class="nline" type="text" name="pfname" id="addr1" tabindex="39"  value="<?php echo $pfname; ?>" placeholder="Partner's First Name" /></span>
<span><div class='div'>Partner's Last Name</div>
 <input  class="nline" type="text" name="plname" id="addr1"    tabindex="40" value="<?php echo $plname; ?>" placeholder="Partner's Last Name" /></span>

<span><div class='div'>Partner's Phone No</div>
	<input  class="nline" type="text" name="pnum" id="pp" tabindex="41" value="<?php echo $pnum; ?>" placeholder="Partner's Phone No" /></span>

<br /><br />

<label ></label> 
<span><div class='div'>Partner's Email Address</div>
 
<input class="nline" type="text" tabindex="41" name="pemail" id="email3"   value="<?php echo $pemail; ?>" onblur="validateEmail('email1', 'erremail1')"  placeholder="Partner's Email Address"  /></span>
	
		
		<div class="alert alert-error" id="err" style="display:none;">
		Email Format should be
		<strong>test@test.com</strong>
		</div><br /><br />

<label ></label> 
	
<span class="divnew">Children's Name & Ages</span><br>
<label ></label> 
		 <textarea placeholder="Children's Name & Ages" rows="3" cols="60" tabindex="43" class="nline"  name="aboutchildren" id="comment1" tabindex="34"><?php echo $aboutchildren;?></textarea ><br /><br />

<label ></label> 
	
<span>Comments</span><br>
<label ></label>
		 <textarea placeholder="Comments" rows="3" cols="60" class="nline" tabindex="44" name="comment" id="comment1" tabindex="34"><?php echo $comment;?></textarea ><br /><br />
<label></label> <div id="uniform-photo" class="uploader" style="width:30%;margin-left:5px;display:none;" >
<input  tabindex="45" class="col-md-4" id="photo" name="photo" type="file"  style="width:60%;" style="display:none" onchange="showimagepreview(this);"> &nbsp;&nbsp;( .jpg , .jpeg , .gif , .png files only )</div>

<hr>
<?php 
if(request_get('action')!="edit" && request_get('action')!="adminedit"){
?>
<label >Login Details:</label>
<span><div class='div'>User Name *</div>
 <input  class="nline firstli" type="text" name="uname" id="uname"  tabindex="46"   placeholder="User Name" required/></span>
<span><div class='div'>Password *</div>
 <input  class="nline firstli" type="password" name="pwd" id="pwd"  tabindex="47"   placeholder="Password" required/></span>
<br /><br />
<hr>

<?php
} 
if(request_get('action')=="adminedit"){
?>
<input type="hidden" name="admin" value="1">
<?php
}
?>
<input type="hidden" id="pic" name="pic" value="<?php echo $image1; ?>">
<input type="hidden" name="lid" value="<?php echo $lid; ?>"> 
<input type="hidden" name="userid" value="<?php echo $userid; ?>"> 

<label ></label> 
<?php if(request_get('action')=="adminedit" || request_get('action')=="" || request_get('action')=="add" ){?>
<div class='checkbox'><div class="icheckbox_flat-aero" >
<input id="icheck1" class="icheck" type="checkbox" tabindex="48" <?php  if($dealer_flag==1){echo "checked";} ?> name="dealer" value='1'>
</div>&nbsp;&nbsp;Assign As  Dealer</div>
<?php 
}
if(request_get('action')=="edit" || request_get('action')=="adminedit"){
?>
<input type="submit" tabindex="49" class="btn btn-blue" value="Update" name="savemember" id="savelead" >
<?php
}else
{?>
<input type="submit" tabindex="50" class="btn btn-blue" value="Save"  name="addmember" id="addmember"  >
<?php
}
?><button type="button"  tabindex="51"class="btn btn-default " id="cancel"onclick='history.back(1)'>Cancel</button>
<hr>
</div>
	<input type="submit"  tabindex="52" class="btn btn-blue" value="Save"  name="addmember" id="hide"  >      
<input type="hidden"  value="<?php echo $leadid;?>"  name="leadid"   >  
<input type="hidden"  value="<?php echo $cus;?>"  name="cus"   >

	</form>

  </div>
    </div>
  </div>
 
</div>

</div>
</div> 
   
<?php
//include "lcas_footer.php";
?>
