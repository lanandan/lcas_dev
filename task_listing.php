<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "tasks.php";
$page_title = $site_name." -  Task Listing";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
include "js/task_listing.js";

?>
<script type="text/javascript">
function confirmDelete(tid,tsqid) 
{
	var agree=confirm("Are you sure do you want to delete this Task? ");
	if (agree){
		$("#spinner").show();
		var data = {
			    	type: 'taskdelete',
				taskid:tid,
				taskseqid:tsqid
			 }
		$.ajax({
		    		type: "POST",
		    		url: "task_actions.php",
		    		data: data,
		    		success: function(resp) {			
					$("#spinner").hide();
					bootbox.alert("Task Successfully Deleted",function(){
						window.location.href="task_listing.php";
					});	
				}
		});

	}
	else
		return false ;
}
</script>
<div id="spinner"></div>
<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>Task Listing&nbsp;&nbsp;<a class="btn btn-blue" href="createtask.php"><span>Create Task</span></a>
	</h3>
</div> 

      </div>
    </div>
  </div>

<div class="container">

<div class="box">

<div class="box-header"><span class="title">Task Listing</span>

</div>
<div class="box-content">
	
<div id="dataTables">	
	<?php
	$userid=get_session('LOGIN_ID');
	$usercreatedid=get_session('LOGIN_USERID');
	$today=date('Y-m-d H:i:s');
	$res=mysql_query("select * from tps_task_details where userid='".$userid."' and showtask='1' group by task_seqid order by id desc")or die(mysql_error());

	$table="<table width='100%' class='dTable responsive' id='appt'><thead>";
	$table.="<tr>";
	$table.="<th>Action</th>";
	$table.="<th><div>Contact Activity</div></th>
		<th><div>Who</div></th>
		<th><div>Contact Reason</div></th>
		<th><div>Assigned To</div></th>
		<th><div>Start Date</div></th>
		<th><div>Due Date</div></th>
		<th><div>Task Status</div></th>
		<th><div>Comments</div></th>
		</tr></thead><tbody>";
	$i=1;
	while($r=mysql_fetch_array($res))
	{
		$who=explode("-",$r['who']);
		
		$whos=$who[1];

		$tid=$r['id'];
		$tseqid=$r['task_seqid'];
		$taskby=$r['createdby'];

		$table.="<tr>";
		if($taskby==$usercreatedid)
		{
			$table.='<td align="center"><a title="Delete Task" href="#" onClick="return confirmDelete('.$r['id'].','.$r['task_seqid'].');"><img src="images/block.png" width="18px"/></a></td>';
		}else{
			$table.="<td>&nbsp;&nbsp;</td>";
		}

		$table.="<td>".$r['contact_activity']."</td>";
		$table.="<td>".$whos."</td>";
		$table.="<td>".$r['contact_reason']."</td>";
		$table.="<td>".$r['assigned_tousers']."</td>";
		$table.="<td>".date('m-d-Y h:i A',strtotime($r['start_date']))."</td>";
		$table.="<td>".date('m-d-Y h:i A',strtotime($r['due_date']))."</td>";	

		if($taskby==$usercreatedid)
		{
		/*$table.="<td><a href='createtask.php?action=edit&tid=$tid&tseqid=$tseqid&job=edit' style='text-decoration:underline;'>".$r['task_status']."</a></td>";*/
			$table.="<td>".$r['task_status']."</td>";
		}else{
	$table.="<td><a href='createtask.php?action=edit&tid=$tid&tseqid=$tseqid&job=taskupd' style='text-decoration:underline;'>".$r['task_status']."</a></td>";
		}

		$table.="<td>".$r['comments']."</td>";			
		$table.='</tr>';
		$i++;
	}
	$table.="</tbody></table>";
		
	echo $table;
		
	?>
</div> 

      </div>
	
    </div>
	
   </div>

 </div> 
 </div>

<?php


include "lcas_footer.php";

?>
