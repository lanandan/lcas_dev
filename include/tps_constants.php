<?php
	$site_name = $_SERVER["SERVER_NAME"];

	define("__USER_TYPE__", 1);
	define("__ADMIN_TYPE__", 2);
	
	$tps_records_per_page = 30;
define("__DEFAULT_COMPANY__", "LCAS");
	//question
	define("__QUESTION_YES_NO__", "1");
	define("__QUESTION_MULTI_ANSWER__", "3");
	define("__QUESTION_MULTI_CHOICE__", "2");
	define("__QUESTION_RATING__", "4");
	define("__QUESTION_SHORT_ANS__", "5");
	define("__QUESTION_LONG_ANS__", "6");
	define("__QUESTION_DROP_DOWN__", "7");
	
	// Eror Logs
	define("__CRITICAL__", "1");
	define("__WARNING__", "2");
	define("__INFO__", "3");

	//appointment timing 
	define("__DEFAULT_TIME__", "50400");
	define("__APPOINTMENT_HRS__", "10800");
	
	// tps_genfunctions.php title creation array
	//remove get from db
	$preftitle=array();
	$preftitle[0]="Select";
	$preftitle[1]="Mr.";
	$preftitle[2]="Mrs.";
	$preftitle[3]="Miss.";

	// tps_genfunctions.php phone type creation array
	//remove get from db
	$phonetypearr=array();
	$phonetypearr[0]="Home";
	$phonetypearr[1]="Work";
	$phonetypearr[2]="Mobile";

	//lead type default value for add_leads.php
	define("__DEFAULT_LTYPE__", "B");
	define("__DEFAULT_LSUBTYPE__", "WC");
	define("__DEFAULT_GTYPE__","Select");

	//STATE default value for add_leads.php
	define("__DEFAULT_STATE__", "PA");

	//LEAD STATUS default value for add_leads.php
	define("__DEFAULT_LSTATUS__", "Confirmed");

	//STATE default value for add_leads.php
	define("__DEFAULT_LRESULT__", "Purchased");


	// PERMISSIONS values are in mysql table column names

	define("__ADDONE__", "add_one");
	define("__EDITONE__", "edit_one");
	define("__DELETEONE__", "delete_one");
	define("__BULKUPDATE__", "bulk_update");
	define("__BULKDELETE__", "bulk_delete");

	//Array for Lead Sub Type
	$arrleadsubtype = array();

	
?>
