-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2014 at 03:20 PM
-- Server version: 5.5.37-0ubuntu0.13.10.1
-- PHP Version: 5.5.3-1ubuntu2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lcas_db`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `live_leads`
--
CREATE TABLE IF NOT EXISTS `live_leads` (
`id` int(11)
,`leadid` int(11)
,`uid` int(11)
,`title1` varchar(8)
,`fname1` varchar(128)
,`lname1` varchar(128)
,`title2` varchar(8)
,`fname2` varchar(128)
,`lname2` varchar(128)
,`add_line1` varchar(128)
,`add_line2` varchar(128)
,`city` varchar(128)
,`state` varchar(128)
,`zip` varchar(16)
,`country` varchar(128)
,`phonetype1` varchar(8)
,`phone1` varchar(128)
,`phonetype2` varchar(8)
,`phone2` varchar(128)
,`phonetype3` varchar(8)
,`phone3` varchar(128)
,`email1` varchar(128)
,`email2` varchar(128)
,`email_opt_in` varchar(128)
,`time_contact` varchar(64)
,`lead_status` varchar(128)
,`lead_asst` varchar(512)
,`set_asst` varchar(128)
,`lead_type` varchar(128)
,`lead_subtype` varchar(64)
,`script_id` int(11)
,`answer_id` int(11)
,`referred_by` varchar(128)
,`relationship` varchar(128)
,`lead_dealer` varchar(128)
,`dealer` varchar(128)
,`lead_in` varchar(1028)
,`oktocall` date
,`lead_result` varchar(128)
,`gift` varchar(128)
,`apptresult` varchar(512)
,`comments` varchar(128)
,`referral_data` varchar(1028)
,`customer_flag` int(11)
,`delete_flag` int(11)
,`referral_flag` int(11)
,`recruit_flag` int(11)
,`form_type` varchar(128)
,`lead_qualification` varchar(128)
,`door_gift_rewards` int(11)
,`referral_rewards` int(11)
,`bonus_rewards` int(11)
,`show_rewards` int(11)
,`rewards_redeemed` int(11)
,`is_referred` tinyint(4)
,`is_surveyed` tinyint(4)
,`surveyed_script` int(11)
,`surveyed_questions` varchar(128)
,`modified` timestamp
,`created_by` varchar(128)
,`created` timestamp
,`modified_by` varchar(128)
,`aptstart` timestamp
,`aptend` timestamp
,`setdate` timestamp
,`setby` int(30)
,`appttype` varchar(50)
,`assignedto` int(30)
);
-- --------------------------------------------------------

--
-- Structure for view `live_leads`
--
DROP TABLE IF EXISTS `live_leads`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `live_leads` AS select `l`.`id` AS `id`,`l`.`leadid` AS `leadid`,`l`.`uid` AS `uid`,`l`.`title1` AS `title1`,`l`.`fname1` AS `fname1`,`l`.`lname1` AS `lname1`,`l`.`title2` AS `title2`,`l`.`fname2` AS `fname2`,`l`.`lname2` AS `lname2`,`l`.`add_line1` AS `add_line1`,`l`.`add_line2` AS `add_line2`,`l`.`city` AS `city`,`l`.`state` AS `state`,`l`.`zip` AS `zip`,`l`.`country` AS `country`,`l`.`phonetype1` AS `phonetype1`,`l`.`phone1` AS `phone1`,`l`.`phonetype2` AS `phonetype2`,`l`.`phone2` AS `phone2`,`l`.`phonetype3` AS `phonetype3`,`l`.`phone3` AS `phone3`,`l`.`email1` AS `email1`,`l`.`email2` AS `email2`,`l`.`email_opt_in` AS `email_opt_in`,`l`.`time_contact` AS `time_contact`,`l`.`lead_status` AS `lead_status`,`l`.`lead_asst` AS `lead_asst`,`l`.`set_asst` AS `set_asst`,`l`.`lead_type` AS `lead_type`,`l`.`lead_subtype` AS `lead_subtype`,`l`.`script_id` AS `script_id`,`l`.`answer_id` AS `answer_id`,`l`.`referred_by` AS `referred_by`,`l`.`relationship` AS `relationship`,`l`.`lead_dealer` AS `lead_dealer`,`l`.`dealer` AS `dealer`,`l`.`lead_in` AS `lead_in`,`l`.`oktocall` AS `oktocall`,`l`.`lead_result` AS `lead_result`,`l`.`gift` AS `gift`,`l`.`apptresult` AS `apptresult`,`l`.`comments` AS `comments`,`l`.`referral_data` AS `referral_data`,`l`.`customer_flag` AS `customer_flag`,`l`.`delete_flag` AS `delete_flag`,`l`.`referral_flag` AS `referral_flag`,`l`.`recruit_flag` AS `recruit_flag`,`l`.`form_type` AS `form_type`,`l`.`lead_qualification` AS `lead_qualification`,`l`.`door_gift_rewards` AS `door_gift_rewards`,`l`.`referral_rewards` AS `referral_rewards`,`l`.`bonus_rewards` AS `bonus_rewards`,`l`.`show_rewards` AS `show_rewards`,`l`.`rewards_redeemed` AS `rewards_redeemed`,`l`.`is_referred` AS `is_referred`,`l`.`is_surveyed` AS `is_surveyed`,`l`.`surveyed_script` AS `surveyed_script`,`l`.`surveyed_questions` AS `surveyed_questions`,`l`.`modified` AS `modified`,`l`.`created_by` AS `created_by`,`l`.`created` AS `created`,`l`.`modified_by` AS `modified_by`,`e`.`start` AS `aptstart`,`e`.`end` AS `aptend`,`e`.`created_at` AS `setdate`,`e`.`createdby` AS `setby`,`e`.`appttype` AS `appttype`,`e`.`assignedto` AS `assignedto` from (`tps_lead_card` `l` left join `live_tps_events` `e` on((`l`.`id` = `e`.`lead_id`))) where (`l`.`delete_flag` = '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
