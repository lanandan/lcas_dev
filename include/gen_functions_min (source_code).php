<?php

function get_session($var)
{
	if (isset($_SESSION[$var])) {
		return $_SESSION[$var];
	}
	else {
		return '';
	}
}

function set_session($var, $value)
{
	$_SESSION[$var] = $value;
}

function unset_session($var)
{
	if(isset($_SESSION[$var])) {
		unset($_SESSION[$var]);
	}

}

function validate_login()
{
	
	if( get_session('LOGGED_IN') == '' || get_session('LOGGED_IN') == '0' )
	{
		header('Location: login.php');
		exit();
	}
	
	return;
}

function safe_sql_nq( $val )
{
	  $val = addslashes( htmlentities( $val, ENT_QUOTES, 'UTF-8') );
	  return $val;
}

function safe_sql( $val )
{
	$val = addslasher(htmlentities( $val.ENT_QUOTES, 'UTF-8') );
	return $val;
}

function request_get($param, $type = 1, $default_value = "") {
	if($type == 1){
		if(isset($_REQUEST[$param]) && trim($_REQUEST[$param]) != "") 
			return htmlentities(addslashes(trim($_REQUEST[$param])));
	}
	if($type == 2){
		return $_REQUEST[$param];
	}
	if($default_value !== "")
		return $default_value;
	
	return "";
}

/*function tps_log_error($critical, $file_name, $line_num, $description)
{
	$file_name = safe_sql_nq( $file_name );
	$description = safe_sql_nq( $description );
	$sql_log_qry = "insert into tps_error_log ( type, filename, line_num, description, created) values (  '$critical', '$file_name', '$line_num', '$description', now() )";
	mysql_query($sql_log_qry);
	return;
}*/

function tps_log_error($critical, $file_name, $line_num, $actiontype, $userid, $description)
{
	$file_name = safe_sql_nq( $file_name );
	$description = safe_sql_nq( $description );
	$actiontype = safe_sql_nq( $actiontype );
	$userid = safe_sql_nq( $userid );

	$sql_log_qry = "insert into tps_error_log ( type, filename, line_num, actiontype, userid, description, created) values ( '$critical', '$file_name', '$line_num', '$actiontype', '$userid', '$description', now() )";

	mysql_query($sql_log_qry);
	return;
}

?>
