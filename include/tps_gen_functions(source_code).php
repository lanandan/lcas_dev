<?php

date_default_timezone_set("Asia/Kolkata");

function display_tree_for_user($userid)
{
	global $db_inst1;

	if ($userid == '') {
		return null;
	}
	$string='';
	$sql = "select * from tps_users where id = '".$userid."' ";
	$result=mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_array($result);
	$level = $row['level'];
	if ($level > 1) {
		if ($row['level1'] > 0 ) {
			$string .= str_repeat('&nbsp;', 5);
			$user_name = "CHA".$row['level1'];
			$string .= get_user_profile_details_new($user_name);
			$string .= "</li>";
		}
	}

	if ($level > 2) {
		if ($row['level2'] > 0 ) {
			$string .= str_repeat('&nbsp;', 10);
			$user_name = "DIR".$row['level2'];
			$string .= get_user_profile_details_new($user_name);

		}
	}
	
	if ($level > 3) {
		if ($row['level3'] > 0 ) {
			$string .= str_repeat('&nbsp;', 15);
			$user_name = "EMP".$row['level3'];
			$string .= get_user_profile_details_new($user_name);
		}
	}

	if ($level > 4) {
		if ($row['level4'] > 0 ) {
			$string .= str_repeat('&nbsp;', 20);
			$user_name = "MEM".$row['level4'];
			$string .= get_user_profile_details_new($user_name);
		}
	}
	

	$ulcount =0;
	return $string;

}
function display_tree_for_user_addition($userid)
{

	global $db_inst1;

	if ($userid == '') {
		return null;
	}
	$string='';
	$sql = "select * from tps_users where id = '".$userid."' ";
	$result=mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_array($result);
	$level = $row['level'];

	if ($level >= 1) {
		if ($row['level1'] > 0 ) {
			$string .= str_repeat('&nbsp;', 5);
			$user_name = "CHA".$row['level1'];
                        $image=$row['image1'];
			$string .= get_user_profile_details_new($user_name);
			$string .= "</li>";
			
		}
	}

	if ($level >= 2) {
		if ($row['level2'] > 0 ) {
			$string .= str_repeat('&nbsp;', 10);
			$user_name = "DIR".$row['level2'];
			$image=$row['image1'];
			$string .= get_user_profile_details_new($user_name);
			
		}
	}
	
	if ($level >= 3) {
		if ($row['level3'] > 0 ) {
			$string .= str_repeat('&nbsp;', 15);
			$user_name = "EMP".$row['level3'];
			$image=$row['image1'];
			$string .= get_user_profile_details_new($user_name);
		}
	}

	if ($level >= 4) {
		if ($row['level4'] > 0 ) {
			$string .= str_repeat('&nbsp;', 20);
			$user_name = "MEM".$row['level4'];
			$image=$row['image1'];
			$string .= get_user_profile_details_new($user_name);
		}
	}
	
	$ulcount =0;
	return $string;


}

function get_user_profile_details_new($user_name, $with_phone=1)
{
	global $db_inst1;

	if ($user_name == '') {
		return null;
	}
        
	$sql = "select * from tps_users where dummyid = '".$user_name."'";
	$result=mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_array($result);
	$parentid=$row['parentid'];
        $image=$row['image1'];
	$user_string = '';
	$user_string .= '<table border="0" cellspacing="2" cellpadding="2"><tr><td>';
	$image_file_name = "images/upload/thumb/$image";
	if ( file_exists($image_file_name)) {
		$user_img = $image_file_name;
	}
	else {
		$user_img = 'images/upload/default.jpeg';
	}
	$user_string .= '<img src="'.$user_img.'" width="50px" height="50px">';
	$user_string .= '</td><td valign="top">';
	$user_string .= $row['username'].'<br/>'.$row['nameprefix'].' '.ucfirst($row['fname']).' '.$row['lname'].'';
	if ($with_phone == 1) {
		$user_string .= '<br/><img src="images/mobile.jpg" width="15" height="15">&nbsp;&nbsp;'.$row['phone1'];
	}
		$user_string .= '</td></tr></table>';

	return $user_string;
}

function send_mail_with_smtp($address, $subject, $msg) {
		
			//global $gbl_do_not_replay_mail_id;

		require_once('phpmailer/class.phpmailer.php');

		$mail = new PHPMailer();

		$mail->IsSMTP(); // telling the class to use SMTP
		//$mail->Host       = "mail.verizon.com"; // SMTP server
		$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
		$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
		$mail->Port       = "465";                   // set the SMTP port for the GMAIL server
		$mail->Username   = "projects@grinfotech.com";  // GMAIL username
		$mail->Password   = "Bgt56yhN@";            // GMAIL password

		//$mail->SetFrom($gbl_do_not_replay_mail_id, '');


		$mail->Subject    = $subject;


		$mail->MsgHTML($msg);

		//var_dump($address);


		$mail->AddAddress($address);

		if($mail->Send())
				return true;
		else
				return false;
		}

function makeThumbnails($updir, $img, $id,$MaxWe,$MaxHe){
    $arr_image_details = getimagesize($img); 
    $width = $arr_image_details[0];
    $height = $arr_image_details[1];

    $percent = 100;
    if($width > $MaxWe) $percent = floor(($MaxWe * 100) / $width);

    if(floor(($height * $percent)/100)>$MaxHe)  
    $percent = (($MaxHe * 100) / $height);

    if($width > $height) {
        $newWidth=$MaxWe;
        $newHeight=round(($height*$percent)/100);
    }else{
        $newWidth=round(($width*$percent)/100);
        $newHeight=$MaxHe;
    }

    if ($arr_image_details[2] == 1) {
        $imgt = "ImageGIF";
        $imgcreatefrom = "ImageCreateFromGIF";
    }
    if ($arr_image_details[2] == 2) {
        $imgt = "ImageJPEG";
        $imgcreatefrom = "ImageCreateFromJPEG";
    }
    if ($arr_image_details[2] == 3) {
        $imgt = "ImagePNG";
        $imgcreatefrom = "ImageCreateFromPNG";
    }


    if ($imgt) {
        $old_image = $imgcreatefrom($img);
        $new_image = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresized($new_image, $old_image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

       $imgt($new_image, $updir."".$id.".jpg");
        return;    
    }
}


function get_session($var)
{
	if (isset($_SESSION[$var])) {
		return $_SESSION[$var];
	}
	else {
		return '';
	}
}

function set_session($var, $value)
{
	$_SESSION[$var] = $value;
}

function unset_session($var)
{
	if(isset($_SESSION[$var])) {
		unset($_SESSION[$var]);
	}

}

function validate_login()
{
	
	
	if( get_session('LOGGED_IN') == '' || get_session('LOGGED_IN') == '0' )
	{
		header('Location: login.php');
		exit();
	}
	
	return;
}

function safe_sql_nq( $val )
{
	  $val = addslashes( htmlentities( $val, ENT_QUOTES, 'UTF-8') );
	  return $val;
}

function safe_sql( $val )
{
	$val = addslasher(htmlentities( $val.ENT_QUOTES, 'UTF-8') );
	return $val;
}

function request_get($param, $type = 1, $default_value = "") {
	if($type == 1){
		if(isset($_REQUEST[$param]) && trim($_REQUEST[$param]) != "") 
			return htmlentities(addslashes(trim($_REQUEST[$param])));
	}
	if($type == 2){
		return $_REQUEST[$param];
	}
	if($default_value !== "")
		return $default_value;
	
	return "";
}

function tps_log_error($critical, $file_name, $line_num, $description)
{
	$file_name = safe_sql_nq( $file_name );
	$description = safe_sql_nq( $description );
	$sql_log_qry = "insert into tps_error_log ( type, filename, line_num, description, created) values (  '$critical', '$file_name', '$line_num', '$description', now() )";
	mysql_query($sql_log_qry);
	return;
}


function create_guid($namespace = '') {    
    static $guid = '';
    $uid = uniqid("", true);
    $data = $namespace;
    $data .= $_SERVER['REQUEST_TIME'];
    $data .= $_SERVER['HTTP_USER_AGENT'];
    $data .= $_SERVER['LOCAL_ADDR'];
    $data .= $_SERVER['LOCAL_PORT'];
    $data .= $_SERVER['REMOTE_ADDR'];
    $data .= $_SERVER['REMOTE_PORT'];
    $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
    $guid = '{' .  
            substr($hash,  0,  8) .
            '-' .
            substr($hash,  8,  4) .
            '-' .
            substr($hash, 12,  4) .
            '-' .
            substr($hash, 16,  4) .
            '-' .
            substr($hash, 20, 12) .
            '}';
    return $guid;
}


function getHostUrlComplete() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}


function getHostUrl() {
	$pageURL = 'http';

	if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"];
	}
	return $pageURL;
}

function get_pagination_question($total_records,$current_page,$records_per_page,$lower_record,$upper_record)
{
	$pagin = "<span class='pagination'>";
	if ($total_records <= 0) {
		$pagin .= "</span>";
		return $pagin;
	}

	$total_pages = $total_records/$records_per_page;

	if($upper_record > $total_records)
	{
		$upper_record = $total_records;
	}

	if($total_records%$records_per_page!=0)
	{
		$total_pages = floor($total_pages+1);
	}
	
	$previous_page = $current_page - 1;
	$next_page = $current_page+1;
	
	if($previous_page < 1){}
	else if($previous_page == 1)
	{
		$pagin .= "<a href='?qn_id=".$_GET['qn_id']."&page=$previous_page'>&lt;Previous</a> ";
	}
	else
	{
		$pagin .= "<a href='?qn_id=".$_GET['qn_id']."&page=1'>&lt;&lt;Start</a> ";
		$pagin .= "<a href='?qn_id=".$_GET['qn_id']."&page=$previous_page'>&lt;Previous</a> ";
	}
	$pagin .= " ".$lower_record." - ".$upper_record." of ".$total_records." ";
	
	if($next_page > $total_pages){}
	else if($next_page == $total_pages)
	{
		$pagin .= "<a href='?qn_id=".$_GET['qn_id']."&page=$next_page'>Next></a> ";
	}
	else
	{
		$pagin .= "<a href='?qn_id=".$_GET['qn_id']."&page=$next_page'>Next></a> ";
		$pagin .= "<a href='?qn_id=".$_GET['qn_id']."&page=$total_pages'>Last>></a> ";
	}
	$pagin .= "</span>";
	return $pagin;
}


function get_pagination($total_records,$current_page,$records_per_page,$lower_record,$upper_record)
{
	$pagin = "<span class='pagination'>";
	if ($total_records <= 0) {
		$pagin .= "</span>";
		return $pagin;
	}

	$total_pages = $total_records/$records_per_page;

	if($upper_record > $total_records)
	{
		$upper_record = $total_records;
	}

	if($total_records%$records_per_page!=0)
	{
		$total_pages = floor($total_pages+1);
	}
	
	$previous_page = $current_page - 1;
	$next_page = $current_page+1;
	
	if($previous_page < 1){}
	else if($previous_page == 1)
	{
		$pagin .= "<a href='?page=$previous_page'>&lt;Previous</a> ";
	}
	else
	{
		$pagin .= "<a href='?page=1'>&lt;&lt;Start</a> ";
		$pagin .= "<a href='?page=$previous_page'>&lt;Previous</a> ";
	}
	$pagin .= " ".$lower_record." - ".$upper_record." of ".$total_records." ";
	
	if($next_page > $total_pages){}
	else if($next_page == $total_pages)
	{
		$pagin .= "<a href='?page=$next_page'>Next></a> ";
	}
	else
	{
		$pagin .= "<a href='?page=$next_page'>Next></a> ";
		$pagin .= "<a href='?page=$total_pages'>Last>></a> ";
	}
	$pagin .= "</span>";
	return $pagin;
}


function display_time_diff_format($var_date,$span_flag=0)
{
	$str_datetime = "";
	if ($var_date > 100) {
		$dateDiff = time() - $var_date;
		$fullDays = floor($dateDiff/(60*60*24));
		$fullHours = floor(($dateDiff-($fullDays*60*60*24))/(60*60));
		$fullMinutes = floor(($dateDiff-($fullDays*60*60*24)-($fullHours*60*60))/60);
		$fullSeconds = $dateDiff%60;
		
		if($fullDays == 0 && $fullHours == 0 && $fullMinutes == 0) {
			$str_datetime = $fullSeconds ." seconds ago";
		}
		else if($fullDays == 0 && $fullHours == 0) {
			$str_datetime = $fullMinutes ." minutes ago";
		}
		else if($fullDays == 0 ) {
			$str_datetime = $fullHours ." hours ago";
		}
		else if( $fullDays<30 ) {
			$str_datetime = $fullDays ." days ago";
		}
		else {
			$str_datetime = date("M j, Y H:i A", $var_date);
		}
		
		if ($span_flag == true)
		{
			$str_datetime_span_disp = date("M j, Y H:i A", $var_date);
			$temp = "<span title='$str_datetime_span_disp'>$str_datetime</span>";
			$str_datetime = $temp;
		}
		return $str_datetime;
	}
	else {
		return "--";
	}
}
function fmt_db_date_time($time_var=0)
{
	if ($time_var==0) {
		$time_var = time();
	}
	return date('Y-m-d H:i:s',$time_var);
}
function get_rand_id($length)
{
	if($length>0) 
	{ 
		$rand_id = '';
		for($i=1; $i <= $length; $i++){
			mt_srand((double)microtime() * 1000000);
			$num = mt_rand(1,36);
			$rand_id .= assign_rand_value($num);
		}
	}
	return $rand_id;
}

function assign_rand_value($num)
{
		$var = array('0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
		return $var[$num];
}


function getNamePrefix($sel)
{
	$sql="select * from tps_name_prefix where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	while($row = mysql_fetch_array($result)){
	if ($row['name']==$sel  ){

		$string .='<option value="'.$row['name'].'" selected="selected">'.$row['name'].'</option>';
	}
	else{
		$string .='<option value="'.$row['name'].'">'.$row['name'].'</option>';
	}
	}
	return $string;
}


function getStates($sel)
{
	$sql="select * from statedetails";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	while($row = mysql_fetch_array($result)){
	if ($row['stateabbr']==$sel  ){

		$string .='<option value="'.$row['stateabbr'].'" selected="selected">'.$row['stateabbr'].'</option>';
	}
	else{
		$string .='<option value="'.$row['stateabbr'].'">'.$row['stateabbr'].'</option>';
	}
	}
	return $string;
}


function getPhoneType($sel)
{
	$res=mysql_query("select * from tps_phone_type where status=0");while($row=mysql_fetch_array($res)){
		if ($sel==$row['display']){
			$string .='<option value="'.$row['display'].'" selected="selected">'.$row['display'].'</option>';
		}
		else{
			$string .='<option value="'.$row['display'].'">'.$row['display'].'</option>';
		}

	}
	
	return $string;
}
function getDDTime($nSeconds)
{
	$string = '';
	for ($i=20; $i < 45; $i++) {
		$y = $i*60*30;
		if (($nSeconds) == $y) {
			$string .= '<option selected="selected" value="'.$y.'" >'.gmdate("h:i A", $y).'</option>';	
		}
		else {
			$string .= '<option value="'.$y.'" >'.gmdate("h:i A", $y).'</option>';	
		}
	}
	return $string;
}
function getDDBestTime($sel)
{
	$sql="select * from tps_best_time_to_contact where status='0'";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){
		if($sel==$row['name'])
			$string .='<option value="'.$row['name'].'" selected="selected">'.$row['name'].'</option>';
		else
			$string .='<option value="'.$row['name'].'" >'.$row['name'].'</option>';
	
	}
	return $string;	
}
function getLeadType($sel)
{
	$sql="select * from tps_lead_type where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){
		if($sel==$row['type_displayname'])
			$string .='<option class="'.$row['type_displayname'].'"value="'.$row['type_displayname'].'" selected>'.$row['type_displayname'].'</option>';
		else
			$string .='<option class="'.$row['type_displayname'].'"value="'.$row['type_displayname'].'" >'.$row['type_displayname'].'</option>';
	
	}
	return $string;	
}
function getLeadSubType($sel, $leadsel)
{
	$sql="select * from tps_lead_type where lead_type='".$leadsel."'";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';

	while($row = mysql_fetch_array($result)){
		if($sel==$row['lead_type']) 
			$string .='<option value="'.$row['lead_subtype'].'" selected>'.$row['subtype_displayname'].'</option>';
		else
			$string .='<option value="'.$row['lead_subtype'].'" >'.$row['subtype_displayname'].'</option>';

	}
	
	return $string;	
}
function getLeadSubType2($leadsel,$sel){$string='';
$sql="select * from tps_lead_subtype where status=0";
	$result=mysql_query($sql) or die(mysql_error());while($row = mysql_fetch_array($result)){
	if($sel==$row['parent']){if($leadsel==$row['subtype_displayname']){
	$string .='<option value="'.$row['subtype_displayname'].'"class="'.$row['parent'].' showsubtype" selected>'.$row['subtype_displayname'].'</option>';}else{
	$string .='<option value="'.$row['subtype_displayname'].'"class="'.$row['parent'].' showsubtype" >'.$row['subtype_displayname'].'</option>';
	}
	}
    else{
	$string .='<option value="'.$row['subtype_displayname'].'"class="'.$row['parent'].' showsubtype"style="display:none;" >'.$row['subtype_displayname'].'</option>';
	}}
	return $string;	
	}
	function getLeadSubType3($leadsel,$sel){echo "<script>var asdf='";
$sql="select * from tps_lead_subtype where status=0";
	$result=mysql_query($sql) or die(mysql_error());while($row = mysql_fetch_array($result)){
	if($sel==$row['parent']){if($leadsel==$row['subtype_displayname']){
	echo '<option value="'.$row['subtype_displayname'].'"class="'.$row['parent'].' showsubtype" selected>'.$row['subtype_displayname'].'</option>';}else{
	echo '<option value="'.$row['subtype_displayname'].'"class="'.$row['parent'].' showsubtype" >'.$row['subtype_displayname'].'</option>';
	}
	}
    else{
	echo '<option value="'.$row['subtype_displayname'].'"class="'.$row['parent'].' showsubtype" >'.$row['subtype_displayname'].'</option>';
	}}
	echo "';</script>";
	}
function getGift($leadsel,$giftsel)
{
	
	$sql="select * from tps_lead_gift where lead_type= '".$leadsel."'";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	while($row = mysql_fetch_array($result)){
		if($giftsel==$row['gift'])
			$string .='<option value="'.$row['gift'].'" selected="selected">'.$row['gift'].'</option>';
		else
			$string .='<option value="'.$row['gift'].'" >'.$row['gift'].'</option>';
	
	}
	return $string;	
}
function getGift2($ltype,$lsubtype,$gift)
{
	
	$sql="select * from tps_lead_gift";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	while($row = mysql_fetch_array($result)){ 
	$result3=mysql_query("select type_displayname from tps_lead_type where type_displayname='".$row['lead_type']."'");$row3=mysql_fetch_array($result3);$result4=mysql_query("select subtype_displayname from tps_lead_subtype where subtype_displayname='".$row['lead_subtype']."'");$row4=mysql_fetch_array($result4);
			if($ltype==$row3['type_displayname'] && $lsubtype==$row4['subtype_displayname']){
			if($gift==$row['gift']){
			$string .='<option value="'.$row['gift'].'" class="'.$row3['type_displayname'].' '.$row4['subtype_displayname'].' hide-gift" selected>'.$row['gift'].'</option>';
			}else{
			$string .='<option value="'.$row['gift'].'" class="'.$row3['type_displayname'].' '.$row4['subtype_displayname'].' hide-gift">'.$row['gift'].'</option>';}
			}else{
			$string .='<option value="'.$row['gift'].'" class="'.$row3['type_displayname'].' '.$row4['subtype_displayname'].' hide-gift"style="display:none;">'.$row['gift'].'</option>';
			}
	
	}
	return $string;	
}
function getGift3($ltype,$lsubtype,$gift)
{echo "<script>var asdf2='";
	
	$sql="select * from tps_lead_gift where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	
	while($row = mysql_fetch_array($result)){ 
	$result3=mysql_query("select type_displayname from tps_lead_type where type_displayname='".$row['lead_type']."'");$row3=mysql_fetch_array($result3);$result4=mysql_query("select subtype_displayname from tps_lead_subtype where subtype_displayname='".$row['lead_subtype']."'");$row4=mysql_fetch_array($result4);
			if($ltype==$row3['type_displayname'] && $lsubtype==$row4['subtype_displayname']){
			if($gift==$row['gift']){
			echo '<option value="'.$row['gift'].'" class="'.$row3['type_displayname'].' '.$row4['subtype_displayname'].' hide-gift" selected>'.$row['gift'].'</option>';
			}else{
			echo '<option value="'.$row['gift'].'" class="'.$row3['type_displayname'].' '.$row4['subtype_displayname'].' hide-gift">'.$row['gift'].'</option>';}
			}else{
			echo '<option value="'.$row['gift'].'" class="'.$row3['type_displayname'].' '.$row4['subtype_displayname'].' hide-gift">'.$row['gift'].'</option>';
			}
	
	}
	echo "';</script>";
}
function getLeadStatus($sel)
{
	$sql="select * from tps_lead_status where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){
		if($sel==$row['name'])
			$string .='<option value="'.$row['name'].'" selected="selected">'.$row['display'].'</option>';
		else
			$string .='<option value="'.$row['name'].'" >'.$row['display'].'</option>';
	
	}
	return $string;	
}

function getDisplayOrderNum($sel)
{
	$string = '';
	for ($i=0; $i < 100; $i++) {
		if($sel==$i)
			$string .='<option value="'.$i.'" selected="selected">'.$i.'</option>';
		else
			$string .='<option value="'.$i.'" >'.$i.'</option>';
	
	}
	return $string;	
}


function getLeadResult($sel)
{
	$sql="select * from tps_lead_result";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){
		if($sel==$row['name'])
			$string .='<option value="'.$row['name'].'" selected="selected">'.$row['display'].'</option>';
		else
			$string .='<option value="'.$row['name'].'" >'.$row['display'].'</option>';
	
	}
	return $string;	
}

function getScriptNameList($script_id)
{
	$sql="select id, name, no_of_questions from tps_scripts where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	while($row = mysql_fetch_array($result)){
		if ($script_id == $result['id']) {
			$string .='<option value="'.$row['id'].'" >'.$row['name'].' - #Questions='.$row['no_of_questions'].'</option>';
		}
		else {
			$string .='<option value="'.$row['id'].'"  selected="selected" >'.$row['name'].' - #Questions='.$row['no_of_questions'].')</option>';
		}
	}
	return $string;	
}


function copyFromScript ($oldScriptId, $newScriptId)
{
	$timestamp = time();
	$sql="select * from tps_questions where script_id='".$oldScriptId."'";
	$result=mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array($result)){
		$inssql = " insert into tps_questions set ".
			" `script_id` = '". $newScriptId ."', ".
			" `name` = '". $row['name'] ."', ".
			" `type` = '". $row['type'] ."', ".
			" `status` = '". $row['status'] ."', ".
			" `answer_option1` = '". $row['answer_option1'] ."', ".
			" `answer_option2` = '". $row['answer_option2'] ."', ".
			" `answer_option3` = '". $row['answer_option3'] ."', ".
			" `answer_option4` = '". $row['answer_option4'] ."', ".
			" `answer_option5` = '". $row['answer_option5'] ."', ".
			" `answer_option6` = '". $row['answer_option6'] ."', ".
			" `created` = '". $timestamp ."', ".										
			" `modified` = '". $timestamp ."', " . 
			"  `createdby`= '". get_session('DISPLAY_NAME') ."',".
			" `modifiedby`= '". get_session('DISPLAY_NAME')."'" ;
		mysql_query($inssql) or die(mysql_error());
	}
	updateQuesionsTotals($newScriptId);
}

function updateQuesionsTotals($scriptId)
{
	$sql="select count(*) as total from tps_questions where script_id='".$scriptId."' and status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_array($result);

	$sql= "update tps_scripts set ".
		" `no_of_questions` = '". $row['total'] ."' ".
		" where id = '".$scriptId."' " ;   

	mysql_query($sql) or die(mysql_error());		
	return;
}

function getManager($sel)
{
	$sql="select * from tps_users";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){
		if($sel==$row['username'])
			$string .='<option value="'.$row['username'].'" selected="selected">'.$row['fname'].' '.$row['lname'].'</option>';
		else
			$string .='<option value="'.$row['username'].'">'.$row['fname'].' '.$row['lname'].'</option>';
	
	}
	return $string;	
}
function getTeam($sel)
{
	$sql="select * from tps_team where status=0";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){
		if($sel==$row['teamname'])
			$string .='<option value="'.$row['teamname'].'" selected="selected">'.$row['teamname'].'</option>';
		else
			$string .='<option value="'.$row['teamname'].'" >'.$row['teamname'].'</option>';
	
	}
	return $string;	
}
function generate_id($tbname,$fieldname)
{
	$sql = "INSERT INTO ".$tbname." (".$fieldname.") VALUES (NULL) ";
	//echo $sql;
	mysql_query($sql) or die(mysql_error());
	$id = mysql_insert_id();
	return $id;
}

function getTeamMembers($mlist)
{
	$sql="select id,userid,username,fname,lname from tps_users";
	
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	$ml=explode(",",$mlist);

	$mlcnt=count($ml);

	$tm=array();
	$tmd=array();
	
	$j=0;
	while($row = mysql_fetch_array($result)){
		
		$tm[$j]=$row['userid'];

		$tmd[$j]=$row['userid'];
		
		if($row['username']!="")
		{
			$tmd[$j].="-".$row['username']." ";
		}
		if($row['fname']!="")
		{
			$tmd[$j].="-".$row['fname']." ";
		}
		if($row['lname']!="")
		{
			$tmd[$j].=$row['lname'];
		}	    
		$j++;
	}
	
	$tmcnt=count($tm);
		
	$k=0;
	foreach ($tm as $search_item)
	{
    		if (in_array($search_item, $ml))
    		{
	        	$string .='<option value="'.$search_item.'" selected >'.$tmd[$k].'</option>';
    		}
		else{
			$string .='<option value="'.$search_item.'">'.$tmd[$k].'</option>';
		}
		$k++;
	}

	return $string;
}

function getTeamMembersByUserId($usrid)
{
	$sql="select id,userid,username,fname,lname from tps_users where userid='".$usrid."'";
	
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	$j=0;
	while($row = mysql_fetch_array($result)){
		
		$string.=$row['userid'];
		if($row['username']!="")
		{
			$string.="-".$row['username']." ";
		}
		if($row['fname']!="")
		{
			$string.="-".$row['fname']." ";
		}
		if($row['lname']!="")
		{
			$string.=$row['lname'];
		}	    
		$j++;
	}
	
	return $string;
}

function getEventType($sel){

	$sql="select * from tps_event_type where status='0'";
	
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){

		if($sel==$row['id'])
			$string .='<option  value="'.$row['id'].'"selected="selected">'.$row['event_type'].'</option>';
		else
			$string .='<option value="'.$row['id'].'" >'.$row['event_type'].'</option>';
	
	}
	return $string;	

}


function getEventSubtype($sel,$parent){

	$sql="select * from tps_event_subtype where status='0' ";
	
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){

	if($parent==$row['parent']){if($sel==$row['event_subtype']){
			$string .='<option  class="'.$row['parent'].' showevent" selected="selected">'.$row['event_subtype'].'</option>';}
		else{
			$string .='<option class="'.$row['parent'].' showevent">'.$row['event_subtype'].'</option>';
}}
else{$string .='<option class="'.$row['parent'].' showevent"style="display:none;">'.$row['event_subtype'].'</option>';}
	
	}
	return $string;	

}


function getEventTypeTopic($topic_id){

	$res=mysql_query("SELECT e.event_type,GROUP_CONCAT(CONCAT(t.id,'||',t.event_subtype) order by t.id asc separator '||') topic FROM tps_event_type e JOIN tps_event_subtype t ON e.id=t.parent where e.status='0' and t.status='0' GROUP BY e.id") or die(mysql_error());

	$string="";
	while($row = mysql_fetch_assoc($res)) 
	{		
		$string.='<optgroup label="'. $row["event_type"] .'">';
				
		$topic = explode("||", $row["topic"]);

		for($i=0; $i<count($topic); $i+= 2) {					

$string.='<option value="'.$topic[$i].'" '.($topic_id == $topic[$i]?'selected="selected"':'').'>'.$topic[$i+1].'</option>';

		}
				
		$string.='</optgroup>';
	}
	return $string;
}

function getEventTypeTopicListByIds($sel)
{
	$res=mysql_query("SELECT e.id,e.event_type,GROUP_CONCAT(CONCAT(t.id,'||',t.event_subtype) order by t.id asc separator '||') topic FROM tps_event_type e JOIN tps_event_subtype t ON e.id=t.parent where e.status='0' and t.status='0' GROUP BY e.id") or die(mysql_error());

	$sid=explode(",",$sel);

	$string="";


	$topiclist=array();
	$trnids=array();
	
	$valArr=array();
	
	$events=array();
	$eventTopicsIds="";
	$eventTopicsTitles="";
	$i=0;
	$cnt=0;
	while($r=mysql_fetch_assoc($res))
	{
		$events[$i]=$r["event_type"];

		$topic = explode("||", $r["topic"]);

		$eventTopicsIds.="$".$r["id"]."$";
		$eventTopicsTitles.="$".$events[$i]."$";

		for($j=0; $j<count($topic); $j+= 2) {	
			
			$topiclist[$i][$j]=$topic[$j+1];
			$trnids[$i][$j]=$topic[$j];

			$eventTopicsIds.=$trnids[$i][$j].",";
			$eventTopicsTitles.=$topiclist[$i][$j].",";

			$cnt++;
		}	
		
		$i++;
	}

	$meids=explode("$",$eventTopicsIds);
	$metopics=explode("$",$eventTopicsTitles);

	$tids=explode(",",$eventTopicsIds);
	$ttps=explode(",",$eventTopicsTitles);

	$newtids=array();
	$newttps=array();

	$mainnewtids=array();
	$mainnewttps=array();

	for($i=0;$i<count($meids);$i++)
	{
		if($i%2==1)
		{
			$mainnewtids[$i]=$meids[$i];
			$mainnewttps[$i]=$metopics[$i];
		}else{
			$newtids[$i]=$meids[$i];
			$newttps[$i]=$metopics[$i];
		}
	}

	$mnt=implode(",",$mainnewtids);
	$mntp=implode(",",$mainnewttps);

	$mmnt=explode(",",$mnt);
	$mmntp=explode(",",$mntp);

	$mmcnt=count($mmntp);

	$nt=implode(",",$newtids);
	$ntp=implode(",",$newttps);

	$nnt=explode(",",$nt);
	$nntp=explode(",",$ntp);


	$k=0;
	$j=0;
	foreach ($nnt as $search_item)
	{
		
    		if (in_array($search_item, $sid))
    		{
			$string .='<option value="'.$search_item.'" selected >'.$nntp[$k].'</option>';	
    		}
		else{
			if($nntp[$k]=="")
			{
				if($j!=0){
					$string .='</optgroup>';
				}
				$string.='<optgroup label="'. $mmntp[$j] .'">';
				if($j<($mmcnt-1))
				{
					$j++;
				}else{
					$j=($mmcnt-1);
				}
			}else{
				$string .='<option value="'.$search_item.'">'.$nntp[$k].'</option>';	
			}
		}
		
		$k++;
	}

	return $string;

}

function getEventCreator($uid)
{
	$sql="select username,fname,lname from tps_users where userid='".$uid."'";
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	$row = mysql_fetch_array($result);

	$string=$row['username']."-".$row['fname']." ".$row['lname'];

	return $string;	
}

function getTrainingName($sel)
{
	$sql="select * from tps_training where status='0'";
	
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){

		if($sel==$row['id'])
			$string .='<option value="'.$row['id'].'" selected="selected">'.$row['trainingname'].'</option>';
		else
			$string .='<option value="'.$row['id'].'" >'.$row['trainingname'].'</option>';
	
	}
	return $string;	
}
function getTrainingNameById($tid)
{
	$sql="select * from tps_training where id='".$tid."'";
	
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	$row = mysql_fetch_array($result);
	
	$string.=$row['trainingname'];	
	
	return $string;	
}

function getApptStatus($sel)
{
	$sql="select * from tps_appt_status where status=0";
		
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){

		if($sel==$row['name'])
			$string .='<option value="'.$row['name'].'" selected="selected">'.$row['name'].'</option>';
		else
			$string .='<option value="'.$row['name'].'" >'.$row['name'].'</option>';
	
	}
	return $string;	
}

function getapptresult($sel)
{
	$sql="select * from tps_appt_result where status=0";
		
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){

		if($sel==$row['name'])
			$string .='<option value="'.$row['name'].'" selected="selected">'.$row['name'].'</option>';
		else
			$string .='<option value="'.$row['name'].'" >'.$row['name'].'</option>';
	
	}
	return $string;	
}



function getContactReason($sel)
{
	$sql="select * from tps_contact_reason";
		
	$result=mysql_query($sql) or die(mysql_error());
	$string='';
	
	while($row = mysql_fetch_array($result)){

		if($sel==$row['name'])
			$string .='<option value="'.$row['name'].'" selected="selected">'.$row['name'].'</option>';
		else
			$string .='<option value="'.$row['name'].'" >'.$row['name'].'</option>';
	
	}
	return $string;	
}
function get_Userslist_Task($sel)
{
	$string='';
	
	$sid=explode(",",$sel);
    
	$userlist=array();
	$userids=array();
	
	$res=mysql_query("select id,username,fname,lname from tps_users")or die(mysql_error());
	$i=0;
	while($r=mysql_fetch_array($res))
	{
		$userlist[$i]=$r['username']."-".ucfirst($r['fname'])." ".$r['lname'];
		$userids[$i]=$r['id'];		
		$i++;
	}
	
	$k=0;
	foreach ($userids as $search_item)
	{
    		if (in_array($search_item, $sid))
    		{
	        	$string .='<option value="'.$search_item.'" selected >'.$userlist[$k].'</option>';
    		}
		else{
			$string .='<option value="'.$search_item.'">'.$userlist[$k].'</option>';
		}
		$k++;
	}

	return $string;
	
}


function get_userslist_by_userid_messages($userids)
{
	$uid=explode(",",$userids);
	
	$string="";

	$userarr=array();

	for($i=0;$i<count($uid);$i++)
	{
		$id=$uid[$i];
		$res=mysql_query("select id,username,fname,lname from tps_users where userid='$id'")or die(mysql_error());
		$r=mysql_fetch_array($res);
		$userarr[$i]=$r['username']."-".ucfirst($r['fname'])." ".$r['lname'];		
	}
	$string=implode(",",$userarr);

	return $string;
}

function get_user_ids_messages($userids)
{
	$uid=explode(",",$userids);
	
	$string="";

	$userarr=array();

	for($i=0;$i<count($uid);$i++)
	{
		$id=$uid[$i];
		$res=mysql_query("select id from tps_users where userid='$id'")or die(mysql_error());
		$r=mysql_fetch_array($res);
		$userarr[$i]=$r['id'];		
	}
	$string=implode(",",$userarr);

	return $string;
}

function get_userslist_by_userid_for_msg($userids)
{
	$uid=explode(",",$userids);
	
	$string="";

	$userarr=array();

	for($i=0;$i<count($uid);$i++)
	{
		$id=$uid[$i];
		$res=mysql_query("select id,username,fname,lname from tps_users where id='$id'")or die(mysql_error());
		$r=mysql_fetch_array($res);
		$userarr[$i]=$r['username']."-".ucfirst($r['fname'])." ".$r['lname'];		
	}
	$string=implode(",",$userarr);

	return $string;
}

function send_internal_message($msgseqid,$sendto,$subject,$message){

		$senttousernames=get_userslist_by_userid_for_msg($sendto);

		$attids="";

		$cur_username=get_session('DISPLAY_NAME');
		$cur_uid=get_session('LOGIN_ID');

		$inbox_sql_frmusr="INSERT INTO `inbox` (`id`, `msg_seqid`, `msguserid`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msgseqid', '$cur_uid', '$cur_uid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'inbox', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

		mysql_query($inbox_sql_frmusr)or die("Inbox : ".mysql_error());

		$userids=explode(",",$sendto);
		
		for($i=0;$i<count($userids);$i++)
		{
			$assigned_userid=$userids[$i];

			$sender=$userids[$i];

			$res=mysql_query("select id,fname from tps_users where id='$sender'")or die(mysql_error());
			$r=mysql_fetch_array($res);
			$userFirstName=ucfirst($r['fname']);

			$message=str_replace("%firstname%",$userFirstName,$message);

			$inbox_sql="INSERT INTO `inbox` (`id`, `msg_seqid`, `msguserid`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msgseqid', '$sender', '$cur_uid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'inbox', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

			mysql_query($inbox_sql)or die("Inbox : ".mysql_error());

		}


}


?>


