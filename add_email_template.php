<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

//validate_login();

$page_name = "add_email_template.php";
$page_title = $site_name." -  Email Template";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

$cur_userid=get_session('LOGIN_ID');
$cur_email=get_session('LOGIN_EMAIL');
$cur_username=get_session('DISPLAY_NAME');

$cur_loguserid=get_session('LOGIN_USERID');
include "js/add_email_template.js";
?>


<div class="main-content" >
<div class="container">
<br /><br />
<?php
$action=0;

$templatename="";
$subject="";
$message="";

if(isset($_REQUEST['action']))
{
	if($_REQUEST['action']=="edit")
	{
		$action=1; // for Edit
		$tempid=$_REQUEST['tempid'];

		$res=mysql_query("select * from tps_email_templates where id='$tempid' and is_system_template='0'") or die(mysql_error());
		$r=mysql_fetch_array($res);

		$templatename=$r['template_name'];
		$subject=$r['template_subject'];
		$message=$r['template_content'];
		
	
	}

}
?>
  <div class="col-md-16">
    <div class="box" >
      <div class="box-header">
	<span class="title">
	<?php 
		if($action==1)
		{ 
			echo "Update Email Template"; 
		}else{
			echo "Create New Email Template";
		}
	?>
	</span>
  </div>
      <div class="box-content padded" style="min-height:500px;" align="center">
	
<form method="post">
	<table width="70%" border="1" align="center">
	<tr>
		<td width="10%">Template Name </td>
		<td width="60%">
		<input type="text" name="templatename" id="templatename" value="<?php echo $templatename; ?>" class="col-md-6"/>	
		</td>
	</tr>
	<tr>
		<td>Subject</td>
		<td>
			<input type="text" name="subject" id="subject" value="<?php echo $subject; ?>" class="col-md-6"/>	

		</td>
	</tr>
	<tr>
		<td valign="top">Your Message </td>
		<td>
		<textarea name="message" id="message" rows="6" class="col-md-6"><?php echo $message; ?></textarea>	
		</td>
	</tr>
	<tr>
		<td></td>
		
		<td>	<?php if($action==1){ ?>

			<input type="hidden" name="tempid" id="tempid" value="<?php echo $tempid; ?>" />		

			<button id="updtemplate" type="button" class="btn btn-blue">Update Template</button> &nbsp;&nbsp;
				<?php }else{ ?>
			<button id="savenewtemplate" type="button" class="btn btn-blue">Save Template</button> &nbsp;&nbsp;
				<?php } ?>
			<button id="reset" type="button" onclick="javascript: window.location.href='email_template_listing.php';" class="btn btn-default">Cancel</button>
		</td>
	</tr>
	</table> 
</form>

<br>


      </div>
	<br />
    </div>
	<br /><br />
   </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";

?>
