<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "add_leads.php";
$page_title = $site_name." -  Add / Edit Leads";
$ctoc='';
$title1='' ;
$fname1 =  '' ;
$deal='';
$lname1='' ;
$fname2=  '' ;
$lname2='' ;
$add_line1='' ;
$add_line2='' ;
$city='' ;
$state='' ;
$zip='' ;
$cus='';
$lid1='';
$lqual='';
$phonetype1='' ;
$phone1='' ;
$phonetype2='' ;
$phone2='' ;
$phonetype3='' ;
$phone3='' ;
$timestamp=date('m/d/Y h:i A');
$setdate='';
$email1='' ;
$email2='' ;
$lasst='';
$setasst='';
$email_opt_in='' ;
$time_contact='' ;
$comments='' ;

$apptdate='' ;
$appttimefrom='' ;
$appttimeto='' ;

$ltype='' ;
$lsubtype='';
$refby='' ;
$ldeal='' ;
$lasst='' ;
$lindate='' ;
$lkdate='' ;
$lstatus='' ;
$lres='' ;
$gift='' ;
$redir='';
$apptresult='' ;
$setby='';
$lid='';
$set='';
$lid1='';
$apptstatus='';
$user_dispname=get_session('DISPLAY_NAME');
$user_logid=get_session('LOGIN_ID');
$url= $_SERVER['HTTP_REFERER'];
$userid=get_session('LOGIN_USERID');
$check_dealer=mysql_query("select userid from tps_users where userid='$userid'") or die(mysql_error());
$sel_deal=mysql_fetch_array($check_dealer);
$deal=$sel_deal['userid'];
$ID=get_session('LOGIN_ID');
$today=date('Y-m-d H:i:s');
$child=array($ID);
$re=mysql_query("select id from tps_users where parentid='$userid'");
while($r=mysql_fetch_array($re)){
array_push($child,$r['id']);
}
$child=implode(',',$child);
$redir=request_get('redir');
$lid=request_get('lid');
$lid1=request_get('lid1');
if(!isset($_REQUEST['action'])){
$cur_userid=get_session('LOGIN_USERID');
$ldeal=$cur_userid;
$re=mysql_query("select count(*) as lead from tps_users where userid='$cur_userid' and dealer_flag='1' ");
$s=mysql_fetch_array($re);
$deal=$s['lead'];
if($deal==1){
$deal=$cur_userid;
}
else
{
$deal='';
}
}

if( request_get('action')=='delete') {
$lid=request_get('lid');
$redir=request_get('redir');
$sql="update tps_events set  delete_flag='1' where lead_id='".$lid."'";
		mysql_query($sql) or die(mysql_error());
$sql="update tps_lead_card set  delete_flag='1',customer_flag='0' where id='".$lid."'";
$result=mysql_query($sql) or die(mysql_error());

$log_desc= ucfirst($user_dispname)." deleted 1 Lead from both Lead and Customer Listing. <br><b><a href=$url target=_blank >$url</a></b>";

tps_log_error(__INFO__, __FILE__, __LINE__, "Lead Deleted", $user_logid, $log_desc);

		if($redir=='3'){
$message="Lead has been deleted from both lead and customer listing";
		set_session('e_flag' , 1);
		set_session('message' , $message);		
		header("Location: leadcard.php");
		exit;
}
if($redir=='2'){
$message="Lead has been deleted from both lead and customer listing";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		header("Location: customer.php");
		exit;
}

}

if( request_get('action')=='deletecustomer') {
$lid=request_get('lid');
$redir=request_get('redir');
$sql="update tps_lead_card set  customer_flag='0' where id='".$lid."'";
$result=mysql_query($sql) or die(mysql_error());
if($redir=='3'){
$message="Lead has been deleted from customer listing";
		set_session('e_flag' , 1);
		set_session('message' , $message);

	$log_desc= ucfirst($user_dispname)." deleted 1 Lead from  Customer Listing. <br><b><a href=$url target=_blank >$url</a></b>";
	tps_log_error(__INFO__, __FILE__, __LINE__, "Customer Deleted", $user_logid, $log_desc);

	header("Location: leadcard.php");
	exit;
}
if($redir=='2'){
$message="Customer has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);
	$log_desc= ucfirst($user_dispname)." deleted 1 Lead from Customer Listing. <br><b><a href=$url target=_blank >$url</a></b>";

	tps_log_error(__INFO__, __FILE__, __LINE__, "Lead Deleted", $user_logid, $log_desc);
	header("Location: customer.php");
	exit;
}
	}


if( request_get('action')=='deletelead') {
$lid=request_get('lid');
$redir=request_get('redir');
$sql="update tps_events set  delete_flag='1' where lead_id='".$lid."'";
		mysql_query($sql) or die(mysql_error());
$sql="update tps_lead_card set  delete_flag='1' where id='".$lid."'";
$result=mysql_query($sql) or die(mysql_error());
		if($redir=='3'){
$message="Lead has been deleted ";
		set_session('e_flag' , 1);
		set_session('message' , $message);

	$log_desc= ucfirst($user_dispname)." deleted 1 Lead from  Lead Listing. <br><b><a href=$url target=_blank >$url</a></b>";
	tps_log_error(__INFO__, __FILE__, __LINE__, "Lead Deleted", $user_logid, $log_desc);

header("Location: leadcard.php");
		exit;
}
if($redir=='2'){
$message="Lead has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);

	$log_desc= ucfirst($user_dispname)." deleted 1 Lead from  Customer Listing. <br><b><a href=$url target=_blank >$url</a></b>";
	tps_log_error(__INFO__, __FILE__, __LINE__, "Lead Deleted", $user_logid, $log_desc);

		header("Location: customer.php");
		exit;
}
	
}

if(request_get('action')=='addcustomer') {
$apptstatus='Demo';
$ctoc='1';
}




if( request_get('action')=='edit'||request_get('action')=='updatecustomer') {
	if ($lid > 0 ) {
$sql="select createdby,DATE_FORMAT(created_at, '%b %d %Y %h:%i %p') as set_date from tps_events where lead_id='".$lid."' and delete_flag='0'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);
	$num=mysql_num_rows($result);
if($num>0){
		$setdate=$row['set_date'];
		$setby=$row['createdby'];
$re=mysql_query("select userid,fname,lname from tps_users where userid='$setby' ")or die(mysql_error());
$r= mysql_fetch_array($re);
		$set=ucfirst($r['fname'])." ".$r['lname'];
}

		$sql="select * from tps_lead_card where id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);

$sql_tas="select appt_status,appt_result from tps_appt_status_update where lead_id='$row[leadid]'and delete_flag='0' order by id desc ";
		$restas=mysql_query($sql_tas) or die(mysql_error());
	        $tas = mysql_fetch_array($restas);
$apptstatus=$tas['appt_status'];
$apptresult=$tas['appt_result'];
		 
		$title1=$row['title1'];
		$fname1 =  $row['fname1'];
		$lname1=$row['lname1'];
		$title2=$row['title2'];
		$fname2=$row['fname2'];
		$lname2=$row['lname2'];
		$add_line1=$row['add_line1'];
		$add_line2=$row['add_line2'];
		$city=$row['city'];
		$state=$row['state'];
		$zip=$row['zip'];
		$deal=$row['dealer'];
		$lqual=$row['lead_qualification'];
		$phonetype1=$row['phonetype1'];
		$phone1=$row['phone1'];
		$phonetype2=$row['phonetype2'];
		$phone2=$row['phone2'];
		$phonetype3=$row['phonetype3'];
		$phone3=$row['phone3'];
		$setasst=$row['set_asst'];
		$email1=$row['email1'];
		$email2=$row['email2'];
		$cus=$row['customer_flag'];
		$email_opt_in=$row['email_opt_in'];

		$time_contact=$row['time_contact'];

		$ltype=$row['lead_type'];
		$lsubtype=$row['lead_subtype'];
		$refby=$row['referred_by'];
		$ldeal=$row['lead_dealer'];
		$lasst=$row['lead_asst'];
		$setasst=$row['set_asst'];
		if($row['lead_in']!='0000-00-00'){
		$date = date_create($row['lead_in']);
		$lindate=date_format($date, 'M d Y');
		}
		else{
		$lindate ='' ;
		}
if($row['oktocall']!='0000-00-00'){
		$date = date_create($row['oktocall']);
		$lkdate=date_format($date, 'M d Y');
}
else{
$lkdate ='' ;
}
		$lstatus=$row['lead_status'];
		$lres=$row['lead_result'];
		$gift=$row['gift'];
		$comments=stripslashes($row['comments']);
		$ctoc=$row['customer_flag'];

		$sql="select * from tps_events where lead_id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());

		$row = mysql_fetch_array($result);

		$date = date_create($row['start']);
		$apptdate=date_format($date, 'm-d-Y'); 
		$hr=date_format($date, 'h');
		$min=date_format($date, 'i');
		$appttimefrom=($hr*60*60) + ($min * 60) ;

		$date = date_create($row['end']);
		$hr=date_format($date, 'h');
		$min=date_format($date, 'i');
		$appttimeto=($hr*60*60) + ($min * 60);
		$leadid=$row['id'];




	}
}



if( isset($_POST['savecustomer']) ){
	$timestamp=fmt_db_date_time();

		$phone1= request_get('phone1');
		$phone2= request_get('phone2');
		$phone3= request_get('phone3');

if( $lid > 0){

$chk_lt=mysql_query("select lead_type from tps_lead_card where id='$lid'")or die(mysql_error());
$chk_ltr=mysql_fetch_array($chk_lt);

$newleadtype=request_get('ltype');
$oldleadtype=$chk_ltr['lead_type'];

if(trim($oldleadtype)!=trim($newleadtype))
{
	$sql2="update tps_lead_card set is_surveyed='0' where id='$lid' ";
	$result2=mysql_query($sql2)or die(mysql_error());  

	$sql4="delete from tps_survey_details where leadid='$lid'";
	$result4=mysql_query($sql4)or die(mysql_error());  

	$sql5="delete from tps_survey_answers where leadid='$lid'";
	$result5=mysql_query($sql5)or die(mysql_error());  

	$log_desc= ucfirst($user_dispname)." deleted Survery Details by changing the Lead type from $oldleadtype to $newleadtype. <br><b><a href=$url target=_blank >$url</a></b>";
	tps_log_error(__INFO__, __FILE__, __LINE__, "Survey deleted", $user_logid, $log_desc);
}

$sql="update tps_lead_card set ".
		" `uid` = '". get_session('LOGIN_ID') ."', ".
		" `title1` = '".ltrim(request_get('prefname1'),"Select")  ."', ".
		" `fname1` = '".  ucfirst(request_get('fname1')) ."', ".
		" `lname1` = '". request_get('lname1') ."', ".
		" `title2` = '". ltrim(request_get('prefname2'),"Select")."', ".
		" `fname2` = '".  ucfirst(request_get('fname2') )."', ".
		" `lname2` = '". request_get('lname2') ."', ".
		" `add_line1` = '". request_get('addr1') ."', ".
		" `add_line2` = '". request_get('addr2') ."', ".
		" `city` = '". request_get('city') ."', ".
		" `state` = '". request_get('state1') ."', ".
		" `zip` = '". request_get('zip') ."', ".
		" `phonetype1` = '". request_get('phone1_type') ."', ".
		" `phone1` = '". $phone1 ."', ".
		" `phonetype2` = '". request_get('phone2_type') ."', ".
		" `phone2` = '". $phone2 ."', ".
		" `phonetype3` = '". request_get('phone3_type') ."', ".
		" `phone3` = '". $phone3 ."', ".
		" `email1` = '". request_get('email1') ."', ".
		" `email2` = '". request_get('email2') ."', ".
		" `email_opt_in` = '". request_get('eoptin') ."', ".
		" `time_contact` = '". request_get('besttime') ."', ".
		" `lead_type` = '". request_get('ltype') ."', ".
		" `lead_subtype` = '". request_get('lsubtype') ."', ".
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `lead_asst` = '". request_get('lasst') ."', ".
		" `set_asst` = '". request_get('setasst') ."', ".
		" `referred_by` = '". request_get('refby') ."', ".
		" `lead_dealer` = '". ltrim(request_get('ldeal'),"Select") ."', ".
		" `dealer` = '". ltrim(request_get('dealer'),"Select")  ."', ".
		" `lead_in` = '".date('m/d/y',strtotime(request_get('lindate'))) ."', ".
		" `oktocall` = '".date('Y-m-d',strtotime(request_get('lkdate'))) ."', ".
		" `lead_result` = '". request_get('lres') ."', ".
		" `gift` = '". request_get('gift') ."', ".
		" `comments` = '". request_get('comment1') ."', ".
		" `lead_qualification` = '". request_get('lqual') ."', ".
		" `customer_flag` = '". request_get('ctoc') ."', ".
		" `modified` = '". $timestamp ."', ".
		" `modified_by` = '". get_session('DISPLAY_NAME') ."' where id='".$lid."'";	
	$result=mysql_query($sql) or die(mysql_error());

	$log_desc= ucfirst($user_dispname)." Update 1  Customer Details. <br><b><a href=$url target=_blank >$url</a></b>";
	tps_log_error(__INFO__, __FILE__, __LINE__, "Customer Updated", $user_logid, $log_desc);

$sql="select * from tps_lead_card where id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);
$leadid=$row['leadid'];
$id=$row['id'];
if(request_get('apptresult')=='Sale'||request_get('apptresult')=='Used RB Sale'){
$Id=get_session('LOGIN_ID');
$lead_type='Buyer';
$lead_subtype='We Call';
$lcres=mysql_query("select id from tps_lead_card where id='".$lid."' order by id") or die(mysql_error());		
$lcr=mysql_fetch_array($lcres);
$name=$name='3_'.$lcr['id'].'';
$sql="update tps_lead_card set lead_type='$lead_type',lead_subtype='$lead_subtype',referral_flag='0' where uid = '$Id' and referred_by='$name'  and customer_flag='0' and delete_flag='0' and  referral_flag='1'";
$result=mysql_query($sql)or die(mysql_error());    

}
$result=request_get('apptresult');
if(request_get('change')==1){
if( request_get('apptresult')=='' && request_get('apptstatus')!='' )
{
$result='Pending';
}

$sql="insert into  tps_appt_status_update set ".
	
		" `lead_id` = '". $leadid ."', ".
		" `appt_status` = '". request_get('apptstatus') ."', ".
		" `dealer` = '". request_get('dealer') ."', ".
		" `appt_result` = '". $result ."', ".
		" `modifiedon` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."'";	
$result2=mysql_query($sql)or die(mysql_error());    


}
$setdate=date('Y-m-d H:i:s',strtotime(request_get('setdate')));
$setby=request_get('setby1');
$sql="update  tps_events set  created_at = '$setdate', createdby='$setby' where lead_id='$lid' and delete_flag='0'";	
$result2=mysql_query($sql)or die(mysql_error());  

/*if(request_get('lstatus')!='')
{
$sql="select * from tps_lead_card where id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);
$leadid=$row['leadid'];
$sql="insert into tps_lead_status_update set ".
	
		" `leadid` = '". $leadid ."', ".
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `modifiedon` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."'";	
$result2=mysql_query($sql)or die(mysql_error());    
}*/
              $message="Customer has been updated";
		set_session('e_flag' , 1);
		set_session('message' , $message);


if(request_get('ctoc')!='1'){
$refby='1_'.$lid.'';
$new_refby='3_'.$lid.'';
$sql=mysql_query("update tps_lead_card set referred_by='$new_refby' where referred_by='$refby'")or die(mysql_error());
$message="Appt Status has been updated and the Lead has been removed from the customer list";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		header("Location: leadcard.php" );
		exit;
}
$redir=request_get('redir');
if($redir==1){
		header("Location: appointments.php" );
		exit;
}
if($redir==2){
		header("Location: customer.php" );
		exit;
}
if($redir==3){	
		header("Location: leadcard.php" );
		exit;
}
else{	
header("Location: customer.php" );
		exit;
}


	}
	else{
	$leadId =generate_id('leadcard','leadcard_id');
	set_session('leadid',$leadId);
$leadid= get_session('leadid');
	$sql="insert into tps_lead_card set ".
		" `uid` = '". get_session('LOGIN_ID') ."', ".
		" `leadid` = '". $leadid."', ".
		" `title1` = '". ltrim(request_get('prefname1'),"Select")  ."', ".
		" `fname1` = '". ucfirst(request_get('fname1')) ."', ".
		" `lname1` = '". request_get('lname1') ."', ".
		" `title2` = '". ltrim(request_get('prefname2'),"Select") ."', ".
		" `fname2` = '". ucfirst(request_get('fname2')) ."', ".
		" `lname2` = '". request_get('lname2') ."', ".
		" `add_line1` = '". request_get('addr1') ."', ".
		" `add_line2` = '". request_get('addr2') ."', ".
		" `city` = '". request_get('city') ."', ".
		" `state` = '". request_get('state1') ."', ".
		" `zip` = '". request_get('zip') ."', ".
		" `phonetype1` = '". request_get('phone1_type') ."', ".
		" `phone1` = '". $phone1 ."', ".
		" `phonetype2` = '". request_get('phone2_type') ."', ".
		" `phone2` = '". $phone2 ."', ".
		" `phonetype3` = '". request_get('phone3_type') ."', ".
		" `phone3` = '". $phone3 ."', ".
		" `email1` = '". request_get('email1') ."', ".
		" `email2` = '". request_get('email2') ."', ".
		" `email_opt_in` = '". request_get('eoptin') ."', ".
		" `time_contact` = '". request_get('besttime') ."', ".
		" `lead_type` = '". request_get('ltype') ."', ".
		" `lead_subtype` = '". request_get('lsubtype') ."', ".
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `lead_asst` = '". request_get('lasst') ."', ".
		" `set_asst` = '". request_get('setasst') ."', ".
		" `referred_by` = '". request_get('refby') ."', ".
		" `lead_dealer` = '". ltrim(request_get('ldeal'),"Select")  ."', ".
		" `dealer` = '". ltrim(request_get('dealer'),"Select")  ."', ".
	        " `lead_in` = '".date('m/d/y',strtotime(request_get('lindate'))) ."', ".
		" `oktocall` = '".date('Y-m-d',strtotime(request_get('lkdate'))) ."', ".
		" `lead_result` = '". request_get('lres') ."', ".
		" `gift` = '". request_get('gift') ."', ".
		" `comments` = '". request_get('comment1') ."', ".
		" `lead_qualification` = '". request_get('lqual') ."', ".
		" `customer_flag` = '1', ".
		" `created` = '". $timestamp ."', ".
		" `modified` = '". $timestamp ."', ".
		" `created_by` = '". get_session('DISPLAY_NAME') ."', ".
		" `modified_by` = '". get_session('DISPLAY_NAME') ."'";	
		
		$result=mysql_query($sql) or die(mysql_error());
$log_desc= ucfirst($user_dispname)." Add 1  New Customer . <br><b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Customer Added", $user_logid, $log_desc);
$id = mysql_insert_id();
$result=request_get('apptresult');
$status=request_get('apptstatus');
if(request_get('change')==1){
if( request_get('apptresult')=='' && request_get('apptstatus')!='' )
{
$result='Pending';
}
$sql="insert into tps_appt_status_update set ".
	
		" `lead_id` = '". $leadid ."', ".
		" `appt_status` = '". request_get('apptstatus') ."', ".
		" `dealer` = '". request_get('dealer') ."', ".
		" `appt_result` = '".$result ."', ".
		" `modifiedon` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."'";	
$result2=mysql_query($sql)or die(mysql_error());    
}
/*if(request_get('lstatus')!='')
{
$sql="insert into tps_lead_status_update set ".
	
		" `leadid` = '". $leadid ."', ".
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `modifiedon` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."'";	
$result2=mysql_query($sql)or die(mysql_error());    
}*/
$appt_title=request_get('fname1')." ".request_get('lname1')." - ".request_get('addr1')." ".request_get('addr2'). " ". request_get('city')." ".request_get('state1')." - ".request_get('phone1');

		$appt_title=trim($appt_title);

		$a = strptime(request_get('apptdate'), '%m-%d-%Y');
		$timestamp = mktime(0, 0, 0, $a['tm_mon']+1, $a['tm_mday'], $a['tm_year']+1900);

		$startime = $timestamp + request_get('appttimefrom');
		$endtime =  $timestamp +request_get('appttimeto');
		
		

		$appt_startdate= gmdate("Y-m-d H:i:s", $startime);
		$appt_enddate= gmdate("Y-m-d H:i:s", $endtime);

		$createdby=get_session('LOGIN_USERID');

	
		$sql="update tps_events set `title`='".$appt_title."'  where `lead_id`='".$lid."'";	
		$result=mysql_query($sql) or die(mysql_error());

		$_SESSION['lid']=0;
		
		$message="New Customer has been added";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		
		header("Location: customer.php" );
		exit;
	}
}







if( isset($_POST['savelead']) ){
	$timestamp=fmt_db_date_time();

		$phone1= request_get('phone1');
		$phone2= request_get('phone2');
		$phone3= request_get('phone3');

if(request_get('ctoc')==1){

}


if( $lid > 0){

	$chk_lt=mysql_query("select lead_type from tps_lead_card where id='$lid'")or die(mysql_error());
	$chk_ltr=mysql_fetch_array($chk_lt);

	$newleadtype=request_get('ltype');
	$oldleadtype=$chk_ltr['lead_type'];

	if(trim($oldleadtype)!=trim($newleadtype))
	{
		$sql2="update tps_lead_card set is_surveyed='0' where id='$lid' ";
		$result2=mysql_query($sql2)or die(mysql_error());  

		$sql4="delete from tps_survey_details where leadid='$lid'";
		$result4=mysql_query($sql4)or die(mysql_error());  

		$sql5="delete from tps_survey_answers where leadid='$lid'";
		$result5=mysql_query($sql5)or die(mysql_error());  

$log_desc= ucfirst($user_dispname)." deleted Survery Details by changing the Lead type from $oldleadtype to $newleadtype. <br><b><a href=$url target=_blank >$url</a></b>";
tps_log_error(__INFO__, __FILE__, __LINE__, "Survey deleted", $user_logid, $log_desc);

	}

		$sql="update tps_lead_card set ".
		" `uid` = '". get_session('LOGIN_ID') ."', ".
		" `title1` = '".ltrim(request_get('prefname1'),"Select")  ."', ".
		" `fname1` = '".  ucfirst(request_get('fname1')) ."', ".
		" `lname1` = '". request_get('lname1') ."', ".
		" `title2` = '". ltrim(request_get('prefname2'),"Select")."', ".
		" `fname2` = '".  ucfirst(request_get('fname2') )."', ".
		" `lname2` = '". request_get('lname2') ."', ".
		" `add_line1` = '". request_get('addr1') ."', ".
		" `add_line2` = '". request_get('addr2') ."', ".
		" `city` = '". request_get('city') ."', ".
		" `state` = '". request_get('state1') ."', ".
		" `zip` = '". request_get('zip') ."', ".
		" `phonetype1` = '". request_get('phone1_type') ."', ".
		" `phone1` = '". $phone1 ."', ".
		" `phonetype2` = '". request_get('phone2_type') ."', ".
		" `phone2` = '". $phone2 ."', ".
		" `phonetype3` = '". request_get('phone3_type') ."', ".
		" `phone3` = '". $phone3 ."', ".
		" `email1` = '". request_get('email1') ."', ".
		" `email2` = '". request_get('email2') ."', ".
		" `email_opt_in` = '". request_get('eoptin') ."', ".
		" `time_contact` = '". request_get('besttime') ."', ".
		" `lead_type` = '". request_get('ltype') ."', ".
		" `lead_subtype` = '". request_get('lsubtype') ."', ".
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `lead_asst` = '". request_get('lasst') ."', ".
		" `set_asst` = '". request_get('setasst') ."', ".
		" `referred_by` = '". request_get('refby') ."', ".
		" `lead_dealer` = '". ltrim(request_get('ldeal'),"Select") ."', ".
		" `dealer` = '". ltrim(request_get('dealer'),"Select")  ."', ".
		" `lead_in` = '".date('m/d/y',strtotime(request_get('lindate'))) ."', ".
		" `oktocall` = '".date('Y-m-d',strtotime(request_get('lkdate'))) ."', ".
		" `lead_result` = '". request_get('lres') ."', ".
		" `gift` = '". request_get('gift') ."', ".
		" `comments` = '". request_get('comment1') ."', ".
		" `lead_qualification` = '". request_get('lqual') ."', ".
		" `customer_flag` = '". request_get('ctoc') ."', ".
		" `modified` = '". $timestamp ."', ".
		" `modified_by` = '". get_session('DISPLAY_NAME') ."' where id='".$lid."'";	
		$result=mysql_query($sql) or die(mysql_error());

	$leadname=ucfirst(request_get('fname1'))." ".request_get('lname1');

	$log_desc= ucfirst($user_dispname)." Updated $leadname Lead Details. <br><b><a href=$url target=_blank >$url</a></b>";

	tps_log_error(__INFO__, __FILE__, __LINE__, "$leadname Lead Updated", $user_logid, $log_desc);

$sql="select * from tps_lead_card where id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);
$leadid=$row['leadid'];
$id=$row['id'];
if(request_get('apptresult')=='Sale'||request_get('apptresult')=='Used RB Sale'){
$Id=get_session('LOGIN_ID');
$lead_type='Buyer';
$lead_subtype='We Call';
$lcres=mysql_query("select id from tps_lead_card where id='".$lid."' order by id") or die(mysql_error());		
$lcr=mysql_fetch_array($lcres);
$name=$name='3_'.$lcr['id'].'';
$sql="update tps_lead_card set lead_type='$lead_type',lead_subtype='$lead_subtype',referral_flag='0' where uid = '$Id' and referred_by='$name'  and customer_flag='0' and delete_flag='0' and  referral_flag='1'";
$result=mysql_query($sql)or die(mysql_error());    
}

$result=request_get('apptresult');
$status=request_get('apptstatus');
if(request_get('change')==1){
if( request_get('apptresult')=='' && request_get('apptstatus')!='' )
{
$result='Pending';
}
$sql="insert into  tps_appt_status_update set ".
	
		" `lead_id` = '". $leadid ."', ".
		" `appt_status` = '". request_get('apptstatus') ."', ".
		" `appt_result` = '". $result ."', ".
		" `dealer` = '". request_get('dealer') ."', ".
		" `modifiedon` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."'";	
$result2=mysql_query($sql)or die(mysql_error());    
}

$setdate=date('Y-m-d H:i:s',strtotime( request_get('setdate')));
$setby=request_get('setby1');
$sql="update  tps_events set  created_at = '$setdate', createdby='$setby' where lead_id='$lid' and delete_flag=0";	
$result2=mysql_query($sql)or die(mysql_error());  
/*if(request_get('lstatus')!='')
{
$sql="select * from tps_lead_card where id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);
$leadid=$row['leadid'];
$sql="insert into tps_lead_status_update set ".
	
		" `leadid` = '". $leadid ."', ".
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `modifiedon` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."'";	
$result2=mysql_query($sql)or die(mysql_error());    
}*/
              $message="Lead has been updated";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		
$appt_title=request_get('fname1')." ".request_get('lname1')." - ".request_get('addr1')." ".request_get('addr2'). " ". request_get('city')." ".request_get('state1')." - ".request_get('phone1');

		$appt_title=trim($appt_title);

		$a = strptime(request_get('apptdate'), '%m-%d-%Y');
		$timestamp = mktime(0, 0, 0, $a['tm_mon']+1, $a['tm_mday'], $a['tm_year']+1900);

		$startime = $timestamp + request_get('appttimefrom');
		$endtime =  $timestamp +request_get('appttimeto');
		
		

		$appt_startdate= gmdate("Y-m-d H:i:s", $startime);
		$appt_enddate= gmdate("Y-m-d H:i:s", $endtime);

		$createdby=get_session('LOGIN_USERID');

	
		$sql="update tps_events set `title`='".$appt_title."'  where `lead_id`='".$lid."'";	
		$result=mysql_query($sql) or die(mysql_error());

		$_SESSION['lid']=0;
		set_session('e_flag' , 1);

if(request_get('ctoc')==1){
if($lid!=''){
$refby='3_'.$lid.'';
$new_refby='1_'.$lid.'';
$sql=mysql_query("update tps_lead_card set referred_by='$new_refby' where referred_by='$refby'")or die(mysql_error());

$log_desc= ucfirst($user_dispname)." Convert 1   Lead to Customer . <br><b><a href=$url target=_blank >$url</a></b>";
tps_log_error(__INFO__, __FILE__, __LINE__, "Lead Converted to Customer", $user_logid, $log_desc);

$message="Lead has been Converted to Customer";
		set_session('e_flag' , 1);
		set_session('message' , $message);
header("Location: customer.php" );
		exit;
}
}


$redir=request_get('redir');
if($redir==1){
		header("Location: appointments.php" );
		exit;
}
if($redir==2){
		header("Location: customer.php" );
		exit;
}
if($redir==3){	
		header("Location: leadcard.php" );
		exit;
}
else{	
header("Location: leadcard.php" );
		exit;
}
		
	}
	else{
	$leadId =generate_id('leadcard','leadcard_id');
	set_session('leadid',$leadId);
$leadid= get_session('leadid');
	$sql="insert into tps_lead_card set ".
		" `uid` = '". get_session('LOGIN_ID') ."', ".
		" `leadid` = '". $leadid."', ".
		" `title1` = '". ltrim(request_get('prefname1'),"Select")  ."', ".
		" `fname1` = '". ucfirst(request_get('fname1')) ."', ".
		" `lname1` = '". request_get('lname1') ."', ".
		" `title2` = '". ltrim(request_get('prefname2'),"Select") ."', ".
		" `fname2` = '". ucfirst(request_get('fname2')) ."', ".
		" `lname2` = '". request_get('lname2') ."', ".
		" `add_line1` = '". request_get('addr1') ."', ".
		" `add_line2` = '". request_get('addr2') ."', ".
		" `city` = '". request_get('city') ."', ".
		" `state` = '". request_get('state1') ."', ".
		" `zip` = '". request_get('zip') ."', ".
		" `phonetype1` = '". request_get('phone1_type') ."', ".
		" `phone1` = '". $phone1 ."', ".
		" `phonetype2` = '". request_get('phone2_type') ."', ".
		" `phone2` = '". $phone2 ."', ".
		" `phonetype3` = '". request_get('phone3_type') ."', ".
		" `phone3` = '". $phone3 ."', ".
		" `email1` = '". request_get('email1') ."', ".
		" `email2` = '". request_get('email2') ."', ".
		" `email_opt_in` = '". request_get('eoptin') ."', ".
		" `time_contact` = '". request_get('besttime') ."', ".
		" `lead_type` = '". request_get('ltype') ."', ".
		" `lead_subtype` = '". request_get('lsubtype') ."', ".
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `lead_asst` = '". request_get('lasst') ."', ".
		" `set_asst` = '". request_get('setasst') ."', ".
		" `referred_by` = '". request_get('refby') ."', ".
		" `lead_dealer` = '". ltrim(request_get('ldeal'),"Select")  ."', ".
		" `dealer` = '". ltrim(request_get('dealer'),"Select")  ."', ".
	        " `lead_in` = '".date('m/d/y',strtotime(request_get('lindate'))) ."', ".
		" `oktocall` = '".date('Y-m-d',strtotime(request_get('lkdate'))) ."', ".
		" `lead_result` = '". request_get('lres') ."', ".
		" `gift` = '". request_get('gift') ."', ".
		" `comments` = '". request_get('comment1') ."', ".
		" `lead_qualification` = '". request_get('lqual') ."', ".
		" `customer_flag` = '". request_get('ctoc') ."', ".
		" `created` = '". $timestamp ."', ".
		" `modified` = '". $timestamp ."', ".
		" `created_by` = '". get_session('DISPLAY_NAME') ."', ".
		" `modified_by` = '". get_session('DISPLAY_NAME') ."'";	
		
		$result=mysql_query($sql) or die(mysql_error());

		$leadname=ucfirst(request_get('fname1'))." ".request_get('lname1');

		$log_desc= ucfirst($user_dispname)." Added $leadname as a New Lead. <br><b><a href=$url target=_blank >$url</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "$leadname Lead Added", $user_logid, $log_desc);

$id = mysql_insert_id();
$result=request_get('apptresult');
if(request_get('change')==1){
if( request_get('apptresult')=='' && request_get('apptstatus')!='' )
{
$result='Pending';
}
$sql="insert into tps_appt_status_update set ".
	
		" `lead_id` = '". $leadid ."', ".
		" `appt_status` = '". request_get('apptstatus') ."', ".
		" `dealer` = '". request_get('dealer') ."', ".
		" `appt_result` = '".$result ."', ".
		" `modifiedon` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."'";	
$result2=mysql_query($sql)or die(mysql_error());    
}
/*if(request_get('lstatus')!='')
{
$sql="insert into tps_lead_status_update set ".
	
		" `leadid` = '". $leadid ."', ".
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `modifiedon` = '". $timestamp ."', ".
		" `modifiedby` = '". get_session('DISPLAY_NAME') ."'";	
$result2=mysql_query($sql)or die(mysql_error());    
}*/
		$last_id_lead=mysql_insert_id();

		$appt_title=request_get('fname1') . " ". request_get('lname1'). " - ".request_get('phone1'). " - ". request_get('email1');
		$appt_title=trim($appt_title);

		$a = strptime(request_get('apptdate'), '%m-%d-%Y');
		
		$timestamp = mktime(0, 0, 0, $a['tm_mon']+1, $a['tm_mday'], $a['tm_year']+1900);
		
		$startime = $timestamp + request_get('appttimefrom');
		$endtime =  $timestamp +request_get('appttimeto');

		$appt_startdate= gmdate("Y-m-d H:i:s", $startime);
		$appt_enddate= gmdate("Y-m-d H:i:s", $endtime);
		
		$createdby=get_session('LOGIN_USERID');


		$message="New Lead Added";
		set_session('e_flag' , 1);
		set_session('message' , $message);
		
		header("Location: leadcard.php" );
		exit;
	}
}

include "lcas_header.php";
include "lcas_top_nav.php";
 echo getLeadSubType333($lsubtype,$ltype);
$asdf2=getGift_new($ltype,$lsubtype,$gift);
echo "<script>var asdf2='".$asdf2."';</script>";
require_once("js/add_leads.js");
?>

<link href="javascripts/fullcalendar/fullcalendar.css" media="screen" rel="stylesheet" type="text/css" />
<link href="javascripts/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css"  media='print' />
<script src="javascripts/fullcalendar/fullcalendar.js" type="text/javascript"></script>

<script src="js/addlead_calender.js" type="text/javascript"></script>

<div class="main-content" style="margin:0px;">
<div class="container">
<br /><br />

<div class="row">
<div class="col-md-5" id='external-events'>
<div class="accordion" id="accordion2">
  <div class="accordion-group" style="width:520px;">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
        <span class="title"><i class="icon-th-list"></i> Name and contact info</span>
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse in">
      <div class="accordion-inner padded">

<form id="frmaddleads" name="frmaddleads" action="add_leads.php" method="POST" onsubmit="myFunction();">
	<select name="prefname1" id="prefname1" class="nline firstli" tabindex="1">
<option vale="">Select</option>
		<?php echo getNamePrefix($title1); ?>
	</select>
	<input type="text" class="validate[required] firstli" data-prompt-position="topLeft" name="fname1" id="fname1"  value="<?php echo $fname1; ?>" placeholder="First Name *" required/>
		<input class="nline firstli" type="text" name="lname1" id="lname1"  value="<?php echo $lname1; ?>" placeholder="Last Name *" required/>
		<br/>

		<select class="nline firstli" name="prefname2" id="prefname2"  >
<option vale="">Select</option>
		<?php echo getNamePrefix($title2); ?>
		</select>
		<input type="text"  data-prompt-position="topLeft" name="fname2"  value="<?php echo $fname2; ?>" id="fname2" placeholder="First Name " />
		<input class="nline firstli"type="text" name="lname2" id="lname2"  value="<?php echo $lname2; ?>" placeholder="Last Name " />
		<br/>

		<input  class="nline firstli" type="text" name="addr1" id="addr1"  value="<?php echo $add_line1; ?>" placeholder="Address Line1" style="width:210px;"/>
		<input class="nline firstli" type="text" name="addr2" id="addr2"  value="<?php echo $add_line2; ?>" placeholder="Address Line2"/>
		<br/>
		<input class="nline firstli" type="text" name="zip" id="zip" placeholder="Zipcode"  value="<?php echo $zip;?>" style="width:120px;"/>
		<input class="nline " type="text" name="city" id="city"  value="<?php echo $city; ?>" placeholder="City"/>
<div id="drag-lead-list" class="col-lg-6 nline" style="border:1px solid #DBDBDB;float:right;padding:0px;height:200px;">
		 <div class="accordion-heading" style="padding:5px;">Drag and Drop for Appointment</div>
		 <ul class="nav nav-list">

		<li>
<?php
	if(request_get('action')=="edit" || request_get('action')=="updatecustomer")
	{ 
		$sql="select * from tps_events where lead_id='".$lid."' and delete_flag='0' ";
		$result=mysql_query($sql) or die(mysql_error());

		$row = mysql_fetch_array($result);
		
		$n=mysql_num_rows($result);
		if($n>0)
		{
			echo '<a id="" class="external-event first">'.$row['title'].'</a> ';
		}else{
			$sql="select title1,fname1,lname1,add_line1,add_line2,city,state,zip,phone1 from tps_lead_card where id='".$lid."'";
			$result=mysql_query($sql) or die(mysql_error());
			$row = mysql_fetch_array($result);

			$title1=$row['title1'];
			$fname1=$row['fname1'];
			$lname1=$row['lname1'];

			$add_line1=$row['add_line1'];
			$add_line2=$row['add_line2'];
			$city=$row['city'];
			$state=$row['state'];
			$zip=$row['zip'];

			$phone1=$row['phone1'];

	$cur_title=$title1." ".$fname1." ".$lname1."-".$add_line1." ".$add_line2." ".$city." ".$state." ".$zip."-".$phone1;
		
			echo '<a id="" class="external-event first">'.$cur_title.'</a> ';
		}
		
	}else{
		echo '<a id="" class="external-event first"> </a> ';
	}
?>
		</li>
			

        	</ul>
		</div>


		<select class="nline" name="state1" id="state1" >
		<?php 
			if($state=='') { $state = __DEFAULT_STATE__; }

			echo getStates($state);
		?>
		</select>
		
		<br/>
		<select class="nline " name="phone1_type" id="phone1_type" >
		<?php echo getPhoneType($phonetype1); ?>
		</select>
		<?php
		
			echo '<input class="nline firstli" type="text" name="phone1"  id="phone1" value="'.$phone1.'" placeholder="Phone Number *" required onblur="exist_lead()" >';
		
		?>
		
		<br />
		<select class="nline" name="phone2_type" >
		<?php echo getPhoneType($phonetype2); ?>
		</select>
		<?php
		
			echo '<input class="nline" type="text" name="phone2"  id="phone2" value="'.$phone2.'"  placeholder="Phone Number"/>';
		
		?>
		
		<br />

		<select class="nline" name="phone3_type" >
		<?php echo getPhoneType($phonetype3); ?>
		</select>
		<?php
		
		   echo '<input class="nline" type="text" name="phone3"  id="phone3" value="'.$phone3.'"  placeholder="Phone Number"/>';
		
		?>
		
		<br />		

		
		<input class="nline firstli" type="text"  name="email1" id="email1"  value="<?php echo $email1; ?>"   placeholder="Email-Id"  />
		<?php 
		if($email_opt_in==1 || $email_opt_in==3)
			echo '<input type="checkbox"   checked="checked"  id="chkemail1" value="email1" >';
		else
			echo '<input type="checkbox"  id="chkemail1" value="email1" >';
		?>
		<label for="email1_optin">Opt In</label>
		<br/>
		<div class="alert alert-error" id="erremail1" style="display:none;">
		Email Format should be
		<strong>test@test.com</strong>
		</div>
		
		<input class="nline" type="text" name="email2" value="<?php echo $email2; ?>" id="email2"   placeholder="Email-Id"/>
		<?php
		if($email_opt_in==2 || $email_opt_in==3)
			echo '<input type="checkbox"  checked="checked" id="chkemail2"  value="email2" >';
		else
			echo '<input type="checkbox"  id="chkemail2" value="email2"  >';
		?>
		<label for="email2_optin">Opt In</label>

		<div class="alert alert-error" id="erremail2" style="display:none;">
		Email Format should be
		<strong>test@test.com</strong>
		</div>

		<input type="hidden" name="eoptin" id="eoptin" placeholder="eoptin" >
		<br/>	 <br>	 <br>	 <br>
		<label  class="nline" style="width:150px;font-weight:normal;">Best Time to Contact :</label> 

		<select class="nline" name="besttime" id="besttime" >
			<option value="" >Select</option>
			<?php echo getDDBestTime($time_contact); ?>
		</select>
	 <br>


	<label  class="nline" style="width:150px;font-weight:normal;">Lead Type :*</label> 
	<select class="nline" name="ltype" id="ltype" onchange="showsubtype(this);"  required>
<option value="" class="Select" selected>Select</option>
	<?php echo getLeadType($ltype); ?>
	</select> <br />
	<label  class="nline" style="width:150px;font-weight:normal;">Lead SubType :* </label> 
	<select class="nline" name="lsubtype" id="lsubtype" onchange="show_gift(this);" required>
	<?php 
	echo "<option value='' class='Select'>Select</option>";
		echo getLeadSubType222($lsubtype,$ltype); 
	
		 
	?>
	</select> <br />

	<label class="nline" style="width:150px;font-weight:normal;" >Gift Choice :</label>

		<select class="nline" name="gift" id="gift" >
<option value=""class="Select">Select</option>
		<?php 
	
			echo getGift_new222($ltype,$lsubtype,$gift);
	
			
		?>
		</select><br />

<label  class="nline" style="width:150px;font-weight:normal;">Referred By :</label> 
<select class="nline" name="refby" id="refby"  >
		<option value="">Select</option>
 <optgroup label="Customers">
		<?php
$uid=get_session('LOGIN_ID');

	$re=mysql_query("select id,fname1,lname1,fname2,lname2 from tps_lead_card where FIND_IN_SET(uid,'$child') and customer_flag='1'");

	while($r=mysql_fetch_array($re))
	{        if($refby=='1_'.$r['id'].'')
                echo "<option value='1_".$r['id']."' selected>".ucfirst($r['lname1'])." ".$r['fname1']." ".$r['lname2']." ".$r['fname2']."</option>";
  		else
		echo "<option value='1_".$r['id']."'>".ucfirst($r['lname1'])." ".$r['fname1']." ".$r['lname2']." ".$r['fname2']."</option>";
	}
echo ' </optgroup>';
echo ' <optgroup label="Campaigns">';
$re=mysql_query("select * from tps_campaign where active=1 and FIND_IN_SET(uid,'$child')");

	while($r=mysql_fetch_array($re))
	{        if($refby=='2_'.$r['id'].'')
                echo "<option value='2_".$r['id']."' selected>".$r['campaign']."</option>";
  		else
		echo "<option value='2_".$r['id']."' >".$r['campaign']."</option>";
	}
echo ' </optgroup>';
$arr=explode('_',$refby);
if($arr[0]==3){
$re=mysql_query("select fname1,lname1 from tps_lead_card where id='$arr[1]'");
$r=mysql_fetch_array($re);
echo ' <optgroup label="Lead">';
echo "<option value='$refby' selected>".ucfirst($r['fname1'])." ".ucfirst($r['lname1'])."</option>";
echo ' </optgroup>';
}		 
		 ?>

		</select><br />


	<label  class="nline" style="width:150px;font-weight:normal;">Lead Dealer :* </label> 
		<select class="nline" name="ldeal" id="ldeal"  required>
		<option value="">Select</option>
		<?php
		
	
	$cur_parentid=get_session('LOGIN_PARENTID');

	/*$re=mysql_query("select fname,lname,userid,nickname from tps_users where status='1'");

	while($r=mysql_fetch_array($re))
	{       
		if($r['nickname']!="")
			$displayname=ucfirst($r['nickname'])." ".$r['lname'];
		else
			$displayname=ucfirst($r['fname'])." ".$r['lname'];

		if($ldeal==$r['userid'])
               		echo "<option value='$r[userid]' selected>".$displayname."</option>";
  		else
			echo "<option value='$r[userid]'>".$displayname."</option>";
	}*/
		echo getLeadDealersInfo($ldeal);
		 ?>
		</select><br />

<label  class="nline" style="width:150px;font-weight:normal;"> Dealer :*</label> 
		<select class="nline" name="dealer" id="deal"  required>
		<option value="">Select</option>
		<?php
	/*$re=mysql_query("select fname,lname,userid from tps_users where dealer_flag=1  and status='1'");

	while($r=mysql_fetch_array($re))
	{        if($deal==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".$r['fname']." ".$r['lname']."</option>";
	}*/
	$re=mysql_query("select fname,lname,userid,nickname from tps_users where userid='$ldeal'");
	$r=mysql_fetch_array($re);
	$displayname="";
	if($r['nickname']!="")
		$displayname=ucfirst($r['nickname'])." ".$r['lname'];
	else
		$displayname=ucfirst($r['fname'])." ".$r['lname'];

	echo "<option value='$r[userid]' selected>".$displayname."</option>";

	echo getDealersInfo($deal);	
		 ?>
		</select><br />

	<label  class="nline" style="width:150px;font-weight:normal;">Lead Asst :</label> 
	<select class="nline" name="lasst" id="lasst" ><option value="">Select</option>
		
		<?php

	$re=mysql_query("select * from tps_users where lead_assist_flag=1  and status='1'");

	while($r=mysql_fetch_array($re))
	{        if($lasst==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".$r['fname']." ".$r['lname']."</option>";
	}
		 
		 ?>
		</select><br />
	<label  class="nline" style="width:150px;font-weight:normal;">Lead In Date :</label> 
	
	<?php
	if($lindate ==''){
		$lindate=date("M d Y");
	}
	?>
	<input class="nline" type="text" name="lindate" id="date" value="<?php echo $lindate; ?>"  placeholder="Lead In Date" style="width:154px;" required > 
	</span><br />
	<label  class="nline" style="width:150px;font-weight:normal;">Start Contacting On  :</label>
	
	<?php
	if($lkdate ==''){
		$lkdate=date("M d Y");
	}
	?>

	<input class="nline" type="text" name="lkdate" id="date1" value="<?php  echo $lkdate; ?>"   placeholder="Start Contacting On" style="width:154px;" />
	</span><br />

	<label  class="nline" style="width:150px;font-weight:normal;">Lead Status :*</label> 
		<select class="nline" name="lstatus" id="lstatus"  required><option value="">Select</option>
		<?php 
		if($lstatus !='' )
			echo getLeadStatus($lstatus);
		else
			echo getLeadStatus(__DEFAULT_LSTATUS__);
		?>
		</select><br />
	
	<label  class="nline" style="width:150px;font-weight:normal;">Lead Result :</label> 
		<select class="nline" name="lres" id="lres" ><option value="">Select</option>
		<?php 
		if($lres !='' )
			echo getLeadResult($lres);
		else
			echo getLeadResult(__DEFAULT_LRESULT__);
		?>
		</select><br />

<label class="nline" style="width:150px;font-weight:normal;"  >Appt Status : </label>
<select class="nline" name="apptstatus" id="apptstatus" ><option value="">Select</option>
	<?php 

	echo getApptStatus($apptstatus);

	?>
</select><br />
	
	<label class="nline" style="width:150px;font-weight:normal;"  >Appt Result :</label>
<select class="nline" name="apptresult" id="apptresult" ><option value="">Select</option>
	<?php 

	echo getapptresult($apptresult);

	?>
</select><br />
		
<label class="nline" style="width:150px;font-weight:normal;"  >Set By :</label>
<?php $c_level=get_session('LOGIN_LEVEL');
if($c_level=='1' || $c_level=='2'){
?>
<select class="nline" name="setby" id="sel_setby" class='setby' >

<option value="">Select</option>
                  	<?php 
$userid=get_session('LOGIN_USERID');
$child=array($setby);
$re=mysql_query("select userid from tps_users where setby_flag=1 and  status=1");
while($r=mysql_fetch_array($re)){
array_push($child,$r['userid']);
}
$child=implode(',',$child);
  $re=mysql_query("select userid,fname,lname from tps_users where FIND_IN_SET(userid,'$child') GROUP BY userid");

	while($r=mysql_fetch_array($re))
	{    
		 if($r['userid']==$setby)
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".ucfirst($r['fname'])." ".$r['lname']."</option>";
	}
		 
		echo '</select>';}
		else{ echo '<input type="text" class="date start nline" placeholder="Set By" id="setby" class="setby" style="width:154px;" value="'.$set.'" disabled/>';
		}
echo '<input type="hidden" class="date start nline"  id="setby1"  name="setby1"  value="'.$setby.'" >';?><br>
<label class="nline" style="width:150px;font-weight:normal;" >Set Date :</label>

<input class="date start nline" type="text" name="setdate" id="setdate" value="<?php echo $setdate; ?>"  placeholder="Set Date" style="width:154px;" > <br>


<label  class="nline" style="width:150px;font-weight:normal;">Set Asst :</label> 
	<select class="nline" name="setasst" id="lasst" ><option value="">Select</option>
		
		<?php
		
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

	$re=mysql_query("select * from tps_users where set_assist_flag=1  and status='1'");

	while($r=mysql_fetch_array($re))
	{        if($setasst==$r['userid'])
                echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".$r['lname']."</option>";
  		else
		echo "<option value='$r[userid]'>".$r['fname']." ".$r['lname']."</option>";
	}
		
		 ?>
		</select><br />


<label  class="nline" style="width:150px;font-weight:normal;">Lead Qualification :</label> 
	<select class="nline" name="lqual" id="lqual" ><option value="">Select</option>
		
		<?php
	

	$re=mysql_query("select display from tps_lead_qualified where status=0");

	while($r=mysql_fetch_array($re))
	{        if($lqual==$r['display'])
                echo "<option  selected>".$r['display']."</option>";
  		else
		echo "<option >".$r['display']."</option>";
	}
		
		 ?>
		</select><br />

		 <textarea placeholder="Notes" rows="3" cols="60" class="nline"  name="comment1" id="comment1" ><?php echo $comments;?></textarea ><br /><br /><br />


<input id="icheck1"   type="checkbox"   value="1"  <?php if($ctoc==1){print "checked='checked'";} ?> name="ctoc" >

<label class="" for="icheck1">Convert to Customer</label>
		<div style="text-align:right; ">
		<input type="hidden" name="lid" id="lid" value="<?php echo $lid; ?>" />
		<input type="hidden" name="lid1" id="lid1" value="<?php echo $lid1; ?>" />
<?php
$sql="select * from tps_lead_card where id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);
$leadid=$row['id'];
?>
		<input type="hidden" name="leadid" id="leadid" value="<?php echo $leadid; ?>" />
		<input type="hidden" name="referred_by" id="referred_by" value="<?php echo $referred_by; ?>" />
<?php
if(isset($_REQUEST['backto'])=="sfu")
{
?>
<button type="button" class="btn btn-green"  onclick="javascript:window.location='survey_followup_qa_form.php?lid=<?php echo $_REQUEST['lid'];?>&leadseqid=<?php echo $_REQUEST['leadseqid'];?>' " >Back to Survey Follow up</button> 
<?php
}
?>
<?php
if(request_get('action')=="updatecustomer"){
?>
<input type="submit" class="btn btn-blue"   value="Update" name="savecustomer" id="savecustomer" >
<?php
}if(request_get('action')=="addcustomer"){?>
<input type="submit" class="btn btn-blue"  value="Save" name="savecustomer" id="savecustomer">
<?php
} 
if(request_get('action')=="edit"){
?>
<input type="submit" class="btn btn-blue" value="Update" name="savelead" id="savelead" onclick="return ctoccheck()" >
<?php
}if(request_get('action')==""){?>
<input type="submit" class="btn btn-blue"  value="Save" name="savelead" id="savelead"   >
<?php
}
?><input type="button" class="btn btn-blue"  value='Cancel' onclick="javascript:window.location='leadcard.php';">
</div>
<input type="hidden" name="redir" value="<?php 	echo $redir;?>">      
<input type="hidden" name="customer" id="customer"  value="<?php echo $cus;?>">      
<input type="hidden" name="change" id="change">      
<input type="hidden" name="exist" id="exist"> 
<input type="hidden"  id="view">       
	</form>

  </div>
    </div>
  </div>
 
</div>

</div>
  <div class="col-md-7">
<?php
if(isset($_REQUEST['backto'])=="sfu")
{
?>
<button type="button" class="btn btn-green" onclick="javascript:window.location='survey_followup_qa_form.php?lid=<?php echo $_REQUEST['lid'];?>&leadseqid=<?php echo $_REQUEST['leadseqid'];?>'">Back to Survey Follow up</button>&nbsp;<br>
<?php
}
?>
    <div class="box" >
      <div class="box-header"><span class="title">Full Calendar</span>
	<span style="float:right;" class="title">
	<b><span style='color:#3A87AD'>Appointment</span> &nbsp;&nbsp; <span style='color:green'>Dealer Availability Time</span></b>
</span>
</div>
      <div class="box-content"><div id="spinner"></div>
      	<div id="add-lead-calendar"></div>
      </div>
  </div>


<br />

<br />

   </div>
 </div> 
 </div>

<?php

include "lcas_footer.php";
?>
  <script>$('a').attr("tabindex","-1");</script>
