<?php
session_start();
$target='';
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

//validate_login();
$item='';
$price='';
$qty='';
$page_name = "Xls_file_details.php";
$page_title = $site_name." -  Xls file details";


if( isset($_POST['save']) ){
$filename = basename($_FILES['file']['name']);
$ext = substr($filename, strrpos($filename, '.') + 1);

		if ( ($ext == "xls") ) {
			
			$file=$_FILES['file']['name'];
			$target = "Excel/"; 
			$target = $target . $_FILES['file']['name']; 
			move_uploaded_file($_FILES['file']['tmp_name'],$target);
	

		
  }
}
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<script>

function deleteRow(r)
{
 var retVal = confirm("Are You Sure To Delete This Row?");
   if( retVal == true ){
  var i = r.parentNode.parentNode.rowIndex;
document.getElementById("myTable").deleteRow(i);
      return true;
   }else{
      return false;
   }
}

</script>
<script src="js/json.js"></script>

<script>
$(document).ready( function(){
$('#run').click( function() {


var TableData;
TableData = storeTblValues();
TableData = $.toJSON(TableData);


$.ajax({
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            url: 'edit.php?pTableData='+TableData,
            
           // data: "pTableData[]=" + TableData,
            async: false,

            success: function (response) {
                alert("Record Has been Saved in Database");
            },

            error: function ()
            { console.log('there is some error'); }
      
});
function storeTblValues()
{
    var TableData = new Array();

    $('#myTable tr').each(function(row, tr){
        TableData[row]={
	    "action" : $(tr).find('td:eq(0)').text()
            , "title1" : $(tr).find('td:eq(1)').text()
            , "fname1" :$(tr).find('td:eq(2)').text()
            , "lname1" : $(tr).find('td:eq(3)').text()
            , "title2" : $(tr).find('td:eq(4)').text()
	    , "fname2" :$(tr).find('td:eq(5)').text()
            , "lname2" : $(tr).find('td:eq(6)').text()
            , "add_line1" : $(tr).find('td:eq(7)').text()
            , "add_line2" :$(tr).find('td:eq(8)').text()
            , "city" : $(tr).find('td:eq(9)').text()
            , "state" : $(tr).find('td:eq(10)').text()
	    , "zip" :$(tr).find('td:eq(11)').text()
            , "phonetype1" : $(tr).find('td:eq(12)').text()
            , "phone1" : $(tr).find('td:eq(13)').text()
            , "phonetype2" :$(tr).find('td:eq(14)').text()
            , "phone2" : $(tr).find('td:eq(15)').text()
            , "phonetype3" : $(tr).find('td:eq(16)').text()
	    , "phone3" :$(tr).find('td:eq(17)').text()
            , "email1" : $(tr).find('td:eq(18)').text()
            , "email2" : $(tr).find('td:eq(19)').text()
            , "email_opt_in" :$(tr).find('td:eq(20)').text()
            , "time_contact" : $(tr).find('td:eq(21)').text()
            , "lead_type" : $(tr).find('td:eq(22)').text()
	    , "lead_subtype" :$(tr).find('td:eq(23)').text()
            , "lead_status" : $(tr).find('td:eq(24)').text()
            , "lead_asst" : $(tr).find('td:eq(25)').text()
            , "referred_by" : $(tr).find('td:eq(26)').text()
            , "lead_dealer" : $(tr).find('td:eq(27)').text()
	    , "lead_in" :$(tr).find('td:eq(28)').text()
            , "oktocall" : $(tr).find('td:eq(29)').text()
            , "lead_result" : $(tr).find('td:eq(30)').text()
            , "gift" :$(tr).find('td:eq(31)').text()
            , "apptresult" : $(tr).find('td:eq(32)').text()
           
        }    
    }); 
    TableData.shift();  // first row will be empty - so remove
    return TableData;
}


});
});



</script>

</head>
<body>
<div class="main-content" >
<br><br>
<form id="frm" name="frm" class="fillup" action="ex.php" method="POST" enctype="multipart/form-data" >
<div id="uniform-photo" class="uploader" style="width:275px;margin-left:15px;">
                        <input   class="col-md-4" id="file" name="file" type="file"  style="width:500px;"> </div>
<input class="btn btn-info pull-right" style="margin-right:700px;" type="submit" name="save" id="save" value="View">
<div class="container">
<br /><br />
  <div class="col-md-16">
    <div class="box" >
      <div class="box-header"><span class="title">XLS File Details </span></div>
      <div class="box-content" style="height:600px;" align="center" >
<?php
echo '<div id="demo"></div><br><br>';
   $table="<div id='dvData' style='overflow:auto;'><table border='0' id='myTable' class='table table-normal' width='98%;' ><thead>";
    include 'Excel/reader.php';
    $excel = new Spreadsheet_Excel_Reader();
    $excel->read($target);    
    $x=1;
    while($x<=$excel->sheets[0]['numRows']) {
if($x==1)
$table.="<tr><td>Action</td>";
else
      $table.="<tr><td><img src='images/small-bin.png' onclick='deleteRow(this)'></td>";
      $y=1;
      while($y<=$excel->sheets[0]['numCols']) {

        $cell = isset($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y] : '';
        $table.="\t\t<td>$cell</td>\n";  
        $y++;
      }  
      $table.="\t</tr>\n";
      $x++;
    }
$table.="<tbody></table>";

			echo $table;


    ?> 
<button id="run">Convert!</button><div id="pass" > </div>
</div> 
   </div>
	<br />
    </div>
	<br /><br />
   </div>

 </div> 
 </div>
</form>
</body>
</html>
<?php

include "lcas_footer.php";

?>

