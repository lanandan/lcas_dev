
<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
$s1='';
$e1='';
$s2='';
$e2='';
//validate_login();

$page_name = "tasks.php";
$page_title = $site_name." - View Dealer availability";
if( request_get('action')=='delete') {
$id= request_get('lid');
$sql="update tps_events set delete_flag='1' where id='$id'";
$result=mysql_query($sql) or die(mysql_error());
$message="your Task has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);
header("location:tasks.php");
exit;
}
function subtractTime($initialHour, $finalHour){
return (date("H:i:s", strtotime("00:00") + strtotime($finalHour) - strtotime($initialHour)));
}
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>

<script type="text/javascript">

$(document).ready(function() {

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 3000);
});
$('#previous').click(function(){

var data = {
    			type: 'view',
			date:$('#date').val(),
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "previousweek.php",
	    		data: data,
	    		success: function(output) {
$('table').html(output);
}
});
});
$('#next').click(function(){

var data = {
    			type: 'next',
			date:$('#date').val(),
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "previousweek.php",
	    		data: data,
	    		success: function(output) {
$('table').html(output);
}
});
});

});
	
	
</script>
<script>

function confirmDelete() 
{
	var agree=confirm("Are You Sure To Delete?");
	if (agree)
		return true ;
	else
		return false ;
}

</script>

<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>Dealer Availability Timing List&nbsp;&nbsp;<a class="btn btn-blue" href="dealer_availability.php"><span>Add New availability time</span></a>	
	</h3>
</div> <button class="btn btn-info pull-left" style="margin-left:10px;margin-top:5px;" id='previous' type="button" >To See Previous Week</button><button class="btn btn-info pull-left" style="margin-left:10px;margin-top:5px;" id='next'  type="button" >To See Next Week</button>
<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
      </div>
    </div>
  </div>

<div class="container">

<div class="box">
<div class="box-header"><span class="title">Dealer Availability Timing List</span></div>
      <div class="box-content" >
	
<div id="dataTables">	
	<?php
	$userid=get_session('LOGIN_USERID');
	$date=date('m/d/Y',strtotime('last sunday'));
	$next_date=strtotime($date);
	



	$table="<table width='98%' class='dTable responsive' id='example'><thead>";
	$table.="<tr>
		<th> </th>
		<th> </th>
		<th>Wants for the Day</th>
		 <th>Date</th>
		 <th>Start Time</th>
		 <th>End Time</th>
		 <th>Start Time</th>
		 <th>End Time</th>
		 <th>Total Hours Available</th>
		</tr></thead><tbody>";

	$i=1;
	while($i<=5)
	{
$s1='';
$e1='';
$s2='';
$e2='';	
	$next='+'.$i." day";               
$next_day = strtotime($next, $next_date);

$next_day1=date('m/d/Y', $next_day);	
$next_day2=date('Y-m-d', $next_day);
$res=mysql_query("select * from tps_dealer_availability where uid='".$userid."' and delete_flag='0' and date='$next_day2'")or die(mysql_error()); 
$j=1;
while($r = mysql_fetch_array($res))
{
if($j=='1'){
$s1=$r['start_time'];
$e1=$r['end_time'];
}
if($j=='2'){
$s2=$r['start_time'];
$e2=$r['end_time'];
}
$j++;
}
$count=mysql_num_rows($res);
$table.="<td>".''."</td>";
$Total_Time=subtractTime($s1,$e1);
$Total_Time1=subtractTime($s2,$e2);
$today = strtotime("TODAY");
$m_time1 = strtotime($Total_Time) - $today;
$m_time2 = strtotime($Total_Time1) - $today;
$total = $m_time1 + $m_time2 + $today;
$total=date('H:i:s',$total);

		if($i=='1'){
			$table.="<td>".'Monday'."</td>";
			$table.="<td>".$count."</td>";
			$table.="<td>".$next_day1."</td>";
			$table.="<td>".$s1."  </td>";
			$table.="<td>".$e1."</td>";
			$table.="<td>".$s2."  </td>";
			$table.="<td>".$e2."</td>";
			$table.="<td>".$total."  </td></tr>";
			}
		if($i=='2'){
			$table.="<td>".'Tuesday'."</td>";
			$table.="<td>".$count."</td>";
			$table.="<td>".$next_day1."</td>";
			$table.="<td>".$s1."  </td>";
			$table.="<td>".$e1."</td>";
			$table.="<td>".$s2."  </td>";
			$table.="<td>".$e2."</td>";
			$table.="<td>".$total."  </td></tr>";
			}
		if($i=='3'){
			$table.="<td>".'Wednesday'."</td>";
			$table.="<td>".$count."</td>";
			$table.="<td>".$next_day1."</td>";
			$table.="<td>".$s1."  </td>";
			$table.="<td>".$e1."</td>";
			$table.="<td>".$s2."  </td>";
			$table.="<td>".$e2."</td>";
			$table.="<td>".$total."  </td></tr>";
			}
                if($i=='4'){
			$table.="<td>".'Thursday'."</td>";
			$table.="<td>".$count."</td>";
			$table.="<td>".$next_day1."</td>";
			$table.="<td>".$s1."  </td>";
			$table.="<td>".$e1."</td>";
			$table.="<td>".$s2."  </td>";
			$table.="<td>".$e2."</td>";
			$table.="<td>".$total."  </td></tr>";
			}
                if($i=='5'){
			$table.="<td>".'Friday'."</td>";
			$table.="<td>".$count."</td>";
			$table.="<td>".$next_day1."</td>";
			$table.="<td>".$s1."  </td>";
			$table.="<td>".$e1."</td>";
			$table.="<td>".$s2."  </td>";
			$table.="<td>".$e2."</td>";
			$table.="<td>".$total."  </td></tr>";
			}

		$i++; 
	}
	$table.="<tbody></table>";
		
	echo $table;
		
	?>
<input type="hidden" id="date" value="<?php echo $date;?>">
</div> 

      </div>
	<br />
    </div>
	<br /><br />
   </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";

?>
