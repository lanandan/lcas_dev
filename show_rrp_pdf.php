<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "show_referral_rewards.php";
$page_title = $site_name." -  Referral Rewards Program ";

include "lcas_header.php";

?>
<script type="text/javascript">

</script>
<div id="spinner"></div>
<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Referral Rewards Program&nbsp;&nbsp;</h3></div>
      </div>
    </div>
  </div>

<div class="container">

<div class="box">
<div class="box-content">
	

<!-- CSS goes in the document HEAD or added to your external stylesheet -->
<?php

$lid=request_get('id');
$res=mysql_query("select lead_type,lead_subtype,lead_dealer from tps_lead_card where leadid='".$lid."'") or die(mysql_error());
$r=mysql_fetch_array($res);

$subsql="";

$lead_type=$r['lead_type'];
$lead_subtype=$r['lead_subtype'];
$cur_dealerid=$r['lead_dealer'];

$subsql=getPdfTemplete($lead_type,$lead_subtype,$cur_dealerid);

$sql ="SELECT * from tps_pdf_templates where id=($subsql) and status='0'";
//echo "<pre>$sql</pre>";

$rs_list=mysql_query($sql) or die(mysql_error());
$rs = mysql_fetch_array($rs_list);

if(isset($_REQUEST['showpdf']))
{
	if($_REQUEST['showpdf']=="1")
	{
		$genpdfid=$rs['stage1'];

echo '<iframe src="gen_pdf.php?pid='.$genpdfid.'&lid='.$lid.'&stage=1&sendemail=0" style="width:100%; height:550px;" frameborder="0"></iframe>';

		//echo '<iframe src="rrp_pdfs/ReferralRewardsProgram.pdf" style="width:100%; height:550px;" frameborder="0"></iframe>';

	}elseif($_REQUEST['showpdf']=="2"){

		$genpdfid=$rs['stage2'];

echo '<iframe src="gen_pdf.php?pid='.$genpdfid.'&lid='.$lid.'&stage=2&sendemail=0" style="width:100%; height:550px;" frameborder="0"></iframe>';

		//echo '<iframe src="rrp_pdfs/ReferralRewardsProgram2.pdf" style="width:100%; height:550px;" frameborder="0"></iframe>';
	}elseif($_REQUEST['showpdf']=="3"){

		$genpdfid=$rs['stage3'];

echo '<iframe src="gen_pdf.php?pid='.$genpdfid.'&lid='.$lid.'&stage=3&sendemail=0" style="width:100%; height:550px;" frameborder="0"></iframe>';

		//echo '<iframe src="rrp_pdfs/ReferralRewardsProgram3.pdf" style="width:100%; height:550px;" frameborder="0"></iframe>';
	}elseif($_REQUEST['showpdf']=="4"){
	
		$genpdfid=$rs['emailtemplate'];

echo '<iframe src="gen_pdf.php?pid='.$genpdfid.'&lid='.$lid.'&stage=4&sendemail=0" style="width:100%; height:550px;" frameborder="0"></iframe>';

	}else{
		echo '<iframe src="rrp_pdfs/ReferralRewardsProgram.pdf" style="width:100%; height:550px;" frameborder="0"></iframe>';
	}
}
?>

</div>
<?php
if(isset($_REQUEST['showpdf']))
{
	if($_REQUEST['showpdf']=="1")
	{
?>
<br><br>
<div align="right">
<a class="btn btn-blue" href="referral_form/referral.php?action=fill&leadid=<?php echo $_REQUEST['id']; ?>" style="display:block;width:120px; margin-right:20px;"><span>Next</span></a>&nbsp;&nbsp;
</div>
<br>
<?php
	}elseif($_REQUEST['showpdf']=="2"){
?>
<br><br>
<div align="right">
<a class="btn btn-blue" href="customer_referred_leads.php?id=<?php echo $_REQUEST['id']; ?>" style="display:block;width:120px; margin-right:20px;"><span>Next</span></a>&nbsp;&nbsp;
</div>
<br>
<?php
	}elseif($_REQUEST['showpdf']=="3"){
?>
<br><br>
<div align="right">
<a class="btn btn-blue" onclick="window.close();" href="#" style="display:block;width:120px; margin-right:20px;"><span>Close Window</span></a>&nbsp;&nbsp;
</div>
<br>
<?php
	}elseif($_REQUEST['showpdf']=="4"){
?>
<br><br>
<div align="right">
<a class="btn btn-blue" href="gen_pdf.php?pid=<?php echo $genpdfid; ?>&lid=<?php echo $lid; ?>&stage=4&sendemail=1" style="display:block;width:120px; margin-right:20px;"><span>Send Email</span></a>&nbsp;&nbsp;
</div>
<br>
<?php
	}
}
?>

    </div>
	
   </div>

 </div> 
 </div>

<?php


include "lcas_footer.php";

?>
