<?php
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
?>


 <div class="main-content">
	<div class="container">
		<div class="row">
			<div class="area-top clearfix">
				<div class="pull-left header">
				<h3 class="title">
					<i class="icon-dashboard"></i>
			            Team List
			        </h3>
			        <h5>
			            <span>Inspirational Quote</span>
		                </h5>
			        </div>
				        <ul class="list-inline pull-right sparkline-box">
			      	          <li class="sparkline-row">
				            <h4 class="blue"><span>Last Month</span> 847</h4>
				            <div class="sparkline big" data-color="blue"><!--25,11,5,28,25,19,27,6,4,23,20,6--></div>
				          </li>
				          <li class="sparkline-row">
            				  <h4 class="green"><span>This Month</span> 1140</h4>
				          <div class="sparkline big" data-color="green"><!--28,26,13,18,8,6,24,19,3,6,19,6--></div>
				          </li>
				          <li class="sparkline-row">
				          <h4 class="red"><span>This Week</span> 230</h4>
			                  <div class="sparkline big"><!--16,23,28,8,12,9,25,11,16,16,17,13--></div>
				          </li>
				        </ul>
      			</div>
    		</div>
	  </div>


<!-- Checking--> 
	<!--<div class="container padded">
		<div class="row">
	      <!-- Breadcrumb line Campaing List-->
			<div id="breadcrumbs">
			      	<div class="breadcrumb-button blue">
          			<span class="breadcrumb-label"><i class="icon-home"></i> Home</span>
			        <span class="breadcrumb-arrow"><span></span></span>
			      	</div>
	      
			        <div class="breadcrumb-button">
			          <span class="breadcrumb-label">
			            <i class="icon-dashboard"></i> Team List
			          </span>
	   
        			  <span class="breadcrumb-arrow"><span></span></span>
        			</div>
      			</div>
	
	<!-- Campaing List table-->
		<!--<div class="col-md-6"> -->
	    <!-- find me in partials/stats_table -->

	<div class="container padded">
				<div class="row">
					<div class="box">
						<div class="box-header">
					    	    <span class="title">Team List</span>
					        </div>    
						<div id="dataTables">
							<table class="dTable responsive">
							      <thead>
								      <tr>
									<th><div>Check Box</th></div>
								        <th><div>First Name</th></div>
								        <th><div>Last Name</th></div>
									<th><div>City</th></div>
									<th><div>Recruited By</th></div>
									<th><div>Opportunity Date</th></div>
									<th><div>Opportunity Time</th></div>
									<th><div>Active</th></div>
									
								      </tr>
							      </thead>
									<tr>
									  <td class="center"><input type="checkbox" class="icheck" checked id="icheck1"></td>
									  <td>Donna</td>
			  						  <td>Johnson</td>
									  <td>Lancaster</td>
									  <td>John Roby</td>	
			  						  <td>10-10-13</td>
									  <td>5:30</td>
									  <td>Yes</td>
									  							
									</tr>
							</table>
						 </div>  
     					</div>
				    </div>


					<div class="container padded">
					    <div class="row">
						<div class="box">
						  <div class="box-header">
												    
							<span class="title">Team Member Card</span>
							<button class="btn btn-blue btn-sm">Attachments</button>
							<ul class="list-inline pull-right sparkline-box">
							<button class="btn btn-sm btn-default">Add New</button>        
					          	<button class="btn btn-sm btn-green">Call</button>
							<button class="btn btn-sm btn-blue">Email</button>
							<button class="btn btn-sm btn-red">Print</button>
							<button class="btn btn-sm btn-green">Creat Task</button>
							</td>
						  </div>    
						</div>
					      </div>
					</div>
<!--Box Left 1-->
				<div class="container">
					<div class="box">
	      					<div class="box-header">
							<div class="row">
								<div class="col-md-6">

    <!-- find me in partials/accordions -->

									<div class="accordion" id="accordion2">
										<div class="accordion-group">
										    <div class="accordion-heading">
										    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Name and Address</a>
										    </div>    
										    <div class="box-content">
										           <div class="padded">
										            	<div class="form-group">
												<label class="control-label col-		lg-2">First&nbsp;Name,&nbsp;Last&nbsp;Name&nbsp;:</label><br>
												</div>		
												<div class="form-group">	
												<label class="control-label col-lg-2">Spouse's&nbsp;First&nbsp;Name&nbsp;2,&nbsp;Spouse's&nbsp;Last&nbsp;Name&nbsp;2&nbsp;:</label><br>
												</div>
												<div class="form-group">
												<label class="control-label col-lg-2">Address&nbsp;City,&nbsp;State,&nbsp;Zip&nbsp;:</label><br>
												</div>
												<div class="form-group">
												<label class="control-label col-lg-2">Phone1,&nbsp;Phone2,&nbsp;Phone3&nbsp;:</label><br>
												</div>	
												<div class="form-group">
												<label class="control-label col-lg-2">Email.Email&nbsp;Opt&nbsp;In&nbsp;:</label><br>
												</div>		
												<div class="form-group">
												<label class="control-label col-lg-2">Best&nbsp;Time&nbsp;To&nbsp;Contact&nbsp;:</label><br>
												</div>
												<div class="form-group">
												<label class="control-label col-lg-2">Team&nbsp;Status&nbsp;:&nbsp;&nbsp;Active</label><br>
												</div>
										            </div>
										      </div>

	 									</div>
 									</div>
								</div>
				<!--Box Left 1 End-->

				<!--Box Right -->

	<div class="box">
	      <div class="box-header">
			<div class="row">
  				<div class="col-md-6">
				    <!-- find me in partials/accordions -->
					<div class="accordion" id="accordion2">
						<div class="accordion-group">
				    			<div class="accordion-heading">
			    				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Recruit Profile</a>
							</div>
				        <div class="box-content">
						<div class="padded">
					        <div class="form-group">
								<label class="control-label col-lg-2">Recruited&nbsp;By&nbsp;:</label><br>
						</div>		
						<div class="form-group">	
								<label class="control-label col-lg-2">Recruited&nbsp;From&nbsp;:</label><br>
						</div>
						<div class="form-group">
								<label class="control-label col-lg-2">Recruit&nbsp;Interested&nbsp;Date&nbsp;:</label><br>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-2">$20&nbsp;GFD&nbsp;:</label><br>
						</div>	
						<div class="form-group">
							<label class="control-label col-lg-2">Opportunity&nbsp;Date&nbsp;:</label><br>
						</div>	
						<div class="form-group">
							<label class="control-label col-lg-2">Graduation&nbsp;Date&nbsp;:</label><br>
						</div>	
						<div class="form-group">
							<label class="control-label col-lg-2">First&nbsp;Solo&nbsp;Demo&nbsp;:</label><br>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-2">First&nbsp;Sale&nbsp;:</label><br>
						</div>	
						</div>
					</div>
			        </div>
    		      	</div>
		</div>
 	</div>

<!--Box Right 1 End-->


<!--Box Left 2-->
				<div class="container">
					<div class="box">
	      					<div class="box-header">
							<div class="row">
								<div class="col-md-6">

    <!-- find me in partials/accordions -->

									<div class="accordion" id="accordion2">
							<div class="accordion-group">
					    		<div class="accordion-heading">
				    			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Team Info</a>
							</div> 
										    <div class="box-content">
		           				<div class="padded">
		          					<div class="form-group">
								<label class="control-label col-lg-2">Date&nbsp;Started&nbsp;:</label><br>
								</div>		
								<div class="form-group">	
								<label class="control-label col-lg-2">Team&nbsp;Type&nbsp;:</label><br>
								</div>
								<div class="form-group">
								<label class="control-label col-lg-2">Team&nbsp;Specifics&nbsp;:</label><br>
								</div>
								<div class="form-group">
								<label class="control-label col-lg-2">Date&nbsp;Finished:</label><br>
								</div>	
								
										            </div>
										      </div>

	 									</div>
 									</div>
								</div>
				<!--Box Left 2 End-->

				<!--Box Right 2 -->

	<div class="box">
	      <div class="box-header">
			<div class="row">
  				<div class="col-md-6">
				    <!-- find me in partials/accordions -->
					<div class="accordion" id="accordion2">
						<div class="accordion-group">
				    			<div class="accordion-heading">
			    				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">User Login</a>
							</div>
				        <div class="box-content">
						<div class="padded">
					        <div class="form-group">
								<label class="control-label col-lg-2">User&nbsp;Name&nbsp;:</label><br>
						</div>		
						<div class="form-group">	
								<label class="control-label col-lg-2">Password:</label><br>
						</div>
						<div class="form-group">
								<label class="control-label col-lg-2">Permissions&nbsp;:</label><br>
						</div>
						<div class="form-group">
								<label class="control-label col-lg-2"></label><br>
						</div>
						
						</div>
					</div>
			        </div>
    		      	</div>
		</div>
 	</div>

<!--Box Right 2 End-->



<!--Toggle Tabs-->

		<div class="box">
	      		<div class="box-header">
		        <ul class="nav nav-tabs nav-tabs-left">
		        <li class="active"><a href="#labels" data-toggle="tab">Reports</a></li>
		        <li><a href="#badges" data-toggle="tab">Business Activity</a></li>
			<li><a href="#badges" data-toggle="tab">Other Activity</a></li>
			<li><a href="#badges" data-toggle="tab">Referrals</a></li>
			<li><a href="#badges" data-toggle="tab">Commissions</a></li>
		        </ul>
		        </div>
			<div class="box-content padded">
				<div class="tab-content">
				        <div class="tab-pane active" id="labels">
 					Reports Details Comes Here.            
				        </div>
				        <div class="tab-pane" id="badges" style="padding: 5px">
         				Business Activity Details Comes Here.
				        </div>
				        <div class="tab-pane" id="badges" style="padding: 5px">
					Other Activity Detatils Comes Here.
        				</div>
					<div class="tab-pane" id="badges" style="padding: 5px">
					Referrals Detatils Comes Here.
        				</div>
					<div class="tab-pane" id="badges" style="padding: 5px">
					Commisions Detatils Comes Here.
        				</div>
					
		        	</div>
		      </div>
		</div>
	</div>
</div>








	


