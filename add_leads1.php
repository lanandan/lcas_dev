<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

//validate_login();

$page_name = "add_leads.php";
$page_title = $site_name." -  Add / Edit Leads";

$lid='';
$lid=request_get('lid');
if( request_get('action')=='delete') {
	if ($lid > 0 ) {
		$sql="delete from tps_lead_card where id='".$lid."'";
		mysql_query($sql) or die(mysql_error());
		header("Location: leadcard.php");
		exit;
	}
}
$ctoc='';
$title1='' ;
$fname1 =  '' ;
$lname1='' ;

$title2='' ;
$fname2='' ;
$lname2='' ;

$add_line1='' ;
$add_line2='' ;
$city='' ;
$state='' ;
$zip='' ;

$phonetype1='' ;
$phone1='' ;
$phonetype2='' ;
$phone2='' ;
$phonetype3='' ;
$phone3='' ;

$email1='' ;
$email2='' ;

$email_opt_in='' ;
$time_contact='' ;
$comments='' ;

$apptdate='' ;
$appttimefrom='' ;
$appttimeto='' ;

$ltype='' ;
$lsubtype='';
$refby='' ;
$ldeal='' ;
$lasst='' ;
$lindate='' ;
$lkdate='' ;
$lstatus='' ;
$lres='' ;
$gift='' ;

$apptresult='' ;

if( request_get('action')=='edit') {
	if ($lid > 0 ) {
		$sql="select * from tps_lead_card where id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result);

		$title1=$row['title1'];
		$fname1 =  $row['fname1'];
		$lname1=$row['lname1'];

		$title2=$row['title2'];
		$fname2=$row['fname2'];
		$lname2=$row['lname2'];

		$add_line1=$row['add_line1'];
		$add_line2=$row['add_line2'];
		$city=$row['city'];
		$state=$row['state'];
		$zip=$row['zip'];

		$phonetype1=$row['phonetype1'];
		$phone1=$row['phone1'];
		$phonetype2=$row['phonetype2'];
		$phone2=$row['phone2'];
		$phonetype3=$row['phonetype3'];
		$phone3=$row['phone3'];

		$email1=$row['email1'];
		$email2=$row['email2'];

		$email_opt_in=$row['email_opt_in'];

		$time_contact=$row['time_contact'];

		$ltype=$row['lead_type'];
		$lsubtype=$row['lead_subtype'];
		$refby=$row['referred_by'];
		$ldeal=$row['lead_dealer'];
		$lasst=$row['lead_asst'];

		$date = date_create($row['lead_in']);
		$lindate=date_format($date, 'm-d-Y');

		$date = date_create($row['oktocall']);
		$lkdate=date_format($date, 'm-d-Y');

		$lstatus=$row['lead_status'];
		$lres=$row['lead_result'];
		$gift=$row['gift'];

		$apptresult=$row['apptresult'] ;
		$comments=$row['comments'];
		$ctoc=$row['customer_flag'];

		$sql="select * from tps_events where lead_id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());

		$row = mysql_fetch_array($result);

		$date = date_create($row['start']);
		$apptdate=date_format($date, 'm-d-Y'); 
		$hr=date_format($date, 'h');
		$min=date_format($date, 'i');
		$appttimefrom=($hr*60*60) + ($min * 60) ;

		$date = date_create($row['end']);
		$hr=date_format($date, 'h');
		$min=date_format($date, 'i');
		$appttimeto=($hr*60*60) + ($min * 60);

	}
}

if( isset($_POST['savenameinfo']) ){
	/*$sql="select script_id from tps_lead_type where lead_type='".request_get('ltype')."' and lead_subtype='G' ";
	$result=mysql_query($sql) or die(mysql_error());
	$script_id=0;
	$row = mysql_fetch_array($result);
	$script_id=$row['script_id'];*/

	$timestamp=fmt_db_date_time();

	if( request_get('phone1')== '(717)    .')
		$phone1= '';
	else
		$phone1= request_get('phone1');

	if( request_get('phone2')== '(717)    .')
		$phone2= '';
	else
		$phone2= request_get('phone2');

	if( request_get('phone3')== '(717)    .')
		$phone3= '';
	else
		$phone3= request_get('phone3');

	//echo $lid; 


if(request_get('ctoc')==1){

		$sql="insert into tps_customer set ".
			" `uid` = '". get_session('LOGIN_ID') ."', ".
			" `title1` = '". request_get('prefname1') ."', ".
			" `fname1` = '". request_get('fname1') ."', ".
			" `lname1` = '". request_get('lname1') ."', ".
			" `title2` = '". request_get('prefname2') ."', ".
			" `fname2` = '". request_get('fname2') ."', ".
			" `lname2` = '". request_get('lname2') ."', ".
			" `add_line1` = '". request_get('addr1') ."', ".
			" `add_line2` = '". request_get('addr2') ."', ".
			" `city` = '". request_get('city') ."', ".
			" `state` = '". request_get('state1') ."', ".
			" `zip` = '". request_get('zip') ."', ".
			" `phonetype1` = '". request_get('phone1_type') ."', ".
			" `phone1` = '". $phone1 ."', ".
			" `phonetype2` = '". request_get('phone2_type') ."', ".
			" `phone2` = '". $phone2 ."', ".
			" `phonetype3` = '". request_get('phone3_type') ."', ".
			" `phone3` = '". $phone3 ."', ".
			" `email1` = '". request_get('email1') ."', ".
			" `email2` = '". request_get('email2') ."', ".
			" `email_opt_in` = '". request_get('eoptin') ."', ".
			" `time_contact` = '". request_get('besttime') ."', ".
			" `lead_type` = '". request_get('ltype') ."', ".
			" `lead_subtype` = '". request_get('lsubtype') ."', ".		
			" `lead_status` = '". request_get('lstatus') ."', ".
			" `lead_asst` = '". request_get('lasst') ."', ".
			" `referred_by` = '". request_get('refby') ."', ".
			" `lead_dealer` = '". request_get('ldeal') ."', ".
			" `lead_in` = STR_TO_DATE('".request_get('lindate') ."','%m/%d/%Y'), ".
			" `oktocall` = STR_TO_DATE('".request_get('lkdate') ."','%m/%d/%Y'), ".
			" `lead_result` = '". request_get('lres') ."', ".
			" `gift` = '". request_get('gift') ."', ".
			" `apptresult` = '". request_get('apptresult') ."', ".
			" `comments` = '". request_get('comment1') ."', ".
			" `created` = '". $timestamp ."', ".
			" `modified` = '". $timestamp ."', ".
			" `created_by` = '". get_session('DISPLAY_NAME') ."', ".
			" `modified_by` = '". get_session('DISPLAY_NAME') ."'";	
		$result=mysql_query($sql) or die(mysql_error());

		}


if( $lid > 0){
		$sql="update tps_lead_card set ".
		" `leadid` = '". get_session('leadid') ."', ".
		" `uid` = '". get_session('LOGIN_ID') ."', ".
		" `title1` = '". request_get('prefname1') ."', ".
		" `fname1` = '". request_get('fname1') ."', ".
		" `lname1` = '". request_get('lname1') ."', ".
		" `title2` = '". request_get('prefname2') ."', ".
		" `fname2` = '". request_get('fname2') ."', ".
		" `lname2` = '". request_get('lname2') ."', ".
		" `add_line1` = '". request_get('addr1') ."', ".
		" `add_line2` = '". request_get('addr2') ."', ".
		" `city` = '". request_get('city') ."', ".
		" `state` = '". request_get('state1') ."', ".
		" `zip` = '". request_get('zip') ."', ".
		" `phonetype1` = '". request_get('phone1_type') ."', ".
		" `phone1` = '". $phone1 ."', ".
		" `phonetype2` = '". request_get('phone2_type') ."', ".
		" `phone2` = '". $phone2 ."', ".
		" `phonetype3` = '". request_get('phone3_type') ."', ".
		" `phone3` = '". $phone3 ."', ".
		" `email1` = '". request_get('email1') ."', ".
		" `email2` = '". request_get('email2') ."', ".
		" `email_opt_in` = '". request_get('eoptin') ."', ".
		" `time_contact` = '". request_get('besttime') ."', ".
		" `lead_type` = '". request_get('ltype') ."', ".
		" `lead_subtype` = '". request_get('lsubtype') ."', ".
		
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `lead_asst` = '". request_get('lasst') ."', ".
		" `referred_by` = '". request_get('refby') ."', ".
		" `lead_dealer` = '". request_get('ldeal') ."', ".
		" `lead_in` = STR_TO_DATE('".request_get('lindate') ."','%m/%d/%Y'), ".
		" `oktocall` = STR_TO_DATE('".request_get('lkdate') ."','%m/%d/%Y'), ".
		" `lead_result` = '". request_get('lres') ."', ".
		" `gift` = '". request_get('gift') ."', ".
		" `apptresult` = '". request_get('apptresult') ."', ".
		" `comments` = '". request_get('comment1') ."', ".
		" `customer_flag` = '". request_get('ctoc') ."', ".
		" `modified` = '". $timestamp ."', ".
		" `modified_by` = '". get_session('DISPLAY_NAME') ."' where id='".$lid."'";	
		$result=mysql_query($sql) or die(mysql_error());

		$sql="delete from tps_events where lead_id='".$lid."'";
		$result=mysql_query($sql) or die(mysql_error());

		$appt_title=request_get('fname1') . " ". request_get('lname1'). " - ".request_get('phone1'). " - ". request_get('email1');
		$appt_title=trim($appt_title);

		$a = strptime(request_get('apptdate'), '%m-%d-%Y');
		$timestamp = mktime(0, 0, 0, $a['tm_mon']+1, $a['tm_mday'], $a['tm_year']+1900);

		$startime = $timestamp + request_get('appttimefrom');
		$endtime =  $timestamp +request_get('appttimeto');
		
		//$startime = request_get('apptdate') + request_get('appttimefrom');
		//$endtime = request_get('apptdate') + request_get('appttimeto');

		$appt_startdate= gmdate("Y-m-d H:i:s", $startime);
		$appt_enddate= gmdate("Y-m-d H:i:s", $endtime);

		$createdby=get_session('LOGIN_USERID');

		$sql="insert into tps_events set ".
				" `lead_id` = '". $lid ."', ".
				" `title` = '". $appt_title ."', ".
				" `allDay` = '0', ".
				" `start` = '". $appt_startdate ."', ".
				" `end` = '". $appt_enddate ."', ".
				" `createdby` = '".$createdby."'";
		//echo $sql;		
		$result=mysql_query($sql) or die(mysql_error());
		$_SESSION['lid']=0;
		set_session('e_flag' , 1);
		header("Location: leadcard.php" );
		exit;
	}
	else{

	/*$sql_gen_leadid="INSERT INTO `leadcard` (`leadcard_id`) VALUES (NULL);";
	$result=mysql_query($sql_gen_leadid) or die(mysql_error());
	$gen_id_lead=mysql_insert_id();
	*/

	$sql="insert into tps_lead_card set ".
		" `uid` = '". get_session('LOGIN_ID') ."', ".
		" `leadid` = '". get_session('leadid') ."', ".
		" `title1` = '". request_get('prefname1') ."', ".
		" `fname1` = '". request_get('fname1') ."', ".
		" `lname1` = '". request_get('lname1') ."', ".
		" `title2` = '". request_get('prefname2') ."', ".
		" `fname2` = '". request_get('fname2') ."', ".
		" `lname2` = '". request_get('lname2') ."', ".
		" `add_line1` = '". request_get('addr1') ."', ".
		" `add_line2` = '". request_get('addr2') ."', ".
		" `city` = '". request_get('city') ."', ".
		" `state` = '". request_get('state1') ."', ".
		" `zip` = '". request_get('zip') ."', ".
		" `phonetype1` = '". request_get('phone1_type') ."', ".
		" `phone1` = '". $phone1 ."', ".
		" `phonetype2` = '". request_get('phone2_type') ."', ".
		" `phone2` = '". $phone2 ."', ".
		" `phonetype3` = '". request_get('phone3_type') ."', ".
		" `phone3` = '". $phone3 ."', ".
		" `email1` = '". request_get('email1') ."', ".
		" `email2` = '". request_get('email2') ."', ".
		" `email_opt_in` = '". request_get('eoptin') ."', ".
		" `time_contact` = '". request_get('besttime') ."', ".
		" `lead_type` = '". request_get('ltype') ."', ".
		" `lead_subtype` = '". request_get('lsubtype') ."', ".
		
		" `lead_status` = '". request_get('lstatus') ."', ".
		" `lead_asst` = '". request_get('lasst') ."', ".
		" `referred_by` = '". request_get('refby') ."', ".
		" `lead_dealer` = '". request_get('ldeal') ."', ".
		" `lead_in` = STR_TO_DATE('".request_get('lindate') ."','%m/%d/%Y'), ".
		" `oktocall` = STR_TO_DATE('".request_get('lkdate') ."','%m/%d/%Y'), ".
		" `lead_result` = '". request_get('lres') ."', ".
		" `gift` = '". request_get('gift') ."', ".
		" `apptresult` = '". request_get('apptresult') ."', ".
		" `comments` = '". request_get('comment1') ."', ".
		" `customer_flag` = '". request_get('ctoc') ."', ".
		" `created` = '". $timestamp ."', ".
		" `modified` = '". $timestamp ."', ".
		" `created_by` = '". get_session('DISPLAY_NAME') ."', ".
		" `modified_by` = '". get_session('DISPLAY_NAME') ."'";	
		
		$result=mysql_query($sql) or die(mysql_error());
		$last_id_lead=mysql_insert_id();

		$appt_title=request_get('fname1') . " ". request_get('lname1'). " - ".request_get('phone1'). " - ". request_get('email1');
		$appt_title=trim($appt_title);

		$a = strptime(request_get('apptdate'), '%m-%d-%Y');
		
		$timestamp = mktime(0, 0, 0, $a['tm_mon']+1, $a['tm_mday'], $a['tm_year']+1900);
		
		$startime = $timestamp + request_get('appttimefrom');
		$endtime =  $timestamp +request_get('appttimeto');

		$appt_startdate= gmdate("Y-m-d H:i:s", $startime);
		$appt_enddate= gmdate("Y-m-d H:i:s", $endtime);
		
		$createdby=get_session('LOGIN_USERID');

		/*$sql="insert into tps_events set ".
				" `lead_id` = '". $gen_id_lead ."', ".
				" `title` = '". $appt_title ."', ".
				" `allDay` = '0', ".
				" `start` = '". $appt_startdate ."', ".
				" `end` = '". $appt_enddate ."', ".
				" `createdby` = '".$createdby."'";

		$result=mysql_query($sql) or die(mysql_error());*/

		set_session('e_flag' , 1);
		
		header("Location: leadcard.php" );
		exit;
	}
}

include "lcas_header.php";
include "lcas_top_nav.php";

//include "lcas_left_nav.php";

/*for displaying save messsage
if ( get_session('e_flag') == 1 ) 
{
	unset($_SESSION['e_flag']);
	echo "<a class='growl-success'></a>";
	echo '<script>$(document).ready(function () {
	 $(\'a.growl-success\').click();
	});</script>';
}*/
?>

<script type="text/javascript" src="javascripts/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/jquery.timepicker.css" />

<script type="text/javascript" src="javascripts/base.js"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/base1.css" />
<script src="javascripts/datepair.js"></script> 


<script src="javascripts/formatter.js"></script> 

<link href="stylesheets/multiple-select.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/jquery.multiple.select.js"></script> 

<script  type="text/javascript" >
var typeval="";
$(document).ready(function() {

    $('#zip').blur(function() {
        $.ajax({
            type: "POST",
            url: "zipret.php",
            data: {
                zip: $("#zip").val()
            },
            success: function(result) {
		
		var obj = jQuery.parseJSON (result);
		if (obj.status == "TRUE") {
			$('#city').val(obj.city);
			$('#state1').val(obj.state);
			
		}
		if (obj.status == "FALSE") {
			$('#city').val("");
			$('#state1').val("PA");
		}
            }
        });
    });

$('#ltype').change(function() {
        $.ajax({
            type: "POST",
	    dataType: "json",
            url: "subtyperet.php",
            data: {
		
                ltype: $("#ltype").val()
            },
            success: function(result) {
		$('#lsubtype').empty();
		$(result).each(function () {
		    // Create option
		    var $option = $('<option>');
		    // Add value and text to option

		    $option.attr("value", this.sublvalue).text(this.subltext);
		    // Add option to drop down list
		    $(lsubtype).append($option);
		});  
		
            }
        });
    });

$('#lsubtype').change(function() {
        $.ajax({
            type: "POST",
	    dataType: "json",
            url: "giftret.php",
            data: {
		
                ltype: $("#ltype").val(),
		lsubtype: $("#lsubtype").val()
            },
            success: function(result) {
		
		$('#gift').empty();
		$(result).each(function () {
		    // Create option
		    var $option = $("<option >");
		    // Add value and text to option
		    $option.attr("value", this.value).text(this.text);
		    // Add option to drop down list
		    $(gift).append($option);

		});            
            }
        });
    });

 $('.firstli').change(function() {
 //alert("echeck");
	var $txtval1 = $('#prefname1').val();
	$('.first').empty();
	$('.first').append(' ');

	if($txtval1 != 'Select')
		$('.first').append($txtval1);
	
	$('.first').append(' ');

	var $txtval2 = $('#fname1').val();
	$('.first').append($txtval2);

	var $txtval3 = $('#lname1').val();

	if(($('.first').html() !='') ){	
		$('.first').append(' ');
	}

	$('.first').append($txtval3);

	if(($('.first').html() !='') ){
		if($('#fname2').val() != '' )
			$('.first').append(' - ');
	}

	/*if(($('.first').html() !='') ){
		$('.first').append(' - ');
	}*/

	var $txtval7 = $('#addr1').val();

	$('.first').append($txtval7);
	$('.first').append(' ');

	var $txtval8 = $('#addr2').val();
	$('.first').append($txtval8);
	
	var $txtval9 = $('#city').val();
	
	if(($('.first').html() !='') ){	
		$('.first').append(' ');
	}

	$('.first').append($txtval9);

	if(($('.first').html() !='') ){	
		$('.first').append(' ');
	}

	var $txtval11 = $('#state1').val();
	$('.first').append($txtval11);
	
	if(($('.first').html() !='') ){	
		$('.first').append(' ');
	}

	var $txtval10 = $('#zip').val();
	$('.first').append($txtval10);

	if(($('.first').html() !='') ){	
		$('.first').append(' - ');
	}

	var $txtval12 = $('#phone1').val();
	
	if( $txtval12  != '(717) . ')
	
	$('.first').append($txtval12);
	$('.first').append('</a>');

	if($('.first').html() !=''){
		$('.first').css('display','block');
	}	

	var txt=$('.first').html();

	//alert(txt);
    });


});
</script>
<script>
function myFunction()
{
	if(document.getElementById("chkemail1").checked==true && document.getElementById("chkemail2").checked==true ){
		document.getElementById("eoptin").value=3;
	}
	else if(document.getElementById("chkemail1").checked==true){
		document.getElementById("eoptin").value=1;
	}
	else if(document.getElementById("chkemail2").checked==true){

		document.getElementById("eoptin").value=2;
	}
	else{
		document.getElementById("eoptin").value='';
	}
}

function threehr()
{
	var curr_time = document.getElementById("appttimefrom").value;
	var to_time = parseInt(curr_time) + parseInt("10800");
	if ( parseInt(to_time) > parseInt("84600") ) {
		 to_time = "84600";
	}
	document.getElementById("appttimeto").value = to_time;
	
}

function validateEmail(email, err) {
  var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
 if(document.getElementById(email).value != '')
 {
	  if( emailReg.test(document.getElementById(email).value) == false ) {
		document.getElementById(err).style.display="block";
	    return false;
	  } else {
			document.getElementById(err).style.display="none";
	    return true;
	  } 
  }
}
function setapptres() {
 if(document.getElementById("apptdate").value != '')
 {
	document.getElementById("apptresult").value ='SET'
  }
}
function ctoccheck() {
	 if(document.getElementById("icheck1").checked)
	 {
		alert("This Lead will be converted to customer");
	 }
	 return true;
}

</script>
<script type="text/javascript">
<!--//
function sizeFrame() {
var F = document.getElementById("myFrame");
//if(F.contentDocument) {
//F.height = F.contentDocument.documentElement.scrollHeight+30; //FF 3.0.11, Opera 9.63, and Chrome
//} else {
//F.height = F.contentWindow.document.body.scrollHeight+30; //IE6, IE7 and Chrome
//}

}

window.onload=sizeFrame;

//-->
</script>
<div class="main-content" style="margin:0px;">
<div class="container">
<br /><br />

<div class="row">
<div class="col-md-5" id='external-events'>
<div class="accordion" id="accordion2">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
        <span class="title"><i class="icon-th-list"></i> Name and contact info</span>
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse in">
      <div class="accordion-inner padded">

<form id="frmaddleads" name="frmaddleads" action="add_leads.php" method="POST">
	<select name="prefname1" id="prefname1" class="nline firstli" tabindex="1">
		<?php echo getNamePrefix($title1); ?>
	</select>
	<input type="text" class="validate[required] firstli" data-prompt-position="topLeft" name="fname1" id="fname1" tabindex="2" value="<?php echo $fname1; ?>" placeholder="First Name" required/>
		<input class="nline firstli" type="text" name="lname1" id="lname1" tabindex="3" value="<?php echo $lname1; ?>" placeholder="Last Name" required/>
		<br/>

		<select class="nline firstli" name="prefname2" id="prefname2" tabindex="4" >
		<?php echo getNamePrefix($title2); ?>
		</select>
		<input type="text" class="validate[required] firstli" data-prompt-position="topLeft" name="fname2" tabindex="5" value="<?php echo $fname2; ?>" id="fname2" placeholder="First Name" required/>
		<input class="nline firstli"type="text" name="lname2" id="lname2" tabindex="6" value="<?php echo $lname2; ?>" placeholder="Last Name" required/>
		<br/>

		<input  class="nline firstli" type="text" name="addr1" id="addr1" tabindex="7" value="<?php echo $add_line1; ?>" placeholder="Address Line1" style="width:210px;"/>
		<input class="nline firstli" type="text" name="addr2" id="addr2" tabindex="8" value="<?php echo $add_line1; ?>" placeholder="Address Line2"/>
		<br/>
		<input class="nline firstli" type="text" name="zip" id="zip" placeholder="Zipcode" tabindex="9" value="<?php echo $zip;?>" style="width:120px;"/>
		<input class="nline firstli" type="text" name="city" id="city" value="<?php echo $city; ?>" placeholder="City"/>
<div id="drag-lead-list" class="col-lg-6 nline" style="border:1px solid #DBDBDB;float:right;padding:0px;height:200px;">
		 <div class="accordion-heading" style="padding:5px;">Drag and Drop for Appointment</div>
		 <ul class="nav nav-list">

			<li><a id=""  class="external-event first"> </a> </li>
			
<!--

			<li id="second" style="display:none;" ></li>
			<li id="third"  style="display:none;"></li>
			<li id="fourth"  style="display:none;"></li>
        	<?php
		      	$sql="select * from tps_lead_card 
		      		order by modified DESC
		      		limit 6";
						$result=mysql_query($sql) or die(mysql_error());
						$row = mysql_fetch_array($result);
						while ($record = mysql_fetch_array($result)) {
							echo "<li><a id=".$record['id']." class='external-event'>".
								$record['fname1']." - ".$record['add_line1']."</a></li>";
						}
		      ?>
		<li id="runtext"></li>
-->
        	</ul>
		</div>

		<select class="nline " name="state1" id="state1" >
		<?php 
			if($state=='') { $state = __DEFAULT_STATE__; }

			echo getStates($state);
		?>
		</select>
		
		<br/>
		<select class="nline " name="phone1_type" id="phone1_type" tabindex="10">
		<?php echo getPhoneType($phonetype1); ?>
		</select>
		<?php
		if($phone1 != '')
			echo '<input class="nline " type="text" name="phone1" tabindex="11" id="phone1" value="'.$phone1.'" placeholder="Phone Number" required />';
		else
			echo '<input class="nline " type="text" name="phone1" tabindex="11" id="phone1" value="(717) . "  placeholder="Phone Number"/>';
		?>
		
		<br />
		<select class="nline" name="phone2_type" tabindex="12">
		<?php echo getPhoneType($phonetype2); ?>
		</select>
		<?php
		if($phone1 != '')
			echo '<input class="nline" type="text" name="phone2" tabindex="13" id="phone2" value="'.$phone2.'"  placeholder="Phone Number"/>';
		else
			echo '<input class="nline" type="text" name="phone2" tabindex="13" id="phone2" value="(717) . "  placeholder="Phone Number"/>';
		?>
		
		<br />

		<select class="nline" name="phone3_type" tabindex="14">
		<?php echo getPhoneType($phonetype3); ?>
		</select>
		<?php
		if($phone1 != '')
			echo '<input class="nline" type="text" name="phone3" tabindex="15" id="phone3" value="'.$phone3.'"  placeholder="Phone Number"/>';
		else
			echo '<input class="nline" type="text" name="phone3" tabindex="15" id="phone3" value="(717) . "  placeholder="Phone Number"/>';
		?>
		
		<br />		

		
		<input class="nline firstli" type="text" tabindex="16" name="email1" id="email1"  value="<?php echo $email1; ?>" onblur="validateEmail('email1', 'erremail1')"  placeholder="Email-Id" required />
		<?php 
		if($email_opt_in==1 || $email_opt_in==3)
			echo '<input type="checkbox" tabindex="17" onclick="myFunction()" checked="checked"  id="chkemail1" value="email1" >';
		else
			echo '<input type="checkbox" tabindex="17" onclick="myFunction()"   id="chkemail1" value="email1" >';
		?>
		<label for="email1_optin">Opt In</label>
		<br/>
		<div class="alert alert-error" id="erremail1" style="display:none;">
		Email Format should be
		<strong>test@test.com</strong>
		</div>
		
		<input class="nline" type="text" name="email2" value="<?php echo $email2; ?>" id="email2" onblur="validateEmail('email2','erremail2')"  placeholder="Email-Id"/>
		<?php
		if($email_opt_in==2 || $email_opt_in==3)
			echo '<input type="checkbox" onclick="myFunction()" checked="checked" id="chkemail2" value="email2" >';
		else
			echo '<input type="checkbox" onclick="myFunction()"  id="chkemail2" value="email2" >';
		?>
		<label for="email2_optin">Opt In</label>

		<div class="alert alert-error" id="erremail2" style="display:none;">
		Email Format should be
		<strong>test@test.com</strong>
		</div>

		<input type="hidden" name="eoptin" id="eoptin" placeholder="eoptin">
		<br/>
		<span  class="nline">Best Time to Contact :</span> 

		<select class="nline" name="besttime" id="besttime" >
			<?php echo getDDBestTime($time_contact); ?>
		</select>
	 <p></p>
<!--
		<p class="nline" >Appointment details : 

		<span class="datepair nline" data-language="javascript" >
				<input type="text" name="apptdate" value="<?php echo $apptdate; ?>" id="apptdate" class="date start nline"  placeholder="date"  onchange="setapptres()"/>	
		</span> 

		<select class="nline" name="appttimefrom" id="appttimefrom" onchange="threehr()" >
			
			<?php 
			if($appttimefrom!='' )
				
				echo getDDTime($appttimefrom);
			else
				
				echo getDDTime(__DEFAULT_TIME__); 
			 ?>
		</select><span class="nline"> to </span>

		<select class="nline" name="appttimeto" id="appttimeto" >
			<?php 
			if($appttimeto !='' )

				echo getDDTime($appttimeto);
			else

				echo getDDTime(__DEFAULT_TIME__ + __APPOINTMENT_HRS__); 
			?>
		</select>
		</p> 
-->

	<!---
		<div class="nav-menu">
		  <ul class="nav nav-list">
		        <li><a class='external-event'>Appointment 1 - Drag and drop to calendar</a></li>
		        <li><a class='external-event'>Appointment 2 - Drag and drop to calendar</a></li>
		   </ul>
		</div>
	--->

	<label  class="nline" style="width:150px;font-weight:normal;">Lead Type :</label> 
	<select class="nline" name="ltype" id="ltype" >
	<?php echo getLeadType($ltype); ?>
	</select> <br />
	<label  class="nline" style="width:150px;font-weight:normal;">Lead SubType :</label> 
	<select class="nline" name="lsubtype" id="lsubtype" >
	<?php 
	if($lsubtype !='' )
		echo getLeadSubType($lsubtype,$ltype); 
	else
		echo getLeadSubType(__DEFAULT_LTYPE__,__DEFAULT_LSUBTYPE__); 
	?>
	</select> <br />

	<label class="nline" style="width:150px;font-weight:normal;" >Gift Choice :</label>

		<select class="nline" name="gift" id="gift">
		<?php 
		if($ltype !='' )
			echo getGift($ltype,$gift);
		else
			echo getGift(__DEFAULT_LTYPE__);
		?>
		</select><br />

	
	<label  class="nline" style="width:150px;font-weight:normal;">Referred By :</label> 
	<input class="nline" type="text" name="refby" id="refby" value="<?php echo $refby; ?>" placeholder="Referred By"/><br />
	<label  class="nline" style="width:150px;font-weight:normal;">Lead Dealer :</label> 
		<select class="nline" name="ldeal" id="ldeal">
		<?php
		 echo getTeam($ldeal);
		 ?>
		</select><br />
	<label  class="nline" style="width:150px;font-weight:normal;">Lead Asst :</label> 
	<select class="nline" name="lasst" id="lasst">
		<?php
		 echo getTeam($lasst);
		 ?>
		</select><br />
	<label  class="nline" style="width:150px;font-weight:normal;">Lead In Date :</label> 
	<span class="datepair nline" data-language="javascript" style="margin:0px;padding:0px;" >
	<?php
	if($lindate ==''){
		$lindate=date("m/d/Y");
	}
	?>
	<input class="date start nline" type="text" name="lindate" id="lindate" value="<?php echo $lindate; ?>"  placeholder="Lead In Date" style="width:154px;"/> 
	</span><br />
	<label  class="nline" style="width:150px;font-weight:normal;">Start Contacting On  :</label>
	<span class="datepair nline" data-language="javascript" style="margin:0px;padding:0px;"  > 
	<?php
	if($lkdate ==''){
		$lkdate=date("m/d/Y");
	}
	?>

	<input class="date start nline" type="text" name="lkdate" id="lkdate" value="<?php  echo $lkdate; ?>"   placeholder="Start Contacting On" style="width:154px;"/>
	</span><br />

	<label  class="nline" style="width:150px;font-weight:normal;">Lead Status :</label> 
		<select class="nline" name="lstatus" id="lstatus">
		<?php 
		if($lstatus !='' )
			echo getLeadStatus($lstatus);
		else
			echo getLeadStatus(__DEFAULT_LSTATUS__);
		?>
		</select><br />
	
	<label  class="nline" style="width:150px;font-weight:normal;">Lead Result :</label> 
		<select class="nline" name="lres" id="lres">
		<?php 
		if($lres !='' )
			echo getLeadResult($lres);
		else
			echo getLeadResult(__DEFAULT_LRESULT__);
		?>
		</select><br />
	
	<label class="nline" style="width:150px;font-weight:normal;"  >Appt Result :</label>
		<input class="nline" type="text" name="apptresult" id="apptresult" value="<?php echo $apptresult; ?>" placeholder="Appt Result"/><br />


		 <textarea placeholder="Notes" rows="3" cols="60" class="nline"  name="comment1" id="comment1"><?php echo $comments;?></textarea><br />

<div class="icheckbox_flat-aero checked nline" style="position: relative;">
<input id="icheck1" class="icheck " type="checkbox"  style="position: absolute; opacity: 0;" value="1"  <?php if($ctoc==1){print "checked='checked'";} ?> name="ctoc"  >
</div>
<label class="" for="icheck1">Convert to Customer</label>
		<div style="text-align:right; ">
		<input type="hidden" name="lid" id="lid" value="<?php echo $lid; ?>" />
		<input type="submit" class="btn btn-blue nline" value="Save" name="savenameinfo" id="savenameinfo" style="margin-right:10%;" onclick="return ctoccheck()" >
            <button type="button" class="btn btn-default" onclick="javascript:window.location='leadcard.php'">Cancel</button>

		</div>
	      
	</form>

		<script>
		      var phoneInput = document.getElementById('phone1');
		      if (phoneInput) {
			new Formatter(phoneInput, {
			    'pattern': '({{999}}) {{999}}.{{9999}}',
			    'persistent': true
			});
		      }
		var phoneInput = document.getElementById('phone2');
		      if (phoneInput) {
			new Formatter(phoneInput, {
			    'pattern': '({{999}}) {{999}}.{{9999}}',
			    'persistent': true
			});
		      }
		var phoneInput = document.getElementById('phone3');
		      if (phoneInput) {
			new Formatter(phoneInput, {
			    'pattern': '({{999}}) {{999}}.{{9999}}',
			    'persistent': true
			});
		      }
		    </script>
      </div>
    </div>
  </div>
 
</div>
<!--
<div class="accordion" id="drag-lead-list">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseTwo">
        <span class="title"><i class="icon-th-list"></i> Leads</span>
      </a>
    </div>
    <div id="collapseTwo" class="accordion-body collapse in">
      <div class="accordion-inner padded">
        <ul class="nav nav-list">
        	<?php
		      	$sql="select * from tps_lead_card 
		      		order by modified DESC
		      		limit 10";
						$result=mysql_query($sql) or die(mysql_error());
						$row = mysql_fetch_array($result);
						while ($record = mysql_fetch_array($result)) {
							echo "<li><a id=".$record['id']." class='external-event'>".
								$record['fname1']." - ".$record['add_line1']."</a></li>";
						}
		      ?>
        </ul>
      </div>
    </div>
  </div>
</div>
-->
</div>
  <div class="col-md-7">
    <div class="box" >
      <div class="box-header"><span class="title">Full calendar</span></div>
      <div class="box-content">
      	<div id="add-lead-calendar"></div>
 	<!-- <iframe src="http://dev.lcasinc.net/login.php" id = "myFrame" scrolling="yes" frameborder="1" width="100%" height="100%"></iframe> 
	<br />	<br />	<br />	<br />	<br />	<br />	<br />	<br />	<br />	<br /> -->
      </div>
  </div>
<div align='right'>
	<b>Event Types : <span style='color:#3A87AD'>Demo</span> &nbsp;&nbsp; 
	<span style='color:#0B610B'>Training</span> &nbsp;&nbsp; <span style='color:#FA5858'>Advanced Training Clinics</span></b>
</div>

<br /><br />
   </div>
 </div> 
 </div>

<?php

include "lcas_footer.php";
?>
