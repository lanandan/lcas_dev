<?php
session_start();
require_once("include/tps_db_conn.php");
require_once("include/tps_constants.php");
require_once("include/tps_gen_functions.php");
$error_message='';
$username="";
$password="";
if (isset($_POST['username']) && isset($_POST['password']) ) {
	$username = request_get('username');
	$password = request_get('password');
	$hash_password = md5($password);

	$login_qry = "select * from tps_users where username='$username' and password='$hash_password' and status ='1' ";
	$result = mysql_query($login_qry) or die(mysql_error());

	if (mysql_num_rows($result) == 0 ) 
	{
		$error_flag = 1;
		$error_message .= "Invalid Username (or) password! <br/>Please try again.<br/>";	
		tps_log_error(__INFO__, __FILE__, __LINE__, "Login", "0", "User Login Failed [$username] [$password]");
	}
	else {
		$row=mysql_fetch_array($result);
		set_session('LOGGED_IN', $row['usertype']);
		set_session('LOGIN_ID', $row['id']);
		set_session('LOGIN_ID', $row['id']);
		set_session('LOGIN_USERID', $row['userid']);
		set_session('LOGIN_DEALER', $row['dealer_flag']);
		set_session('LOGIN_PARENTID', $row['parentid']);
		set_session('LOGIN_EMAIL', $row['email']);
		set_session('LOGIN_NAME', $row['username']);
		set_session('LOGIN_LEVEL', $row['level']);
		set_session('LOGIN_DUMMY', $row['dummyid']);
		set_session('DISPLAY_NAME', $row['fname']." ".$row['lname']);

		/*------------ for messages ----------*/
		$_SESSION['userid']=$row['id'];
		$_SESSION['loguserid']=$row['userid'];
		$_SESSION['username']=ucfirst($row['fname'])." ".$r['lname'];
		$_SESSION['email']=$row['email1'];			
		/*------------------------------------*/

		$login_upd_qry = "update tps_users set lastlogin = now()  where id = '".$row['id']."' ";
		mysql_query($login_upd_qry) or die(mysql_error());
		
		$login_ins_qry = "insert into tps_users_login_history (id, username, logintime, ip_addr) values (NULL, '$username', now(), '". $_SERVER['REMOTE_ADDR']."') ";
		mysql_query($login_ins_qry) or die(mysql_error());

		$userid=$row['id'];

		tps_log_error(__INFO__, __FILE__, __LINE__, "Login", $userid, "User Login Success [$username] [$password]");

		header("Location: dashboard.php");
		exit();
						
	}
}
?>


<!doctype html>
<html>
<head>

  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">


  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine or request Chrome Frame -->
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">

  <!-- Use title if it's in the page YAML frontmatter -->
  <title>LCAS Login</title>

<link href="stylesheets/application.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/application.js" type="text/javascript"></script>
<script language="JavaScript">

function submitform()
{
document.frm_login.submit();
}

$(document).ready(function() {
	$(".container").keyup(function(e) {
	   
	    var code = e.keyCode || e.which;
	    if (code==13) {
		$("#submit").click();
	    }
	});
});
</script>

</head>

<body>

<nav class="navbar navbar-default navbar-inverse navbar-static-top" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <a class="navbar-brand" href="#">LCAS Login</a>

    
  </div>

  
</nav>
<div class="container">
  
<div class="col-md-4 col-md-offset-4">


  <div class="padded">
    <div class="login box" style="margin-top: 80px;">

<?php
if ( isset($error_flag) == 1 ) {
?>

<div class="alert alert-error">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Login Failure!</strong> <?php echo $error_message; ?>
</div>
<br/>

<?php

}
?>



      <div class="box-header">
        <span class="title">LCAS Login</span>
      </div>

      <div class="box-content padded">
        <form class="separate-sections" name="frm_login" id="frm_login" action="login.php" method="POST">
          <div class="input-group addon-left">
            <span class="input-group-addon" href="#">
              <i class="icon-user"></i>
            </span>
            <input type="text" name="username" placeholder="username" title="User Name" required  />
          </div>

          <div class="input-group addon-left">
            <span class="input-group-addon" href="#">
              <i class="icon-key"></i>
            </span>
            <input type="password"  name="password" placeholder="password" title="Password" required />
          </div>

          <div>
            <a class="btn btn-blue btn-block" href="#" id="submit" onClick="submitform();" >
                Login <i class="icon-signin"></i> 
            </a> 
          </div>

        </form>

        <div>
          <a href="forgot_pass.php">
              Forgot your password? <strong>Click Here</strong>
          </a>
        </div>

      </div>

    </div>

   </div>
</div>
</div>

<?php
/*
echo "<hr/>";
echo "SESSION ";
var_dump($_SESSION);
echo "<hr/>";
echo "POST Vars";
var_dump($_POST);
echo "<hr/> ";
*/
?>

</body>
</html>
