<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
validate_login();

if($_REQUEST['type'] == 'editlead'){
		
		
		$popupHtml = genPopup();
		$event_edit = array(
			      'popupHtml' => $popupHtml,
			      
			  	);
		echo json_encode($event_edit);


}
if($_REQUEST['type'] == 'editSaveEvent'){
$ltype=$_POST['ltype'];
$lsubtype=$_POST['lsubtype'];
$json=array();
$json['ltype']=$ltype;
$json['lsubtype']=$lsubtype;

echo json_encode($json);
}
function genPopup(){
$ltype='';
$lsubtype='';
$lead_subtype='';
$lead_type='';
$lead_type=getLeadType($ltype);
$lead_subtype=getLeadSubType222($lsubtype,$ltype);



$html= '
  <div class="box" style="height:280px;margin-top:0px;">

 <div class="box-header" ><span class="title">Activity Details </span></div>
	<div  class="msg" id="msg" style="margin-left:auto;float:none;display:none;"> Please Select All the options</div>	
<div align="center">
		<table style="width: 100%; border-spacing: 1px; border-collapse: separate;" id="edit-event-dialog-table" >
		<tbody>
			<tr>
		    <td>Lead Type </td>
                 <td> <select id="ltype" width="200px" onchange="showsubtype(this);" >
                        <option value="">Select</option>
			'.$lead_type.'
			</select> </td></tr>
		 <tr>
		    <td> Lead Subtype: </td> <td><select id="lsubtype" width="200px" required>
                        <option value="">Select</option>
			'.$lead_subtype.'
			</select> </td></tr>
                
                </tbody></table></div> <a id="save" class="btn btn-blue"style="width:100px;margin-left:5px;" >  Save  </a>

     </div>';
	
return $html;	
}

?>

