<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();
$page_name = "Campaign.php";
$page_title = $site_name." Campaign";
$cur_page="Campaig";
$delete_flag='';
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
include "js/campaign.js";
$userid=get_session('LOGIN_USERID');
$ID=get_session('LOGIN_ID');
$today=date('Y-m-d H:i:s');
$child=array($ID);
$re=mysql_query("select id from tps_users where parentid='$userid'");
while($r=mysql_fetch_array($re)){
array_push($child,$r['id']);
}
$child=implode(',',$child);
?>

<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Campaign Listing&nbsp;&nbsp;<a class="btn btn-blue" href="add_campaign.php"><span>Add New Campaign</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="createtask.php"><span>Create Task</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="create_event.php"><span>Create Event</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="#" id="emailit"><span>Email</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="#" id="mapit" ><span>Map It</span></a>	
&nbsp;&nbsp;<button  type="button" id="ddd"class="btn btn-blue" onclick="printpage()">Print</button></h3>
</div> 
<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:50px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
      </div>
    </div>
  </div>

<div class="container">

<div class="box">

<div class="box-header"><span class="title">Campaign Listing</span>

</div>
<div class="box-content">
<div id="dataTables"  style="overflow-x:scroll;">
<table cellpadding="0" cellspacing="0" border="0" class="dTable responsive" id="click" >
<thead>
<tr>
	<th>Action <span style='margin-left:10px;'><input type="checkbox" id="check"  class="icheck1" ></span></th>
	<th style="display:none;"><div>Name</th></div>
	<th style="display:none;"><div>Address</th></div>
	<th style="display:none;"><div>City,State,zip</th></div>
	<th style="display:none;"><div>Phone Num</th></div>
	<th style="display:none;"><div>Comments</th></div>
	<th style="display:none;"><div>Email</th></div>

	<th><div>Lead Type</th></div>
	<th><div>Lead SubType</th></div>
	<th><div>Campaign Name ("Referred By")</th></div>
	<th><div>City</th></div>
	<th><div>Lead Dealer</th></div>
	<th><div>From Date</th></div>
	<th><div>To Date</th></div>
                <th style="display:none;"><div>dprice</th></div>
        <th style="display:none;"><div>cprice</th></div>
        <th style="display:none;"><div>gift1</th></div>
        <th style="display:none;"><div>gift2</th></div>
        <th style="display:none;"><div>gift3</th></div>
        <th style="display:none;"><div>gift4</th></div>
        <th style="display:none;"><div>gift5</th></div>
        <th style="display:none;"><div>gift6</th></div>
        <th style="display:none;"><div>ctype</th></div>
        <th style="display:none;"><div>csubtype</th></div>
         <th style="display:none;"><div>Lead Asst</th></div>
	<th><div>Active</th></div>
	<th><div>Campaign Leads</th></div>
</tr>
</thead>

<?php 
$loginid=get_session('LOGIN_ID');

$sql = "";

if(isset($_REQUEST['type']))
{
	if($_REQUEST['type']=="newtask")
	{
		$viewid=$_REQUEST['id'];
		$sql="select * from tps_campaign where FIND_IN_SET(uid,'$child') and id='$viewid' and delete_flag='0' order by id desc";
		$result=mysql_query($sql) or die(mysql_error());

	}else{
		$sql="select * from tps_campaign where FIND_IN_SET(uid,'$child') and delete_flag='0' order by id desc";
		$result=mysql_query($sql) or die(mysql_error());
	}
}
else{

$sql="select * from tps_campaign where uid = '$loginid' and delete_flag='0' order by id desc";
$result=mysql_query($sql) or die(mysql_error());

}

while($row = mysql_fetch_array($result))
{
$leadtype=getleadtype_name($row['lead_type']);
$leadsubtype=getleadsubtype_name($row['lead_subtype']);
$sql_select="select fname,lname from tps_users where userid='$row[lead_dealer]'";
$result_select = mysql_query($sql_select) or die(mysql_error());
$row1 = mysql_fetch_array($result_select);
?>

			<tr>
			  <td>
	<div class="btn-group">
	      <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button>
	      <ul class="dropdown-menu">
		
		 <li><a href="add_campaign.php?action=edit&lid=<?php print $row['id'];?>"><img src="images/edit.png" height="18px" width="18px"/>&nbsp;&nbsp;Edit Campaign</a></li>
		 <li class="divider"></li>
		 <li><a href="add_campaign.php?action=delete&lid=<?php print $row['id'];?>" onClick="return confirmDelete();"><img src="images/block.png" width="18px"/>&nbsp;&nbsp;Delete</a></li>
	      </ul>
<div style="margin-left:50px;margin-top:5px;"><input type="checkbox" id="icheck1" value="<?php echo $row['id'];?>" class="icheck1" ></div>
	</div>

<!--<a href="campaign_lead.php?id=<?php print $row['id'];?>"><img src=images/view.png  title="View Campaign Leads" height="10px" width="20px"></a>&nbsp;|&nbsp; <a href="add_campaign.php?action=edit&lid=<?php print $row['id'];?>" title="Edit"><img src="images/edit.png"  title="Edit"/></a>&nbsp;|&nbsp;
 	<a href="add_campaign.php?action=delete&lid=<?php print $row['id'];?>" title="Delete" onClick="return confirmDelete();" > <img src="images/block.png" width="15px" class="key_image" title="Delete"/></a> -->

 </td>
 <td style="display:none;"><?php echo $row['title']." ".$row['fname']." ".$row['lname']; ?></td>
 <td style="display:none;"><?php echo $row['add_line']; ?></td>
 <td style="display:none;"><?php echo $row['city']." ".$row['state']." ".$row['zip']; ?></td>
 <td style="display:none;"><?php echo $row['phone1'].",".$row['phone2']; ?></td>
 <td style="display:none;"><?php echo $row['comments']; ?></td>
<td style="display:none;"><?php echo $row['email1']; ?></td>
<td><?php echo $leadtype; ?></td>
<td><?php echo $leadsubtype; ?></td>
<td><?php echo $row['campaign']; ?></td>
<td><?php echo $row['city']; ?></td>
<td><?php echo ucfirst($row1['fname'])." ".ucfirst($row1['lname']);?></td>
<td><?php echo date('m/d/Y',strtotime($row['start_date'])); ?></td>
<td><?php echo date('m/d/Y',strtotime($row['expiration_date'])); ?></td>
<td style="display:none;"><?php echo $row['drawing_prize']; ?></td>
<td style="display:none;"><?php echo $row['campaign_prize']; ?></td>
<td style="display:none;"><?php echo $row['gift1']; ?></td>
<td style="display:none;"><?php echo $row['gift2']; ?></td>
<td style="display:none;"><?php echo $row['gift3']; ?></td>
<td style="display:none;"><?php echo $row['gift4']; ?></td>
<td style="display:none;"><?php echo $row['gift5']; ?></td>
<td style="display:none;"><?php echo $row['gift6']; ?></td>
<td style="display:none;"><?php echo $row['campaign_type']; ?></td>
<td style="display:none;"><?php echo $row['campaign_subtype']; ?></td>
<td style="display:none;"><?php echo $row['lead_asst']; ?></td>
			  <td><?php if($row['active']=='1'){echo "Yes"; } else { echo "No"; } ?></td>
<?php 
$ref="2_".$row['id'];

$sql1="select * from tps_lead_card where referred_by='$ref' and delete_flag='0' ";
$result1=mysql_query($sql1) or die(mysql_error());
$r=mysql_num_rows($result1);

$sql2="select * from tps_lead_card where referred_by='$ref' and form_type='Door_Canvas_Form' and delete_flag='0'";
$result2=mysql_query($sql2) or die(mysql_error());
$r1=mysql_num_rows($result2);
?>
<td><a href="campaign_lead.php?id=<?php print $row['id'];?>"style="border-bottom: 3px double;" class="link">(<?php echo $r;?>)</a></td>
			</tr>
<?php } ?>
		</table>

	 </div>  
     </div>
    </div>

	    
		<div class="row">
  <div class="col-md-4" style="width:32%">
    <div class="box">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Name & Address</span>
      </div>
      <div class="box-content padded">

		<div id="campaign_info"></div>
                                <p id="campaign_name"></p>
       				<p id="name"></p>
				<p id="addr"></p>
				<p id="addr2"></p>
				<p id="phone"></p>
				<p id="email"></p>
                                <p id="notes"></p>
        
      </div>
    </div>
  </div>
   <div class="col-md-4" style="width:32%">
    <div class="box">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i>Campaign Details</span>
      </div>
      <div class="box-content padded">

		<div id="campaign_details"></div>

       				<p id="ltype"></p>
                                <p id="lsubtype"></p>
                                <p id="ctype"></p>
                                <p id="csubtype"></p>
				<p id="ldeal"></p>
                                <p id="leadassis"></p>
				<p id="sdate"></p>
				<p id="edate"></p>
				

        
      </div>
    </div>
  </div>
 
  <div class="col-md-4" style="width:32%">
    <div class="box">
      <div class="box-header">
        <span class="title"><i class="icon-th-list"></i> Campaign Incentive</span>
      </div>
      <div class="box-content padded">
	
		<div id="campaign_incentive"></div>

				<p id="drawingprice"></p>
				<p id="campaignprice"></p>
				<p id="gift11"></p>
				<p id="gift12"></p>
                                <p id="gift13"></p>
                                <p id="gift14"></p>
                                <p id="gift15"></p>
                                <p id="gift16"></p>
      </div>
    </div>
  </div>
</div>


  </div>

<?php
include "lcas_footer.php";
?>

