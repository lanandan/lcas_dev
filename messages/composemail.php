<?php
session_start();
$pagetitle="Message :: Compose Mail ";
$pageno=0;
include('header.php');
include('genfunctions.php');

?>
<script type="text/javascript">
	window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>
<style type="text/css">

.uploadifive-button {
	float: left;
	margin-right: 10px;
}
#queue {
	border: 1px solid #E5E5E5;
	height: 100px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 80%;
}
.ui-dialog-titlebar-close{
	display:none;
}
</style>

<link rel="stylesheet" href="assets/css/jquery-ui-1.10.3.full.min.css" />
<script src="assets/js/jquery-ui-1.10.3.full.min.js"></script>

<link rel="stylesheet" href="assets/css/select2.css" />
<script src="assets/js/select2.min.js"></script>

<link rel="stylesheet" type="text/css" href="assets/uploadify/uploadifive.css">
<script src="assets/uploadify/jquery.uploadifive.min.js" type="text/javascript"></script>

<div class="main-content">
<div class="breadcrumbs" id="breadcrumbs">
	<script type="text/javascript">
		try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
	</script>

	<ul class="breadcrumb">
		<li>
			<i class="icon-home home-icon"></i>
			<a href="../dashboard.php">Home</a>
		</li>
		<li class="active">Messages</li>
	</ul><!-- .breadcrumb -->
</div>

<script type="text/javascript">
$(document).ready(function(){

<?php $timestamp = time();?>

$('#file_upload').uploadifive({
	'auto'             : true,
	'formData'         : {
				   'timestamp' : '<?php echo $timestamp;?>',
				   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
	                     },
	'queueID'          : 'queue',
	'fileSizeLimit'    : '2MB',
	'uploadScript'     : 'uploadifive.php?job=compose&tabname=inbox',
	'onProgress'   	   : function(file, e) {
			    if (e.lengthComputable) {
				var percent = Math.round((e.loaded / e.total) * 100);
			    }
			    file.queueItem.find('.fileinfo').html(' - ' + percent + '%');
			    file.queueItem.find('.progress-bar').css('width', percent + '%');
        }, 
	'onUploadComplete' : function(file, data) {

				$('#output').append(data+',');

				document.getElementById('upfileids').value = $('#output').html();

				file.queueItem.find('.close').html(data);
				
			}
	
});


$('#btnsend').click(function(){

	var recpt=$('#sendto').val();
	var subject=$('#subject').val();
	var message=$('#message').html();
	var attids=$('#upfileids').val();

	var draftid=0;
	
	if(recpt!=null)
	{
		var data = {
				type: 'composemail',
				sendto: recpt,
				subject: subject,
				message:message,
				attids:attids,
				draftid:draftid
			 }

			$.ajax({
				type: "POST",
				url: "message_actions.php",
				data: data,
				success: function(resp) {
					alert("Mail Sent Successfully");
					window.location.href="sent.php";
			    	},
			    	error: function() {
					alert('Error while Saving');
			    	},
			});
	}else{
		alert("Please Specify atleast one Recipient ");
	}

});


$('#btnsendmail').click(function(){

	var recpt=$('#sendto1').val();
	var subject=$('#subject').val();
	var message=$('#message').html();
	var attids=$('#upfileids').val();

	var draftid=0;
	
	if(recpt!=null)
	{
		$( "#dialog-message" ).removeClass('hide').dialog({
					modal: true,
					title: "Sending Mail",
					title_html: true,
					height:100
			      });

		var data = {
				type: 'composetaskmail',
				sendto: recpt,
				subject: subject,
				message:message,
				attids:attids,
				draftid:draftid
			 }

			$.ajax({
				type: "POST",
				url: "message_actions.php",
				data: data,
				success: function(resp) {
					
					$( "#dialog-message" ).delay(2000).fadeOut();
					$( "#dialog-message" ).addClass('hide').dialog({
						modal: true,
						title: "Sending Mail",
						title_html: true,
						height:100
			     		 });
					window.location.href="sent.php";
			    	},
			    	error: function() {
					alert('Error while Saving');
			    	},
			});
	}else{
		alert("Please Specify atleast one Recipient ");
	}

});

$('#savedraft').click(function(){
	
	var recpt=$('#sendto').val();
	var subject=$('#subject').val();
	var message=$('#message').html();
	var attids=$('#upfileids').val();

	
	var data = {
			type: 'savedraft',
			sendto: recpt,
			subject: subject,
			message:message,
			attids:attids
		 }

		$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				alert("Mail saved in draft folder successfully");
				window.location.href="draft.php";
		    	},
		    	error: function() {
				alert('Error while Saving');
		    	},
		});
	

});

});

function draftsend(draftid)
{
	var recpt=$('#sendto').val();
	var subject=$('#subject').val();
	var message=$('#message').html();
	var attids=$('#upfileids').val();
	var draftupfileids=$('#draftupfileids').val();

	var attids1=draftupfileids+","+attids;

	var data = {
			type: 'composemail',
			sendto: recpt,
			subject: subject,
			message:message,
			attids:attids1,
			draftid:draftid
		 }

		$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				alert("Mail Sent Successfully");
				window.location.href="sent.php";
		    	},
		    	error: function() {
				alert('Error while Saving');
		    	},
		});
}

function delatt(aid,draftid)
{
	var data = {
			type: 'deldraftatt',
			aid: aid,
			draftid:draftid
		 }
	
	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				//alert("Mail Sent Successfully");
				window.location.href="composemail.php?view=frmdraft&id="+draftid;
		    	},
		    	error: function() {
				alert('Error while Saving');
		    	},
		});
}

function discard(draftid)
{
	var data = {
			type: 'discarddraft',
			draftid:draftid
		 }
	
	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				alert("Draft Discarded Successfully");
				window.location.href="composemail.php";
		    	},
		    	error: function() {
				alert('Error while Saving');
		    	},
		});

}

</script>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->

<div id="dialog-message" class="hide">
<img src="assets/css/images/loading.gif"> Please Wait...
</div>

<div class="row">
<div class="col-xs-12">
<div class="tabbable">
<?php 
include('mailmenu.php');

$sendto="";
$subject="";
$message="";
$attachments="";

$isdraft=0;
$draft_id=0;

$istask=0;

if(isset($_REQUEST['view']))
{

	if($_REQUEST['view']=='frmdraft')
	{
		$draft_id=$_REQUEST['id'];
		$res=mysql_query("select * from draft where id='$draft_id'")or die(mysql_error());
		$r=mysql_fetch_array($res);
	
		$sendto=$r['to_userids'];
		$subject=$r['subject'];
		$message=$r['body'];
		
		$attachments=$r['attachment_ids'];

		$isdraft=1;
	
	}
}

if(isset($_REQUEST['type']))
{
	if($_REQUEST['type']=="newtask")
	{
		$istask=1;

		$who=$_REQUEST['who'];
		
		$a=explode("-",$who);
	
		$b=explode("_",$a[0]);
		
		$table=$b[0];
		$id=$b[1];

		$who_sql="";

		if($table=="1") // for campaign
		{
			$who_sql="select email1 from `tps_campaign` where id='$id'";
		}
		else if($table=="2") // for Customer
		{
			$who_sql="select email1,email2 from tps_lead_card where customer_flag='1' and id='$id'";
		}
		else if($table=="3") // for Leads
		{
			$who_sql="select email1,email2 from `tps_lead_card` where id='$id'";
		}
		else if($table=="4") // for Team Members
		{
			$who_sql="select email1,email2 from `tps_users` where id='$id'";
		}

		$wres=mysql_query($who_sql)or die(mysql_error());

		$wr=mysql_fetch_array($wres);

		$tolist="";

		if($table=="1")
		{
			$tolist=$wr['email1'];
		}
		else if($table=="2")
		{
			$tolist=$wr['email1'].",".$wr['email2'];
		}
		else if($table=="3")
		{
			$tolist=$wr['email1'].",".$wr['email2'];
		}
		else if($table=="4")
		{
			$tolist=$wr['email1'].",".$wr['email2'];
		}
		else{
			$tolist=$wr['email1'];
		}
		
	}

	if($_REQUEST['type']=="leadmail")
	{
		$istask=1;
		$lids=$_REQUEST['leadid'];
		$table=$_REQUEST['tbl'];

		if($table=="1") // for campaign
		{
			$who_sql="select email1 from `tps_campaign` where FIND_IN_SET( `id`, '$lids' )";
		}
		else if($table=="2") // for Customer
		{
			$who_sql="select email1,email2 from tps_lead_card where customer_flag='1' and FIND_IN_SET( `leadid`, '$lids' )";
		}
		else if($table=="3") // for Leads
		{
			$who_sql="select email1,email2 from `tps_lead_card` where FIND_IN_SET( `leadid`, '$lids' )";
		}
		else if($table=="4") // for Team Members
		{
			$who_sql="select email1,email2 from `tps_users` where id='$lids'";
		}

		$wres=mysql_query($who_sql)or die(mysql_error());

		//$wr=mysql_fetch_array($wres);

		$tolist="";
		$toListArray=array();
		if($table=="1")
		{
			$i=0;
			while($wr=mysql_fetch_array($wres))
			{
				$toListArray[$i]=$wr['email1'];
				$i++;
			}

			$tolist=implode(",",$toListArray);
		}
		else if($table=="2")
		{
			$i=0;
			while($wr=mysql_fetch_array($wres))
			{
				$toListArray[$i]=$wr['email1'].",".$wr['email2'];
				$i++;
			}
			$tolist=implode(",",$toListArray);
			
		}
		else if($table=="3")
		{
			$i=0;
			while($wr=mysql_fetch_array($wres))
			{
				$toListArray[$i]=$wr['email1'].",".$wr['email2'];
				$i++;
			}
			$tolist=implode(",",$toListArray);
		}
		else if($table=="4")
		{
			$tolist=$wr['email1'].",".$wr['email2'];
		}
		else{
			$tolist=$wr['email1'];
		}
			
	}
}

?>
<div id="id-message-new-navbar" class="message-navbar align-center clearfix">
<div class="message-bar">
	<div class="message-toolbar">
	<?php if($istask==0){  ?>
		<a href="#" class="btn btn-xs btn-message" id="savedraft">
			<i class="icon-save bigger-125"></i>
			<span class="bigger-110">Save Draft</span>
		</a>
	<?php } ?>
	<?php if($isdraft==1){ ?>
		<a href="#" onclick="javascript:discard(<?php echo $draft_id; ?>)" class="btn btn-xs btn-message">
			<i class="icon-remove bigger-125"></i>
			<span class="bigger-110">Discard</span>
		</a>
	<?php } ?>
	</div>
</div>

<div class="message-item-bar">
	<div class="messagebar-item-left">
		<a href="inbox.php" class="btn-back-message-list no-hover-underline">
			<i class="icon-arrow-left blue bigger-110 middle"></i>
			<b class="middle bigger-110">Back</b>
		</a>
	</div>

	<div class="messagebar-item-right">
		<span class="inline btn-send-message">
			
		</span>
	</div>
</div>
</div>

<form id="id-message-form" class="form-horizontal message-form  col-xs-12" enctype="multipart/form-data">
<div class="">
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-recipient">To:</label>

		<div class="col-sm-9 col-xs-12">

		<?php 
		if($istask!=0)
		{
		?>
<input type="text" name="sendto1" id="sendto1" value="<?php echo $tolist; ?>" tabindex="1" placeholder="More Recipient... "/>			
		<?php 
		}else{
		?>		
			<select multiple="" tabindex="1" class="width-70" id="sendto" data-placeholder="Choose a Recipient...">
			<?php
				if($isdraft==0)
				{
					$sel="0";
					echo get_recipient_list($sel);
				}else{
					echo get_recipient_listAll($sendto);
				}
			?>		
			</select>
		<?php
		}
		?>	
		</div>
	</div>

	<div class="hr hr-18 dotted"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-subject">Subject:</label>

		<div class="col-sm-6 col-xs-12">
			<div class="input-icon block col-xs-12 no-padding">
<input maxlength="100" type="text" class="col-xs-12" name="subject" id="subject" tabindex="2" placeholder="Subject" value="<?php echo $subject; ?>" />
				<i class="icon-comment-alt"></i>
			</div>
		</div>
	</div>

	<div class="hr hr-18 dotted"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right">
			<span class="inline space-24 hidden-480"></span>
			Message:
		</label>

		<div class="col-sm-9">
			<div class="wysiwyg-editor" tabindex="3" id="message"><?php echo html_entity_decode($message); ?></div>
		</div>
	</div>

	<div class="hr hr-18 dotted"></div>

	<div class="form-group no-margin-bottom">
		<label class="col-sm-3 control-label no-padding-right">Attachments:</label>

		<div class="col-sm-9">
<?php
if($attachments!="")
{
?>

	<div class="message-attachment clearfix">
	
	<ul class="attachment-list pull-left list-unstyled">

<?php
	$imgid=explode(',',$attachments);	
	for($j=0;$j<count($imgid);$j++)
	{
		$delid=$imgid[$j];
		if($imgid[$j]!="")
		{
			$upimgres=mysql_query("select filepath from attachments where id='".$imgid[$j]."'") or die(mysql_error());

			$upimgr=mysql_fetch_array($upimgres);
			echo '<li>
			<a href="download.php?type=download&filename='.$upimgr['filepath'].'" class="attached-file inline">
				<i class="icon-file-alt bigger-110 middle"></i>
				<span class="attached-name middle">'.basename($upimgr['filepath']).'</span>
			</a>
			<div class="action-buttons inline">
				<a href="download.php?type=download&filename='.$upimgr['filepath'].'">
					<i class="icon-download-alt bigger-125 blue"></i>
				</a> 
		&nbsp; <a href="#" onclick="javascript:delatt('.$delid.','.$draft_id.')"> <i class="icon-trash bigger-125 red"></i></a>
			</div>
			</li>';

		}
		
	}

?>
	</ul>

	</div>	<br>
<?php
}
?>
			<div id="queue">Drag & Drop Image Files Here</div>

			<input id="file_upload" name="file_upload" type="file" multiple="true">
			<div id="output" style="display:none;"></div>
			<input type="hidden" name="upfileids" id="upfileids">
			<?php 
			if($isdraft==1)
			{
				echo '<input type="hidden" name="draftupfileids" id="draftupfileids" value="'.$attachments.'">';
			}	
			?>
		</div>
	</div>

	<div class="hr hr-18 dotted"></div>
	
	<div class="form-group no-margin-bottom">
		<label class="col-sm-3 control-label no-padding-right"></label>
		<div class="col-sm-9">
	<?php 
	if($istask!=0)
	{ ?>
			<button type="button" id="btnsendmail" tabindex="4" class="btn btn-sm btn-primary no-border">
				<span class="bigger-110">Send mail</span>

				<i class="icon-arrow-right icon-on-right"></i>
			</button>
	<?php 
	}
	else{ 
		if($isdraft==0)
		{	
	?>
			<button type="button" id="btnsend" class="btn btn-sm btn-primary no-border">
				<span class="bigger-110">Send</span>

				<i class="icon-arrow-right icon-on-right"></i>
			</button>
	
	<?php }else{ ?>
			<button type="button" onclick="javascript:draftsend(<?php echo $draft_id; ?>)" class="btn btn-sm btn-primary no-border">
				<span class="bigger-110">Send</span>

				<i class="icon-arrow-right icon-on-right"></i>
			</button>
		
	<?php } 

	}
	?>
	</div>
	</div>
	<div class="space"></div>


</div>
</form>
					</div><!-- /.message-container -->
				</div><!-- /.tab-pane -->
			</div><!-- /.tab-content -->
		</div><!-- /.tabbable -->
	</div><!-- /.col -->
</div><!-- /.row -->


				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div><!-- /.main-content -->

<script src="assets/js/bootstrap-tag.min.js"></script>
<script src="assets/js/jquery.hotkeys.min.js"></script>
<script src="assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<style>
.tags{
	width:66% !important;
}
</style>
<script type="text/javascript">
jQuery(function($){

	$("#sendto").select2(); 
	
//intialize wysiwyg editor
	$('.message-form .wysiwyg-editor').ace_wysiwyg({
		toolbar:
		[
			'bold',
			'italic',
			'strikethrough',
			'underline',
			null,
			'justifyleft',
			'justifycenter',
			'justifyright',
			null,
			'createLink',
			'unlink',
			null,
			'undo',
			'redo'
		]
	}).prev().addClass('wysiwyg-style1');

	var tag_input = $('#sendto1');

	if(! ( /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase())) ) 
		tag_input.tag({placeholder:tag_input.attr('placeholder')});

});
</script>
<?php
include('footer.php');
?>
