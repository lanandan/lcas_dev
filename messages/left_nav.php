<script type="text/javascript">
	try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
</script>

<ul class="nav nav-list">
<li>
	<a href="../dashboard.php" class="dropdown-toggle">
		<i class="icon-dashboard"></i>
		<span class="menu-text"> Dashboard </span>
	</a>
</li>
<li>
	<a href="#" class="dropdown-toggle">
		<i class="icon-envelope-alt "></i>
		<span class="menu-text"> Messages </span>

		<b class="arrow icon-angle-down"></b>
	</a>

	<ul class="submenu">
		<li>
			<a href="composemail.php">
				<i class="icon-double-angle-right"></i>
				<span class="menu-text"> Compose Mail </span>
			</a>
		</li>
		<li>
			<a href="inbox.php" class="dropdown-toggle">
				<i class="icon-double-angle-right"></i>
				<span class="menu-text"> Inbox </span>
			</a>
		</li>
		<li>
			<a href="sent.php" class="dropdown-toggle">
				<i class="icon-double-angle-right"></i>
				<span class="menu-text"> Sent Items </span>
			</a>
		</li>
		<li>
			<a href="draft.php" class="dropdown-toggle">
				<i class="icon-double-angle-right"></i>
				<span class="menu-text"> Drafts </span>
			</a>
		</li>
		<li>
			<a href="stared.php" class="dropdown-toggle">
				<i class="icon-double-angle-right"></i>
				<span class="menu-text"> Starred </span>
			</a>
		</li>
		<!-- <li>
			<a href="trash.php" class="dropdown-toggle">
				<i class="icon-double-angle-right"></i>
				<span class="menu-text"> Trash </span>
			</a>
		</li>-->
	</ul>
</li>
<li>
	<a href="../logout.php" class="dropdown-toggle">
		<i class="icon-list"></i>
		<span class="menu-text"> Logout </span>
	</a>
</li>
</ul><!-- /.nav-list -->

<div class="sidebar-collapse" id="sidebar-collapse">
	<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
</div>

<script type="text/javascript">
	try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
</script>

