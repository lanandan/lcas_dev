<?php
session_start();

include('dbconnect.php');
include('genfunctions.php');

if(isset($_REQUEST['type']))
{
	$cur_userid=$_SESSION['userid'];
	$cur_username=$_SESSION['username'];

	if($_REQUEST['type']=="composemail")
	{
		$sendto=implode(",",$_REQUEST['sendto']);
		$subject=htmlentities($_REQUEST['subject'], ENT_QUOTES, "UTF-8");
		$message=htmlentities($_REQUEST['message'], ENT_QUOTES, "UTF-8");
		$attids=$_REQUEST['attids'];
	
		$draftid=$_REQUEST['draftid'];

		if($draftid!=0)
		{
			mysql_query("update draft set status='1' where id='$draftid'")or die("Draft Status : ".mysql_error());
		}
		
		$sendtoids=explode(",",$sendto);
	
		$senttousernames=get_userslist_by_userid($sendto);

		mysql_query("INSERT INTO `msg_seqid` (`id`) VALUES (NULL);");

		$msg_seqid=mysql_insert_id();
		
		$inbox_sql_frmusr="INSERT INTO `inbox` (`id`, `msg_seqid`, `msguserid`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msg_seqid', '$cur_userid', '$cur_userid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'inbox', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

		mysql_query($inbox_sql_frmusr)or die("Inbox : ".mysql_error());


		for($i=0;$i<count($sendtoids);$i++)
		{
			$sender=$sendtoids[$i];

	$inbox_sql="INSERT INTO `inbox` (`id`, `msg_seqid`, `msguserid`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msg_seqid', '$sender', '$cur_userid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'inbox', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

		mysql_query($inbox_sql)or die("Inbox : ".mysql_error());

		}
	
	}// end composemail

	//composetaskmail

	if($_REQUEST['type']=="composetaskmail")
	{
		$sendto=$_REQUEST['sendto'];
		$subject=$_REQUEST['subject'];
		$message=$_REQUEST['message'];
		$attids=trim($_REQUEST['attids']);
	
		$sendtoids=explode(",",$sendto);
	
		mysql_query("INSERT INTO `msg_seqid` (`id`) VALUES (NULL);");

		$msg_seqid=mysql_insert_id();
		
		$inbox_sql_frmusr="INSERT INTO `inbox` (`id`, `msg_seqid`, `msguserid`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msg_seqid', '$cur_userid', '$cur_userid', '$cur_username', '$sendto', '$sendto', '$subject', '$message', '$attids', 'inbox', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

		mysql_query($inbox_sql_frmusr)or die("Inbox : ".mysql_error());

		for($i=0;$i<count($sendtoids);$i++)
		{
			$sender=$sendtoids[$i];

			send_lcas_mail_with_smtp($sender, $subject, $message, $attids);
	
		}
	
	}// end composemail


	if($_REQUEST['type']=="savedraft")
	{
		$sendto=implode(",",$_REQUEST['sendto']);
		$subject=htmlentities($_REQUEST['subject'], ENT_QUOTES, "UTF-8");
		$message=htmlentities($_REQUEST['message'], ENT_QUOTES, "UTF-8");
		$attids=$_REQUEST['attids'];
		
		$sendtoids=explode(",",$sendto);
	
		$senttousernames=get_userslist_by_userid($sendto);

		mysql_query("INSERT INTO `msg_seqid` (`id`) VALUES (NULL);");

		$msg_seqid=mysql_insert_id();
		
		$inbox_sql_frmusr="INSERT INTO `draft` (`id`, `msg_seqid`, `msguserid`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msg_seqid', '$cur_userid', '$cur_userid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'inbox', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

		mysql_query($inbox_sql_frmusr)or die("Inbox : ".mysql_error());


		for($i=0;$i<count($sendtoids);$i++)
		{
			$sender=$sendtoids[$i];

	$inbox_sql="INSERT INTO `draft` (`id`, `msg_seqid`, `msguserid`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msg_seqid', '$sender', '$cur_userid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'inbox', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

		mysql_query($inbox_sql)or die("Inbox : ".mysql_error());

		}
	
	}// end savedraft

	if($_REQUEST['type']=="replytomail")
	{
		$sendto=implode(",",$_REQUEST['sendto']);
		$subject=htmlentities($_REQUEST['subject'], ENT_QUOTES, "UTF-8");
		$message=htmlentities($_REQUEST['message'], ENT_QUOTES, "UTF-8");
		$orgmsgid=$_REQUEST['orgmsgid'];
		$attids=$_REQUEST['attids'];
		$msg_seqid=$_REQUEST['msg_seqid'];

		$sendtoids=explode(",",$sendto);
		
		$senttousernames=get_userslist_by_userid($sendto);

		mysql_query("INSERT INTO `reply_seqid` (`id`) VALUES (NULL);");

		$reply_seqid=mysql_insert_id();	


		$inbox_sql_frmusr="INSERT INTO `inbox_reply` (`id`, `msg_seqid`, `msguserid`, `reply_seqid`, `inbox_id`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msg_seqid', '$cur_userid', '$reply_seqid', '$orgmsgid', '$cur_userid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'reply', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

		mysql_query($inbox_sql_frmusr)or die("Inbox : ".mysql_error());

		for($i=0;$i<count($sendtoids);$i++)
		{
			$sender=$sendtoids[$i];

	$inbox_sql="INSERT INTO `inbox_reply` (`id`, `msg_seqid`, `msguserid`, `reply_seqid`, `inbox_id`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msg_seqid', '$sender', '$reply_seqid', '$orgmsgid', '$cur_userid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'reply', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

		mysql_query($inbox_sql)or die("Reply : ".mysql_error());
		
		}
	
	} // end replytomail

	if($_REQUEST['type']=="starmsg")
	{
		$curstar=$_REQUEST['curstar'];
		$msgid=$_REQUEST['msgid'];
		$msgtype=$_REQUEST['msgtype'];
		$tablename=$_REQUEST['tablename'];

		$upd_star_sql="";

		if($tablename!="draft")
		{
			if($msgtype=="inbox")
			{
				if($curstar=="0")
				{
					$upd_star_sql="update inbox set star_flag='1' where id='$msgid'";
				}elseif($curstar=="1"){
					$upd_star_sql="update inbox set star_flag='0' where id='$msgid'";
				}
			}
			else if($msgtype=="reply")
			{
				if($curstar=="0")
				{
					$upd_star_sql="update inbox_reply set star_flag='1' where id='$msgid'";
				}elseif($curstar=="1"){
					$upd_star_sql="update inbox_reply set star_flag='0' where id='$msgid'";
				}
			}

			mysql_query($upd_star_sql)or die("UPDATE STAR STATUS : ".mysql_error());
		}else{
			if($curstar=="0")
			{
				$upd_star_sql="update draft set star_flag='1' where id='$msgid'";
			}elseif($curstar=="1"){
				$upd_star_sql="update draft set star_flag='0' where id='$msgid'";
			}
			mysql_query($upd_star_sql)or die("UPDATE STAR STATUS : ".mysql_error());

			echo " starmsg : ".$upd_star_sql;

		}

	} // end starmsg

	if($_REQUEST['type']=="viewstarmsg")
	{
		$curstatus=$_REQUEST['curstatus'];
		$msgseqid=$_REQUEST['msgseqid'];
		$msgid=$_REQUEST['msgid'];
		$replyid=$_REQUEST['replyid'];
		
		$upd_read_sql="";

		if($replyid=="0")
		{
			if($curstatus=="0")
			{
				$upd_read_sql="update inbox set star_flag='0' where msg_seqid='$msgseqid' and id='$msgid'";
			}elseif($curstatus=="1"){
				$upd_read_sql="update inbox set star_flag='1' where msg_seqid='$msgseqid' and id='$msgid'";
			}
		}
		else
		{
			if($curstatus=="0")
			{
		$upd_read_sql="update inbox_reply set star_flag='0' where msg_seqid='$msgseqid' and inbox_id='$msgid' and id='$replyid'";
			}elseif($curstatus=="1"){
		$upd_read_sql="update inbox_reply set star_flag='1' where msg_seqid='$msgseqid' and inbox_id='$msgid' and id='$replyid'";
			}
		}

		mysql_query($upd_read_sql)or die("UPDATE STAR STATUS : ".mysql_error());

	} // end viewstarmsg

	if($_REQUEST['type']=="readmsg")
	{
		$curstatus=$_REQUEST['curstatus'];
		$msgseqid=$_REQUEST['msgseqid'];
		$msgid=$_REQUEST['msgid'];
		$replyid=$_REQUEST['replyid'];
		
		$upd_read_sql="";

		if($replyid=="0")
		{
			if($curstatus=="0")
			{
				$upd_read_sql="update inbox set read_flag='0' where msg_seqid='$msgseqid' and id='$msgid'";
			}elseif($curstatus=="1"){
				$upd_read_sql="update inbox set read_flag='1' where msg_seqid='$msgseqid' and id='$msgid'";
			}
		}
		else
		{
			if($curstatus=="0")
			{
		$upd_read_sql="update inbox_reply set read_flag='0' where msg_seqid='$msgseqid' and inbox_id='$msgid' and id='$replyid'";
			}elseif($curstatus=="1"){
		$upd_read_sql="update inbox_reply set read_flag='1' where msg_seqid='$msgseqid' and inbox_id='$msgid' and id='$replyid'";
			}
		}

		mysql_query($upd_read_sql)or die("UPDATE READ STATUS : ".mysql_error());

	} // end readmsg

	if($_REQUEST['type']=="inboxreadmsg")
	{
		$curstatus=$_REQUEST['curstatus'];
		$msgseqid=explode(",",$_REQUEST['msgseqid']);

		$upd_read_sql="";
		$upd_read_sql1="";

		$tablename=$_REQUEST['tablename'];

		if($tablename!="draft")
		{
			for($i=0;$i<count($msgseqid);$i++)
			{
				$msid=$msgseqid[$i];
				if($msid!="")
				{
			
				$ids=explode("_",$msid);
			
				$seqid=$ids[0];
				$cid=$ids[1];
				$rid=$ids[2];

				if($rid=="0")
				{
					if($curstatus=="0")
					{
						$upd_read_sql="update inbox set read_flag='0' where msg_seqid='$seqid' and id='$cid'";
					}elseif($curstatus=="1"){
						$upd_read_sql="update inbox set read_flag='1' where msg_seqid='$seqid' and id='$cid'";
					}
				}
				else
				{
					if($curstatus=="0")
					{
				$upd_read_sql="update inbox_reply set read_flag='0' where msg_seqid='$seqid' and inbox_id='$cid' and id='$rid'";
					}elseif($curstatus=="1"){
				$upd_read_sql="update inbox_reply set read_flag='1' where msg_seqid='$seqid' and inbox_id='$cid' and id='$rid'";
					}
				}

				mysql_query($upd_read_sql)or die("UPDATE READ STATUS : ".mysql_error());

				}
			}
		}else{
			for($i=0;$i<count($msgseqid);$i++)
			{
				$msid=$msgseqid[$i];
				if($msid!="")
				{
					$ids=explode("_",$msid);
			
					$seqid=$ids[0];
					$cid=$ids[1];
					$rid=$ids[2];

					if($curstatus=="0")
					{
						$upd_star_sql="update draft set read_flag='0' where msg_seqid='$seqid' and id='$cid'";
					}elseif($curstatus=="1"){
						$upd_star_sql="update draft set read_flag='1' where msg_seqid='$seqid' and id='$cid'";
					}
					mysql_query($upd_star_sql)or die("UPDATE STAR STATUS : ".mysql_error());
	
				}
			}
		}

	} // end inboxreadmsg

	if($_REQUEST['type']=="inboxstarmsg")
	{
		$curstatus=$_REQUEST['curstatus'];
		$msgseqid=explode(",",$_REQUEST['msgseqid']);

		$upd_read_sql="";
		$upd_read_sql1="";
		$tablename=$_REQUEST['tablename'];

		if($tablename!="draft")
		{
			for($i=0;$i<count($msgseqid);$i++)
			{
				$msid=$msgseqid[$i];
				if($msid!="")
				{
			
				$ids=explode("_",$msid);
			
				$seqid=$ids[0];
				$cid=$ids[1];
				$rid=$ids[2];

				if($rid=="0")
				{
					if($curstatus=="0")
					{
						$upd_read_sql="update inbox set star_flag='0' where msg_seqid='$seqid' and id='$cid'";
					}elseif($curstatus=="1"){
						$upd_read_sql="update inbox set star_flag='1' where msg_seqid='$seqid' and id='$cid'";
					}
				}
				else
				{
					if($curstatus=="0")
					{
				$upd_read_sql="update inbox_reply set star_flag='0' where msg_seqid='$seqid' and inbox_id='$cid' and id='$rid'";
					}elseif($curstatus=="1"){
				$upd_read_sql="update inbox_reply set star_flag='1' where msg_seqid='$seqid' and inbox_id='$cid' and id='$rid'";
					}
				}

				mysql_query($upd_read_sql)or die("UPDATE STAR STATUS: ".mysql_error());

				}
			
			}

		}else{
			for($i=0;$i<count($msgseqid);$i++)
			{
				$msid=$msgseqid[$i];
				if($msid!="")
				{
					$ids=explode("_",$msid);
			
					$seqid=$ids[0];
					$cid=$ids[1];

					if($curstatus=="0")
					{
						$upd_star_sql="update draft set star_flag='0' where msg_seqid='$seqid' and id='$cid'";
					}elseif($curstatus=="1"){
						$upd_star_sql="update draft set star_flag='1' where msg_seqid='$seqid' and id='$cid'";
					}
					mysql_query($upd_star_sql)or die("UPDATE STAR STATUS : ".mysql_error());
					
				}
			}

		}

	} // end inboxstarmsg

	if($_REQUEST['type']=="inboxdeletemsg")
	{
		$curstatus=$_REQUEST['curstatus'];
		$msgseqid=explode(",",$_REQUEST['msgseqid']);

		$upd_read_sql="";
		$tablename=$_REQUEST['tablename'];

		if($tablename!="draft")
		{
			for($i=0;$i<count($msgseqid);$i++)
			{
				$msid=$msgseqid[$i];
			
				if($msid!="")
				{
					$ids=explode("_",$msid);
			
					$seqid=$ids[0];
					$cid=$ids[1];
					$rid=$ids[2];
					if($rid==0)
					{
						if($curstatus=="0")
						{
					$upd_read_sql="update inbox set status='1' where msg_seqid='$seqid' and msguserid='$cur_userid'";
						}

						mysql_query($upd_read_sql)or die("UPDATE MSG STATUS: ".mysql_error());
					}else{
						if($curstatus=="0")
						{
					$upd_read_sql="update inbox_reply set status='1' where msg_seqid='$seqid' and msguserid='$cur_userid'";
						}

						mysql_query($upd_read_sql)or die("UPDATE MSG STATUS: ".mysql_error());
					}	
	
				} // end if
			} // for loop 

		}else{

			for($i=0;$i<count($msgseqid);$i++)
			{
				$msid=$msgseqid[$i];
			
				if($msid!="")
				{
					$ids=explode("_",$msid);
			
					$seqid=$ids[0];
					$cid=$ids[1];
					
					if($curstatus=="0")
					{
						$upd_read_sql="update draft set status='1' where msg_seqid='$seqid' and msguserid='$cur_userid'";
					}

					mysql_query($upd_read_sql)or die("UPDATE MSG STATUS: ".mysql_error());
					
				} // end if

			} // for loop 

			
		}

	} // end inboxdeletemsg


	if($_REQUEST['type']=="viewdeletemsg")
	{
		$curstatus=$_REQUEST['curstatus'];
		$msgseqid=$_REQUEST['msgseqid'];
		$replyid=$_REQUEST['replyid'];

		$upd_read_sql="";
		
		if($replyid==0)
		{
			if($curstatus=="0")
			{
				$upd_read_sql="update inbox set status='1' where msg_seqid='$msgseqid' and msguserid='$cur_userid'";
			}

			mysql_query($upd_read_sql)or die("UPDATE MSG STATUS: ".mysql_error());
		}else{
			if($curstatus=="0")
			{
				$upd_read_sql="update inbox_reply set status='1' where msg_seqid='$msgseqid' and msguserid='$cur_userid'";
			}

			mysql_query($upd_read_sql)or die("UPDATE MSG STATUS: ".mysql_error());
		}
	
	} // end viewdeletemsg


	if($_REQUEST['type']=="deldraftatt")
	{
		$id=$_REQUEST['aid'];
		$draftid=$_REQUEST['draftid'];

		mysql_query("update attachments set status='1' where id='$id'") or die(mysql_error());

		$r=mysql_fetch_array(mysql_query("select attachment_ids from draft where id='$draftid'")) or die(mysql_error());
		
		$oldids=explode(",",$r['attachment_ids']);
		
		$newids="";

		for($i=0;$i<count($oldids);$i++)
		{
			if($oldids[$i]!="")
			{
				if($oldids[$i]!=$id)
				{
					$newids.=$oldids[$i].",";
				}
			}
		}
		
		mysql_query("update draft set attachment_ids='$newids' where id='$draftid'") or die(mysql_error());

	}

	if($_REQUEST['type']=="discarddraft")
	{
		$draftid=$_REQUEST['draftid'];

		mysql_query("delete from draft where id='$draftid'") or die(mysql_error());

	}

	if($_REQUEST['type']=="acceptEventPopup")
	{
		$tseq_id=$_REQUEST['tseqid'];
		$loguserid=$_SESSION['loguserid'];

		$sql="select * from tps_training_details where trainingseqid='".$tseq_id."' and receiveruserid='".$loguserid."'";

		$result=mysql_query($sql) or die(mysql_error());

		$r=mysql_fetch_array($result);
		
		if(mysql_num_rows($result)>0)
		{
			$popupHtml = genTrainingAcceptPopup($r);
			$event_edit = array(
			      'popupHtml' => $popupHtml
		  	);
		}else{
			$popupHtml = "<br><b>The event was not found, could be deleted.</b><br><br><br><br>";
			$event_edit = array(
			      'popupHtml' => $popupHtml
		  	);
		}

		echo json_encode($event_edit);
	}

	
	if($_REQUEST['type'] == 'saveRemarks'){
		
			$appttype=$_REQUEST['appttype'];
			$isaccept=$_REQUEST['isaccept'];
			$remarks=$_REQUEST['remarks'];
			$seqid=$_REQUEST['seqid'];
			$tseqid=$_REQUEST['tseqid'];
		
			$cur_userid=$_SESSION['loguserid'];

			if($appttype=="Training"){
		
		$sql="update tps_training_details set isaccepted='".$isaccept."', remarks='".$remarks."', acceptdate='".date("Y-m-d H:i:s")."' where receiveruserid='".$cur_userid."' and trainingname='0' and id='".$seqid."'";
		
			}elseif($appttype=="TrainingFor"){

		$sql="update tps_training_details set isaccepted='".$isaccept."', remarks='".$remarks."', acceptdate='".date("Y-m-d H:i:s")."' where receiveruserid='".$cur_userid."' and id='".$seqid."' and eventid='0' and trainingseqid='".$tseqid."'";
	
			}
		mysql_query($sql) or die(mysql_error());

	}	
}


function genTrainingAcceptPopup($r){

$s=$r['starttime'];
$s1=$r['endtime'];

$p=explode("-",$s);
$a=explode(" ",$p[2]);

$dt=$p[1]."/".$a[0]."/".$p[0];

$se=explode(" ",$s1);

$dt1=date('M d, Y', strtotime($dt));

$st=date('h:i a',strtotime($a[1]));
$ed=date('h:i a',strtotime($se[1]));

$invdate=$dt1." ".$st." - ".$ed;

/*$appttype=$_REQUEST['appttype'];*/

$appttype="TrainingFor";

$seqid=$r['id'];

$tseqid=$r['trainingseqid'];

$attachmentslist="";

if($r['attachments']!="")
{
	$attachmentslist='<ul class="attachment-list pull-left list-unstyled">';
	$imgid=array_values(array_filter(explode(',',$r['attachments'])));
	
	for($j=0;$j<count($imgid);$j++)
	{
		if($imgid[$j]!="")
		{
			$upimgres=mysql_query("select filepath from attachments where id='".$imgid[$j]."'") or die(mysql_error());

			$upimgr=mysql_fetch_array($upimgres);
			$attachmentslist.='<li>
	<a href="download.php?type=download&filename=../'.$upimgr['filepath'].'" class="attached-file inline">
		<i class="icon-file-alt bigger-110 middle"></i>
		<span class="attached-name middle">'.basename($upimgr['filepath']).'</span>
	</a>
	<span class="action-buttons inline">
		<a href="download.php?type=download&filename=../'.$upimgr['filepath'].'"><i class="icon-download-alt bigger-125 blue"></i>
		</a>
	</span>
	</li>';

		}

	}
	$attachmentslist.='</ul>';
}

	$html = '<div class="widget-box"><div class="widget-header"><h4>Event Details</h4></div>
		<div class="widget-body" align="center">
	<table style="width: 100%; border-spacing: 8px; border-collapse: separate;" id="edit-drop-event-dialog-table">
		<tbody>
			<tr>
		<td valign="top" colspan="2">'.$r['trainingname'].' - '.$r['trainingsubname'].' - '.$r['trainingdesc'].' - <b>'.$invdate.'</b>';
		if($attachmentslist!=""){
			$html .='<br> <b>Attachements</b> <br>'.$attachmentslist;
		}
			$html .='</td>
			</tr>
			<tr>
			    <td>
			<input type="hidden" name="appttype" id="edit-drop-event-dialog-appttype-remarks" value="'.$appttype.'" />
			<input type="hidden" name="seqid" id="edit-drop-event-dialog-seqid" value="'.$seqid.'" />
			<input type="hidden" name="tseqid" id="edit-drop-event-dialog-tseqid" value="'.$tseqid.'" />
			    </td>
			    <td>';

	if($r['isaccepted']=="YES"){
		$html.='<button id="yes" class="btn btn-success" value="YES">YES</button> ';
	}else{
		$html.='<button id="yes" class="btn btn-light" value="YES">YES</button> ';
 	} 

	if($r['isaccepted']=="MAYBE"){
		$html.='<button id="maybe" class="btn btn-gray" value="MAYBE">MAYBE</button> ';
	}else{
		$html.='<button id="maybe" class="btn btn-light" value="MAYBE">MAYBE</button> ';
	}

	if($r['isaccepted']=="NO"){
		$html.='<button id="no" class="btn btn-danger" value="NO">NO</button>';
	}else{
		$html.='<button id="no" class="btn btn-light" value="NO">NO</button>';
	}
			$html.='</td>
		  	</tr>
			<tr>
			    <td valign="top">Remarks : </td>
			    <td><textarea id="edit-drop-event-dialog-remarks" rows="2" cols="40"></textarea></td>
		  	</tr>
		 	<tr>
				<td colspan="2">
					<div class="buttons">
						<a id="save-TrainingAccept-edit" class="btn btn-primary" > Save Details </a>
						<a id="cancel-event-edit" class="btn btn-default" >Cancel</a>
					</div>				
				</td>
			</tr>
		</tbody>
	</table></div></div><br><br>';
	return $html;
}

?>
