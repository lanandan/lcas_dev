<?php
session_start();
$pagetitle="Message :: Sent Items ";
$pageno=2;
include('header.php');
include('genfunctions.php');

?>
<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

<link rel="stylesheet" href="assets/css/select2.css" />
<script src="assets/js/select2.min.js"></script>

<div class="main-content">
<div class="breadcrumbs" id="breadcrumbs">
	<script type="text/javascript">
		try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
	</script>

	<ul class="breadcrumb">
		<li>
			<i class="icon-home home-icon"></i>
			<a href="../dashboard.php">Home</a>
		</li>
		<li class="active">Messages</li>
	</ul><!-- .breadcrumb -->
</div>

<script type="text/javascript">
$(document).ready(function(){


});

function starfun(curstar,msgid,msgtype)
{
	
	var data = {
			type: 'starmsg',
			curstar: curstar,
			msgid: msgid,
			msgtype: msgtype
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="sent.php";
		    	},
		    	error: function() {
				alert('Error while Staring');
		    	},
	});

}

function readcheckedfun(readstatus)
{
	
	var checkBoxes = document.getElementsByTagName('input');
       
        var param = "";
        for (var counter=0; counter < checkBoxes.length; counter++) {
                        if (checkBoxes[counter].type.toUpperCase()=='CHECKBOX' && checkBoxes[counter].checked == true) {
                                        param += checkBoxes[counter].value+",";
                        }
        }	

	var data = {
			type: 'inboxreadmsg',
			curstatus: readstatus,
			msgseqid: param
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="sent.php";
		    	},
		    	error: function() {
				alert('Error while Read/Unread Update!');
		    	},
	});

}

function starcheckedfun(starstatus)
{
	
	var checkBoxes = document.getElementsByTagName('input');
       
        var param = "";
        for (var counter=0; counter < checkBoxes.length; counter++) {
                        if (checkBoxes[counter].type.toUpperCase()=='CHECKBOX' && checkBoxes[counter].checked == true) {
                                        param += checkBoxes[counter].value+",";
                        }
        }	

	var data = {
			type: 'inboxstarmsg',
			curstatus: starstatus,
			msgseqid: param
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="sent.php";
		    	},
		    	error: function() {
				alert('Error while Star/Unstar Update!');
		    	},
	});

}

function deletefun(status)
{
	
	var checkBoxes = document.getElementsByTagName('input');
       
        var param = "";
        for (var counter=0; counter < checkBoxes.length; counter++) {
                        if (checkBoxes[counter].type.toUpperCase()=='CHECKBOX' && checkBoxes[counter].checked == true) {
                                        param += checkBoxes[counter].value+",";
                        }
        }	

	var data = {
			type: 'inboxdeletemsg',
			curstatus: status,
			msgseqid: param
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="sent.php";
		    	},
		    	error: function() {
				alert('Error while Msg Status Update!');
		    	},
	});

}
</script>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
<div class="row">
<div class="col-xs-12">
<div class="tabbable">
<?php 
include('mailmenu.php');
?>
<div class="tab-content no-border no-padding">
<div class="tab-pane in active">

<div class="message-container">
<div id="id-message-list-navbar" class="message-navbar align-center clearfix">

<div class="message-bar">
	<div class="message-infobar" id="id-message-infobar">
		<span class="blue bigger-150">Sent Items</span>
		<span class="grey bigger-110"><!--(2 unread messages)--></span>
	</div>

<div class="message-toolbar hide">
<div class="inline position-relative align-left">
	<a href="#" class="btn-message btn btn-xs dropdown-toggle" data-toggle="dropdown">
		<span class="bigger-110">Action</span>

		<i class="icon-caret-down icon-on-right"></i>
	</a>

	<ul class="dropdown-menu dropdown-lighter dropdown-caret dropdown-125">
		<li>
			<a href="#" onclick="javascript:readcheckedfun(1);">
				<i class="icon-eye-open blue"></i>
				&nbsp; Mark as read
			</a>
		</li>

		<li>
			<a href="#" onclick="javascript:readcheckedfun(0);">
				<i class="icon-eye-close green"></i>
				&nbsp; Mark unread
			</a>
		</li>

		<li class="divider"></li>

		<li>
			<a href="#" onclick="javascript:starcheckedfun(1);">
				<i class="icon-star orange2"></i>
				&nbsp; Add Star
			</a>
		</li>
		
		<li>
			<a href="#" onclick="javascript:starcheckedfun(0);">
				<i class="icon-star-empty light-grey"></i>
				&nbsp; Remove Star
			</a>
		</li>

		<li class="divider"></li>

		<li>
			<a href="#" onclick="javascript:deletefun(0);">
				<i class="icon-trash red bigger-110"></i>
				&nbsp; Delete
			</a>
		</li>
	</ul>
</div>

<div class="inline position-relative align-left">

</div>

	<a href="#" class="btn btn-xs btn-message" onclick="javascript:deletefun(0);">
		<i class="icon-trash bigger-125"></i>
		<span class="bigger-110">Delete</span>
	</a>
</div>
</div>

<div>
<div class="messagebar-item-left">
	<label class="inline middle">
		<input type="checkbox" id="id-toggle-all" class="ace" />
		<span class="lbl"></span>
	</label>

	&nbsp;
	<div class="inline position-relative">
		<a href="#" data-toggle="dropdown" class="dropdown-toggle">
			<i class="icon-caret-down bigger-125 middle"></i>
		</a>

		<ul class="dropdown-menu dropdown-lighter dropdown-100">
			<li>
				<a id="id-select-message-all" href="#">All</a>
			</li>

			<li>
				<a id="id-select-message-none" href="#">None</a>
			</li>
		</ul>
	</div>
</div>

<div class="messagebar-item-right">
<div class="inline position-relative">

</div>
</div>

<div class="nav-search minimized">
<form class="form-search">
	<!--<span class="input-icon">
		<input type="text" autocomplete="off" class="input-small nav-search-input" placeholder="Search inbox ..." />
		<i class="icon-search nav-search-icon"></i>
	</span>-->
</form>
</div>
</div>
</div>

<div class="message-list-container">
<div class="message-list" id="message-list">

<?php
$cur_userid=$_SESSION['userid'];

$inbox_sql=mysql_query("SELECT * from inbox where msguserid='$cur_userid' and from_userid='$cur_userid' and status='0' group by msg_seqid order by createddate desc")or die("INBOX : ".mysql_error());

$inbox_reply_sql=mysql_query("select * from inbox_reply where msguserid='$cur_userid' and from_userid='$cur_userid' and status='0' order by createddate desc")or die("INBOX REPLY: ".mysql_error());

$inbox=array();

$k=0; // inbox array indexing

$i=0;
while($ibr=mysql_fetch_array($inbox_sql))
{
	$cur_msgid=$ibr['id'];
	$cur_msgseqid=$ibr['msg_seqid'];
	
$reply_sql=mysql_query("SELECT * from inbox_reply WHERE status='0' and msg_seqid='$cur_msgseqid' group by msg_seqid, reply_seqid, from_userid") or die("Inbox Reply : ".mysql_error());
$ibcnt=mysql_num_rows($reply_sql);
	
	$inbox[$k]['id']=$ibr['id'];
	$inbox[$k]['msgseqid']=$ibr['msg_seqid'];
	$inbox[$k]['replyseqid']=0;
	$inbox[$k]['inboxid']="0";
	$inbox[$k]['from_userid']=$ibr['from_userid'];
	$inbox[$k]['from_username']=$ibr['from_username'];
	$inbox[$k]['to_userids']=$ibr['to_userids'];
	$inbox[$k]['to_usernames']=$ibr['to_usernames'];
	$inbox[$k]['subject']=$ibr['subject'];
	$inbox[$k]['read_flag']=$ibr['read_flag'];
	$inbox[$k]['star_flag']=$ibr['star_flag'];
	$inbox[$k]['createddate']=$ibr['createddate'];
	$inbox[$k]['replycnt']=$ibcnt;
	$inbox[$k]['msgtype']="inbox";
	$inbox[$k]['attachment_ids']=$ibr['attachment_ids'];

	$i++;
	
	$k++;
}


// for Reply Mails

$i=0;
while($ibr=mysql_fetch_array($inbox_reply_sql))
{
	$cur_msgid=$ibr['id'];
	$cur_msgseqid=$ibr['msg_seqid'];
	
$replycnt_sql=mysql_query("SELECT * FROM inbox_reply WHERE status = '0' AND msg_seqid = '$cur_msgseqid' group by msg_seqid, reply_seqid, from_userid") or die("Reply : ".mysql_error());
$ibrcnt=mysql_num_rows($replycnt_sql);

	$inbox[$k]['id']=$ibr['id'];
	$inbox[$k]['msgseqid']=$ibr['msg_seqid'];
	$inbox[$k]['replyseqid']=$ibr['reply_seqid'];
	$inbox[$k]['inboxid']=$ibr['inbox_id'];
	$inbox[$k]['from_userid']=$ibr['from_userid'];
	$inbox[$k]['from_username']=$ibr['from_username'];
	$inbox[$k]['to_userids']=$ibr['to_userids'];
	$inbox[$k]['to_usernames']=$ibr['to_usernames'];
	$inbox[$k]['subject']=$ibr['subject'];
	$inbox[$k]['read_flag']=$ibr['read_flag'];
	$inbox[$k]['star_flag']=$ibr['star_flag'];
	$inbox[$k]['createddate']=$ibr['createddate'];
	$inbox[$k]['replycnt']=$ibrcnt;
	$inbox[$k]['msgtype']="reply";
	$inbox[$k]['attachment_ids']=$ibr['attachment_ids'];
	
	$i++;

	$k++;
}

$inbox = array_map("unserialize", array_unique(array_map("serialize", $inbox)));


function array_sort_by_column(&$array, $column, $direction = SORT_DESC) {
    $reference_array = array();

    foreach($array as $key => $row) {
        $reference_array[$key] = $row[$column];
    }

    array_multisort($reference_array, $direction, $array);
}

array_sort_by_column($inbox, 'createddate');
 
$maxmsgseqid=0;
$maxreplyseqid=0;

$inboxcnt=0;

for($i=0;$i<count($inbox);$i++)
{

if($inbox[$i]['replycnt']>1)
{
	if($inbox[$i]['msgtype']!="inbox")
	{
if($maxmsgseqid!=$inbox[$i]['msgseqid'])
{
	$inboxcnt++;
	$maxmsgseqid=$inbox[$i]['msgseqid'];

?>

<?php

if($inbox[$i]['read_flag']=='0')
	echo '<div class="message-item message-unread">'; 
else
	echo '<div class="message-item">';

?>
	<label class="inline">
<?php if($inbox[$i]['msgtype']=="reply"){ ?>
<input type="checkbox" class="ace" name="ibck" value="<?php echo $inbox[$i]['msgseqid'].'_'.$inbox[$i]['inboxid'].'_'.$inbox[$i]['id']; ?>" />
<?php   }else if($inbox[$i]['msgtype']=="inbox"){ ?>
<input type="checkbox" class="ace" name="ibck" value="<?php echo $inbox[$i]['msgseqid'].'_'.$inbox[$i]['id'].'_0'; ?>" />
<?php	} ?>
		<span class="lbl"></span>
	</label>

<?php
if($inbox[$i]['star_flag']=="0")
{?>
	<i class="message-star icon-star-empty light-grey" onclick="javascript:starfun(0,<?php echo $inbox[$i]['id']; ?>,'<?php echo $inbox[$i]['msgtype']; ?>');"></i>
<?php
}else if($inbox[$i]['star_flag']=="1"){
?>
<i class="message-star icon-star orange2" onclick="javascript:starfun(1,<?php echo $inbox[$i]['id']; ?>,'<?php echo $inbox[$i]['msgtype']; ?>');"></i>
<?php
}
?>

	<span class="sender" title="<?php echo ucfirst($inbox[$i]['to_usernames']); ?>">
		<?php 

if($inbox[$i]['read_flag']=='0')
{
	
	if($inbox[$i]['msgtype']=="reply")
	{
	echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."' >".ucfirst($inbox[$i]['to_usernames'])."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
	echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."'>".ucfirst($inbox[$i]['to_usernames'])."</a>";
	}
}else{

	if($inbox[$i]['msgtype']=="reply")
	{
	echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."'  style='color:#555;' >".ucfirst($inbox[$i]['to_usernames'])."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
	echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."' style='color:#555;'>".ucfirst($inbox[$i]['to_usernames'])."</a>";
	}
}

			if($inbox[$i]['replycnt']>0)
			{				
				echo ' <span class="grey">('.$inbox[$i]['replycnt'].')</span>';
			}
		?>
	</span>
	<span class="time">
	<?php 
			$createddate=strtotime($inbox[$i]['createddate']); 
			echo date('g:i a',$createddate); 
	?>
	</span>
<?php
if($inbox[$i]['attachment_ids']!="")
{
?>
	<span class="attachment">
		<i class="icon-paper-clip"></i>
	</span>
<?php
}
?>
	<span class="summary">
		<!--<span class="badge badge-pink mail-tag"></span>-->
		<span class="text">
<?php

if($inbox[$i]['read_flag']=='0')
{

	if($inbox[$i]['msgtype']=="reply")
	{
echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."'>".$inbox[$i]['subject']."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
		echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."'>".$inbox[$i]['subject']."</a>";
	}

}else{
	
	if($inbox[$i]['msgtype']=="reply")
	{

echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."' style='color:#555;'>".$inbox[$i]['subject']."</a>";

	}else if($inbox[$i]['msgtype']=="inbox"){

echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."' style='color:#555;'>".$inbox[$i]['subject']."</a>";

	}

}
?>
		</span>
	</span>
</div>

<?php

   }// max msgseqid

}// end of msgtype

}else{ 

?>

<?php

$inboxcnt++;

if($inbox[$i]['read_flag']=='0')
	echo '<div class="message-item message-unread">'; 
else
	echo '<div class="message-item">';

?>
	<label class="inline">

<?php if($inbox[$i]['msgtype']=="reply"){ ?>
<input type="checkbox" class="ace" name="ibck" value="<?php echo $inbox[$i]['msgseqid'].'_'.$inbox[$i]['inboxid'].'_'.$inbox[$i]['id']; ?>" />
<?php   }else if($inbox[$i]['msgtype']=="inbox"){ ?>
<input type="checkbox" class="ace" name="ibck" value="<?php echo $inbox[$i]['msgseqid'].'_'.$inbox[$i]['id'].'_0'; ?>" />
<?php	} ?>

		<span class="lbl"></span>
	</label>
<?php
if($inbox[$i]['star_flag']=="0")
{?>
	<i class="message-star icon-star-empty light-grey" onclick="javascript:starfun(0,<?php echo $inbox[$i]['id']; ?>,'<?php echo $inbox[$i]['msgtype']; ?>');"></i>
<?php
}else if($inbox[$i]['star_flag']=="1"){
?>
<i class="message-star icon-star orange2" onclick="javascript:starfun(1,<?php echo $inbox[$i]['id']; ?>,'<?php echo $inbox[$i]['msgtype']; ?>');"></i>
<?php
}
?>

<span class="sender" title="<?php echo ucfirst($inbox[$i]['to_usernames']); ?>">
<?php 
if($inbox[$i]['read_flag']=='0')
{
	if($inbox[$i]['msgtype']=="reply")
	{
	echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."'>".ucfirst($inbox[$i]['to_usernames'])."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
	echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."'>".ucfirst($inbox[$i]['to_usernames'])."</a>";
	}
}else{

	if($inbox[$i]['msgtype']=="reply")
	{
	echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."' style='color:#555;' >".ucfirst($inbox[$i]['to_usernames'])."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
	echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."' style='color:#555;' >".ucfirst($inbox[$i]['to_usernames'])."</a>";
	}
}

		if($inbox[$i]['replycnt']>0)
			{				
				echo ' <span class="grey">('.$inbox[$i]['replycnt'].')</span>';
			}
		?>
	</span>
	<span class="time">
	<?php 
			$createddate=strtotime($inbox[$i]['createddate']); 
			echo date('g:i a',$createddate); 
	?>
	</span>

<?php
if($inbox[$i]['attachment_ids']!="")
{
?>
	<span class="attachment">
		<i class="icon-paper-clip"></i>
	</span>
<?php
}
?>
	<span class="summary">
		<!--<span class="badge badge-pink mail-tag"></span>-->
		<span class="text">
<?php

if($inbox[$i]['read_flag']=='0')
{

	if($inbox[$i]['msgtype']=="reply")
	{
echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."'>".$inbox[$i]['subject']."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
		echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."'>".$inbox[$i]['subject']."</a>";
	}

}else{
	
	if($inbox[$i]['msgtype']=="reply")
	{

echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."' style='color:#555;'>".$inbox[$i]['subject']."</a>";

	}else if($inbox[$i]['msgtype']=="inbox"){

echo "<a href='viewsentmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."' style='color:#555;'>".$inbox[$i]['subject']."</a>";

	}

}
?>
		</span>
	</span>
</div>
<?php
} // end of replycnt

}//end of forloop


?>

</div>
</div><!-- /.message-list-container -->


<div class="message-footer message-footer-style2 clearfix">
	<div class="pull-left"> <?php echo $inboxcnt; ?> messages total </div>

	<div class="pull-right">
		<div class="inline middle"> page 1 of 1 </div>

		&nbsp; &nbsp;
		<ul class="pagination middle">
			<li>
				<a href="#">
					<i class="icon-angle-left bigger-150"></i>
				</a>
			</li>

			<li>
				<a href="#">
					<i class="icon-angle-right bigger-150"></i>
				</a>
			</li>
	

	</div>
</div>

			</div><!-- /.message-container -->
				</div><!-- /.tab-pane -->
			</div><!-- /.tab-content -->
		</div><!-- /.tabbable -->
	</div><!-- /.col -->
</div><!-- /.row -->

				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div><!-- /.main-content -->
<script src="assets/js/bootstrap-tag.min.js"></script>
<script src="assets/js/jquery.hotkeys.min.js"></script>
<script src="assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>

<script type="text/javascript">

jQuery(function($){

		$("#sendto").select2(); 
		
		//handling tabs and loading/displaying relevant messages and forms
		//not needed if using the alternative view, as described in docs
		var prevTab = 'inbox'
		$('#inbox-tabs a[data-toggle="tab"]').on('show.bs.tab', function (e) {
			var currentTab = $(e.target).data('target');
			if(currentTab == 'write') {
				Inbox.show_form();
			}
			else {
				if(prevTab == 'write')
					Inbox.show_list();
	
				//load and display the relevant messages 
			}
			prevTab = currentTab;
		})
	
		//basic initializations
		$('.message-list .message-item input[type=checkbox]').removeAttr('checked');
		$('.message-list').delegate('.message-item input[type=checkbox]' , 'click', function() {
			$(this).closest('.message-item').toggleClass('selected');
			if(this.checked) Inbox.display_bar(1);//display action toolbar when a message is selected
			else {
				Inbox.display_bar($('.message-list input[type=checkbox]:checked').length);
				//determine number of selected messages and display/hide action toolbar accordingly
			}		
		});
	
	
		//check/uncheck all messages
		$('#id-toggle-all').removeAttr('checked').on('click', function(){
			if(this.checked) {
				Inbox.select_all();
			} else Inbox.select_none();
		});
		
		//select all
		$('#id-select-message-all').on('click', function(e) {
			e.preventDefault();
			Inbox.select_all();
		});
		
		//select none
		$('#id-select-message-none').on('click', function(e) {
			e.preventDefault();
			Inbox.select_none();
		});
		
		//select read
		$('#id-select-message-read').on('click', function(e) {
			e.preventDefault();
			Inbox.select_read();
		});
	
		//select unread
		$('#id-select-message-unread').on('click', function(e) {
			e.preventDefault();
			Inbox.select_unread();
		});
	
		/////////
		//display first message in a new area

		
		//display second message right inside the message list
		
	
	
	
		//back to message list
		$('.btn-back-message-list').on('click', function(e) {
			e.preventDefault();
			Inbox.show_list();
			$('#inbox-tabs a[data-target="inbox"]').tab('show'); 
		});
	
	
	
		//hide message list and display new message form
		/**
		$('.btn-new-mail').on('click', function(e){
			e.preventDefault();
			Inbox.show_form();
		});
		*/
	
	
	
	
		var Inbox = {
			//displays a toolbar according to the number of selected messages
			display_bar : function (count) {
				if(count == 0) {
					$('#id-toggle-all').removeAttr('checked');
					$('#id-message-list-navbar .message-toolbar').addClass('hide');
					$('#id-message-list-navbar .message-infobar').removeClass('hide');
				}
				else {
					$('#id-message-list-navbar .message-infobar').addClass('hide');
					$('#id-message-list-navbar .message-toolbar').removeClass('hide');
				}
			}
			,
			select_all : function() {
				var count = 0;
				$('.message-item input[type=checkbox]').each(function(){
					this.checked = true;
					$(this).closest('.message-item').addClass('selected');
					count++;
				});
				
				$('#id-toggle-all').get(0).checked = true;
				
				Inbox.display_bar(count);
			}
			,
			select_none : function() {
				$('.message-item input[type=checkbox]').removeAttr('checked').closest('.message-item').removeClass('selected');
				$('#id-toggle-all').get(0).checked = false;
				
				Inbox.display_bar(0);
			}
			,
			select_read : function() {
				$('.message-unread input[type=checkbox]').removeAttr('checked').closest('.message-item').removeClass('selected');
				
				var count = 0;
				$('.message-item:not(.message-unread) input[type=checkbox]').each(function(){
					this.checked = true;
					$(this).closest('.message-item').addClass('selected');
					count++;
				});
				Inbox.display_bar(count);
			}
			,
			select_unread : function() {
				$('.message-item:not(.message-unread) input[type=checkbox]').removeAttr('checked').closest('.message-item').removeClass('selected');
				
				var count = 0;
				$('.message-unread input[type=checkbox]').each(function(){
					this.checked = true;
					$(this).closest('.message-item').addClass('selected');
					count++;
				});
				
				Inbox.display_bar(count);
			}
		}
	
		//show message list (back from writing mail or reading a message)
		Inbox.show_list = function() {
			$('.message-navbar').addClass('hide');
			$('#id-message-list-navbar').removeClass('hide');
	
			$('.message-footer').addClass('hide');
			$('.message-footer:not(.message-footer-style2)').removeClass('hide');
	
			$('.message-list').removeClass('hide').next().addClass('hide');
			//hide the message item / new message window and go back to list
		}
	
		//show write mail form
		Inbox.show_form = function() {
			if($('.message-form').is(':visible')) return;
			if(!form_initialized) {
				initialize_form();
			}
			
			
			var message = $('.message-list');
			$('.message-container').append('<div class="message-loading-overlay"><i class="icon-spin icon-spinner orange2 bigger-160"></i></div>');
			
			setTimeout(function() {
				message.next().addClass('hide');
				
				$('.message-container').find('.message-loading-overlay').remove();
				
				$('.message-list').addClass('hide');
				$('.message-footer').addClass('hide');
				$('.message-form').removeClass('hide').insertAfter('.message-list');
				
				$('.message-navbar').addClass('hide');
				$('#id-message-new-navbar').removeClass('hide');
				
				
				//reset form??
				$('.message-form .wysiwyg-editor').empty();
			
				$('.message-form .ace-file-input').closest('.file-input-container:not(:first-child)').remove();
				$('.message-form input[type=file]').ace_file_input('reset_input');
				
				$('.message-form').get(0).reset();
				
			}, 300 + parseInt(Math.random() * 300));
		}
	
	
	
	
		var form_initialized = false;
		function initialize_form() {
			if(form_initialized) return;
			form_initialized = true;
			
			//intialize wysiwyg editor
			$('.message-form .wysiwyg-editor').ace_wysiwyg({
				toolbar:
				[
					'bold',
					'italic',
					'strikethrough',
					'underline',
					null,
					'justifyleft',
					'justifycenter',
					'justifyright',
					null,
					'createLink',
					'unlink',
					null,
					'undo',
					'redo'
				]
			}).prev().addClass('wysiwyg-style1');
	
			//file input
			$('.message-form input[type=file]').ace_file_input()
			//and the wrap it inside .span7 for better display, perhaps
			.closest('.ace-file-input').addClass('width-90 inline').wrap('<div class="row file-input-container"><div class="col-sm-7"></div></div>');
	
			//the button to add a new file input
			$('#id-add-attachment').on('click', function(){
				var file = $('<input type="file" name="attachment[]" />').appendTo('#form-attachments');
				file.ace_file_input();
				file.closest('.ace-file-input').addClass('width-90 inline').wrap('<div class="row file-input-container"><div class="col-sm-7"></div></div>')
				.parent(/*.span7*/).append('<div class="action-buttons pull-right col-xs-1">\
					<a href="#" data-action="delete" class="middle">\
						<i class="icon-trash red bigger-130 middle"></i>\
					</a>\
				</div>').find('a[data-action=delete]').on('click', function(e){
					//the button that removes the newly inserted file input
					e.preventDefault();			
					$(this).closest('.row').hide(300, function(){
						$(this).remove();
					});
				});
			});
		}//initialize_form
	
	
		//turn the recipient field into a tag input field!
		/**	
		var tag_input = $('#form-field-recipient');
		if(! ( /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase())) ) 
			tag_input.tag({placeholder:tag_input.attr('placeholder')});
	
	
		//and add form reset functionality
		$('.message-form button[type=reset]').on('click', function(){
			$('.message-form .message-body').empty();
			
			$('.message-form .ace-file-input:not(:first-child)').remove();
			$('.message-form input[type=file]').ace_file_input('reset_input');
			
			
			var val = tag_input.data('value');
			tag_input.parent().find('.tag').remove();
			$(val.split(',')).each(function(k,v){
				tag_input.before('<span class="tag">'+v+'<button class="close" type="button">&times;</button></span>');
			});
		});
		*/
	
	});
</script>
<?php
include('footer.php');
?>
