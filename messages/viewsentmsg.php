<?php
session_start();
$pagetitle="Message :: View Sent Message ";
$pageno=5;
include('header.php');
include('genfunctions.php');

?>
<script type="text/javascript">
	window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>

<link rel="stylesheet" href="assets/css/select2.css" />
<script src="assets/js/select2.min.js"></script>


<link rel="stylesheet" type="text/css" href="assets/uploadify/uploadifive.css">
<script src="assets/uploadify/jquery.uploadifive.min.js" type="text/javascript"></script>

<div class="main-content">
<div class="breadcrumbs" id="breadcrumbs">
	<script type="text/javascript">
		try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
	</script>

	<ul class="breadcrumb">
		<li>
			<i class="icon-home home-icon"></i>
			<a href="#">Home</a>
		</li>
		<li class="active">Dashboard</li>
	</ul><!-- .breadcrumb -->
</div>

<script type="text/javascript">
$(document).ready(function(){


<?php $timestamp = time();?>

$('#file_upload').uploadifive({
	'auto'             : true,
	'formData'         : {
				   'timestamp' : '<?php echo $timestamp;?>',
				   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
	                     },
	'queueID'          : 'queue',
	'fileSizeLimit'    : '2MB',
	'uploadScript'     : 'uploadifive.php?job=reply&tabname=inbox_inbox',
	'onProgress'   	   : function(file, e) {
			    if (e.lengthComputable) {
				var percent = Math.round((e.loaded / e.total) * 100);
			    }
			    file.queueItem.find('.fileinfo').html(' - ' + percent + '%');
			    file.queueItem.find('.progress-bar').css('width', percent + '%');
        }, 
	'onUploadComplete' : function(file, data) {

				$('#output').append(data+',');

				document.getElementById('upfileids').value = $('#output').html();

				file.queueItem.find('.close').html(data);
				
			}
	
});


$('#btnsendreply').click(function(){

	var recpt=$('#replytouserid').val();
	var subject=$('#subject').val();
	var message=$('#message').html();
	var orgmsgid=$('#orgmsgid').val();
	var attids=$('#upfileids').val();
	var msg_seqid=$('#msg_seqid').val();
	
	if(recpt!=null)
	{
		var data = {
				type: 'replytomail',
				sendto: recpt,
				subject: subject,
				message:message,
				orgmsgid:orgmsgid,
				attids:attids,
				msg_seqid:msg_seqid
			 }

			$.ajax({
				type: "POST",
				url: "message_actions.php",
				data: data,
				success: function(resp) {
					alert("Mail Sent Successfully");
					window.location.href="sent.php";
			    	},
			    	error: function() {
					alert('Error while Saving');
			    	},
			});
	}else{
		alert("Please Specify atleast one Recipient ");
	}
});


});

function readfun(curstatus,msgseqid,msgid,replyid)
{	
	var data = {
			type: 'readmsg',
			curstatus: curstatus,
			msgseqid: msgseqid,
			msgid: msgid,
			replyid: replyid
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="sent.php";
		    	},
		    	error: function() {
				alert('Error while Saving');
		    	},
	});

}

function starfun(curstatus,msgseqid,msgid,replyid)
{	
	var data = {
			type: 'viewstarmsg',
			curstatus: curstatus,
			msgseqid: msgseqid,
			msgid: msgid,
			replyid: replyid
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="sent.php";
		    	},
		    	error: function() {
				alert('Error while Saving');
		    	},
	});

}

function deletefun(status,msgseqid,replyid)
{
	
	var data = {
			type: 'viewdeletemsg',
			curstatus: status,
			msgseqid: msgseqid,
			replyid: replyid
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="sent.php";
		    	},
		    	error: function() {
				alert('Error while Msg Status Update!');
		    	},
	});

}

</script>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
<div class="row">
<div class="col-xs-12">
<div class="tabbable">
<?php 
include('mailmenu.php');
$cur_userid=$_SESSION['userid'];

$ibtolist=array();
$ibtolistids=array();

$cur_replyid="";

if(isset($_REQUEST['msgid']))
{
	$msgid=$_REQUEST['msgid'];
	$msgseqid=$_REQUEST['msgseqid'];

	if(isset($_REQUEST['replymsgid']))
	{
		$cur_replyid=$_REQUEST['replymsgid'];
	}else{
		$cur_replyid=0;
	}


	$upd_as_readmsg_sql=mysql_query("update inbox set read_flag='1' where msg_seqid='$msgseqid' and msguserid='$cur_userid' and status='0'")or die("VIEW MSG : ".mysql_error());

	$upd_as_readmsg_reply_sql=mysql_query("update inbox_reply set read_flag='1' where msg_seqid='$msgseqid' and inbox_id='$msgid' and msguserid='$cur_userid' and status='0'")or die("VIEW MSG : ".mysql_error());	

	$inbox_sql=mysql_query("select * from inbox where msg_seqid='$msgseqid' and msguserid='$cur_userid'")or die("INBOX : ".mysql_error());

	$ibr=mysql_fetch_array($inbox_sql);
 
?>

<div id="id-message-item-navbar" class="message-navbar align-center clearfix">
<div class="message-bar">

<div class="message-toolbar">

<!-- Reply Menu -->
<div class="inline position-relative align-left">
	<a href="#" class="btn-message btn btn-xs dropdown-toggle" data-toggle="dropdown">
		<span class="bigger-110">Action</span><i class="icon-caret-down icon-on-right"></i></a>

	<ul class="dropdown-menu dropdown-lighter dropdown-caret dropdown-125">
		<li>
			<a href="viewmsg.php?msgseqid=<?php echo $msgseqid; ?>&msgid=<?php echo $msgid; ?>&action=reply">
			<i class="icon-mail-reply blue"></i>&nbsp; Reply</a>
		</li>
		<li>
			<a href="viewmsg.php?msgseqid=<?php echo $msgseqid; ?>&msgid=<?php echo $msgid; ?>&action=replyall">
			<i class="icon-mail-reply blue"></i>&nbsp; Reply to all</a>
		</li>
		<!--<li>
			<a href="#">
				<i class="icon-mail-forward green"></i>&nbsp; Forward
			</a>
		</li>-->
		<li class="divider"></li>

		<li>
		<?php
			if($cur_replyid!='')
				$rlyid=$cur_replyid;
			else
				$rlyid=0;
		?>
			<a href="#" onclick="javascript:readfun(1,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">
				<i class="icon-eye-open blue"></i>&nbsp; Mark as read
			</a>
		</li>

		<li>
			<a href="#" onclick="javascript:readfun(0,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">
				<i class="icon-eye-close green"></i>&nbsp; Mark unread
			</a>
		</li>

		<li class="divider"></li>

		<li>
			<a href="#" onclick="javascript:starfun(1,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">
				<i class="icon-star orange2"></i>
				&nbsp; Add Star
			</a>
		</li>
		
		<li>
			<a href="#" onclick="javascript:starfun(0,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">
				<i class="icon-star-empty light-grey"></i>
				&nbsp; Remove Star
			</a>
		</li>

		<li class="divider"></li>

		<li>
			<a href="#" onclick="javascript:deletefun(0,<?php echo $msgseqid; ?>,<?php echo $rlyid; ?>);">
				<i class="icon-trash red bigger-110"></i>&nbsp; Delete
			</a>
		</li>
	</ul>
</div>

<!-- Reply Menu -->

	<a href="#" onclick="javascript:deletefun(0,<?php echo $msgseqid; ?>,<?php echo $rlyid; ?>);" class="btn btn-xs btn-message">
		<i class="icon-trash bigger-125"></i>
		<span class="bigger-110">Delete</span>
	</a>
</div>
</div>

<div>
	<div class="messagebar-item-left">
		<a href="sent.php" class="btn-back-message-list">
			<i class="icon-arrow-left blue bigger-110 middle"></i>
			<b class="bigger-110 middle">Back</b>
		</a>
	</div>

	<div class="messagebar-item-right">
		<i class="icon-time bigger-110 orange middle"></i>
		<span class="time grey">
		<?php 
			$createddate=strtotime($ibr['createddate']); 
			echo display_time_diff_format($createddate); 
		?>
		</span>
	</div>
</div>
</div>

<div class="message-content" id="id-message-content">
<div class="message-header clearfix">

	<div class="pull-left">
		<span class="blue bigger-125"><?php echo ucfirst($ibr['subject']); ?> </span>

		<div class="space-4"></div>
		
	</div>

	<div class="pull-right">

		&nbsp;
		<i class="icon-time bigger-110 orange middle"></i>
		<span class="time">
		<?php 
			echo date('F j, Y g:i A',$createddate);  
			echo " &nbsp; (".display_time_diff_format($createddate).") "; 
		?>
		</span>
	</div>
<br><br>
<?php
	$fromuserid=$ibr['from_userid'];
	$userimg="";

	$imgr=mysql_query("select image1 from tps_users where id='$fromuserid'")or die(mysql_error());
	$imgres=mysql_fetch_array($imgr);

	$userimg="../images/upload/".$imgres['image1'];

	if($userimg=="")
	{
		$userimg="assets/avatars/avatar.png";
	}
?>
	<img class="middle" alt="User Image" src="<?php echo $userimg; ?>" width="32" />
	<span class="sender"><?php echo ucfirst($ibr['from_username']); ?></span>
	<br><span style="font-size:11px;color:gray;">To : <?php echo $ibr['to_usernames']; ?></span>
</div>

<div class="hr"></div>

<div class="message-body">
	<?php echo html_entity_decode($ibr['body']); ?>
</div>

<div class="hr"></div>
<?php
if($ibr['attachment_ids']!="")
{
?>
<div class="message-attachment clearfix">
	<div class="attachment-title">
		<span class="blue bolder bigger-110">Attachments</span>
	</div>

	&nbsp;
	<ul class="attachment-list pull-left list-unstyled">

<?php
	$imgid=explode(',',$ibr['attachment_ids']);	

	for($j=0;$j<count($imgid);$j++)
	{
		if($imgid[$j]!="")
		{
			$upimgres=mysql_query("select filepath from attachments where id='".trim($imgid[$j])."'") or die(mysql_error());
			
			$upimgr=mysql_fetch_array($upimgres);
			
			echo '<li>
			<a href="download.php?type=download&filename='.$upimgr['filepath'].'" class="attached-file inline">
				<i class="icon-file-alt bigger-110 middle"></i>
				<span class="attached-name middle">'.basename($upimgr['filepath']).'</span>
			</a>
			<div class="action-buttons inline">
				<a href="download.php?type=download&filename='.$upimgr['filepath'].'">
					<i class="icon-download-alt bigger-125 blue"></i>
				</a>
			</div>
			</li>';

		}
		
	}

?>
	</ul>

	<div class="attachment-images pull-right">
		<div class="vspace-sm-4"></div>

		<div>
		</div>
	</div>
</div>
<?php
}//end of attachments
?>
<div class="hr hr-double"></div>

<?php

$reply_inbox_sql=mysql_query("select * from inbox_reply where msg_seqid='$msgseqid' and status='0' group by msg_seqid, reply_seqid, from_userid")or die("INBOX : ".mysql_error());

while($r_ibr=mysql_fetch_array($reply_inbox_sql))
{
	$replyseqid=$r_ibr['reply_seqid'];
	
	$userimg="";

	$imgr=mysql_query("select image1 from tps_users where id='$fromuserid'")or die(mysql_error());
	$imgres=mysql_fetch_array($imgr);

	$userimg="../images/upload/".$imgres['image1'];

	if($userimg=="")
	{
		$userimg="assets/avatars/avatar.png";
	}

?>
<div id="id-message-item-navbar" class="message-navbar align-left clearfix blue" style="border-right:1px solid #D6E1EA; border-left:1px solid #D6E1EA;">
<span style="float:left;">
<img class="middle" alt="User Image" src="<?php echo $userimg; ?>" width="32" />
<b>Reply By : <?php //echo "[ cur_reply msgid - ".$r_ibr['id']." ] ";  
		echo ucfirst($r_ibr['from_username']); ?></b>
<br><span style="font-size:11px;color:gray;">To : <?php echo $r_ibr['to_usernames']; ?></span>
</span>
<span style="float:right;">
<?php
	$createddate=strtotime($r_ibr['createddate']); 
	echo '<i class="icon-time bigger-110 orange middle"></i>';
	echo ' <span class="time grey"> '.display_time_diff_format($createddate).'</span>'; 
?>
</span>
</div>

<div class="message-content" id="id-message-content" style=" border:1px solid #D6E1EA;">
	<div class="message-body">
		<?php 
			echo html_entity_decode($r_ibr['body']);
			echo "<br><br>";		
		 ?>
	</div>


<?php
if($r_ibr['attachment_ids']!="")
{
?>

<div class="hr"></div>

	<div class="message-attachment clearfix">
	
	<div class="attachment-title">
		<span class="blue bolder bigger-110">Attachments</span>
	</div>
	&nbsp;
	<ul class="attachment-list pull-left list-unstyled">

<?php
	$imgid=explode(',',$r_ibr['attachment_ids']);	
	for($j=0;$j<count($imgid);$j++)
	{
		
		if($imgid[$j]!="")
		{
			$upimgres=mysql_query("select filepath from attachments where id='".$imgid[$j]."'") or die(mysql_error());

			$upimgr=mysql_fetch_array($upimgres);
			echo '<li>
			<a href="download.php?type=download&filename='.$upimgr['filepath'].'" class="attached-file inline">
				<i class="icon-file-alt bigger-110 middle"></i>
				<span class="attached-name middle">'.basename($upimgr['filepath']).'</span>
			</a>
			<div class="action-buttons inline">
				<a href="download.php?type=download&filename='.$upimgr['filepath'].'">
					<i class="icon-download-alt bigger-125 blue"></i>
				</a>
				<a href="#" onclick="javascript:"><i class="icon-trash bigger-125 red"></i></a>
			</div>
			</li>';

		}
		
	}

?>
	</ul>

	</div>	
<?php
}
?>
</div><!-- /.message-content -->
<br>
<?php
}
?>
</div>	

</div>
<style type="text/css">

.uploadifive-button {
	float: left;
	margin-right: 10px;
}
#queue {
	border: 1px solid #E5E5E5;
	height: 100px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 80%;
}
.wysiwyg-editor {
    background-color: #F7F8FA;
    border: 1px solid #BBC0CA;
    border-collapse: separate;
    height: 100px;
    max-height: 200px;
    outline: 0 none;
    overflow-x: hidden;
    overflow-y: auto;
    padding: 4px;
}
</style>

<?php
if(isset($_REQUEST['action']))
{
	if($_REQUEST['action']=="reply" || $_REQUEST['action']=="replyall")
	{
		if($_REQUEST['action']=="reply")
		{
			$replyto=$ibr['from_userid']."-".ucfirst($ibr['from_username']);
		}
		else if($_REQUEST['action']=="replyall")
		{
			$replyto_all_list=array();
			$replyto_all_list[0]=$ibr['from_userid'];
			
			$uids=explode(",",$ibr['to_userids']);
			
			$j=1;
			for($i=0;$i<count($uids);$i++)
			{
				if($uids[$i]!=$cur_userid)
				{
					$replyto_all_list[$j]=$uids[$i];
				}
				$j++;
			}
			$usrlist=implode(",",$replyto_all_list);
			$replyto=get_userslist_by_userid($usrlist);
		}
?>

<div id="id-message-item-navbar" class="message-navbar align-left clearfix blue" style="border-right:1px solid #D6E1EA; border-left:1px solid #D6E1EA;"><b>Reply To : <?php echo $replyto ?></b></div>

<div class="message-content" id="id-message-content" style=" border:1px solid #D6E1EA;">
<div class="message-body">
	
<form id="id-message-form" class="form-horizontal col-xs-12">
<div class="">
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-recipient">Reply To:</label>

		<div class="col-sm-6 col-xs-12">
		
<input type="hidden" name="orgmsgid" id="orgmsgid" placeholder="Original Message" value="<?php echo $msgid; ?>" />

<select multiple="" class="width-80" id="replytouserid" data-placeholder="Choose a Recipient..." style="400px;">
	<?php 
		if($_REQUEST['action']=="reply")
		{
			echo get_recipient_list($ibr['from_userid']);
		}
		else if($_REQUEST['action']=="replyall")
		{
			echo get_recipient_listAll($usrlist);
		}
	?>
</select>
	
<input type="hidden" name="msg_seqid" id="msg_seqid" value="<?php echo $ibr['msg_seqid']; ?>" />

		</div>
	</div>

	<div class="hr hr-18 dotted"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-subject">Subject:</label>

		<div class="col-sm-6 col-xs-12">
			<div class="input-icon block col-xs-12 no-padding">
<input type="text" class="col-xs-12" name="subject" id="subject" placeholder="Subject" value="<?php echo $ibr['subject']; ?>" readonly />
				<i class="icon-comment-alt"></i>
			</div>
		</div>
	</div>

	<div class="hr hr-18 dotted"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right">
			<span class="inline space-24 hidden-480"></span>
			Message:
		</label>

		<div class="col-sm-9">
			<div class="wysiwyg-editor wysiwyg-style1" id="message"></div>
		</div>
	</div>

	<div class="hr hr-18 dotted"></div>

	<div class="form-group no-margin-bottom">
		<label class="col-sm-3 control-label no-padding-right">Attachments:</label>

		<div class="col-sm-9">
			<div id="queue">Drag & Drop Image Files Here</div>

			<input id="file_upload" name="file_upload" type="file" multiple="true">
			<div id="output" style="display:none;"></div>
			<input type="hidden" name="upfileids" id="upfileids">
		</div>
	</div>


	<div class="space"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-subject"></label>

		<div class="col-sm-6 col-xs-12">
			<div class="input-icon block col-xs-12 no-padding">
				<button type="button" id="btnsendreply" class="btn btn-sm btn-primary no-border">
				<span class="bigger-110">Send</span>
				<i class="icon-arrow-right icon-on-right"></i>
				</button>
			</div>
		</div>
	</div>
	
</div>
</form>
</div>

<div class="message-attachment clearfix">
	
</div>	

</div><!-- /.message-content -->

<?php
	}
}
?>


<div class="message-footer message-footer-style2 clearfix">
	<div class="pull-left"> <!--simpler footer--> </div>

	<div class="pull-right">
		<div class="inline middle"> <!--message 1 of 151--> </div>
	<!--
		&nbsp; &nbsp;
		<ul class="pagination middle">
			<li class="disabled">
				<span>
					<i class="icon-angle-left bigger-150"></i>
				</span>
			</li>

			<li>
				<a href="#">
					<i class="icon-angle-right bigger-150"></i>
				</a>
			</li>
		</ul>
	-->
	</div>
</div>
		
<?php
}
?>

					</div><!-- /.message-container -->
				</div><!-- /.tab-pane -->
			</div><!-- /.tab-content -->
		</div><!-- /.tabbable -->
	</div><!-- /.col -->
</div><!-- /.row -->


				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div><!-- /.main-content -->
<script src="assets/js/bootstrap-tag.min.js"></script>
<script src="assets/js/jquery.hotkeys.min.js"></script>
<script src="assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>

<script type="text/javascript">

jQuery(function($){


	$('#replytouserid').select2();
		
	$('#message').ace_wysiwyg({
		toolbar:
		[
			'bold',
			'italic',
			'strikethrough',
			'underline',
			null,
			'justifyleft',
			'justifycenter',
			'justifyright',
			null,
			'createLink',
			'unlink',
			null,
			'undo',
			'redo'
		]
	}).prev().addClass('wysiwyg-style1');

});
</script>
<?php
include('footer.php');
?>
