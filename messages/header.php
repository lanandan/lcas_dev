<?php
//date_default_timezone_set ("America/New_York");
//echo date_default_timezone_get();
require_once('../include/tps_gen_functions.php');

require_once('dbconnect.php');

function unreadmsgcountheader($userid)
{
	$res=mysql_query("select count(*) as msgcnt from inbox where msguserid='$userid' and from_userid!='$userid' and read_flag='0' and status='0' order by createddate desc")or die(mysql_error());
	$r=mysql_fetch_array($res);

	$res_reply=mysql_query("select count(*) as msgreplycnt from inbox_reply where msguserid='$userid' and from_userid!='$userid' and read_flag='0' and status='0' order by createddate desc")or die("INBOX REPLY: ".mysql_error());
	
	$rr=mysql_fetch_array($res_reply);

	$msgcount=$r['msgcnt']+$rr['msgreplycnt'];

	return $msgcount;	
}


$id=get_session('LOGIN_ID') ;
$sql="select * from tps_users where id='$id'";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$nameprefix=$row['nameprefix'];
$thu=$row['image1'];

if($row['image1']=='default.jpeg')
{
$thu=$row['image1'];
}
else
{
$expl=explode(".",$row['image1']);
$thu= $expl[0].".jpg";
}
$dealer=get_session('LOGIN_DEALER');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title><?php echo $pagetitle; ?></title>

	<meta name="description" content="Minimal empty page" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- basic styles -->

	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="assets/css/font-awesome.min.css" />

	<!--[if IE 7]>
	  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
	<![endif]-->

	<!-- page specific plugin styles -->

	<!-- fonts -->

	<link rel="stylesheet" href="assets/css/ace-fonts.css" />

	<!-- ace styles -->

	<link rel="stylesheet" href="assets/css/ace.min.css" />
	<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
	<link rel="stylesheet" href="assets/css/ace-skins.min.css" />

	<!--[if lte IE 8]>
	  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
	<![endif]-->

	<!-- inline styles related to this page -->

	<!-- ace settings handler -->

	<script src="assets/js/ace-extra.min.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	
</head>

<body>
	<div class="navbar navbar-default" id="navbar">
		<script type="text/javascript">
			try{ace.settings.check('navbar' , 'fixed')}catch(e){}
		</script>

		<div class="navbar-container" id="navbar-container">
			<div class="navbar-header pull-left">
				<a href="#" class="navbar-brand">
					<small>
						LCAS Messages
					</small>
				</a><!-- /.brand -->
			</div><!-- /.navbar-header -->

			<div class="navbar-header pull-right" role="navigation">
	<ul class="nav ace-nav">
	    <li class="grey"><a href="../dashboard.php">Dashboard</a></li>
	    <li class="grey"><a href="../appointments.php">Appointments</a></li>
	    <li class="dropdown grey">
              <a href="leadcard.php" class="dropdown-toggle" data-toggle="dropdown">Leads <i class="icon-caret-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="../add_leads.php">Add Leads</a></li>
		<li ><a href="../addlead_xls.php">Import Leads</a></li>
                <li><a href="../leadcard.php">View Leads</a></li>
              </ul>
            </li>
            <li class="grey"><a href="../customer.php">Customers</a></li>
            <li class="grey"><a href="../campaign.php">Campaigns</a></li>		
            <li class="grey"><a href="../task_listing.php">Tasks</a></li>
	    <li class="grey"><a href="../user_training_details.php">Events</a></li>

	    <li class="dropdown grey">
	<a href="inbox.php" class="dropdown-toggle" data-toggle="dropdown">Messages<?php
 	$msgcnt=unreadmsgcountheader($id); 
	if($msgcnt>0){ echo '<span class="badge badge-grey">'.$msgcnt.'</span>';  } ?>
	<i class="icon-caret-down"></i></a>
	    	      <ul class="dropdown-menu">
			<li><a href="composemail.php">Compose Mail</a></li>
		        <li><a href="inbox.php">Inbox</a></li>
			<li><a href="sent.php">Sent Items</a></li>
		        <li><a href="draft.php">Drafts</a></li>
		      	<li><a href="stared.php">Starred</a></li>
		      </ul>
            </li>
                    
<li class="dropdown grey">
	<a data-toggle="dropdown" href="#" class="dropdown-toggle dropdown-avatar">
<img class="nav-user-photo" src="../images/upload/thumb/<?php echo $thu; ?>" /> 

		<span class="user-info">
			<small>Welcome,</small>
			<?php echo ucfirst($nameprefix); echo get_session('DISPLAY_NAME');?>
		</span>

		<i class="icon-caret-down"></i>
	</a>

	<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
	<li><a href="../viewprofile.php?l_id=<?php echo get_session('LOGIN_ID'); ?>" ><i class="icon-user"></i> <span>My Profile</span></a></li>
        <li><a href="../teamview.php"><i class="icon-cog"></i> <span>My Team</span></a></li>
	
	<?php if($dealer==1){?>
	<li class="grey "><a href="../dealer_availability.php">Availability Time</a></li>
	<?php } ?>
        <li><a href="inbox.php"><i class="icon-envelope"></i> <span>Messages</span></a></li>
        <li><a href="../recent_activities.php" ><i class="icon-bar-chart"></i> <span>Recent Activities</span></a></li>
	<li class="divider"></li>
  <li><a href="../change_password.php?l_id=<?php echo get_session('LOGIN_ID'); ?>" ><i class="icon-cog"></i> <span>Change Password</span></a></li>
	<li>
		<a href="../logout.php">
			<i class="icon-off"></i>
			Logout
		</a>
	</li>
	</ul>
</li>
          
				</ul><!-- /.ace-nav -->
			</div><!-- /.navbar-header -->
		</div><!-- /.container -->
	</div>

	<div class="main-container" id="main-container">
		<script type="text/javascript">
			try{ace.settings.check('main-container' , 'fixed')}catch(e){}
		</script>

		<div class="main-container-inner">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="sidebar" id="sidebar">
				<?php include('left_nav.php'); ?>
			</div>

