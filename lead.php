<?php
session_start();
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
?>


<div class="main-content">
  <div class="container">
    <div class="row">

      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
            <i class="icon-dashboard"></i>
            Lead List
          </h3>
          <h5>
            <span>
              Inspirational Quote
            </span>
          </h5>
        </div>



        <ul class="list-inline pull-right sparkline-box">

          <li class="sparkline-row">
            <h4 class="blue"><span>Last Month</span> 847</h4>
            <div class="sparkline big" data-color="blue"><!--25,11,5,28,25,19,27,6,4,23,20,6--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="green"><span>This Month</span> 1140</h4>
            <div class="sparkline big" data-color="green"><!--28,26,13,18,8,6,24,19,3,6,19,6--></div>
          </li>

          <li class="sparkline-row">
            <h4 class="red"><span>This Week</span> 230</h4>
            <div class="sparkline big"><!--16,23,28,8,12,9,25,11,16,16,17,13--></div>
          </li>

        </ul>
      </div>
    </div>
  </div>



  <div class="container padded">
    <div class="row">


      <!-- Breadcrumb line Campaing List-->
	
      <div id="breadcrumbs">
        <div class="breadcrumb-button blue">
          <span class="breadcrumb-label"><i class="icon-home"></i> Home</span>
          <span class="breadcrumb-arrow"><span></span></span>
        </div>

        

        <div class="breadcrumb-button">
          <span class="breadcrumb-label">
            <i class="icon-dashboard"></i> Lead List
          </span>
	
    
          <span class="breadcrumb-arrow"><span></span></span>
        </div>
      </div>

	
	<!-- Campaing List table-->
		<!--<div class="col-md-6"> -->
	    <!-- find me in partials/stats_table -->

  <div class="container padded">
    <div class="row">
	
	<div class="box">
	  <div class="box-header">

	    <span class="title">Lead List</span>
	  </div>    


		<div id="dataTables">

			<table class="dTable responsive">
		      <thead>
		      <tr>
			<th><div>Check Box</th></div>
		        <th><div>Lead In</th></div>
		        <th><div>Lead First Name</th></div>
			<th><div>Lead Last Name</th></div>
			<th><div>City</th></div>
			<th><div>Phone Number</th></div>
			<th><div>*Lead Result</th></div>
			<th><div>Add New Action</th></div>
		      </tr>
		      </thead>
			<tr>
			  <td class="center"><input type="checkbox" class="icheck" checked id="icheck1"></td>
			  <td>10-5-10</td>
			  <td>Sally</td>
			  <td>Jackson</td>
			  <td>Litiz</td>
			  <td>(717)222-2222</td>
			  <td>Open</td>
			  <td>Yes</td>
			</tr>
		</table>
	 </div>  
     </div>
    </div>

<!--
  <div class="container padded">
    <div class="row">
	
	<div class="box">
	  <div class="box-header">
	    <span class="title">Lead Card</span>
		<ul class="list-inline pull-right sparkline-box">

       
            	<button class="btn btn-sm btn-default">Add New</button>        
          	<button class="btn btn-sm btn-green">Call</button>
		<button class="btn btn-sm btn-blue">Email</button>
		<button class="btn btn-sm btn-red">Print</button>
		<button class="btn btn-sm btn-green">Creat Task</button>

	  </div>    
		<div class="row">

  <div class="col-md-6">
    <!-- find me in partials/accordions 

<div class="accordion" id="accordion2">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
        Name and Address
      </a>
    </div>
    
      
        <div class="box-content">

           <div class="padded">

            	<div class="form-group">
			<label class="control-label col-lg-2">First&nbsp;Name,&nbsp;Last&nbsp;Name&nbsp;:</label><br>
		</div>		
		<div class="form-group">	
			<label class="control-label col-lg-2">First&nbsp;Name&nbsp;2,&nbsp;Last&nbsp;Name&nbsp;2&nbsp;:</label><br>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-2">Address&nbsp;City,&nbsp;State,&nbsp;Zip&nbsp;:</label><br>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-2">Phone1,&nbsp;Phone2,&nbsp;Phone3&nbsp;:</label><br>
		</div>	
		<div class="form-group">
			<label class="control-label col-lg-2">Email.Email&nbsp;Opt&nbsp;In&nbsp;:</label><br>
		</div>		
		<div class="form-group">
			<label class="control-label col-lg-2">Best&nbsp;Time&nbsp;To&nbsp;Contact&nbsp;:</label><br>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-2">Lead&nbsp;Status&nbsp;:&nbsp;&nbsp;Active</label><br>
		</div>
            </div>
      </div>

	 </div>


 </div>

 </div>
	<div class="box">
	      <div class="box-header">
	<div class="row">

  <div class="col-md-6">
    <!-- find me in partials/accordions 

	<div class="accordion" id="accordion2">
		<div class="accordion-group">
    			<div class="accordion-heading">
    				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
					Lead Profile
				</a>
			</div>
		        <div class="box-content">
		           <div class="padded">
		            	<div class="form-group">
					<label class="control-label col-lg-2">Lead&nbsp;Type&nbsp;:</label><br>
				</div>		
				<div class="form-group">	
					<label class="control-label col-lg-2">Referred&nbsp;By&nbsp;:</label><br>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Relationship&nbsp;:</label><br>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Lead&nbsp;Dealer&nbsp;:</label><br>
				</div>	
				<div class="form-group">
					<label class="control-label col-lg-2">Lead&nbsp;In&nbsp;:</label><br>
				</div>	
				<div class="form-group">
					<label class="control-label col-lg-2">Lead&nbsp;Result&nbsp;:</label><br>
				</div>	
				<div class="form-group">
					<label class="control-label col-lg-2">Comments&nbsp;:</label><br>
				</div>	
	
			</div>
		</div>

        </div>
    
	      </div>
	</div>

 </div>



<!--Toggle Tabs

<div class="box">

      <div class="box-header">
        <ul class="nav nav-tabs nav-tabs-left">
          <li class="active">
<a href="#labels" data-toggle="tab">Reports</a></li>
          <li><a href="#badges" data-toggle="tab">Activity</a></li>
	  <li><a href="#badges" data-toggle="tab">Appointment</a></li>
        </ul>
      </div>

      <div class="box-content padded">
        <div class="tab-content">
          <div class="tab-pane active" id="labels">
 		Reports Details Comes Here.            
          </div>
          <div class="tab-pane" id="badges" style="padding: 5px">
         	Activity Details Comes Here.
          </div>
	  <div class="tab-pane" id="badges" style="padding: 5px">
		Appointment Detatils Comes Here.
        </div>
        </div>
      </div>
    </div>






<!--

<div class="row">



<div class="col-md-6">
    <!-- find me in partials/accordions 

	<div class="accordion" id="accordion2">
	  <div class="accordion-group">
	    <div class="accordion-heading">
	      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
		Campaign Status
	      </a>
	    </div>


	
	<div class="col-lg-6">

              <ul class="padded separate-sections">
                <li class="input">
                  <textarea placeholder="This is a textarea" rows="6"></textarea>
                </li>
	     </ul>

		<div class="form-actions">
        	    	<button type="submit" class="btn btn-blue">Save changes</button>
        	    	<button type="button" class="btn btn-default">Cancel</button>
        	</div>
	</div> -->
</div>



	 </div>  
     </div>

    </div>







	

<?php
include "lcas_footer.php";
?>

