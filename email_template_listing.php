<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

//validate_login();

$page_name = "email_template_listing.php";
$page_title = $site_name." -  Email Template Listing";

if( request_get('action')=='delete') {
	$tempid= request_get('tempid');
	$sql="delete from tps_email_templates where id='$tempid'";
	$result=mysql_query($sql) or die(mysql_error());
	header("location:email_template_listing.php");
	exit;
}

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<script type="text/javascript">

$(document).ready(function() {

});

function confirmDelete() 
{
	var agree=confirm("Are you sure do you really want to delete this Template? ");
	if (agree)
		return true ;
	else
		return false ;
}

</script>
<div class="main-content" >
<div class="container">
<br /><br />

  <div class="col-md-16">
    <div class="box" >
      <div class="box-header"><span class="title">Email Template Listing
	<a href="add_email_template.php" ><button type="button" id="list"  class="btn btn-blue" style="margin-left:10px;height:22px;padding-top:2px;">
                        Create New Template
                      </button></a></span>
</div>
      <div class="box-content" style="height:600px;" align="center">
	
<div id="dataTables">	
	<?php
	$userid=get_session('LOGIN_ID');
	$usercreatedid=get_session('LOGIN_USERID');
	$today=date('Y-m-d H:i:s');
	$res=mysql_query("select * from tps_email_templates where is_system_template='0'")or die(mysql_error());

	$table="<table width='98%' class='dTable responsive'><thead>";
	$table.="<tr>
		<td><div>Action</div></td>
		<td><div>Template Name</div></td>
		<td><div>Template Subject</div></td>
		<td><div>Template Messagte</div></td>
		</tr></thead><tbody>";
	$i=1;
	while($r=mysql_fetch_array($res))
	{
		$table.="<tr><td><a href='add_email_template.php?action=edit&tempid=".$r['id']."' title='Edit'><img src='images/edit.png'  title='Edit'/></a> &nbsp;|&nbsp; <a href='email_template_listing.php?action=delete&tempid=".$r['id']." title='Delete' onClick='return confirmDelete();' > <img src='images/block.png' width='15px' class='key_image' title='Delete'/></a>  </td>";
		$table.="<td>".$r['template_name']."</td>";
		$table.="<td>".$r['template_subject']."</td>";
		$table.="<td>".$r['template_content']."</td>";		
		$table.='</tr>';
		$i++;
	}
	$table.="<tbody></table>";
		
	echo $table;
		
	?>
</div> 

      </div>
	<br />
    </div>
	<br /><br />
   </div>

 </div> 
 </div>

<?php


include "lcas_footer.php";

?>
