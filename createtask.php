<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "tasks.php";
if(isset($_REQUEST['action']))
{
	if($_REQUEST['action']=="edit")
	{
		$page_title = $site_name." -  Update Tasks";
	}else{
		$page_title = $site_name." -  Create New Tasks";
	}
}else{
	$page_title = $site_name." -  Create New Tasks";
}

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

$cur_userid=get_session('LOGIN_ID');
$cur_email=get_session('LOGIN_EMAIL');
$cur_username=get_session('DISPLAY_NAME');

$cur_loguserid=get_session('LOGIN_USERID');
include "js/create_task.js";
?>

<div class="main-content" >
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>
	<?php

	if(isset($_REQUEST['action']))
	{
		if($_REQUEST['action']=="edit")
		{
			echo " Update Tasks";
		}else{
			echo " Create New Tasks";
		}
	}else{
		echo " Create New Tasks";
	}?>
	</a>
	</h3>
</div> 

      </div>
    </div>
  </div>
<div class="container">
<?php
$action=0;

$cont_act="";
$who="";
$cont_reason="";
$assigned_to=$cur_userid;
$startdate="";
$duedate="";
$task_status="";
$comments="";
$showtask=0;

$job="add";
$createdby=$cur_loguserid;

if(isset($_REQUEST['action']))
{
	if($_REQUEST['action']=="edit")
	{
		$action=1; // for Edit
		$tid=$_REQUEST['tid'];
		$tseqid=$_REQUEST['tseqid'];

		$job=$_REQUEST['job'];

		$res=mysql_query("select * from tps_task_details where id='$tid' and task_seqid='$tseqid'") or die(mysql_error());
		$r=mysql_fetch_array($res);

		$cont_act=$r['contact_activity'];
		$who=$r['who'];
		$cont_reason=$r['contact_reason'];
		$assigned_to=$r['assigned_to'];
		$startdate=date("m-d-Y h:i A",strtotime($r['start_date']));
		$duedate=date("m-d-Y h:i A",strtotime($r['due_date']));
		$task_status=$r['task_status'];
		$comments=$r['comments'];
		$showtask=$r['showtask'];

		$createddate=$r['createddate'];
		$createdby=$r['createdby'];

		//$cur_loguserid
	
	}

	if($_REQUEST['action']=="NewApptTask")
	{
		$action=1; // for Edit
		$job=$_REQUEST['job'];

		$leadcard_id=$_REQUEST['lcid'];

		$res=mysql_query("select * from tps_lead_card where id='$leadcard_id'")or die(mysql_error());
		$r=mysql_fetch_array($res);

		$cont_act="Call";
		$who="3_".$r['id']."-".$r['lname1']." ".$r['fname1']." ".$r['fname2'];
		$cont_reason="Reset Appt";

		$lead_userid=$r['lead_dealer'];
		$uidres=mysql_query("select id,userid from tps_users where userid='$lead_userid'")or die(mysql_error());
		$uidr=mysql_fetch_array($uidres);
		$assigned_to=$uidr['id'];

		$startdate=date("m-d-Y h:i A",strtotime(date('Y-m-d H:i:s')));
		$duedate="";

		$task_status="Not Started";
		$showtask=0;

		$createddate=date('d-m-Y H:i:s');

	}
		

}
?>
  <div class="col-md-16">
    <div class="box" >
      <div class="box-header">
	<span class="title">
	<?php 
		if($action==1)
		{ 
			if($_REQUEST['action']=="NewApptTask")
			{
				echo "Create New Task";
			}else{
				echo "Update Task"; 
			}
		}else{
			echo "Create New Task";
		}
	?>
	</span>
  </div>
      <div class="box-content padded" style="min-height:500px;" align="center">
	
<form method="post">
<?php 
	if($job!="taskupd"){
?>
	<table width="80%" border="0">
	<tr>
		<td width="20%">Contact Activity </td>
		<td width="40%">
		<select id="cont_act" name="cont_act" class="customddl" tabindex="1">
		<option value="0">Select One</option>
		<?php
			$cmres=mysql_query("select id,name,display,displayorder from tps_contact_method where status='0'")or die(mysql_error());
			while($cmr=mysql_fetch_array($cmres))
			{
				if($cont_act==$cmr['name'])
					echo "<option value='".$cmr['name']."' selected >".$cmr['name']."</option>";
				else
					echo "<option value='".$cmr['name']."'>".$cmr['name']."</option>";
			}
		?>
		</select>	
		</td>
		<td width="20%"></td>
	</tr>
	<tr>
		<td>Who</td>
		<td>
		<select id="who" name="who" class="chzn-select" tabindex="2">
		<option value="0">Select One</option>
		<optgroup label="Campaigns">
                <?php
		$cpres=mysql_query("select id,uid,campaign from tps_campaign where delete_flag='0' and uid='$cur_userid'")or die(mysql_error());
			while($cpr=mysql_fetch_array($cpres))
			{
				$title=$cpr['campaign'];
				$val="1_".$cpr['id']."-".$cpr['campaign']; // 1_ for Campaigns

				if($who==$val)
					echo "<option value='".$val."' selected >".$cpr['campaign']."</option>";
				else
					echo "<option value='".$val."'>".$cpr['campaign']."</option>";

			}
		?>
               </optgroup>
	       <optgroup label="Customers">
		<?php

$cures=mysql_query("select id,uid,leadid,fname1,lname1,fname2 from tps_lead_card where delete_flag='0' and customer_flag='1' and uid='$cur_userid'")or die(mysql_error());

			while($cur=mysql_fetch_array($cures))
			{
				$title=$cur['lname1']." ".$cur['fname1']." ".$cur['fname2'];
				$val="2_".$cur['id']."-".$cur['lname1']." ".$cur['fname1']." ".$cur['fname2'];  // 2_ for Customers

				if($who==$val)
					echo "<option value='".$val."' selected >".$title."</option>";
				else
					echo "<option value='".$val."' >".$title."</option>";

			}
		?>
		</optgroup>
		<optgroup label="Leads">
		<?php

$cures=mysql_query("select id,uid,leadid,fname1,lname1,fname2 from tps_lead_card where delete_flag='0' and uid='$cur_userid'")or die(mysql_error());

			while($cur=mysql_fetch_array($cures))
			{
				$title=$cur['lname1']." ".$cur['fname1']." ".$cur['fname2'];
				$val="3_".$cur['id']."-".$cur['lname1']." ".$cur['fname1']." ".$cur['fname2'];  // 3_ for Leads

				if($who==$val)
					echo "<option value='".$val."' selected >".$title."</option>";
				else
					echo "<option value='".$val."' >".$title."</option>";
			}
		?>
		</optgroup>
		<optgroup label="Team Members">
		<?php 
			$res=mysql_query("select id,username,fname,lname,nickname from tps_users where status='1'")or die(mysql_error());

			while($r=mysql_fetch_array($res))
			{	
				$title="";
				if($r['nickname']!="")
					$title=ucfirst($r['nickname'])." ".$r['lname'];
				else
					$title=ucfirst($r['fname'])." ".$r['lname'];

				$val="4_".$r['id']."-".ucfirst($r['fname'])." ".$r['lname'];  // 4_ for Team Members

				if($who==$val)
					echo "<option value='".$val."' selected >".$title."</option>";
				else
					echo "<option value='".$val."' >".$title."</option>";
			}
		?>
		</optgroup>

		</select>	
		</td>
		<td></td>
	</tr>
	<tr>
		<td>Contact Reason</td>
		<td>
		<select id="cont_reason" name="cont_reason" class="customddl" tabindex="3">
		<option value="0">Select One</option>
		<?php
			$crres=mysql_query("select id,name,display,displayorder from tps_contact_reason where status='0'")or die(mysql_error());
			while($crr=mysql_fetch_array($crres))
			{
				if($cont_reason==$crr['name'])
					echo "<option value='".$crr['name']."' selected >".$crr['name']."</option>";
				else
					echo "<option value='".$crr['name']."'>".$crr['name']."</option>";
			}
		?>
		</select>	

		</td>
		<td></td>
	</tr>
	<tr>
		<td>Assigned To</td>
		<td>
		<select id="assigned_to" name="assigned_to" multiple="multiple" style="width:80%" placeholder="Select Members" tabindex="4">
		<?php 
			echo get_Userslist_Task($assigned_to); 
		?>
		</select>	
	
		</td>
		<td><button class="btn btn-blue" tabindex="10" style="width:60%;" type="button" id="openinemail" >Open in Email</button></td>
	</tr>
	<tr>
		<td>Start Date</td>
		<td> 
	<input type="text" id="startdate" tabindex="5" value="<?php echo $startdate; ?>" placeholder="Select Start Date" class="customddl"/>
		</td>
		<td><button class="btn btn-blue" tabindex="11" style="width:60%;" type="button" id="openinleadlist">Open in Lead List</button></td>
	</tr>
	<tr>
		<td>Due Date</td>
		<td>
	<input type="text" id="duedate" tabindex="6" value="<?php echo $duedate; ?>" placeholder="Select Due Date" class="customddl"/>    </td>
		<td>
	<button class="btn btn-blue" tabindex="12" style="width:60%;" type="button" id="openinleaddetails">Open in Lead Details</button></td>
	</tr>
	<tr>
		<td>Task Status</td>
		<td>
			<select id="task_status" name="task_status" class="customddl" tabindex="7">
				<option value="0">Select One</option>
				<?php
					$tsres=mysql_query("select id,name from tps_task_status where status='0'")or die(mysql_error());
					while($tsr=mysql_fetch_array($tsres))
					{
						if($task_status==$tsr['name'])
							echo "<option value='".$tsr['name']."' selected >".$tsr['name']."</option>";
						else
							echo "<option value='".$tsr['name']."'>".$tsr['name']."</option>";
					}
				?>
			</select>
		</td>
		<td>
<button class="btn btn-blue" tabindex="13" style="width:60%;" type="button" id="openincustomerlist">Open in Customer List</button></td>
	</tr>
	<tr>
		<td>Comments</td>
		<td><textarea id="task_comments" tabindex="8" name="task_comments" style="width:70%;"><?php echo $comments; ?></textarea></td>
		<td></td>
	</tr>
	<?php
		if($createdby==$cur_loguserid)
		{
		 	
	?>
	<tr>
		<td>Do you want to display this task to your calender</td>
		<td><input type="checkbox" tabindex="9" name="selfcalender" id="selfcalender" <?php if($showtask=="1") echo "checked"; ?> ></td>
		<td></td>
	</tr>
	<?php 
		}
	?>
	<tr>
		<td>Created By : <?php 
					if($action==1){
						$c=mysql_fetch_array(mysql_query("select fname,lname from tps_users where userid='$createdby'")); 
						echo ucfirst($c['fname'])." ".$c['lname'];
					}else
						echo ucfirst($cur_username); 
				 ?>
		</td>
		<td>Created On : <?php 
						if($action==1)
						{
							echo date('m-d-Y h:i A',strtotime($createddate));
						}else{
							$createddate=date('d-m-Y H:i:s');
							echo date('m-d-Y h:i A',strtotime($createddate));
						} 
					?>
		</td>
		<td>	
			<?php if($action==1){ ?>
			<input type="hidden" name="tid" id="tid" value="<?php echo $tid; ?>" />			
			<input type="hidden" name="tseqid" id="tseqid" value="<?php echo $tseqid; ?>" />
			<?php if($job=="NewApptTask"){?>
				<button id="savenewtask" type="button" tabindex="14" class="btn btn-blue">Save</button> &nbsp;&nbsp;
			<?php }else{?>
				<button id="updtask1" type="button" tabindex="14" class="btn btn-blue">Update</button> &nbsp;&nbsp;
			<?php } // end of NewApptTask
			}else{ ?>
				<button id="savenewtask" type="button" tabindex="14" class="btn btn-blue">Save</button> &nbsp;&nbsp;
			<?php } ?>
		<button id="reset" type="reset" onclick="javascript:window.location.href='task_listing.php';" tabindex="15" class="btn btn-default">Cancel</button>
		</td>
	</tr>
	</table> 
</form><br>
<?php 
}else if($job=="taskupd")
{
?>


<table width="80%" border="0">
	<tr>
		<td width="20%">Contact Activity </td>
		<td width="40%"><?php echo $cont_act; ?></td>
		<td width="20%"></td>
	</tr>
	<tr>
		<td>Who</td>
		<td><?php $w=explode("-",$who); echo $w[1]; ?> <input type="hidden" name="who" id="who" value="<?php echo $who; ?>" /> </td>
		<td></td>
	</tr>
	<tr>
		<td>Assigned To</td>
		<td><?php 
			$re=mysql_fetch_array(mysql_query("select fname,lname from tps_users where id='$assigned_to'")); 
			echo ucfirst($re['fname'])." ".$re['lname']; 
		    ?>
		</td>
		<td><button class="btn btn-blue" tabindex="3" style="width:60%;" type="button" id="openinemail" >Open in Email</button></td>
	</tr>
	<tr>
		<td>Start Date</td>
		<td><?php echo $startdate; ?></td>
		<td><button class="btn btn-blue" tabindex="4" style="width:60%;" type="button" id="openinleadlist">Open in Lead List</button></td>
	</tr>
	<tr>
		<td>Due Date</td>
		<td><?php echo $duedate; ?>  </td>
		<td>
<button class="btn btn-blue" tabindex="5" style="width:60%;" type="button" id="openinleaddetails">Open in Lead Details</button></td>
	</tr>
	<tr>
		<td>Task Status</td>
		<td>
			<select id="task_status" name="task_status" class="customddl" tabindex="1">
				<option value="0">Select One</option>
				<?php
					$tsres=mysql_query("select id,name from tps_task_status where status='0'")or die(mysql_error());
					while($tsr=mysql_fetch_array($tsres))
					{
						if($task_status==$tsr['name'])
							echo "<option value='".$tsr['name']."' selected >".$tsr['name']."</option>";
						else
							echo "<option value='".$tsr['name']."'>".$tsr['name']."</option>";
					}
				?>
			</select>
		</td>
		<td>
<button class="btn btn-blue" tabindex="6" style="width:60%;" type="button" id="openincustomerlist">Open in Customer List</button></td>
	</tr>
	<tr>
		<td>Comments</td>
		<td><?php echo $comments; ?></td>
		<td></td>
	</tr>
	<?php
		if($createdby==$cur_loguserid)
		{
	?>
	<tr>
		<td>Do you want to display this task to your calender</td>
		<td><input type="checkbox" tabindex="2" name="selfcalender" id="selfcalender" <?php if($showtask=="1") echo "checked"; ?> ></td>
		<td></td>
	</tr>
	<?php 
		}
	?>
	<tr>
		<td>Created By : <?php 
					if($action==1){
						$c=mysql_fetch_array(mysql_query("select fname,lname from tps_users where userid='$createdby'")); 
						echo ucfirst($c['fname'])." ".$c['lname'];
					}else
						echo ucfirst($cur_username); 
				 ?>
		</td>
		<td>Created Date Time : <?php 
						if($action==1)
						{
							echo date('m-d-Y h:i A',strtotime($createddate));
						}else{
							$createddate=date('d-m-Y H:i:s');
							echo date('m-d-Y h:i A',strtotime($createddate));
						} 
					?>
		</td>
		<td>	
			<input type="hidden" name="tid" id="tid" value="<?php echo $tid; ?>" />			
			<input type="hidden" name="tseqid" id="tseqid" value="<?php echo $tseqid; ?>" />

			<button id="updtaskstatus" type="button" tabindex="7" class="btn btn-blue">Update Task Status</button> &nbsp;&nbsp;
				
		<button id="reset" type="reset" tabindex="8" onclick="javascript:window.location.href='task_listing.php';" class="btn btn-default">Cancel</button>
		</td>
	</tr>
	</table> 
</form><br>


<?php } ?>
<br>


      </div>
	<br />
    </div>
	<br /><br />
   </div>

 </div> 
 </div>

<div id="spinner"></div>

<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>

<script type="text/javascript">

$(document).ready(function() {

	$('#startdate').datetimepicker({
		format:'m/d/Y h:i A',
		lang:'en',
		formatTime:'h:i A',
		allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
	});

	$('#duedate').datetimepicker({
		format:'m/d/Y h:i A',
		lang:'en',
		formatTime:'h:i A',
		allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ],
		minDate:$('#startdate').val()
	});

	
});


</script>

<?php

include "lcas_footer.php";

?>
