<?php
session_start();

require_once("include/tps_db_conn.php");
require_once("include/tps_constants.php");
require_once("include/tps_gen_functions.php");

if(isset($_REQUEST['type']))
{

	$cur_uid=get_session('LOGIN_ID');
	$cur_userid=get_session('LOGIN_USERID');
	$cur_email=get_session('LOGIN_EMAIL');
	$cur_username=get_session('DISPLAY_NAME');

	$user_logdispname=get_session('DISPLAY_NAME');
	$user_logid=get_session('LOGIN_ID');
	$url= $_SERVER['HTTP_REFERER'];
	
	if($_REQUEST['type']=="savenewtask")		
	{
		$cont_act=request_get('cont_act');
		$who=request_get('who');
		$cont_reason=request_get('cont_reason');
		$assigned_to=implode(",",$_REQUEST['assigned_to']);	
		$startdate=date('Y-m-d H:i:s', strtotime(request_get('startdate')));
		$duedate=date('Y-m-d H:i:s', strtotime(request_get('duedate')));
		$task_status=request_get('task_status');
		$task_comments=request_get('task_comments');
		$dispappt=request_get('dispappt');
	
		$createddate=date('Y-m-d H:i:s');

		$senttousernames=get_userslist_by_userid_for_msg($assigned_to);

		$whoname=explode("-",$who);
		$whoname1=$whoname[1];

		$msgbody="";
		$msgbody.="<div style=color:#000; line-height:25px;> Hi, <br><br> The Following Task assigned by $cur_username to you <br><br>";
		$msgbody.="<table cellpadding=10 class=table-bordered width=80%>
				<tr><td>Contact Activity</td><td>$cont_act</td></tr>
				<tr><td>Who</td><td>$whoname1</td></tr>
				<tr><td>Contact Reason</td><td>$cont_reason</td></tr>
				<tr><td>Assigned To</td><td>$senttousernames</td></tr>
				<tr><td>Start Date</td><td>".date('m/d/Y h:i A',strtotime($startdate))."</td></tr>
				<tr><td>Due Date</td><td>".date('m/d/Y h:i A',strtotime($duedate))."</td></tr>
				<tr><td>Task Status</td><td>$task_status</td></tr>
				<tr><td>Task Comment</td><td>$task_comments</td></tr>
			</table>";
		$msgbody.="<br><br> Task Created by $cur_username at ".date('m/d/Y h:i A',strtotime($createddate))." <br>";
		$msgbody.="<br></div><br>";
		
		$tbname="task_seqid";
		$fieldname="id";

		$whoarr=explode("_",$who);
		
		$whotype=$whoarr[0];
		$whoid=$whoarr[1];
		
		if($whotype=="2")
		{
			$wres=mysql_query("select id,leadid from tps_lead_card where customer_flag='1' and id='$whoid'")or die(mysql_error());
			$wre=mysql_fetch_array($wres);
			
			$lcid=$wre['leadid'];

			$lres=mysql_query("select id,leadid from tps_lead_card where id='$lcid'")or die(mysql_error());
			$lre=mysql_fetch_array($lres);
			$lead_id=$lre['leadid'];

			$sql2="insert into tps_lead_status_update set leadid='$lead_id', activity='$cont_act', contact_reason='$cont_reason', comments='$task_comments', modifiedby='$cur_username', modifiedon='$createddate' ";
		
			$result2=mysql_query($sql2)or die(mysql_error()); 

			$log_desc= ucfirst($user_logdispname)." at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank >$url</a></b>";
			tps_log_error(__INFO__, __FILE__, __LINE__, "Lead Status Updated ", $user_logid, $log_desc);

		}

		if($whotype=="3")
		{

			$lres=mysql_query("select id,leadid from tps_lead_card where id='$whoid'")or die(mysql_error());
			$lre=mysql_fetch_array($lres);
			$lead_id=$lre['leadid'];

			$sql2="insert into tps_lead_status_update set leadid='$lead_id', activity='$cont_act', contact_reason='$cont_reason', comments='$task_comments', modifiedby='$cur_username', modifiedon='$createddate' ";
			
			$result2=mysql_query($sql2)or die(mysql_error()); 

			$log_desc= ucfirst($user_logdispname)." at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank >$url</a></b>";
			tps_log_error(__INFO__, __FILE__, __LINE__, "Lead Status Updated ", $user_logid, $log_desc);

		}

		$start=date('Y-m-d H:i:s',strtotime($startdate));
		$end=date('Y-m-d H:i:s',strtotime($startdate)+10800);

		$task_seqid=generate_id($tbname,$fieldname);

		$task_sql="INSERT INTO `tps_task_details` (`id`, `task_seqid`, `userid`, `contact_activity`, `who`, `contact_reason`, `assigned_to`, `assigned_tousers`, `start_date`, `due_date`, `task_status`, `comments`, `showtask`, `createddate`, `createdby`) VALUES (NULL, '$task_seqid', '$cur_uid', '$cont_act', '$who', '$cont_reason', '$assigned_to', '$senttousernames', '$startdate', '$duedate', '$task_status', '$task_comments', '$dispappt', '$createddate', '$cur_userid')";

		mysql_query($task_sql) or die("Save New Task : ".mysql_error());

		$taskid=mysql_insert_id();

		if($dispappt==1)
		{		
			$title="New Task assigned by me";

			$task_event_sql="INSERT INTO `tps_tasks` (`id`, `task_seqid`, `task_id`, `taskuserid`, `title`, `appttype`, `allDay`, `remarks`, `start`, `end`, `url`, `createdby`, `modifiedby`, `modifiedtime`, `delete_flag`) VALUES (NULL, '$task_seqid', '$taskid', '$cur_uid', '$title', 'Task', '0', '', '$start', '$end', '', '$cur_userid', '', '', '')";

			mysql_query($task_event_sql) or die(mysql_error());
		}

		$title="New Task from $cur_username";

		$userids=explode(",",$assigned_to);
		
		for($i=0;$i<count($userids);$i++)
		{
			$assigned_userid=$userids[$i];

			$sender=$userids[$i];

			$task_sql="INSERT INTO `tps_task_details` (`id`, `task_seqid`, `userid`, `contact_activity`, `who`, `contact_reason`, `assigned_to`, `assigned_tousers`, `start_date`, `due_date`, `task_status`, `comments`, `showtask`, `createddate`, `createdby`) VALUES (NULL, '$task_seqid', '$assigned_userid', '$cont_act', '$who', '$cont_reason', '$assigned_to', '$senttousernames', '$startdate', '$duedate', '$task_status', '$task_comments', '1', '$createddate', '$cur_userid')";

			mysql_query($task_sql) or die("Save New Task : ".mysql_error());

			$taskid=mysql_insert_id();

			$task_event_sql="INSERT INTO `tps_tasks` (`id`, `task_seqid`, `task_id`, `taskuserid`, `title`, `appttype`, `allDay`, `remarks`, `start`, `end`, `url`, `createdby`, `modifiedby`, `modifiedtime`, `delete_flag`) VALUES (NULL, '$task_seqid', '$taskid', '$sender', '$title', 'Task', '0', '', '$start', '$end', '', '$cur_userid', '', '', '')";

			mysql_query($task_event_sql) or die(mysql_error());

			
		}
		
		$log_desc= ucfirst($user_logdispname)." Created a Task <b>Contact Activity - $cont_act, Who - $who, Contact Reason - $cont_reason </b>  at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank >$url</a></b>";
		tps_log_error(__INFO__, __FILE__, __LINE__, "Task Created", $user_logid, $log_desc);

		/* Start of Sending Messages */

		mysql_query("INSERT INTO `msg_seqid` (`id`) VALUES (NULL);");

		$msg_seqid=mysql_insert_id();

		$sendto=$assigned_to;
		$subject=htmlentities($title, ENT_QUOTES, "UTF-8");
		$message=htmlentities($msgbody, ENT_QUOTES, "UTF-8");

		send_internal_message($msg_seqid,$sendto,$subject,$message);
		
		/* End of Sending Messages */

		/*$msgurl="/messages/sent.php";

		$log_desc=" <b>$subject</b> subjected message sent by $user_dispname <b><a href=$msgurl target=_blank >http://".$_SERVER['SERVER_NAME']."$msgurl</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Task Message Sent", $user_logid, $log_desc);*/

	}


	if($_REQUEST['type']=="updtask")		
	{
		$tid=request_get('tid');
		$tseqid=request_get('tseqid');
		$cont_act=request_get('cont_act');
		$who=request_get('who');
		$cont_reason=request_get('cont_reason');
		$assigned_to=implode(",",$_REQUEST['assigned_to']);	
		$startdate=date('Y-m-d H:i:s', strtotime(request_get('startdate')));
		$duedate=date('Y-m-d H:i:s', strtotime(request_get('duedate')));
		$task_status=request_get('task_status');
		$task_comments=request_get('task_comments');
		$dispappt=request_get('dispappt');
	
		$createddate=date('Y-m-d H:i:s');

		$senttousernames=get_userslist_by_userid_for_msg($assigned_to);

		$whoname=explode("-",$who);
		$whoname1=$whoname[1];

		$msgbody="";
		$msgbody.="<div style=color:#000; line-height:25px;> Hi, <br><br> The Following Task assigned by $cur_username to you <br><br>";
		$msgbody.="<table cellpadding=10 class=table-bordered width=80%>
				<tr><td>Contact Activity</td><td>$cont_act</td></tr>
				<tr><td>Who</td><td>$whoname1</td></tr>
				<tr><td>Contact Reason</td><td>$cont_reason</td></tr>
				<tr><td>Assigned To</td><td>$senttousernames</td></tr>
				<tr><td>Start Date</td><td>".date('m/d/Y h:i A',strtotime($startdate))."</td></tr>
				<tr><td>Due Date</td><td>".date('m/d/Y h:i A',strtotime($duedate))."</td></tr>
				<tr><td>Task Status</td><td>$task_status</td></tr>
				<tr><td>Task Comment</td><td>$task_comments</td></tr>
			</table>";
		$msgbody.="<br><br> Task Created by $cur_username at ".date('m/d/Y h:i A',strtotime($createddate))." <br>";
		$msgbody.="<br></div><br>";
		
		$tbname="task_seqid";
		$fieldname="id";

		$whoarr=explode("_",$who);
		
		$whotype=$whoarr[0];
		$whoid=$whoarr[1];
		
		
		if($whotype=="2")
		{
			$wres=mysql_query("select id,leadid from tps_lead_card where customer_flag='1' and id='$whoid'")or die(mysql_error());
			$wre=mysql_fetch_array($wres);
			
			$lcid=$wre['leadid'];

			$lres=mysql_query("select id,leadid from tps_lead_card where id='$lcid'")or die(mysql_error());
			$lre=mysql_fetch_array($lres);
			$lead_id=$lre['leadid'];

			$sql2="insert into tps_lead_status_update set leadid='$lead_id', activity='$cont_act', contact_reason='$cont_reason', comments='$task_comments', modifiedby='$cur_username', modifiedon='$createddate' ";
		
			$result2=mysql_query($sql2)or die(mysql_error()); 

		}

		if($whotype=="3")
		{

			$lres=mysql_query("select id,leadid from tps_lead_card where id='$whoid'")or die(mysql_error());
			$lre=mysql_fetch_array($lres);
			$lead_id=$lre['leadid'];

			$sql2="insert into tps_lead_status_update set leadid='$lead_id', activity='$cont_act', contact_reason='$cont_reason', comments='$task_comments', modifiedby='$cur_username', modifiedon='$createddate' ";
			
			$result2=mysql_query($sql2)or die(mysql_error()); 

		}

		$start=date('Y-m-d H:i:s',strtotime($startdate));
		$end=date('Y-m-d H:i:s',strtotime($startdate)+10800);

		$task_seqid=generate_id($tbname,$fieldname);

		$task_sql="INSERT INTO `tps_task_details` (`id`, `task_seqid`, `userid`, `contact_activity`, `who`, `contact_reason`, `assigned_to`, `assigned_tousers`, `start_date`, `due_date`, `task_status`, `comments`, `showtask`, `createddate`, `createdby`) VALUES (NULL, '$task_seqid', '$cur_uid', '$cont_act', '$who', '$cont_reason', '$assigned_to', '$senttousernames', '$startdate', '$duedate', '$task_status', '$task_comments', '$dispappt', '$createddate', '$cur_userid')";

		mysql_query($task_sql) or die("Save New Task : ".mysql_error());

		$taskid=mysql_insert_id();

		if($dispappt==1)
		{		
			$title="New Task assigned by me";

			$task_event_sql="INSERT INTO `tps_tasks` (`id`, `task_seqid`, `task_id`, `taskuserid`, `title`, `appttype`, `allDay`, `remarks`, `start`, `end`, `url`, `createdby`, `modifiedby`, `modifiedtime`, `delete_flag`) VALUES (NULL, '$task_seqid', '$taskid', '$cur_uid', '$title', 'Task', '0', '', '$start', '$end', '', '$cur_userid', '', '', '')";

			mysql_query($task_event_sql) or die(mysql_error());
		}


		/* Sending Messages */

		$sendto=$assigned_to;
		$subject=htmlentities("A New Task from LCAS", ENT_QUOTES, "UTF-8");
		$message=htmlentities($msgbody, ENT_QUOTES, "UTF-8");
		$attids="";

		mysql_query("INSERT INTO `msg_seqid` (`id`) VALUES (NULL);");

		$msg_seqid=mysql_insert_id();

		$inbox_sql_frmusr="INSERT INTO `inbox` (`id`, `msg_seqid`, `msguserid`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msg_seqid', '$cur_uid', '$cur_uid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'inbox', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

		mysql_query($inbox_sql_frmusr)or die("Inbox : ".mysql_error());

		/* Sending Messages */

		$title="New Task from $cur_username";

		$userids=explode(",",$assigned_to);
		
		for($i=0;$i<count($userids);$i++)
		{
			$assigned_userid=$userids[$i];

			$sender=$userids[$i];

			$task_sql="INSERT INTO `tps_task_details` (`id`, `task_seqid`, `userid`, `contact_activity`, `who`, `contact_reason`, `assigned_to`, `assigned_tousers`, `start_date`, `due_date`, `task_status`, `comments`, `showtask`, `createddate`, `createdby`) VALUES (NULL, '$task_seqid', '$assigned_userid', '$cont_act', '$who', '$cont_reason', '$assigned_to', '$senttousernames', '$startdate', '$duedate', '$task_status', '$task_comments', '1', '$createddate', '$cur_userid')";

			mysql_query($task_sql) or die("Save New Task : ".mysql_error());

			$taskid=mysql_insert_id();

			$task_event_sql="INSERT INTO `tps_tasks` (`id`, `task_seqid`, `task_id`, `taskuserid`, `title`, `appttype`, `allDay`, `remarks`, `start`, `end`, `url`, `createdby`, `modifiedby`, `modifiedtime`, `delete_flag`) VALUES (NULL, '$task_seqid', '$taskid', '$sender', '$title', 'Task', '0', '', '$start', '$end', '', '$cur_userid', '', '', '')";

			mysql_query($task_event_sql) or die(mysql_error());

			/* Sending Messages */

			$inbox_sql="INSERT INTO `inbox` (`id`, `msg_seqid`, `msguserid`, `from_userid`, `from_username`, `to_userids`, `to_usernames`, `subject`, `body`, `attachment_ids`, `msgtype`, `read_flag`, `status`, `createddate`, `createdby`) VALUES (NULL, '$msg_seqid', '$sender', '$cur_uid', '$cur_username', '$sendto', '$senttousernames', '$subject', '$message', '$attids', 'inbox', '0', '0', '".date('Y-m-d H:i:s')."', '$cur_username')";

			mysql_query($inbox_sql)or die("Inbox : ".mysql_error());

			/* Sending Messages */
		}
		
		
	}

	if($_REQUEST['type']=="updtaskstatus")		
	{
		$tid=request_get('tid');
		$tseqid=request_get('tseqid');
		$task_status=request_get('task_status');
	
		$createddate=date('Y-m-d H:i:s');

		$task_sql="UPDATE `tps_task_details` SET `task_status`='$task_status', `modifieddate`='$createddate', `modifiedby`='$cur_userid' where task_seqid='$tseqid'";

		mysql_query($task_sql)or die(mysql_error());

		$log_desc= ucfirst($user_logdispname)." at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank >$url</a></b>";
		tps_log_error(__INFO__, __FILE__, __LINE__, "Task Status Updated", $user_logid, $log_desc);

		$res=mysql_query("select * from `tps_task_details` where task_seqid='$tseqid'");

		$uesrids=array();
		$i=0;
		while($r=mysql_fetch_array($res))
		{
			if($cur_uid!=$r['userid'])
			{
				$userids[$i]=$r['userid'];
			}

			$cont_act=$r['contact_activity'];

			$who=$r['who'];
			$whoname=explode("-",$who);
			$whoname1=$whoname[1];

			$cont_reason=$r['contact_reason'];
			$startdate=$r['start_date'];		
			$duedate=$r['due_date'];
			$task_status=$r['task_status'];
			$task_comments=$r['comments'];	
			$i++;
		}

		$assigned_to=implode(",",$userids);

		/* Start of Sending Messages */

		$title="Task Status Updated by $cur_username";

		$senttousernames=get_userslist_by_userid_for_msg($assigned_to);

	$msgbody="<div style=color:#000; line-height:25px;> Hi, <br><br> The Following Task status has been updated by $cur_username. <br><br>";
		$msgbody.="<table cellpadding=10 class=table-bordered width=80%>
				<tr><td>Contact Activity</td><td>$cont_act</td></tr>
				<tr><td>Who</td><td>$whoname1</td></tr>
				<tr><td>Contact Reason</td><td>$cont_reason</td></tr>
				<tr><td>Assigned To</td><td>$senttousernames</td></tr>
				<tr><td>Start Date</td><td>".date('m/d/Y h:i A',strtotime($startdate))."</td></tr>
				<tr><td>Due Date</td><td>".date('m/d/Y h:i A',strtotime($duedate))."</td></tr>
				<tr><td><b>Task Status</b></td><td><b>$task_status</b></td></tr>
				<tr><td>Task Comment</td><td>$task_comments</td></tr>
			</table>";
		$msgbody.="<br></div><br>";

		mysql_query("INSERT INTO `msg_seqid` (`id`) VALUES (NULL);");

		$msg_seqid=mysql_insert_id();

		$sendto=$assigned_to;
		$subject=htmlentities($title, ENT_QUOTES, "UTF-8");
		$message=htmlentities($msgbody, ENT_QUOTES, "UTF-8");

		send_internal_message($msg_seqid,$sendto,$subject,$message);
		
		/* End of Sending Messages */

		$msgurl="/messages/sent.php";

		$log_desc=" <b>$subject</b> subjected message sent by $user_dispname <b><a href=$msgurl target=_blank >http://".$_SERVER['SERVER_NAME']."$msgurl</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Task Update Message Sent", $user_logid, $log_desc);

	}

	if($_REQUEST['type']=="taskdelete")
	{
		$tid=request_get('taskid');
		$tseqid=request_get('taskseqid');

		$res=mysql_query("select * from `tps_task_details` where task_seqid='$tseqid'");

		$uesrids=array();
		$i=0;
		while($r=mysql_fetch_array($res))
		{
			if($cur_uid!=$r['userid'])
			{
				$userids[$i]=$r['userid'];
			}
			
			$cont_act=$r['contact_activity'];

			$who=$r['who'];
			$whoname=explode("-",$who);
			$whoname1=$whoname[1];

			$cont_reason=$r['contact_reason'];
			$startdate=$r['start_date'];		
			$duedate=$r['due_date'];
			$task_status=$r['task_status'];
			$task_comments=$r['comments'];	
			$i++;
		}

		if(!empty($userids))
		{
			$assigned_to=implode(",",$userids);
		}else{
			$assigned_to=$cur_uid;
		}

		/* Start of Sending Messages */

		$title="Task Deleted by $cur_username";

		$senttousernames=get_userslist_by_userid_for_msg($assigned_to);

		$msgbody="<div style=color:#000; line-height:25px;> Hi, <br><br><b>The Following Task has been deleted by $cur_username.</b><br><br>";
		$msgbody.="<table cellpadding=10 class=table-bordered width=80%>
				<tr><td>Contact Activity</td><td>$cont_act</td></tr>
				<tr><td>Who</td><td>$whoname1</td></tr>
				<tr><td>Contact Reason</td><td>$cont_reason</td></tr>
				<tr><td>Assigned To</td><td>$senttousernames</td></tr>
				<tr><td>Start Date</td><td>".date('m/d/Y h:i A',strtotime($startdate))."</td></tr>
				<tr><td>Due Date</td><td>".date('m/d/Y h:i A',strtotime($duedate))."</td></tr>
				<tr><td><b>Task Status</b></td><td><b>$task_status</b></td></tr>
				<tr><td>Task Comment</td><td>$task_comments</td></tr>
			</table>";
		$msgbody.="<br> Task Deleted by $cur_username on ".date('m/d/Y h:i A')." <br>";
		$msgbody.="<br></div><br>";

		mysql_query("INSERT INTO `msg_seqid` (`id`) VALUES (NULL);");

		$msg_seqid=mysql_insert_id();

		$sendto=$assigned_to;
		$subject=htmlentities($title, ENT_QUOTES, "UTF-8");
		$message=htmlentities($msgbody, ENT_QUOTES, "UTF-8");

		send_internal_message($msg_seqid,$sendto,$subject,$message);
		
		/* End of Sending Messages */

		$msgurl="/messages/sent.php";

		$log_desc=" <b>$subject</b> subjected message sent by $user_dispname <b><a href=$msgurl target=_blank >http://".$_SERVER['SERVER_NAME']."$msgurl</a></b>";

		tps_log_error(__INFO__, __FILE__, __LINE__, "Task Delete Message Sent", $user_logid, $log_desc);


		$sql="delete from tps_task_details where task_seqid='$tseqid'";
		$result=mysql_query($sql) or die(mysql_error());

		$log_desc= ucfirst($user_logdispname)." at ".date('M d Y h:i:s A')." <b><a href=$url target=_blank >$url</a></b>";
		tps_log_error(__INFO__, __FILE__, __LINE__, "Task Deleted", $user_logid, $log_desc);

	}


}

?>
