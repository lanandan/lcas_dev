<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "show_referral_rewards.php";
$page_title = $site_name." -  Referral Rewards ";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
include "js/task_listing.js";

?>
<script type="text/javascript">
function confirmDelete(tid,tsqid) 
{
	var agree=confirm("Are you sure do you want to delete this Task? ");
	if (agree){
		$("#spinner").show();
		var data = {
			    	type: 'taskdelete',
				taskid:tid,
				taskseqid:tsqid
			 }
		$.ajax({
		    		type: "POST",
		    		url: "task_actions.php",
		    		data: data,
		    		success: function(resp) {			
					$("#spinner").hide();
					bootbox.alert("Task Successfully Deleted",function(){
						window.location.href="task_listing.php";
					});	
				}
		});

	}
	else
		return false ;
}
</script>
<div id="spinner"></div>
<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title"><i class="icon-magic"></i>Referral Rewards&nbsp;&nbsp;</h3></div>
      </div>
    </div>
  </div>

<div class="container">

<div class="box">

<div class="box-header"><span class="title">Referral Rewards</span></div>
<div class="box-content padded">
	
<?php
$lid=request_get('lid');
$sql="select * from tps_lead_card where id='$lid'";
$res=mysql_query($sql) or die(mysql_error());

$r=mysql_fetch_array($res);

$lead_type=$r['lead_type'];
$lead_subtype=$r['lead_subtype'];
$lead_dealer=$r['lead_dealer'];

$leadfullname=$r['fname1']." ".$r['lname1']." & ".$r['fname2']." ".$r['lname2'];

if($r['customer_flag']=='1'){
	$refby='1_'.$r['id'].'';
}
else{
	$refby='3_'.$r['id'].'';
}

$lt='Referral';
$sql_qry = "select * from tps_lead_card where referred_by='$refby' and delete_flag='0' order by id desc";
$result = mysql_query($sql_qry) or die(mysql_error());

$ReferralActivityTable="<table border='0' class='table table-bordered' >";
$ReferralActivityTable.="<thead><tr>
				<th>Name</th>
				<th>Phone Number</th>
				<th>Lead Result</th>
				<th>Appt Date & Time</th>
				<th>Appt Status</th>
				<th>Bonus Rewards</th>
				<th>Demo Rewards</th>
			</tr></thead><tbody>";
while($row = mysql_fetch_array($result))
{
	$refleadfullname=$row['fname1']." ".$row['lname1']." & ".$row['fname2']." ".$row['lname2'];

	$phoneNo=$row['phone1'];
	if( $row['phone2'] != ''  && $row['phone2'] !='')
		$phoneNo.=", <br/>".$row['phone2'];
	if( $row['phone3'] != '' &&  $row['phone3'] !='' )
		$phoneNo.=", <br/>".$row['phone3'];

	$lead_result=$row['lead_result'];

	$ref_apt_sql="select DATE_FORMAT(start , '%b %d %Y %h:%i %p') as aptdate from tps_events where lead_id='$row[id]'";
	$ref_apt_sql_r=mysql_query($ref_apt_sql) or die(mysql_error());
	$rasr=mysql_fetch_array($ref_apt_sql_r);
	
	$aptdate=$rasr['aptdate'];

	$lead_id=$row['leadid'];
	$sql12="select id,appt_result from tps_appt_status_update where lead_id= '$lead_id'and delete_flag='0' order by id desc ";
	$result12=mysql_query($sql12) or die(mysql_error());
	$aptres=mysql_fetch_array($result12);
	$apptresult=$aptres['appt_result'];

	$ReferralActivityTable.="<tr>";
	$ReferralActivityTable.="<td>$refleadfullname</td>
				<td>$phoneNo</td>
				<td>$lead_result</td>
				<td>$aptdate</td>
				<td>$apptresult</td>
				<td>Bonus Rewards</td>
				<td>Demo Rewards</td>";
	$ReferralActivityTable.="</tr>";
}
$ReferralActivityTable.="</tbody></table>";


$subsql=getRefferalRewardsTemplete($lead_type,$lead_subtype,$lead_dealer);

$sql ="SELECT id, name, content from tps_ref_rewards_template where id=($subsql) and status='0'";

//echo "<pre>$sql</pre>";
$rs_list=mysql_query($sql) or die(mysql_error());
$rs = mysql_fetch_array($rs_list);

//echo "<pre>$rs[name] <br><br> ".trim($rs['content'])." </pre>";

if($r['fname1']!=''){
	$content=str_replace("%FirstName1 LastName1 FirstName2 LastName2%","<b>".$leadfullname."</b>",$rs['content']);
}else{
   $content=str_replace("%FirstName1 LastName1 FirstName2 LastName2%","<b><a>%FirstName1 LastName1 FirstName2 LastName2%</a></b>",$rs['content']); 
}

$content1=str_replace("%Referrals Activity%","<b>".$ReferralActivityTable."</b>",$content);

echo "<pre>$content1</pre>";
?>


</div>
	
    </div>
	
   </div>

 </div> 
 </div>

<?php


include "lcas_footer.php";

?>
