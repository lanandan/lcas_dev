<?php
ob_start();  
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
validate_login();
$exp[]='';
$exp='';
$page_name = "profile.php";
$page_title = $site_name." -Dealer availability";
$message='';
$id='';
$wt='';
$etime='Select';
$stime='Select';
$st='';
$et='';
$daid='';
$date='';
$daid=request_get('lid');echo $id;
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
?>
<!--
<link href="stylesheets/fullcalendar.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/fullcalendar.js" type="text/javascript"></script>
<script src="javascripts/calendar_functions.js" type="text/javascript"></script>
-->
<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>
<link href="javascripts/fullcalendar/fullcalendar.css" media="screen" rel="stylesheet" type="text/css" />
<link href="javascripts/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css"  media='print' />
<script src="javascripts/fullcalendar/fullcalendar.js" type="text/javascript"></script>

<script src="js/addlead_calender.js" type="text/javascript"></script>
<div id="spinner"></div>
<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i> Add / Edit Dealer Availability Timing&nbsp;&nbsp;&nbsp;&nbsp;
<button class="btn btn-blue" id="copy"> Copy from previous Week</button>
</div> 

      </div>
    </div>
  </div>

<div class="container">
<div class="box">
 <div class="box-header">
        <div class="title">Add / Edit Dealer Availability Timing</div>
	<div class="title"style="margin-right:20px;float:right;">
	<b><span style='color:#4cb052'>Available Time</span></b>
      </div>
      </div>
<div class="box-header">

      <div class="box-content">

  <div class="box-content">
        <div id="dealer_availability"></div>
      </div>
</div>
</div>
</div>

</div> 



<?php

include "lcas_footer.php";
?>
