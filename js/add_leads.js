<script type="text/javascript" src="javascripts/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/jquery.timepicker.css" />
<style>
#spinner {
position:absolute;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.6;
background: url('images/preload.gif') 50% 50% no-repeat #ede9df;
}
</style>
<script type="text/javascript" src="javascripts/base.js"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/base1.css" />
<script src="javascripts/datepair.js"></script> 

<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>
<script src="javascripts/formatter.js"></script> 
<link href="javascripts/fullcalendar/events_style.css" media="screen" rel="stylesheet" type="text/css" />
<link href="stylesheets/multiple-select.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/jquery.multiple.select.js"></script> 
<script src="js/jquery.maskedinput.min.js"></script>
	<script>
jQuery(function($) {

$('#phone1').mask('(999) 999-9999');
$('#phone2').mask('(999) 999-9999');
$('#phone3').mask('(999) 999-9999');

var cache = {};
$("#city").autocomplete({
	
	minLength: 2,
	source: function( request, response ) {
	var term = request.term;
	if ( term in cache ) {
	response( cache[ term ] );
	return;
	}
	$.getJSON( "get_city.php?type=get_city", request, function( data, status, xhr ) {
		cache[ term ] = data;
		response( data );
	});
	}

});


});
</script> 
<script>
/*
function showsubtype(t) {
    var val = $('#ltype option:selected').attr("class") + " showsubtype";
    $('#lsubtype .showsubtype,#gift .hide-gift').css({
        display: 'none'
    });
    $('#lsubtype .showsubtype,#gift .hide-gift').removeAttr("selected");
    $('#lsubtype option[class="' + val + '"]').css({
        display: 'block'
    });

 	var val = $('#ltype').val()+$('#lsubtype').val()+"hide-gift";
    	var val1 = $('#ltype').val()+"hide-gift";
    $('#gift :not(option[class="Select"])').hide();

$('#gift').find('option').each(function(){
var classname=$(this).attr('class');
if(classname==val || classname==val1){
$(this).show();
}
});

}
*/
function showsubtype(t) {
var val=t.value;

$("#lsubtype").find("option :not(first)").remove();
$("#lsubtype").html(asdf);
//$("#lsubtype").find("option").hide();
$("#lsubtype").find("option[class!='"+val+" showsubtype'][class!='first_option']").remove();

$("#gift").find("option").remove();
$("#gift").html(asdf2);
//$("#lsubtype").find("option").hide();
$("#gift").find("option[class!='"+val+"hide-gift'][class!='first_option']").remove();
$("#gift").find("option").show();

}
/* 
function show_gift() {
    var val = $('#ltype').val()+$('#lsubtype').val()+"hide-gift";
    var val1 = $('#ltype').val()+"hide-gift";
$('#gift :not(option[class="Select"])').hide();
$('#gift').find('option').each(function(){
var classname=$(this).attr('class');
if(classname==val || classname==val1 ){
$(this).show();
}
});

}*/
function show_gift(t) {
var val=$('#ltype').val()+t.value;
var val2=$('#ltype').val();

$("#gift").find("option").remove();
$("#gift").html(asdf2);
//$("#lsubtype").find("option").hide();
$("#gift").find("option[class!='"+val+"hide-gift'][class!='"+val2+"hide-gift'][class!='first_option']").remove();
$("#gift").find("option").show();

}
</script>
<script  type="text/javascript" >
var typeval="";
$(document).ready(function(){
var a=$('#apptstatus').val();
if(a=='Demo'){
$("#icheck1").prop('checked',true);
}
$('#apptstatus').change(function(){
var a=$('#apptstatus').val();

 $("#apptresult option").prop('selected', false);
if(a!='Demo'){
$("#icheck1").prop('checked',false);
}
if(a=='Cancelled'){
 $("#apptresult option[value='Cancelled Appt']").prop('selected', true);
}
if(a=='Call To Reset'){
 $("#apptresult option[value='Reset']").prop('selected', true);
}
if(a=='Rescheduled'){
 $("#apptresult option[value='Pending']").prop('selected', true);
}
if(a=='Confirmed'){
 $("#apptresult option[value='Pending']").prop('selected', true);
}
if(a=='Needs Confirmed'){
 $("#apptresult option[value='Pending']").prop('selected', true);
}
if(a=='OK to Go'){
 $("#apptresult option[value='Pending']").prop('selected', true);
}
if(a=='Demo'){
$("#icheck1").prop('checked',true);
}


});
$('#setdate').datetimepicker({
		format:'M d Y h:i A',
		lang:'en',
		timepicker:true,
		validateOnBlur:false,	
		formatTime:'h:i A',
		allowTimes:[
'09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM','02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM','07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM','11:30 PM', '12:00 AM'
			 ],
		});
$('#date,#date1').datetimepicker({
		format:'M d Y',
		lang:'en',
		timepicker:false,
		validateOnBlur:false,	
		});

$('#setby').change(function() {
var set_val=$('#setby').val();
$('#setby1').val(set_val);
});
    
  $('#zip').blur(function() {
        $.ajax({
            type: "POST",
            url: "zipret.php",
            data: {
                zip: $("#zip").val()
            },
            success: function(result) {
		
		var obj = jQuery.parseJSON (result);
		if (obj.status == "TRUE") {
			$('#city').val(obj.city);
			$('#state1').val(obj.state);
			$('.first').append(obj.city);
			$('.first').append(' ');
			$('.first').append(obj.state);
		}
		if (obj.status == "FALSE") {
			$('#city').val("");
			$('#state1').val("PA");
		}
            }
        });
    });


  $('#city').change(function() {

	var cityid=$("#city").val();
	//alert(cityid);

	if(cityid!=""){

		$.ajax({
		    type: "POST",
		    url: "get_city.php?type=set_city&city="+cityid,
		    success: function(result) {
		
			var obj = jQuery.parseJSON (result);
			if (obj.status == "TRUE") {
				$('#zip').val(obj.zip);
				$('#city').val(obj.city);
				$('#state1').val(obj.state);
				/*$('.first').append(obj.zip);
				$('.first').append(' ');
				$('.first').append(obj.state);*/
			}
			if (obj.status == "FALSE") {
				$('#city').val("");
				$('#state1').val("PA");
			}
		    }
		});

	}
    });
/*
$('#ltype').change(function() {
        $.ajax({
            type: "POST",
	    dataType: "json",
            url: "subtyperet.php",
            data: {
		
                ltype: $("#ltype").val()
            },
            success: function(result) {
		$('#lsubtype').empty();
		$(result).each(function () {
		    // Create option
		    var $option = $('<option>');
		    // Add value and text to option

		    $option.attr("value", this.sublvalue).text(this.subltext);
		    // Add option to drop down list
		    $(lsubtype).append($option);
		});  
		
            }
        });
    });
*//*
$('#lsubtype').change(function() {
 
	  $.ajax({
            type: "POST",
	    dataType: "json",
            url: "giftret.php",
            data: {
		
                ltype: $("#ltype").val(),
		lsubtype: $("#lsubtype").val()
		
            },
            success: function(result) {
		
		$('#gift').empty();
		$(result).each(function () {
		    // Create option
		    var $option = $("<option >");
		    // Add value and text to option
		    $option.attr("value", this.value).text(this.text);
		    // Add option to drop down list
		    $(gift).append($option);

		});            
            }
        });
    });
*/
 $('.firstli').change(function() {
 //alert("echeck");
	var $txtval1 = $('#prefname1').val();
	$('.first').empty();
	$('.first').append(' ');

	if($txtval1 != 'Select')
		$('.first').append($txtval1);
	
	$('.first').append(' ');

	var $txtval2 = $('#fname1').val();
	$('.first').append($txtval2);

	var $txtval3 = $('#lname1').val();

	if(($('.first').html() !='') ){	
		$('.first').append(' ');
	}

	$('.first').append($txtval3);

	if(($('.first').html() !='') ){
		if($('#fname2').val() != '' )
			$('.first').append(' - ');
	}

	/*if(($('.first').html() !='') ){
		$('.first').append(' - ');
	}*/

	var $txtval7 = $('#addr1').val();

	$('.first').append($txtval7);
	$('.first').append(' ');

	var $txtval8 = $('#addr2').val();
	$('.first').append($txtval8);
	
	var $txtval9 = $('#city').val();
	
	if(($('.first').html() !='') ){	
		$('.first').append(' ');
	}

	$('.first').append($txtval9);

	if(($('.first').html() !='') ){	
		$('.first').append(' ');
	}

	var $txtval11 = $('#state1').val();
	$('.first').append($txtval11);
	
	if(($('.first').html() !='') ){	
		$('.first').append(' ');
	}

	var $txtval10 = $('#zip').val();
	$('.first').append($txtval10);

	if(($('.first').html() !='') ){	
		$('.first').append(' - ');
	}

	var $txtval12 = $('#phone1').val();
	
	$('.first').append($txtval12);
	$('.first').append(' ');
	$('.first').append('</a>');

	if($('.first').html() !=''){
		$('.first').css('display','block');
	}	

	var txt=$('.first').html();

	//alert(txt);
    });


});
</script>
<script>
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



var action=getParameterByName('action');

$(function(){

if(action==''){
$('#fname1,#fname2,#lname2,#lname2,#phone1').change(function(e){
var fname1= $('#fname1').val();
var lname1= $('#lname1').val();
var fname2= $('#fname2').val();
var lname2= $('#lname2').val();
var phone1= $('#phone1').val();
 $.ajax({
            type: "POST",
            url: "verify.php",
            data: {
		type:'check',
                fname1: fname1,
		lname1: lname1,
		fname2: fname2,
		lname2: lname2,
		phone1: phone1,
		
            },
            success: function(result) {
		if(result==1){
		$('#exist').val('1');
var where_to=  confirm("This Lead is already exists. Do you want to add this Lead again?")
 if (where_to== true){
     return true;
}
 else{
window.location.href="add_leads.php";
return false;

 }
		}
		else{
		$('#exist').val('');
			}
		}
        	});


});
}


var appt=$('#apptstatus').val();var res=$('#apptresult').val(); var dealer=$('#deal').val();
var appt=$('#apptstatus').val();
$("#savecustomer,#savelead").click(function(){
var cus=$('#customer').val();
var appt1=$('#apptstatus').val();var res1=$('#apptresult').val(); var dealer1=$('#deal').val();
if(cus==1 && appt1 !='Demo'){
var con=confirm("Please confirm that you want to change the Appt Status and remove them as a Customer?");
if(con ==true){
if(appt!=appt1 || res1!=res || dealer!=dealer1){
$('#change').val('1');
}
return true;
}
else{
return false;
}

}
else{
if(appt!=appt1 || res1!=res || dealer!=dealer1){
$('#change').val('1');
}
}
}); 
$('#icheck1').change(function(){
var check=$('#icheck1').val();
if(check==1){
$('#apptstatus option[value="Demo"]').prop('selected', true);
$('#apptresult option').prop('selected', false);
$('#change').val('1');
}
});
});
function myFunction()
{
	if(document.getElementById("chkemail1").checked==true && document.getElementById("chkemail2").checked==true ){
		document.getElementById("eoptin").value=3;
	}
	else if(document.getElementById("chkemail1").checked==true){
		document.getElementById("eoptin").value=1;
	}
	else if(document.getElementById("chkemail2").checked==true){

		document.getElementById("eoptin").value=2;
	}
	else{
		document.getElementById("eoptin").value='';
	}

}

function threehr()
{
	var curr_time = document.getElementById("appttimefrom").value;
	var to_time = parseInt(curr_time) + parseInt("10800");
	if ( parseInt(to_time) > parseInt("84600") ) {
		 to_time = "84600";
	}
	document.getElementById("appttimeto").value = to_time;
	
}

function exist_lead(){

 if(document.getElementById("icheck1").checked)
	 {      
var where_to= confirm("Are You Sure to Convert This Lead to Customer?");
 if (where_to== true){
     return true;
}
 else{
     
     return false;
 }
 
}		
}

function validateEmail(email, err) {
  var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
 if(document.getElementById(email).value != '')
 {
	  if( emailReg.test(document.getElementById(email).value) == false ) {
		document.getElementById(err).style.display="block";
	    return false;
	  } else {
			document.getElementById(err).style.display="none";
	    return true;
	  } 
  }
}
function setapptres() {
 if(document.getElementById("apptdate").value != '')
 {
	document.getElementById("apptresult").value ='SET'
  }
}

function ctoccheck() {
	 if(document.getElementById("icheck1").checked)
	 {      
var where_to= confirm("Are You Sure to Convert This Lead to Customer?");
 if (where_to== true){
     return true;
}
 else{
     
     return false;
 }
 
}		

}

</script>
<script type="text/javascript">
<!--//
function sizeFrame() {
var F = document.getElementById("myFrame");
//if(F.contentDocument) {
//F.height = F.contentDocument.documentElement.scrollHeight+30; //FF 3.0.11, Opera 9.63, and Chrome
//} else {
//F.height = F.contentWindow.document.body.scrollHeight+30; //IE6, IE7 and Chrome
//}

}

window.onload=sizeFrame;

//-->
</script>
<style>
#level1{
font-weight:bold;
}
#level2{
padding-left:10px;
font-weight:bold;
color:gray;
}
#level3{
padding-left:20px;
font-weight:lighter;
}
#below{
max-width:300px;
}
</style>
