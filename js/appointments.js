<style>
.tabRS{

line-height: 35px;

}

table.dataTable thead th{
height:70px !important;
}
input[type="text"] {
    width: 100px;
}
#spinner {
position: fixed;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.5;
background: url('images/preload.gif') 50% 50% no-repeat #ede9df;
}
#level1{
font-weight:bold;
}
#level2{
padding-left:10px;
font-weight:bold;
color:gray;
}
#level3{
padding-left:20px;
font-weight:lighter;
}
#below{
max-width:300px;
}
</style>
<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>
<script type="text/javascript">
function printpage() {
    var  data = '<button onclick="window.print()"  class="noprint">Print the Report</button><br/>';
data += '<table><tr><th>Appointment Listing</th></tr></table><table class="remove" border="1" cellspacing="0" frame="box" rules="all" >' + document.getElementsByTagName('table')[0].innerHTML + '</table>';
   
    data += '<style type="text/css" media="print"> .noprint {visibility: hidden;} </style>';
    myWindow = window.open('', '', 'width=800,scrollbars=yes');
    myWindow.innerWidth = screen.width;
    myWindow.innerHeight = screen.height;
    myWindow.screenX = 0;
    myWindow.screenY = 0;
    myWindow.document.write(data);
    myWindow.focus();
}
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete this appointment? ");
	if (agree)
		return true ;
	else
		return false ;
}


function loadrecurit(leadid)
{


		var data = {
    			type: 'suggession',
			leadid:leadid
		}
    		$.ajax({

	    		type: "POST",
	    		url: "suggession.php",
	    		data: data,
	    		success: function(output) {
				var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:350,
							openjs:function(){

						$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	

							$('a#save-event-edit').click(function(){


							
							var leadid=$('#ld').val();
					if($('.take').is(':checked')) {
					var selectedRadio = $("input[name='take']:checked").val();
	
				 }
				else{
					$('.msg').css({'display':'block'});
					$('.msg').html('Pls choose Any One Option');
					return false;
				}	




					var data = {
								    	type: 'redirect',
									leadid:leadid,
									take:selectedRadio
								 }
								 
				 $.ajax({
					type: "POST",
					url: "suggession.php",
					data: data,
					success: function(resp) {
		        		window.location.href = resp;
					
								}

									});
								});
						}    
					});
				}
		}); 

}

$(document).ready(function() {

$('#deleteapt').click(function(){ 

 	var aptids = [];

	$(nNodes).find('input:checked').each(function(i, checkbox){

		var aptid=$(checkbox).attr('aptid');
		aptids.push(aptid);	 

	});

	if(aptids==''){
		alert("Please Select any Appointment ");
		return false;
	}else{
		
		bootbox.confirm("Are you sure do you want to delete?", function(result) {
			if(result==true)
			{
				var data = {
				    	type: 'deleteAll',
					aptid:aptids
				}

				$.ajax({
			    		type: "POST",
			    		url: "multi_edit_apptdetails.php",
			    		data: data,
			    		success: function(resp) {
						bootbox.alert(resp,function(){
							
							window.location.href = 'appointments.php';
						});
					}
				});

			}//end true
		}); //end confirm box

	} // end else if

});
$('#bulkDelete').click(function(){ 

	var values = [];
 	var aptids = [];

	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
		var aptid=$(checkbox).attr('aptid');
		aptids.push(aptid);	 
	});

	//alert("Lead Ids : "+values+" ; Appt Ids : "+aptids);

	if(values==''){
		bootbox.alert("Please Select any Appointment ");
		return false;
	}
else{

	var result=confirm("Are you sure You want to delete this appoinment?");

		if(result==true)
		{
			var data={
					type:"BulkDelete",
					aptid:aptids,
					page:"appointments"
				}
			$.ajax({
				type:"POST",
				url:"bulkdelete.php",
				data:data,
				success:function(res){
					window.location.href="appointments.php";
				}
			});
		}
	
		
	} //else

});
$('#bulk').click(function(){ 

	var values = [];
 	var aptids = [];

	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
		var aptid=$(checkbox).attr('aptid');
		aptids.push(aptid);	 
	});

	//alert("Lead Ids : "+values+" ; Appt Ids : "+aptids);

	if(values==''){
		alert("Please Select any Appointment ");
		return false;
	}

	var data = {
    			type: 'editlead',
			leadid:values,
		}
    	$.ajax({
    		type: "POST",
    		url:  "multi_edit_apptdetails.php",
    		data: data,
    		success: function(output) {
			var obj =JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:600,
				height:400,
				openjs:function(){
				
					$('a#cancel-event-edit').click(function(){
						window.parent.TINY.box.hide();
					});	

					$("#bsuleadin").datepicker({ autoclose:true });	

					$("#bsuleadtype").change(function() {
						$.get('loadsubtype.php?leadtype=' + $(this).val(), function(data) {
							$("#bsuleadsubtype").html(data);
						});	
					});								

					$('#bsustatus').change(function(){

						var a=$('#bsustatus').val();

						$("#bsuresult option").prop('selected', false);
						if(a=='Demo'){
						 $("#bsuresult option[value='Sale']").prop('selected', true);
						}
						if(a=='Other'){
						 $("#bsuresult option[value='Pending']").prop('selected', true);
						}
						if(a=='Cancelled'){
						 $("#bsuresult option[value='Cancelled Appt']").prop('selected', true);
						}
						if(a=='Call To Reset'){
						 $("#bsuresult option[value='Reset']").prop('selected', true);
						}
						if(a=='Rescheduled'){
						 $("#bsuresult option[value='Pending']").prop('selected', true);
						}
						if(a=='Confirmed'){
						 $("#bsuresult option[value='Pending']").prop('selected', true);
						}
						if(a=='Needs Confirmed'){
						 $("#bsuresult option[value='Pending']").prop('selected', true);
						}
						if(a=='OK to Go'){
						 $("#bsuresult option[value='Pending']").prop('selected', true);
						}

					});


					$('a#save-event-edit').click(function(){

							var leadtype = $('#bsuleadtype').val();
							var leadsubtype = $('#bsuleadsubtype').val();
							var leaddealer = $('#bsuleaddealer').val();
							var status = $('#bsustatus').val();
							var result = $('#bsuresult').val();
							var quali = $('#bsuquali').val();
							var leadindate = $('#bsuleadin').val();

				if(leadtype=='' && leadsubtype=='' && leaddealer=='' && status=='' && result=='' && quali==''  && leadindate==''){
								$('.msg').css({'display':'block'});
								$('.msg').html('Pls Choose atleast one options');
								return false ;
							}

							var data = {
							    	type: 'editSaveEvent',
								leadid:values,
								aptid:aptids,
								leadtype: leadtype,
								leadsubtype:leadsubtype,
								leaddealer:leaddealer,
								status:status,
								result:result
							}

							$.ajax({
							    		type: "POST",
							    		url: "multi_edit_apptdetails.php",
							    		data: data,
							    		success: function(resp) {

										bootbox.alert(resp,function(){
											window.parent.TINY.box.hide();
											window.location.href = 'appointments.php';
										});
										
									}

							});
					});	
								
				} // end of open js				    	

			});

	    } // end of sucess function
            

        });
}); // end of bluk click





$('#appt').dataTable(
    {
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
"sPaginationType": "full_numbers",
 "bDestroy": true,
"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [0] }
       ],
 "oTableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        },
 "stateSave": true,
 "bStateSave": true
    }

);
jQuery.fn.dataTableExt.oApi.fnGetHiddenNodes = function ( settings )
{
var nodes;
var display = jQuery('tbody tr', settings.nTable);

if ( jQuery.fn.dataTable.versionCheck ) {
// DataTables 1.10
var api = new jQuery.fn.dataTable.Api( settings );
nodes = api.rows().nodes().toArray();
}
else {
// 1.9-
nodes = this.oApi._fnGetTrNodes( settings );
}

/* Remove nodes which are being displayed */
for ( var i=0 ; i<display.length ; i++ ) {
var iIndex = jQuery.inArray( display[i], nodes );

if ( iIndex != -1 ) {
nodes.splice( iIndex, 1 );
}
}

return nodes;
};
var oTable = $('#appt').dataTable();
var nNodes = oTable.fnGetNodes( );
 $("#check").click ( function (){
var i=$('#check').is(":checked");
var hidden = oTable.fnGetHiddenNodes();
$(nNodes).each(function(){
if(i==true)
 $(this).find('input[type="checkbox"]').prop('checked',true);
else
$(this).find('input[type="checkbox"]').prop('checked',false);
});
$(hidden).each(function(){
 $(this).find('input[type="checkbox"]').prop('checked',false);
});
});
$(nNodes).hover(
  function () {
    $(this).css("background","#3E93BD");
    $(this).css("color","#FFFFFF");
    $('.link',this).css("color","#FFFFFF");
  }, 
  function () {
    $(this).css("background","");
    $(this).css("color","#756A71");
    $('.link',this).css("color","#756A71");
  }
);

var oTable = $('#appt').dataTable();
var nNodes = oTable.fnGetNodes( );
$('#mapit').click(function(){ 
	var values = [];
	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
	});
	
	if(values=="")
	{
/*bootbox.confirm("Loading all the Lead Addresses in your Listings..! <br> Might take some time to Load Please Wait...",function(result){
			if(result==true){
				//window.location.href="gmap_listing.php?page=leadcard&leadid=0";
				window.open('gmap_listing.php?page=leadcard&leadid=0','_blank');
			}
		});*/
		alert("Please select any Appointment to view Map");
	}else{
		//window.location.href="gmap_listing.php?page=leadcard&leadid="+values;
		
		window.open('gmap_listing.php?page=leadcard&leadid='+values,'_blank');
	}

});

$('#emailit').click(function(){ 
	var values = [];
	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
	});
	
	if(values=="")
	{
		alert("Please select any Appointment to send E-Mail");
	}else{
		window.open('messages/composemail.php?type=leadmail&tbl=3&leadid='+values,'_blank');
	}

});


$('#set_date').datetimepicker({
		format:'M d Y',
		lang:'en',
		timepicker:false,
		validateOnBlur:false,	
		});



 $('.from').datetimepicker({
  format:'M d Y',
 validateOnBlur:false,
  onShow:function( ct ){
   this.setOptions({
    maxDate:$('.to').val()?$('.to').val():false,
formatDate:'M d Y'
   })
  },
  timepicker:false
 });



$('.to').datetimepicker({
format:'M d Y',
 validateOnBlur:false,
  onShow:function( ct ){
   this.setOptions({
    minDate:$('.from').val()?$('.from').val():false,
formatDate:'M d Y'
   })
  },
  timepicker:false
 });


$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});
 
			
	$('#dealer').change(function() { 
	
		var dealer=$('#dealer').val();

		var data = {
    			type: 'filterbydealer',
			dealer:dealer
		}
    		$.ajax({
	    		url: "apptfilters.php",
	    		data: data,
	    		success: function(output) {
				$('#dispContent').html(output);
			}
		});

	});
	

});

function saveChanges(object){   
    $.ajax({
        url: 'editlead.php',
        data: 'content=' + object.value,
        cache: false,
        
        success: function(response){

}
    });   
}


  function loadAjaxContent(x,y)
    { 
$("#spinner").show();
var data = {
    			type: 'editlead',
			id:x,
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url: "editlead.php",
	    		data: data,
	    		success: function(output) {
$("#spinner").hide();
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:900,
							height:630,
							openjs:function(){

							$('#resch').addClass('tabRS');

							$('#startdate').datetimepicker({
								format:'M d Y',
								lang:'en',
								timepicker:false,
							});

							$('#starttime').datetimepicker({
								format:'h:i A',
								lang:'en',
								formatTime:'h:i A',
		allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ],
								datepicker:false
							});
							
							$('#datepicker').datepicker({
								autoclose:true								
								});
                                                           
             						$('#datepicker1').datepicker({
								autoclose:true								
								});
							

var ss=$('#status').val();
var rr=$('#result').val();
var cc=$("#cmts").val();
var dd=$("#dealer").val();
var rreason=$("#rresetreason").val();
var creason=$("#cresetreason").val();
var sdate=$('#startdate').val();
var stime=$('#starttime').val();
var a=$('#status').val();
$("#cmt_tr").show();
$("#ntcnote").hide();
$("#resch").hide();
$("#ctrnote").hide();

//$("#apptresult option").removeAttr("selected","selected");

if(a=='Cancelled'){
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").hide();
}
if(a=='Call To Reset'){
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").show();
}
if(a=='Rescheduled'){
 $("#ntcnote").hide();
 $("#resch").show();
 $("#ctrnote").hide();
}
if(a=='Confirmed'){
 $("#ntcnote").hide();
 $("#ctrnote").hide();
}
if(a=='Needs Confirmed'){
 $("#ntcnote").show();
 $("#resch").hide();
 $("#ctrnote").hide();
}
if(a=='OK to Go'){
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").hide();
}

if(a==''){
	$("#cmt_tr").hide();
}


$('#create_task').click(function(){
var s=$('#status').val();
if(s=='Call To Reset'){

window.location.href ='createtask.php?prefill=true';
}
else
{
window.location.href ='createtask.php';
}
});

$('#status').change(function(){
$('#startdate').val('');
$('#starttime').val('');
$("#cmts").val('');
var a=$('#status').val();

$("#cmt_tr").show();
$("#ntcnote").hide();
$("#resch").hide();
$("#ctrnote").hide();

//$("#apptresult option").removeAttr("selected","selected");
 $("#result option").prop('selected', false);

if(a=='Cancelled'){
 $("#result option[value='Cancelled Appt']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").hide();
}
if(a=='Call To Reset'){
$("#cresetreason option").prop('selected', false);
 $("#result option[value='Reset']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").show();
}
if(a=='Rescheduled'){
$("#rresetreason option").prop('selected', false);
 $("#result option[value='Reset']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").show();
 $("#ctrnote").hide();
}
if(a=='Confirmed'){
 $("#result option[value='Pending']").prop('selected', true);
 $("#ntcnote").hide();
 $("#ctrnote").hide();
}
if(a=='Needs Confirmed'){
 $("#result option[value='Pending']").prop('selected', true);
 $("#ntcnote").show();
 $("#resch").hide();
 $("#ctrnote").hide();
}
if(a=='OK to Go'){
 $("#result option[value='Pending']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").hide();
}

if(a==''){
	$("#cmt_tr").hide();
}


});

$('.delete').click(function(){
var agree=confirm("Are you sure you want to delete this status update?");
	if (agree)
		return true ;
	else
		return false ;

});

$('a#recruite').click(function(){

//window.parent.TINY.box.hide();
var data = {
    			type: 'suggession',
			leadid:y
		}
		$("#spinner").show();	
    		$.ajax({

	    		type: "POST",
	    		url: "suggession.php",
	    		data: data,
	    		success: function(output) {
		$("#spinner").hide();	
				var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:290,
							openjs:function(){
						var fname=$.trim($('#lead2').text());
						if(fname==''){
						$('.fname2').hide();
						}
						var length = $('#below').find("option").length;
						if(length==''){
						$('#team').hide();
						$('#team_name').show();
						}
						$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	

							$('a#save-event-edit').click(function(){


							
							var leadid=$('#ld').val();
			if($('.take').is(':checked')) {
					var selectedRadio = $("input[name='take']:checked").val();
	
				 }
				else{
					$('.msg').css({'display':'block'});
					$('.msg').html('Pls choose Any One Option');
					return false;
				}	
				var below=$.trim($('#below').val());
				if(below==''){
				below=$('#parentid').val();
				}


				$("#spinner").show();
					var data = {
								    	type: 'redirect',
									leadid:leadid,
									take:selectedRadio,
									below:below
								 }
					 		$("#spinner").show();	
				 $.ajax({
					type: "POST",
					url: "suggession.php",
					data: data,
					success: function(resp) {	
		        		window.location.href = resp;
					//window.parent.TINY.box.hide();
								}

									});
								});
						}    
					});
				}
		}); 

});	
		$('a#cancel-event-edit').click(function(){
			window.parent.TINY.box.hide();
		});	

		$('a#save-event-edit').click(function(){

var ss_new=$('#status').val();
var rr_new=$('#result').val();
var cc_new=$("#cmts").val();
var dd_new=$("#dealer").val();
if(ss=="Rescheduled"){
var startdate=$('#startdate').val();
var starttime=$('#starttime').val();
var resetreason=$("#rresetreason").val();
if(ss==ss_new && rr==rr_new && cc==cc_new && dd==dd_new && rreason==resetreason && sdate==startdate && stime==starttime){
window.parent.TINY.box.hide();
return false;
}
}
else if(ss=="Call To Reset"){
var resetreason=$("#cresetreason").val();
if(ss==ss_new && rr==rr_new && cc==cc_new && dd==dd_new && creason==resetreason ){
window.parent.TINY.box.hide();
return false;
}
}
else{
if(ss==ss_new && rr==rr_new && cc==cc_new && dd==dd_new ){
window.parent.TINY.box.hide();
return false;
}
}
					var appt_status = $('#status').val()
					var cus=$('#customer').val();
					var appt_result = $('#result').val();

			if(appt_status=="Rescheduled"){
					var startdate=$('#startdate').val();
					var starttime=$('#starttime').val();
						if(startdate=="" || starttime=="")
						{
						bootbox.alert('Please Select Both Rescheduled Appointment Date and Time!');
						return false;
						}
					}
if(cus==1 && appt_status!='Demo'){
bootbox.confirm("Please confirm that you want to change the Appt Status and remove them as a Customer?", function(confirmed) {
if(confirmed){

					if(appt_status=='')
					{
					$('.msg').css({'display':'block'});
					$('.msg').html('Pls choose Appointment Status');
					return false ;
					}
				
					var dealer=$('#dealer').val();
					var aptdatetime=$('#apptdatetime').val();
					var comments=$("#cmts").val();
					var startdate=$("#startdate").val();							
					var starttime=$("#starttime").val();
					var resetreason="";

					if(appt_status=="Rescheduled")
					{
						resetreason=$("#rresetreason").val();
					}else{
						resetreason=$("#cresetreason").val();
					}

					if(appt_status=="Rescheduled"){

						if(startdate!="" && starttime!="")
						{
							var data = {
						    	type: 'custolead',
							id: x,
							leadid:y,
							status: appt_status,
							result: appt_result,
							dealer:dealer,
							aptdatetime:aptdatetime,
							comments:comments,
							startdate:startdate,
							starttime:starttime,
							resetreason:resetreason
						 }
						 
						 $.ajax({
						    		type: "POST",
						    		url: "editlead.php",
						    		data: data,
						    		success: function(resp) {
                                                          window.location.href = 'appointments.php';
									window.parent.TINY.box.hide();
								}

							});
						}else{
							alert('Please Select Rescheduled Appointment Date and Time!');
						}
					}else{
						var data = {
						    	type: 'custolead',
							id: x,
							leadid:y,
							status: appt_status,
							result: appt_result,
							dealer:dealer,
							aptdatetime:aptdatetime,
							comments:comments,
							startdate:startdate,
							starttime:starttime,
							resetreason:resetreason
						 }
						 
						 $.ajax({
						    		type: "POST",
						    		url: "editlead.php",
						    		data: data,
						    		success: function(resp) {
                                                     window.location.href = 'appointments.php';
									window.parent.TINY.box.hide();
								}

							});


						}
///if he is already customer


}

}); 
}

else{

					var appt_status = $('#status').val()
					if(appt_status=='')
					{
					$('.msg').css({'display':'block'});
					$('.msg').html('Pls choose Appointment Status');
					return false ;
					}
					var appt_result = $('#result').val();
					var dealer=$('#dealer').val();
					var aptdatetime=$('#apptdatetime').val();
					var comments=$("#cmts").val();
					var startdate=$("#startdate").val();							
					var starttime=$("#starttime").val();
					var resetreason="";

					if(appt_status=="Rescheduled")
					{
						resetreason=$("#rresetreason").val();
					}else{
						resetreason=$("#cresetreason").val();
					}

					if(appt_status=="Rescheduled"){

						if(startdate!="" && starttime!="")
						{
							var data = {
						    	type: 'editSaveEvent',
							id: x,
							leadid:y,
							status: appt_status,
							result: appt_result,
							dealer:dealer,
							aptdatetime:aptdatetime,
							comments:comments,
							startdate:startdate,
							starttime:starttime,
							resetreason:resetreason
						 }
						 
						 $.ajax({
						    		type: "POST",
						    		url: "editlead.php",
						    		data: data,
						    		success: function(resp) {
                                                          window.location.href = 'appointments.php';
									window.parent.TINY.box.hide();
								}

							});
						}else{
							alert('Please Select Rescheduled Appointment Date and Time!');
						}
					}else{
						var data = {
						    	type: 'editSaveEvent',
							id: x,
							leadid:y,
							status: appt_status,
							result: appt_result,
							dealer:dealer,
							aptdatetime:aptdatetime,
							comments:comments,
							startdate:startdate,
							starttime:starttime,
							resetreason:resetreason
						 }
						 
						 $.ajax({
						    		type: "POST",
						    		url: "editlead.php",
						    		data: data,
						    		success: function(resp) {
                                                         window.location.href = 'appointments.php';
									window.parent.TINY.box.hide();
								}

							});

						}
					 }
				});									
			}				    	

		});
	    }
            

        });

    }


</script>
<link href="stylesheets/dtable.css" media="screen" rel="stylesheet" type="text/css" />
