<script type="text/javascript" src="javascripts/bootbox.min.js"></script>
<link href="stylesheets/dtable.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/jquery.base64.js"></script>
<script src="javascripts/jquery.btechco.excelexport.js"></script>
<style>
.bootbox-body{
padding-left:100px;
font-size:14px;
}
.modal-title{
font-weight: 300;
}
</style>
<script type="text/javascript">

function viewaddr_gmaps(address,leadid)
{
	var data={
		type:'vwleadaddr',
		addr:address,
		leadid:leadid
	}
	$.ajax({
		type: "POST",
		url: "view_gmap_addr.php",
		data: data,
		success: function(output)
		{
			var obj=JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:700,
				height:570,
				openjs:function(){

				}
				
			});
		
		}

	});
}

  function loadAjaxContent(x,y)
    { 

var data = {
    			type: 'editlead',
			id:x,
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "edit_leadstatus.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:550,
							openjs:function(){
							

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	



								

							$('a#save-event-edit').click(function(){

							var lead_status = $('#status').val();
							
							var data = {
							    	type: 'editSaveEvent',
								id: x,
								leadid:y,
								status: lead_status,
								
							
							 }


								$.ajax({
								    		type: "POST",
								    		url: "edit_leadstatus.php",
								    		data: data,
								    		success: function(resp) {
                                                                                window.location.href = 'leadcard.php';
									        window.parent.TINY.box.hide();
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
    }

function confirmDelete(id) 
{
$(document).ready(function(){
bootbox.dialog({
  message: "<input type='radio' value='cus' name='opt'> Customer</input> <input type='radio' value='both' name='opt' >Both Lead and Customer</input>",
  title: "Please Confirm to delete this customer from",
  buttons: {
    danger: {
      label: "Cancel",
      callback: function() {
        //do something
      }
    },
    main: {
      label: "Ok",
      className: "btn-primary",
      callback: function() {
       var sel=$('input[name="opt"]:checked').val();
if(sel=='cus'){
window.location.href="add_leads.php?action=deletecustomer&lid="+id+"&redir=2";
      }
if(sel=='both'){
window.location.href="add_leads.php?action=delete&lid="+id+"&redir=2";
      }
else{
return false;
}
      }
    }
  }
});
});
}

var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var curdate = ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day + '' + d.getFullYear();


$(document).ready(function() {
$('#click').dataTable(
    {
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
"sPaginationType": "full_numbers",
 "bDestroy": true
    }

);

$('#click').dataTable(
    {
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
"sPaginationType": "full_numbers",
 "bDestroy": true,
"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [0] }
       ],
 "oTableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        },
    }

);


var oTable = $('#click').dataTable();
 var nNodes = oTable.fnGetNodes();

$("#check").click (function () 
{
    // Use the _ function instead to filter the rows to the visible
	var data = oTable._('tr', {"filter":"applied"});
	var i=$('#check').is(":checked");
	if(i==true){
	    $.each(data, function(key, index)
	    {
		var input = $(this).find('input[type="checkbox"]').prop('checked',true);
	    });
	}else{
	    $.each(data, function(key, index)
	    {
		var input = $(this).find('input[type="checkbox"]').prop('checked',false);
	    });
	}

});
	var fn="LeadLists_"+curdate;

	 $("#btnExport").click(function () {
            $("#click").btechco_excelexport({
                containerid: "click",
                datatype: $datatype.Table,
		filename: fn
            });
        });

var oTable = $('#click').dataTable();
var nNodes = oTable.fnGetNodes( );

$(nNodes).hover(
  function () {
    $(this).css("background","#3E93BD");
    $(this).css("color","#FFFFFF");
    $('.link',this).css("color","#FFFFFF");
  }, 
  function () {
    $(this).css("background","");
    $(this).css("color","#756A71");
    $('.link',this).css("color","#756A71");
  }
);


$('#bulkDelete').click(function(){ 

	var values = [];
	var cids = [];

	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);

		var cid=$(checkbox).attr('cid');
		cids.push(cid);	 
	});
	
	if(values=="")
	{
		alert("Please select any Customer");
	}else{

	var result=confirm("Deleting this customer will also delete lead information associated with it. Continue deleting this Customer?");

		if(result==true)
		{
			var data={
					type:"BulkDelete",
					leadids:values,
					cids:cids,
					page:"customer"
				}
			$.ajax({
				type:"POST",
				url:"bulkdelete.php",
				data:data,
				success:function(res){
					window.location.href="customer.php";
				}
			});
		}
	
		
	} //else

});


$('#emailit').click(function(){ 
	var values = [];
	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
	});
	
	if(values=="")
	{
		alert("Please select any Customer to send E-Mail");
	}else{
		window.open('messages/composemail.php?type=leadmail&tbl=3&leadid='+values,'_blank');
	}

});

$('#mapit').click(function(){ 
	var values = [];
	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
	});
	
	if(values=="")
	{
		/*bootbox.confirm("Loading all the Customer Addresses in your Listings..! <br> Might take some time to Load Please Wait...",function(result){
			if(result==true){
				//window.location.href="gmap_listing.php?page=leadcard&leadid=0";
				window.open('gmap_listing.php?page=customer&leadid=0','_blank');
			}
		});*/
		alert("Please select any Customers to view Map");
	}else{
		//window.location.href="gmap_listing.php?page=leadcard&leadid="+values;
		window.open('gmap_listing.php?page=customer&leadid='+values,'_blank');
	}

});


	var lid=getParameterByName('lid');
	var ls=getParameterByName('ls');
	var lseqid=getParameterByName('leadseqid');
	var nxtapt=getParameterByName('nxtapt');

	if(lid!=="" && ls!="" && nxtapt!="")
	{
	if(lid!=null || ls!=null || nxtapt!=null )
	{
		
		if(nxtapt!="no")
		{
		   var txt=" <br>Your Survey Saved with Lead Status "+ls+" Successfully! <br><br> The Call Date will be on <b>"+nxtapt+"</b>";
		   //bootbox.alert(txt);
			$.ajax({
	    		type: "POST",
	    		url: "nxtaptstatus.php?lid="+lid+"&leadseqid="+lseqid+"&ls="+ls+"&nxtapt="+nxtapt,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:350,
							openjs:function(){
								
							   $('#apptdate').datepicker({
									autoclose:true
							   });

							   var curr_time = document.getElementById("appttimefrom").value;
							   var to_time = parseInt(curr_time) + parseInt("10800");
							   document.getElementById("appttimeto").value = to_time;
	
							   $('#appttimefrom').change(function (){
								var curr_time = document.getElementById("appttimefrom").value;
								var to_time = parseInt(curr_time) + parseInt("10800");
								if ( parseInt(to_time) > parseInt("84600") ) {
									 to_time = "84600";
								}
								document.getElementById("appttimeto").value = to_time;
							    });

								$('#cancel').click(function(){
									window.parent.TINY.box.hide();
								});

							    $('#update').click(function(){

								var apptdate=$('#apptdate').val();
								var apptfrm=$('#appttimefrom').val();
								var apptto=$('#appttimeto').val();
								var leadseqid=$('#leadseqid').val();
								var leadid=$('#leadid').val();
								var aptstatus=$('#aptstatus').val();
								var contactreason=$('#contactreason').val();

								var data = {
    									type: 'updEvent',
									apptdate: apptdate,
									apptfrm:apptfrm,
									apptto:apptto,
									leadseqid:leadseqid,
									leadid:leadid,
									aptstatus:aptstatus,
									contactreason:contactreason
								}
								$.ajax({
	    								type: "POST",
	    								url: "nxtaptstatus.php",
	    								data: data,
	    								success: function(output) {
							
							var txt=" <br>Survey Scheduled as <b> Task on "+output+"</b> Successfully! <br>";
		  				        bootbox.alert(txt);
							 window.location.href = 'leadcard.php';
							window.parent.TINY.box.hide();
									}
								});

							    });
							}
					    });
				}
			});
		}else{
		   var txt=" <br>Your Survey Saved with Lead Status "+ls+" Successfully! <br>";
		   bootbox.alert(txt);
		}
	}
	}




});
$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
  
</script>
<script>
function loadrecurit(leadid)
{
//window.parent.TINY.box.hide();

		var data = {
    			type: 'suggession',
			leadid:leadid
		}
    		$.ajax({

	    		type: "POST",
	    		url: "cus_suggession.php",
	    		data: data,
	    		success: function(output) {
				var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:250,
							openjs:function(){

						$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	

							$('a#save-event-edit').click(function(){


							
							var leadid=$('#leadid').val();
					if($('.take').is(':checked')) {
					var selectedRadio = $("input[name='take']:checked").val();
	
				 }
				else{
					$('.msg').css({'display':'block'});
					$('.msg').html('Pls choose Any One Option');
					return false;
				}	




					var data = {
								    	type: 'redirect',
									leadid:leadid,
									take:selectedRadio
								 }
								 
				 $.ajax({
					type: "POST",
					url: "cus_suggession.php",
					data: data,
					success: function(resp) {
		        		window.location.href = resp;
					//window.parent.TINY.box.hide();
								}

									});
								});
						}    
					});
				}
		}); 

}

function printpage() {
    var data = '<table><tr><th>Customer Listing</th></tr></table><table border="1" cellspacing="0" frame="box" rules="all">' + document.getElementsByTagName('table')[0].innerHTML + '</table>';
    data += '<br/><button onclick="window.print()"  class="noprint">Print the Report</button>';
    data += '<style type="text/css" media="print"> .noprint {visibility: hidden;} </style>';
    myWindow = window.open('', '', 'width=800,scrollbars=yes');
    myWindow.innerWidth = screen.width;
    myWindow.innerHeight = screen.height;
    myWindow.screenX = 0;
    myWindow.screenY = 0;
    myWindow.document.write(data);
    myWindow.focus();
}
</script>
