<style>
#spinner {
position: absolute;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.5;
background: url('images/preload.gif') 50% 50% no-repeat #ffffff;
}
.box-header label{
width:120px;
margin-left:10px;
display: inline-block;
}
.box-header button{
margin-left:30px;
}
.box-header select{
width:110px;
}
#search_container{
margin-top:10px;
margin-bottom:10px;
}
</style>
<script type="text/javascript" src="javascripts/bootbox.min.js"></script>
<link href="stylesheets/dtable.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/jquery.base64.js"></script>
<style>
.bootbox-body{
/*padding-left:100px;*/
font-size:14px;
}
.modal-title{
font-weight: 300;
}
</style>

<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>

<script type="text/javascript">

function viewaddr_gmaps(address,leadid)
{
	var data={
		type:'vwleadaddr',
		addr:address,
		leadid:leadid
	}
	$.ajax({
		type: "POST",
		url: "view_gmap_addr.php",
		data: data,
		success: function(output)
		{
			var obj=JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:700,
				height:570,
				openjs:function(){

				}
				
			});
		
		}

	});
}

  function loadAjaxContent(x,y)
    { 

var data = {
    			type: 'editlead',
			id:x,
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "edit_leadstatus.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:800,
							height:550,
							openjs:function(){
							

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	



								

							$('a#save-event-edit').click(function(){
								
							var lead_status = $('#status').val();
							var activity = $('#activity').val();
							var contactreason = $('#contactreason').val();
							var comments = $('#comments').val();
					if(activity==''||contactreason==''||lead_status==''){
						$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose All The Options ');
							return false ;
						}
							var data = {
							    	type: 'editSaveEvent',
								id: x,
								leadid:y,
								status: lead_status,
								activity:activity,
								contactreason:contactreason,
								comments:comments,
						
							 }


								$.ajax({
								    		type: "POST",
								    		url: "edit_leadstatus.php",
								    		data: data,
								    		success: function(resp) {
										bootbox.alert(resp);
									        window.parent.TINY.box.hide();
										window.location.href="leadcard.php";
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
    }
function confirmDelete(id,cus) 
{

if(cus=='1'){
$(document).ready(function(){
bootbox.dialog({
  message: "<input type='radio' value='cus' name='opt'> Customer</input> <input type='radio' value='both' name='opt' >Both Lead and Customer</input>",
  title: "Please Confirm to delete this customer from",
  buttons: {
    danger: {
      label: "Cancel",
      callback: function() {
        //do something
      }
    },
    main: {
      label: "Ok",
      className: "btn-primary",
      callback: function() {
       var sel=$('input[name="opt"]:checked').val();
if(sel=='cus'){
window.location.href="add_leads.php?action=deletecustomer&lid="+id+"&redir=3";
      }
if(sel=='both'){
window.location.href="add_leads.php?action=delete&lid="+id+"&redir=3";
      }
else{
return false;
}
      }
    }
  }
});
});
}
else{
	var agree=confirm("Deleting this lead will also delete any appointments and information associated with it. Continue deleting this lead? ");
}
	if (agree)
		window.location.href="add_leads.php?action=deletelead&lid="+id+"&redir=3";
	else
		return false ;
}

var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var curdate = ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day + '' + d.getFullYear();


$(document).ready(function() {


$('#bonus_exp_date').datetimepicker({
		format:'M d Y',
		lang:'en',
		timepicker:false,
	});

$('#prog_exp_date').datetimepicker({
		format:'M d Y',
		lang:'en',
		timepicker:false,
	});

$('#example').dataTable({
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
"sPaginationType": "full_numbers",
 "bDestroy": true,
"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [0] }
       ],
 "oTableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        },
 "stateSave": true,
 "bStateSave": true
});


	var fn="LeadLists_"+curdate;

	 $("#btnExport").click(function () {
            $("#leeLooDallas").btechco_excelexport({
                containerid: "click",
                datatype: $datatype.Table,
		filename: fn
            });
        });


	var lid=getParameterByName('lid');
	var ls=getParameterByName('ls');
	var lseqid=getParameterByName('leadseqid');
	var nxtapt=getParameterByName('nxtapt');

	if(lid!=="" && ls!="" && nxtapt!="")
	{
	if(lid!=null || ls!=null || nxtapt!=null )
	{
		
		if(nxtapt!="no")
		{
		   var txt=" <br>Your Survey Saved with Lead Status "+ls+" Successfully! <br><br> The Call Date will be on <b>"+nxtapt+"</b>";
		   //bootbox.alert(txt);
			$.ajax({
	    		type: "POST",
	    		url: "nxtaptstatus.php?lid="+lid+"&leadseqid="+lseqid+"&ls="+ls+"&nxtapt="+nxtapt,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:350,
							openjs:function(){
								
							   $('#apptdate').datepicker({
									autoclose:true
							   });

							   var curr_time = document.getElementById("appttimefrom").value;
							   var to_time = parseInt(curr_time) + parseInt("10800");
							   document.getElementById("appttimeto").value = to_time;
	
							   $('#appttimefrom').change(function (){
								var curr_time = document.getElementById("appttimefrom").value;
								var to_time = parseInt(curr_time) + parseInt("10800");
								if ( parseInt(to_time) > parseInt("84600") ) {
									 to_time = "84600";
								}
								document.getElementById("appttimeto").value = to_time;
							    });

								$('#cancel').click(function(){
									window.parent.TINY.box.hide();
								});

							    $('#update').click(function(){

								var apptdate=$('#apptdate').val();
								var apptfrm=$('#appttimefrom').val();
								var apptto=$('#appttimeto').val();
								var leadseqid=$('#leadseqid').val();
								var leadid=$('#leadid').val();
								var aptstatus=$('#aptstatus').val();
								var contactreason=$('#contactreason').val();

								var data = {
    									type: 'updEvent',
									apptdate: apptdate,
									apptfrm:apptfrm,
									apptto:apptto,
									leadseqid:leadseqid,
									leadid:leadid,
									aptstatus:aptstatus,
									contactreason:contactreason
								}
								$.ajax({
	    								type: "POST",
	    								url: "nxtaptstatus.php",
	    								data: data,
	    								success: function(output) {
							
							var txt=" <br>Survey Scheduled as <b> Task on "+output+"</b> Successfully! <br>";
		  				        bootbox.alert(txt);
							 window.location.href = 'leadcard.php';
							window.parent.TINY.box.hide();
									}
								});

							    });
							}
					    });
				}
			});
		}else{
		   var txt=" <br>Your Survey Saved with Lead Status "+ls+" Successfully! <br>";
		   bootbox.alert(txt);
		}
	}
	}

var oTable = $('#example').dataTable();
 var nNodes = oTable.fnGetNodes();

$("#check").click (function () 
{
    // Use the _ function instead to filter the rows to the visible
	var data = oTable._('tr', {"filter":"applied"});
	var i=$('#check').is(":checked");
	if(i==true){
	    $.each(data, function(key, index)
	    {
		var input = $(this).find('input[type="checkbox"]').prop('checked',true);
	    });
	}else{
	    $.each(data, function(key, index)
	    {
		var input = $(this).find('input[type="checkbox"]').prop('checked',false);
	    });
	}

});
$(nNodes).hover(
  function () {
    $(this).css("background","#3E93BD");
    $(this).css("color","#FFFFFF");
    $('.link',this).css("color","#FFFFFF");
  }, 
  function () {
    $(this).css("background","");
    $(this).css("color","#756A71");
    $('.link',this).css("color","#756A71");
  }
);

   
$(nNodes).click(
  function () {
	
	//var newRow = '<tr><td>This is a dynamically added row.</td></tr>';
	//$('#lcNameAddr tr:last').after(newRow);
	
	var lead_info='<table width="100%" class="table table-bordered padded">';
		lead_info+='<tr><td width="35%">Lead Name </td><td>'+$(this).closest('tr').children('td:eq(3)').html()+'</td></tr>';
		lead_info+='<tr><td width="35%">Lead Address </td><td>'+$(this).closest('tr').children('td:eq(4)').html()+'</td></tr>';
		lead_info+='<tr><td width="35%">Phone 1 </td><td>'+$(this).closest('tr').children('td:eq(27)').html()+'</td></tr>';
		lead_info+='<tr><td width="35%">Phone 2 </td><td>'+$(this).closest('tr').children('td:eq(30)').html()+'</td></tr>';
		lead_info+='<tr><td width="35%">Phone 3 </td><td>'+$(this).closest('tr').children('td:eq(31)').html()+'</td></tr>';
		lead_info+='<tr><td width="35%">E-Mail 1 </td><td>'+$(this).closest('tr').children('td:eq(6)').text()+'</td></tr>';
		lead_info+='<tr><td width="35%">E-Mail 2 </td><td>'+$(this).closest('tr').children('td:eq(32)').text()+'</td></tr>';
		lead_info+='</table>';

	$('#lead_info').html(lead_info);

	var lead_details='<table width="100%" class="table table-bordered padded">';
	
	var leadsubtype=$(this).closest('tr').children('td:eq(9)').text()+" / "+$(this).closest('tr').children('td:eq(10)').text();

	lead_details+='<tr><td width="40%">Lead / Sub Type </td><td>'+leadsubtype+'</td></tr>';

	lead_details+='<tr><td width="40%">Referred By </td><td>'+$(this).closest('tr').children('td:eq(11)').html()+'</td></tr>';

	lead_details+='<tr><td width="40%">Lead Dealer </td><td>'+$(this).closest('tr').children('td:eq(12)').text()+'</td></tr>';

	lead_details+='<tr><td width="40%">Best Time to contact</td><td>'+$(this).closest('tr').children('td:eq(7)').text()+'</td></tr>';

	lead_details+='<tr><td width="40%">Lead In </td><td>'+$(this).closest('tr').children('td:eq(8)').text()+'</td></tr>';	
	
	lead_details+='<tr><td width="40%">Start Contacting </td><td>'+$(this).closest('tr').children('td:eq(13)').text()+'</td></tr>';	
	
	lead_details+='<tr><td width="40%">Lead Status </td><td>'+$(this).closest('tr').children('td:eq(16)').text()+'</td></tr>';

	lead_details+='<tr><td width="40%">Lead Result </td><td>'+$(this).closest('tr').children('td:eq(17)').text()+'</td></tr>';

	lead_details+='</table>';

	$('#lead_details').html(lead_details);
	

	var apt_info='<table width="100%" class="table table-bordered padded">';
		apt_info+='<tr><td width="45%">Set Date </td><td>'+$(this).closest('tr').children('td:eq(14)').html()+'</td></tr>';
		apt_info+='<tr><td width="45%">Set By </td><td>'+$(this).closest('tr').children('td:eq(15)').html()+'</td></tr>';
		apt_info+='<tr><td width="45%">Appointment Date Time </td><td>'+$(this).closest('tr').children('td:eq(18)').html()+'</td></tr>';
		apt_info+='<tr><td width="45%">Appointment Type </td><td>'+$(this).closest('tr').children('td:eq(19)').text()+'</td></tr>';
		apt_info+='<tr><td width="45%">Appt. Status </td><td>'+$(this).closest('tr').children('td:eq(28)').text()+'</td></tr>';
		apt_info+='<tr><td width="45%">Appt. Result </td><td>'+$(this).closest('tr').children('td:eq(29)').text()+'</td></tr>';
		apt_info+='<tr><td width="45%">Dealer </td><td>'+$(this).closest('tr').children('td:eq(25)').text()+'</td></tr>';
		apt_info+='<tr><td width="40%">Demo Gift </td><td> &nbsp; </td></tr>';
		apt_info+='</table>';

	$('#apt_info').html(apt_info);

// For Referral Leads Page 


	var lead_info1='<table width="100%" class="table table-bordered padded">';
		lead_info1+='<tr><td width="35%">Lead Name </td><td>'+$(this).closest('tr').children('td:eq(3)').html()+'</td></tr>';
		lead_info1+='<tr><td width="35%">Lead Address </td><td>'+$(this).closest('tr').children('td:eq(4)').html()+'</td></tr>';
		lead_info1+='<tr><td width="35%">Phone 1 </td><td>'+$(this).closest('tr').children('td:eq(28)').text()+'</td></tr>';
		lead_info1+='<tr><td width="35%">Phone 2 </td><td>'+$(this).closest('tr').children('td:eq(29)').text()+'</td></tr>';
		lead_info1+='<tr><td width="35%">Phone 3 </td><td>'+$(this).closest('tr').children('td:eq(30)').text()+'</td></tr>';
		lead_info1+='<tr><td width="35%">E-Mail 1 </td><td>'+$(this).closest('tr').children('td:eq(6)').text()+'</td></tr>';
		lead_info1+='<tr><td width="35%">E-Mail 2 </td><td>'+$(this).closest('tr').children('td:eq(32)').text()+'</td></tr>';
		lead_info1+='</table>';

	$('#lead_info1').html(lead_info1);


	var lead_details1='<table width="100%" class="table table-bordered padded">';
	
	var leadsubtype1=$(this).closest('tr').children('td:eq(9)').text()+" / "+$(this).closest('tr').children('td:eq(10)').text();

	lead_details1+='<tr><td width="40%">Lead / Sub Type </td><td>'+leadsubtype1+'</td></tr>';

	lead_details1+='<tr><td width="40%">Referred By </td><td>'+$(this).closest('tr').children('td:eq(11)').html()+'</td></tr>';
	
	lead_details1+='<tr><td width="40%">Lead Dealer </td><td>'+$(this).closest('tr').children('td:eq(12)').text()+'</td></tr>';
	
	lead_details1+='<tr><td width="40%">Best Time to contact</td><td>'+$(this).closest('tr').children('td:eq(7)').html()+'</td></tr>';

	lead_details1+='<tr><td width="40%">Lead In </td><td>'+$(this).closest('tr').children('td:eq(8)').text()+'</td></tr>';
	
	lead_details1+='<tr><td width="40%">Start Contacting </td><td>'+$(this).closest('tr').children('td:eq(32)').text()+'</td></tr>';	
	
	lead_details1+='<tr><td width="40%">Lead Status </td><td>'+$(this).closest('tr').children('td:eq(15)').text()+'</td></tr>';

	lead_details1+='<tr><td width="40%">Lead Result </td><td>'+$(this).closest('tr').children('td:eq(16)').text()+'</td></tr>';

	lead_details1+='</table>';

	$('#lead_details1').html(lead_details1);

	var apt_info1='<table width="100%" class="table table-bordered padded">';

		apt_info1+='<tr><td width="45%">Set Date </td><td>'+$(this).closest('tr').children('td:eq(13)').html()+'</td></tr>';
		apt_info1+='<tr><td width="45%">Set By </td><td>'+$(this).closest('tr').children('td:eq(14)').text()+'</td></tr>';
		apt_info1+='<tr><td width="45%">Appointment Date Time </td><td>'+$(this).closest('tr').children('td:eq(17)').html()+'</td></tr>';
		apt_info1+='<tr><td width="45%">Appointment Type </td><td>'+$(this).closest('tr').children('td:eq(18)').text()+'</td></tr>';
		apt_info1+='<tr><td width="45%">Appt. Status </td><td>'+$(this).closest('tr').children('td:eq(26)').text()+'</td></tr>';
		apt_info1+='<tr><td width="45%">Appt. Result </td><td>'+$(this).closest('tr').children('td:eq(27)').text()+'</td></tr>';
		apt_info1+='<tr><td width="45%">Dealer </td><td>'+$(this).closest('tr').children('td:eq(25)').text()+'</td></tr>';
		apt_info1+='<tr><td width="40%">Demo Gift </td><td> &nbsp; </td></tr>';
		apt_info1+='</table>';

	$('#apt_info1').html(apt_info1);
	
  }
);

});
$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
  
</script>
<script language="javascript">
function printpage() {
    var data = '<table><tr><th>Lead Listing</th></tr></table><table class="remove" border="1" cellspacing="0" frame="box" rules="all" >' + document.getElementsByTagName('table')[0].innerHTML + '</table>';
    data += '<br/><button onclick="window.print()"  class="noprint">Print the Report</button>';
    data += '<style type="text/css" media="print"> .noprint {visibility: hidden;} </style>';
    myWindow = window.open('', '', 'width=800,scrollbars=yes');
    myWindow.innerWidth = screen.width;
    myWindow.innerHeight = screen.height;
    myWindow.screenX = 0;
    myWindow.screenY = 0;
    myWindow.document.write(data);
    myWindow.focus();
}
</script>
<script>
$(document).ready(function() {


var oTable = $('#example').dataTable();
var nNodes = oTable.fnGetNodes( );

$('#emailit').click(function(){ 
	var values = [];
	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
	});
	
	if(values=="")
	{
		alert("Please select any Lead to send E-Mail");
	}else{
		window.open('messages/composemail.php?type=leadmail&tbl=3&leadid='+values,'_blank');
	}

});

$('#bulkDelete').click(function(){ 

	var values = [];
	var lids = [];

	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);

		var lid=$(checkbox).attr('lid');
		lids.push(lid);	 
	});
	
	if(values=="")
	{
		alert("Please select any Lead");
	}else{

	var result=confirm("Deleting this lead will also delete any appointments and information associated with it. Continue deleting this lead?");

		if(result==true)
		{
			var data={
					type:"BulkDelete",
					leadids:values,
					lids:lids,
					page:"leadcard"
				}
			$.ajax({
				type:"POST",
				url:"bulkdelete.php",
				data:data,
				success:function(res){
					window.location.href="leadcard.php";
				}
			});
		}
	
		
	} //else

});

$('#mapit').click(function(){ 
	var values = [];
	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
	});
	
	if(values=="")
	{
		/*bootbox.confirm("Loading all the Lead Addresses in your Listings..! <br> Might take some time to Load Please Wait...",function(result){
			if(result==true){
				//window.location.href="gmap_listing.php?page=leadcard&leadid=0";
				window.open('gmap_listing.php?page=leadcard&leadid=0','_blank');
			}
		});*/
		alert("Please select any Leads to view Map");
	}else{
		//window.location.href="gmap_listing.php?page=leadcard&leadid="+values;
		window.open('gmap_listing.php?page=leadcard&leadid='+values,'_blank');
	}

});


$('#bulk').click(function(){ 

	var values = [];

	$(nNodes).find('input:checked').each(function(i, checkbox){
	var val=$(checkbox).val();
	 values.push(val);
	});

	if(values==''){
		alert("Please Select any Lead ");
		return false;
	}

	var data = {
    			type: 'editlead',
			leadid:values,
		}
    	$.ajax({
    		type: "POST",
    		url:  "multi_edit_leadstatus.php",
    		data: data,
    		success: function(output) {
			var obj =JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:600,
				height:450,
				openjs:function(){
				
					$('a#cancel-event-edit').click(function(){
						window.parent.TINY.box.hide();
					});	

					$("#bsuleadin").datepicker({ autoclose:true });	
					$("#startcont").datepicker({ autoclose:true });	

					$("#bsuleadtype").change(function() {

	var lt=$(this).val();
	if(lt!="")
	{
	    var res=confirm("Changing Lead type will affect survey details if already taken to selected Lead(s)! Are you sure do you want to change?");
	    if(res==true)
	    {
			$.get('loadsubtype.php?leadtype=' + lt, function(data) {
				$("#bsuleadsubtype").html(data);
			});	
	    }else{
		$("#bsuleadtype").val('Select');
	    }
	}else{
		$("#bsuleadsubtype").html('<option>Select</option>');
	}
					});								

					$('a#save-event-edit').click(function(){

							var leadtype = $('#bsuleadtype').val();
							var leadsubtype = $('#bsuleadsubtype').val();
							var leaddealer = $('#bsuleaddealer').val();
							var status = $('#bsustatus').val();
							var result = $('#bsuresult').val();
							var quali = $('#bsuquali').val();
							var leadindate = $('#bsuleadin').val();
							var startcont = $('#startcont').val();

	if(leadtype=='' && leadsubtype=='' && leaddealer=='' && status=='' && result=='' && quali==''  && leadindate=='' &&startcont==''){
								$('.msg').css({'display':'block'});
								$('.msg').html('Pls Choose atleast one options');
								return false ;
							}

							var data = {
							    	type: 'editSaveEvent',
								leadid:values,
								leadtype: leadtype,
								leadsubtype:leadsubtype,
								leaddealer:leaddealer,
								status:status,
								result:result,
								quali:quali,
								leadindate:leadindate,
								startcont:startcont
							}

							$.ajax({
							    		type: "POST",
							    		url: "multi_edit_leadstatus.php",
							    		data: data,
							    		success: function(resp) {

										bootbox.alert(resp,function(){
											window.parent.TINY.box.hide();
											window.location.href = 'leadcard.php';
										});
										
									}

							});
					});	
								
				} // end of open js				    	

			});

	    } // end of success function
            

        });
});// end of bulk click

$('#utype').click(function(){
var values = [];
$(nNodes).find('input:checked').each(function(i, checkbox){
var val=$(checkbox).val();
 values.push(val);
});
if(values==''){
alert("Please Make a Selection");
return false;
}
var data = {
    			type: 'editlead',
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "multi_edit_leadtype.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:500,
							height:400,
							openjs:function(){
 						$('#datepicker').datepicker({
									autoclose:true
							   });							


 $('#mtype').change(function(){
var val=$('#mtype option:selected').attr("class")+" showsubtype";
$('#msubtype .showsubtype').css({display:'none'});
$('#msubtype .showsubtype').removeAttr("selected");
$('#msubtype option[class="'+val+'"]').css({'display':'block'});
});

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	


								

							$('a#save-event-edit').click(function(){
								
							var lead_type = $('#mtype').val();
							var lead_subtype = $('#msubtype').val();
							var ldealer = $('#ldealer').val();
							var lstatus = $('#status').val();
							var lqual = $('#qual').val();
							var ldate = $('#datepicker').val();



					if(lead_type!=''){
					if(lead_subtype==''){
						$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose Lead Subtype ');
							return false ;
								}
						}
					if(lead_type=='' &&  lead_subtype=='' && ldealer=='' &&  lstatus=='' && lqual=='' && ldate==''){

							$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose Any One Selection');
							return false ;
							}

							var data = {
							    	type: 'update_event',
								leadid:values,
								ltype: lead_type,
								subtype:lead_subtype,
								ldealer:ldealer,
								lstatus:lstatus,
								lqual:lqual,
								ldate :ldate ,
							 }

								$.ajax({
								    		type: "POST",
								    		url: "multi_edit_leadtype.php",
								    		data: data,
								    		success: function(resp) {
									        window.parent.TINY.box.hide();
										location.reload();
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
});

var oTable = $('#example').dataTable();
 var nNodes = oTable.fnGetNodes( );
$(nNodes).click(function(evt){

 var $cell=$(evt.target).closest('td');
    if( $cell.index()==0){
     
}
else{
$("#spinner").show();
var id=$(this).closest('tr').children('td:eq(23)').text();
var leadid=$(this).closest('tr').children('td:eq(24)').text();

//alert("Id"+id+" ;  leadid"+leadid);

var data = {
    			type: 'view_answer',
			lid:id,
			leadseqid:leadid,
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "view_lead_answer.php",
	    		data: data,
	    		success: function(output) {
$("#spinner").hide();
$('#survey').html(output);
}
});
}
});

});

</script>
