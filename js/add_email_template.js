<script type="text/javascript">

var dispappt=0;

$(document).ready(function() {


	$('#savenewtemplate').click(function(){

		var templatename=$('#templatename').val();
		var subject=$('#subject').val();
		var message=$('#message').val();

		if(templatename=="")
		{
			alert("Please Fill Template Name");
		}else if(subject=="")
		{
			alert("Please Fill Subject");
		}else if(message=="")
		{
			alert("Please Fill Message Content");
		}else{

			var data = {
	    			type: 'savenewtemplate',
				templatename:templatename,
				subject:subject,
				message:message
			}
	    		$.ajax({
		    		url: "email_template_actions.php",
				type: "POST",
		    		data: data,
		    		success: function(output) {
					alert('Template Created Successfully');
					window.location.href="add_email_template.php";
				}
			});

		}

	});

	$('#updtemplate').click(function(){

		var tempid=$('#tempid').val();
		var templatename=$('#templatename').val();
		var subject=$('#subject').val();
		var message=$('#message').val();

		if(templatename=="")
		{
			alert("Please Fill Template Name");
		}else if(subject=="")
		{
			alert("Please Fill Subject");
		}else if(message=="")
		{
			alert("Please Fill Message Content");
		}else{

			var data = {
	    			type: 'updatetemplate',
				tempid:tempid,
				templatename:templatename,
				subject:subject,
				message:message
			}
	    		$.ajax({
		    		url: "email_template_actions.php",
				type: "POST",
		    		data: data,
		    		success: function(output) {
					alert('Template Updated Successfully');
					window.location.href="email_template_listing.php";
				}
			});

		}

	});

	



});


</script>

<style type="text/css">

table, td, tr, th
{
	border:none;
}
td
{
	padding:6px;
}
.select2-container {
	width: 70% !important;
}
.customddl{
	width: 70% !important;
}

</style>
