<link href="stylesheets/dtable.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript">

$(document).ready(function() {
$('#appt').dataTable(
    {
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
"sPaginationType": "full_numbers",
 "bDestroy": true
    }

);
});
function taskupdate(tid)
{
	var data={
		type:'taskupdate',
		taskid:tid
	}
	$.ajax({
		type: "POST",
		url: "task_actions.php",
		data: data,
		success: function(output)
		{
			var obj=JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:700,
				height:570,
				openjs:function(){

				}
				
			});
		
		}

	});
}
</script>
