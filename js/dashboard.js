<script src="js/jquery.uploadifive.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/uploadifive.css">

<link href="stylesheets/multiple-select.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/jquery.multiple.select.js"></script> 

<!----<script type="text/javascript" src="javascripts/base.js"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/base1.css" />--->

<script src="javascripts/datepair.js"></script> 

<script src="javascripts/formatter.js"></script> 

<script type="text/javascript" src="javascripts/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/jquery.timepicker.css" />

<script type="text/javascript" src="javascripts/limiters.js"></script>


<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>

<script type="text/javascript">
$(document).ready(function() {

$('#edit-training-dialog-desc1').limiter();

$('#edit-training-dialog-desc').limiter();


});
</script>
<script>
function showsubevent(t) {

    var val = $('#edit-training-dialog-trainingname').val() + " showevent";

    $('#topic .showevent').css({
        display: 'none'
    });
    $('#topic .showevent').removeAttr("selected");
    $('#topic option[class="'+val+'"]').css({
        display: 'block'
    });
}

function deleteupimg(id, rid)
{
	bootbox.confirm("<b>Are you sure you want to delete this file?</b>", function(result) {
		if(result) {
			var data = {
				type: 'deleteEventFiles',
				fileid:id,
				eventid:rid
			}
			$.ajax({
				type: "POST",
				url: "events_json.php",
				data: data,
				success: function(resp) {
					$('#g'+id).hide();
					//window.location.href = 'add_company_features.php?job=edit&id='+id;
				}
			});
		}
	});

}

</script>

