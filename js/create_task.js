<script type="text/javascript">

var dispappt=0;

$(document).ready(function() {

	//$('#who').select2();

	$('#assigned_to').select2();

	$('#savenewtask').click(function(){

		var cont_act=$('#cont_act').val();
		var who=$('#who').val();
		var cont_reason=$('#cont_reason').val();
		var assigned_to=$('#assigned_to').val();
		var startdate=$('#startdate').val();
		var duedate=$('#duedate').val();
		var task_status=$('#task_status').val();
		var task_comments=$('#task_comments').val();

		var ischecked=$('#selfcalender').is(':checked');

		if(ischecked){
			dispappt=1;			
		}else{
			dispappt=0;
		}

		if(cont_act==0)
		{
			alert("Please Select Contact Activity");
		}else if(who==0)
		{
			alert("Please Select Who Feild");
		}else if(cont_reason==0)
		{
			alert("Please Select Contact Reason");
		}else if(assigned_to=="")
		{
			alert("Please Select Assigned To");
		}else if(startdate=="")
		{
			alert("Please Select Start Date");
		}else if(task_status==0)
		{
			alert("Please Select Task Status");
		}else{
			$("#spinner").show();
			var data = {
	    			type: 'savenewtask',
				cont_act:cont_act,
				who:who,
				cont_reason:cont_reason,
				assigned_to:assigned_to,
				startdate:startdate,
				duedate:duedate,
				task_status:task_status,
				task_comments:task_comments,
				dispappt:dispappt
			}
	    		$.ajax({
		    		url: "task_actions.php",
				type: "POST",
		    		data: data,
		    		success: function(output) {
					$("#spinner").hide();
					alert('Task Created Successfully');
					window.location.href="createtask.php";
				}
			});

		}

	});

	$('#updtask').click(function(){

		var tid=$('#tid').val();
		var tseqid=$('#tseqid').val();
		var cont_act=$('#cont_act').val();
		var who=$('#who').val();
		var cont_reason=$('#cont_reason').val();
		var assigned_to=$('#assigned_to').val();
		var startdate=$('#startdate').val();
		var duedate=$('#duedate').val();
		var task_status=$('#task_status').val();
		var task_comments=$('#task_comments').val();

		var ischecked=$('#selfcalender').is(':checked');

		if(ischecked){
			dispappt=1;			
		}else{
			dispappt=0;
		}

		if(cont_act==0)
		{
			alert("Please Select Contact Activity");
		}else if(who==0)
		{
			alert("Please Select Who Feild");
		}else if(cont_reason==0)
		{
			alert("Please Select Contact Reason");
		}else if(assigned_to=="")
		{
			alert("Please Select Assigned To");
		}else if(startdate=="")
		{
			alert("Please Select Start Date");
		}else if(task_status==0)
		{
			alert("Please Select Task Status");
		}else{

			var data = {
	    			type: 'updtask',
				tid:tid,
				tseqid:tseqid,
				cont_act:cont_act,
				who:who,
				cont_reason:cont_reason,
				assigned_to:assigned_to,
				startdate:startdate,
				duedate:duedate,
				task_status:task_status,
				task_comments:task_comments,
				dispappt:dispappt
			}
	    		$.ajax({
		    		url: "task_actions.php",
				type: "POST",
		    		data: data,
		    		success: function(output) {
					alert('Task Created Successfully');
					window.location.href="createtask.php";
				}
			});

		}

	});

	$("#updtaskstatus").click(function(){

		var tid=$('#tid').val();
		var tseqid=$('#tseqid').val();
		var task_status=$('#task_status').val();

		if(task_status==0)
		{
			alert("Please Select Task Status");
		}else{
			$("#spinner").show();
			var data = {
	    			type: 'updtaskstatus',
				tid:tid,
				tseqid:tseqid,
				task_status:task_status
			}
	    		$.ajax({
		    		url: "task_actions.php",
				type: "POST",
		    		data: data,
		    		success: function(output) {
					$("#spinner").hide();
					alert('Task Status Updated Successfully');
					window.location.href="task_listing.php";
				}
			});

		}


	});

	$('#openinemail').click(function(){

		var who=$('#who').val();

		if(who==0)
		{
			alert("Please Select Who Feild");
		}else{
				
			window.location.href="messages/composemail.php?type=newtask&who="+who;

		}

	});

	$('#openinleadlist').click(function(){

		var who=$('#who').val();
		

		if(who==0)
		{
			alert("Please Select Who Feild");
		}else{

			var a = who.split("-");
			
			var b = a[0].split("_");

			var page=b[0];
			var id=b[1];
			
			if(page=="1") // for campaign
			{
				window.location.href="campaign.php?type=newtask&id="+id;
			}
			else if(page=="2") // for Customer - Lead Card
			{
				window.location.href="leadcard.php?type=newtask&id="+id;
			}
			else if(page=="3") // for Customer - Lead Card
			{
				window.location.href="leadcard.php?type=newtask&id="+id;
			}
			else if(page=="4") // for Team Member
			{
				alert('The Selected Person Not in the Lead Listing ');
			}

		}

	});

	
	$('#openinleaddetails').click(function(){

		var who=$('#who').val();
		

		if(who==0)
		{
			alert("Please Select Who Feild");
		}else{

			var a = who.split("-");
			
			var b = a[0].split("_");

			var page=b[0];
			var id=b[1];
			
			if(page=="1") // for campaign
			{
				window.location.href="add_campaign.php?action=edit&lid="+id;
			}
			else if(page=="2") // for Customer - Lead Card
			{
				window.location.href="add_leads.php?action=edit&lid="+id;
			}
			else if(page=="3") // for Customer - Lead Card
			{
				window.location.href="add_leads.php?action=edit&lid="+id;
			}
			else if(page=="4") // for Team Member
			{
				alert('The Selected Person Not in the Lead Listing ');
			}

		}

	});

	
	$('#openincustomerlist').click(function(){

		var who=$('#who').val();
		

		if(who==0)
		{
			alert("Please Select Who Feild");
		}else{

			var a = who.split("-");
			
			var b = a[0].split("_");

			var page=b[0];
			var id=b[1];
			
			if(page=="1") // for campaign
			{
				window.location.href="campaign.php?type=newtask&id="+id;
			}
			else if(page=="2") // for Customer 
			{
				window.location.href="customer.php?type=newtask&id="+id;
			}
			else if(page=="3") // for Lead Card
			{
				alert('The Selected Person Not in the Customer Listing ');
			}
			else if(page=="4") // for Team Member
			{
				alert('The Selected Person Not in the Customer Listing ');
			}

		}

	});



});


</script>

<style type="text/css">

table, td, tr, th
{
	border:none;
}
td
{
	padding:6px;
}
.select2-container {
	width: 70% !important;
}
.customddl{
	width: 70% !important;
}

</style>
