<style>
ul
{
list-style-type: none;
}
#spinner {
position: fixed;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.5;
background: url('images/preload.gif') 50% 50% no-repeat #ede9df;
}
hr{
margin-top:3px;
margin-bottom:3px;
}
table.dataTable tr.odd {
    background-color: #EAEBEF;
}
table.dataTable tr th{
min-width:230px;
}
.dataTables_scrollBody{
 background-color: #EAEBEF;
}
</style>
<link href="stylesheets/dtable.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>
<script src="js/FixedColumns.min.js"></script>
<script>
$(function(){
  $('.from').datetimepicker({
  format:'M d Y',
 validateOnBlur:false,
  onShow:function( ct ){
   this.setOptions({
    maxDate:$('.to').val()?$('.to').val():false,
formatDate:'M d Y'
   })
  },
  timepicker:false
 });



$('.to').datetimepicker({
format:'M d Y',
 validateOnBlur:false,
  onShow:function( ct ){
   this.setOptions({
    minDate:$('.from').val()?$('.from').val():false,
formatDate:'M d Y'
   })
  },
  timepicker:false
 });

});

</script>
<script>
$(document).ready(function() {
var pTable = $('.dTable').dataTable({
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
 "sPaginationType": "full_numbers",
 "bDestroy": true,
  "sScrollY": 200,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": false,
    });
  new $.fn.dataTable.FixedColumns(pTable );

$('.tab').click(function(){
var tab_id=$(this).attr('href') +" #reset";
setTimeout(function() {
    $(tab_id).trigger('click');
}, 0e3);
})
$('.list').click(function(){
$("#spinner").show();
var from=$('.active .from').val();
var to=$('.active .to').val();
if(from!=''){
if(to==''){
$("#spinner").hide();
bootbox.alert('please Fill The "To" Date ');
return false;
}
}
if(to!=''){
if(from==''){
$("#spinner").hide();
alert('please Fill The "From" Date ');
return false;
}
}
var data={
		type:'filter',
		userid:$('.active .user').val(),
		subtype:$('.active .subtype').val(),
		from:$('.active .from').val(),
		to:$('.active .to').val(),
		parent:$('.active .sub').val(),
		div:$('.active .div').val(),
		status:$('.active .status').val()
	}
	$.ajax({
		type: "POST",
		url: "filter.php",
		data: data,
		dataType: 'HTML',
		cache: false,
		success: function(output)
		{
$("#spinner").hide();
	$('.active .user_table').html(output);
	var pTable = $('.active .dTable').dataTable({
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
 "sPaginationType": "full_numbers",
 "bDestroy": true,
  "sScrollY": 200,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": false,
    });
  new $.fn.dataTable.FixedColumns(pTable );
		}
		});
});

$('.reset').click(function(){
$("#spinner").show();
var data={
		type: 'reset',
		parent:$('.active .sub').val(),
	}
	$.ajax({
		type: "POST",
		url: "filter.php",
		data: data,
		cache: false,
		dataType: 'HTML',
		success: function(output)
		{
$("#spinner").hide();
		$('.active .user').val('');
		$('.active .subtype').val('');
		$('.active .from').val('');
		$('.active .to').val('');
		$('.active .status').val('')
	$('.active .user_table').html(output);
	var pTable = $('.active .dTable').dataTable({
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
 "sPaginationType": "full_numbers",
 "bDestroy": true,
  "sScrollY": 200,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": false,

    });
  new $.fn.dataTable.FixedColumns(pTable );
	
		}
		});

});
});
</script>
