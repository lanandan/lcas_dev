<script>
function confirmDelete() 
{
	var agree=confirm("Are you sure you want to delete this Campaign?");
	if (agree)
		return true ;
	else
		return false ;
}
</script>
<script>
$(document).ready(function() {

var oTable = $('#click').dataTable();
 	var nNodes = oTable.fnGetNodes();
	$('#check').click(function(){
	var i=$('#check').is(":checked");
	if(i==true)
		$(nNodes).find('input[type=checkbox]').prop('checked',true);
	else
		$(nNodes).find('input[type=checkbox]').prop('checked',false);
});

$('#emailit').click(function(){ 
	var values = [];
	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
	});
	
	if(values=="")
	{
		alert("Please select any Campaign to send E-Mail");
	}else{
		window.open('messages/composemail.php?type=leadmail&tbl=1&leadid='+values,'_blank');
	}

});

$('#mapit').click(function(){ 
	var values = [];
	$(nNodes).find('input:checked').each(function(i, checkbox){
		var val=$(checkbox).val();
		values.push(val);
	});
	
	if(values=="")
	{
	/*bootbox.confirm("Loading all the Campaign Addresses in your Listings..! <br> Might take some time to Load. Please Wait...",function(result){
			if(result==true){
				//window.location.href="gmap_listing.php?page=leadcard&leadid=0";
				window.open('gmap_listing.php?page=campaign&leadid=0','_blank');
			}
		});*/
		alert("Please select any Campaign to view Map");
	}else{
		//window.location.href="gmap_listing.php?page=leadcard&leadid="+values;
		window.open('gmap_listing.php?page=campaign&leadid='+values,'_blank');
	}

});


$('#click').dataTable(
    {
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
"sPaginationType": "full_numbers",
 "bDestroy": true
    }

);


$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});
$("tr").not(':first').hover(
  function () {
    $(this).css("background","#3E93BD");
    $(this).css("color","#FFFFFF");
    $('.link',this).css("color","#FFFFFF");
  }, 
  function () {
    $(this).css("background","");
    $(this).css("color","#756A71");
    $('.link',this).css("color","#756A71");
  }
);

$("tr").not(':first').click(
  function () {

	var campg_addr=$(this).closest('tr').children('td:eq(2)').html()+"<br>"+$(this).closest('tr').children('td:eq(3)').text();

	var campaign_info='<table width="100%" class="table table-bordered padded">';
		campaign_info+='<tr><td width="35%">Campaign Name </td><td>'+$(this).closest('tr').children('td:eq(9)').html()+'</td></tr>';
		campaign_info+='<tr><td width="35%">Contact Person </td><td>'+$(this).closest('tr').children('td:eq(1)').html()+'</td></tr>';
		campaign_info+='<tr><td width="35%">Address </td><td>'+campg_addr+'</td></tr>';
		campaign_info+='<tr><td width="35%">Phone # </td><td>'+$(this).closest('tr').children('td:eq(4)').html()+'</td></tr>';
		campaign_info+='<tr><td width="35%">E-Mail </td><td>'+$(this).closest('tr').children('td:eq(6)').text()+'</td></tr>';
		campaign_info+='<tr><td width="35%">Notes </td><td>'+$(this).closest('tr').children('td:eq(5)').text()+'</td></tr>';
		campaign_info+='</table>';

	$('#campaign_info').html(campaign_info);


	var campaign_details='<table width="100%" class="table table-bordered padded">';
	
	var leadsubtype=$(this).closest('tr').children('td:eq(7)').text()+" / "+$(this).closest('tr').children('td:eq(8)').text();

	if($(this).closest('tr').children('td:eq(3)').text()!='')
		campaign_details+='<tr><td width="40%">Lead / Sub Type </td><td>'+leadsubtype+'</td></tr>';

	var campgsubtype=$(this).closest('tr').children('td:eq(22)').text()+" / "+$(this).closest('tr').children('td:eq(23)').text();

	if($(this).closest('tr').children('td:eq(22)').text()!='')
		campaign_details+='<tr><td width="40%">Campign / Sub Type </td><td>'+campgsubtype+'</td></tr>';

	if($(this).closest('tr').children('td:eq(11)').text()!='')
		campaign_details+='<tr><td width="35%">Lead Dealer </td><td>'+$(this).closest('tr').children('td:eq(11)').text()+'</td></tr>';

	if($(this).closest('tr').children('td:eq(24)').text()!='')
		campaign_details+='<tr><td width="35%">Lead Asst </td><td>'+$(this).closest('tr').children('td:eq(24)').text()+'</td></tr>';

	if($(this).closest('tr').children('td:eq(12)').text()!='')
		campaign_details+='<tr><td width="35%">From Date </td><td>'+$(this).closest('tr').children('td:eq(12)').text()+'</td></tr>';
		
	if($(this).closest('tr').children('td:eq(13)').text()!='')
		campaign_details+='<tr><td width="35%">To Date </td><td>'+$(this).closest('tr').children('td:eq(13)').text()+'</td></tr>';
	
	campaign_details+='</table>';

	$('#campaign_details').html(campaign_details);

	var campaign_incentive='<table width="100%" class="table table-bordered padded">';
		campaign_incentive+='<tr><td width="45%">Drawing Prize </td><td>'+$(this).closest('tr').children('td:eq(14)').html()+'</td></tr>';
		campaign_incentive+='<tr><td width="45%">Campaign Prize </td><td>'+$(this).closest('tr').children('td:eq(15)').html()+'</td></tr>';
		campaign_incentive+='<tr><td width="45%">Gift1 </td><td>'+$(this).closest('tr').children('td:eq(16)').html()+'</td></tr>';
		campaign_incentive+='<tr><td width="45%">Gift2 </td><td>'+$(this).closest('tr').children('td:eq(17)').text()+'</td></tr>';
		campaign_incentive+='<tr><td width="45%">Gift3 </td><td>'+$(this).closest('tr').children('td:eq(18)').text()+'</td></tr>';
		campaign_incentive+='<tr><td width="45%">Gift4 </td><td>'+$(this).closest('tr').children('td:eq(19)').text()+'</td></tr>';
		campaign_incentive+='<tr><td width="45%">Gift5 </td><td>'+$(this).closest('tr').children('td:eq(20)').text()+'</td></tr>';
		campaign_incentive+='<tr><td width="45%">Gift6 </td><td>'+$(this).closest('tr').children('td:eq(21)').text()+'</td></tr>';
		campaign_incentive+='</table>';

	$('#campaign_incentive').html(campaign_incentive);
	
	/*$('#name').html("Contact Person    :  " +$(this).closest('tr').children('td:eq(1)').text());
	$('#addr').html("Address           :  " +$(this).closest('tr').children('td:eq(2)').text());
	$('#addr2').html("Address Line2    :  " +$(this).closest('tr').children('td:eq(3)').text());
	$('#phone').html("Phone1 , Phone2   :  " +$(this).closest('tr').children('td:eq(4)').text());
	$('#notes').html("Notes     : "+$(this).closest('tr').children('td:eq(5)').text());
	$('#email').html("Email              :  "+$(this).closest('tr').children('td:eq(6)').text());
	$('#campaign_name').html("Campaign Name:  "+$(this).closest('tr').children('td:eq(9)').text());
	$('#ltype').html("Lead Type :  "+$(this).closest('tr').children('td:eq(7)').text());
	$('#lsubtype').html("Lead Subtype :  "+$(this).closest('tr').children('td:eq(8)').text());
	$('#ldeal').html("Lead Dealer :  "+$(this).closest('tr').children('td:eq(11)').text());
	$('#sdate').html("From Date :  "+$(this).closest('tr').children('td:eq(12)').text());
	$('#edate').html("To Date :  "+$(this).closest('tr').children('td:eq(13)').text());
	$('#drawingprice').html("Drawing Prize:  "+$(this).closest('tr').children('td:eq(14)').text());	
	$('#campaignprice').html("Campaign Prize:  "+$(this).closest('tr').children('td:eq(15)').text());
	$('#gift11').html("Gift1:  "+$(this).closest('tr').children('td:eq(16)').text());
	$('#gift12').html("Gift2:  "+$(this).closest('tr').children('td:eq(17)').text());
	$('#gift13').html("Gift3:  "+$(this).closest('tr').children('td:eq(18)').text());
	$('#gift14').html("Gift4:  "+$(this).closest('tr').children('td:eq(19)').text());
	$('#gift15').html("Gift5:  "+$(this).closest('tr').children('td:eq(20)').text());
	$('#gift16').html("Gift6:  "+$(this).closest('tr').children('td:eq(21)').text());
	$('#ctype').html("Campaign Type:  "+$(this).closest('tr').children('td:eq(22)').text());
	$('#csubtype').html("Campaign Subtype:  "+$(this).closest('tr').children('td:eq(23)').text());
	$('#leadassis').html("Lead Asst :  "+$(this).closest('tr').children('td:eq(24)').text());*/

  }
);

});



</script>
<script language="javascript">
function printpage() {
    var data = '<table><tr><th>Campaign Listing</th></tr></table><table class="remove" border="1" cellspacing="0" frame="box" rules="all" >' + document.getElementsByTagName('table')[0].innerHTML + '</table>';
    data += '<br/><button onclick="window.print()"  class="noprint">Print the Report</button>';
    data += '<style type="text/css" media="print"> .noprint {visibility: hidden;} </style>';
    myWindow = window.open('', '', 'width=800,scrollbars=yes');
    myWindow.innerWidth = screen.width;
    myWindow.innerHeight = screen.height;
    myWindow.screenX = 0;
    myWindow.screenY = 0;
    myWindow.document.write(data);
    myWindow.focus();
}
</script>
<link href="stylesheets/dtable.css" media="screen" rel="stylesheet" type="text/css" />
