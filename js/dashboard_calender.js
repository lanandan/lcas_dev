$(document).ready(function() {

$('#edit-training-dialog-desc1').limiter();

$('#edit-training-dialog-desc').limiter();

  var d, date, m, y, dd;
  new CalendarEvents($('#external-events'));
  date = new Date();
  d = date.getDate();
  m = date.getMonth();
  y = date.getFullYear();

  dd = date.getDay();

  var tmischecked=$('#teammember').is(':checked');
  var tm=0;

  if(tmischecked){
	tm=1;			
  }else{
	tm=0;
  }

  var demoischecked=$('#demo').is(':checked');
  var demo=0;

  if(demoischecked){
	demo=1;			
  }else{
	demo=0;
  }

  var eventsischecked=$('#events').is(':checked');
  var events=0;

  if(eventsischecked){
	events=1;			
  }else{
	events=0;
  }

  var taskischecked=$('#task').is(':checked');
  var task=0;

  if(taskischecked){
	task=1;			
  }else{
	task=0;
  }

  var source='events_json.php?type=fetchEventsDashBoard&tm='+tm+'&demo='+demo+'&events='+events+'&task='+task;

  //dashboard calendar
  var dbcal= $("#calendar").fullCalendar({
    header: {
      left: "prev,next today",
      center: "title",
      right: "month,agendaWeek,agendaDay"
    },
    defaultView: 'agendaWeek',
    editable: true,
    droppable: true,
    firstHour: 10,
    height: 640,
    firstDay:dd,
    startParam: 'start',
    endParam: 'end',
    slotEventOverlap:false,
    selectable: true,
    selectHelper: true,
   // ignoreTimezone:false,
    eventClick: function( event, jsEvent, view ) { 
	//console.log(event);

	end = new Date(event.start);
	end.setHours(end.getHours() + 3);
	
	var month=new Array();
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";
	var mmn = month[end.getMonth()]; 

	var seldate=end.getDate()+'/'+mmn+'/'+end.getFullYear();
	
	var data = {
    			type: 'editTrainingEventbyCreator',
			id:event.id,
			tseqid:event.tseqid,
			sdate: mmn,
			appttype: event.appttype,
			start: event.start,
			end: event.end,
			allDay: event.allDay
		}

		$.ajax({
	    	type: "POST",
	    	url: "events_json.php",
	    	data: data,
	    	success: function(output) {
			var obj = JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:750,
				height:460,
				openjs:function(){

					autoclose:true

					var cseqid=$('#curtrainingseqid1').val();

					$('#file1').uploadifive({
						'auto'             : true,
						'queueID'          : 'queue1',
						'queueSizeLimit'   : 5,
						'uploadLimit'      : 5,
						'uploadScript'     : 'uploadifive.php?job=edit&tabname=evnt&tseqid='+cseqid,
						'onProgress'   	   : function(file, e) {
								    if (e.lengthComputable) {
									var percent = Math.round((e.loaded / e.total) * 100);
								    }
								    file.queueItem.find('.fileinfo').html(' - ' + percent + '%');
								    file.queueItem.find('.progress-bar').css('width', percent + '%');
						}, 
						'onUploadComplete' : function(file, data) {

									var d1=data.trim();
									
									$('#output1').append(d1+',');
									
									document.getElementById('upimgids1').value = $('#output1').html();

									file.queueItem.find('.close').html(data);
				
								},
						'onCancel'     : function() {
							    alert('The file ' + file.name + ' was cancelled!');
						} 
	
					});

					$('#memberslist1').multipleSelect();

					/*$('#memberslist1').select2();
					$('#memberslist1').addClass('chzn-select');*/

					$('#edit-training-dialog-trainingname').select2();
					$('#edit-training-dialog-trainingname').addClass('chzn-select');

					/*$('#topic').select2();
					$('#topic').addClass('chzn-select');*/

					$('#newtraining').hide();
					
					$('#edit-training-dialog-desc1').limiter();
					$('#edit-training-dialog-desc').limiter();

					$('#edit-training-dialog-trainingname').change(function() { 
						var selval=$('#edit-training-dialog-trainingname').val();
							//alert(selval);
						if(selval=="others")
						{
							$('#newtraining').show();
						}else if(selval=="0")
						{
							$('#newtraining').hide();				
						}else
						{
							$('#newtraining').hide();				
						}
				    	});
			
				$('#edit-training-dialog-start-date').datetimepicker({
					format:'m/d/Y',
					lang:'en',
					timepicker:false,
				});

				$('#training-time-from').datetimepicker({
					format:'h:i A',
					formatTime:'h:i A',
					lang:'en',
					datepicker:false,
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});

				$('#training-time-to').datetimepicker({
					format:'h:i A',
					formatTime:'h:i A',
					lang:'en',
					datepicker:false,
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});


var typeval1=$('#edit-event-dialog-type').val();
	
	if(typeval1=="Demo"){
		$('#trnteam').hide();
	}else if(typeval1=="Training"){
		$('#trnteam').show();
	}else if(typeval1=="AdvancedTrainingClinics"){
		$('#trnteam').hide();
	}

$('#memberslist').select2();
$('#memberslist').addClass('chzn-select');

$('#edit-event-dialog-type').change(function() { 

	typeval=$('#edit-event-dialog-type').val();
	
	if(typeval=="Demo"){
		$('#trnteam').hide();
	}else if(typeval=="Training"){
		$('#trnteam').show();
	}else if(typeval=="AdvancedTrainingClinics"){
		$('#trnteam').hide();
	}

});

// for accepting training 
var isaccepted1="";
	
	$('#yes').click(function(){
		$('#yes').removeClass('btn-default');
		$('#maybe').removeClass('btn-gray');
		$('#maybe').addClass('btn-default');
		$('#no').removeClass('btn-red');
		$('#no').addClass('btn-default');

		$('#yes').addClass('btn-green');
		$(this).data('clicked', true);
		//isaccepted1=$('#yes').val();	
	}); 
	$('#maybe').click(function(){	
		$('#maybe').removeClass('btn-default');
		$('#yes').removeClass('btn-green');
		$('#yes').addClass('btn-default');	
		
		$('#no').removeClass('btn-red');		
		$('#no').addClass('btn-default');

		$('#maybe').addClass('btn-gray');
		$(this).data('clicked', true);
		//isaccepted1=$('#maybe').val();	
	}); 
	$('#no').click(function(){
		$('#no').removeClass('btn-default');
		$('#yes').removeClass('btn-green');
		$('#yes').addClass('btn-default');
		$('#maybe').removeClass('btn-gray');
		$('#maybe').addClass('btn-default');

		$('#no').addClass('btn-red');
		$(this).data('clicked', true);
		//isaccepted1=$('#no').val();	
	}); 

$('a#save-TrainingAttend-edit').click(function(){

	var ctseqid=$('#curtrainingseqid1').val();
	
	var checkedids = new Array('0');
	var uncheckedids = new Array('0');

	$('input[name="isatd"]').each(function () {
	    if ( $(this).is(':checked') ) {
		checkedids.push( $(this).attr('value') );
	    }else{
		uncheckedids.push( $(this).attr('value') );
	    }
	});

	//alert("Checked IDs : "+checkedids+" Unchecked IDs : "+uncheckedids);

	var data = {
		type: 'saveAttendedDetails',
		tseqid:ctseqid,
		appttype:event.appttype,
		checkedids:checkedids,
		uncheckedids:uncheckedids
		}
		$.ajax({
			type: "POST",
			url: "events_json.php",
			data: data,
			success: function(resp) {
			//alert('success msg');
			//alert('result = ' + resp);
			 	$('#calendar').fullCalendar( 'refetchEvents');
				window.parent.TINY.box.hide();
	  	        },
		error: function() {
			alert('error in saving Attended Details');
		},

		});


});

$('a#save-TrainingAccept-edit').click(function(){

	var rem=$('#edit-drop-event-dialog-remarks').val();
	var appttype=$('#edit-drop-event-dialog-appttype-remarks').val();
	//var isaccepted=$('input[name=isaccept]:checked').val();
	var seqid=$('#edit-drop-event-dialog-seqid').val();
	var tseqid=$('#edit-drop-event-dialog-tseqid').val();

	var isaccepted1="";

	if(jQuery('#yes').data('clicked')) {
    		isaccepted1=$('#yes').val();	
	} else if(jQuery('#maybe').data('clicked')){
    		isaccepted1=$('#maybe').val();	
	}else if(jQuery('#no').data('clicked')){
		isaccepted1=$('#no').val();	
	}	

	//alert(isaccepted1);

	var data = {
		type: 'saveRemarks',
		id:event.id,
		isaccept:isaccepted1,
		appttype:appttype,
		remarks:rem,
		seqid:seqid,
		tseqid:tseqid
		}
		$.ajax({
			type: "POST",
			url: "events_json.php",
			data: data,
			success: function(resp) {
			//alert('success msg');
			//alert('result = ' + resp);
			 	$('#calendar').fullCalendar( 'refetchEvents');
				window.parent.TINY.box.hide();
	  	        },
		error: function() {
			alert('error in saving');
		},

		});

									
});

$('a#ok-event-edit').click(function(){
		window.parent.TINY.box.hide();
	});	

$('a#cancel-event-edit').click(function(){
		window.parent.TINY.box.hide();
	});	
	
$('a#save-event-edit').click(function(){

var totalSec = $('#edit-event-dialog-table #event-time-from').val();
var hours = parseInt( totalSec / 3600 ) % 24;
var minutes = parseInt( totalSec / 60 ) % 60;
var seconds = totalSec % 60;

var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

var totalSec2 = $('#edit-event-dialog-table #event-time-to').val();
var hours2 = parseInt( totalSec2 / 3600 ) % 24;
var minutes2 = parseInt( totalSec2 / 60 ) % 60;
var seconds2 = totalSec2 % 60;

var result2 = (hours2 < 10 ? "0" + hours2 : hours2) + ":" + (minutes2 < 10 ? "0" + minutes2 : minutes2) + ":" + (seconds2  < 10 ? "0" + seconds2 : seconds2);
var newdate=$('#edit-event-dialog-table #edit-event-dialog-start-date').val();
var newdatesplit=newdate.split("/");
									var eventid = $('#edit-event-dialog-table #event-id-hd').val();
									var eventtype = $('#edit-event-dialog-type').val();
									var eventTitle = $('#edit-event-dialog-table #edit-event-dialog-title1').val() + ' - ' + $('#edit-event-dialog-table #edit-event-dialog-title2').val()+ ' - ' + $('#edit-event-dialog-table #edit-event-dialog-title3').val();  
									var startDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result;
									var endDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result2;
									var memberslist=$('#memberslist').val();
									
									var data = {
							    			type: 'editsaveEvent',
										id: eventid,
										title: eventTitle,
										type1: eventtype,
										start:startDate,
										end:endDate,
										memberslist: memberslist
									 }

									$.ajax({
								    		type: "POST",
								    		url: "event_functions.php",
								    		data: data,
								    		success: function(resp) {
										//alert('result = ' + resp);
										$('#calendar').fullCalendar( 'refetchEvents');
											window.parent.TINY.box.hide();
									    	},
									    	error: function() {
											alert('error in edit saving');
									    	},
								    	});
	
									//alert("callback function in javascript/calender_functions.js");
									
								});	
		$('a#delete-event-edit').click(function(){
		
			var curtrainingseqid=$('#curtrainingseqid1').val();	
			
			bootbox.confirm("Are you sure do you want to delete this Event?", function(result) {
		
				 if (result == true) {
					var data = {
						type: 'Deletetraining',
						curtrainingseqid: curtrainingseqid,
						
					}
					$.ajax({
	    					type: "POST",
						data:data,
	    					url: "events_json.php",
	    					success: function(output) {
							bootbox.alert("Event Deleted Successfully");
							dbcal.fullCalendar('refetchEvents');
							window.parent.TINY.box.hide();
						},
						error: function() {
							bootbox.alert('Error while Deleting the Event Details');
						}

					});
				} 
			}); 
					

		});
			

	$('a#save-training-edit1').click(function(){
	
		
			var eventtype = $('#edit-training-dialog-trainingname').val();
			
			var curtrainingseqid=$('#curtrainingseqid1').val();

			var trndesc=$('#edit-training-dialog-desc1').val();
			var upimgids=document.getElementById('upimgids1').value;
			var memberslist1=$('#memberslist1').val();

	if(eventtype==null || eventtype=="")
	{
		alert("Please Select Event Type/Topic!");
	}else if(memberslist1==null || memberslist1=="")
	{
		alert("Please Select Invite Members!");
	}
	else{

		var data = {
			type: 'saveTraining1',
			trainingname: eventtype,
			curtrainingseqid: curtrainingseqid,
			trndesc:trndesc,
			upimgids:upimgids,
			memberslist1:memberslist1
		 }

		$.ajax({
			type: "POST",
			url: "events_json.php",
			data: data,
			success: function(resp) {
			//alert('result = ' + resp);
			dbcal.fullCalendar('refetchEvents');
				window.parent.TINY.box.hide();
		    	},
		    	error: function() {
				alert('Error while Saving Trainging Details');
		    	},
		});
	}
	
		
	}); // end of save btn click			


	    } // end of openjs
	});
}

	});
		
    },
    select: function(start, end, allDay, jsEvent, view) {

	//alert("Start : "+start+" ; End : "+end);

	end = new Date(start);
	end.setHours(end.getHours() + 3);
	
	var month=new Array();
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";
	var mmn = month[end.getMonth()]; 

	var tdate=("0" + end.getDate()).slice(-2);
	
	var seldate=tdate+'/'+mmn+'/'+end.getFullYear();

	var sdate1=mmn+'/'+tdate+'/'+end.getFullYear();

	function formatAMPM(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'PM' : 'AM';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}
	
	function formatAMPMforEndTime(date) {
	  var hours = date.getHours()+1;
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'PM' : 'AM';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}

	var starttime=formatAMPM(start);
	var endtime=formatAMPMforEndTime(start);

	//alert("Start Time : "+starttime+" ; End Time : "+endtime);

	var data = {
    		type: 'addTrainingEvent',
		sdate: sdate1,
		start: start,
		end: end,
		starttime:starttime,
		endtime:endtime,
		allDay: allDay
	}
	$.ajax({
	    	type: "POST",
	    	url: "events_json.php",
	    	data: data,
	    	success: function(output) {
	
		
			var obj = JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:700,
				openjs:function(){
					autoclose:true

$('#file').uploadifive({
	'auto'             : true,
	'queueID'          : 'queue',
	'queueSizeLimit'   : 5,
	'uploadLimit'      : 5,
	'uploadScript'     : 'uploadifive.php?job=add&tabname=evnt',
	'onProgress'   	   : function(file, e) {
			    if (e.lengthComputable) {
				var percent = Math.round((e.loaded / e.total) * 100);
			    }
			    file.queueItem.find('.fileinfo').html(' - ' + percent + '%');
			    file.queueItem.find('.progress-bar').css('width', percent + '%');
        }, 
	'onUploadComplete' : function(file, data) {

				//console.log(data);

				var d1=data.trim();
				$('#output').append(d1+',');

				document.getElementById('upimgids').value = $('#output').html();

				file.queueItem.find('.close').html(data);
				
				
			}
});



					$('#memberslist1').multipleSelect();

					/*$('#memberslist1').select2();
					$('#memberslist1').addClass('chzn-select');*/

					$('#edit-training-dialog-trainingname').select2();
					$('#edit-training-dialog-trainingname').addClass('chzn-select');

					$('#edit-training-dialog-trainingname').change(function() { 

						var selval=$('#edit-training-dialog-trainingname').val();	
								
				    	});
					
					$('#edit-training-dialog-desc1').limiter();
					$('#edit-training-dialog-desc').limiter();

				$('#edit-training-dialog-start-date').datetimepicker({
					format:'m/d/Y',
					lang:'en',
					timepicker:false
					
				});

				$('#edit-training-dialog-start-date').val(sdate1);

				$('#training-time-from').change(function(){

					var st=$('#training-time-from').val();
					
					var time = $("#training-time-from").val();
					var hours = Number(time.match(/^(\d+)/)[1]);
					var minutes = Number(time.match(/:(\d+)/)[1]);
					var AMPM = time.match(/\s(.*)$/)[1];
					if(AMPM == "PM" && hours<12) hours = hours+12;
					if(AMPM == "AM" && hours==12) hours = hours-12;
					var sHours = hours.toString();
					var sMinutes = minutes.toString();
					if(hours<10) sHours = "0" + sHours;
					if(minutes<10) sMinutes = "0" + sMinutes;

					//var stt=sHours + ":" + sMinutes;

					var stt = new Date('2014','04','11',sHours,sMinutes)

					function formatAMPMforEndTime(date) {
					  var hours = date.getHours()+1;
					  var minutes = date.getMinutes();
					  var ampm = hours >= 12 ? 'PM' : 'AM';
					  hours = hours % 12;
					  hours = hours ? hours : 12; // the hour '0' should be '12'
					  minutes = minutes < 10 ? '0'+minutes : minutes;
					  var strTime = hours + ':' + minutes + ' ' + ampm;
					  return strTime;
					}

					var ed=formatAMPMforEndTime(stt);
					$('#training-time-to').val(ed);

					//alert("Start : "+st+" ; End : "+ed);


				});

				$('#training-time-from').datetimepicker({
					lang:'en',
					datepicker:false,
					formatTime:'h:i A',
					format:'h:i A',
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});

				$('a#cancel-event-edit').click(function(){
					window.parent.TINY.box.hide();
				});

				$('#training-time-to').datetimepicker({
					format:'h:i A',
					formatTime:'h:i A',
					lang:'en',
					datepicker:false,
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});

			
	$('a#save-training-edit').click(function(){
	
		
			var eventtype = $('#edit-training-dialog-trainingname').val();
			//var eventsubtype = $('#topic').val();
			//var trainingnamenew = $('#edit-training-dialog-trainingnamenew').val();			
			var memberslist1=$('#memberslist1').val();	
			var trndesc=$('#edit-training-dialog-desc').val();
			var eventdate=$('#edit-training-dialog-start-date').val();
			var starttime=$('#training-time-from').val();
			var endtime=$('#training-time-to').val();
			var upimgids=document.getElementById('upimgids').value;
			

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 30000);
});

if(eventtype==''&&eventsubtype=='')
{
$('.msg').css({'display':'block'});
$('.msg').html('Pls choose Event type & subtype');
return false;
}
if(memberslist1=='null')
{
$('.msg').css({'display':'block'});
$('.msg').html('Pls invite atleast one member');
return false;
}

	if(eventtype==null || eventtype=="")
	{
		alert("Please Select Event Type/Topic!");
	}else if(memberslist1==null || memberslist1=="")
	{
		alert("Please Select Invite Members!");
	}
	else if(eventdate=="")
	{
		alert("Please Select Event Date");
	}
	else if(starttime==""){
		alert("Please Select Start Time");
	}else if(endtime==""){
		alert("Please Select End Time");
	}else{

		$("#spinner").show();

		var data = {
			type: 'saveTraining',
			trainingname: eventtype,
			trndesc:trndesc,
			eventdate:eventdate,
			start:starttime,
			end:endtime,
			file:upimgids,
			memberslist1: memberslist1
		 }


		$.ajax({
			type: "POST",
			url: "events_json.php",
			data:data,
			success: function(resp) {
			//alert('result = ' + resp);
			dbcal.fullCalendar('refetchEvents');
				$("#spinner").hide();
				window.parent.TINY.box.hide();
			},
		    	error: function() {
				alert('Error while Saving Trainging Details');
		    	},
		});
	}
	
		
	}); // end of save btn click			


			    } // end of openjs

			});
		

		dbcal.fullCalendar('unselect');

		}
	});

    },
    drop: function(date, allDay) {
      var copiedEventObject, originalEventObject;
      originalEventObject = $(this).data('eventObject');
      copiedEventObject = $.extend({}, originalEventObject);
      copiedEventObject.start = date;
      copiedEventObject.allDay = allDay;
      dbcal.fullCalendar('renderEvent', copiedEventObject, true);
      if ($("#drop-remove").is(":checked")) {
        return $(this).remove();
      }
    },
    eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {

		var data = {
    			type: 'editDropTrainingEvent',
			id:event.id,
			tseqid:event.tseqid,
			appttype:event.appttype	
		}
    		$.ajax({
	    		type: "POST",
	    		url: "events_json.php",
	    		data: data,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							openjs:function(){
			
								$('#edit-training-dialog-desc1').limiter();

								$('#edit-training-dialog-desc').limiter();

								$('a#cancel-event-edit').click(function(){
									dbcal.fullCalendar( 'refetchEvents');
									window.parent.TINY.box.hide();
								});	
								$('a#save-event-edit').click(function(){
									var startDate = event.start;
							    		var endDate = event.end;

				var sdate = new Date(Date.parse(startDate));
	
				var currentDate = sdate.getUTCDate();
				currentDate = ("0" + currentDate).slice(-2);

				var currentMonth = sdate.getMonth()+1;
				currentMonth = ("0" + currentMonth).slice(-2);

				var currentHours = sdate.getHours();
				currentHours = ("0" + currentHours).slice(-2);

				var currentMinutes = sdate.getMinutes();
				currentMinutes = ("0" + currentMinutes).slice(-2);

				var endHours = sdate.getHours()+1;
				endHours = ("0" + endHours).slice(-2);

			var srtdate=sdate.getFullYear() + "-" +currentMonth+ "-" + currentDate +" "+currentHours+":"+ currentMinutes+":00";
			var eddate=sdate.getFullYear() + "-" +currentMonth+ "-" + currentDate +" "+endHours+":"+ currentMinutes+":00";

									var rem=$('#edit-drop-event-dialog-remarks').val();
									var data = {
							    			type: 'saveDropTrainingEvent',
										id:event.id,
										tseqid:event.tseqid,
										appttype:event.appttype,
										start:srtdate,
										remarks:rem,
										end:eddate }
										$.ajax({
									    		type: "POST",
									    		url: "events_json.php",
									    		data: data,
									    		success: function(resp) {
												//alert('success msg');
												//alert('result = ' + resp);
											dbcal.fullCalendar( 'refetchEvents');
												window.parent.TINY.box.hide();

										    	},
										    	error: function() {
												alert('error in saving');
										    	},
									    	});
									
								});			
							}
						});
		    	},
		    	error: function() {
		    	},
	    	});

	    },
   /* eventSources: [
    {
	url: 'events_json.php?demo='+demo+'&events='+events+'&task='+task,
	type: 'POST',
	data: {
	    type: 'fetchEventsDashBoard'
	},
	error: function() {
	  alert('there was an error while fetching events!');
	},
    }],*/
   events:source,
   annotations: [

		{ start: new Date(y, m, d-1, 0, 0), end: new Date(y, m, d-1, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-2, 0, 0), end: new Date(y, m, d-2, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-3, 0, 0), end: new Date(y, m, d-3, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-4, 0, 0), end: new Date(y, m, d-4, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-5, 0, 0), end: new Date(y, m, d-5, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-6, 0, 0), end: new Date(y, m, d-6, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-7, 0, 0), end: new Date(y, m, d-7, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-8, 0, 0), end: new Date(y, m, d-8, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-9, 0, 0), end: new Date(y, m, d-9, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-10, 0, 0), end: new Date(y, m, d-10, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-11, 0, 0), end: new Date(y, m, d-11, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-12, 0, 0), end: new Date(y, m, d-12, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-13, 0, 0), end: new Date(y, m, d-13, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-14, 0, 0), end: new Date(y, m, d-14, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-15, 0, 0), end: new Date(y, m, d-15, 10, 00),title: '', cls: 'open', background: '#eeeef0' },

		{ start: new Date(y, m, d, 0, 0), end: new Date(y, m, d, 10, 00),title: '', cls: 'open', background: '#eeeef0' },

		{ start: new Date(y, m, d+1, 0, 0), end: new Date(y, m, d+1, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+2, 0, 0), end: new Date(y, m, d+2, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+3, 0, 0), end: new Date(y, m, d+3, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+4, 0, 0), end: new Date(y, m, d+4, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+5, 0, 0), end: new Date(y, m, d+5, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+6, 0, 0), end: new Date(y, m, d+6, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+7, 0, 0), end: new Date(y, m, d+7, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+8, 0, 0), end: new Date(y, m, d+8, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+9, 0, 0), end: new Date(y, m, d+9, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+10, 0, 0), end: new Date(y, m, d+10, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+11, 0, 0), end: new Date(y, m, d+11, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+12, 0, 0), end: new Date(y, m, d+12, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+13, 0, 0), end: new Date(y, m, d+13, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+14, 0, 0), end: new Date(y, m, d+14, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+15, 0, 0), end: new Date(y, m, d+15, 10, 00),title: '', cls: 'open', background: '#eeeef0' },



		{ start: new Date(y, m, d-1, 23, 0),end: new Date(y, m, d-1, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-2, 23, 0),end: new Date(y, m, d-2, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-3, 23, 0),end: new Date(y, m, d-3, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-4, 23, 0),end: new Date(y, m, d-4, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-5, 23, 0),end: new Date(y, m, d-5, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-6, 23, 0),end: new Date(y, m, d-6, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-7, 23, 0),end: new Date(y, m, d-7, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-8, 23, 0),end: new Date(y, m, d-8, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-9, 23, 0),end: new Date(y, m, d-9, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-10, 23, 0),end: new Date(y, m, d-10, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-11, 23, 0),end: new Date(y, m, d-11, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-12, 23, 0),end: new Date(y, m, d-12, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-13, 23, 0),end: new Date(y, m, d-13, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-14, 23, 0),end: new Date(y, m, d-14, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-15, 23, 0),end: new Date(y, m, d-15, 23, 59),title: '', cls: 'open', background: '#eeeef0'},

		{ start: new Date(y, m, d, 23, 0),end: new Date(y, m, d, 23, 59),title: '', cls: 'open', background: '#eeeef0'},

		{ start: new Date(y, m, d+1, 23, 0),end: new Date(y, m, d+1, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+2, 23, 0),end: new Date(y, m, d+2, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+3, 23, 0),end: new Date(y, m, d+3, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+4, 23, 0),end: new Date(y, m, d+4, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+5, 23, 0),end: new Date(y, m, d+5, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+6, 23, 0),end: new Date(y, m, d+6, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+7, 23, 0),end: new Date(y, m, d+7, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+8, 23, 0),end: new Date(y, m, d+8, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+9, 23, 0),end: new Date(y, m, d+9, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+10, 23, 0),end: new Date(y, m, d+10, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+11, 23, 0),end: new Date(y, m, d+11, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+12, 23, 0),end: new Date(y, m, d+12, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+13, 23, 0),end: new Date(y, m, d+13, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+14, 23, 0),end: new Date(y, m, d+14, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+15, 23, 0),end: new Date(y, m, d+15, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		
	]	
    
  });


/*$('.fc-button-prev span').click(function(){
   alert('prev is clicked, do something');
});

$('.fc-button-next span').click(function(){
   alert('nextis clicked, do something');
});*/

var demo=1;
var events=1;
var task=1;
var tm=1;

$('#teammember').click(function(){

	var tmischecked=$('#teammember').is(':checked');
	var demoischecked=$('#demo').is(':checked');
	var eventsischecked=$('#events').is(':checked');
	var taskischecked=$('#task').is(':checked');

	if(tmischecked){
		tm=1;	
	}else{
		tm=0;
	}

	if(demoischecked){
		demo=1;	
	}else{
		demo=0;
	}

	if(eventsischecked){
		events=1;
	}else{
		events=0;	
	}

	if(taskischecked){
		task=1;	
	}else{
		task=0;
	}
	var newsource='events_json.php?type=fetchEventsDashBoard&tm='+tm+'&demo='+demo+'&events='+events+'&task='+task;
	//var newsource='events_json.php?type=fetchEventsDashBoard&demo='+demo+'&events='+events+'&task='+task;

	$("#calendar").fullCalendar('removeEventSource',source);
	$('#calendar').fullCalendar('refetchEvents');
	$("#calendar").fullCalendar('addEventSource',newsource); 
	$('#calendar').fullCalendar('refetchEvents');

	source = newsource;	

});


$('#demo').click(function(){

	var tmischecked=$('#teammember').is(':checked');
	var demoischecked=$('#demo').is(':checked');
	var eventsischecked=$('#events').is(':checked');
	var taskischecked=$('#task').is(':checked');

	if(tmischecked){
		tm=1;	
	}else{
		tm=0;
	}

	if(demoischecked){
		demo=1;	
	}else{
		demo=0;
	}

	if(eventsischecked){
		events=1;
	}else{
		events=0;	
	}

	if(taskischecked){
		task=1;	
	}else{
		task=0;
	}
	var newsource='events_json.php?type=fetchEventsDashBoard&tm='+tm+'&demo='+demo+'&events='+events+'&task='+task;
	//var newsource='events_json.php?type=fetchEventsDashBoard&demo='+demo+'&events='+events+'&task='+task;

	$("#calendar").fullCalendar('removeEventSource',source);
	$('#calendar').fullCalendar('refetchEvents');
	$("#calendar").fullCalendar('addEventSource',newsource); 
	$('#calendar').fullCalendar('refetchEvents');

	source = newsource;	

});


$('#events').click(function(){

	var tmischecked=$('#teammember').is(':checked');
	var demoischecked=$('#demo').is(':checked');
	var eventsischecked=$('#events').is(':checked');
	var taskischecked=$('#task').is(':checked');

	if(tmischecked){
		tm=1;	
	}else{
		tm=0;
	}

	if(demoischecked){
		demo=1;	
	}else{
		demo=0;
	}

	if(eventsischecked){
		events=1;
	}else{
		events=0;	
	}

	if(taskischecked){
		task=1;	
	}else{
		task=0;
	}
	
	var newsource='events_json.php?type=fetchEventsDashBoard&tm='+tm+'&demo='+demo+'&events='+events+'&task='+task;
	//var newsource='events_json.php?type=fetchEventsDashBoard&demo='+demo+'&events='+events+'&task='+task;

	$("#calendar").fullCalendar('removeEventSource',source);
	$('#calendar').fullCalendar('refetchEvents');
	$("#calendar").fullCalendar('addEventSource',newsource); 
	$('#calendar').fullCalendar('refetchEvents');

	source = newsource;

});

$('#task').click(function(){

	var tmischecked=$('#teammember').is(':checked');
	var demoischecked=$('#demo').is(':checked');
	var eventsischecked=$('#events').is(':checked');
	var taskischecked=$('#task').is(':checked');

	if(tmischecked){
		tm=1;	
	}else{
		tm=0;
	}

	if(demoischecked){
		demo=1;	
	}else{
		demo=0;
	}

	if(eventsischecked){
		events=1;
	}else{
		events=0;	
	}

	if(taskischecked){
		task=1;	
	}else{
		task=0;
	}
	
	var newsource='events_json.php?type=fetchEventsDashBoard&tm='+tm+'&demo='+demo+'&events='+events+'&task='+task;
	//var newsource='events_json.php?type=fetchEventsDashBoard&demo='+demo+'&events='+events+'&task='+task;
	
	$("#calendar").fullCalendar('removeEventSource',source);
	$('#calendar').fullCalendar('refetchEvents');
	$("#calendar").fullCalendar('addEventSource',newsource); 
	$('#calendar').fullCalendar('refetchEvents');

	source = newsource;


});

});// End of Document ready 

