$(document).ready(function() {

  var d, date, m, y, dd;
  new CalendarEvents($('#external-events'));
  date = new Date();
  d = date.getDate();
  m = date.getMonth();
  y = date.getFullYear();

  dd = date.getDay();


//Lead Page

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



var eid=getParameterByName('lid');
if(eid=="" || eid==null)
{
	eid=0;
}

  //add lead calendar
  $("#add-lead-calendar").fullCalendar({
	   header: {
	      left: "prev,next",
	      center: "title",
	      right: "month,agendaWeek,agendaDay"
	    },
	    editable: true,
	    droppable: true,
	    defaultView: 'agendaWeek',
	    firstDay:dd,
	    firstHour: 10,
    	    height: 640,
	    defaultEventMinutes:180,
	    slotEventOverlap:true,
	    dayClick: function( date, allDay, jsEvent, view ) {
		//alert(date);
        	//dayClicked(date);
    	    },
	    eventClick: function( event, jsEvent, view ) {
    		
		//console.log(event);
var appttype=event.appttype;
var created_by=event.created_by;
if(created_by=='dealer' && appttype=='Demo'){
bootbox.alert("You Dont have permission to view Your Dealer's Appointment");
return false;
}
if(appttype=="AvailabilityTime" || appttype=="TrainingFor" || appttype=="AvailabilityTime"){
return false;
}
		var dealerid=$('#deal').val();

		var data = {
    			type: 'editEvent',
			title: event.title,
			id:event.id,
			appttype:event.appttype,
			start:event.start,
			end:event.end,
			dealerid:dealerid
		}
    		$.ajax({
	    		type: "POST",
	    		url: "event_functions.php",
	    		data: data,
	    		success: function(output) {
					var obj = JSON.parse(output);
	    				TINY.box.show({
							html:obj.popupHtml,
							width:700,
							openjs:function(){
								$('#edit-event-dialog-table #event-time-from').val(obj.startSecs);
								$('#edit-event-dialog-table #event-time-to').val(obj.endSecs);
								$('#edit-event-dialog-table #edit-event-dialog-start-date').datepicker({
									autoclose:true								
								});
				$('#edit-event-dialog-table #event-time-from').change(function(){
				if($('#edit-event-dialog-type').val()=='AdvancedTrainingClinics'){
			$('#edit-event-dialog-table #event-time-to').val(parseInt($('#edit-event-dialog-table #event-time-from').val() )+ 3600);
			}
else{
	$('#edit-event-dialog-table #event-time-to').val(parseInt($('#edit-event-dialog-table #event-time-from').val()) + 10800);

}
		
								$('#edit-event-dialog-type').val(obj.appttype);
						});

var typeval1=$('#edit-event-dialog-type').val();
	
	if(typeval1=="Demo"){
		$('#trnteam').hide();
	}else if(typeval1=="Training"){
		$('#trnteam').show();
	}else if(typeval1=="AdvancedTrainingClinics"){
		$('#trnteam').hide();
	}

$('#memberslist').select2();
$('#memberslist').addClass('chzn-select');

$('#edit-event-dialog-type').change(function() { 

	typeval=$('#edit-event-dialog-type').val();
	
	if(typeval=="Demo"){
		$('#trnteam').hide();
	}else if(typeval=="Training"){
		$('#trnteam').show();
	}else if(typeval=="AdvancedTrainingClinics"){
		$('#trnteam').hide();
	}

});
								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});



									$('a#save-event-edit').click(function(){
  									window.parent.TINY.box.hide();
									});


		$('a#delete-event-edit').click(function(){
		
			var eventid=$('#event-id-hd').val();
			var eventid1=$('#event-id-hd1').val();
			var dealerid=$('#deal').val();
			bootbox.confirm("Are you sure do you want to delete this Appointment?", function(result) {
		
				 if (result == true) {
					var data = {
						type: 'DeleteEvent',
						eventid: eventid,
						eventid1:eventid1,	
					}
					$.ajax({
	    					type: "POST",
						data:data,
	    					url: "event_functions.php",
	    					success: function(output) {
							var source="event_functions.php?type=fetchEvents&dealeruserid="+dealerid;

				$("#add-lead-calendar").fullCalendar('removeEvents'); 
				$("#add-lead-calendar").fullCalendar('addEventSource',source); 
							window.parent.TINY.box.hide();
						},
						error: function() {
							bootbox.alert('Error while Deleting the Event Details');
						}

					});
				} 
			}); 
					

		});
	
								$('#dsf').click(function(){

var totalSec = $('#edit-event-dialog-table #event-time-from').val();
var hours = parseInt( totalSec / 3600 ) % 24;
var minutes = parseInt( totalSec / 60 ) % 60;
var seconds = totalSec % 60;

var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

var totalSec2 = $('#edit-event-dialog-table #event-time-to').val();
var hours2 = parseInt( totalSec2 / 3600 ) % 24;
var minutes2 = parseInt( totalSec2 / 60 ) % 60;
var seconds2 = totalSec2 % 60;

var result2 = (hours2 < 10 ? "0" + hours2 : hours2) + ":" + (minutes2 < 10 ? "0" + minutes2 : minutes2) + ":" + (seconds2  < 10 ? "0" + seconds2 : seconds2);
var newdate=$('#edit-event-dialog-table #edit-event-dialog-start-date').val();
var newdatesplit=newdate.split("/");
									
									var eventtype = $('#edit-event-dialog-type').val();
									var eventTitle = $('#edit-event-dialog-table #edit-event-dialog-title1').val() + ' - ' + $('#edit-event-dialog-table #edit-event-dialog-title2').val()+ ' - ' + $('#edit-event-dialog-table #edit-event-dialog-title3').val();   
									var startDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result;
									var endDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result2;
									var memberslist=$('#memberslist').val();
									var eventid = $('#edit-event-dialog-table #event-id-hd').val();
						
									var data = {
							    			type: 'editsaveEvent',
										id: eventid,
										title: eventTitle,
										type1: eventtype,
										start:startDate,
										end:endDate,
										memberslist: memberslist
									 }

									$.ajax({
								    		type: "POST",
								    		url: "event_functions.php",
								    		data: data,
								    		success: function(resp) {
										//alert('result = ' + resp);
										$('#add-lead-calendar').fullCalendar( 'refetchEvents');
											window.parent.TINY.box.hide();
									    	},
									    	error: function() {
											alert('error in edit saving');
									    	},
								    	});
	
									//alert("callback function in javascript/calender_functions.js");
									
								});			
							}
						});
		    	},
		    	error: function() {
		    	},
	    	});
	    },
	    eventSources: [{
		url: 'event_functions.php',
		type: 'POST',
		data: {
		  type: 'fetchEvents',
		  editeventid:eid,
		  dealeruserid:$('#deal').val()
		},
		error: function() {
		  bootbox.alert('There was an error while fetching events!');
		},
      	   }],
	   drop: function( date, allDay, jsEvent, ui ) {
    		/*var leadId = $(this).attr('id');*/

function formatAMPM(date) {
	var dateString = '';
var  d = date.getDate();
var month=new Array();
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";
	var mon = month[date.getMonth()]; 
var  y = date.getFullYear();
var h = date.getHours();
var m = date.getMinutes();
var s = date.getSeconds();

if (h < 10) h = '0' + h;
if (m < 10) m = '0' + m;
if (s < 10) s = '0' + s;

dateString =d+'-'+mon+'-'+y+' '+ h + ':' + m + ':' + s;
return dateString;
	}
	
		var leadId = eid;
		var dealerid=$('#deal').val();

//alert("Event Drop : "+leadId+" ; Dealer : "+dealerid);

    		var eventTitle = $(this).html();
    		var startDate = formatAMPM(date);
    		var NameAddr = eventTitle.split(" - ");
    		
		var data={
				type: 'check',
				leadId:leadId,
					}
		$.ajax({
				   type: "POST",
				   url: "update_appt_status.php",
				   data: data,
				   success: function(resp) {
						
				if(resp!=0)
				{
					bootbox.alert(resp);
					return false;
				}
		var data = {
    			type: 'update_appt_status',
			
			 }
    		$.ajax({
	    		type: "POST",
	    		url: "update_appt_status.php",
	    		data: data,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:450,
							height:400,
							openjs:function(){
								$('#mtype').change(function(){
var val=$('#mtype option:selected').attr("class")+" showsubtype";
$('#msubtype .showsubtype').css({display:'none'});
$('#msubtype .showsubtype').removeAttr("selected");
$('#msubtype option[class="'+val+'"]').css({'display':'block'});
});
								$('#status').change(function(){

							var a=$('#status').val();
							//$("#apptresult option").removeAttr("selected","selected");
							 $("#result option").prop('selected', false);

							if(a=='Cancelled'){
							 $("#result option[value='Cancelled Appt']").prop('selected', true);
							}
							if(a=='Call To Reset'){
							 $("#result option[value='Reset']").prop('selected', true);
							}
							if(a=='Rescheduled'){
							 $("#result option[value='Pending']").prop('selected', true);
							}
							if(a=='Confirmed'){
							 $("#result option[value='Pending']").prop('selected', true);
							}
							if(a=='Needs Confirmed'){
							 $("#result option[value='Pending']").prop('selected', true);
							}
							if(a=='OK to Go'){
							 $("#result option[value='Pending']").prop('selected', true);
							}


						});

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	
								$('a#save').click(function(){
									var status=$('#status').val();
									var result=$('#result').val();
									var lstatus=$('#mtype').val();
									var lresult=$('#msubtype').val();
									var setby=$('#user').val();
									var time=$('#time').val();
									var setby_uid=$('#setby_uid').val();

									var activity=$('#activity').val();
									var contactreason=$('#contactreason').val();

									if(status=='')
								{
							$('.msg').css({'display':'block'});
							$('.msg').html('Pls choose Appointment Status');
							return false;
								}
							if(result=='')
								{
								result='Pending';
								}
								var data={
									type: 'update',
									leadId:leadId,
									start:startDate,
									status:status,
									result:result,
									lstatus:lstatus,
									lresult:lresult,
									activity:activity,
									contactreason:contactreason
									
										}
									$.ajax({
									    		type: "POST",
									    		url: "update_appt_status.php",
									    		data: data,
									    		success: function(resp) {
											
											if(resp!=0)
											{
												bootbox.alert(resp);
												return false;
											}
									var data = {
						    			type: 'addEvent',
									title: eventTitle,
									leadId:leadId,
									allDay:allDay,
									start:startDate,
									status:status,
									result:result,
									dealerid:dealerid
										}
									$.ajax({
									    		type: "POST",
									    		url: "event_functions.php",
									    		data: data,
									    		success: function(resp) {
											
											var numeric=$.isNumeric( resp );
											
							if(numeric==true){
									var id=resp;
								$('#lid').val(id);
							bootbox.alert("Appointment has been created");
							}
						else{
								bootbox.alert(resp);
							}
									
						var source="event_functions.php?type=fetchEvents&dealeruserid="+dealerid;
						$("#add-lead-calendar").fullCalendar('removeEvents'); 
						$("#add-lead-calendar").fullCalendar('addEventSource',source); 

							//$('#add-lead-calendar').fullCalendar( 'refetchEvents');
							window.parent.TINY.box.hide();

							$('#apptstatus option[value="'+status+'"]').prop('selected', true);
							$('#apptresult option[value="'+result+'"]').prop('selected', true);	
							if(lstatus!=''){
							$('#lstatus option[value="'+lstatus+'"]').prop('selected', true);
							}
							if(lresult!=''){
							$('#lres option[value="'+lresult+'"]').prop('selected', true);
							}

							$('#sel_setby option[value="'+setby_uid+'"]').prop('selected', true);
							$('#sel_setby').append("<option value="+setby_uid+" selected >"+setby+"</option>");
							$('#setby1').val(setby_uid);
							$('#setby').val(setby);
							$('#setdate').val(time);
							if(status=='Demo' && result=='Sale' || result=='Used RB Sale'){
							$("#icheck1").prop('checked',true);
							}
							else{
							$("#icheck1").prop('checked',false);
							}
										    	},
										    	error: function() {
												alert('error in saving');
										    	},
									    	});
									//alert("callback function in javascript/calender_functions.js");
										}
							});										

								});			
							}
						});
}
						});
		    	},
		    	error: function() {
		    	},
	    	});
		    	},
		
    		
    		//alert("Dropped on " + date + " with allDay=" + allDay);

eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {

var appttype=event.appttype;
var created_by=event.created_by;
if(created_by=='dealer' && appttype=='Demo'){
bootbox.alert("You Dont have permission to Edit Your Dealer's Appointment");
return false;
}
if(appttype=="AvailabilityTime" || appttype=="TrainingFor" ){
return false;
}
function formatAMPM(date) {
	var dateString = '';
var  d = date.getDate();
var month=new Array();
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";
	var mon = month[date.getMonth()]; 
var  y = date.getFullYear();
var h = date.getHours();
var m = date.getMinutes();
var s = date.getSeconds();

if (h < 10) h = '0' + h;
if (m < 10) m = '0' + m;
if (s < 10) s = '0' + s;

dateString =d+'-'+mon+'-'+y+' '+ h + ':' + m + ':' + s;
return dateString;
	}
	
	var start= event.start;

	var starttime=formatAMPM(start);


		var data = {
    			type: 'editlead',
			start:starttime,
			id:event.id,
			 }
    		$.ajax({

	    		type: "POST",
	    		url: "rescheduled_appointment.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:900,
							height:650,
							openjs:function(){

							$('#resch').addClass('tabRS');

							$('#startdate').datetimepicker({
								format:'m/d/Y',
								lang:'en',
								validateOnBlur:false,
								timepicker:false,
							});

							$('#starttime').datetimepicker({
								format:'h:i A',
								lang:'en',
								validateOnBlur:false,
								formatTime:'h:i A',
		allowTimes:[
'12:30 AM', '01:30 AM', '02:00 AM', '02:30 AM', '03:00 AM', '03:30 AM', '04:00 AM', '04:30 AM', '05:00 AM', '05:30 AM','06:00 AM', '06:30 AM', '07:00 AM', '07:30 AM', '08:00 AM', '08:30 AM','09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM','02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM','07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM','11:30 PM', '12:00 AM'
			 ],
								datepicker:false
							});
							
							$('#datepicker').datepicker({
								autoclose:true,
								validateOnBlur:false,
																										
								});
                                                           
             						$('#datepicker1').datepicker({
								autoclose:true,
								validateOnBlur:false,							
								});
							

$("#status option[value='Rescheduled']").prop('selected', true);
 $("#result option[value='Reset']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").show();
 $("#ctrnote").hide();

$('#create_task').click(function(){
var s=$('#status').val();
if(s=='Call To Reset'){

window.location.href ='createtask.php?prefill=true';
}
else
{
window.location.href ='createtask.php';
}
});


$('#status').change(function(){
var a=$('#status').val();

$("#cmt_tr").show();
$("#ntcnote").hide();
$("#resch").hide();
$("#ctrnote").hide();

//$("#apptresult option").removeAttr("selected","selected");
 $("#result option").prop('selected', false);

if(a=='Cancelled'){
 $("#result option[value='Cancelled Appt']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").hide();
}
if(a=='Call To Reset'){
 $("#result option[value='Reset']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").show();
}
if(a=='Rescheduled'){
 $("#result option[value='Reset']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").show();
 $("#ctrnote").hide();
}
if(a=='Confirmed'){
 $("#result option[value='Pending']").prop('selected', true);
 $("#ntcnote").hide();
 $("#ctrnote").hide();
}
if(a=='Needs Confirmed'){
 $("#result option[value='Pending']").prop('selected', true);
 $("#ntcnote").show();
 $("#resch").hide();
 $("#ctrnote").hide();
}
if(a=='OK to Go'){
 $("#result option[value='Pending']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").hide();
}

if(a==''){
	$("#cmt_tr").hide();
}


});

$('.delete').click(function(){
var agree=confirm("Are you sure you want to delete this status update?");
	if (agree)
		return true ;
	else
		return false ;

});

$('a#recruite').click(function(){
var x=$('#id').val();
var y=$('#l_id').val();
//window.parent.TINY.box.hide();
var data = {
    			type: 'suggession',
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url: "suggession.php",
	    		data: data,
	    		success: function(output) {
				var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:250,
							openjs:function(){

						$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	

							$('a#save-event-edit').click(function(){


							
							var leadid=$('#ld').val();
			if($('.take').is(':checked')) {
					var selectedRadio = $("input[name='take']:checked").val();
	
				 }
				else{
					$('.msg').css({'display':'block'});
					$('.msg').html('Pls choose Any One Option');
					return false;
				}	




					var data = {
								    	type: 'redirect',
									leadid:leadid,
									take:selectedRadio
								 }
								 
				 $.ajax({
					type: "POST",
					url: "suggession.php",
					data: data,
					success: function(resp) {
		        		window.location.href = resp;
					//window.parent.TINY.box.hide();
								}

									});
								});
						}    
					});
				}
		}); 

});	
		$('a#cancel-event-edit').click(function(){
			window.parent.TINY.box.hide();
		});	

		$('a#save-event-edit').click(function(){
					var x=$('#id').val();
					var y=$('#l_id').val();
					var appt_status = $('#status').val()
					if(appt_status=='')
					{
					$('.msg').css({'display':'block'});
					$('.msg').html('Pls choose Appointment Status');
					return false ;
					}
					var appt_result = $('#result').val();
					var dealer=$('#dealer').val();
					var aptdatetime=$('#apptdatetime').val();
					var comments=$("#cmts").val();
					var startdate=$("#startdate").val();							
					var starttime=$("#starttime").val();
					var resetreason="";

					if(appt_status=="Rescheduled")
					{
						resetreason=$("#rresetreason").val();
					}else{
						resetreason=$("#cresetreason").val();
					}

					if(appt_status=="Rescheduled"){

						if(startdate!="" && starttime!="")
						{
							var data = {
						    	type: 'editSaveEvent',
							id: x,
							leadid:y,
							status: appt_status,
							result: appt_result,
							dealer:dealer,
							aptdatetime:aptdatetime,
							comments:comments,
							startdate:startdate,
							starttime:starttime,
							resetreason:resetreason
						 }
						 
						 $.ajax({
						    		type: "POST",
						    		url: "rescheduled_appointment.php",
						    		data: data,
						    		success: function(resp) {
								
								//$('#add-lead-calendar').fullCalendar('refetchEvents');
								var source="event_functions.php?type=fetchEvents&dealeruserid="+dealer;

								$("#add-lead-calendar").fullCalendar('removeEvents'); 
								$("#add-lead-calendar").fullCalendar('addEventSource',source); 
								bootbox.alert(resp);
									window.parent.TINY.box.hide();
								}

							});
						}else{
							bootbox.alert('Please Select Rescheduled Appointment Date and Time!');
						}
					}else{
						var data = {
						    	type: 'editSaveEvent',
							id: x,
							leadid:y,
							status: appt_status,
							result: appt_result,
							dealer:dealer,
							aptdatetime:aptdatetime,
							comments:comments,
							startdate:startdate,
							starttime:starttime,
							resetreason:resetreason
						 }
						 
						 $.ajax({
						    		type: "POST",
						    		url: "rescheduled_appointment.php",
						    		data: data,
						    		success: function(resp) {
									//$('#add-lead-calendar').fullCalendar('refetchEvents');
			var source="event_functions.php?type=fetchEvents&dealeruserid="+dealer;

				$("#add-lead-calendar").fullCalendar('removeEvents'); 
				$("#add-lead-calendar").fullCalendar('addEventSource',source); 
									bootbox.alert(resp);
									window.parent.TINY.box.hide();
								}

							});
					 }
				});									
			}				    	

		});
	    }
            

        });
/*
		var eventTitle = event.title;
    		var startDate = event.start;
    		var endDate = event.end;
		
		var data = {
    			type: 'saveEvent',
			id:event.id,
			title: eventTitle,
			allDay:allDay,
			start:startDate,
			end:endDate }
			$.ajax({
		    		type: "POST",
		    		url: "event_functions.php",
		    		data: data,
		    		success: function(resp) {
					//alert('success msg');
					//alert('result = ' + resp);
	    			
			    	},
			    	error: function() {
			    	},
		    	});
		/*

		if (!confirm("Are you sure about this change?")) {
		    revertFunc();
		}
		*/

	    },
	eventResize: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
		var eventTitle = event.title;
    		var startDate = event.start;
    		var endDate = event.end;
		
		var data = {
    			type: 'saveEvent',
			id:event.id,
			title: eventTitle,
			allDay:allDay,
			start:startDate,
			end:endDate 
		}
			$.ajax({
		    		type: "POST",
		    		url: "event_functions.php",
		    		data: data,
		    		success: function(resp) {
					//alert('success msg');
					//alert('result = ' + resp);
	    			
			    	},
			    	error: function() {
			    	},
		    	});
	    },
	annotations: [

		{ start: new Date(y, m, d-1, 0, 0), end: new Date(y, m, d-1, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-2, 0, 0), end: new Date(y, m, d-2, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-3, 0, 0), end: new Date(y, m, d-3, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-4, 0, 0), end: new Date(y, m, d-4, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-5, 0, 0), end: new Date(y, m, d-5, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-6, 0, 0), end: new Date(y, m, d-6, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-7, 0, 0), end: new Date(y, m, d-7, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-8, 0, 0), end: new Date(y, m, d-8, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-9, 0, 0), end: new Date(y, m, d-9, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-10, 0, 0), end: new Date(y, m, d-10, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-11, 0, 0), end: new Date(y, m, d-11, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-12, 0, 0), end: new Date(y, m, d-12, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-13, 0, 0), end: new Date(y, m, d-13, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-14, 0, 0), end: new Date(y, m, d-14, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-15, 0, 0), end: new Date(y, m, d-15, 10, 00),title: '', cls: 'open', background: '#eeeef0' },

		{ start: new Date(y, m, d, 0, 0), end: new Date(y, m, d, 10, 00),title: '', cls: 'open', background: '#eeeef0' },

		{ start: new Date(y, m, d+1, 0, 0), end: new Date(y, m, d+1, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+2, 0, 0), end: new Date(y, m, d+2, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+3, 0, 0), end: new Date(y, m, d+3, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+4, 0, 0), end: new Date(y, m, d+4, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+5, 0, 0), end: new Date(y, m, d+5, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+6, 0, 0), end: new Date(y, m, d+6, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+7, 0, 0), end: new Date(y, m, d+7, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+8, 0, 0), end: new Date(y, m, d+8, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+9, 0, 0), end: new Date(y, m, d+9, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+10, 0, 0), end: new Date(y, m, d+10, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+11, 0, 0), end: new Date(y, m, d+11, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+12, 0, 0), end: new Date(y, m, d+12, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+13, 0, 0), end: new Date(y, m, d+13, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+14, 0, 0), end: new Date(y, m, d+14, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+15, 0, 0), end: new Date(y, m, d+15, 10, 00),title: '', cls: 'open', background: '#eeeef0' },



		{ start: new Date(y, m, d-1, 23, 0),end: new Date(y, m, d-1, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-2, 23, 0),end: new Date(y, m, d-2, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-3, 23, 0),end: new Date(y, m, d-3, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-4, 23, 0),end: new Date(y, m, d-4, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-5, 23, 0),end: new Date(y, m, d-5, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-6, 23, 0),end: new Date(y, m, d-6, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-7, 23, 0),end: new Date(y, m, d-7, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-8, 23, 0),end: new Date(y, m, d-8, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-9, 23, 0),end: new Date(y, m, d-9, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-10, 23, 0),end: new Date(y, m, d-10, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-11, 23, 0),end: new Date(y, m, d-11, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-12, 23, 0),end: new Date(y, m, d-12, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-13, 23, 0),end: new Date(y, m, d-13, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-14, 23, 0),end: new Date(y, m, d-14, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-15, 23, 0),end: new Date(y, m, d-15, 23, 59),title: '', cls: 'open', background: '#eeeef0'},

		{ start: new Date(y, m, d, 23, 0),end: new Date(y, m, d, 23, 59),title: '', cls: 'open', background: '#eeeef0'},

		{ start: new Date(y, m, d+1, 23, 0),end: new Date(y, m, d+1, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+2, 23, 0),end: new Date(y, m, d+2, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+3, 23, 0),end: new Date(y, m, d+3, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+4, 23, 0),end: new Date(y, m, d+4, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+5, 23, 0),end: new Date(y, m, d+5, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+6, 23, 0),end: new Date(y, m, d+6, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+7, 23, 0),end: new Date(y, m, d+7, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+8, 23, 0),end: new Date(y, m, d+8, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+9, 23, 0),end: new Date(y, m, d+9, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+10, 23, 0),end: new Date(y, m, d+10, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+11, 23, 0),end: new Date(y, m, d+11, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+12, 23, 0),end: new Date(y, m, d+12, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+13, 23, 0),end: new Date(y, m, d+13, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+14, 23, 0),end: new Date(y, m, d+14, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+15, 23, 0),end: new Date(y, m, d+15, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		
	]	

  });


//Dealers Calenders

var JsonEvents=[{"id":"","tseqid":"","title":"","start":"","end":"","allDay":0,"address":"","appttype":"","backgroundColor":"","borderColor":"","textColor":""}];

$('#deal').change(function(){

	dealerid=$('#deal').val();

	
	var source="event_functions.php?type=fetchEvents&dealeruserid="+dealerid;

	/*$("#dealercalendar").fullCalendar('removeEvents'); 
	$("#dealercalendar").fullCalendar('addEventSource',source); */

	$("#add-lead-calendar").fullCalendar('removeEvents'); 
	$("#add-lead-calendar").fullCalendar('addEventSource',source); 


});

$('#dealercalendar').fullCalendar({
             header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                    },
                defaultView: 'agendaWeek',
		minTime:10,
		maxTime:23,
		firstDay:dd,
		aspectRatio:1,
		slotEventOverlap:false,
		events: JsonEvents
		
		

  }); // dealer calender



$('#dealer_availability').fullCalendar({
             header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                    },
                defaultView: 'agendaWeek',
		firstDay:dd,
		firstHour: 10,
    	        height: 640,
		aspectRatio:1,
		slotEventOverlap:false,
		events: JsonEvents,
		slotEventOverlap:false,
		selectable: true,
		selectHelper: true,
		
		select: function(start, end, allDay, jsEvent, view) {

	function formatAMPM(date) {
	var dateString = '';
var  d = date.getDate();
var month=new Array();
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";
	var mon = month[date.getMonth()]; 
var  y = date.getFullYear();
var h = date.getHours();
var m = date.getMinutes();
var s = date.getSeconds();

if (h < 10) h = '0' + h;
if (m < 10) m = '0' + m;
if (s < 10) s = '0' + s;

dateString =d+'-'+mon+'-'+y+' '+ h + ':' + m + ':' + s;
return dateString;
	}
	
	

	var starttime=formatAMPM(start);
	var endtime=formatAMPM(end);

var data = {
    			type: 'available_for_demo',
			start:starttime,
			end:endtime,	
		}
    		$.ajax({
	    		type: "POST",
	    		url: "deal_avail.php",
	    		data: data,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:300,
							openjs:function(){
			
							$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});
								$('a#save-event-edit').click(function(){
									
									var data = {
							    			type: 'saveavailabilitytime',
										count:$('#count').val(),
										start:starttime,
										end:endtime,	
										}
										$.ajax({
									    		type: "POST",
									    		url: "deal_avail.php",
									    		data: data,
									    		success: function(resp) {
									bootbox.alert('Your Availability Time has been added');
												//alert('success msg');
												//alert('result = ' + resp);
									$('#dealer_availability').fullCalendar( 'refetchEvents');
												window.parent.TINY.box.hide();

										    	},
										    	error: function() {
												alert('error in saving');
										    	},
									    	});
									
								});			
							}
						});
		    	},
		    	error: function() {
		    	},
	    	});


	},
eventClick: function(calEvent, jsEvent, view) {

  var data = {
    			type: 'delete',
			id:calEvent.id,
		}
    		$.ajax({
	    		type: "POST",
	    		url: "deal_avail.php",
	    		data: data,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:300,
							openjs:function(){
			
							$('a#cancel').click(function(){
									window.parent.TINY.box.hide();
								});


						$('#avail_date').datetimepicker({
										format:'M d Y',
										lang:'en',
										timepicker:false,
										validateOnBlur:false,	
										});
							jQuery('#avail_time,#end').datetimepicker({
										 format:'h:i A',
					formatTime:'h:i A',
					lang:'en',
					datepicker:false,
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
										});
var date=$('#avail_date').val();
var stime=$('#avail_time').val();
var etime=$('#end').val();
if(date=='' || stime=='' || etime==''){
return false;
}
								$('a#update-event-edit').click(function(){

								var data = {
							    			type: 'update_time',
										id:calEvent.id,
										avail_date:$('#avail_date').val(),
										start:$('#avail_time').val(),
										end:$('#end').val(),
										count:$('#count').val(),
										}
										$.ajax({
									    		type: "POST",
									    		url: "deal_avail.php",
									    		data: data,
									    		success: function(resp) {
									bootbox.alert('Your Availability Time has been Updated');
												//bootbox.alert(resp);
												//alert('result = ' + resp);
									$('#dealer_availability').fullCalendar( 'refetchEvents');
												window.parent.TINY.box.hide();

										    	},
										    	error: function() {
												alert('error in deleting');
										    	},
									    	});

									});



								$('a#delete').click(function(){
				bootbox.confirm("Are you sure to delete?", function(result) {	
								if(result==true){	
									var data = {
							    			type: 'delete_time',
										id:calEvent.id,
										}
										$.ajax({
									    		type: "POST",
									    		url: "deal_avail.php",
									    		data: data,
									    		success: function(resp) {
									bootbox.alert('Your Availability Time has been deleted');
												//alert('success msg');
												//alert('result = ' + resp);
									$('#dealer_availability').fullCalendar( 'refetchEvents');
												window.parent.TINY.box.hide();

										    	},
										    	error: function() {
												alert('error in deleting');
										    	},
									    	});
	}		});
								});
											
							}
						});
		    	},
		    	error: function() {
		    	},
	    	});
      
       

    },
annotations: [

		{ start: new Date(y, m, d-1, 0, 0), end: new Date(y, m, d-1, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-2, 0, 0), end: new Date(y, m, d-2, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-3, 0, 0), end: new Date(y, m, d-3, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-4, 0, 0), end: new Date(y, m, d-4, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-5, 0, 0), end: new Date(y, m, d-5, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-6, 0, 0), end: new Date(y, m, d-6, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-7, 0, 0), end: new Date(y, m, d-7, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-8, 0, 0), end: new Date(y, m, d-8, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-9, 0, 0), end: new Date(y, m, d-9, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-10, 0, 0), end: new Date(y, m, d-10, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-11, 0, 0), end: new Date(y, m, d-11, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-12, 0, 0), end: new Date(y, m, d-12, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-13, 0, 0), end: new Date(y, m, d-13, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-14, 0, 0), end: new Date(y, m, d-14, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d-15, 0, 0), end: new Date(y, m, d-15, 10, 00),title: '', cls: 'open', background: '#eeeef0' },

		{ start: new Date(y, m, d, 0, 0), end: new Date(y, m, d, 10, 00),title: '', cls: 'open', background: '#eeeef0' },

		{ start: new Date(y, m, d+1, 0, 0), end: new Date(y, m, d+1, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+2, 0, 0), end: new Date(y, m, d+2, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+3, 0, 0), end: new Date(y, m, d+3, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+4, 0, 0), end: new Date(y, m, d+4, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+5, 0, 0), end: new Date(y, m, d+5, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+6, 0, 0), end: new Date(y, m, d+6, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+7, 0, 0), end: new Date(y, m, d+7, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+8, 0, 0), end: new Date(y, m, d+8, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+9, 0, 0), end: new Date(y, m, d+9, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+10, 0, 0), end: new Date(y, m, d+10, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+11, 0, 0), end: new Date(y, m, d+11, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+12, 0, 0), end: new Date(y, m, d+12, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+13, 0, 0), end: new Date(y, m, d+13, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+14, 0, 0), end: new Date(y, m, d+14, 10, 00),title: '', cls: 'open', background: '#eeeef0' },
		{ start: new Date(y, m, d+15, 0, 0), end: new Date(y, m, d+15, 10, 00),title: '', cls: 'open', background: '#eeeef0' },



		{ start: new Date(y, m, d-1, 23, 0),end: new Date(y, m, d-1, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-2, 23, 0),end: new Date(y, m, d-2, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-3, 23, 0),end: new Date(y, m, d-3, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-4, 23, 0),end: new Date(y, m, d-4, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-5, 23, 0),end: new Date(y, m, d-5, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-6, 23, 0),end: new Date(y, m, d-6, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-7, 23, 0),end: new Date(y, m, d-7, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-8, 23, 0),end: new Date(y, m, d-8, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-9, 23, 0),end: new Date(y, m, d-9, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-10, 23, 0),end: new Date(y, m, d-10, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-11, 23, 0),end: new Date(y, m, d-11, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-12, 23, 0),end: new Date(y, m, d-12, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-13, 23, 0),end: new Date(y, m, d-13, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-14, 23, 0),end: new Date(y, m, d-14, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d-15, 23, 0),end: new Date(y, m, d-15, 23, 59),title: '', cls: 'open', background: '#eeeef0'},

		{ start: new Date(y, m, d, 23, 0),end: new Date(y, m, d, 23, 59),title: '', cls: 'open', background: '#eeeef0'},

		{ start: new Date(y, m, d+1, 23, 0),end: new Date(y, m, d+1, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+2, 23, 0),end: new Date(y, m, d+2, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+3, 23, 0),end: new Date(y, m, d+3, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+4, 23, 0),end: new Date(y, m, d+4, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+5, 23, 0),end: new Date(y, m, d+5, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+6, 23, 0),end: new Date(y, m, d+6, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+7, 23, 0),end: new Date(y, m, d+7, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+8, 23, 0),end: new Date(y, m, d+8, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+9, 23, 0),end: new Date(y, m, d+9, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+10, 23, 0),end: new Date(y, m, d+10, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+11, 23, 0),end: new Date(y, m, d+11, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+12, 23, 0),end: new Date(y, m, d+12, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+13, 23, 0),end: new Date(y, m, d+13, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+14, 23, 0),end: new Date(y, m, d+14, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		{ start: new Date(y, m, d+15, 23, 0),end: new Date(y, m, d+15, 23, 59),title: '', cls: 'open', background: '#eeeef0'},
		
	]

  });

$('#copy').click(function(){
var d=$('#dealer_availability').fullCalendar( 'getDate');
		var data = {
			type: 'copy',
			date:d
		}
			$.ajax({
			type: "POST",
			url: "deal_avail.php",
			data: data,
			success: function(resp) {
			bootbox.alert('Your Availability Time has been Copied from previous week');
			$('#dealer_availability').fullCalendar( 'refetchEvents');
			window.parent.TINY.box.hide();
				},
			error: function() {
			alert('error in copying');
			},
			});
});
    
var JsonEvents=[{"id":"","tseqid":"","title":"","start":"","end":"","allDay":0,"address":"","appttype":"","backgroundColor":"","borderColor":"","textColor":""}];

	var source="deal_avail.php?type=fetchDealerEvents";

	$("#dealer_availability").fullCalendar('removeEvents'); 
	$("#dealer_availability").fullCalendar('addEventSource',source); 


$('#test11').click(function() {
	var date=new Date();

   	 $('#calendar').fullCalendar('gotoDate', date );
});



});// document ready
