<script type="text/javascript" src="javascripts/bootbox.min.js"></script>
<link href="stylesheets/dtable.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/jquery.base64.js"></script>
<script src="javascripts/jquery.btechco.excelexport.js"></script>

<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>

<script type="text/javascript">

function viewaddr_gmaps(address,leadid)
{
	var data={
		type:'vwleadaddr',
		addr:address,
		leadid:leadid
	}
	$.ajax({
		type: "POST",
		url: "view_gmap_addr.php",
		data: data,
		success: function(output)
		{
			var obj=JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:700,
				height:570,
				openjs:function(){

				}
				
			});
		
		}

	});
}

  function loadAjaxContent(x,y)
    { 

var data = {
    			type: 'editlead',
			id:x,
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "edit_leadstatus.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:550,
							openjs:function(){
							

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	



								

							$('a#save-event-edit').click(function(){

							var lead_status = $('#status').val();
							
							var data = {
							    	type: 'editSaveEvent',
								id: x,
								leadid:y,
								status: lead_status,
								
							
							 }


								$.ajax({
								    		type: "POST",
								    		url: "edit_leadstatus.php",
								    		data: data,
								    		success: function(resp) {
                                                                                window.location.href = 'leadcard.php';
									        window.parent.TINY.box.hide();
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
    }
function confirmDelete() 
{
	var agree=confirm("Are You Sure To Delete This Referral? ");
	if (agree)
		return true ;
	else
		return false ;
}

var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var curdate = ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day + '' + d.getFullYear();


$(document).ready(function() {


$('#click').dataTable(
    {
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
"sPaginationType": "full_numbers",
 "bDestroy": true
    }

);

	var fn="LeadLists_"+curdate;

	 $("#btnExport").click(function () {
            $("#click").btechco_excelexport({
                containerid: "click",
                datatype: $datatype.Table,
		filename: fn
            });
        });


	var lid=getParameterByName('lid');
	var ls=getParameterByName('ls');
	var lseqid=getParameterByName('leadseqid');
	var nxtapt=getParameterByName('nxtapt');

	if(lid!=="" && ls!="" && nxtapt!="")
	{
	if(lid!=null || ls!=null || nxtapt!=null )
	{
		
		if(nxtapt!="no")
		{
		   var txt=" <br>Your Survey Saved with Lead Status "+ls+" Successfully! <br><br> The Call Date will be on <b>"+nxtapt+"</b>";
		   //bootbox.alert(txt);
			$.ajax({
	    		type: "POST",
	    		url: "nxtaptstatus.php?lid="+lid+"&leadseqid="+lseqid+"&ls="+ls+"&nxtapt="+nxtapt,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:350,
							openjs:function(){
								
							   $('#apptdate').datepicker({
									autoclose:true
							   });

							   var curr_time = document.getElementById("appttimefrom").value;
							   var to_time = parseInt(curr_time) + parseInt("10800");
							   document.getElementById("appttimeto").value = to_time;
	
							   $('#appttimefrom').change(function (){
								var curr_time = document.getElementById("appttimefrom").value;
								var to_time = parseInt(curr_time) + parseInt("10800");
								if ( parseInt(to_time) > parseInt("84600") ) {
									 to_time = "84600";
								}
								document.getElementById("appttimeto").value = to_time;
							    });

								$('#cancel').click(function(){
									window.parent.TINY.box.hide();
								});

							    $('#update').click(function(){

								var apptdate=$('#apptdate').val();
								var apptfrm=$('#appttimefrom').val();
								var apptto=$('#appttimeto').val();
								var leadseqid=$('#leadseqid').val();
								var leadid=$('#leadid').val();
								var aptstatus=$('#aptstatus').val();
								var contactreason=$('#contactreason').val();

								var data = {
    									type: 'updEvent',
									apptdate: apptdate,
									apptfrm:apptfrm,
									apptto:apptto,
									leadseqid:leadseqid,
									leadid:leadid,
									aptstatus:aptstatus,
									contactreason:contactreason
								}
								$.ajax({
	    								type: "POST",
	    								url: "nxtaptstatus.php",
	    								data: data,
	    								success: function(output) {
							
							var txt=" <br>Survey Scheduled as <b> Task on "+output+"</b> Successfully! <br>";
		  				        bootbox.alert(txt);
							 window.location.href = 'leadcard.php';
							window.parent.TINY.box.hide();
									}
								});

							    });
							}
					    });
				}
			});
		}else{
		   var txt=" <br>Your Survey Saved with Lead Status "+ls+" Successfully! <br>";
		   bootbox.alert(txt);
		}
	}
	}

var oTable = $('#click').dataTable();
   // Get the nodes from the table
 var nNodes = oTable.fnGetNodes( );

$(nNodes).hover(
  function () {
    $(this).css("background","#3E93BD");
    $(this).css("color","#FFFFFF");
  }, 
  function () {
    $(this).css("background","");
    $(this).css("color","#756A71");
  }
);

   
$(nNodes).click(
  function () {
	$('#name2').html("Lead Name :  " +$(this).closest('tr').children('td:eq(3)').text());
	$('#addr').html("Lead Address :  " +$(this).closest('tr').children('td:eq(4)').text());
	$('#phone').html("Phone Num :  " +$(this).closest('tr').children('td:eq(5)').text());
	$('#email').html("Email Id :  " +$(this).closest('tr').children('td:eq(6)').text());
	if($(this).closest('tr').children('td:eq(8)').text()!='')
	$('#timecont').html("Best Time to contact :  " + $(this).closest('tr').children('td:eq(7)').text());
	if($(this).closest('tr').children('td:eq(22)').text()!='')
	$('#lstatus').html("Lead Status :  " + $(this).closest('tr').children('td:eq(22)').text());
	if($(this).closest('tr').children('td:eq(9)').text()!='')
	$('#ltype').html("Lead Type :  " + $(this).closest('tr').children('td:eq(9)').text());
	if($(this).closest('tr').children('td:eq(10)').text()!='')
	$('#lsubtype').html("Lead subType :  " + $(this).closest('tr').children('td:eq(10)').text());
	if($(this).closest('tr').children('td:eq(11)').text()!='')
	$('#refby').html("Refferred By :  " + $(this).closest('tr').children('td:eq(11)').text());
	if($(this).closest('tr').children('td:eq(12)').text()!='')
	$('#ldeal').html("Lead Dealer :  " + $(this).closest('tr').children('td:eq(12)').text());
	if($(this).closest('tr').children('td:eq(8)').text()!='')
	$('#lin').html("Lead In :  " + $(this).closest('tr').children('td:eq(8)').text());
	if($(this).closest('tr').children('td:eq(16)').text()!='')
	$('#lres').html("Lead Result :  " + $(this).closest('tr').children('td:eq(16)').text());
	if($(this).closest('tr').children('td:eq(17)').text()!='-')
	$('#com').html("Appointment Time:  " + $(this).closest('tr').children('td:eq(17)').text());
	if($(this).closest('tr').children('td:eq(18)').text()!='')
	$('#type').html("Appointment Type:  " + $(this).closest('tr').children('td:eq(18)').text());
	
  }
);

});
$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
  
</script>
