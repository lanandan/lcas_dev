<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>

<link href="stylesheets/multiple-select.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/jquery.multiple.select.js"></script> 

<script src="js/jquery.uploadifive.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/uploadifive.css">

<style type="text/css">

.uploadifive-button {
	float: left;
	margin-right: 10px;
}
#queue {
	border: 1px solid #E5E5E5;
	height: 70px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 60%;
}
</style>

<script type="text/javascript">

var dispappt=0;

$(document).ready(function() {

$('#file').uploadifive({
	'auto'             : true,
	'queueID'          : 'queue',
	'queueSizeLimit'   : 5,
	'uploadLimit'      : 5,
	'uploadScript'     : 'uploadifive.php?job=add&tabname=evnt',
	'onProgress'   	   : function(file, e) {
			    if (e.lengthComputable) {
				var percent = Math.round((e.loaded / e.total) * 100);
			    }
			    file.queueItem.find('.fileinfo').html(' - ' + percent + '%');
			    file.queueItem.find('.progress-bar').css('width', percent + '%');
        }, 
	'onUploadComplete' : function(file, data) {

				var d1=data.trim();
				$('#output').append(d1+',');

				document.getElementById('upimgids').value = $('#output').html();

				file.queueItem.find('.close').html(data);
				
			}
	
});


$("#uniform-file").removeClass("uploader");
$(".filename").text("");
$(".action").text("");

					$('#memberslist1').multipleSelect();
	
					/*$('#memberslist1').select2();
					$('#memberslist1').addClass('chzn-select');*/
				
					$('#training-time-from').change(function(){

					var st=$('#training-time-from').val();
					
					var time = $("#training-time-from").val();
					var hours = Number(time.match(/^(\d+)/)[1]);
					var minutes = Number(time.match(/:(\d+)/)[1]);
					var AMPM = time.match(/\s(.*)$/)[1];
					if(AMPM == "PM" && hours<12) hours = hours+12;
					if(AMPM == "AM" && hours==12) hours = hours-12;
					var sHours = hours.toString();
					var sMinutes = minutes.toString();
					if(hours<10) sHours = "0" + sHours;
					if(minutes<10) sMinutes = "0" + sMinutes;

					//var stt=sHours + ":" + sMinutes;

					var stt = new Date('2014','04','11',sHours,sMinutes)

					function formatAMPMforEndTime(date) {
					  var hours = date.getHours()+1;
					  var minutes = date.getMinutes();
					  var ampm = hours >= 12 ? 'PM' : 'AM';
					  hours = hours % 12;
					  hours = hours ? hours : 12; // the hour '0' should be '12'
					  minutes = minutes < 10 ? '0'+minutes : minutes;
					  var strTime = hours + ':' + minutes + ' ' + ampm;
					  return strTime;
					}

					var ed=formatAMPMforEndTime(stt);
					$('#training-time-to').val(ed);

					//alert("Start : "+st+" ; End : "+ed);


				});

					$('#edit-training-dialog-trainingname').select2();
					$('#edit-training-dialog-trainingname').addClass('chzn-select');

	$('#edit-training-dialog-start-date').datetimepicker({
					format:'m/d/Y',
					lang:'en',
					timepicker:false
					
				});

$('#training-time-from').datetimepicker({
					lang:'en',
					datepicker:false,
					formatTime:'h:i A',
					format:'h:i A',
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});


				$('#training-time-to').datetimepicker({
					format:'h:i A',
					formatTime:'h:i A',
					lang:'en',
					datepicker:false,
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});

$('a#save-training-edit').click(function(){

		var eventtype = $('#edit-training-dialog-trainingname').val();			
		var memberslist1=$('#memberslist1').val();	
		var trndesc=$('#edit-training-dialog-desc').val();
		var eventdate=$('#edit-training-dialog-start-date').val();
		var starttime=$('#training-time-from').val();
		var endtime=$('#training-time-to').val();
		var upimgids=document.getElementById('upimgids').value;
		
		if(eventtype==null || eventtype=="")
		{
			bootbox.alert("Please Select Event Type/Topic!");
		}else if(memberslist1==null || memberslist1=="")
		{
			bootbox.alert("Please Select Invite Members!");
		}
		else if(eventdate=="")
		{
			bootbox.alert("Please Select Event Date");
		}
		else if(starttime==""){
			bootbox.alert("Please Select Start Time");
		}else if(endtime==""){
			bootbox.alert("Please Select End Time");
		}else{
			var data = {
				type: 'saveTraining',
				trainingname: eventtype,
				trndesc:trndesc,
				eventdate:eventdate,
				start:starttime,
				end:endtime,
				file:upimgids,
				memberslist1: memberslist1
			 }


			$.ajax({
				type: "POST",
				url: "events_json.php",
				data:data,
				success: function(resp) {
				bootbox.alert("Event Created Successfully",function(){
				 window.location.href = 'create_event.php';
				});
	 					    	
				},
			    	error: function() {
					alert('Error while Saving Trainging Details');
			    	},
			});

		}

});


});


</script>

<style type="text/css">

table, td, tr, th
{
	border:none;
}
td
{
	padding:6px;
}
.select2-container {
	width: 60% !important;
}
.customddl{
	width: 70% !important;
}

</style>
