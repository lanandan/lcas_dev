<style>
#spinner {
position: absolute;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.5;
background: url('images/preload.gif') 50% 50% no-repeat #ffffff;
}
.box-header label{
width:120px;
margin-left:10px;
display: inline-block;
}
.box-header button{
margin-left:30px;
}
.box-header select{
width:110px;
}
#search_container{
margin-top:10px;
margin-bottom:10px;
}
</style>
<script type="text/javascript" src="javascripts/bootbox.min.js"></script>
<link href="stylesheets/dtable.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/jquery.base64.js"></script>
<style>
.bootbox-body{
/*padding-left:100px;*/
font-size:14px;
}
.modal-title{
font-weight: 300;
}
</style>

<link rel="stylesheet" type="text/css" href="stylesheets/jquery.datetimepicker.css"/>
<script src="javascripts/jquery.datetimepicker.js"></script>

<script type="text/javascript">

function viewaddr_gmaps(address,leadid)
{
	var data={
		type:'vwleadaddr',
		addr:address,
		leadid:leadid
	}
	$.ajax({
		type: "POST",
		url: "view_gmap_addr.php",
		data: data,
		success: function(output)
		{
			var obj=JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:700,
				height:570,
				openjs:function(){

				}
				
			});
		
		}

	});
}

  function loadAjaxContent(x,y)
    { 

var data = {
    			type: 'editlead',
			id:x,
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "edit_leadstatus.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:800,
							height:550,
							openjs:function(){
							

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	

							$('a#save-event-edit').click(function(){
								
							var lead_status = $('#status').val();
							var activity = $('#activity').val();
							var contactreason = $('#contactreason').val();
							var comments = $('#comments').val();
					if(activity==''||contactreason==''||lead_status==''){
						$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose All The Options ');
							return false ;
						}
							var data = {
							    	type: 'editSaveEvent',
								id: x,
								leadid:y,
								status: lead_status,
								activity:activity,
								contactreason:contactreason,
								comments:comments,
						
							 }


								$.ajax({
								    		type: "POST",
								    		url: "edit_leadstatus.php",
								    		data: data,
								    		success: function(resp) {
										bootbox.alert(resp);
									        window.parent.TINY.box.hide();
										window.location.href="leadcard.php";
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
    }


var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var curdate = ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day + '' + d.getFullYear();


$(document).ready(function() {

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 10000);
});

$('#bonus_exp_date').datetimepicker({
		format:'M d Y',
		lang:'en',
		timepicker:false,
	});

$('#prog_exp_date').datetimepicker({
		format:'M d Y',
		lang:'en',
		timepicker:false,
	});

$('#example').dataTable({
      "sDom": '<"table-header"ilfp<"clear">>rt<"table-footer"ilfp<"clear">>',
"sPaginationType": "full_numbers",
 "bDestroy": true,
"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [0] }
       ],
 "oTableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        },
 "stateSave": true,
 "bStateSave": true
});


var oTable = $('#example').dataTable();
 var nNodes = oTable.fnGetNodes();

$("#check").click (function () 
{
    // Use the _ function instead to filter the rows to the visible
	var data = oTable._('tr', {"filter":"applied"});
	var i=$('#check').is(":checked");
	if(i==true){
	    $.each(data, function(key, index)
	    {
		var input = $(this).find('input[type="checkbox"]').prop('checked',true);
	    });
	}else{
	    $.each(data, function(key, index)
	    {
		var input = $(this).find('input[type="checkbox"]').prop('checked',false);
	    });
	}

});
$(nNodes).hover(
  function () {
    $(this).css("background","#3E93BD");
    $(this).css("color","#FFFFFF");
    $('.link',this).css("color","#FFFFFF");
  }, 
  function () {
    $(this).css("background","");
    $(this).css("color","#756A71");
    $('.link',this).css("color","#756A71");
  }
);


$(nNodes).click(
  function () {

	var lead_info='<table width="100%" class="table table-bordered padded">';
		lead_info+='<tr><td width="35%">Lead Name </td><td>'+$(this).closest('tr').children('td:eq(5)').html()+'</td></tr>';
		lead_info+='<tr><td width="35%">Lead Address </td><td>'+$(this).closest('tr').children('td:eq(6)').html()+'</td></tr>';
		lead_info+='<tr><td width="35%">Phone 1 </td><td>'+$(this).closest('tr').children('td:eq(27)').text()+'</td></tr>';
		lead_info+='<tr><td width="35%">Phone 2 </td><td>'+$(this).closest('tr').children('td:eq(28)').text()+'</td></tr>';
		lead_info+='<tr><td width="35%">Phone 3 </td><td>'+$(this).closest('tr').children('td:eq(29)').text()+'</td></tr>';
		lead_info+='<tr><td width="35%">E-Mail 1 </td><td>'+$(this).closest('tr').children('td:eq(30)').text()+'</td></tr>';
		lead_info+='<tr><td width="35%">E-Mail 2 </td><td>'+$(this).closest('tr').children('td:eq(30)').text()+'</td></tr>';
		lead_info+='</table>';

	$('#lead_info').html(lead_info);


	var lead_details='<table width="100%" class="table table-bordered padded">';
	
	var leadsubtype=$(this).closest('tr').children('td:eq(3)').text()+" / "+$(this).closest('tr').children('td:eq(4)').text();

	lead_details+='<tr><td width="40%">Lead / Sub Type </td><td>'+leadsubtype+'</td></tr>';

	lead_details+='<tr><td width="40%">Referred By </td><td>'+$(this).closest('tr').children('td:eq(11)').html()+'</td></tr>';

	lead_details+='<tr><td width="40%">Lead In </td><td>'+$(this).closest('tr').children('td:eq(2)').text()+'</td></tr>';

	lead_details+='<tr><td width="40%">Start Contacting </td><td>'+$(this).closest('tr').children('td:eq(32)').text()+'</td></tr>';	
		
	lead_details+='<tr><td width="40%">Lead Status </td><td>'+$(this).closest('tr').children('td:eq(8)').text()+'</td></tr>';

	lead_details+='<tr><td width="40%">Lead Result </td><td>'+$(this).closest('tr').children('td:eq(9)').text()+'</td></tr>';

	lead_details+='<tr><td width="40%">Lead Dealer </td><td>'+$(this).closest('tr').children('td:eq(24)').text()+'</td></tr>';

	lead_details+='</table>';

	$('#lead_details').html(lead_details);

	var apt_info='<table width="100%" class="table table-bordered padded">';
		apt_info+='<tr><td width="45%">Set Date </td><td>'+$(this).closest('tr').children('td:eq(10)').html()+'</td></tr>';
		apt_info+='<tr><td width="45%">Set By </td><td>'+$(this).closest('tr').children('td:eq(11)').html()+'</td></tr>';
		apt_info+='<tr><td width="45%">Appointment Date Time </td><td>'+$(this).closest('tr').children('td:eq(12)').html()+'</td></tr>';
		apt_info+='<tr><td width="45%">Appointment Type </td><td>'+$(this).closest('tr').children('td:eq(20)').text()+'</td></tr>';
		apt_info+='<tr><td width="45%">Appt. Status </td><td>'+$(this).closest('tr').children('td:eq(24)').text()+'</td></tr>';
		apt_info+='<tr><td width="45%">Appt. Result </td><td>'+$(this).closest('tr').children('td:eq(25)').text()+'</td></tr>';
		apt_info+='<tr><td width="45%">Dealer </td><td>'+$(this).closest('tr').children('td:eq(19)').text()+'</td></tr>';
		apt_info+='<tr><td width="40%">Demo Gift </td><td> &nbsp; </td></tr>';		
		apt_info+='</table>';

	$('#apt_info').html(apt_info);
	
  }
);

$('#utype').click(function(){

	var values = [];
	$(nNodes).find('input:checked').each(function(i, checkbox){
	var val=$(checkbox).val();
	 values.push(val);
	});
	if(values==''){
	alert("Please Make a Selection");
	return false;
	}
	var data = {
	    			type: 'editlead',
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "multi_edit_leadtype.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:500,
							height:400,
							openjs:function(){
 						$('#datepicker').datepicker({
									autoclose:true
							   });							


							$('#mtype').change(function(){
								var val=$('#mtype option:selected').attr("class")+" showsubtype";
								$('#msubtype .showsubtype').css({display:'none'});
								$('#msubtype .showsubtype').removeAttr("selected");
								$('#msubtype option[class="'+val+'"]').css({'display':'block'});
							});

							$('a#cancel-event-edit').click(function(){
								window.parent.TINY.box.hide();
							});	

							$('a#save-event-edit').click(function(){
								
							var lead_type = $('#mtype').val();
							var lead_subtype = $('#msubtype').val();
							var ldealer = $('#ldealer').val();
							var lstatus = $('#status').val();
							var lqual = $('#qual').val();
							var ldate = $('#datepicker').val();



					if(lead_type!=''){
					if(lead_subtype==''){
						$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose Lead Subtype ');
							return false ;
								}
						}
					if(lead_type=='' &&  lead_subtype=='' && ldealer=='' &&  lstatus=='' && lqual=='' && ldate==''){

							$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose Any One Selection');
							return false ;
							}

							var data = {
							    	type: 'update_event',
								leadid:values,
								ltype: lead_type,
								subtype:lead_subtype,
								ldealer:ldealer,
								lstatus:lstatus,
								lqual:lqual,
								ldate :ldate ,
							 }

								$.ajax({
								    		type: "POST",
								    		url: "multi_edit_leadtype.php",
								    		data: data,
								    		success: function(resp) {
									        window.parent.TINY.box.hide();
										location.reload();
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
});

var oTable = $('#example').dataTable();
 var nNodes = oTable.fnGetNodes( );
$(nNodes).click(function(evt){

 var $cell=$(evt.target).closest('td');
    if( $cell.index()==0){
     
}
else{
$("#spinner").show();
var id=$(this).closest('tr').children('td:eq(17)').text();
var leadid=$(this).closest('tr').children('td:eq(18)').text();
var data = {
    			type: 'view_answer',
			lid:id,
			leadseqid:leadid,
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "view_lead_answer.php",
	    		data: data,
	    		success: function(output) {
$("#spinner").hide();
$('#survey').html(output);
}
});
}
});

});

</script
