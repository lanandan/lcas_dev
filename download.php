<?php
$mime_type="";

if(isset($_REQUEST['type']))
{
	if($_REQUEST['type']=="download")
	{
		$file=$_REQUEST['filename'];

	$file = trim(preg_replace('/ +/', '', preg_replace('[^A-Za-z0-9 ]', '', urldecode(html_entity_decode(strip_tags($file))))));

	$file=preg_replace('/[$&#?]/i', '', $file); 

		$known_mime_types=array(
		 	"pdf" => "application/pdf",
		 	"txt" => "text/plain",
		 	"html" => "text/html",
		 	"htm" => "text/html",
			"exe" => "application/octet-stream",
			"zip" => "application/zip",
			"doc" => "application/msword",
			"xls" => "application/vnd.ms-excel",
			"ppt" => "application/vnd.ms-powerpoint",
			"gif" => "image/gif",
			"png" => "image/png",
			"jpeg"=> "image/jpg",
			"jpg" => "image/jpg",
			"php" => "text/plain",
			'flv' => "video/x-flv"
 		);
 
		 if($mime_type==''){
			 $file_extension = strtolower(substr(strrchr($file,"."),1));
			 if(array_key_exists($file_extension, $known_mime_types)){
				$mime_type=$known_mime_types[$file_extension];
			 } else {
				$mime_type="application/force-download";
			 };
		 };

		if (file_exists($file)) {
		    header("Cache-Control: public");	
		    header('Content-Description: File Transfer');
		    header('Content-Disposition: attachment; filename='.basename($file));
		    header('Content-Type: ' . $mime_type);
		    header("Content-Transfer-Encoding: binary");
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}else{
			header('Location: unauthorised.php?err=filenotfound');
			exit;
		}

	}
}
?>

