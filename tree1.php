<?php
session_start();
require_once("include/tps_db_conn.php");
require_once("include/tps_constants.php");
require_once("include/tps_gen_functions.php");
validate_login();
?>

﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="javascript" type="text/javascript" src="js/jquery-1.4.1.js"></script>
<style type="text/css">
#mob{width:15px;height:15px;}

#mail{width:15px;height:10px;}

#delete_option{margin-left:5px;margin-top:0px;cursor:pointer;}

#edit_option{margin-left:5px;margin-top:0px;cursor:pointer;}

#img{width:75px; height:75px;position:absolute;margin-left:5px;border:1px solid #a1a1a1;border-radius:5px; }

#div{margin-left:95px;padding-bottom:0px;margin-top:3px !important;font-size:12px;}

#div1{margin-left:95px;padding-bottom:3px;margin-top:0px !important;font-size:12px;}

#details{padding-bottom:100px;}

li #div:nth-child(1){
height:40px;
}

#spinner {
position: fixed;
left: 0px;
top: 0px;
display:none;
width: 100%;
height: 100%;
z-index: 9999;
opacity:.5;
background: url('images/preload.gif') 50% 50% no-repeat #ede9df;
}
        .wfm { width:100%;margin-top: -30px;  }
     
        ul { list-style:none;}
   
        
        ul, li, div { float:left }

hr{margin-bottom:20px;}

        ul, li { width:100% }
    
        .expand { width:15px;height:15px; }
	
	.nochild { width:15px;height:15px; }
    
        .collapse { width:15px;height:15px;display:none }
		

td img { border:1px solid #a1a1a1;border-radius:5px;  }

tr{font-size:12px;}

    </style>
<script>
function mylocation(val1){
window.top.location="adduser.php?lid=" + val1;
}

function edit(val1){
window.top.location="adduser.php?action=adminedit&lid=" + val1;
}


function deleteuser(lid,uid){
$(document).ready(function(){
$("#spinner").show();
 $.ajax({
            type: "POST",
            url: "team_check.php",
            data: {
		type:'get_team',
                uid:uid
            },
            success: function(result) {
$("#spinner").hide();
if(result==0){
 var where_to= confirm("Are You sure to remove this employee From Your Team");
 if (where_to==true)
{
window.top.location="adduser.php?id=" + lid;
     return true;
}
 else{
     
     return false;
 }

}
else{
alert('This User have team below him , Please delete the team members first');

}
}
});
});
    
 
}		


</script>
</head>
<body>
<div id="spinner"></div>
<?php

$ul_count = 0;
$cnt=0;

function get_ul_li_details_start ($current_level, $row_level)
{
	global $cnt;
	$html_str = "";
	if ($current_level == $row_level) {
		$html_str .= '<li><div><img alt="" class="expand" src="images/minus.png" /><img alt="" class="collapse" src="images/plus.png" /></div>';
		return $html_str;
	}
	else if ($current_level < $row_level) {
		$html_str =  '<ul><li><div><img alt="" class="expand" src="images/minus.png" /><img alt="" class="collapse" src="images/plus.png" /></div>';

	}
	else {
		echo "</li>";
		for ($i=$current_level; $i != $row_level; $i--) {

			$html_str .= "</ul></li>";
		}

		$html_str .= '<li><div><img alt="" class="expand" src="images/minus.png" /><img alt="" class="collapse" src="images/plus.png" /></div>';

	}
	return $html_str;
	
}

function get_ul_li_details_finish ($current_level, $row_level)
{
	$html_str = "</li>";
	for ($i=$current_level; $i != $row_level; $i--) {
		$html_str .= "</ul></li>";
	}
	$html_str .= "</ul>";
	return $html_str;

}

echo '<div class="wfm">';
$current_level = 0;
$row_level = 0;
$sql = "select * from tps_users  ";
preg_match('/\d+/',get_session('LOGIN_DUMMY'),$matches);
switch(get_session('LOGIN_LEVEL'))
{
case 1: 
        $sql .='  where level1="'.$matches[0].'" and status="1" ';
	break;
case 2:
	$sql .=' where level2="'.$matches[0].'" and status="1" ';
	break;
case 3:
	$sql .=' where level3="'.$matches[0].'" and status="1"';
	break;
case 4:
	$sql .=' where level4="'.$matches[0].'" and status="1" ';
	break;
default:
	break;

}
$sql.=" AND level in (1,2,3,4) order by level1, level2, level3, level4  ASC";
$result=mysql_query($sql) or die(mysql_error());


$usr=get_session('LOGIN_NAME');


$get_seq_id_sql="select id from tps_users where username='".$usr."'";

$usr_seq_id_result=mysql_query($get_seq_id_sql) or die(mysql_error());

$usr_seq_id_row = mysql_fetch_array($usr_seq_id_result);

$id=$usr_seq_id_row[0];

echo display_tree_for_user($id);

$current_level = get_session('LOGIN_LEVEL'); 
$c_level=get_session('LOGIN_LEVEL');
echo "<hr>";

while ($row = mysql_fetch_array($result)) {
	
	$row_level = $row['level'];
	echo get_ul_li_details_start ($current_level, $row_level);
	$display = "";


	switch ($row_level) {
		

case 1:
			$display = '&nbsp;&nbsp;<div id="details"><img src="images/upload/'.$row['image1'].'" id="img"></div>
			<div id="div">'.$row['username'].''; 
			$display .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick='mylocation(".$row['id'].");'>Add Team Managers</a></div>";
			$name=$row['nameprefix']." ".$row['fname']." ".$row['lname'];
			$display .= "<br><div id='div'>".$name."</div>";
			$display .= "<br><div id='div'><img src='images/mobile.jpg' id='mob'>&nbsp;".$row['phone1'].'</div>';
			$display .= "<br><div id='div'><img src='images/mail.png' id='mail'>&nbsp;".$row['email1'].'</div>';break;
		


case 2:
			$display = '&nbsp;&nbsp;<div id="details"><img src="images/upload/'.$row['image1'].'" id="img"></div>
			<div id="div1">'.$row['username'].''; 
			$display .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick='mylocation(".$row['id'].");'>Add TeamLeader</a>";
			if($c_level==1){
			$display .= "<img src='images/small-bin.png' id='delete_option' 
			onclick='deleteuser(".$row['id'].",".$row['userid'].");'>
			<img src='images/edit.png' id='edit_option' onclick='edit(".$row['id'].");'>";}
			$display .="</div>";
			$name=$row['nameprefix']." ".$row['fname']." ".$row['lname'];
			$display .= "<br><div id='div'>".$name."</div>";
			$display .= "<br><div id='div'><img src='images/mobile.jpg' id='mob'>&nbsp;".$row['phone1'].'</div>';
			$display .= "<br><div id='div'><img src='images/mail.png' id='mail'>&nbsp;".$row['email1'].'</div>';break;
		



case 3:
			$display = '&nbsp;&nbsp;<div id="details"><img src="images/upload/'.$row['image1'].'" id="img"></div>
			<div id="div1">'.$row['username'].''; 
			$display .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick='mylocation(".$row['id'].");'>Add Team Member</a> ";
			if($c_level==1||$c_level==2){
			$display .= "<img src='images/small-bin.png' id='delete_option' 
			onclick='deleteuser(".$row['id'].",".$row['userid'].");'> 
			<img src='images/edit.png' id='edit_option' onclick='edit(".$row['id'].");'>";}
			$display .="</div>";
			$name=$row['nameprefix']." ".$row['fname']." ".$row['lname'];
			$display .= "<br><div id='div'>".$name."</div>";
			$display .= "<br><div id='div'><img src='images/mobile.jpg' id='mob'>&nbsp;".$row['phone1'].'</div>';
			$display .= "<br><div id='div'><img src='images/mail.png' id='mail'>&nbsp;".$row['email1'].'</div>';break;
		


case 4:
			$display = '<div id="details"><img src="images/upload/'.$row['image1'].'" id="img"></div>
			<div id="div1">'.$row['username'].'';
			if($c_level==1||$c_level==2){
			$display .= "<img src='images/small-bin.png' id='delete_option' 
			onclick='deleteuser(".$row['id'].",".$row['userid'].");'>
			<img src='images/edit.png' id='edit_option' onclick='edit(".$row['id'].");'>";}
			$display .="</div>";
			$name=$row['nameprefix']." ".$row['fname']." ".$row['lname'];
			$display .= "<br><div id='div'>".$name."</div>";
			$display .= "<br><div id='div'><img src='images/mobile.jpg' id='mob'>&nbsp;".$row['phone1'].'</div>';
			$display .= "<br><div id='div'><img src='images/mail.png' id='mail'>&nbsp;".$row['email1'].'</div>'; 
			 break;
		
			default:
			$display = ""; 
			break;



	}
	echo "<div>$display</div>";

	$current_level = $row_level;

}

$row_level = 0;

echo get_ul_li_details_finish ($current_level, $row_level);



echo "</div>"


?>

   <script type="text/javascript" language="javascript">
$(document).ready(function(){
$("#spinner").show();
var i=0;
$("li:first,ul li").each(function(){
i++;
var child = $(this).find('li').size();
if(child==0){
	$(this).find("div .expand").remove();
        $(this).find("div #img").css({'margin-left':'20px'});
	$(this).find("div #div,#div1").css({'margin-left':'110px'});
}
else{
if(i!=1){
    $(this).find("div .expand").trigger('click');
}
}
    });
var len=$(".expand").get().length;
var j=1;
$($(".expand").get().reverse()).each(function(){
if(j!=len){
 $(this).toggle();
    $(this).next().toggle();
    $(this).parent().parent().children().last().toggle();
}
j++;
});
$("#spinner").hide();
$(".expand").click(function () {
    $(this).toggle();
    $(this).next().toggle();
    $(this).parent().parent().children().last().toggle();
});
$(".collapse").click(function () {
    $(this).toggle();
    $(this).prev().toggle();
    $(this).parent().parent().children().last().toggle();
});
});

</script>
</body>
</html>
