<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
$tas['appt_result']='';
$string='';
$sel='';
$query='';
$q='';
$referred_by='';
$setdate='';
$datefrom='';
$dateto='';
$setby='';
$appt_query='';
$refby='';
$appt_status='';
$appt_result='';
validate_login();
$page_name = "appointments.php";
$page_title = $site_name." -  Appointments";
if( request_get('action')=='delete') {
$id= request_get('lid');
$sql="update tps_appt_status_update set delete_flag='1' where id='$id'";
$result=mysql_query($sql) or die(mysql_error());
$message="your Status has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);
header("location:appointments.php");
exit;
}

if( request_get('action')=='deleteapt') {
$id= request_get('eid');
$sql="update tps_events set delete_flag='1' where id='$id'";
$result=mysql_query($sql) or die(mysql_error());
$message="Appointment has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);
header("location:appointments.php");
exit;
}
$Id=get_session('LOGIN_ID');
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";
include "js/appointments.js";
?>
<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>Appointments&nbsp;&nbsp;<a class="btn btn-blue" 
&nbsp;&nbsp;<a class="btn btn-blue" href="createtask.php"><span>Create Task</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="create_event.php"><span>Create Event</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" href="#" id="emailit" ><span>Email</span></a>
&nbsp;&nbsp;<a class="btn btn-blue" id="mapit" href="#"><span>Map It</span></a>	
&nbsp;&nbsp;<button type="button" class="btn btn-blue" onclick="printpage()">Print</button>
&nbsp;&nbsp;<a class="btn btn-blue" id="bulk" href="#"><span>Bulk Update</span></a>
&nbsp;&nbsp;<a class="btn btn-red" id="bulkDelete" href="#"><span>Bulk Delete</span></a>
	</h3>
</div> 

      </div>
    </div>
  </div>
<div class="container">
<?php
if(isset($_POST['list'])){
$array = array();
foreach($_POST['select'] as $val)
{

 array_push($array, $val);
	$q .= $val;
}

$lead_dealer=$array[0];
$setdate=$array[1];
$datefrom=$array[2];
$dateto=$array[3];
$setby=$array[4];
$sel_ref=$array[5];
$appt_status=$array[6];
$appt_result=$array[7];
}
?>
  <div class="col-md-16">
    <div class="box" >
<form id="frm" name="frm" class="fillup" action="appointments.php" method="POST">	
      <div class="box-header">
<div style="height:40px;margin-left:10px;">
<label><span style="">Lead Dealer</span>
<select id="lead_dealer" name="select[]" style="width:60px;margin-left:20px;margin-top:5px;">
<option value="">All</option>
<?php	
	$cur_userid=get_session('LOGIN_USERID');
	$cur_parentid=get_session('LOGIN_PARENTID');

	$re=mysql_query("select id,userid,username,fname,lname from tps_users where status='1'");

	while($r=mysql_fetch_array($re))
	{       
		if($lead_dealer==$r['userid'])
                	echo "<option value='$r[userid]' selected>".ucfirst($r['fname'])." ".ucfirst($r['lname'])."</option>";
  		else
			echo "<option value='$r[userid]'>".ucfirst($r['fname'])." ".ucfirst($r['lname'])."</option>";
	} 
?>
</select>   </label>
<label>
<span style="">Set Date</span>
<input type="text" name="select[]"  id="set_date" placeholder="Select Date" value="<?php echo $setdate; ?>"></label>
<span style="">Appt Date</span>
<input type="text" name="select[]" class="from" id="start_date" placeholder="From" value="<?php echo $datefrom; ?>">
<input type="text" name="select[]"  class="to" id="end_date" placeholder="To" value="<?php echo $dateto; ?>">
<label><span style="">Set By</span>
<select id="setby" name="select[]" style="width:60px;margin-top:5px;">
<option value="">All</option>
<?php	
	

	$re=mysql_query("select  fname,lname from tps_users where status='1'");

	while($r=mysql_fetch_array($re))
	{       
		if($setby==$r['fname']." ".$r['lname'])
                	echo "<option  selected>".$r['fname']." ".$r['lname']."</option>";
  		else
			echo "<option>".$r['fname']." ".$r['lname']."</option>";
	} 
?>
</select>   </label>
<label><span style="">Referred By</span>
<select id="setby" name="select[]" style="width:60px;margin-top:5px;">
<option value="">All</option>
<?php 
$ID=get_session('LOGIN_ID');
$today=date('Y-m-d H:i:s');
$child=array($ID);
$re=mysql_query("select id from tps_users where parentid='$userid'");
while($r=mysql_fetch_array($re)){
array_push($child,$r['id']);
}
$child=implode(',',$child);
$sql_new="select DISTINCT l.referred_by from tps_lead_card l JOIN tps_events e ON l.id=e.lead_id where FIND_IN_SET(l.uid,'$child') and e.delete_flag='0' and l.referred_by!='' order by l.id desc";

$result_new=mysql_query($sql_new);

while($row=mysql_fetch_array($result_new))
{	

	if($row['referred_by']!=''){

		$ref=$row['referred_by'];
		$im=explode('_',$ref);
		if($im[0]==1){
			$sq=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=ucfirst($r['lname1'])." ".$r['fname1']." ".$r['lname2']." ".$r['fname2'];
		}
		if($im[0]==2){
			$sq=mysql_query("select campaign from tps_campaign where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=$r['campaign'];
		}
		if($im[0]==3){
			$sq=mysql_query("select fname1,lname1 from tps_lead_card where id='$im[1]'")or die(mysql_error());
			$r=mysql_fetch_array($sq);
			$refby=$r['fname1'].' '.$r['lname1'];
		}

	}
	if(trim($sel_ref)==trim($row['referred_by']))
             echo "<option value=".$row['referred_by']." selected=selected>".$refby."</option>";
  	else
	     echo "<option value=".$row['referred_by'].">".$refby."</option>";
	
	$refby='';
}

?>
			</select>       </label>
<label><span style="">Appt Status</span>
<select id="setby" name="select[]" style="width:60px;margin-top:5px;">
<option value="">All</option>
<?php	
	

	$re=mysql_query("select name from tps_appt_status where status='0'");

	while($r=mysql_fetch_array($re))
	{       
		if($appt_status==$r['name'])
                	echo "<option  selected>".$r['name']."</option>";
  		else
			echo "<option>".$r['name']."</option>";
	} 
?>
</select>   </label>
<label><span style="">Appt Result</span>
<select id="setby" name="select[]" style="width:60px;margin-top:5px;">
<option value="">All</option>
<?php	
	

	$re=mysql_query("select name from tps_appt_result where status='0'");

	while($r=mysql_fetch_array($re))
	{       
		if($appt_result==$r['name'])
                	echo "<option  selected>".$r['name']."</option>";
  		else
			echo "<option>".$r['name']."</option>";
	} 
?>
</select></label>   
<button type="submit" id="list"  name="list"class="btn btn-blue" style="margin-left:10px;height:22px;padding-top:2px;">
                        List
                      </button>
		<button type="submit" name="reset" class="btn btn-blue" style="margin-left:10px;height:22px;padding-top:2px;">Reset</button>
                      </a>

</div>
<?php if( get_session('e_flag') == 1& get_session('message')!='') 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:450px;margin-top:10px;margin-bottom:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?></div>
      <div class="box-content" align="center">
	<?php
	$userid=get_session('LOGIN_USERID');

	$today=date('Y-m-d H:i:s');
$child=array($userid);
$re=mysql_query("select userid from tps_users where parentid='$userid'");
while($r=mysql_fetch_array($re)){
array_push($child,$r['userid']);
}
$child=implode(',',$child);

//echo "<pre>select userid from tps_users where parentid='$userid' <br> $child </pre>";

if(isset($_POST['list'])){

	$new_array = array();
	$n_array = array();
$n=1;
foreach($_POST['select'] as $value)
{


switch ($n)
{
case 1:
  $column='l.lead_dealer';
  break;
  
case 2:
if($value!=''){
$value=date('Y/m/d',strtotime($value));
}
  $column="DATE_FORMAT(e.created_at ,'%Y/%m/%d')";
  break;
  
case 3:
if($value!=''){
$value=date('Y/m/d',strtotime($value));
}
  $column="DATE_FORMAT(e.start, '%Y/%m/%d')>";
  break;
  
case 4:
if($value!=''){
$value=date('Y/m/d',strtotime($value));
}
  $column="DATE_FORMAT(e.start, '%Y/%m/%d')<";
  break;

case 5:
  $column="l.created_by";
  break;
 
case 6:
  $column="l.referred_by";
  break;

case 7:
  $column="s.appt_status";
  break;

case 8:
  $column="s.appt_result";
  break;
} 
if($value!=''){

$query .= $column."='".$value."' and ";

}


$n++;	
}
$string = rtrim($query, ' and ');
if($string=='')
{
$apt_sql= "SELECT l.id,l.leadid, l.lead_type,l.recruit_flag,l.customer_flag, l.lead_subtype, l.title1,l.title2,l.fname1, l.lname1, l.fname2, l.lname2, l.city, l.state, l.zip, l.phone1, l.lead_dealer, l.dealer, l.apptresult, l.referred_by, l.created, l.created_by,e.id as eid, e.lead_id as elead_id, DATE_FORMAT(e.start , '%b %d %Y %h:%i %p') as aptdate,DATE_FORMAT(e.created_at , '%m/%d/%y  %h:%i %p') as set_date,e.createdby as setby from tps_lead_card l JOIN tps_events e ON l.id=e.lead_id where  e.appttype!='Task' and e.delete_flag='0' and ( FIND_IN_SET(e.createdby,'$child') or e.assignedto='".$userid."' ) GROUP BY e.lead_id";
set_session('aptqry',$apt_sql);

}
else
{

$apt_sql= "select  l.id,l.leadid, l.lead_type,l.recruit_flag,l.customer_flag, l.lead_subtype, l.title1,l.title2, l.fname1, l.lname1, l.fname2, l.lname2, l.city, l.state, l.zip, l.phone1, l.lead_dealer, l.dealer, l.apptresult, l.referred_by, l.created, l.created_by,e.id as eid, e.lead_id as elead_id,DATE_FORMAT(e.created_at , '%b %d %Y %h:%i %p') as set_date ,e.createdby as setby, DATE_FORMAT(e.start , '%m/%d/%y  %h:%i %p') as aptdate from tps_lead_card l
INNER JOIN tps_events e on e.lead_id=l.id 
INNER JOIN( select * from tps_appt_status_update where id in (select distinct max(id) from tps_appt_status_update group by lead_id)  )s on  s.lead_id=l.leadid where $string and e.appttype!='Task' and e.delete_flag='0' and ( FIND_IN_SET(e.createdby,'$child') or e.assignedto='".$userid."' ) GROUP BY e.lead_id ";
set_session('aptqry',$apt_sql);

}

}else if(isset($_POST['reset'])){

$apt_sql="SELECT distinct l.id,l.leadid, l.lead_type,l.recruit_flag,l.customer_flag, l.title1,l.title2, l.lead_subtype, l.fname1, l.lname1, l.fname2, l.lname2, l.city, l.state, l.zip, l.phone1, l.lead_dealer, l.dealer, l.apptresult, l.referred_by, l.created, l.created_by,e.id as eid, e.lead_id as elead_id, DATE_FORMAT(e.start , '%b %d %Y %h:%i %p') as aptdate,DATE_FORMAT(e.created_at , '%m/%d/%y %h:%i %p') as set_date,e.createdby as setby from tps_lead_card l JOIN tps_events e ON l.id=e.lead_id where e.appttype!='Task' and e.delete_flag='0' and ( FIND_IN_SET(e.createdby,'$child') or e.assignedto='".$userid."' ) GROUP BY e.lead_id";
set_session('aptqry',$apt_sql);

}else{
$apt_sql="SELECT distinct l.id,l.leadid, l.lead_type,l.recruit_flag,l.customer_flag, l.title1,l.title2, l.lead_subtype, l.fname1, l.lname1, l.fname2, l.lname2, l.city, l.state, l.zip, l.phone1, l.lead_dealer, l.dealer, l.apptresult, l.referred_by, l.created, l.created_by,e.id as eid, e.lead_id as elead_id, DATE_FORMAT(e.start , '%b %d %Y %h:%i %p') as aptdate,DATE_FORMAT(e.created_at , '%m/%d/%y %h:%i %p') as set_date,e.createdby as setby from tps_lead_card l JOIN tps_events e ON l.id=e.lead_id where e.appttype!='Task' and e.delete_flag='0' and ( FIND_IN_SET(e.createdby,'$child') or e.assignedto='".$userid."' ) GROUP BY e.lead_id";

}
//echo "<pre>".$apt_sql."</pre>";

$aptQuery=get_session('aptqry');
//echo "<pre>$aptQuery</pre>";

if($aptQuery!="")
{
	//echo "<pre>$aptQuery</pre>";
	$res = mysql_query($aptQuery) or die(mysql_error());
	set_session('aptqry',$aptQuery);
	$aptQuery=get_session('aptqry');
}else{
	$res = mysql_query($apt_sql) or die(mysql_error());
	set_session('aptqry',$apt_sql);
}
	
	//$res=mysql_query($apt_sql)or die(mysql_error());

	$table="<table cellpadding='0'  cellspacing='0' border='0' class='dTable responsive' id='appt' >";
	
	$table.="<thead>
		<tr>
		 <th><div style='height:100%;display:inline-block;'>Action<span style='margin-left:10px;'><input type='checkbox' id='check' class='icheck1' ></span></div></th>
		 <th><div style='height:100%;display:inline-block;width:100px;'>Name1 & Name2</div></th>
		 <th><div style='height:100%;display:inline-block;'>City</div></th>
		 <th><div style='height:100%;display:inline-block;'>Phone</div></th>
    		 <th><div style='height:100%;display:inline-block;'>Lead Type</div></th>
		 <th><div style='height:100%;display:inline-block;'>Lead SubType</div></th>
		 <th><div style='height:100%;display:inline-block;'>Referred By</div></th>
		 <th><div style='height:100%;display:inline-block;'>Lead Dealer</div></th>
		 <th><div style='height:100%;display:inline-block;'>Set Date </div></th>
		 <th><div style='height:100%;display:inline-block;'>Set By</div></th>
		 <th><div style='height:100%;display:inline-block;'>Dealer</div></th>
		 <th><div style='height:100%;display:inline-block;'>Appt.Date&Time</div></th>		
		 <th><div style='height:100%;display:inline-block;'>Appt Status</div></th>
		 <th><div style='height:100%;display:inline-block;'>Appt Result</div></th>
		<th><div style='height:100%;display:inline-block;'>Referrals</div></th>
		<th><div style='height:100%;display:inline-block;'>Recruit</div></th>
		</tr></thead><tbody>";
	$i=1;
	while($lcr=mysql_fetch_array($res))
	{
$leadtype=getleadtype_name($lcr['lead_type']);
$leadsubtype=getleadsubtype_name($lcr['lead_subtype']);
$sql2="select fname,lname from tps_users where userid='".$lcr['setby']."'";
		$result2=mysql_query($sql2) or die(mysql_error());
		$row5 = mysql_fetch_array($result2);
		$setby=ucfirst($row5['fname'])." ".$row5['lname'];
	$leadid=$lcr['elead_id'];
if($lcr['customer_flag']=='1'){
$name='1_'.$lcr['id'];	
}
else{
$name='3_'.$lcr['id'];
}

	$lead_dealerid=$lcr['lead_dealer'];
	$sql_select="select id,userid,username,fname,lname from tps_users where userid='$lead_dealerid'";
	$result_select = mysql_query($sql_select) or die(mysql_error());
	$row1 = mysql_fetch_array($result_select);
	$lead_dealer_name=ucfirst($row1['fname'])." ".$row1['lname'];

	$dealerid=$lcr['dealer'];
	$sql_select="select id,userid,username,fname,lname from tps_users where userid='$dealerid'";
	$result_select = mysql_query($sql_select) or die(mysql_error());
	$row1 = mysql_fetch_array($result_select);
	$dealer_name=ucfirst($row1['fname'])." ".$row1['lname'];

	$refby=$lcr['referred_by'];
	$arr=explode('_',$refby);

	if($arr[0]==3){
		$re=mysql_query("select fname1,lname1 from tps_lead_card where id='$arr[1]'");
		$r=mysql_fetch_array($re);
		$referred_by=ucfirst($r['fname1'])." ".ucfirst($r['lname1']);
	}
	if($arr[0]==2){
		$re=mysql_query("select campaign from tps_campaign where id='$arr[1]'");
		$r=mysql_fetch_array($re);
		$referred_by=ucfirst($r['campaign']);
	}
	if($arr[0]==1){
		$re=mysql_query("select fname1,lname1,fname2,lname2 from tps_lead_card where id='$arr[1]'");
		$r=mysql_fetch_array($re);
		$referred_by=ucfirst($r['lname1'])." ".$r['fname1']." ".$r['lname2']." ".$r['fname2'];
	}

$table.='<tr>';

	if($lcr['customer_flag']=='1'){

$table.='<td>
<div class="btn-group">
	      <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button>
	      <ul class="dropdown-menu">
		 <li><a href="add_leads.php?action=updatecustomer&lid= '.$lcr['id'].'&redir=1"><img src="images/edit.png"/>&nbsp;&nbsp;Edit</a></li>
<li><a href="#"  onclick="loadAjaxContent('.$lcr['id'].','.$lcr['leadid'].');"><img src="images/update.png"  title="Update Status"/ height="20px" width="20px">&nbsp;&nbsp;Activity</a></li>
		 <li class="divider"></li>
		 <li>
		<a href="appointments.php?action=deleteapt&eid='.$lcr['eid'].'" onClick="return confirmDelete();"><img src="images/block.png" width="18px"/>&nbsp;&nbsp;Delete</a></li>


	      </ul>
<div style="margin-left:50px;margin-top:5px;"><input type="checkbox" id="icheck1" value="'.$lcr['leadid'].'" aptid="'.$lcr['eid'].'" class="icheck1" ></div>
	</div></td>';
}
else{	$table.='<td>
	<div class="btn-group">
	      <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button>
	      <ul class="dropdown-menu">
		 <li><a href="add_leads.php?action=edit&lid= '.$lcr['id'].'&redir=1"><img src="images/edit.png"/>&nbsp;&nbsp;Edit</a></li>
<li><a href="#"  onclick="loadAjaxContent('.$lcr['id'].','.$lcr['leadid'].');"><img src="images/update.png"  title="Update Status"/ height="20px" width="20px">&nbsp;&nbsp;Activity</a></li>
		 <li class="divider"></li>
		 <li>
		<a href="appointments.php?action=deleteapt&eid='.$lcr['eid'].'" onClick="return confirmDelete();"><img src="images/block.png" width="18px"/>&nbsp;&nbsp;Delete</a></li>


	      </ul>
<div style="margin-left:50px;margin-top:5px;">
<input type="checkbox" id="icheck1" value="'.$lcr['leadid'].'" aptid="'.$lcr['eid'].'" class="icheck1" >
</div>
	</div>
		</td>';
}
		$table.="<td>".$lcr['title1'].' '.ucfirst($lcr['fname1'])." ".$lcr['lname1']."<br>".$lcr['title2'].' '.ucfirst($lcr['fname2'])." ".$lcr['lname2']."</td>";
		$table.="<td>".$lcr['city']."</td>";
		$table.="<td>".$lcr['phone1']."</td>";
		$table.="<td>".$leadtype."</td>";
		$table.="<td>".$leadsubtype."</td>";
		$table.="<td>".$referred_by."</td>";
		$table.="<td>".$lead_dealer_name."</td>";
		$table.="<td>".$lcr['set_date']."</td>";
		$table.="<td>".$setby."</td>";
		$table.="<td>".$dealer_name."</td>";
		$table.="<td>".$lcr['aptdate']."</td>";
		$leadid=$lcr['leadid'];

		$sql_tas="select appt_status,appt_result from tps_appt_status_update where lead_id='$leadid' and delete_flag='0' order by id desc ";

		$restas=mysql_query($sql_tas) or die(mysql_error());
	        $tas = mysql_fetch_array($restas);

		$appt_result=$tas['appt_result'];
		$appt_status=$tas['appt_status'];
		if($appt_status=='')
		{
		$tas['appt_status']="No Appt Status";
		}
		if($appt_result=='')
		{
		$tas['appt_result']="No Result";
		}
		
		$table.="<td>". $tas['appt_status'] ."</td>";
	
		$table.="<td>" .$tas['appt_result']."</td>";

		$sql_tas="select * from tps_lead_card where referred_by='$name'  and delete_flag='0' order by id desc ";

		$restas=mysql_query($sql_tas) or die(mysql_error());
	        $tas = mysql_num_rows($restas);
		
		$table.='<td><a style="border-bottom: 3px double;" class="link" href="referred_leads.php?id='.$lcr['leadid'].'">('.$tas.')</a></td>';
if($lcr['recruit_flag']==0){	$table.='<td><a style="border-bottom: 3px double;" class="link test" onclick="loadrecurit('.$lcr['leadid'].');" >('.$lcr['recruit_flag'].')</a>
			</td></tr>';}
else{
$table.='<td><a style="border-bottom: 3px double;" class="test"  >('.$lcr['recruit_flag'].')</a>
			</td></tr>';}
$referred_by='';
		$i++;

	}
	$table.="</tbody></table>";
	
		
	echo $table;
		
?>

      </div>
    </div>
   </div>
</div>

 </div>
 </div>
</form>
<script>
$('#utype').click(function(){ 
var values = [];
$(nNodes).find('input:checked').each(function(i, checkbox){
var val=$(checkbox).val();
 values.push(val);
});
if(values==''){
//alert("Please Make a Selection");
//return false;
}alert("dd");
var data = {
    			type: 'editlead',
		}
    		$.ajax({

	    		type: "POST",
	    		url:  "multi_edit_leadtype.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:500,
							height:400,
							openjs:function(){
 						$('#datepicker').datepicker({
									autoclose:true
							   });							


$('#mtype').change(function(){
var val=$('#mtype option:selected').attr("class")+" showsubtype";
$('#msubtype .showsubtype').css({display:'none'});
$('#msubtype .showsubtype').removeAttr("selected");
$('#msubtype option[class="'+val+'"]').css({'display':'block'});
});

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	


								

							$('a#save-event-edit').click(function(){
								
							var lead_type = $('#mtype').val();
							var lead_subtype = $('#msubtype').val();
							var ldealer = $('#ldealer').val();
							var lstatus = $('#status').val();
							var lqual = $('#qual').val();
							var ldate = $('#datepicker').val();



					if(lead_type!=''){
					if(lead_subtype==''){
						$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose Lead Subtype ');
							return false ;
								}
						}
					if(lead_type=='' &&  lead_subtype=='' && ldealer=='' &&  lstatus=='' && lqual=='' && ldate==''){

							$('.msg').css({'display':'block'});
							$('.msg').html('Pls Choose Any One Selection');
							return false ;
							}

							var data = {
							    	type: 'update_event',
								leadid:values,
								ltype: lead_type,
								subtype:lead_subtype,
								ldealer:ldealer,
								lstatus:lstatus,
								lqual:lqual,
								ldate :ldate ,
							 }

								$.ajax({
								    		type: "POST",
								    		url: "multi_edit_leadtype.php",
								    		data: data,
								    		success: function(resp) {
									        window.parent.TINY.box.hide();
										location.reload();
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
});
</script>
<?php

include "lcas_footer.php";

?>
