<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

validate_login();

$page_name = "recent_activities.php";
$page_title = $site_name." -  Recent Activities";

include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

$cur_userid=get_session('LOGIN_ID');
$cur_email=get_session('LOGIN_EMAIL');
$cur_username=get_session('DISPLAY_NAME');

$cur_loguserid=get_session('LOGIN_USERID');

?>
<style>
.react{
	color:#2098D1;
}
a {
	color:#2098D1;
}
a:hover
{
	color:#2098D1;
	text-decoration:underline;
}
</style>
<div class="main-content" >
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
         <h3 class="title">
	 <i class="icon-bar-chart"></i>Recent Activities</a>
	</h3>
</div> 

      </div>
    </div>
  </div>
<div class="container">

  <div class="col-md-16">
    <div class="box" >
      <div class="box-header">
	<span class="title">
	Recent Activities
	</span>
  </div>
      <div class="box-content scrollable" style="height: 500px; overflow-y: auto" >
	
<?php

$sql="select * from tps_error_log where userid='$cur_userid' and actiontype!='Login' and type!='1' order by created desc";
$res=mysql_query($sql)or die("Error : ".mysql_error());

$imgr=mysql_query("select image1 from tps_users where id='$cur_userid'")or die(mysql_error());
$imgres=mysql_fetch_array($imgr);

$userimg="images/upload/".$imgres['image1'];

if($userimg=="")
{
	$userimg="images/upload/default.jpeg";
}

while($r=mysql_fetch_array($res))
{
	$created=strtotime($r['created']);	
?>

<div class="box-section news">
  <div class="avatar"><img class="avatar-small" src="<?php echo $userimg; ?>" /></div>
  <div class="news-time">
    <span>&nbsp;</span> <?php echo display_time_diff_format($created); ?>
  </div>
  <div class="news-content">
    <div class="news-title"><?php echo $r['actiontype']; ?></div>
    <div class="news-text">
     <?php echo html_entity_decode($r['description']); ?>
    </div>
  </div>
</div>

<?php
}
?>

      </div>
    </div>
	<br />
   </div>

 </div> 
 </div>

<div id="spinner"></div>

<?php

include "lcas_footer.php";

?>
