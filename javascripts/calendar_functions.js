$(document).ready(function() {
  var d, date, m, y, dd;
  new CalendarEvents($('#external-events'));
  date = new Date();
  d = date.getDate();
  m = date.getMonth();
  y = date.getFullYear();

  dd = date.getDay();

  //dashboard calendar
  var dbcal= $("#calendar").fullCalendar({
    header: {
      left: "prev,next today",
      center: "title",
      right: "month,agendaWeek,agendaDay"
    },
    editable: true,
    droppable: true,
    minTime:10,
    maxTime:23,
    firstDay:dd,
    startParam: 'start',
    endParam: 'end',
    defaultView: 'agendaWeek',
    slotEventOverlap:false,
    selectable: true,
    selectHelper: true,
   // ignoreTimezone:false,
    eventClick: function( event, jsEvent, view ) { 
	//console.log(event);

	end = new Date(event.start);
	end.setHours(end.getHours() + 3);
	
	var month=new Array();
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";
	var mmn = month[end.getMonth()]; 

	var seldate=end.getDate()+'/'+mmn+'/'+end.getFullYear();
	
	var data = {
    			type: 'editTrainingEventbyCreator',
			id:event.id,
			tseqid:event.tseqid,
			sdate: mmn,
			appttype: event.appttype,
			start: event.start,
			end: event.end,
			allDay: event.allDay
		}

		$.ajax({
	    	type: "POST",
	    	url: "events_json.php",
	    	data: data,
	    	success: function(output) {
			var obj = JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:700,
				openjs:function(){
					autoclose:true

					var cseqid=$('#curtrainingseqid1').val();

					$('#file1').uploadifive({
						'auto'             : true,
						'queueID'          : 'queue1',
						'queueSizeLimit'   : 5,
						'uploadLimit'      : 5,
						'uploadScript'     : 'uploadifive.php?job=edit&tabname=evnt&tseqid='+cseqid,
						'onProgress'   	   : function(file, e) {
								    if (e.lengthComputable) {
									var percent = Math.round((e.loaded / e.total) * 100);
								    }
								    file.queueItem.find('.fileinfo').html(' - ' + percent + '%');
								    file.queueItem.find('.progress-bar').css('width', percent + '%');
						}, 
						'onUploadComplete' : function(file, data) {

									var d1=data.trim();
									
									$('#output1').append(d1+',');
									
									document.getElementById('upimgids1').value = $('#output1').html();

									file.queueItem.find('.close').html(data);
				
								}
	
					});

					$('#memberslist1').select2();
					$('#memberslist1').addClass('chzn-select');

					$('#edit-training-dialog-trainingname').select2();
					$('#edit-training-dialog-trainingname').addClass('chzn-select');

					/*$('#topic').select2();
					$('#topic').addClass('chzn-select');*/

					$('#newtraining').hide();
					
					$('#edit-training-dialog-desc1').limiter();
					$('#edit-training-dialog-desc').limiter();

					$('#edit-training-dialog-trainingname').change(function() { 
						var selval=$('#edit-training-dialog-trainingname').val();
							//alert(selval);
						if(selval=="others")
						{
							$('#newtraining').show();
						}else if(selval=="0")
						{
							$('#newtraining').hide();				
						}else
						{
							$('#newtraining').hide();				
						}
				    	});
			
				$('#edit-training-dialog-start-date').datetimepicker({
					format:'m/d/Y',
					lang:'en',
					timepicker:false,
				});

				$('#training-time-from').datetimepicker({
					format:'h:i A',
					formatTime:'h:i A',
					lang:'en',
					datepicker:false,
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});

				$('#training-time-to').datetimepicker({
					format:'h:i A',
					formatTime:'h:i A',
					lang:'en',
					datepicker:false,
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});


var typeval1=$('#edit-event-dialog-type').val();
	
	if(typeval1=="Demo"){
		$('#trnteam').hide();
	}else if(typeval1=="Training"){
		$('#trnteam').show();
	}else if(typeval1=="AdvancedTrainingClinics"){
		$('#trnteam').hide();
	}

$('#memberslist').select2();
$('#memberslist').addClass('chzn-select');

$('#edit-event-dialog-type').change(function() { 

	typeval=$('#edit-event-dialog-type').val();
	
	if(typeval=="Demo"){
		$('#trnteam').hide();
	}else if(typeval=="Training"){
		$('#trnteam').show();
	}else if(typeval=="AdvancedTrainingClinics"){
		$('#trnteam').hide();
	}

});

// for accepting training 
var isaccepted1="";
	
	$('#yes').click(function(){
		$('#yes').removeClass('btn-default');
		$('#maybe').removeClass('btn-gray');
		$('#maybe').addClass('btn-default');
		$('#no').removeClass('btn-red');
		$('#no').addClass('btn-default');

		$('#yes').addClass('btn-green');
		$(this).data('clicked', true);
		//isaccepted1=$('#yes').val();	
	}); 
	$('#maybe').click(function(){	
		$('#maybe').removeClass('btn-default');
		$('#yes').removeClass('btn-green');
		$('#yes').addClass('btn-default');	
		
		$('#no').removeClass('btn-red');		
		$('#no').addClass('btn-default');

		$('#maybe').addClass('btn-gray');
		$(this).data('clicked', true);
		//isaccepted1=$('#maybe').val();	
	}); 
	$('#no').click(function(){
		$('#no').removeClass('btn-default');
		$('#yes').removeClass('btn-green');
		$('#yes').addClass('btn-default');
		$('#maybe').removeClass('btn-gray');
		$('#maybe').addClass('btn-default');

		$('#no').addClass('btn-red');
		$(this).data('clicked', true);
		//isaccepted1=$('#no').val();	
	}); 

$('a#save-TrainingAttend-edit').click(function(){

	//var ctid=$('#curtrainingid1').val();
	var ctseqid=$('#curtrainingseqid1').val();
	
	var $checkboxes = jQuery('input[type="checkbox"]');

	var checkedids = new Array();
	var uncheckedids = new Array();

	jQuery.each($checkboxes, function(i) {
		if($(this).is(':checked')){
		     var value = $(this).attr('value'); 
		     checkedids[i] = value;
		}else{
		     var value = $(this).attr('value'); 
		     uncheckedids[i] = value;
		}
	});

	//alert("Checked IDs : "+checkedids+" Unchecked IDs : "+uncheckedids);

	var data = {
		type: 'saveAttendedDetails',
		tseqid:ctseqid,
		appttype:event.appttype,
		checkedids:checkedids,
		uncheckedids:uncheckedids
		}
		$.ajax({
			type: "POST",
			url: "events_json.php",
			data: data,
			success: function(resp) {
			//alert('success msg');
			//alert('result = ' + resp);
			 	$('#calendar').fullCalendar( 'refetchEvents');
				window.parent.TINY.box.hide();
	  	        },
		error: function() {
			alert('error in saving Attended Details');
		},

		});


});

$('a#save-TrainingAccept-edit').click(function(){

	var rem=$('#edit-drop-event-dialog-remarks').val();
	var appttype=$('#edit-drop-event-dialog-appttype-remarks').val();
	//var isaccepted=$('input[name=isaccept]:checked').val();
	var seqid=$('#edit-drop-event-dialog-seqid').val();
	var tseqid=$('#edit-drop-event-dialog-tseqid').val();

	var isaccepted1="";

	if(jQuery('#yes').data('clicked')) {
    		isaccepted1=$('#yes').val();	
	} else if(jQuery('#maybe').data('clicked')){
    		isaccepted1=$('#maybe').val();	
	}else if(jQuery('#no').data('clicked')){
		isaccepted1=$('#no').val();	
	}	

	//alert(isaccepted1);

	var data = {
		type: 'saveRemarks',
		id:event.id,
		isaccept:isaccepted1,
		appttype:appttype,
		remarks:rem,
		seqid:seqid,
		tseqid:tseqid
		}
		$.ajax({
			type: "POST",
			url: "events_json.php",
			data: data,
			success: function(resp) {
			//alert('success msg');
			//alert('result = ' + resp);
			 	$('#calendar').fullCalendar( 'refetchEvents');
				window.parent.TINY.box.hide();
	  	        },
		error: function() {
			alert('error in saving');
		},

		});

									
});

$('a#ok-event-edit').click(function(){
		window.parent.TINY.box.hide();
	});	

$('a#cancel-event-edit').click(function(){
		window.parent.TINY.box.hide();
	});	
	
$('a#save-event-edit').click(function(){

var totalSec = $('#edit-event-dialog-table #event-time-from').val();
var hours = parseInt( totalSec / 3600 ) % 24;
var minutes = parseInt( totalSec / 60 ) % 60;
var seconds = totalSec % 60;

var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

var totalSec2 = $('#edit-event-dialog-table #event-time-to').val();
var hours2 = parseInt( totalSec2 / 3600 ) % 24;
var minutes2 = parseInt( totalSec2 / 60 ) % 60;
var seconds2 = totalSec2 % 60;

var result2 = (hours2 < 10 ? "0" + hours2 : hours2) + ":" + (minutes2 < 10 ? "0" + minutes2 : minutes2) + ":" + (seconds2  < 10 ? "0" + seconds2 : seconds2);
var newdate=$('#edit-event-dialog-table #edit-event-dialog-start-date').val();
var newdatesplit=newdate.split("/");
									var eventid = $('#edit-event-dialog-table #event-id-hd').val();
									var eventtype = $('#edit-event-dialog-type').val();
									var eventTitle = $('#edit-event-dialog-table #edit-event-dialog-title1').val() + ' - ' + $('#edit-event-dialog-table #edit-event-dialog-title2').val()+ ' - ' + $('#edit-event-dialog-table #edit-event-dialog-title3').val();  
									var startDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result;
									var endDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result2;
									var memberslist=$('#memberslist').val();
									
									var data = {
							    			type: 'editsaveEvent',
										id: eventid,
										title: eventTitle,
										type1: eventtype,
										start:startDate,
										end:endDate,
										memberslist: memberslist
									 }

									$.ajax({
								    		type: "POST",
								    		url: "event_functions.php",
								    		data: data,
								    		success: function(resp) {
										//alert('result = ' + resp);
										$('#calendar').fullCalendar( 'refetchEvents');
											window.parent.TINY.box.hide();
									    	},
									    	error: function() {
											alert('error in edit saving');
									    	},
								    	});
	
									//alert("callback function in javascript/calender_functions.js");
									
								});	
		$('a#delete-event-edit').click(function(){
		
			var curtrainingseqid=$('#curtrainingseqid1').val();	
			
			bootbox.confirm("Are you sure do you want to delete this training?", function(result) {
		
				 if (result == true) {
					var data = {
						type: 'Deletetraining',
						curtrainingseqid: curtrainingseqid,
						
					}
					$.ajax({
	    					type: "POST",
						data:data,
	    					url: "events_json.php",
	    					success: function(output) {
							bootbox.alert("Training Deleted Successfully");
							dbcal.fullCalendar('refetchEvents');
							window.parent.TINY.box.hide();
						},
						error: function() {
							bootbox.alert('Error while Deleting the Event Details');
						}

					});
				} 
			}); 
					

		});
			

	$('a#save-training-edit1').click(function(){
	
			/*var totalSec = $('#training-time-from').val();
			var hours = parseInt( totalSec / 3600 ) % 24;
			var minutes = parseInt( totalSec / 60 ) % 60;
			var seconds = totalSec % 60;

var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

var totalSec2 = $('#training-time-to').val();
var hours2 = parseInt( totalSec2 / 3600 ) % 24;
var minutes2 = parseInt( totalSec2 / 60 ) % 60;
var seconds2 = totalSec2 % 60;

var result2 = (hours2 < 10 ? "0" + hours2 : hours2) + ":" + (minutes2 < 10 ? "0" + minutes2 : minutes2) + ":" + (seconds2  < 10 ? "0" + seconds2 : seconds2);
var newdate=$('#edit-training-dialog-start-date').val();

var newdatesplit=newdate.split("/");

			
var startDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result;
var endDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result2; */
			
			var eventtype = $('#edit-training-dialog-trainingname').val();
			//var eventsubtype=$('#topic').val();
			//var trainingnamenew = $('#edit-training-dialog-trainingnamenew1').val();

			//var curtrainingid=$('#curtrainingid1').val();	

			var curtrainingseqid=$('#curtrainingseqid1').val();

			var trndesc=$('#edit-training-dialog-desc1').val();
			var upimgids=document.getElementById('upimgids1').value;
			var memberslist1=$('#memberslist1').val();

	if(eventtype==null || eventtype=="")
	{
		alert("Please Select Event Type/Topic!");
	}else if(memberslist1==null || memberslist1=="")
	{
		alert("Please Select Invite Members!");
	}
	else{

		var data = {
			type: 'saveTraining1',
			trainingname: eventtype,
			curtrainingseqid: curtrainingseqid,
			trndesc:trndesc,
			upimgids:upimgids,
			memberslist1:memberslist1
		 }

		$.ajax({
			type: "POST",
			url: "events_json.php",
			data: data,
			success: function(resp) {
			//alert('result = ' + resp);
			dbcal.fullCalendar('refetchEvents');
				window.parent.TINY.box.hide();
		    	},
		    	error: function() {
				alert('Error while Saving Trainging Details');
		    	},
		});
	}
	
		
	}); // end of save btn click			


	    } // end of openjs
	});
}

	});
		
    },
    select: function(start, end, allDay, jsEvent, view) {

	//alert("Start : "+start+" ; End : "+end);

	end = new Date(start);
	end.setHours(end.getHours() + 3);
	
	var month=new Array();
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";
	var mmn = month[end.getMonth()]; 

	var tdate=("0" + end.getDate()).slice(-2);
	
	var seldate=tdate+'/'+mmn+'/'+end.getFullYear();

	function formatAMPM(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'PM' : 'AM';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}
	
	function formatAMPMforEndTime(date) {
	  var hours = date.getHours()+1;
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'PM' : 'AM';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}

	var starttime=formatAMPM(start);
	var endtime=formatAMPMforEndTime(start);

	//alert("Start Time : "+starttime+" ; End Time : "+endtime);

	var data = {
    		type: 'addTrainingEvent',
		sdate: seldate,
		start: start,
		end: end,
		starttime:starttime,
		endtime:endtime,
		allDay: allDay
	}
	$.ajax({
	    	type: "POST",
	    	url: "events_json.php",
	    	data: data,
	    	success: function(output) {
	
			var obj = JSON.parse(output);
			TINY.box.show({
				html:obj.popupHtml,
				width:700,
				height:630,
				openjs:function(){
					autoclose:true

$('#file').uploadifive({
	'auto'             : true,
	'queueID'          : 'queue',
	'queueSizeLimit'   : 5,
	'uploadLimit'      : 5,
	'uploadScript'     : 'uploadifive.php?job=add&tabname=evnt',
	'onProgress'   	   : function(file, e) {
			    if (e.lengthComputable) {
				var percent = Math.round((e.loaded / e.total) * 100);
			    }
			    file.queueItem.find('.fileinfo').html(' - ' + percent + '%');
			    file.queueItem.find('.progress-bar').css('width', percent + '%');
        }, 
	'onUploadComplete' : function(file, data) {

				var d1=data.trim();
				$('#output').append(d1+',');

				document.getElementById('upimgids').value = $('#output').html();

				file.queueItem.find('.close').html(data);
				
			}
	
});

	
					$('#memberslist1').select2();
					$('#memberslist1').addClass('chzn-select');

					$('#edit-training-dialog-trainingname').select2();
					$('#edit-training-dialog-trainingname').addClass('chzn-select');

					$('#edit-training-dialog-trainingname').change(function() { 

						var selval=$('#edit-training-dialog-trainingname').val();	
								
				    	});
					
					$('#edit-training-dialog-desc1').limiter();
					$('#edit-training-dialog-desc').limiter();

				$('#edit-training-dialog-start-date').datetimepicker({
					format:'m/d/Y',
					lang:'en',
					timepicker:false
					
				});

				$('#training-time-from').change(function(){

					var st=$('#training-time-from').val();
					
					var time = $("#training-time-from").val();
					var hours = Number(time.match(/^(\d+)/)[1]);
					var minutes = Number(time.match(/:(\d+)/)[1]);
					var AMPM = time.match(/\s(.*)$/)[1];
					if(AMPM == "PM" && hours<12) hours = hours+12;
					if(AMPM == "AM" && hours==12) hours = hours-12;
					var sHours = hours.toString();
					var sMinutes = minutes.toString();
					if(hours<10) sHours = "0" + sHours;
					if(minutes<10) sMinutes = "0" + sMinutes;

					//var stt=sHours + ":" + sMinutes;

					var stt = new Date('2014','04','11',sHours,sMinutes)

					function formatAMPMforEndTime(date) {
					  var hours = date.getHours()+1;
					  var minutes = date.getMinutes();
					  var ampm = hours >= 12 ? 'PM' : 'AM';
					  hours = hours % 12;
					  hours = hours ? hours : 12; // the hour '0' should be '12'
					  minutes = minutes < 10 ? '0'+minutes : minutes;
					  var strTime = hours + ':' + minutes + ' ' + ampm;
					  return strTime;
					}

					var ed=formatAMPMforEndTime(stt);
					$('#training-time-to').val(ed);

					//alert("Start : "+st+" ; End : "+ed);


				});

				$('#training-time-from').datetimepicker({
					lang:'en',
					datepicker:false,
					formatTime:'h:i A',
					format:'h:i A',
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});

				$('a#cancel-event-edit').click(function(){
					window.parent.TINY.box.hide();
				});

				$('#training-time-to').datetimepicker({
					format:'h:i A',
					formatTime:'h:i A',
					lang:'en',
					datepicker:false,
					allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ]
				});

			
	$('a#save-training-edit').click(function(){
	
		
			var eventtype = $('#edit-training-dialog-trainingname').val();
			//var eventsubtype = $('#topic').val();
			//var trainingnamenew = $('#edit-training-dialog-trainingnamenew').val();			
			var memberslist1=$('#memberslist1').val();	
			var trndesc=$('#edit-training-dialog-desc').val();
			var eventdate=$('#edit-training-dialog-start-date').val();
			var starttime=$('#training-time-from').val();
			var endtime=$('#training-time-to').val();
			var upimgids=document.getElementById('upimgids').value;
			

$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 30000);
});

if(eventtype==''&&eventsubtype=='')
{
$('.msg').css({'display':'block'});
$('.msg').html('Pls choose Event type & subtype');
return false;
}
if(memberslist1=='null')
{
$('.msg').css({'display':'block'});
$('.msg').html('Pls invite atleast one member');
return false;
}

	if(eventtype==null || eventtype=="")
	{
		alert("Please Select Event Type/Topic!");
	}else if(memberslist1==null || memberslist1=="")
	{
		alert("Please Select Invite Members!");
	}
	else if(eventdate=="")
	{
		alert("Please Select Event Date");
	}
	else if(starttime==""){
		alert("Please Select Start Time");
	}else if(endtime==""){
		alert("Please Select End Time");
	}else{
		var data = {
			type: 'saveTraining',
			trainingname: eventtype,
			trndesc:trndesc,
			eventdate:eventdate,
			start:starttime,
			end:endtime,
			file:upimgids,
			memberslist1: memberslist1
		 }


		$.ajax({
			type: "POST",
			url: "events_json.php",
			data:data,
			success: function(resp) {
			//alert('result = ' + resp);
			dbcal.fullCalendar('refetchEvents');
				window.parent.TINY.box.hide();
 					    	
			},
		    	error: function() {
				alert('Error while Saving Trainging Details');
		    	},
		});
	}
	
		
	}); // end of save btn click			


			    } // end of openjs
			});
		

		dbcal.fullCalendar('unselect');

		}
	});

    },
    drop: function(date, allDay) {
      var copiedEventObject, originalEventObject;
      originalEventObject = $(this).data('eventObject');
      copiedEventObject = $.extend({}, originalEventObject);
      copiedEventObject.start = date;
      copiedEventObject.allDay = allDay;
      dbcal.fullCalendar('renderEvent', copiedEventObject, true);
      if ($("#drop-remove").is(":checked")) {
        return $(this).remove();
      }
    },
    eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {

		console.log(event);
		var data = {
    			type: 'editDropTrainingEvent',
			id:event.id,
			tseqid:event.tseqid,
			appttype:event.appttype	
		}
    		$.ajax({
	    		type: "POST",
	    		url: "events_json.php",
	    		data: data,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							openjs:function(){
			
								$('#edit-training-dialog-desc1').limiter();

								$('#edit-training-dialog-desc').limiter();

								$('a#cancel-event-edit').click(function(){
									dbcal.fullCalendar( 'refetchEvents');
									window.parent.TINY.box.hide();
								});	
								$('a#save-event-edit').click(function(){
									var startDate = event.start;
							    		var endDate = event.end;
									var rem=$('#edit-drop-event-dialog-remarks').val();
									var data = {
							    			type: 'saveDropTrainingEvent',
										id:event.id,
										tseqid:event.tseqid,
										appttype:event.appttype,
										start:startDate,
										remarks:rem,
										end:endDate }
										$.ajax({
									    		type: "POST",
									    		url: "events_json.php",
									    		data: data,
									    		success: function(resp) {
												//alert('success msg');
												//alert('result = ' + resp);
											dbcal.fullCalendar( 'refetchEvents');
												window.parent.TINY.box.hide();

										    	},
										    	error: function() {
												alert('error in saving');
										    	},
									    	});
									
								});			
							}
						});
		    	},
		    	error: function() {
		    	},
	    	});

	    },
    eventSources: [
    {
	url: 'events_json.php',
	type: 'POST',
	data: {
	    type: 'fetchEventsDashBoard'
	},
	error: function() {
	  alert('there was an error while fetching events!');
	},
    }]
    
  });
  

//Lead Page

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



var eid=getParameterByName('lid');
if(eid=="" || eid==null)
{
	eid=0;
}

  //add lead calendar
  $("#add-lead-calendar").fullCalendar({
	   header: {
	      left: "prev,next",
	      center: "title",
	      right: "month,agendaWeek,agendaDay"
	    },
	    editable: true,
	    droppable: true,
	    defaultView: 'agendaWeek',
	    minTime:10,
	    maxTime:23,
	    firstDay:dd,
	    height:700,
	    defaultEventMinutes:180,
	    slotEventOverlap:true,
	    dayClick: function( date, allDay, jsEvent, view ) {
		//alert(date);
        	//dayClicked(date);
    	    },
	    eventClick: function( event, jsEvent, view ) {
    		
		//console.log(event);

		var dealerid=$('#deal').val();

		var data = {
    			type: 'editEvent',
			title: event.title,
			id:event.id,
			appttype:event.appttype,
			start:event.start,
			end:event.end,
			dealerid:dealerid
		}
    		$.ajax({
	    		type: "POST",
	    		url: "event_functions.php",
	    		data: data,
	    		success: function(output) {
					var obj = JSON.parse(output);
	    				TINY.box.show({
							html:obj.popupHtml,
							width:700,
							openjs:function(){
								$('#edit-event-dialog-table #event-time-from').val(obj.startSecs);
								$('#edit-event-dialog-table #event-time-to').val(obj.endSecs);
								$('#edit-event-dialog-table #edit-event-dialog-start-date').datepicker({
									autoclose:true								
								});
				$('#edit-event-dialog-table #event-time-from').change(function(){
				if($('#edit-event-dialog-type').val()=='AdvancedTrainingClinics'){
			$('#edit-event-dialog-table #event-time-to').val(parseInt($('#edit-event-dialog-table #event-time-from').val() )+ 3600);
			}
else{
	$('#edit-event-dialog-table #event-time-to').val(parseInt($('#edit-event-dialog-table #event-time-from').val()) + 10800);

}
		
								$('#edit-event-dialog-type').val(obj.appttype);
						});

var typeval1=$('#edit-event-dialog-type').val();
	
	if(typeval1=="Demo"){
		$('#trnteam').hide();
	}else if(typeval1=="Training"){
		$('#trnteam').show();
	}else if(typeval1=="AdvancedTrainingClinics"){
		$('#trnteam').hide();
	}

$('#memberslist').select2();
$('#memberslist').addClass('chzn-select');

$('#edit-event-dialog-type').change(function() { 

	typeval=$('#edit-event-dialog-type').val();
	
	if(typeval=="Demo"){
		$('#trnteam').hide();
	}else if(typeval=="Training"){
		$('#trnteam').show();
	}else if(typeval=="AdvancedTrainingClinics"){
		$('#trnteam').hide();
	}

});
								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});

		$('a#delete-event-edit').click(function(){
		
			var eventid=$('#event-id-hd').val();
			var eventid1=$('#event-id-hd1').val();
			bootbox.confirm("Are you sure do you want to delete this event?", function(result) {
		
				 if (result == true) {
					var data = {
						type: 'DeleteEvent',
						eventid: eventid,
						eventid1:eventid1,	
					}
					$.ajax({
	    					type: "POST",
						data:data,
	    					url: "event_functions.php",
	    					success: function(output) {
							$('#add-lead-calendar').fullCalendar('refetchEvents');
							window.parent.TINY.box.hide();
						},
						error: function() {
							bootbox.alert('Error while Deleting the Event Details');
						}

					});
				} 
			}); 
					

		});
	
								$('a#save-event-edit').click(function(){

var totalSec = $('#edit-event-dialog-table #event-time-from').val();
var hours = parseInt( totalSec / 3600 ) % 24;
var minutes = parseInt( totalSec / 60 ) % 60;
var seconds = totalSec % 60;

var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

var totalSec2 = $('#edit-event-dialog-table #event-time-to').val();
var hours2 = parseInt( totalSec2 / 3600 ) % 24;
var minutes2 = parseInt( totalSec2 / 60 ) % 60;
var seconds2 = totalSec2 % 60;

var result2 = (hours2 < 10 ? "0" + hours2 : hours2) + ":" + (minutes2 < 10 ? "0" + minutes2 : minutes2) + ":" + (seconds2  < 10 ? "0" + seconds2 : seconds2);
var newdate=$('#edit-event-dialog-table #edit-event-dialog-start-date').val();
var newdatesplit=newdate.split("/");
									
									var eventtype = $('#edit-event-dialog-type').val();
									var eventTitle = $('#edit-event-dialog-table #edit-event-dialog-title1').val() + ' - ' + $('#edit-event-dialog-table #edit-event-dialog-title2').val()+ ' - ' + $('#edit-event-dialog-table #edit-event-dialog-title3').val();   
									var startDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result;
									var endDate = newdatesplit['2'] + "-" +newdatesplit['1'] + "-" + newdatesplit['0'] + ' ' + result2;
									var memberslist=$('#memberslist').val();
									var eventid = $('#edit-event-dialog-table #event-id-hd').val();
						
									var data = {
							    			type: 'editsaveEvent',
										id: eventid,
										title: eventTitle,
										type1: eventtype,
										start:startDate,
										end:endDate,
										memberslist: memberslist
									 }

									$.ajax({
								    		type: "POST",
								    		url: "event_functions.php",
								    		data: data,
								    		success: function(resp) {
										//alert('result = ' + resp);
										$('#add-lead-calendar').fullCalendar( 'refetchEvents');
											window.parent.TINY.box.hide();
									    	},
									    	error: function() {
											alert('error in edit saving');
									    	},
								    	});
	
									//alert("callback function in javascript/calender_functions.js");
									
								});			
							}
						});
		    	},
		    	error: function() {
		    	},
	    	});
	    },
	    eventSources: [{
		url: 'event_functions.php',
		type: 'POST',
		data: {
		  type: 'fetchEvents',
		  editeventid:eid,
		  dealeruserid:$('#deal').val()
		},
		error: function() {
		  bootbox.alert('There was an error while fetching events!');
		},
      	   }],
	   drop: function( date, allDay, jsEvent, ui ) {
    		/*var leadId = $(this).attr('id');*/
		var leadId = eid;
		var dealerid=$('#deal').val();

	//alert("Event Drop : "+leadId+" ; Dealer : "+dealerid);

    		var eventTitle = $(this).html();
    		var startDate = Math.round(date.getTime() / 1000);
    		var NameAddr = eventTitle.split(" - ");

    		if(allDay){
    			var endDate = startDate;
    		}else{
    			var endDate = startDate+(3*60*60);//Add default event hours of 3
    		}

		var data={
				type: 'check',
				leadId:leadId,
					}
		$.ajax({
				   type: "POST",
				   url: "update_appt_status.php",
				   data: data,
				   success: function(resp) {
						
				if(resp!=0)
				{
					bootbox.alert(resp);
					return false;
				}
		var data = {
    			type: 'update_appt_status',
			
			 }
    		$.ajax({
	    		type: "POST",
	    		url: "update_appt_status.php",
	    		data: data,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:350,
							height:300,
							openjs:function(){
								$('#mtype').change(function(){
var val=$('#mtype option:selected').attr("class")+" showsubtype";
$('#msubtype .showsubtype').css({display:'none'});
$('#msubtype .showsubtype').removeAttr("selected");
$('#msubtype option[class="'+val+'"]').css({'display':'block'});
});
								$('#status').change(function(){

							var a=$('#status').val();
							//$("#apptresult option").removeAttr("selected","selected");
							 $("#result option").prop('selected', false);

							if(a=='Cancelled'){
							 $("#result option[value='Cancelled Appt']").prop('selected', true);
							}
							if(a=='Call To Reset'){
							 $("#result option[value='Reset']").prop('selected', true);
							}
							if(a=='Rescheduled'){
							 $("#result option[value='Pending']").prop('selected', true);
							}
							if(a=='Confirmed'){
							 $("#result option[value='Pending']").prop('selected', true);
							}
							if(a=='Needs Confirmed'){
							 $("#result option[value='Pending']").prop('selected', true);
							}
							if(a=='OK to Go'){
							 $("#result option[value='Pending']").prop('selected', true);
							}


						});

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	
								$('a#save').click(function(){
									var status=$('#status').val();
									var result=$('#result').val();
									var ltype=$('#mtype').val();
									var lsubtype=$('#msubtype').val();
							if(ltype!=''){
								if(lsubtype=='')
								{
							$('.msg').css({'display':'block'});
							$('.msg').html('Pls choose Lead Subtype Status');
							return false;
								}
									}
									if(status=='')
								{
							$('.msg').css({'display':'block'});
							$('.msg').html('Pls choose Appointment Status');
							return false;
								}
							if(result=='')
								{
								result='Pending';
								}
								var data={
									type: 'update',
									leadId:leadId,
									start:startDate,
									end:endDate ,
									status:status,
									result:result,
									ltype:ltype,
									lsubtype:lsubtype,
									
										}
									$.ajax({
									    		type: "POST",
									    		url: "update_appt_status.php",
									    		data: data,
									    		success: function(resp) {
											
											if(resp!=0)
											{
												bootbox.alert(resp);
												return false;
											}
									var data = {
						    			type: 'addEvent',
									title: eventTitle,
									leadId:leadId,
									allDay:allDay,
									start:startDate,
									end:endDate ,
									status:status,
									result:result,
									dealerid:dealerid
										}
									$.ajax({
									    		type: "POST",
									    		url: "event_functions.php",
									    		data: data,
									    		success: function(resp) {
											
											if(resp!=0)
											{
												bootbox.alert(resp);
											}

										$('#add-lead-calendar').fullCalendar( 'refetchEvents');
												window.parent.TINY.box.hide();
							$('#apptstatus option[value="'+status+'"]').prop('selected', true);
							$('#apptresult option[value="'+result+'"]').prop('selected', true);	
if(ltype!=''){
$('#ltype option[value="'+ltype+'"]').prop('selected', true);
var val=ltype+" showsubtype";
$('#lsubtype .showsubtype').css({display:'none'});
$('#lsubtype .showsubtype').removeAttr("selected");
$('#lsubtype option[class="'+val+'"]').css({'display':'block'});
$('#lsubtype option[value="'+lsubtype+'"]').prop('selected', true);
}
										    	},
										    	error: function() {
												alert('error in saving');
										    	},
									    	});
									//alert("callback function in javascript/calender_functions.js");
										}
							});										

								});			
							}
						});
}
						});
		    	},
		    	error: function() {
		    	},
	    	});
		    	},
		
    		
    		//alert("Dropped on " + date + " with allDay=" + allDay);

eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
var appttype=event.appttype;
if(appttype=="AvailabilityTime"){
return false;
}
function formatAMPM(date) {
	var dateString = '';
var  d = date.getDate();
var month=new Array();
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";
	var mon = month[date.getMonth()]; 
var  y = date.getFullYear();
var h = date.getHours();
var m = date.getMinutes();
var s = date.getSeconds();

if (h < 10) h = '0' + h;
if (m < 10) m = '0' + m;
if (s < 10) s = '0' + s;

dateString =d+'-'+mon+'-'+y+' '+ h + ':' + m + ':' + s;
return dateString;
	}
	
	var start= event.start;

	var starttime=formatAMPM(start);


		var data = {
    			type: 'editlead',
			start:starttime,
			id:event.id,
			 }
    		$.ajax({

	    		type: "POST",
	    		url: "rescheduled_appointment.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:900,
							height:650,
							openjs:function(){

							$('#resch').addClass('tabRS');

							$('#startdate').datetimepicker({
								format:'m/d/Y',
								lang:'en',
								validateOnBlur:false,
								timepicker:false,
							});

							$('#starttime').datetimepicker({
								format:'h:i A',
								lang:'en',
								validateOnBlur:false,
								formatTime:'h:i A',
		allowTimes:[
			  '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', 				  '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', 				  '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', 
			  '11:30 PM', '12:00 AM'
			 ],
								datepicker:false
							});
							
							$('#datepicker').datepicker({
								autoclose:true,
								validateOnBlur:false,
																										
								});
                                                           
             						$('#datepicker1').datepicker({
								autoclose:true,
								validateOnBlur:false,							
								});
							

$("#status option[value='Rescheduled']").prop('selected', true);
 $("#result option[value='Reset']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").show();
 $("#ctrnote").hide();

$('#create_task').click(function(){
var s=$('#status').val();
if(s=='Call To Reset'){

window.location.href ='createtask.php?prefill=true';
}
else
{
window.location.href ='createtask.php';
}
});


$('#status').change(function(){
var a=$('#status').val();

$("#cmt_tr").show();
$("#ntcnote").hide();
$("#resch").hide();
$("#ctrnote").hide();

//$("#apptresult option").removeAttr("selected","selected");
 $("#result option").prop('selected', false);

if(a=='Cancelled'){
 $("#result option[value='Cancelled Appt']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").hide();
}
if(a=='Call To Reset'){
 $("#result option[value='Reset']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").show();
}
if(a=='Rescheduled'){
 $("#result option[value='Reset']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").show();
 $("#ctrnote").hide();
}
if(a=='Confirmed'){
 $("#result option[value='Pending']").prop('selected', true);
 $("#ntcnote").hide();
 $("#ctrnote").hide();
}
if(a=='Needs Confirmed'){
 $("#result option[value='Pending']").prop('selected', true);
 $("#ntcnote").show();
 $("#resch").hide();
 $("#ctrnote").hide();
}
if(a=='OK to Go'){
 $("#result option[value='Pending']").prop('selected', true);
 $("#ntcnote").hide();
 $("#resch").hide();
 $("#ctrnote").hide();
}

if(a==''){
	$("#cmt_tr").hide();
}


});

$('.delete').click(function(){
var agree=confirm("Are you sure you want to delete this status update?");
	if (agree)
		return true ;
	else
		return false ;

});

$('a#recruite').click(function(){
var x=$('#id').val();
var y=$('#l_id').val();
//window.parent.TINY.box.hide();
var data = {
    			type: 'suggession',
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url: "suggession.php",
	    		data: data,
	    		success: function(output) {
				var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:250,
							openjs:function(){

						$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	

							$('a#save-event-edit').click(function(){


							
							var leadid=$('#ld').val();
			if($('.take').is(':checked')) {
					var selectedRadio = $("input[name='take']:checked").val();
	
				 }
				else{
					$('.msg').css({'display':'block'});
					$('.msg').html('Pls choose Any One Option');
					return false;
				}	




					var data = {
								    	type: 'redirect',
									leadid:leadid,
									take:selectedRadio
								 }
								 
				 $.ajax({
					type: "POST",
					url: "suggession.php",
					data: data,
					success: function(resp) {
		        		window.location.href = resp;
					//window.parent.TINY.box.hide();
								}

									});
								});
						}    
					});
				}
		}); 

});	
		$('a#cancel-event-edit').click(function(){
			window.parent.TINY.box.hide();
		});	

		$('a#save-event-edit').click(function(){
					var x=$('#id').val();
					var y=$('#l_id').val();
					var appt_status = $('#status').val()
					if(appt_status=='')
					{
					$('.msg').css({'display':'block'});
					$('.msg').html('Pls choose Appointment Status');
					return false ;
					}
					var appt_result = $('#result').val();
					var dealer=$('#dealer').val();
					var aptdatetime=$('#apptdatetime').val();
					var comments=$("#cmts").val();
					var startdate=$("#startdate").val();							
					var starttime=$("#starttime").val();
					var resetreason="";

					if(appt_status=="Rescheduled")
					{
						resetreason=$("#rresetreason").val();
					}else{
						resetreason=$("#cresetreason").val();
					}

					if(appt_status=="Rescheduled"){

						if(startdate!="" && starttime!="")
						{
							var data = {
						    	type: 'editSaveEvent',
							id: x,
							leadid:y,
							status: appt_status,
							result: appt_result,
							dealer:dealer,
							aptdatetime:aptdatetime,
							comments:comments,
							startdate:startdate,
							starttime:starttime,
							resetreason:resetreason
						 }
						 
						 $.ajax({
						    		type: "POST",
						    		url: "rescheduled_appointment.php",
						    		data: data,
						    		success: function(resp) {
								$('#add-lead-calendar').fullCalendar('refetchEvents');
								bootbox.alert(resp);
									window.parent.TINY.box.hide();
								}

							});
						}else{
							bootbox.alert('Please Select Rescheduled Appointment Date and Time!');
						}
					}else{
						var data = {
						    	type: 'editSaveEvent',
							id: x,
							leadid:y,
							status: appt_status,
							result: appt_result,
							dealer:dealer,
							aptdatetime:aptdatetime,
							comments:comments,
							startdate:startdate,
							starttime:starttime,
							resetreason:resetreason
						 }
						 
						 $.ajax({
						    		type: "POST",
						    		url: "rescheduled_appointment.php",
						    		data: data,
						    		success: function(resp) {
									$('#add-lead-calendar').fullCalendar('refetchEvents');
									bootbox.alert(resp);
									window.parent.TINY.box.hide();
								}

							});
					 }
				});									
			}				    	

		});
	    }
            

        });
/*
		var eventTitle = event.title;
    		var startDate = event.start;
    		var endDate = event.end;
		
		var data = {
    			type: 'saveEvent',
			id:event.id,
			title: eventTitle,
			allDay:allDay,
			start:startDate,
			end:endDate }
			$.ajax({
		    		type: "POST",
		    		url: "event_functions.php",
		    		data: data,
		    		success: function(resp) {
					//alert('success msg');
					//alert('result = ' + resp);
	    			
			    	},
			    	error: function() {
			    	},
		    	});
		/*

		if (!confirm("Are you sure about this change?")) {
		    revertFunc();
		}
		*/

	    },
	eventResize: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
		var eventTitle = event.title;
    		var startDate = event.start;
    		var endDate = event.end;
		
		var data = {
    			type: 'saveEvent',
			id:event.id,
			title: eventTitle,
			allDay:allDay,
			start:startDate,
			end:endDate }
			$.ajax({
		    		type: "POST",
		    		url: "event_functions.php",
		    		data: data,
		    		success: function(resp) {
					//alert('success msg');
					//alert('result = ' + resp);
	    			
			    	},
			    	error: function() {
			    	},
		    	});
	    }
  });


//Dealers Calenders

var JsonEvents=[{"id":"","tseqid":"","title":"","start":"","end":"","allDay":0,"address":"","appttype":"","backgroundColor":"","borderColor":"","textColor":""}];

$('#deal').change(function(){

	dealerid=$('#deal').val();

	var source="event_functions.php?type=fetchEvents&dealeruserid="+dealerid;

	/*$("#dealercalendar").fullCalendar('removeEvents'); 
	$("#dealercalendar").fullCalendar('addEventSource',source); */

	$("#add-lead-calendar").fullCalendar('removeEvents'); 
	$("#add-lead-calendar").fullCalendar('addEventSource',source); 


});

$('#dealercalendar').fullCalendar({
             header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                    },
                defaultView: 'agendaWeek',
		minTime:10,
		maxTime:23,
		firstDay:dd,
		aspectRatio:1,
		slotEventOverlap:false,
		events: JsonEvents
		
		

  }); // dealer calender


$('#dealer_availability').fullCalendar({
             header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                    },
                defaultView: 'agendaWeek',
		minTime:10,
		maxTime:23,
		firstDay:dd,
		aspectRatio:1,
		slotEventOverlap:false,
		events: JsonEvents,
		slotEventOverlap:false,
		selectable: true,
		selectHelper: true,
		
		select: function(start, end, allDay, jsEvent, view) {

	function formatAMPM(date) {
	var dateString = '';
var  d = date.getDate();
var month=new Array();
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";
	var mon = month[date.getMonth()]; 
var  y = date.getFullYear();
var h = date.getHours();
var m = date.getMinutes();
var s = date.getSeconds();

if (h < 10) h = '0' + h;
if (m < 10) m = '0' + m;
if (s < 10) s = '0' + s;

dateString =d+'-'+mon+'-'+y+' '+ h + ':' + m + ':' + s;
return dateString;
	}
	
	

	var starttime=formatAMPM(start);
	var endtime=formatAMPM(end);

var data = {
    			type: 'available_for_demo',
			start:starttime,
			end:endtime,	
		}
    		$.ajax({
	    		type: "POST",
	    		url: "deal_avail.php",
	    		data: data,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:300,
							openjs:function(){
			
							$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});
								$('a#save-event-edit').click(function(){
									
									var data = {
							    			type: 'saveavailabilitytime',
										count:$('#count').val(),
										start:starttime,
										end:endtime,	
										}
										$.ajax({
									    		type: "POST",
									    		url: "deal_avail.php",
									    		data: data,
									    		success: function(resp) {
									bootbox.alert('Your Availability Time has been added');
												//alert('success msg');
												//alert('result = ' + resp);
									$('#dealer_availability').fullCalendar( 'refetchEvents');
												window.parent.TINY.box.hide();

										    	},
										    	error: function() {
												alert('error in saving');
										    	},
									    	});
									
								});			
							}
						});
		    	},
		    	error: function() {
		    	},
	    	});


	},
eventClick: function(calEvent, jsEvent, view) {

  var data = {
    			type: 'delete',
			id:calEvent.id,
		}
    		$.ajax({
	    		type: "POST",
	    		url: "deal_avail.php",
	    		data: data,
	    		success: function(output) {
						var obj = JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:200,
							openjs:function(){
			
							$('a#cancel').click(function(){
									window.parent.TINY.box.hide();
								});
								$('a#delete').click(function(){
				bootbox.confirm("Are you sure to delete?", function(result) {	
								if(result==true){	
									var data = {
							    			type: 'delete_time',
										id:calEvent.id,
										}
										$.ajax({
									    		type: "POST",
									    		url: "deal_avail.php",
									    		data: data,
									    		success: function(resp) {
									bootbox.alert('Your Availability Time has been deleted');
												//alert('success msg');
												//alert('result = ' + resp);
									$('#dealer_availability').fullCalendar( 'refetchEvents');
												window.parent.TINY.box.hide();

										    	},
										    	error: function() {
												alert('error in deleting');
										    	},
									    	});
	}		});
								});
											
							}
						});
		    	},
		    	error: function() {
		    	},
	    	});
      
       

    }

  });

$('#copy').click(function(){
var d=$('#dealer_availability').fullCalendar( 'getDate');
		var data = {
			type: 'copy',
			date:d
		}
			$.ajax({
			type: "POST",
			url: "deal_avail.php",
			data: data,
			success: function(resp) {
			bootbox.alert('Your Availability Time has been Copied from previous week');
			$('#dealer_availability').fullCalendar( 'refetchEvents');
			window.parent.TINY.box.hide();
				},
			error: function() {
			alert('error in copying');
			},
			});
});
    
var JsonEvents=[{"id":"","tseqid":"","title":"","start":"","end":"","allDay":0,"address":"","appttype":"","backgroundColor":"","borderColor":"","textColor":""}];

	var source="deal_avail.php?type=fetchDealerEvents";

	$("#dealer_availability").fullCalendar('removeEvents'); 
	$("#dealer_availability").fullCalendar('addEventSource',source); 


$('#test11').click(function() {
	var date=new Date();

   	 $('#calendar').fullCalendar('gotoDate', date );
});



});  // document ready






function dayClicked(){

	var date=new Date();
alert("Day Click : "+date);

 	$('#calendar').fullCalendar('gotoDate', date );
	
}
