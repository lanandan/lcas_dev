(function($) {
    $.fn.limiter = function(options) {

        var that = this;
        options = $.extend({
            limit: 100,
            counter: $('#counter')
        }, options);

        var count = function(value, e) {

            var total = options.limit - value.length;

            if (total >= 0) {
                options.counter.text(total);
            } else {

                if (e.type == 'keypress') {

                    e.preventDefault();


                }
            }


        };

        return that.each(function() {

            $(that).on('keypress keyup', function(evt) {

                count($(that).val(), evt);

            });

        });

    };
})(jQuery);

