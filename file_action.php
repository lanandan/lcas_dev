<?php
session_start();
require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
ob_start();
validate_login();
function Download($path, $speed = null)
{
    if (is_file($path) === true)
    {
    	set_time_limit(0);

    	while (ob_get_level() > 0)
    	{
    		ob_end_clean();
    	}

    	$size = sprintf('%u', filesize($path));
    	$speed = (is_null($speed) === true) ? $size : intval($speed) * 1024;

    	header('Expires: 0');
    	header('Pragma: public');
    	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    	header('Content-Type: application/octet-stream');
    	header('Content-Length: ' . $size);
    	header('Content-Disposition: attachment; filename="' . basename($path) . '"');
    	header('Content-Transfer-Encoding: binary');

    	for ($i = 0; $i <= $size; $i = $i + $speed)
    	{
    		echo file_get_contents($path, false, null, $i, $speed);

    		while (ob_get_level() > 0)
    		{
    			ob_end_clean();
    		}

    		flush();  
    		sleep(1);
    	}

    	exit();
    }
else{
$res="There is No file found <button class='btn btn-default' onclick='history.back(1);'>
<span>Back To Previous Page</span>
</button>";
return $res;
}
    return false;
}





if(request_get('type')=="download")
{
$file_path="images/attachments/".request_get('attachments');
echo Download($file_path);
}
else if(request_get('type')=="delete_option")
{
$lid=$_REQUEST['lid'];
$sql=mysql_query("select attachments from tps_campaign where id='$lid'") or die(mysql_error());
$row=mysql_fetch_array($sql);
$filename="images/attachments/".$row['attachments'];
unlink($filename);
$sql=mysql_query("update  tps_campaign set attachments='' where id='$lid'") or die(mysql_error());
}
?>
