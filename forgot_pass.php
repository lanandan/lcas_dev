<?php
session_start();

require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

$username="";
if (isset($_POST['username']) ) {
	$username = request_get('username');

	$login_qry = "select * from tps_users where username='$username' AND status ='1' ";
	$result = mysql_query($login_qry) or die(mysql_error());
		
	if (mysql_num_rows($result) == 0 ) 
	{
		$error_flag = 1;
		$error_message .= "Invalid Username given!! Please try again.<br/>";	
		tps_log_error(__INFO__, __FILE__, __LINE__, "Forgot password Failed [$username]");
	}
	else {
		$row=mysql_fetch_array($result);
		$userid = $row['id'];
		$username = $row['username'];
		$guid = create_guid();
		$url = getHostUrl()."/reset_pass.php?id=".$guid;

		$forgot_pass_ins_qry = "insert into tps_forgot_pass (id, userid, username, guid, url, used, createddate, createdby) values (NULL, '$userid', '$username', '$guid', '$url', '0', now(), '". $_SERVER['REMOTE_ADDR']."') ";
		mysql_query($forgot_pass_ins_qry) or die(mysql_error());

		// email users with the password reset link

		tps_log_error(__INFO__, __FILE__, __LINE__, "Forgot password Success [$username]");
		$error_flag = 2;
		$error_message = "An link for resetting the password is emailed to your email. Please click the link and follow instructions!!";

	}
}
?>


<!doctype html>
<html>
<head>

  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">


  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine or request Chrome Frame -->
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">

  <!-- Use title if it's in the page YAML frontmatter -->
  <title>LCAS Forgot Password</title>

<link href="stylesheets/application.css" media="screen" rel="stylesheet" type="text/css" />
<script src="javascripts/application.js" type="text/javascript"></script>
<script language="JavaScript">
function submitform()
{
document.frm_forgot_pass.submit();
}
</script>


</head>

<body>

<nav class="navbar navbar-default navbar-inverse navbar-static-top" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <a class="navbar-brand" href="#">LCAS Forgot Password</a>

    
  </div>

  
</nav>
<div class="container">
  
<div class="col-md-4 col-md-offset-4">


  <div class="padded">
    <div class="login box" style="margin-top: 80px;">

<?php
if ( $error_flag == 1 ) {
?>

<div class="alert alert-error">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Username not found!</strong> <?php echo $error_message; ?>
</div>
<br/>

<?php

}
else if ( $error_flag == 2 ) {
?>

<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Well done!</strong> <?php echo $error_message; ?>
</div>
<br/>

<?php
}
?>



      <div class="box-header">
        <span class="title">LCAS Forgot Password</span>
      </div>

      <div class="box-content padded">
        <form class="separate-sections" name="frm_login" id="frm_forgot_pass" action="forgot_pass.php" method="POST">
          <div class="input-group addon-left">
            <span class="input-group-addon" href="#">
              <i class="icon-user"></i>
            </span>
            <input type="text" name="username" placeholder="username" required title="User Name"  />
          </div>

          <div>
            <a class="btn btn-blue btn-block" href="#" onClick="submitform();" >
                Submit <i class="icon-signin"></i>
            </a>
          </div>

        </form>

        <div>
          <a href="#">
              Don't have an account? <strong>Sign Up</strong>
          </a>
        </div>
        <div>
          <a href="login.php">
              Got your password? <strong>Login Here</strong>
          </a>
        </div>

      </div>

    </div>

   </div>
</div>
</div>

<?php
echo "<hr/>";
echo "SESSION ";
var_dump($_SESSION);
echo "<hr/>";
echo "POST Vars";
var_dump($_POST);
echo "<hr/>";
?>

</body>
</html>
