<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

//validate_login();

$page_name = "tasks.php";
$page_title = $site_name." -  Tasks";
if( request_get('action')=='delete') {
$id= request_get('lid');
$sql="update tps_events set delete_flag='1' where id='$id'";
$result=mysql_query($sql) or die(mysql_error());
$message="your Task has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);
header("location:tasks.php");
exit;
}
include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>
<script type="text/javascript">

$(document).ready(function() {
$(window).load(function(){
  setTimeout(function(){ $('.msg').fadeOut() }, 3000);
});

	
			
	$('#dealer').change(function() { 
	
		var dealer=$('#dealer').val();

		var data = {
    			type: 'filterbydealer',
			dealer:dealer
		}
    		$.ajax({
	    		url: "apptfilters.php",
	    		data: data,
	    		success: function(output) {
				$('#dispContent').html(output);
			}
		});

	});
	

});

function saveChanges(object){   
    $.ajax({
        url: 'edit_task.php',
        data: 'content=' + object.value,
        cache: false,
        
        success: function(response){
            
            var val=object.value;
            var value=val.replace(/\s/g, '');
            $('.name').css({display:'none'});
           

	document.getElementById(value).style.display='block';

}
    });   
}


  function loadAjaxContent(x,y)
    { 

var data = {
    			type: 'editlead',
			id:x,
			leadid:y
		}
    		$.ajax({

	    		type: "POST",
	    		url: "edit_task.php",
	    		data: data,
	    		success: function(output) {
						var obj =JSON.parse(output);
    				TINY.box.show({
							html:obj.popupHtml,
							width:600,
							height:550,
							openjs:function(){
							$('#datepicker').datepicker({
								autoclose:true								
								});
                                                           
             					$('#datepicker1').datepicker({
								autoclose:true								
								});

								$('a#cancel-event-edit').click(function(){
									window.parent.TINY.box.hide();
								});	

							$('a#save-event-edit').click(function(){
							var appt_status = $('#status').val();
							var appt_status1=appt_status.replace(/\s/g, '');
							var appt_result = $('#'+appt_status1+' #result').val();
                                                        var remarks = $('#'+appt_status1+' #comments').val();
							var datepicker = $('#'+appt_status1+' .datestartnline').val();
							var besttime =$('#'+appt_status1+' #besttime').val();
							
							var data = {
							    	type: 'editSaveEvent',
								id: x,
								leadid:y,
								status: appt_status,
								result: appt_result,
								remarks: remarks,
								re_date: datepicker,
    								besttime: besttime,
							
							 }


								$.ajax({
								    		type: "POST",
								    		url: "edit_task.php",
								    		data: data,
								    		success: function(resp) {
                                                                                window.location.href = 'tasks.php';
									        window.parent.TINY.box.hide();
 							
									}

									});
						});									
					}				    	

				});
	    }
            

        });
    }


</script>
<script>

function confirmDelete() 
{
	var agree=confirm("Are You Sure To Delete This Task?");
	if (agree)
		return true ;
	else
		return false ;
}

</script>
<div class="main-content" >
<div class="container">
<br /><br />

  <div class="col-md-16">
    <div class="box" >
      <div class="box-header"><span class="title">Tasks</span>
<?php if( get_session('e_flag') == 1 ) 
{       
        $message =get_session('message');
	echo '<div class="msg" id="msg" style="margin-left:400px;margin-top:10px;">'.$message.'</div>';
	unset($_SESSION['e_flag']);
	unset($_SESSION['message']);
}
?>
</div>
      <div class="box-content" style="height:600px;" align="center">
	
<div id="dataTables">	
	<?php
	$userid=get_session('LOGIN_USERID');
	$today=date('Y-m-d H:i:s');
	$res=mysql_query("select * from tps_events where appttype='Task' and start>='".$today."' and delete_flag='0' and createdby='".$userid."' order by start asc")or die(mysql_error());

	$table="<table width='98%' class='dTable responsive'><thead>";
	$table.="<tr><td>Action</td><td>Date</td><td>Title</td><td>Update</td></tr></thead><tbody>";
	$i=1;
	while($r=mysql_fetch_array($res))
	{$lcres=mysql_query("select id from tps_lead_card where leadid='".$r['lead_id']."'") or die(mysql_error());
		
		$lcr=mysql_fetch_array($lcres);
		$table.='<tr><td align="center"> <a href="add_leads.php?action=edit&lid='.$lcr['id'].'" title="Edit"><img src="images/edit.png"  title="Edit"/></a>&nbsp;|&nbsp;<a href="tasks.php?action=delete&lid='.$r['id'].'" title="Delete" onClick="return confirmDelete();" > <img src="images/block.png" width="15px" class="key_image" title="Delete"/></a></td>';

		$table.="<td>".date('M d, Y h:i:s A',strtotime($r['start']))."</td>";
			
		$table.="<td>".$r['title']."</td>";			
	
		
		
		$table.='<td> <a href="#" style="text-decoration:underline;" onclick="loadAjaxContent('.$r['id'].','.$r['lead_id'].');"><img src="images/update.png"  title="Update Status"/ height="20px" width="20px"></a> </td></tr>';
		$i++;
	}
	$table.="<tbody></table>";
		
	echo $table;
		
	?>
</div> 

      </div>
	<br />
    </div>
	<br /><br />
   </div>

 </div> 
 </div>

<?php

include "lcas_footer.php";

?>
