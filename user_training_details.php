<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");

 $tab_class='';
 $current_tab='';
validate_login();

$page_name = "user_training_details.php";
$page_title = $site_name." -  Event Listing";

include "lcas_header.php";
include "lcas_top_nav.php";
include "js/user_training_details.js";
?>

<div id="spinner"></div>
<div class="main-content" style="margin:0px;">
  <div class="container">
    <div class="row">
      <div class="area-top clearfix">
        <div class="pull-left header">
          <h3 class="title">
	<i class="icon-magic"></i>Event Listing&nbsp;&nbsp;<a class="btn btn-blue" href="create_event.php"><span>Create Event</span></a>
	</h3>
</div> 

      </div>
    </div>
  </div>

<div class="container">
  <div class="col-md-16">
    <div class="box" >
      <div class="box-header"><span class="title">Event Listing</span>
</div>
   <div class="box-content" style="min-height:500px;" >
  <ul class="nav nav-tabs" id="lb-tabs">
 <?php   
$ssss=mysql_query("select DISTINCT topics_parent_id from tps_training_details where delete_flag='0'")or die(mysql_error());
$j=1;
while($tr_name=mysql_fetch_array($ssss)){
$tnres=mysql_query("select id,event_type from tps_event_type where id='$tr_name[topics_parent_id]'") or die(mysql_error());
$tnr=mysql_fetch_array($tnres);
if($j==1){
$current_tab = $tnr['event_type'];
}
 $tab_class = ($tnr['event_type']==$current_tab) ? 'active' : '' ;
        echo '<li class="'.$tab_class.'"><a  class="tab" href="#'.str_replace(' ','',$tnr['event_type']).'" data-toggle="tab">' .           
        $tnr['event_type'] .  ' </a></li>';
		$j++;
}
    ?>
    </ul><!-- /nav-tabs -->
   		 <div class="tab-content">
		
        <?php 
$ssss=mysql_query("select DISTINCT topics_parent_id from tps_training_details where delete_flag='0'")or die(mysql_error());
while($tr_name=mysql_fetch_array($ssss)){
$tnres=mysql_query("select id,event_type from tps_event_type where id='$tr_name[topics_parent_id]'") or die(mysql_error());
$tnr=mysql_fetch_array($tnres);
 $tab = $tnr['event_type'];

        //set the class to "active" for the active content.
        $content_class = ($tab==$current_tab) ? 'active' : '' ;
        ?>
		
       <div class="tab-pane <?php echo $content_class;?>" id="<?php echo str_replace(' ','',$tab); ?>">
	   <input type="hidden" value="<?php echo str_replace(' ','',$tab); ?>" class="div">
            <div class="links">
                <ul class="col">
                  <li>
 <br>
		 <div align="center"> <span style="margin-left:20px;">Team Member</span><select class="user" name="user"   >
<option value="">All</option>
<?php 
	$usrres=mysql_query("select id,parentid,userid,username,nickname,fname,lname from tps_users where status='1'") or die(mysql_error()); 
        while($usr=mysql_fetch_array($usrres))
	{
		if($usr['nickname']!="")
			$displayname=ucfirst($usr['nickname'])." ".$usr['lname'];
		else
			$displayname=ucfirst($usr['fname'])." ".$usr['lname'];

		echo '<option value='.$usr['userid'].'>'.$displayname.'</option>';
	}
?>
</select>


<span style="margin-left:10px;">Event Subtype</span><select  name="subtype" class="subtype"  >
<option value=''>All</option>

<?php $sql="select id,parent,event_subtype from tps_event_subtype where status='0' and parent='$tr_name[topics_parent_id]' ";
	
	$result=mysql_query($sql) or die(mysql_error());
	
	while($row = mysql_fetch_array($result)){
echo '<option value='.$row['id'].'>'.$row['event_subtype'].'</option>';
}
 ?>                  	
</select> 
<input type="hidden" value="<?php echo $tr_name['topics_parent_id']; ?>" class="sub">
<span style="margin-left:10px;">From </span>
<input type="text" class="from" name="sdate">
<span style="margin-left:10px;">to </span>
<input type="text" class="to" name="edate">
<span style="margin-left:10px;">Status</span><select  name="status" class="status"  >
<option value=''>All</option>
<option value='1'>Attended</option>
<option value='0'>Not Attended</option>
</select>
<a href="#" class="list" ><button type="button"  class="btn btn-blue"  class="list" style="margin-left:10px;">
                        List
                      </button></a>
<a href="#" class="reset"><button type="button" id="reset"  class="btn btn-blue" class="reset" style="margin-left:10px;">
                        Reset
                      </button></a>
</div>	<hr>
					  
<?php

	echo '<div class="user_table">';		 
$tabname=$tnr['event_type'];
	$trnid=array();
	$trnname=array();
		
	$tnres=mysql_query("select id,event_subtype from tps_event_subtype where parent='$tr_name[topics_parent_id]'") or die(mysql_error());

	$usrres=mysql_query("select id,parentid,userid,username,nickname,fname,lname from tps_users where status='1'") or die(mysql_error());
			
	$table="<table width='100%' cellspacing='0' class='dTable responsive'   ><thead>";
	$table.="<tr><th><div style='height:100%;display:inline-block;'>USER ID</div></th>";
			
	$i=0;
	while($tnr=mysql_fetch_array($tnres))
	{
		$table.="<th><div style='height:100%;display:inline-block;'>".$tnr['event_subtype']."</div></th>";
		$trnid[$i]=$tnr['id'];
		$trnname[$i]=$tnr['event_subtype'];
		$i++;
	}
			
	$trncnt=count($trnid);

	$table.="</tr></thead><tbody>";
			
	while($usr=mysql_fetch_array($usrres))
	{
		if($usr['nickname']!="")
			$displayname=ucfirst($usr['nickname'])." ".$usr['lname'];
		else
			$displayname=ucfirst($usr['fname'])." ".$usr['lname'];

		$table.="<tr><td><div style='height:100%;display:inline-block;'>".$usr['userid']." ".$displayname."</div></td>";
		for($b=0;$b<$trncnt;$b++)
		{
			$res=mysql_query("SELECT * FROM `tps_training_details`
					  WHERE receiveruserid='$usr[userid]' and FIND_IN_SET($trnid[$b], topicids) ") or die(mysql_error());

			$table.="<td><div style='height:100%;display:inline-block;'>";
			while($r=mysql_fetch_array($res))
			{
						
				$s=$r['starttime'];
				$s1=$r['endtime'];

				$p=explode(" ",$s);
				$a=explode("-",$p[0]);

				$dt=$a[1]."/".$a[2]."/".$a[0];
					
				$se=explode(" ",$s1);

				$dt1=date('M d Y', strtotime($dt));

				$st=date('h:i a',strtotime($p[1]));
				$ed=date('h:i a',strtotime($se[1]));
				
				$invdate=$dt1." ".$st." - ".$ed;
				$table.=$invdate." ";
				if($r['isattended']=="1")
				{
					$table.="<img src='images/yes.png' width='15px' height='15px' /><hr>";
				}
				else if($r['isattended']=="0")
				{
					$table.="<img src='images/no.png' width='15px' height='15px' /><hr>";
				}else{
					$table.="<img src='images/no.png' width='15px' height='15px' /><hr>";
				}
			}
			$table.="</div></td>";
						
		}
		 		
		$table.="</tr>";
	}

	$table.="</tbody></table>";

	echo $table; 
	echo '</div>';	
	echo '</li>'; 
	echo '</ul>';

?>
            </div>
        </div><!-- /tab-pane  -->
<?php } ?>
    </div><!-- /tab-content  -->

<!-- END OF YOUR CODE -->
    
 </div> 
   
      </div>
    </div>
   </div>

 </div> 
 </div>
<?php

include "lcas_footer.php";
?>
