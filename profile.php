<?php
session_start();

require_once("include/tps_constants.php");
require_once("include/tps_db_conn.php");
require_once("include/tps_gen_functions.php");
$exp[]='';
$exp='';
$page_name = "profile.php";
$page_title = $site_name." -edit Profile";
$message='';
if( request_get('action')=='delete')
{
$id=get_session('LOGIN_ID');
$sql= "update tps_users set image1='default.jpeg' WHERE id = '$id'";
$result=mysql_query($sql) or die(mysql_error());
$message="your Profile picture has been deleted";
		set_session('e_flag' , 1);
		set_session('message' , $message);	
        header("location:viewprofile.php");
	exit;
}
$id=get_session('LOGIN_ID');

if( isset($_POST['save']) ){
$pic=request_get('pic');
	if (isset($_FILES["photo"]["type"])) {
		if ((($_FILES["photo"]["type"] == "image/gif")
			|| ($_FILES["photo"]["type"] == "image/jpeg")
			|| ($_FILES["photo"]["type"] == "image/jpg")
			|| ($_FILES["photo"]["type"] == "image/pjpeg")
			|| ($_FILES["photo"]["type"] == "image/x-png")
			|| ($_FILES["photo"]["type"] == "image/png")))
		{       
			
			$expl=explode(".",$_FILES['photo']['name']);	

                   	  $ext= $expl[sizeof($expl) - 1];
                          $user_id=get_session('LOGIN_USERID');
                          $file_name= $user_id.".".$ext;

			$target = "images/upload/"; 
			$target = $target . $_FILES['photo']['name']; 
			$pic=$file_name; 
                        makeThumbnails("images/upload/thumb/", $_FILES['photo']['tmp_name'], $user_id,50,50);
			move_uploaded_file($_FILES['photo']['tmp_name'],$target);
			 rename($target,"images/upload/".$file_name);

		}
		
	}
$phone1='';
$phone2='';
$phone3='';
if(request_get('phone1')!='(   )    .'){ 
 $phone1=request_get('phone1');
}
if(request_get('phone2')!='(   )    .'){ 
 $phone2=request_get('phone2');
}
if(request_get('phone3')!='(   )    .'){ 
 $phone3=request_get('phone3');
}
$sql = "update tps_users set ".
        "nameprefix = '".ltrim(request_get('nameprefix'),"Select")."', ".
	"fname = '".request_get('fname')."', ".
	"lname = '".request_get('lname')."', ".
	"address1 = '".request_get('address1')."', ".
	"address2 = '".request_get('address2')."', ".
	"city = '".request_get('city')."', ".
	"state = '".ltrim(request_get('state'),"Select")."', ".
	"zipcode = '".request_get('zipcode')."', ".
	"phone1 = '".$phone1."', ".
        "phone2 = '".$phone2."', ".
        "phone3 = '".$phone3."', ".
        "phonetype1 = '".request_get('phonetype1')."', ".
        "phonetype2 = '".request_get('phonetype2')."', ".
        "phonetype3 = '".request_get('phonetype3')."', ".
	"email1 = '".request_get('email1')."', ".
        "email2 = '".request_get('email2')."', ".
	"image1 = '".$pic."' ".
	" where id = '".$id."' ";

   
	$result=mysql_query($sql) or die(mysql_error());
	$message="Your profile has been saved";
		set_session('e_flag' , 1);
		set_session('message' , $message);	
        header("location:viewprofile.php");
	exit;
	
}

if($id != ''){
	$sql="select * from tps_users where id='".$id."'";
	$result=mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_array($result);
	$nameprefix=$row['nameprefix'];
	$fname=$row['fname'];
	$lname=$row['lname'];
	$address1=$row['address1'];
	$address2=$row['address2'];
	$city=$row['city'];
	$state=$row['state'];
	$zipcode=$row['zipcode'];
	$phone1=$row['phone1'];
        $phone2=$row['phone2'];
        $phone3=$row['phone3'];
        $phonetype1=$row['phonetype1'];
        $phonetype2=$row['phonetype2'];
        $phonetype3=$row['phonetype3'];
	$email1=$row['email1'];
        $email2=$row['email2'];
	$image1=$row['image1'];
}


include "lcas_header.php";
include "lcas_top_nav.php";
include "lcas_left_nav.php";

?>

<script type="text/javascript" src="javascripts/jquery.timepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="stylesheets/jquery.timepicker.css" />

  <script type="text/javascript" src="javascripts/base.js"></script>
  <link rel="stylesheet" type="text/css" href="stylesheets/base1.css" />
	<script src="javascripts/datepair.js"></script> 

 
   <script src="js/jquery.maskedinput.min.js"></script> 

<script  type="text/javascript" >



$(document).ready(function() {
    $('#zip').blur(function() {
        $.ajax({
            type: "POST",
            url: "zipret.php",
            data: {
                zip: $("#zip").val()
            },
            success: function(result) {
		
		var obj = jQuery.parseJSON (result);
		if (obj.status == "TRUE") {
			$('#city').val(obj.city);
			$('#state1').val(obj.state);
			
		}
		if (obj.status == "FALSE") {
			$('#city').val("");
			$('#state1').val("PA");
		}
            }
        });
    });

$('#ltype').change(function() {
        $.ajax({
            type: "POST",
	    dataType: "json",
            url: "subtyperet.php",
            data: {
		
                ltype: $("#ltype").val()
            },
            success: function(result) {
		$('#lsubtype').empty();
		$(result).each(function () {
		    // Create option
		    var $option = $("<option />");
		    // Add value and text to option

		    $option.attr("value", this.sublvalue).text(this.subltext);
		    // Add option to drop down list
		    $(lsubtype).append($option);
		});  
		
            }
        });
    });

$('#lsubtype').change(function() {
        $.ajax({
            type: "POST",
	    dataType: "json",
            url: "giftret.php",
            data: {
		
                ltype: $("#ltype").val(),
		lsubtype: $("#lsubtype").val()
            },
            success: function(result) {
		
		$('#gift').empty();
		$(result).each(function () {
		    // Create option
		    var $option = $("<option />");
		    // Add value and text to option
		    $option.attr("value", this.value).text(this.text);
		    // Add option to drop down list
		    $(gift).append($option);

		});            
            }
        });
    });

 $('.firstli').change(function() {
 //alert("echeck");
	var $txtval1 = $('#prefname1').val();
$('.first').empty();
$('.first').append(' ');
if($txtval1 != 'Select')
$('.first').append($txtval1);
$('.first').append(' ');
	var $txtval2 = $('#fname1').val();
	$('.first').append($txtval2);
var $txtval3 = $('#lname1').val();
if(($('.first').html() !='') ){
	
		$('.first').append(' ');
}
		$('.first').append($txtval3);
if(($('.first').html() !='') ){
	if($('#fname2').val() != '' )
		$('.first').append(' - ');
}

if(($('.first').html() !='') ){
	
		$('.first').append(' - ');
}
	var $txtval7 = $('#addr1').val();
$('.first').append($txtval7);
$('.first').append(' ');
	var $txtval8 = $('#addr2').val();
	$('.first').append($txtval8);
var $txtval9 = $('#city').val();
if(($('.first').html() !='') ){
	
	$('.first').append(' ');
}
	$('.first').append($txtval9);
if(($('.first').html() !='') ){
	
	$('.first').append(' ');
}
	var $txtval11 = $('#state1').val();
	$('.first').append($txtval11);
if(($('.first').html() !='') ){
	
		$('.first').append(' ');
}

var $txtval10 = $('#zip').val();
$('.first').append($txtval10);
if(($('.first').html() !='') ){
	
		$('.first').append(' - ');
}


    });


});
</script>
<script>
function myFunction()
{
	if(document.getElementById("chkemail1").checked==true && document.getElementById("chkemail2").checked==true ){
		document.getElementById("eoptin").value=3;
	}
	else if(document.getElementById("chkemail1").checked==true){
		document.getElementById("eoptin").value=1;
	}
	else if(document.getElementById("chkemail2").checked==true){

		document.getElementById("eoptin").value=2;
	}
	else{
		document.getElementById("eoptin").value='';
	}
}

function threehr()
{
	var curr_time = document.getElementById("appttimefrom").value;
	var to_time = parseInt(curr_time) + parseInt("10800");
	if ( parseInt(to_time) > parseInt("84600") ) {
		 to_time = "84600";
	}
	document.getElementById("appttimeto").value = to_time;
	
}

function validateEmail(email, err) {
  var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
 if(document.getElementById(email).value != '')
 {
	  if( emailReg.test(document.getElementById(email).value) == false ) {
		document.getElementById(err).style.display="block";
	    return false;
	  } else {
			document.getElementById(err).style.display="none";
	    return true;
	  } 
  }
}
function setapptres() {
 if(document.getElementById("apptdate").value != '')
 {
	document.getElementById("apptresult").value ='SET'
  }
}

function profconfirm() {
     var where_to= confirm("Are You Sure to delete your profile picture?");
 if (where_to==true)
{
     return true;
}
 else{
     
     return false;
 }
 
}		
	


</script>
<script type="text/javascript">
<!--//
function sizeFrame() {
var F = document.getElementById("myFrame");
//if(F.contentDocument) {
//F.height = F.contentDocument.documentElement.scrollHeight+30; //FF 3.0.11, Opera 9.63, and Chrome
//} else {
//F.height = F.contentWindow.document.body.scrollHeight+30; //IE6, IE7 and Chrome
//}

}

window.onload=sizeFrame;

//-->
</script>

<div class="main-content" >
<div class="container">
<br /><br />



  <div class="col-md-16">
    <div class="box" >
      <div class="box-header"><span class="title">Edit Profile</span></div>
	<br />
      <div class="box-content" style="height:750px;">
 		
	
	<form id="frm" name="frm" class="fillup" action="profile.php" method="POST" enctype="multipart/form-data">
 <div style="position:absolute; right:30px; border:1px solid black; height:150px; width:150px;border-radius:15px;">
<a href="profile.php?action=delete" title="Delete"  ><img src="images/delete.png" style="width:10px; position:absolute;margin-left:130px;margin-top:5px;<?php if($image1=="default.jpeg"){echo "display:none;";} ?>" id="pimage" class="key_image" title="Delete" onclick="return profconfirm()"/></a>
<img src="images/upload/<?php echo $image1; ?>"style="border-radius:15px;height:100%;width:100%;"></div> 
<div class="padded">
<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Name Prefix </label>
<div class="col-lg-10">
			 <select id="title" name="nameprefix" class="span4" onchange="">
				<?php echo getNamePrefix($nameprefix); ?>
                          </select>
		</div>
	</div><br><br>
	<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">First Name </label>
		<div class="col-lg-10">
			<input class="col-md-4" type="text" name="fname" id="fname" value="<?php echo $fname; ?>" placeholder="First Name" required/>
		</div>
	</div><br><br>
	<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Last Name</label>
		<div class="col-lg-10">
			<input class="col-md-4" type="text" name="lname" id="lname" value="<?php echo $lname; ?>" placeholder="Last Name" required/>
		</div>
	</div><br><br>
	<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Address 1</label>
		<div class="col-lg-10">
			<input class="col-md-4" type="text" name="address1" id="address1" value="<?php echo $address1; ?>" placeholder="Address 1" />
		</div>
	</div><br><br>
	<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Address 2</label>
		<div class="col-lg-10">
			<input class="col-md-4" type="text" name="address2" id="address2" value="<?php echo $address2; ?>" placeholder="Address 2" />
		</div>
	</div><br><br>
	<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Zipcode</label>
		<div class="col-lg-10">
		<input class="col-md-4" type="text" name="zipcode" id="zip" placeholder="Zipcode" value="<?php echo $zipcode;?>" style="width:120px;"/>
</div>
	</div><br><br>
<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">City</label>
		<div class="col-lg-10">
		<input class="col-md-4" type="text" name="city" id="city" value="<?php echo $city; ?>" placeholder="City"/>
</div>
	</div><br><br>
	<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">State</label>
		<div class="col-lg-10">
			
		<select class="nline" name="state" id="state1" style="margin-left:0px;">
		<option> Select </option>
		<?php 
			
                        
			echo getStates($state);
		?>
		</select>
		</div>
	</div><br><br>
</select>
		
		<br/>
<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Phone Number1</label>
		<div class="col-lg-10">
		<select class="nline " name="phonetype1" id="phone1_type"style="margin-left:0px;width:80px">
		<?php echo getPhoneType($phonetype1); ?>
		</select>
		<?php
		if($phone1 != '')
			echo '<input class="nline " type="text" name="phone1" id="phone1" value="'.$phone1.'"  placeholder="Phone Number" required />';
		else
			echo '<input class="nline " type="text" name="phone1" id="phone1"  placeholder="Phone Number" ';
		?>
		</div>
	</div><br><br>
		<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Phone Number2</label>
		<div class="col-lg-10">
		<select class="nline" name="phonetype2"style="margin-left:0px;width:80px" >
		<?php echo getPhoneType($phonetype2); ?>
		</select>
		<?php
		if($phone2 != '')
			echo '<input class="nline" type="text" name="phone2" id="phone2" value="'.$phone2.'"  placeholder="Phone Number"/>';
		else
			echo '<input class="nline" type="text" name="phone2" id="phone2"   placeholder="Phone Number"/>';
		?></div>
	</div><br><br>
<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Phone Number3</label>
		<div class="col-lg-10">
		<select class="nline" name="phonetype3"style="margin-left:0px;width:80px" >
		<?php echo getPhoneType($phonetype3); ?>
		</select>
		<?php
		if($phone3 != '')
			echo '<input class="nline" type="text" name="phone3" id="phone3" value="'.$phone3.'"  placeholder="Phone Number"/>';
		else
			echo '<input class="nline" type="text" name="phone3" id="phone3"  placeholder="Phone Number"/>';
		?>
		
		</div>
	</div><br><br>
	
	
	<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Email-1</label>
		<div class="col-lg-10">
			<input class="col-md-4" type="text" name="email1" id="email" value="<?php echo $email1; ?>" placeholder="Email" required/>
		</div>
	</div><br><br>
<div class="form-group">
		<label class="control-label col-lg-2" style="text-align:right;">Email-2</label>
		<div class="col-lg-10">
			<input class="col-md-4" type="text" name="email2" id="email" value="<?php echo $email2; ?>" placeholder="Email" />
		</div>
	</div><br><br>
	<div class="form-group">
                      <label class="control-label col-lg-2" style="text-align:right;">
                      Image upload
                      </label>
                      <div id="uniform-photo" class="uploader" style="width:275px;margin-left:15px;">
                        <input   class="col-md-4" id="photo" name="photo" type="file" value="<?php echo $image1; ?>" style="width:500px;">  
	
                      </div>
                    </div>
<input type="hidden" name="pic" value="<?php echo $image1; ?>">
	

	<input class="btn btn-info pull-right" style="margin-right:10px;" type="submit" name="save" id="save" value="Save changes">

	<button class="btn btn-info pull-right" style="margin-right:10px;" type="button" onclick="window.location.href='<?php echo 'viewprofile.php'; ?>'">Cancel</button>

	</form>
<script>
jQuery(function($) {

$('#phone1').mask('(999) 999-9999');
$('#phone2').mask('(999) 999-9999');
$('#phone3').mask('(999) 999-9999');

});
</script> 
	
      </div>
	<br />
    </div>
	<br /><br />
</div>
   </div>

 </div> 

<?php

include "lcas_footer.php";
?>
